<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\User;

/**
 * @name \Change\User\ProfileManager
 */
class ProfileManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'ProfileManager';

	const EVENT_LOAD = 'load';

	const EVENT_PROFILES = 'profiles';

	const EVENT_SAVE = 'save';

	const EVENT_PROFILE_FIELDS = 'getProfileFields';

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/ProfileManager');
	}

	/**
	 * @return string[]
	 */
	public function getProfileNames()
	{
		$event = new \Change\Events\Event(static::EVENT_PROFILES, $this);
		$this->getEventManager()->triggerEvent($event);
		$profiles = $event->getParam('profiles');
		if (is_array($profiles))
		{
			return array_keys(array_count_values($profiles));
		}
		return [];
	}

	/**
	 * @param UserInterface $user
	 * @param string $profileName
	 * @return \Change\User\ProfileInterface|null
	 */
	public function loadProfile($user, $profileName)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['user' => $user, 'profileName' => $profileName]);
		$event = new \Change\Events\Event(static::EVENT_LOAD, $this, $args);
		$em->triggerEvent($event);

		$profile = $event->getParam('profile');
		if ($profile instanceof \Change\User\ProfileInterface)
		{
			return $profile;
		}
		return null;
	}

	/**
	 * @param UserInterface $user
	 * @param \Change\User\ProfileInterface $profile
	 * @return \Change\User\ProfileInterface|null
	 */
	public function saveProfile($user, $profile)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['user' => $user, 'profile' => $profile]);
		$event = new \Change\Events\Event(static::EVENT_SAVE, $this, $args);
		$em->triggerEvent($event);

		$profile = $event->getParam('profile');
		if ($profile instanceof \Change\User\ProfileInterface)
		{
			return $profile;
		}
		return null;
	}

	/**
	 * @param string $profileFieldsName
	 * @return ProfileFieldInterface[]
	 */
	public function getProfileFields($profileFieldsName)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['profileFieldsName' => $profileFieldsName, 'profileFields' => null]);
		$em->trigger(static::EVENT_PROFILE_FIELDS, $this, $args);

		$profileFields = $args['profileFields'];
		if (is_array($profileFields))
		{
			return array_values(array_filter($profileFields, function ($field)
			{
				return $field instanceof ProfileFieldInterface;
			}));
		}
		return [];
	}
}