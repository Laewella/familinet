<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\User;

/**
 * @name \Change\User\ProfileFieldInterface
 */
interface ProfileFieldInterface
{
	/**
	 * @return string
	 */
	public function getProfileName();

	/**
	 * @return string
	 */
	public function getFieldName();

	/**
	 * @return boolean
	 */
	public function getReadonly();
}