<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Transaction;

/**
 * @name \Change\Transaction\DefaultListeners
 * @ignore
 */
class DefaultListeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callBack = function(\Change\Events\Event $event) {
			$event->getApplicationServices()->getDbProvider()->beginTransaction($event);
			$event->getApplicationServices()->getDocumentManager()->beginTransaction($event);
		};
		$this->listeners[] = $events->attach(TransactionManager::EVENT_BEGIN, $callBack, 5);

		$callBack = function(\Change\Events\Event $event) {
			$event->getApplicationServices()->getDocumentManager()->commit($event);
			$event->getApplicationServices()->getDbProvider()->commit($event);
		};
		$this->listeners[] = $events->attach(TransactionManager::EVENT_COMMIT, $callBack, 5);

		$callBack = function(\Change\Events\Event $event) {
			$event->getApplicationServices()->getDocumentManager()->rollBack($event);
			$event->getApplicationServices()->getDbProvider()->rollBack($event);
		};
		$this->listeners[] = $events->attach(TransactionManager::EVENT_ROLLBACK, $callBack, 5);
	}
}