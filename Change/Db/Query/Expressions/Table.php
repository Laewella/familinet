<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Expressions;

/**
 * @name \Change\Db\Query\Expressions\Table
 */
class Table extends AbstractExpression
{
	/**
	 * @var string
	 */
	protected $database;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $alias;

	/**
	 * @param string $name
	 * @param string $database
	 */
	public function __construct(string $name = null, string $database = null)
	{
		$this->name = $name;
		$this->database = $database;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getDatabase()
	{
		return $this->database;
	}

	/**
	 * @param string $database
	 */
	public function setDatabase(string $database = null)
	{
		$this->database = $database;
	}

	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		$identifierParts = [];
		$dbName = $this->database;
		$tableName = $this->name;
		if (!\Change\Stdlib\StringUtils::isEmpty($dbName))
		{
			$identifierParts[] = '"' . $dbName . '"';
		}
		$identifierParts[] = '"' . $tableName . '"';
		return implode('.', $identifierParts);
	}
}