<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Expressions;

//For php code size optimisation
use Change\Db\ScalarType;

/**
 * @name \Change\Db\Query\Expressions\Value
 */
class Value extends AbstractExpression
{
	/**
	 * @var mixed
	 */
	protected $value;
	
	/**
	 * @var integer
	 */
	protected $scalarType;
	
	/**
	 * return \Change\Db\ScalarType::* constant value
	 * @return integer
	 */
	public function getScalarType()
	{
		return $this->scalarType;
	}

	/**
	 * @param integer $scalarType
	 */
	public function setScalarType($scalarType)
	{
		switch ($scalarType) 
		{
			case ScalarType::BOOLEAN:
			case ScalarType::DATETIME:
			case ScalarType::DECIMAL:
			case ScalarType::INTEGER:
			case ScalarType::LOB:
			case ScalarType::STRING:
			case ScalarType::TEXT:
				$this->scalarType = $scalarType;
				break;
		}
	}

	/**
	 * @param mixed $value
	 * @param integer $scalarType \Change\Db\ScalarType::*
	 */
	public function __construct($value = null, int $scalarType = ScalarType::STRING)
	{
		$this->value = $value;
		$this->setScalarType($scalarType);
	}
	
	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}
	
	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}
	
	/**
	 * @return boolean
	 */
	public function isNull()
	{
		return ($this->value === null);
	}
	
	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		if ($this->value === null)
		{
			return 'NULL';
		}
		return "'" . str_replace('\'', '\\\'', (string)$this->value) . "'";
	}
}