<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Mysql;

use Change\Db\Schema\FieldDefinition;
use Change\Db\Schema\TableDefinition;

/**
 * @name \Change\Db\Mysql\SchemaManager
 */
class SchemaManager implements \Change\Db\InterfaceSchemaManager
{
	/**
	 * @var \Change\Db\Mysql\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Logging\Logging
	 */
	protected $logging;

	/**
	 * @var \PDO
	 */
	protected $pdo;

	/**
	 * @var string[]
	 */
	protected $tables;

	/**
	 * @var \Zend\Stdlib\Parameters
	 */
	protected $options;

	/**
	 * @param \Change\Db\Mysql\DbProvider $dbProvider
	 * @param \Change\Logging\Logging $logging
	 */
	public function __construct(\Change\Db\Mysql\DbProvider $dbProvider, \Change\Logging\Logging $logging)
	{
		$this->setDbProvider($dbProvider);
		$this->setLogging($logging);
	}

	/**
	 * @return \Change\Db\Mysql\DbProvider
	 */
	public function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @param \Change\Db\Mysql\DbProvider $dbProvider
	 */
	public function setDbProvider(\Change\Db\Mysql\DbProvider $dbProvider)
	{
		$this->dbProvider = $dbProvider;
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	public function getLogging()
	{
		return $this->logging;
	}

	/**
	 * @param \Change\Logging\Logging $logging
	 */
	public function setLogging(\Change\Logging\Logging $logging)
	{
		$this->logging = $logging;
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getOptions()
	{
		if ($this->options === null)
		{
			$this->options = new \Zend\Stdlib\Parameters();
		}
		return $this->options;
	}

	/**
	 * @return \PDO
	 */
	private function getDriver()
	{
		if ($this->pdo === null)
		{
			if ($this->dbProvider->getDisabled())
			{
				throw new \RuntimeException('This DB provider is disabled!', 999999);
			}
			$this->pdo = $this->dbProvider->getConnection($this->dbProvider->getConnectionInfos());
		}
		return $this->pdo;
	}

	/**
	 * @param string $query
	 * @return \PDOStatement
	 * @throws \Exception
	 */
	private function query($query)
	{
		return $this->getDriver()->query($query);
	}

	/**
	 * @return string|NULL
	 */
	public function getName()
	{
		$ci = $this->dbProvider->getConnectionInfos();
		return $ci['database'] ?? null;
	}

	/**
	 * @return boolean
	 */
	public function check()
	{
		$dbProvider = $this->dbProvider;
		if ($dbProvider->getDisabled() || $dbProvider->getReadOnly())
		{
			return false;
		}
		try
		{
			$dbProvider->getConnection($dbProvider->getConnectionInfos());
		}
		catch (\PDOException $e)
		{
			$this->getLogging()->exception($e);
			return false;
		}
		return true;
	}

	/**
	 * @return void
	 */
	public function closeConnection()
	{
		$this->pdo = null;
	}

	/**
	 * @param string $sql
	 * @return integer the number of affected rows
	 * @throws \Exception on error
	 */
	public function execute($sql)
	{
		return $this->getDriver()->exec($sql);
	}

	/**
	 * @param string $script
	 * @param boolean $throwOnError
	 * @throws \Exception on error
	 */
	public function executeBatch($script, $throwOnError = false)
	{
		foreach (explode(';', $script) as $sql)
		{
			$sql = trim($sql);
			if (!$sql)
			{
				continue;
			}
			$this->getDriver()->exec($sql);
		}
	}

	/**
	 * Drop all tables from current configured database
	 */
	public function clearDB()
	{
		$tables = $this->getTableNames();
		if (count($tables))
		{
			foreach ($this->getTableNames() as $table)
			{
				try
				{
					$this->execute('DROP TABLE `' . $table . '`');
				}
				catch (\Exception $e)
				{
					$this->logging->warn($e->getMessage());
				}
			}
		}
		$this->tables = null;
	}

	/**
	 * @return string[]
	 */
	public function getTableNames()
	{
		if ($this->tables === null)
		{
			$sql = 'SELECT `TABLE_NAME` FROM `information_schema`.`TABLES` WHERE `TABLE_SCHEMA` = \'' . $this->getName() . '\'';
			$stmt = $this->query($sql);
			$this->tables = $stmt->fetchAll(\PDO::FETCH_COLUMN);
		}
		return $this->tables;
	}

	/**
	 * @param string $tableName
	 * @return \Change\Db\Schema\TableDefinition|null
	 */
	public function getTableDefinition($tableName)
	{
		$tableDef = null;
		$sql = 'SELECT `COLUMN_NAME`, `COLUMN_DEFAULT`, `IS_NULLABLE`, `DATA_TYPE`, `COLUMN_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE`, `EXTRA` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = \'' . $this->getName() . '\' AND `TABLE_NAME` = \'' . $tableName . '\' ORDER BY `ORDINAL_POSITION`';
		$statement = $this->query($sql);
		foreach ($statement->fetchAll(\PDO::FETCH_NUM) as $row)
		{
			if ($tableDef === null)
			{
				$tableDef = new TableDefinition($tableName);
			}
			list($name, $defaultValue, $nullable, $dataType, $ctype, $maxLength, $precision, $scale, $extra) = $row;
			$fd = new FieldDefinition($name);
			switch ($dataType)
			{
				case 'int':
				case 'bigint':
				case 'smallint':
					$type = FieldDefinition::INTEGER;
					break;
				case 'tinyint':
					$type = FieldDefinition::SMALLINT;
					break;
				case 'double':
				case 'float':
					$type = FieldDefinition::FLOAT;
					break;
				case 'decimal':
					$type = FieldDefinition::DECIMAL;
					break;
				case 'datetime':
					$type = FieldDefinition::DATE;
					break;
				case 'timestamp':
					$type = FieldDefinition::TIMESTAMP;
					break;
				case 'varchar':
					$type = FieldDefinition::VARCHAR;
					break;
				case 'char':
					$type = FieldDefinition::CHAR;
					break;
				case 'enum':
					$type = FieldDefinition::ENUM;
					$values = explode('\',\'', substr($ctype, 6, strlen($ctype) - 8));
					$fd->setOption('VALUES', $values);
					break;
				case 'mediumblob':
				case 'blob':
				case 'longblob':
					$type = FieldDefinition::LOB;
					break;
				case 'mediumtext':
				case 'text':
				case 'longtext':
					$type = FieldDefinition::TEXT;
					break;
				default:
					$type = FieldDefinition::LOB;
					break;
			}
			$fd->setType($type);
			if ($precision !== null)
			{
				$fd->setPrecision($precision);
			}
			if ($scale !== null)
			{
				$fd->setScale($scale);
			}
			if ($maxLength !== null)
			{
				$fd->setLength($maxLength);
			}
			$fd->setNullable($nullable === 'YES');
			$fd->setDefaultValue($defaultValue);
			if ($extra === 'auto_increment')
			{
				$fd->setAutoNumber(true);
			}

			$tableDef->addField($fd);
		}
		$statement->closeCursor();

		if ($tableDef)
		{
			$sql = 'SHOW INDEX FROM `' . $this->getName() . '`.`' . $tableName . '`';
			$statement = $this->query($sql);
			/* @var $k \Change\Db\Schema\KeyDefinition */
			$k = null;
			foreach ($statement->fetchAll(\PDO::FETCH_ASSOC) as $row)
			{
				if ($k === null || $k->getName() !== $row['Key_name'])
				{
					$k = new \Change\Db\Schema\KeyDefinition();
					$tableDef->addKey($k);
					$k->setName($row['Key_name']);
					if ($row['Key_name'] === 'PRIMARY')
					{
						$k->setType(\Change\Db\Schema\KeyDefinition::PRIMARY);
					}
					elseif ($row['Non_unique'])
					{
						$k->setType(\Change\Db\Schema\KeyDefinition::INDEX);
					}
					else
					{
						$k->setType(\Change\Db\Schema\KeyDefinition::UNIQUE);
					}
				}
				$k->addField($tableDef->getField($row['Column_name']));
			}
		}

		return $tableDef;
	}

	/**
	 * @param string $tableName
	 * @return \Change\Db\Schema\TableDefinition
	 */
	public function newTableDefinition($tableName)
	{
		$td = new TableDefinition($tableName);
		$td->setOptions(['ENGINE' => 'InnoDB', 'CHARSET' => 'utf8', 'COLLATION' => 'utf8_unicode_ci']);
		return $td;
	}

	/**
	 * @param integer $scalarType
	 * @param array $fieldDbOptions
	 * @param array $defaultDbOptions
	 * @throws \InvalidArgumentException
	 * @return array
	 */
	public function getFieldDbOptions($scalarType, array $fieldDbOptions = null, array $defaultDbOptions = null)
	{
		switch ($scalarType)
		{
			case \Change\Db\ScalarType::STRING:
				if (!isset($fieldDbOptions['length']))
				{
					$fieldDbOptions['length'] = ($defaultDbOptions === null) ? 255 : (int)$defaultDbOptions['length'];
				}
				else
				{
					$fieldDbOptions['length'] = (int)$fieldDbOptions['length'];
				}
				return $fieldDbOptions;

			case \Change\Db\ScalarType::TEXT:
			case \Change\Db\ScalarType::LOB:
				if (!isset($fieldDbOptions['length']))
				{
					$fieldDbOptions['length'] = 16777215;
				}
				else
				{
					$fieldDbOptions['length'] = (int)$fieldDbOptions['length'];
				}
				return $fieldDbOptions;
			case \Change\Db\ScalarType::DECIMAL:
				if (!isset($fieldDbOptions['precision']))
				{
					$fieldDbOptions['precision'] = ($defaultDbOptions === null) ? 13 : (int)$defaultDbOptions['precision'];
				}
				else
				{
					$fieldDbOptions['precision'] = (int)$fieldDbOptions['precision'];
				}
				if (!isset($fieldDbOptions['scale']))
				{
					$fieldDbOptions['scale'] = ($defaultDbOptions === null) ? 4 : (int)$defaultDbOptions['scale'];
				}
				else
				{
					$fieldDbOptions['scale'] = (int)$fieldDbOptions['scale'];
				}
				return $fieldDbOptions;

			case \Change\Db\ScalarType::BOOLEAN:
			case \Change\Db\ScalarType::INTEGER:
				if (!isset($fieldDbOptions['precision']))
				{
					$def = $scalarType === \Change\Db\ScalarType::BOOLEAN ? 3 : 10;
					$fieldDbOptions['precision'] = ($defaultDbOptions === null) ? $def : (int)$defaultDbOptions['precision'];
				}
				$fieldDbOptions['scale'] = 0;
				return $fieldDbOptions;

			case \Change\Db\ScalarType::DATETIME:
				if (!is_array($fieldDbOptions))
				{
					$fieldDbOptions = is_array($defaultDbOptions) ? $defaultDbOptions : [];
				}
				return $fieldDbOptions;

			default:
				throw new \InvalidArgumentException('Invalid Field type: ' . $scalarType, 41000);
		}
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @throws \InvalidArgumentException
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newEnumFieldDefinition($name, array $dbOptions)
	{
		if (!isset($dbOptions['VALUES']) || !is_array($dbOptions['VALUES']) || count($dbOptions['VALUES']) == 0)
		{
			throw new \InvalidArgumentException('Invalid Enum values', 41001);
		}
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::ENUM);
		$fd->setOption('VALUES', $dbOptions['VALUES']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newCharFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::CHAR);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::STRING, $dbOptions);
		$fd->setLength($dbOptions['length']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newVarCharFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::VARCHAR);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::STRING, $dbOptions);
		$fd->setLength($dbOptions['length']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newNumericFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::DECIMAL);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::DECIMAL, $dbOptions);
		$fd->setPrecision($dbOptions['precision']);
		$fd->setScale($dbOptions['scale']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newBooleanFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::SMALLINT);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::BOOLEAN, $dbOptions);
		$fd->setPrecision($dbOptions['precision']);
		$fd->setScale($dbOptions['scale']);
		$fd->setNullable(false);
		$fd->setDefaultValue(0);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newIntegerFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::INTEGER);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::INTEGER, $dbOptions);
		$fd->setPrecision($dbOptions['precision']);
		$fd->setScale($dbOptions['scale']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newFloatFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::FLOAT);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newTextFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::TEXT);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::TEXT, $dbOptions);
		$fd->setLength($dbOptions['length']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newLobFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::LOB);
		$dbOptions = $this->getFieldDbOptions(\Change\Db\ScalarType::LOB, $dbOptions);
		$fd->setLength($dbOptions['length']);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newDateFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::DATE);
		return $fd;
	}

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newTimeStampFieldDefinition($name, array $dbOptions = null)
	{
		$fd = new FieldDefinition($name);
		$fd->setType(FieldDefinition::TIMESTAMP);
		$fd->setDefaultValue('CURRENT_TIMESTAMP');
		$fd->setNullable(false);
		return $fd;
	}

	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @return string SQL definition
	 */
	public function createOrAlterTable(\Change\Db\Schema\TableDefinition $tableDefinition)
	{
		if (in_array($tableDefinition->getName(), $this->getTableNames()))
		{
			$oldDef = $this->getTableDefinition($tableDefinition->getName());
			if ($oldDef)
			{
				return $this->alterTable($tableDefinition, $oldDef);
			}
		}
		return $this->createTable($tableDefinition);
	}

	/**
	 * @param FieldDefinition $fieldDefinition
	 * @throws \RuntimeException
	 * @return string
	 */
	protected function generateColumnType(FieldDefinition $fieldDefinition)
	{
		switch ($fieldDefinition->getType())
		{
			case FieldDefinition::CHAR:
				$type = 'char(' . $fieldDefinition->getLength() . ')';
				break;
			case FieldDefinition::VARCHAR:
				$type = 'varchar(' . $fieldDefinition->getLength() . ')';
				break;
			case FieldDefinition::DATE:
				$type = 'datetime';
				break;
			case FieldDefinition::TIMESTAMP:
				$type = 'timestamp';
				break;
			case FieldDefinition::DECIMAL:
				$type = 'decimal(' . $fieldDefinition->getPrecision() . ',' . $fieldDefinition->getScale() . ')';
				break;
			case FieldDefinition::ENUM:
				$values = $fieldDefinition->getOption('VALUES');
				if (!is_array($values) || count($values) == 0)
				{
					throw new \RuntimeException('Invalid Enum Values', 41001);
				}
				$type = 'enum(\'' . implode('\',\'', $values) . '\')';
				break;
			case FieldDefinition::FLOAT:
				$type = 'double';
				break;
			case FieldDefinition::INTEGER:
				$type = 'int(11)';
				break;
			case FieldDefinition::SMALLINT:
				$type = 'tinyint(1)';
				break;
			case FieldDefinition::LOB:
				$type = 'mediumblob';
				break;
			case FieldDefinition::TEXT:
				$type = 'mediumtext';
				break;
			default:
				throw new \RuntimeException('Invalid Field Definition type: ' . $fieldDefinition->getType(), 41002);
				break;
		}

		if (!$fieldDefinition->getNullable())
		{
			$type .= ' NOT NULL';
		}

		if ($fieldDefinition->getDefaultValue() !== null)
		{
			if ($fieldDefinition->getDefaultValue() === 'CURRENT_TIMESTAMP')
			{
				$type .= ' DEFAULT CURRENT_TIMESTAMP';
			}
			else
			{
				$type .= ' DEFAULT \'' . $fieldDefinition->getDefaultValue() . '\'';
			}
		}
		elseif ($fieldDefinition->getAutoNumber())
		{
			$type .= ' auto_increment';
		}
		elseif ($fieldDefinition->getNullable())
		{
			$type .= ' NULL';
		}
		return $type;
	}

	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @param \Change\Db\Schema\TableDefinition $oldDef
	 * @return string SQL definition
	 */
	public function alterTable(\Change\Db\Schema\TableDefinition $tableDefinition, \Change\Db\Schema\TableDefinition $oldDef)
	{
		$sqlScript = [];
		foreach ($tableDefinition->getFields() as $field)
		{
			/* @var $field \Change\Db\Schema\FieldDefinition */
			$oldField = $oldDef->getField($field->getName());
			$type = $this->generateColumnType($field);
			if ($oldField)
			{
				$oldType = $this->generateColumnType($oldField);
				if ($type != $oldType)
				{
					$sql = 'ALTER TABLE `' . $tableDefinition->getName() . '` CHANGE `' . $field->getName() . '` `'
						. $field->getName() . '` ' . $type;
					$sqlScript[] = $sql;
					$this->execute($sql);
				}
			}
			else
			{
				$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` ADD `' . $field->getName() . '` ' . $type;
				$sqlScript[] = $sql;
				$this->execute($sql);
			}
		}
		foreach ($tableDefinition->getKeys() as $key)
		{
			$add = true;
			if ($key->isPrimary())
			{
				foreach ($oldDef->getKeys() as $oldKey)
				{
					if ($oldKey->isPrimary())
					{
						if ($this->getSQLKey($oldKey) != $this->getSQLKey($key))
						{
							$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP PRIMARY KEY';
							$sqlScript[] = $sql;
							$this->execute($sql);
						}
						else
						{
							$add = false;
							break;
						}
					}
				}
			}
			else
			{
				foreach ($oldDef->getKeys() as $oldKey)
				{
					if ($oldKey->getName() == $key->getName())
					{
						if ($this->getSQLKey($oldKey) != $this->getSQLKey($key))
						{
							if ($oldKey->isPrimary())
							{
								$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP PRIMARY KEY';
							}
							elseif ($oldKey->isUnique())
							{
								$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP KEY `' . $oldKey->getName() . '`';
							}
							else
							{
								$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP INDEX `' . $oldKey->getName() . '`';
							}
							$sqlScript[] = $sql;
							$this->execute($sql);
						}
						else
						{
							$add = false;
							break;
						}
					}
				}
			}

			if ($add)
			{
				$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` ADD ' . $this->getSQLKey($key);
				$sqlScript[] = $sql;
				$this->execute($sql);
			}
		}

		if ($this->getOptions()->get('dropUndefinedKeys', false))
		{
			foreach ($oldDef->getKeys() as $oldKey)
			{
				$remove = true;
				if ($oldKey->isPrimary())
				{
					foreach ($tableDefinition->getKeys() as $key)
					{
						if ($key->isPrimary())
						{
							$remove = false;
							break;
						}
					}
				}
				else
				{
					foreach ($tableDefinition->getKeys() as $key)
					{
						if ($oldKey->getName() == $key->getName())
						{
							$remove = false;
							break;
						}
					}
				}

				if ($remove)
				{
					if ($oldKey->isPrimary())
					{
						$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP PRIMARY KEY';
					}
					elseif ($oldKey->isUnique())
					{
						$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP KEY `' . $oldKey->getName() . '`';
					}
					else
					{
						$sql = 'ALTER TABLE  `' . $tableDefinition->getName() . '` DROP INDEX `' . $oldKey->getName() . '`';
					}
					$sqlScript[] = $sql;
					$this->execute($sql);
				}
			}
		}

		if (count($sqlScript) && $this->getLogging())
		{
			$logging = $this->getLogging();
			foreach ($sqlScript as $sql)
			{
				$logging->info($sql);
			}
		}
		return implode(';' . PHP_EOL, $sqlScript);
	}

	/**
	 * @param \Change\Db\Schema\KeyDefinition $key
	 * @return string
	 */
	protected function getSQLKey(\Change\Db\Schema\KeyDefinition $key)
	{
		$kf = [];
		foreach ($key->getFields() as $kField)
		{
			/* @var $kField \Change\Db\Schema\FieldDefinition */
			$kf[] = '`' . $kField->getName() . '`';
		}

		if ($key->isPrimary())
		{
			return 'PRIMARY KEY  (' . implode(', ', $kf) . ')';
		}
		elseif ($key->isUnique())
		{
			return 'UNIQUE KEY `' . $key->getName() . '` (' . implode(', ', $kf) . ')';
		}
		else
		{
			return 'INDEX `' . $key->getName() . '` (' . implode(', ', $kf) . ')';
		}
	}

	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @return string SQL definition
	 */
	public function createTable(\Change\Db\Schema\TableDefinition $tableDefinition)
	{
		$sql = 'CREATE TABLE `' . $tableDefinition->getName() . '` (';
		$parts = [];
		foreach ($tableDefinition->getFields() as $field)
		{
			/* @var $field \Change\Db\Schema\FieldDefinition */

			$type = $this->generateColumnType($field);
			$parts[] = '`' . $field->getName() . '` ' . $type;
		}

		foreach ($tableDefinition->getKeys() as $key)
		{
			$parts[] = $this->getSQLKey($key);
		}

		$engine = $tableDefinition->getOption('ENGINE');
		if ($engine)
		{
			$engine = ' ENGINE= ' . $engine;
		}

		$startAuto = $tableDefinition->getOption('AUTONUMBER');
		if ($startAuto)
		{
			$startAuto = ' AUTO_INCREMENT=' . $startAuto;
		}

		$charset = $tableDefinition->getOption('CHARSET');
		if ($charset)
		{
			$charset = ' CHARACTER SET ' . $charset;
		}

		$collation = $tableDefinition->getOption('COLLATION');
		if ($collation)
		{
			$collation = ' COLLATE ' . $collation;
		}

		$sql .= implode(', ', $parts) . ')' . $engine . $startAuto . $charset . $collation;
		$this->execute($sql);
		if (is_array($this->tables) && !in_array($tableDefinition->getName(), $this->tables))
		{
			$this->tables[] = $tableDefinition->getName();
		}

		if ($this->getLogging())
		{
			$this->getLogging()->info($sql);
		}
		return $sql;
	}

	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @return string SQL definition
	 */
	public function dropTable(\Change\Db\Schema\TableDefinition $tableDefinition)
	{
		$sql = 'DROP TABLE IF EXISTS `' . $tableDefinition->getName() . '`';
		if (is_array($this->tables))
		{
			$this->tables = array_values(array_diff($this->tables, [$tableDefinition->getName()]));
		}
		$this->execute($sql);
		return $sql;
	}

	/**
	 * @return \Change\Db\Schema\SchemaDefinition
	 */
	public function getSystemSchema()
	{
		return new \Change\Db\Mysql\Schema($this);
	}
}