<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db;

use Change\Db\Query\AbstractQuery;
use Change\Db\Query\Builder;
use Change\Db\Query\StatementBuilder;
use Change\Logging\Logging;

/**
 * @name \Change\Db\DbProvider
 * @api
 */
abstract class DbProvider
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_SQL_FRAGMENT_STRING = 'SQLFragmentString';

	const EVENT_SQL_FRAGMENT = 'SQLFragment';

	/**
	 * @var array
	 */
	protected $connectionInfos;

	/**
	 * @var array|null
	 */
	protected $readOnlyConnectionInfos;

	/**
	 * @var array
	 */
	protected $timers;

	/**
	 * @var \Change\Db\SqlMapping
	 */
	protected $sqlMapping;

	/**
	 * @var AbstractQuery[]
	 */
	protected $builderQueries;

	/**
	 * @var AbstractQuery[]
	 */
	protected $statementBuilderQueries;

	/**
	 * @var string[]
	 */
	protected $listenerAggregateClassNames;

	/**
	 * @var boolean
	 */
	protected $readOnly = false;

	/**
	 * @var boolean
	 */
	protected $disabled = false;

	/**
	 * @var boolean
	 */
	protected $checkTransactionBeforeWriting = true;

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Db'];
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		$classNames = [];
		foreach ($this->getEventManagerIdentifier() as $identifier)
		{
			$entry = $this->getApplication()->getConfiguredListenerClassNames('Change/Events/' . str_replace('.', '/', $identifier));
			if (is_array($entry))
			{
				foreach ($entry as $className)
				{
					if (is_string($className))
					{
						$classNames[] = $className;
					}
				}
			}
		}
		return array_unique($classNames);
	}

	/**
	 * @return string
	 */
	public abstract function getType();

	/**
	 * @param \Change\Application $application
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		if ($this->connectionInfos === null)
		{
			$this->connectionInfos = [];
			$dbConfig = $application->getConfiguration()->getEntry('Change/Db');
			$section = $dbConfig['use'] ?? 'default';
			$connectionInfos = $dbConfig[$section] ?? [];
			$this->setConnectionInfos($connectionInfos);
			if ($connectionInfos)
			{
				$readOnlyConnectionInfos = $dbConfig[$section . 'ReadOnly'] ?? null;
				if ($readOnlyConnectionInfos['enabled'] ?? false)
				{
					$this->setReadOnlyConnectionInfos($readOnlyConnectionInfos);
				}
			}
			else
			{
				$this->setDisabled(true);
			}
		}
	}

	public function __construct()
	{
		$this->timers = ['init' => microtime(true), 'select' => 0, 'exec' => 0, 'longTransaction' => 0.2, 'roSelect' => 0];
	}

	/**
	 * @param boolean $readOnly
	 * @return $this
	 */
	public function setReadOnly($readOnly)
	{
		$this->readOnly = (bool)$readOnly;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getReadOnly()
	{
		return $this->readOnly;
	}

	/**
	 * @param boolean $disabled
	 * @return $this
	 */
	public function setDisabled($disabled)
	{
		$this->disabled = (bool)$disabled;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getDisabled()
	{
		return $this->disabled;
	}

	/**
	 * @return array
	 */
	public function getTimers()
	{
		return $this->timers ?: [];
	}

	/**
	 * @param boolean $checkTransactionBeforeWriting
	 * @return $this
	 */
	public function setCheckTransactionBeforeWriting($checkTransactionBeforeWriting)
	{
		$this->checkTransactionBeforeWriting = $checkTransactionBeforeWriting;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getCheckTransactionBeforeWriting()
	{
		return $this->checkTransactionBeforeWriting;
	}

	/**
	 * @return array
	 */
	public function getConnectionInfos()
	{
		return $this->connectionInfos;
	}

	/**
	 * @param array $connectionInfos
	 */
	public function setConnectionInfos(array $connectionInfos)
	{
		$this->connectionInfos = $connectionInfos;
		if (isset($connectionInfos['longTransaction']))
		{
			$this->timers['longTransaction'] = (float)$connectionInfos['longTransaction'];
		}
	}

	/**
	 * @return array|null
	 */
	public function getReadOnlyConnectionInfos()
	{
		return $this->readOnlyConnectionInfos;
	}

	/**
	 * @param array|null $readOnlyConnectionInfos
	 * @return $this
	 */
	public function setReadOnlyConnectionInfos(array $readOnlyConnectionInfos = null)
	{
		$this->readOnlyConnectionInfos = $readOnlyConnectionInfos;
		return $this;
	}

	/**
	 * @param AbstractQuery $query
	 */
	public function addBuilderQuery(AbstractQuery $query)
	{
		if ($query->getCachedKey() !== null)
		{
			$this->builderQueries[$query->getCachedKey()] = $query;
		}
	}

	/**
	 * @param null $cacheKey
	 * @return Builder
	 */
	public function getNewQueryBuilder($cacheKey = null)
	{
		$query = ($cacheKey !== null && isset($this->builderQueries[$cacheKey])) ? $this->builderQueries[$cacheKey] : null;
		return new Builder($this, $cacheKey, $query);
	}

	/**
	 * @param AbstractQuery $query
	 */
	public function addStatementBuilderQuery(AbstractQuery $query)
	{
		if ($query->getCachedKey() !== null)
		{
			$this->statementBuilderQueries[$query->getCachedKey()] = $query;
		}
	}

	/**
	 * @param string $cacheKey
	 * @throws \RuntimeException
	 * @return StatementBuilder
	 */
	public function getNewStatementBuilder($cacheKey = null)
	{
		$query = ($cacheKey !== null
			&& isset($this->statementBuilderQueries[$cacheKey])) ? $this->statementBuilderQueries[$cacheKey] : null;
		return new StatementBuilder($this, $cacheKey, $query);
	}

	public function __destruct()
	{
		unset($this->builderQueries, $this->statementBuilderQueries);
	}

	/**
	 * @return void
	 */
	public abstract function closeConnection();

	/**
	 * @return \Change\Db\InterfaceSchemaManager
	 */
	public abstract function getSchemaManager();

	/**
	 * @return \Change\Db\SqlMapping
	 */
	public function getSqlMapping()
	{
		if ($this->sqlMapping === null)
		{
			$this->sqlMapping = new SqlMapping();
		}
		return $this->sqlMapping;
	}

	/**
	 * @return Logging
	 */
	public function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public abstract function beginTransaction($event = null);

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public abstract function commit($event);

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public abstract function rollBack($event);

	/**
	 * @param string $tableName
	 * @return integer
	 */
	public abstract function getLastInsertId($tableName);

	/**
	 * @api
	 * @param Query\InterfaceSQLFragment $fragment
	 * @return string
	 */
	public abstract function buildSQLFragment(Query\InterfaceSQLFragment $fragment);

	/**
	 * @param Query\InterfaceSQLFragment $fragment
	 * @return string
	 */
	public function buildCustomSQLFragment(Query\InterfaceSQLFragment $fragment)
	{
		$event = new \Change\Events\Event(static::EVENT_SQL_FRAGMENT_STRING, $this, ['fragment' => $fragment]);
		$this->getEventManager()->triggerEvent($event);
		$sql = $event->getParam('sql');
		if (is_string($sql))
		{
			return $sql;
		}
		$this->getLogging()->warn(__METHOD__ . '(' . get_class($fragment) . ') not implemented');
		return $fragment->toSQL92String();
	}

	/**
	 * @param array $argument
	 * @return Query\InterfaceSQLFragment|null
	 */
	public function getCustomSQLFragment(array $argument = [])
	{
		$event = new \Change\Events\Event(static::EVENT_SQL_FRAGMENT, $this, $argument);
		$this->getEventManager()->triggerEvent($event);
		$fragment = $event->getParam('SQLFragment');
		if ($fragment instanceof Query\InterfaceSQLFragment)
		{
			return $fragment;
		}
		return null;
	}

	/**
	 * @param \Change\Db\Query\SelectQuery $selectQuery
	 * @return array
	 */
	public abstract function getQueryResultsArray(\Change\Db\Query\SelectQuery $selectQuery);

	/**
	 * @param AbstractQuery $query
	 * @return integer
	 */
	public abstract function executeQuery(AbstractQuery $query);

	/**
	 * @param mixed $value
	 * @param integer $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	public abstract function phpToDB($value, $scalarType);

	/**
	 * @param mixed $value
	 * @param integer $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	public abstract function dbToPhp($value, $scalarType);
}