<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Schema;

/**
 * @name \Change\Db\Schema\TableDefinition
 */
class TableDefinition
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var \Change\Db\Schema\FieldDefinition[]
	 */
	protected $fields = [];

	/**
	 * @var \Change\Db\Schema\KeyDefinition[]
	 */
	protected $keys = [];

	/**
	 * @var array
	 */
	protected $options = [];

	public function __construct($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return \Change\Db\Schema\FieldDefinition[]
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * @return \Change\Db\Schema\KeyDefinition[]
	 */
	public function getKeys()
	{
		return $this->keys;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param \Change\Db\Schema\FieldDefinition[] $fields
	 */
	public function setFields($fields)
	{
		$this->fields = [];
		foreach ($fields as $field)
		{
			if ($field instanceof FieldDefinition)
			{
				$this->fields[$field->getName()] = $field;
			}
		}
	}

	/**
	 * @param \Change\Db\Schema\KeyDefinition[] $keys
	 */
	public function setKeys($keys)
	{
		$this->keys = [];
		foreach ($keys as $key)
		{
			if ($key instanceof KeyDefinition)
			{
				$this->keys[] = $key;
			}
		}
	}

	/**
	 * @param \Change\Db\Schema\FieldDefinition $field
	 * @return \Change\Db\Schema\TableDefinition
	 */
	public function addField(\Change\Db\Schema\FieldDefinition $field)
	{
		$this->fields[$field->getName()] = $field;
		return $this;
	}

	/**
	 * @param \Change\Db\Schema\KeyDefinition $key
	 * @return \Change\Db\Schema\TableDefinition
	 */
	public function addKey(\Change\Db\Schema\KeyDefinition $key)
	{
		$this->keys[] = $key;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isValid()
	{
		return $this->name !== null && count($this->fields) > 0;
	}

	/**
	 * @param string $name
	 * @return \Change\Db\Schema\FieldDefinition|NULL
	 */
	public function getField($name)
	{
		return $this->fields[$name] ?? null;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 * @return $this
	 */
	public function setOptions($options)
	{
		$this->options = is_array($options) ? $options : [];
		return $this;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function setOption($name, $value)
	{
		if ($value === null)
		{
			unset($this->options[$name]);
		}
		else
		{
			$this->options[$name] = $value;
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getOption($name)
	{
		return $this->options[$name] ?? null;
	}

	/**
	 * @param string $charset
	 * @return $this
	 */
	public function setCharset($charset)
	{
		$this->setOption('CHARSET', $charset);
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCharset()
	{
		return $this->getOption('CHARSET');
	}

	/**
	 * @param string $collation
	 * @return $this
	 */
	public function setCollation($collation)
	{
		$this->setOption('COLLATION', $collation);
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCollation()
	{
		return $this->getOption('COLLATION');
	}
}