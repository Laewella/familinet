<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Events;

/**
 * @name \Change\Events\Event
 */
class Event extends \Zend\EventManager\Event
{
	/**
	 * @return \Change\Application|null
	 */
	public function getApplication()
	{
		return $this->getParam('application');
	}

	/**
	 * @return \Change\Services\ApplicationServices|null
	 */
	public function getApplicationServices()
	{
		return $this->getServices('applicationServices');
	}

	/**
	 * @param string $serviceName
	 * @return \Zend\Stdlib\Parameters|\Zend\ServiceManager\ServiceManager|null
	 */
	public function getServices($serviceName = null)
	{
		$services = $this->getParam('services');
		if ($services instanceof \Zend\Stdlib\Parameters)
		{
			return $serviceName ? $services->get($serviceName) : $services;
		}
		return null;
	}

	/**
	 * @return array
	 */
	public function paramsToArray()
	{
		$array = [];
		foreach ($this->getParams() as $name => $value)
		{
			if ($name !== 'services' && $name !== 'application')
			{
				$array[$name] = $value;
			}
		}
		return $array;
	}
} 