<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Events;

/**
 * @name \Change\Events\EventManager
 */
class EventManager extends \Zend\EventManager\EventManager
{
	public function clearAllListeners() 
	{
		if ($this->events)
		{
			$names = array_keys($this->events);
			foreach ($names as $name)
			{
				$this->clearListeners($name);
			}
		}
	}
}