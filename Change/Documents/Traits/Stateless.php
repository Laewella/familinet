<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Traits;

/**
 * @name \Change\Documents\Traits\Stateless
 *
 * From \Change\Documents\AbstractDocument
 * @method integer getPersistentState()
 * @method integer setPersistentState($newValue)
 * @method \Change\Documents\DocumentManager getDocumentManager()
 * @method \Change\Documents\AbstractModel getDocumentModel()
 * @method \Change\Events\EventManager getEventManager()
 * @method string[] getModifiedPropertyNames()
 */
trait Stateless
{
	/**
	 * Load properties
	 * @api
	 */
	public function load()
	{
		if ($this->getPersistentState() === \Change\Documents\AbstractDocument::STATE_INITIALIZED)
		{
			$this->doLoad();
			$this->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);
			$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_LOADED, $this);
			$this->getEventManager()->triggerEvent($event);
		}
	}

	/**
	 * @throws \Exception
	 * @return void
	 */
	abstract protected function doLoad();

	/**
	 * Call create() or update()
	 * @api
	 */
	public function save()
	{
		if ($this->getPersistentState() === \Change\Documents\AbstractDocument::STATE_NEW)
		{
			$this->create();
		}
		else
		{
			$this->update();
		}
	}

	/**
	 * @api
	 */
	public function create()
	{
		if ($this->getPersistentState() !== \Change\Documents\AbstractDocument::STATE_NEW)
		{
			throw new \RuntimeException('Document is not new', 51001);
		}

		$callable = [$this, 'onCreate'];
		if (is_callable($callable))
		{
			call_user_func($callable);
		}
		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_CREATE, $this);
		$this->getEventManager()->triggerEvent($event);

		$propertiesErrors = $event->getParam('propertiesErrors');
		if (is_array($propertiesErrors) && count($propertiesErrors))
		{
			$e = new \Change\Documents\PropertiesValidationException('Invalid document properties.', 52000);
			$e->setPropertiesErrors($propertiesErrors);
			throw $e;
		}

		$this->doCreate();

		$this->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);
		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_CREATED, $this);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @throws \Exception
	 * @return void
	 */
	abstract protected function doCreate();

	/**
	 * @api
	 */
	public function update()
	{
		if ($this->getPersistentState() === \Change\Documents\AbstractDocument::STATE_NEW)
		{
			throw new \RuntimeException('Document is new', 51002);
		}

		$callable = [$this, 'onUpdate'];
		if (is_callable($callable))
		{
			call_user_func($callable);
		}

		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_UPDATE, $this);
		$this->getEventManager()->triggerEvent($event);

		$propertiesErrors = $event->getParam('propertiesErrors');
		if (is_array($propertiesErrors) && count($propertiesErrors))
		{
			$e = new \Change\Documents\PropertiesValidationException('Invalid document properties.', 52000);
			$e->setPropertiesErrors($propertiesErrors);
			throw $e;
		}

		$modifiedPropertyNames = $this->getModifiedPropertyNames();

		$this->doUpdate($modifiedPropertyNames);
		$this->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);

		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_UPDATED, $this,
			['modifiedPropertyNames' => $modifiedPropertyNames]);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @param string[] $modifiedPropertyNames
	 * @return void
	 */
	abstract protected function doUpdate($modifiedPropertyNames);

	/**
	 * @api
	 */
	public function delete()
	{
		//Already deleted
		if ($this->getPersistentState() === \Change\Documents\AbstractDocument::STATE_DELETED
			|| $this->getPersistentState() === \Change\Documents\AbstractDocument::STATE_DELETING
		)
		{
			return;
		}

		if ($this->getPersistentState() === \Change\Documents\AbstractDocument::STATE_NEW)
		{
			throw new \RuntimeException('Document is new', 51002);
		}

		$callable = [$this, 'onDelete'];
		if (is_callable($callable))
		{
			call_user_func($callable);
		}
		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_DELETE, $this);
		$this->getEventManager()->triggerEvent($event);

		$this->doDelete();
		$this->setPersistentState(\Change\Documents\AbstractDocument::STATE_DELETED);

		$event->setName(\Change\Documents\Events\Event::EVENT_DELETED);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @throws \Exception
	 */
	abstract protected function doDelete();
}