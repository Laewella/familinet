<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents;


/**
 * @name \Change\Documents\Serializer
 */
class Serializer
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager)
	{
		$this->documentManager = $documentManager;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return string|null
	 */
	public function serialize(\Change\Documents\AbstractDocument $document)
	{
		$data = [];
		$model = $document->getDocumentModel();
		foreach ($model->getProperties() as $n => $p)
		{
			if ($p->getLocalized() || $p->getStateless())
			{
				continue;
			}
			$v = $p->getValue($document);
			if ($v instanceof \Change\Documents\DocumentArrayProperty)
			{
				$v = $v->getIds();
			}
			elseif ($v instanceof \Change\Documents\RichtextProperty)
			{
				$v = $v->toArray();
			}
			elseif ($v instanceof \Change\Documents\AbstractDocument)
			{
				$v = $v->getId();
			}
			elseif ($v instanceof \Change\Documents\AbstractInline)
			{
				$v = serialize($v->dbData());
			}
			elseif ($v instanceof \Change\Documents\InlineArrayProperty)
			{
				$v = serialize($v->dbData());
			}
			elseif ($v instanceof \DateTime)
			{
				$v = $v->format(\DateTime::ATOM);
			}
			elseif (is_array($v))
			{
				if ($p->getType() === \Change\Documents\Property::TYPE_OBJECT)
				{
					$v = serialize($v);
				}
				else
				{
					$v = json_encode($v);
				}
			}
			$data[$n] = $v;
		}

		if ($document instanceof \Change\Documents\Interfaces\Localizable)
		{
			$data['__LCID'] = [];
			foreach ($document->getLCIDArray() as $LCID)
			{
				$i18nData = [];
				$this->documentManager->pushLCID($LCID);
				$localizedDoc = $document->getCurrentLocalization();
				foreach ($model->getProperties() as $n => $p)
				{
					if ($n === 'LCID' || !$p->getLocalized() || $p->getStateless())
					{
						continue;
					}
					$v = $p->getLocalizedValue($localizedDoc);
					if ($v instanceof \Change\Documents\RichtextProperty)
					{
						$v = $v->toArray();
					}
					elseif (is_array($v))
					{
						if ($p->getType() === \Change\Documents\Property::TYPE_OBJECT)
						{
							$v = serialize($v);
						}
						else
						{
							$v = json_encode($v);
						}
					}
					$i18nData[$n] = $v;
				}
				$data['__LCID'][$LCID] = $i18nData;
				$this->documentManager->popLCID();
			}
		}
		$data['__attributesValues'] = $document->getCachedAttributesValue();
		$data['__typologyId'] = $document->getCachedTypologyId();
		$data['__pathRules'] = $document->getCachedPathRules();
		return $data ? serialize($data) : null;
	}

	/**
	 * @param $string
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function unserialize($string)
	{
		if (!$string || !is_string($string))
		{
			return null;
		}

		$data = unserialize($string);
		$document = null;
		if (isset($data['id'], $data['model']))
		{
			$model = $this->documentManager->getModelManager()->getModelByName($data['model']);
			if (!$model)
			{
				return $document;
			}
			$document = $this->documentManager->getNewDocumentInstanceByModel($model, false);
			$document->initialize((int)$data['id'], \Change\Documents\AbstractDocument::STATE_LOADING);

			$document->setCachedAttributesValue($data['__attributesValues']);
			$document->setCachedTypologyId($data['__typologyId']);
			$document->setCachedPathRules($data['__pathRules']);

			foreach ($model->getProperties() as $n => $p)
			{
				if ($n === 'id' || $n === 'model' || !array_key_exists($n, $data))
				{
					continue;
				}
				$v = $data[$n];
				if ($p->getType() === \Change\Documents\Property::TYPE_DOCUMENTARRAY)
				{
					$sp = new \Change\Documents\DocumentArrayProperty($this->documentManager, $p->getDocumentType());
					if ($v && is_array($v))
					{
						$sp->fromIds($v);
					}
					$p->setValue($document, $sp);
				}
				else
				{
					$p->setValue($document, $v);
				}
			}
			$document->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);

			if ($document instanceof \Change\Documents\Interfaces\Localizable)
			{
				foreach ($data['__LCID'] as $LCID => $i18nData)
				{
					$this->documentManager->pushLCID($LCID);
					$localizedDoc = $document->getNewLocalizationByLCID($LCID);
					$localizedDoc->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADING);
					foreach ($model->getProperties() as $n => $p)
					{
						if ($n === 'LCID' || !array_key_exists($n, $i18nData))
						{
							continue;
						}
						$v = $i18nData[$n];
						$p->setLocalizedValue($localizedDoc, $v);
					}
					$localizedDoc->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);
					$this->documentManager->popLCID();
				}
				$document->setLCIDArray(array_keys($data['__LCID']));
			}
		}
		return $document;
	}

}