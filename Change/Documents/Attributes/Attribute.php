<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes;

/**
 * @name \Change\Documents\Attributes\Attribute
 */
class Attribute implements \Change\Documents\Attributes\Interfaces\Attribute
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var \Change\Documents\RichtextProperty
	 */
	protected $description;

	/**
	 * @var string
	 */
	protected $renderingMode;

	/**
	 * @var boolean
	 */
	protected $localized;

	/**
	 * @param array|null $attribute
	 */
	public function __construct($attribute = null)
	{
		if (is_array($attribute))
		{
			$this->fromArray($attribute);
		}
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return $this
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return \Change\Documents\RichtextProperty
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param \Change\Documents\RichtextProperty|array|string|null $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->description = new \Change\Documents\RichtextProperty($description);
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getLocalized()
	{
		return $this->localized;
	}

	/**
	 * @return string|null
	 */
	public function getRenderingMode()
	{
		return $this->renderingMode;
	}

	/**
	 * @@param string|null $renderingMode
	 * @return $this
	 */
	public function setRenderingMode($renderingMode)
	{
		$this->renderingMode = $renderingMode;
		return $this;
	}

	/**
	 * @param boolean $localized
	 * @return $this
	 */
	public function setLocalized($localized)
	{
		$this->localized = $localized;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getAJAXFormatter()
	{
		return null;
	}

	/**
	 * @return null|string
	 */
	public function getFilterType()
	{
		switch ($this->getType())
		{
			case self::TYPE_INTEGER:
			case self::TYPE_BOOLEAN:
			case self::TYPE_DOCUMENT_ID:
			case self::TYPE_DOCUMENT_ID_ARRAY:
				return 'integer';

			case self::TYPE_FLOAT:
				return 'float';

			case self::TYPE_DATETIME:
				return 'date';

			case self::TYPE_STRING:
				return 'string';
		}
		return null;
	}

	/**
	 * @param array $attribute
	 */
	protected function fromArray(array $attribute)
	{
		$this->name = isset($attribute['name']) ? $attribute['name'] : null;
		$this->type = isset($attribute['type']) ? $attribute['type'] : null;
		$this->title = isset($attribute['title']) ? $attribute['title'] : null;
		$this->setDescription(isset($attribute['description']) ? $attribute['description'] : null);
		$this->renderingMode = isset($attribute['renderingMode']) ? $attribute['renderingMode'] : null;
		$this->localized = isset($attribute['localized']) ? $attribute['localized'] : null;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return [
			'name' => $this->getName(),
			'type' => $this->getType(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription()->toArray(),
			'renderingMode' => $this->getRenderingMode(),
			'localized' => $this->getLocalized(),
		];
	}
}