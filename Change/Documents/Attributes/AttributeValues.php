<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes;

/**
 * @name \Change\Documents\Attributes\AttributeValues
 */
class AttributeValues
{
	/**
	 * @var string
	 */
	protected $LCID;

	/**
	 * @var array
	 */
	protected $values = [];

	/**
	 * @param string $LCID
	 * @param array $values
	 */
	public function __construct($LCID, array $values = [])
	{
		$this->LCID = $LCID;
		$this->values = $values;
	}

	/**
	 * @return string
	 */
	public function getLCID()
	{
		return $this->LCID;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function get($name)
	{
		if (isset($this->values[$name]))
		{
			return $this->values[$name];
		}
		return null;
	}

	/**
	 * @param string $name
	 * @return boolean
	 */
	public function has($name) 
	{
		return array_key_exists($name, $this->values);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function set($name, $value)
	{
		$this->values[$name] = $value;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getValues()
	{
		return $this->values;
	}
}