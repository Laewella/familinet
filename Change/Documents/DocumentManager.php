<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents;

use Change\Db\Query\ResultsConverter;
use Change\Db\ScalarType;

/**
 * @name \Change\Documents\DocumentManager
 * @api
 */
class DocumentManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'Documents';

	/**
	 * @var integer
	 */
	protected $cacheSize = 5000;

	/**
	 * @var integer
	 */
	protected $cycleCount = 0;

	/**
	 * Temporary identifier for new persistent document
	 * @var integer
	 */
	protected $newInstancesCounter = 0;

	/**
	 * @var string[] ex: "en_US" or "fr_FR"
	 */
	protected $LCIDStack = [];

	/**
	 * @var boolean
	 */
	protected $inTransaction = false;

	/**
	 * @var array
	 */
	protected $LCIDStackTransaction = [];

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var array
	 */
	protected $typologyIds = [];

	/**
	 * @var boolean
	 */
	protected $persistentCache = false;

	/**
	 * @var AbstractDocument[]
	 */
	protected $documentInstances = [];

	/**
	 * @var integer[]
	 */
	protected $modifiedInstanceIds = [];

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/DocumentManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('injection', [$this, 'onDefaultInjection'], 5);

		$eventManager->attach('getFromPersistentCache', function (\Change\Events\Event $event) { $this->onGetFromPersistentCache($event); }, 5);
		$eventManager->attach('addToPersistentCache', function (\Change\Events\Event $event) { $this->onAddToPersistentCache($event); }, 5);

		$eventManager->attach('preLoad', function (\Change\Events\Event $event) { $this->onPreLoad($event); }, 5);

		$eventManager->attach('getDisplayableDocument', function (\Change\Events\Event $event) { $this->onGetDisplayableDocument($event); }, 5);

		$eventManager->attach('getTypology', function (\Change\Events\Event $event) { $this->onGetTypologyFromCache($event); }, 50);
		$eventManager->attach('getTypology', function (\Change\Events\Event $event) { $this->onSetTypologyInCache($event); }, -50);

		$eventManager->attach('getAttributeValues', function (\Change\Events\Event $event) { $this->onGetAttributeValuesFromCache($event); }, 50);
		$eventManager->attach('getAttributeValues', function (\Change\Events\Event $event) { $this->onSetAttributeValuesInCache($event); }, -50);

		$sharedManager = $eventManager->getSharedManager();
		if (!isset($sharedManager->documentEventListeners))
		{
			$sharedManager->documentEventListeners = true;

			$cb = function (\Change\Documents\Events\Event $event) { $this->onDocumentModified($event); };
			foreach (['documents.created', 'documents.localized.created', 'documents.updated',
				'documents.deleted', 'documents.localized.deleted', 'documents.attributes.changed', 'documents.attributes.deleted'] as $name)
			{
				$sharedManager->attach('Documents', $name, $cb, 5);
			}
			$classNames = $this->getApplication()->getConfiguredListenerClassNames('Change/Events/Documents');

			foreach ($classNames as $className)
			{
				if (is_string($className) && class_exists($className))
				{
					$listenerAggregate = new $className();
					if (is_callable([$listenerAggregate, 'attachShared']))
					{
						$listenerAggregate->{'attachShared'}($sharedManager);
					}
				}
				else
				{
					$this->getLogging()->error($className . ' Shared Listener aggregate Class name not found.');
				}
			}
		}
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	protected function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider(\Change\Db\DbProvider $dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @return \Change\Db\DbProvider
	 */
	protected function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager(\Change\I18n\I18nManager $i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager(\Change\Documents\ModelManager $modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @api
	 * @return \Change\Documents\ModelManager
	 */
	public function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function beginTransaction(\Change\Events\Event $event)
	{
		if ($event->getParam('primary'))
		{
			$this->inTransaction = true;
			$this->persistentCache = false;
		}
		$count = $event->getParam('count');
		$this->LCIDStackTransaction[$count] = $this->LCIDStack;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function commit(\Change\Events\Event $event)
	{
		if ($event->getParam('primary'))
		{
			$this->inTransaction = false;
			if ($this->modifiedInstanceIds)
			{
				$cacheManager = $event->getApplicationServices()->getCacheManager();
				if ($cacheManager->isValidNamespace('prefetch'))
				{
					foreach ($this->modifiedInstanceIds as $id)
					{
						$cacheManager->removeEntry('prefetch', (string)$id);
					}
				}
				$this->modifiedInstanceIds = [];
			}
			$this->reset();
		}
		$count = $event->getParam('count');
		unset($this->LCIDStackTransaction[$count]);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function rollBack(\Change\Events\Event $event)
	{
		$count = $event->getParam('count');
		if (isset($this->LCIDStackTransaction[$count]))
		{
			$this->LCIDStack = $this->LCIDStackTransaction[$count];
		}
		if ($event->getParam('primary'))
		{
			$this->LCIDStackTransaction = [];
			$this->inTransaction = false;
			$this->reset();
		}
	}

	/**
	 * @api
	 * @return bool
	 */
	public function inTransaction()
	{
		return $this->inTransaction;
	}

	public function shutdown()
	{
		if ($this->eventManager)
		{
			$this->reset();
		}
	}

	/**
	 * @api
	 * Cleanup all documents instance
	 */
	public function reset()
	{
		$this->typologyIds = [];
		$this->newInstancesCounter = 0;
		$this->cycleCount = 0;

		array_map(function (\Change\Documents\AbstractDocument $document) { $document->cleanUp(); }, $this->documentInstances);
		$this->documentInstances = [];
	}

	/**
	 * @param string $cacheKey
	 * @return \Change\Db\Query\Builder
	 */
	protected function getNewQueryBuilder($cacheKey = null)
	{
		return $this->getDbProvider()->getNewQueryBuilder($cacheKey);
	}

	/**
	 * @param string $cacheKey
	 * @return \Change\Db\Query\StatementBuilder
	 */
	protected function getNewStatementBuilder($cacheKey = null)
	{
		return $this->getDbProvider()->getNewStatementBuilder($cacheKey);
	}

	/**
	 * @api
	 * @param string $modelName
	 * @param boolean $defaultValue
	 * @throws \InvalidArgumentException
	 * @return \Change\Documents\AbstractDocument
	 */
	public function getNewDocumentInstanceByModelName($modelName, $defaultValue = true)
	{
		$model = $this->getModelManager()->getModelByName($modelName);
		if ($model === null)
		{
			throw new \InvalidArgumentException('Invalid model name (' . $modelName . ')', 50002);
		}
		return $this->getNewDocumentInstanceByModel($model, $defaultValue);
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractModel $model
	 * @param boolean $defaultValue
	 * @return \Change\Documents\AbstractDocument
	 */
	public function getNewDocumentInstanceByModel(\Change\Documents\AbstractModel $model, $defaultValue = true)
	{
		$newDocument = $this->createNewDocumentInstance($model);
		$this->newInstancesCounter--;
		$newDocument->initialize($this->newInstancesCounter, AbstractDocument::STATE_NEW);
		if ($defaultValue)
		{
			$newDocument->setDefaultValues($model);
		}
		return $newDocument;
	}

	/**
	 * @param \Change\Documents\AbstractModel $model
	 * @throws \RuntimeException
	 * @return \Change\Documents\AbstractDocument
	 */
	protected function createNewDocumentInstance(\Change\Documents\AbstractModel $model)
	{
		if ($model->isAbstract() || $model->isInline())
		{
			throw new \RuntimeException('Unable to create document instance of model: ' . $model, 999999);
		}
		$className = $model->getDocumentClassName();
		if (!class_exists($className))
		{
			throw new \RuntimeException('Class could not be loaded ' . $className, 999999);
		}

		/* @var $document \Change\Documents\AbstractDocument */
		$document = new $className($model);
		$document->setApplication($this->getApplication())
			->setDocumentManager($this)
			->setDbProvider($this->dbProvider);
		$this->getEventManager()->trigger('injection', $this, ['document' => $document]);
		return $document;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultInjection(\Change\Events\Event $event)
	{
		$document = $event->getParam('document');
		if ($document instanceof AbstractDocument)
		{
			$document->onDefaultInjection($event);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetDisplayableDocument(\Change\Events\Event $event)
	{
		$documentId = $event->getParam('documentId');
		$document = $this->getDocumentInstance($documentId);
		if ($document instanceof AbstractDocument)
		{

			if ($document instanceof Interfaces\Publishable)
			{
				if ($document->published())
				{
					/** @var \Change\Http\Web\Event $httpEvent */
					$httpEvent = $event->getParam('httpEvent');
					$website = $httpEvent->getWebsite();
					if ($document->getCanonicalSection($website))
					{
						$event->setParam('displayableDocument', $document);
					}
				}
			}
			else
			{
				$event->setParam('displayableDocument', $document);
			}
		}
	}

	/**
	 * @api
	 * @param string $modelName
	 * @param boolean $initializeDefault
	 * @throws \InvalidArgumentException
	 * @return \Change\Documents\AbstractInline
	 */
	public function getNewInlineInstanceByModelName($modelName, $initializeDefault = true)
	{
		$model = $this->getModelManager()->getModelByName($modelName);
		if ($model === null)
		{
			throw new \InvalidArgumentException('Invalid model name (' . $modelName . ')', 50002);
		}
		return $this->getNewInlineInstanceByModel($model, $initializeDefault);
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractModel $model
	 * @param boolean $initializeDefault
	 * @return \Change\Documents\AbstractInline
	 */
	public function getNewInlineInstanceByModel(AbstractModel $model, $initializeDefault = true)
	{
		$newDocument = $this->createNewInlineInstance($model);
		if ($initializeDefault)
		{
			$newDocument->setDefaultValues();
		}
		return $newDocument;
	}

	/**
	 * @param \Change\Documents\AbstractModel $model
	 * @throws \RuntimeException
	 * @return \Change\Documents\AbstractInline
	 */
	protected function createNewInlineInstance(\Change\Documents\AbstractModel $model)
	{
		if ($model->isAbstract() || !$model->isInline())
		{
			throw new \RuntimeException('Unable to create inline instance of model: ' . $model->getName(), 999999);
		}
		$className = $model->getDocumentClassName();
		if (!class_exists($className))
		{
			throw new \RuntimeException('Class could not be loaded ' . $className, 999999);
		}

		/* @var $inlineDocument AbstractInline */
		$inlineDocument = new $className($model);
		$inlineDocument->setApplication($this->getApplication())->setDocumentManager($this);
		return $inlineDocument;
	}

	/**
	 * @api
	 * @param integer $documentId
	 * @param AbstractModel|string $model
	 * @return AbstractDocument|null
	 */
	public function getDocumentInstance($documentId, $model = null)
	{
		$id = (int)$documentId;
		if ($id <= 0)
		{
			return null;
		}

		if (is_string($model))
		{
			$modelName = $model;
			$model = $this->getModelManager()->getModelByName($modelName);
			if ($model === null)
			{
				$this->getLogging()->warn(__METHOD__ . ' Invalid document model name: ' . $modelName);
				return null;
			}
		}
		elseif ($model && !($model instanceof AbstractModel))
		{
			$this->getLogging()->warn(__METHOD__ . ' Invalid document model' . $model);
			return null;
		}

		$document = $this->getFromCache($id);
		if (!$document && $this->usePersistentCache())
		{
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['id' => (int)$id, 'document' => null]);
			$eventManager->trigger('getFromPersistentCache', $this, $args);
			$document = $args['document'];
		}

		if ($document !== null)
		{
			if ($document && $model && $document->getDocumentModelName() !== $model->getName()
				&& !in_array($model->getName(), $document->getDocumentModel()->getAncestorsNames())
			)
			{
				$this->getLogging()->warn(
					__METHOD__ . ' Invalid document model name: ' . $document->getDocumentModelName() . ', '
					. $model->getName() . ' Expected');
				return null;
			}
			return $document;
		}

		$this->gcCache();

		if ($model)
		{
			if ($model->isAbstract())
			{
				return null;
			}
			elseif ($model->isStateless())
			{
				$document = $this->createNewDocumentInstance($model);
				$document->initialize($id, AbstractDocument::STATE_INITIALIZED);
				$document->load();
				return $document;
			}
		}

		$qb = $this->getNewQueryBuilder(__METHOD__ . ($model ? $model->getRootName() : 'std'));
		if (!$qb->isCached())
		{

			$fb = $qb->getFragmentBuilder();
			if ($model)
			{
				$qb->select($fb->alias($fb->getDocumentColumn('model'), 'model'))
					->from($fb->getDocumentTable($model->getRootName()))
					->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
			}
			else
			{
				$qb->select($fb->alias($fb->getDocumentColumn('model'), 'model'))
					->from($fb->getDocumentIndexTable())
					->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
			}
		}

		$query = $qb->query();
		$query->bindParameter('id', $id);

		$constructorInfos = $query->getFirstResult();
		if ($constructorInfos)
		{
			$modelName = $constructorInfos['model'];
			$documentModel = $this->getModelManager()->getModelByName($modelName);
			if ($documentModel !== null && !$documentModel->isAbstract())
			{
				$document = $this->createNewDocumentInstance($documentModel);
				$document->initialize($id, AbstractDocument::STATE_INITIALIZED);
				if ($this->usePersistentCache())
				{
					$this->getEventManager()->trigger('addToPersistentCache', $this, ['document' => $document]);
				}
				return $document;
			}
			else
			{
				$this->getLogging()->error(__METHOD__ . ' Invalid model name: ' . $modelName);
			}
		}
		else
		{
			$this->getLogging()->info('Document id ' . $id . ' not found');
		}

		return null;
	}

	/**
	 * @param array $ids Format [[ID, MODEL_NAME|null], ...]
	 */
	public function preLoad(array $ids)
	{
		$this->getEventManager()->trigger('preLoad', $this, ['ids' => $ids]);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onPreLoad(\Change\Events\Event $event)
	{
		/** @var array $ids */
		$ids = $event->getParam('ids');
		if (!$ids)
		{
			return;
		}

		$useDataCache = $this->usePersistentCache();
		$cacheManager = null;
		$cacheOptions = ['ttl' => 36000];
		$s = null;

		$idsByModel = [];
		$noModelIds = [];

		if ($useDataCache)
		{

			$s = new Serializer($this);
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$keys = [];
			foreach ($ids as list($id, $modelName))
			{
				if (!$this->getFromCache($id))
				{
					$keys[(string)$id] = $modelName;
				}
			}

			if ($keys)
			{
				$values = $cacheManager->getEntries('prefetch', array_keys($keys), $cacheOptions);
				foreach ($values as $key => $str)
				{
					if ($str)
					{
						$loadedDoc = $s->unserialize($str);
						if ($loadedDoc)
						{
							$keys[$key] = false;
							$loadedDoc->getEventManager()->trigger('documents.preLoaded', $loadedDoc);
						}
					}
				}

				foreach ($keys as $id => $modelName)
				{
					if ($modelName === false)
					{
						continue;
					}

					if ($modelName)
					{
						$idsByModel[$modelName][] = (int)$id;
					}
					else
					{
						$noModelIds[] = $id;
					}
				}
			}
		}
		else
		{
			foreach ($ids as list($id, $modelName))
			{
				if ($modelName)
				{
					$idsByModel[$modelName][] = (int)$id;
				}
				else
				{
					$noModelIds[] = $id;
				}
			}
		}

		if ($noModelIds)
		{
			foreach ($this->getModelNames($noModelIds) as $row)
			{
				$idsByModel[$row['model']][] = (int)$row['id'];
			}
		}

		/** @var AbstractDocument[] $documents */
		$documents = [];
		foreach ($idsByModel as $modelName => $docIds)
		{
			$listIds = array_keys(array_count_values($docIds));
			$model = $this->getModelManager()->getModelByName($modelName);
			if ($model && $listIds)
			{
				foreach ($this->preLoadIdsByModel($listIds, $model) as $document)
				{
					$documents[$document->getId()] = $document;
				}
			}
		}

		if ($documents)
		{
			if (count($documents) > 1)
			{
				$this->preLoadCached($documents);
			}

			if ($useDataCache)
			{
				foreach ($documents as $doc)
				{
					$str = $s->serialize($doc);
					$cacheManager->setEntry('prefetch', (string)$doc->getId(), $str, $cacheOptions);
				}
			}
		}
	}

	/**
	 * @param AbstractDocument[] $documents Indexed by Id
	 */
	protected function preLoadCached($documents)
	{
		$dbProvider = $this->getDbProvider();

		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$idsExp = array_map(function ($id) use ($fb) { return $fb->number($id); }, array_keys($documents));

		$qb->select($fb->column('document_id'), $fb->column('typology_id'), $fb->column('data'))
			->from($fb->getDocumentAttributesTable())
			->where($fb->in($fb->column('document_id'), $idsExp));

		$sq = $qb->query();
		$attributes = $sq->getResults($sq->getRowsConverter()->addIntCol('document_id', 'typology_id')->addTxtCol('data')->indexBy('document_id'));

		//Documents rules
		$rules = [];
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('relative_path'), $fb->column('query'),
			$fb->column('website_id'), $fb->column('lcid'), $fb->column('section_id'), $fb->column('document_id'));
		$qb->from($qb->getSqlMapping()->getPathRuleTable());
		$qb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('http_status'), $fb->number(200)),
				$fb->in($fb->column('document_id'), $idsExp)
			)
		);
		$sq = $qb->query();
		$sq->getFromClause()->setOptions([['FORCE INDEX', 'CACHE_DOC']]);
		foreach ($sq->getResults($sq->getRowsConverter()
			->addIntCol('document_id', 'website_id', 'section_id')->addTxtCol('relative_path', 'lcid', 'query')) as $row)
		{
			if ($row['query'])
			{
				continue;
			}
			$id = $row['document_id'];
			$wsk = $row['website_id'] . $row['lcid'];
			$rules[$id][$wsk][$row['section_id']] = $row['relative_path'];
		}

		//Documents alias rules
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('relative_path'), $fb->column('query'),
			$fb->column('website_id'), $fb->column('lcid'), $fb->column('section_id'), $fb->column('document_alias_id'));
		$qb->from($qb->getSqlMapping()->getPathRuleTable());
		$qb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('http_status'), $fb->number(200)),
				$fb->in($fb->column('document_alias_id'), $idsExp)
			)
		);
		$sq = $qb->query();
		$sq->getFromClause()->setOptions([['FORCE INDEX', 'CACHE_DOC1']]);
		foreach ($sq->getResults($sq->getRowsConverter()
			->addIntCol('document_alias_id', 'website_id', 'section_id')->addTxtCol('relative_path', 'lcid', 'query')) as $row)
		{
			if ($row['query'])
			{
				continue;
			}
			$id = $row['document_alias_id'];
			$wsk = $row['website_id'] . $row['lcid'];
			$rules[$id][$wsk][0] = $row['relative_path'];
		}

		foreach ($documents as $id => $document)
		{
			$a = $attributes[$id] ?? ['typology_id' => false, 'data' => false];
			$document->setCachedTypologyId($a['typology_id'] ?: 0);
			$document->setCachedAttributesValue($a['data'] ? json_decode($a['data'], true) : []);

			$r = $rules[$id] ?? [];
			$document->setCachedPathRules($r);
		}
	}

	/**
	 * @param integer[] $listIds
	 * @return array[]
	 */
	protected function getModelNames($listIds)
	{
		$dbProvider = $this->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->alias($fb->getDocumentColumn('id'), 'id'), $fb->alias($fb->getDocumentColumn('model'), 'model'))
			->from($fb->getDocumentIndexTable())
			->where($fb->in($fb->getDocumentColumn('id'), array_map(function ($id) use ($fb) { return $fb->number($id); }, $listIds)));
		return $qb->query()->getResults();
	}

	/**
	 * @param integer[] $listIds
	 * @param AbstractModel $model
	 * @return AbstractDocument[]
	 */
	protected function preLoadIdsByModel($listIds, $model)
	{
		$documents = [];
		$dbProvider = $this->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$idsExp = array_map(function ($id) use ($fb) { return $fb->number($id); }, $listIds);
		$qb->select()
			->from($fb->alias($fb->getDocumentTable($model->getRootName()), 'd'))
			->where($fb->in($fb->getDocumentColumn('id', 'd'), $idsExp));
		$LCID = $model->isLocalized() ? $this->getLCID() : false;
		if ($LCID)
		{
			$qb->innerJoin($fb->alias($fb->getDocumentI18nTable($model->getRootName()), 'i'),
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('id', 'd'), $fb->getDocumentColumn('id', 'i')),
					$fb->eq($fb->getDocumentColumn('LCID', 'i'), $fb->string($LCID))
				)
			);
		}
		$relations = [];
		foreach ($model->getProperties() as $name => $property)
		{
			/* @var $property \Change\Documents\Property */
			if ($property->getStateless())
			{
				continue;
			}
			if ($property->getType() === Property::TYPE_DOCUMENTARRAY)
			{
				$relations[$name] = [];
			}
			$qb->addColumn($fb->alias($fb->getDocumentColumn($name, $property->getLocalized() ? 'i' : 'd'), $name));
		}

		if ($relations)
		{
			$rqb = $dbProvider->getNewQueryBuilder();
			$rfb = $rqb->getFragmentBuilder();
			$rqb->select(
				$rfb->alias($rfb->getDocumentColumn('id'), 'id'),
				$rfb->alias($rfb->column('relatedid'), 'relId'),
				$rfb->alias($rfb->column('relname'), 'rel'))
				->from($rfb->getDocumentRelationTable($model->getRootName()))
				->where($rfb->in($rfb->getDocumentColumn('id'), $idsExp))
				->orderAsc($rfb->column('relorder'));
			foreach ($rqb->query()->getResults() as $row)
			{
				$relations[$row['rel']][(int)$row['id']][] = (int)$row['relId'];
			}
		}

		$select = $qb->query();
		$modelManager = $this->getModelManager();
		$sqlMapping = $fb->getSqlMapping();
		foreach ($select->getResults() as $dbData)
		{
			$id = (int)$dbData['id'];

			/** @var AbstractDocument|\Change\Documents\Interfaces\Localizable $document */
			$document = $this->getFromCache($id);

			/** @var \Change\Documents\AbstractLocalizedDocument $localisation */
			$localisation = null;
			if ($document === null)
			{
				$document = $this->createNewDocumentInstance($modelManager->getModelByName((string)$dbData['model']));
				$document->initialize($id, AbstractDocument::STATE_LOADING);
			}
			else
			{
				$document->setPersistentState(AbstractDocument::STATE_LOADING);
			}

			if ($LCID)
			{
				$localisation = $document->getNewLocalizationByLCID($LCID);
				$localisation->setPersistentState(AbstractDocument::STATE_LOADING);
			}
			foreach ($model->getProperties() as $name => $property)
			{
				if ($property->getStateless())
				{
					continue;
				}
				$type = $property->getType();
				$setter = 'set' . ucfirst($name);
				if ($type === Property::TYPE_DOCUMENTARRAY)
				{
					$value = new DocumentArrayProperty($this, $property->getDocumentType());
					$ids = $relations[$name][$id] ?? [];
					$value->setDefaultIds($ids);
					$document->$setter($value);
				}
				else
				{
					$value = $dbProvider->dbToPhp($dbData[$name], $sqlMapping->getDbScalarType($type));
					if ($property->getLocalized())
					{
						$localisation->$setter($value);
					}
					elseif ($name !== 'id' && $name !== 'model')
					{
						$document->$setter($value);
					}
				}
			}

			$document->setPersistentState(AbstractDocument::STATE_LOADED);
			if ($LCID)
			{
				$localisation->setPersistentState(AbstractDocument::STATE_LOADED);
			}

			$documents[] = $document;
		}

		return $documents;
	}

	/**
	 * @param AbstractDocument $document
	 */
	public function reference(AbstractDocument $document)
	{
		if ($document->getId() > 0)
		{
			$this->setInCache($document);
		}
	}

	/**
	 * @api
	 * @return integer
	 */
	public function getCacheSize()
	{
		return $this->cacheSize;
	}

	/**
	 * @api
	 * @param integer $cacheSize
	 * @return $this
	 */
	public function setCacheSize($cacheSize)
	{
		$this->cacheSize = max(50, (int)$cacheSize);
		return $this;
	}

	/**
	 * @api
	 * @param $documentId
	 * @return boolean
	 */
	public function isInCache($documentId)
	{
		return $this->getFromCache($documentId) !== null;
	}

	/**
	 * @api
	 * @param integer $documentId
	 * @return AbstractDocument|null
	 */
	public function getFromCache($documentId)
	{
		$id = (int)$documentId;
		return $this->documentInstances[$id] ?? null;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return $this
	 */
	protected function setInCache(AbstractDocument $document)
	{
		if ($document->getId() > 0)
		{
			$this->documentInstances[$document->getId()] = $document;
		}
		return $this;
	}

	protected function gcCache()
	{
		if (!$this->inTransaction)
		{
			$this->cycleCount++;
			if ($this->cycleCount > $this->cacheSize)
			{
				$this->application->getLogging()->info(__METHOD__, $this->cycleCount, $this->cacheSize);
				$this->reset();
			}
		}
	}

	/**
	 * @param boolean|null $persistentCache
	 * @return boolean Old value
	 */
	public function usePersistentCache($persistentCache = null)
	{
		if ($persistentCache === null)
		{
			return $this->persistentCache;
		}
		$oldValue = $this->persistentCache;
		$this->persistentCache = (bool)$persistentCache;
		return $oldValue;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetFromPersistentCache(\Change\Events\Event $event)
	{
		$id = $event->getParam('id', 0);
		$cacheManager = $event->getApplicationServices()->getCacheManager();
		$cacheOptions = ['ttl' => 36000];
		$str = $cacheManager->getEntry('prefetch', (string)$id, $cacheOptions);
		if ($str)
		{
			$doc = (new Serializer($this))->unserialize($str);
			$event->setParam('document', $doc);
			$event->stopPropagation();
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onAddToPersistentCache(\Change\Events\Event $event)
	{
		/** @var AbstractDocument $document */
		$document = $event->getParam('document');
		$cacheManager = $event->getApplicationServices()->getCacheManager();
		if ($document && $cacheManager->isValidNamespace('prefetch'))
		{
			$cacheOptions = ['ttl' => 36000];
			$str = (new Serializer($this))->serialize($document);
			$cacheManager->setEntry('prefetch', (string)$document->getId(), $str, $cacheOptions);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDocumentModified(\Change\Documents\Events\Event $event)
	{
		$id = $event->getDocument()->getId();
		$this->modifiedInstanceIds[$id] = $id;
	}

	/**
	 * @api
	 * @param int $id
	 * @return $this
	 */
	public function addModifiedInstanceId(int $id)
	{
		$this->modifiedInstanceIds[$id] = $id;
		return $this;
	}

	/**
	 * @api
	 * @param string|AbstractModel $modelOrModelName
	 * @param string $LCID if null use refLCID of document
	 * @return \Change\Documents\Query\Query
	 */
	public function getNewQuery($modelOrModelName, $LCID = null)
	{
		$query = new \Change\Documents\Query\Query($modelOrModelName, $this, $this->getModelManager(), $this->getDbProvider());
		if ($LCID)
		{
			$query->setLCID($LCID);
		}
		return $query;
	}

	/**
	 * @api
	 * @param AbstractDocument $document
	 * @param array $backupData
	 * @return integer
	 */
	public function insertDocumentBackup(AbstractDocument $document, array $backupData)
	{
		$qb = $this->getNewStatementBuilder('insertDocumentBackup_delete');
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentDeletedTable());
			$qb->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}
		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $document->getId());
		$dq->execute();

		$qb = $this->getNewStatementBuilder('insertDocumentBackup');
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->getDocumentDeletedTable(), $fb->getDocumentColumn('id'), $fb->getDocumentColumn('model'),
				'deletiondate', 'datas')
				->addValues($fb->integerParameter('id'), $fb->parameter('model'),
					$fb->dateTimeParameter('deletiondate'),
					$fb->lobParameter('datas'));
		}
		$iq = $qb->insertQuery();
		$iq->bindParameter('id', $document->getId());
		$iq->bindParameter('model', $document->getDocumentModelName());
		$iq->bindParameter('deletiondate', new \DateTime());
		$iq->bindParameter('datas', json_encode($backupData));
		return $iq->execute();
	}

	/**
	 * @api
	 * @param integer $documentId
	 * @return array|null
	 */
	public function getBackupData($documentId)
	{
		$qb = $this->getNewQueryBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->getDocumentColumn('model'), 'model'), 'deletiondate', 'datas')
				->from($fb->getDocumentDeletedTable())
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}

		$sq = $qb->query();
		$sq->bindParameter('id', $documentId);

		$converter = new ResultsConverter($sq->getDbProvider(), ['datas' => ScalarType::TEXT,
			'deletiondate' => ScalarType::DATETIME]);

		$row = $sq->getFirstResult([$converter, 'convertRow']);
		if ($row !== null)
		{
			$datas = json_decode($row['datas'], true);
			$datas['id'] = (int)$documentId;
			$datas['model'] = $row['model'];
			$datas['deletiondate'] = $row['deletiondate'];
			return $datas;
		}
		return null;
	}

	// Working lang.
	/**
	 * Get the current lcid.
	 * @api
	 * @return string ex: "en_US" or "fr_FR"
	 */
	public function getLCID()
	{
		if (count($this->LCIDStack) > 0)
		{
			return end($this->LCIDStack);
		}
		else
		{
			return $this->getI18nManager()->getLCID();
		}
	}

	/**
	 * Push a new working language code.
	 * @api
	 * @throws \InvalidArgumentException
	 * @param string $LCID ex: "fr_FR"
	 */
	public function pushLCID($LCID)
	{
		if (!$this->getI18nManager()->isSupportedLCID($LCID))
		{
			throw new \InvalidArgumentException('Invalid LCID argument: ' . $LCID, 51012);
		}
		$this->LCIDStack[] = $LCID;
	}

	/**
	 * Pop the last working language code.
	 * @api
	 * @param \Exception $exception
	 * @throws null
	 */
	public function popLCID($exception = null)
	{
		if ($this->getLCIDStackSize() === 0)
		{
			if ($exception === null)
			{
				$exception = new \LogicException('Invalid LCID Stack size', 51013);
			}
		}
		else
		{
			array_pop($this->LCIDStack);
		}

		if ($exception !== null)
		{
			throw $exception;
		}
	}

	/**
	 * Get the lang stack size.
	 * @api
	 * @return integer
	 */
	public function getLCIDStackSize()
	{
		return count($this->LCIDStack);
	}

	// Attributes.

	/**
	 * @api
	 * @param integer $id
	 * @return \Change\Documents\Attributes\Interfaces\Typology|null
	 */
	public function getTypology($id)
	{
		$eventManager = $this->getEventManager();
		$params = $eventManager->prepareArgs(['typologyId' => $id]);
		$eventManager->trigger('getTypology', $this, $params);
		return $params['typology'] ?? null;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetTypologyFromCache($event)
	{
		$typologyId = (int)$event->getParam('typologyId', 0);
		if (!$typologyId)
		{
			return;
		}
		$cacheManager = $event->getApplicationServices()->getCacheManager();
		$namespace = $cacheManager->buildMemoryNamespace('DocumentManager');
		$key = 'typology_' . $typologyId . '_' . $this->getLCID();
		$typology = $cacheManager->getEntry($namespace, $key);
		if ($typology instanceof \Change\Documents\Attributes\Interfaces\Typology)
		{
			$event->setParam('typology', $typology);
		}
		else
		{
			$event->setParam('cacheEntry', [$namespace, $key]);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onSetTypologyInCache($event)
	{
		$cacheEntry = $event->getParam('cacheEntry');
		if (is_array($cacheEntry))
		{
			list($namespace, $key) = $cacheEntry;
			$typology = $event->getParam('typology');
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$cacheManager->setEntry($namespace, $key, $typology);
		}
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @return \Change\Documents\Attributes\Interfaces\Typology|null
	 */
	public function getTypologyByDocument($document)
	{
		$typologyId = $this->getTypologyIdByDocument($document);
		return $typologyId ? $this->getTypology($typologyId) : null;
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @return integer|null
	 */
	public function getTypologyIdByDocument($document)
	{
		$typologyId = 0;
		$doc = is_numeric($document) ? $this->getDocumentInstance($document) : $document;
		if ($doc instanceof AbstractDocument)
		{
			$typologyId = $doc->getCachedTypologyId();
		}
		return $typologyId ?: null;
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @return \Change\Documents\Attributes\AttributeValues
	 */
	public function getAttributeValues($document)
	{
		$doc = $document instanceof AbstractDocument ? $document : $this->getDocumentInstance((int)$document);
		if ($doc)
		{
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['document' => $doc]);
			$eventManager->trigger('getAttributeValues', $this, $args);
			if (isset($args['attributeValues']) && $args['attributeValues'] instanceof \Change\Documents\Attributes\AttributeValues)
			{
				return $args['attributeValues'];
			}
		}
		return new \Change\Documents\Attributes\AttributeValues($this->getLCID());
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetAttributeValuesFromCache($event)
	{
		$document = $event->getParam('document');
		if ($document instanceof AbstractDocument)
		{
			$documentId = $document->getId();
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$namespace = $cacheManager->buildMemoryNamespace('DocumentManager');
			$key = 'attributeValues_' . $documentId . '_' . $this->getLCID();
			$attributeValues = $cacheManager->getEntry($namespace, $key);
			if ($attributeValues instanceof \Change\Documents\Attributes\AttributeValues)
			{
				$event->setParam('attributeValues', $attributeValues);
			}
			else
			{
				$event->setParam('cacheEntry', [$namespace, $key]);
				if ($event->getParam('typologyId') === null)
				{
					$typologyId = $document->getCachedTypologyId();
					$event->setParam('typologyId', $typologyId);
					$event->setParam('rawValues', $document->getCachedAttributesValue());
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onSetAttributeValuesInCache($event)
	{
		$cacheEntry = $event->getParam('cacheEntry');
		if (is_array($cacheEntry))
		{
			list($namespace, $key) = $cacheEntry;
			$attributeValues = $event->getParam('attributeValues');
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$cacheManager->setEntry($namespace, $key, $attributeValues);
		}
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param \Change\Documents\Attributes\Interfaces\Typology|null $typology
	 * @param \Change\Documents\Attributes\AttributeValues|null $values
	 */
	public function saveAttributeValues($document, $typology, $values)
	{
		$doc = is_numeric($document) ? $this->getDocumentInstance($document) : $document;

		if (!($doc instanceof \Change\Documents\AbstractDocument))
		{
			return;
		}
		$documentId = $doc->getId();

		$oldData = $this->doLoadAttributeValues($documentId);
		$oldTypologyId = $oldData['typologyId'];
		$oldRawValues = $oldData['rawValues'];

		if (!($typology instanceof \Change\Documents\Attributes\Interfaces\Typology))
		{
			if ($oldTypologyId)
			{
				$this->doDeleteAttributeValues($documentId);

				$documentEvent = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_ATTRIBUTES_DELETED,
					$doc, ['oldTypologyId' => $oldTypologyId, 'oldValues' => $oldRawValues]);
				$doc->getEventManager()->triggerEvent($documentEvent);
			}
			return;
		}
		$typologyId = $typology->getId();

		$eventManager = $this->getEventManager();
		$params = $eventManager->prepareArgs([
			'document' => $doc,
			'typology' => $typology,
			'values' => $values,
			'oldTypologyId' => $oldTypologyId,
			'oldRawValues' => $oldRawValues
		]);
		$eventManager->trigger('saveAttributeValues', $this, $params);
		$values = $params['rawValues'] ?? null;

		// Check that there is a real change.
		if ($oldTypologyId == $typologyId && $values == $oldRawValues)
		{
			return;
		}

		// Update data in main table.
		if ($oldTypologyId === null)
		{
			$this->doInsertAttributeValues($documentId, $typologyId, $values);
		}
		else
		{
			$this->doUpdateAttributeValues($documentId, $typologyId, $values);
		}

		$doc->setCachedAttributesValue(null);
		$doc->setCachedTypologyId(null);

		$documentEvent = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_ATTRIBUTES_CHANGED,
			$doc, ['oldTypologyId' => $oldTypologyId, 'oldValues' => $oldRawValues]);
		$doc->getEventManager()->triggerEvent($documentEvent);
	}

	/**
	 * @param integer $documentId
	 * @return array
	 */
	protected function doLoadAttributeValues($documentId)
	{
		$qb = $this->getNewQueryBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->column('typology_id'), $fb->column('data'))
				->from($fb->getDocumentAttributesTable())
				->where($fb->eq($fb->column('document_id'), $fb->integerParameter('id')));
		}

		$sq = $qb->query();
		$sq->bindParameter('id', $documentId);
		$row = $sq->getFirstResult();
		if (is_array($row))
		{
			return ['typologyId' => $row['typology_id'], 'rawValues' => $row['data'] ? json_decode($row['data'], true) : []];
		}
		return ['typologyId' => null, 'rawValues' => []];
	}

	/**
	 * @param integer $documentId
	 * @param integer $typologyId
	 * @param array $values
	 */
	protected function doInsertAttributeValues($documentId, $typologyId, $values)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->getDocumentAttributesTable(), $fb->column('document_id'), $fb->column('typology_id'),
				$fb->column('data'));
			$qb->addValues($fb->integerParameter('document_id'), $fb->integerParameter('typology_id'), $fb->lobParameter('data'));
		}
		$is = $qb->insertQuery();
		$is->bindParameter('document_id', $documentId)->bindParameter('typology_id', $typologyId)
			->bindParameter('data', json_encode($values));
		$is->execute();
	}

	/**
	 * @param integer $documentId
	 * @param integer $typologyId
	 * @param array $values
	 */
	protected function doUpdateAttributeValues($documentId, $typologyId, $values)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->getDocumentAttributesTable());
			$qb->assign($fb->column('typology_id'), $fb->integerParameter('typology_id'));
			$qb->assign($fb->column('data'), $fb->lobParameter('data'));
			$qb->where($fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')));
		}
		$uq = $qb->updateQuery();
		$uq->bindParameter('document_id', $documentId)->bindParameter('typology_id', $typologyId)
			->bindParameter('data', json_encode($values));
		$uq->execute();
	}

	/**
	 * @param integer $documentId
	 */
	protected function doDeleteAttributeValues($documentId)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentAttributesTable());
			$qb->where($fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')));
		}
		$dq = $qb->deleteQuery();
		$dq->bindParameter('document_id', $documentId);
		$dq->execute();
	}
}