<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change\Stdlib;

/**
 * @name \Change\Stdlib\FluentdWriter
 */
class FluentdWriter
{
	const CONNECTION_TIMEOUT = 3;
	const SOCKET_TIMEOUT = 3;
	const MAX_WRITE_RETRY = 5;

	/* 1000 means 0.001 sec */
	const USLEEP_WAIT = 1000;

	/**
	 * backoff strategies: default usleep
	 *
	 * attempts | wait
	 * 1        | 0.003 sec
	 * 2        | 0.009 sec
	 * 3        | 0.027 sec
	 * 4        | 0.081 sec
	 * 5        | 0.243 sec
	 **/
	const BACKOFF_TYPE_EXPONENTIAL = 0x01;
	const BACKOFF_TYPE_USLEEP = 0x02;

	/* @var string host name */
	protected $host;

	/* @var int port number. when you wanna use unix domain socket. set port to 0 */
	protected $port;

	/* @var string Various style transport: `tcp://localhost:port` */
	protected $transport;

	/* @var resource */
	protected $socket;

	protected $options = [
		'socket_timeout' => self::SOCKET_TIMEOUT,
		'connection_timeout' => self::CONNECTION_TIMEOUT,
		'backoff_mode' => self::BACKOFF_TYPE_USLEEP,
		'backoff_base' => 3,
		'usleep_wait' => self::USLEEP_WAIT,
		'persistent' => false,
		'retry_socket' => true,
		'max_write_retry' => self::MAX_WRITE_RETRY,
	];

	protected static $supported_transports = [
		'tcp',
		'unix',
	];

	protected static $acceptable_options = [
		'socket_timeout',
		'connection_timeout',
		'backoff_mode',
		'backoff_base',
		'usleep_wait',
		'persistent',
		'retry_socket',
		'max_write_retry',
	];

	/**
	 * create fluent logger object.
	 *
	 * @param string $host
	 * @param int $port
	 * @param array $options
	 */
	public function __construct($host, $port, array $options = [])
	{
		if (!$host || !$port)
		{
			throw new \InvalidArgumentException('Fluentd host and port can\'t be empty');
		}

		/* keep original host and port */
		$this->host = $host;
		$this->port = $port;

		/* make various URL style socket transports */
		$this->transport = $this->getTransportUri($host, $port);

		$this->mergeOptions($options);
	}

	/**
	 * make a various style transport uri with specified host and port.
	 * currently, in_forward uses tcp transport only.
	 *
	 * @param $host
	 * @param $port
	 * @return string
	 * @throws \Exception
	 */
	protected function getTransportUri($host, $port)
	{
		if (($pos = strpos($host, '://')) !== false)
		{
			$transport = substr($host, 0, $pos);
			$host = substr($host, $pos + 3);

			if (!in_array($transport, self::$supported_transports))
			{
				throw new \Exception('transport `{$transport}` does not support');
			}

			// Now, unix socket is recommended on PHP client.
			if ($transport === 'unix')
			{
				// unix domain socket have to ignore port number
				$result = 'unix://' . $host;
			}
			else
			{
				if (strpos($host, '::') !== false)
				{
					/* ipv6 address should be surrounded brackets */
					$host = sprintf('[%s]', trim($host, '[]'));
				}

				$result = sprintf('%s://%s:%d', $transport, $host, $port);
			}
		}
		else
		{
			if (strpos($host, '::') !== false)
			{
				/* ipv6 address should be surrounded brackets */
				$host = sprintf('[%s]', trim($host, '[]'));
			}

			$result = sprintf('tcp://%s:%d', $host, $port);
		}

		return $result;
	}

	/**
	 * merge options
	 *
	 * @param array $options
	 * @throws \Exception
	 */
	protected function mergeOptions(array $options)
	{
		foreach ($options as $key => $value)
		{
			if (in_array($key, self::$acceptable_options))
			{
				$this->options[$key] = $value;
			}
		}
	}

	/**
	 * create a connection to specified fluentd
	 *
	 * @throws \Exception
	 */
	public function connect()
	{
		$connect_options = \STREAM_CLIENT_CONNECT;
		if ($this->getOption('persistent', false))
		{
			$connect_options |= \STREAM_CLIENT_PERSISTENT;
		}

		// could not suppress warning without ini setting.
		// for now, we use error control operators.
		$socket = @stream_socket_client($this->transport, $errno, $errstr,
			$this->getOption('connection_timeout', self::CONNECTION_TIMEOUT),
			$connect_options
		);

		if (!$socket)
		{
			$errors = error_get_last();
			throw new \Exception($errors['message']);
		}

		// set read / write timeout.
		stream_set_timeout($socket, $this->getOption('socket_timeout', self::SOCKET_TIMEOUT));
		$this->socket = $socket;
	}

	/**
	 * create a connection if Fluent Logger hasn't a socket connection.
	 *
	 * @return void
	 */
	public function reconnect()
	{
		if (!is_resource($this->socket))
		{
			$this->connect();
		}
	}

	/**
	 * send a message to specified fluentd.
	 *
	 * @param string $tag
	 * @param array $data
	 * @param int|null $time
	 * @return bool
	 *
	 * @api
	 */
	public function post($tag, array $data, $time = null)
	{
		$entity = ['tag' => $tag, 'data' => $data, 'time' => ($time ?? time())];

		return $this->postImpl($entity);
	}

	/**
	 * post implementation
	 *
	 * @param array $entity
	 * @return bool
	 * @throws \Exception
	 */
	protected function postImpl($entity)
	{
		$buffer = $packed = $this->pack($entity);
		$length = strlen($packed);
		$retry = $written = 0;

		try
		{
			$this->reconnect();
		}
		catch (\Exception $e)
		{
			$this->close();
			throw $e;
		}

		try
		{
			// PHP socket looks weird. we have to check the implementation.
			while ($written < $length)
			{
				$nwrite = $this->writeSocket($buffer);

				if ($nwrite === false)
				{
					// could not write messages to the socket.
					// e.g) Resource temporarily unavailable
					throw new \Exception('could not write message');
				}
				elseif ($nwrite === '')
				{
					// sometimes fwrite returns null string.
					// probably connection aborted.
					throw new \Exception('connection aborted');
				}
				elseif ($nwrite === 0)
				{
					if (!$this->getOption('retry_socket', true))
					{
						throw new \Exception('could not send entities');
					}

					if ($retry > $this->getOption('max_write_retry', self::MAX_WRITE_RETRY))
					{
						throw new \Exception('failed fwrite retry: retry count exceeds limit.');
					}

					$errors = error_get_last();
					if ($errors)
					{
						if (isset($errors['message']) && strpos($errors['message'], 'errno=32 ') !== false)
						{
							// breaking pipes: we have to close socket manually
							$this->close();
							$this->reconnect();
						}
						/** @noinspection NotOptimalIfConditionsInspection */
						/** @noinspection MissingOrEmptyGroupStatementInspection */
						elseif (isset($errors['message']) && strpos($errors['message'], 'errno=11 ') !== false)
						{
							// we can ignore EAGAIN message. just retry.
						}
						else
						{
							throw new \Exception('unhandled error detected. please report this issue to http://github.com/fluent/fluent-logger-php/issues: '
								. var_export($errors, true));
						}
					}

					if ($this->getOption('backoff_mode', self::BACKOFF_TYPE_EXPONENTIAL) == self::BACKOFF_TYPE_EXPONENTIAL)
					{
						$this->backoffExponential(3, $retry);
					}
					else
					{
						usleep($this->getOption('usleep_wait', self::USLEEP_WAIT));
					}
					$retry++;
					continue;
				}

				$written += $nwrite;
				$buffer = substr($packed, $written);
			}
		}
		catch (\Exception $e)
		{
			$this->close();
			throw $e;
		}

		return true;
	}

	/**
	 * backoff exponential sleep
	 *
	 * @param $base int
	 * @param $attempt int
	 */
	protected function backoffExponential($base, $attempt)
	{
		usleep(pow($base, $attempt) * 1000);
	}

	/**
	 * write data
	 *
	 * @param string $data
	 * @return mixed integer|false
	 */
	public function writeSocket($buffer)
	{
		// We handle fwrite error on postImpl block. ignore error message here.
		return @fwrite($this->socket, $buffer);
	}

	/**
	 * close the socket
	 *
	 * @return void
	 */
	protected function close()
	{
		if (is_resource($this->socket))
		{
			fclose($this->socket);
		}
	}

	/**
	 * destruct objects and socket.
	 *
	 * @return void
	 */
	public function __destruct()
	{
		if (!$this->getOption('persistent', false) && is_resource($this->socket))
		{
			fclose($this->socket);
		}
	}

	/**
	 * get specified option's value
	 *
	 * @param      $key
	 * @param null $default
	 * @return mixed
	 */
	protected function getOption($key, $default = null)
	{
		$result = $default;
		if (isset($this->options[$key]))
		{
			$result = $this->options[$key];
		}

		return $result;
	}

	/**
	 * pack entity as a json string.
	 *
	 * @param array $entity
	 * @return string
	 */
	private function pack($entity)
	{
		return json_encode([$entity['tag'], $entity['time'], $entity['data']]);
	}
}