<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Sms;

/**
 * @name \Change\Sms\SmsManager
 */
class SmsManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'SmsManager';

	const EVENT_SEND_MESSAGE = 'sendMessage';

	const EVENT_MESSAGE_STATUS = 'MessageStatus';

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(self::EVENT_SEND_MESSAGE, function($event) {$this->onDefaultSendMessage($event);});
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/SmsManager');
	}

	/**
	 * @param string $mobilePhone
	 * @param string $message
	 * @return string|null message identifier
	 */
	public function sendMessage($mobilePhone, $message)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['mobilePhone' => $mobilePhone, 'message' => $message, 'messageIdentifier' => null]);
		$eventManager->trigger(self::EVENT_SEND_MESSAGE, $this, $args);
		return $args['messageIdentifier'] ?: null;
	}


	protected function onDefaultSendMessage(\Change\Events\Event $event)
	{
		if ($event->getParam('messageIdentifier') !== null)
		{
			return;
		}

		$application = $event->getApplication();
		$configuration = $application->getConfiguration('Change/Sms');
		if (!is_array($configuration) || !isset($configuration['type']))
		{
			return;
		}
		$mobilePhone = $event->getParam('mobilePhone');
		$message = $event->getParam('message');
		switch ($configuration['type'])
		{
			case 'mobyt':
				$provider = new MobytProvider($configuration, $application);
				$messageIdentifier = $provider->sendMessage($mobilePhone, $message);
				$event->setParam('messageIdentifier', $messageIdentifier);
				break;
			case 'proximis':
				$provider = new ProximisProvider($configuration, $application);
				$messageIdentifier = $provider->sendMessage($mobilePhone, $message);
				$event->setParam('messageIdentifier', $messageIdentifier);
				break;
		}
	}


	/**
	 * @param string $messageIdentifier
	 * @return mixed
	 */
	public function getMessageStatus($messageIdentifier)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['messageIdentifier' => $messageIdentifier, 'messageStatus' => null]);
		$eventManager->trigger(self::EVENT_MESSAGE_STATUS, $this, $args);
		return $args['messageStatus'];
	}
}