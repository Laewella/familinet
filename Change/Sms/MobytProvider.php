<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Sms;

/**
 * @name \Change\Sms\MobytProvider
 */
class MobytProvider
{
	/**
	 * @var array
	 */
	protected $configuration;

	/**
	 * @var string
	 */
	protected $changeVersion;

	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @param array $configuration
	 * @param \Change\Application $application
	 */
	public function __construct(array $configuration, $application)
	{
		$this->configuration = array_merge(['login' => null, 'password' => null, 'fakeSms' => null,
				'from' => 'Proximis', 'domaine' => 'http://multilevel.mobyt.fr', 'quality' => 'n'], $configuration);

		$this->changeVersion = $application->getVersion();
		$this->application = $application;
	}

	/**
	 * @param string $recipient
	 * @param string $message
	 * @return false|string
	 */
	public function sendMessage($recipient, $message)
	{
		$params = [];
		$params['user'] = $login = $this->configuration['login'];

		$params['sender'] = $from = $this->configuration['from'];
		$params['rcpt'] = $this->configuration['fakeSms'] ?: $recipient;
		$params['data'] = $message;
		$params['operation'] = 'TEXT';
		$params['url'] = '';
		$params['return_id'] = '1';

		$password = $this->application->checkDevValue($this->configuration['password']);
		$params['qty'] = $quality = $this->configuration['quality'];
		$params['pass'] = '';
		$params['ticket'] = md5($login . $recipient . $from . $message . $quality . md5($password));

		$params['domaine'] = $domaine = $this->configuration['domaine'];
		$params['path'] = $path = '/sms/send.php';

		$postFields = [];
		foreach ($params as $k => $v)
		{
			if ($v === null)
			{
				return false;
			}
			$postFields[] = $k . '=' . urlencode($v);
		}

		return $this->doPost(implode('&', $postFields), $domaine. $path);
	}

	/**
	 * @param string $postFieldsString
	 * @param string $url
	 * @return string|false
	 */
	protected function doPost($postFieldsString, $url)
	{
		if ($ch = @curl_init($url))
		{
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Proximis Omnichannel/'. $this->changeVersion .' (curl)');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postFieldsString);
			$result = curl_exec($ch);
			curl_close($ch);
			//Sample success result 'OK HTTP00120492212'
			if (is_string($result) && substr($result, 0 , 7) === 'OK HTTP')
			{
				return substr($result, 7);
			}
		}
		return false;
	}
}

