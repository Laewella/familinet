<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change\Logging;

/**
 * @api
 * @name \Change\Logging\Logging
 */
class Logging
{
	/**
	 * @var \Change\Configuration\Configuration
	 */
	protected $configuration;

	/**
	 * @var \Change\Workspace
	 */
	protected $workspace;

	/**
	 * @var integer
	 */
	protected $priority;

	/**
	 * @var \Zend\Log\Logger[]
	 */
	protected $loggers = [];

	/**
	 * @var string
	 */
	protected $defaultDirectory;

	/**
	 * @var \Change\User\UserInterface
	 */
	protected $user;

	/**
	 * @param \Change\Workspace $workspace
	 * @return $this
	 */
	public function setWorkspace(\Change\Workspace $workspace)
	{
		$this->workspace = $workspace;
		return $this;
	}

	/**
	 * @return \Change\Workspace
	 */
	public function getWorkspace()
	{
		return $this->workspace;
	}

	/**
	 * @param \Change\Configuration\Configuration $configuration
	 * @return $this
	 */
	public function setConfiguration(\Change\Configuration\Configuration $configuration)
	{
		$this->configuration = $configuration;
		return $this;
	}

	/**
	 * @return \Change\Configuration\Configuration
	 */
	public function getConfiguration()
	{
		return $this->configuration;
	}

	/**
	 * @param \Change\User\UserInterface|null $user
	 * @return $this
	 */
	public function setUser($user)
	{
		$this->user = $user instanceof \Change\User\UserInterface ? $user : null;
		return $this;
	}

	/**
	 * @return \Change\User\UserInterface|null
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @return string (DEBUG, INFO, NOTICE, WARN, ERR, ALERT, EMERG)
	 */
	public function getLevel()
	{
		return $this->configuration->getEntry('Change/Logging/level');
	}

	/**
	 * @return integer
	 */
	public function getPriority()
	{
		if ($this->priority === null)
		{
			switch ($this->getLevel())
			{
				case 'ALERT' :
					$this->priority = 1;
					break;
				case 'ERR' :
					$this->priority = 3;
					break;
				case 'NOTICE' :
					$this->priority = 5;
					break;
				case 'DEBUG' :
					$this->priority = 7;
					break;
				case 'INFO' :
					$this->priority = 6;
					break;
				default :
					$this->priority = 4;
					break;
			}
		}
		return $this->priority;
	}

	/**
	 * @param $priority integer
	 */
	public function setPriority($priority)
	{
		$this->priority = (is_int($priority) && $priority >= 1 && $priority <= 7) ? $priority : null;
	}

	/**
	 * @param string $name
	 * @param \Zend\Log\Logger $logger
	 * @return $this
	 */
	public function setLoggerByName($name, $logger)
	{
		$this->loggers[$name] = $logger;
		return $this;
	}

	/**
	 * @param string $name
	 * @return \Zend\Log\Logger
	 */
	public function getLoggerByName($name = 'application')
	{
		if (!isset($this->loggers[$name]))
		{
			$this->loggers[$name] = $this->createLogger($name);
		}
		return $this->loggers[$name];
	}

	/**
	 * @return string
	 */
	protected function getDefaultDirectory()
	{
		if ($this->defaultDirectory === null)
		{
			$defaultDirectory = $this->getConfiguration()->getEntry('Change/Logging/writers/directory', 'log');
			$this->defaultDirectory = $this->getWorkspace()->composeAbsolutePath($defaultDirectory);
		}
		return $this->defaultDirectory;
	}

	/**
	 * @param string $name
	 * @return \Zend\Log\Writer\AbstractWriter
	 */
	protected function createStreamWriter($name)
	{
		$directory = ($name == 'application' || $name == 'phperror') ? 'project' : 'other';
		$filePath = $this->getWorkspace()->composePath($this->getDefaultDirectory(), $directory, $name . '.log');
		if (!file_exists($filePath))
		{
			\Change\Stdlib\FileUtils::mkdir(dirname($filePath));
		}
		$writer = new \Zend\Log\Writer\Stream($filePath);
		if ($name === 'phperror')
		{
			$writer->setFormatter(new \Zend\Log\Formatter\ErrorHandler());
		}
		return $writer;
	}

	/**
	 * @param string $name
	 * @return \Zend\Log\Writer\AbstractWriter
	 */
	protected function createSyslogWriter($name)
	{
		$this->getConfiguration()->getEntry('Change/Logging/writers/directory', 'log');
		$formatter = $name === 'phperror' ?
			new \Zend\Log\Formatter\ErrorHandler('%priorityName% (%priority%) %message% (errno %extra[errno]%) in %extra[file]% on line %extra[line]%') :
			new \Zend\Log\Formatter\Simple('%priorityName% (%priority%): %message% %extra%');
		return new \Zend\Log\Writer\Syslog(['application' => 'Proximis\\' . ucfirst($name),
			'formatter' => $formatter]);
	}

	/**
	 * @param string $name
	 * @return \Change\Logging\GoogleFluentd
	 */
	protected function createGooglefluentdWriter($name)
	{
		$conf = $this->getConfiguration();
		$fluentdConf = $this->getConfiguration()->getEntry('Change/fluentd') ?? $this->getConfiguration()->getEntry('Change/Logging/writers/fluentd');
		if(!$fluentdConf)
		{
			throw new \Exception('No configuration found for Fluentd');
		}

		$ip = $fluentdConf['ip'] ?? null;
		$port = $fluentdConf['port'] ?? null;
		$writer = new \Change\Logging\GoogleFluentd($ip, $port);
		$writer->setVersion($conf->getEntry('Change/Application/version'));
		$writer->setInstance($fluentdConf['instance'] ?? null);

		return $writer;
	}

	/**
	 * @param string $loggerName
	 * @return string
	 */
	protected function getCreateWriterMethodName($loggerName)
	{
		$writerType = $this->configuration->getEntry('Change/Logging/writers/' . $loggerName, null);
		if ($writerType === null)
		{
			$writerType = $this->configuration->getEntry('Change/Logging/writers/default', 'stream');
		}
		$methodName = 'create' . ucfirst(strtolower($writerType)) . 'Writer';
		if (method_exists($this, $methodName))
		{
			return $methodName;
		}
		return 'createStreamWriter';
	}

	/**
	 * @param string $name
	 * @return \Zend\Log\Logger
	 */
	protected function createLogger($name)
	{
		$logger = new \Zend\Log\Logger();
		$writerMethodName = $this->getCreateWriterMethodName($name);
		/* @var $writer \Zend\Log\Writer\AbstractWriter */
		$writer = $this->$writerMethodName($name);
		if ($name === 'phperror')
		{
			$writer->setConvertWriteErrorsToExceptions(false);
		}
		elseif ($name === 'application')
		{
			$writer->addFilter(new \Zend\Log\Filter\Priority($this->getPriority()));
		}
		$logger->addWriter($writer);
		return $logger;
	}

	/**
	 * @api
	 * @param string $message
	 * @param string[] $complements
	 */
	public function debug($message, ...$complements)
	{
		if ($complements)
		{
			$message .= ' ' . implode(' ', array_map('strval', $complements));
		}
		$this->getLoggerByName('application')->debug($message, ['userId' => $this->user ? $this->user->getId() : 0]);
	}

	/**
	 * @api
	 * @param string $message
	 * @param string[] $complements
	 */
	public function info($message, ...$complements)
	{
		if ($complements)
		{
			$message .= ' ' . implode(' ', array_map('strval', $complements));
		}
		$this->getLoggerByName('application')->info($message, ['userId' => $this->user ? $this->user->getId() : 0]);
	}

	/**
	 * @api
	 * @param string $message
	 * @param string[] $complements
	 */
	public function warn($message, ...$complements)
	{
		if ($complements)
		{
			$message .= ' ' . implode(' ', array_map('strval', $complements));
		}
		$this->getLoggerByName('application')->warn($message, ['userId' => $this->user ? $this->user->getId() : 0]);
	}

	/**
	 * @api
	 * @param string $message
	 * @param string[] $complements
	 */
	public function error($message, ...$complements)
	{
		if ($complements)
		{
			$message .= ' ' . implode(' ', array_map('strval', $complements));
		}
		$this->getLoggerByName('application')->err($message, ['userId' => $this->user ? $this->user->getId() : 0]);
	}

	/**
	 * @api
	 * @param \Exception $e
	 */
	public function exception($e)
	{
		$extra = ['line' => $e->getLine(), 'file' => $e->getFile(), 'errno' => $e->getCode(), 'throwable' => $e,
			'userId' => $this->user ? $this->user->getId() : 0];
		$this->getLoggerByName('phperror')->alert(get_class($e) . ': ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString(), $extra);
	}

	/**
	 * @api
	 * @param \Throwable $t
	 */
	public function throwable($t)
	{
		$extra = ['line' => $t->getLine(), 'file' => $t->getFile(), 'errno' => $t->getCode(), 'throwable' => $t,
			'userId' => $this->user ? $this->user->getId() : 0];
		$this->getLoggerByName('phperror')->alert(get_class($t) . ': ' . $t->getMessage() . PHP_EOL . $t->getTraceAsString(), $extra);
	}

	/**
	 * @api
	 * @param string $message
	 * @param string[] $complements
	 */
	public function fatal($message, ...$complements)
	{
		if ($complements)
		{
			$message .= ' ' . implode(' ', array_map('strval', $complements));
		}
		$this->getLoggerByName('application')->emerg($message, ['userId' => $this->user ? $this->user->getId() : 0]);
	}

	/**
	 * @api
	 * @param string $message
	 * @param string[] $complements
	 */
	public function deprecated($message, ...$complements)
	{
		if ($this->getConfiguration()->getEntry('Change/Application/development-mode'))
		{
			if ($complements)
			{
				$message .= ' ' . implode(' ', array_map('strval', $complements));
			}
			trigger_error($message, E_USER_DEPRECATED);
		}
	}

	/**
	 *
	 */
	public function registerErrorHandler()
	{
		ini_set('display_errors', 0);
		ini_set('html_errors', 0);
		error_reporting($this->getLevel() === 'DEBUG' ? -1 : E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_DEPRECATED);
		$logger = $this->getLoggerByName('phperror');
		\Zend\Log\Logger::registerErrorHandler($logger);
		\Zend\Log\Logger::registerExceptionHandler($logger);
		\Zend\Log\Logger::registerFatalErrorShutdownFunction($logger);
	}

	/**
	 * @api
	 * @param string $stringLine
	 * @param string $logName
	 */
	public function namedLog($stringLine, $logName)
	{
		$logger = $this->getLoggerByName($logName);
		$logger->log($this->getPriority(), $stringLine);
	}
}