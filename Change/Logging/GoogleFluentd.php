<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change\Logging;

/**
 * @name \Change\Logging\GoogleFluentd
 */
class GoogleFluentd extends \Zend\Log\Writer\AbstractWriter
{

	/**
	 * @var \Change\Stdlib\FluentdWriter the writer
	 */
	protected $writer;

	/**
	 * @var string the project version
	 */
	private $version;

	/**
	 * @var string the project instance name
	 */
	private $instance;

	/**
	 * create fluent logger object.
	 *
	 * @param string $host
	 * @param int $port
	 * @param array $options
	 */
	public function __construct($host, $port, array $options = [])
	{
		if (!$host || !$port)
		{
			throw new \InvalidArgumentException("Fluentd host and port can't be empty");
		}

		$this->writer = new \Change\Stdlib\FluentdWriter($host, $port, $options);

		parent::__construct($options);
	}

	/**
	 * Write a message to the log
	 *
	 * @param array $event log data event
	 * @return void
	 */
	protected function doWrite(array $event)
	{
		$data = $this->format($event);
		$tag = 'Proximis.' . ($event['priority'] < 4 ? 'error.' : '') . $event['priorityName'];
		try
		{
			$this->writer->post($tag, $data);
		}
		catch (\Exception $e)
		{
			error_log(sprintf('%s %s %s: %s', get_class($this->writer), $e->getMessage(), $tag, json_encode($data)));
		}
	}

	private function formatBacktrace(array $stack, $skip = true)
	{
		$str = "\nStack trace:\n";
		$n = 0;
		$keep = null;
		foreach ($stack as $trace)
		{
			if ($keep)
			{
				$trace['class'] = $keep['class'];
				$trace['function'] = $keep['function'];
				$trace['type'] = $keep['type'];
				$keep = null;
			}
			if (isset($trace['class']) && $trace['class'] === \Change\Logging\Logging::class)
			{
				$skip = false;
			}
			if ($skip)
			{
				continue;
			}
			if (!isset($trace['file']))
			{
				$keep = ['class' => $trace['class'], 'function' => $trace['function'], 'type' => $trace['type']];
				continue;
			}
			$str .= "#$n " . $trace['file'] . '(' . $trace['line'] . '): ' .
				str_replace("\\\\", "\\", $trace['class'] ?? '') . ($trace['type'] ?? '') . $trace['function'] . "()\n";
			$n++;
		}
		return $str . "#$n {main}";
	}

	/**
	 * @param array $event
	 * @return array
	 */
	protected function format($event)
	{
		if (isset($event['extra'], $event['extra']['app']))
		{
			return $event['extra']['app'];
		}

		$throwable = $event['extra']['throwable'] ?? null;
		if ($throwable instanceof \Throwable)
		{
			$message = 'PHP Fatal error: ' . $throwable->getMessage() . ' (' . get_class($throwable) . ' - ' . $throwable->getCode() . ') ' .
				$this->formatBacktrace($throwable->getTrace(), false);
		}
		elseif ($event['priority'] < 4)
		{
			$message = 'PHP Fatal error: ' . $event['message'] . $this->formatBacktrace(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
		}
		elseif ($event['priority'] === 4)
		{
			$message = 'PHP Warning: ' . $event['message'] . $this->formatBacktrace(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
		}
		else
		{
			$message = 'PHP Info: ' . $event['message'];
		}

		$data = [
			'eventTime' => $event['timestamp'] instanceof \DateTime ? $event['timestamp']->format(\DateTime::RFC3339) :
				(new \DateTime())->format(\DateTime::RFC3339),
			'serviceContext' => [
				'service' => 'Proximis' . ($this->instance ? ' ' . $this->instance : ''),
				'version' => $this->version
			],
			'message' => $message,
			'context' => [
				'reportLocation' => [
					'filePath' => $event['extra']['file'] ?? $_SERVER['SCRIPT_NAME'],
					'lineNumber' => $event['extra']['line'] ?? 0,
					'functionName' => '-'
				]
			]
		];
		$fields = [
			'REQUEST_METHOD' => 'method',
			'REQUEST_URI' => 'url',
			'HTTP_USER_AGENT' => 'userAgent',
			'HTTP_REFERER' => 'referrer',
			'REMOTE_ADDR' => 'remoteIp',
		];
		foreach ($fields as $key => $field)
		{
			if (isset($_SERVER[$key]))
			{
				$data['context']['httpRequest'][$field] = $_SERVER[$key];
			}
		}
		$data['context']['user'] = (string)($event['extra']['userId'] ?? 0);
		return $data;
	}

	public function setVersion($version)
	{
		$this->version = $version;
	}

	public function setInstance($instance)
	{
		$this->instance = $instance;
	}
}