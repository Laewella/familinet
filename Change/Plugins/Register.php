<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins;

/**
 * @name \Change\Plugins\Register
 * @ignore
 */
class Register extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @param Plugin $plugin
	 */
	public function __construct(Plugin $plugin)
	{
		$this->plugin = $plugin;
	}

	/**
	 * @return Plugin
	 */
	public function getPlugin()
	{
		return $this->plugin;
	}

	/**
	 * @param Plugin $plugin
	 * @return string
	 */
	public function getInstallClassName(Plugin $plugin)
	{
		return $plugin->getNamespace() . '\\Setup\Install';
	}

	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$plugin = $this->getPlugin();
		$installClassName = $this->getInstallClassName($plugin);
		if (class_exists($installClassName))
		{
			$installClass = new $installClassName();
			if ($installClass instanceof InstallBase)
			{
				$listeners = $installClass->attach($events, $plugin);
				if (is_array($listeners)) 
				{
					foreach ($listeners as $listener)
					{
						$this->listeners[] = $listener;
					}
				}
			}
			elseif (is_callable([$installClass, 'attach']))
			{
				$listeners = $installClass->{'attach'}($events, $plugin);
				if (is_array($listeners))
				{
					foreach ($listeners as $listener)
					{
						$this->listeners[] = $listener;
					}
				}
			}
		}
		elseif ($this->getPlugin()->getType() === Plugin::TYPE_MODULE)
		{
			$installClass = new InstallBase();
			$listeners = $installClass->attach($events, $plugin);
			if (is_array($listeners))
			{
				foreach ($listeners as $listener)
				{
					$this->listeners[] = $listener;
				}
			}
		}
		elseif ($this->getPlugin()->getType() === Plugin::TYPE_THEME)
		{
			$installClass = new ThemeInstallBase();
			$listeners = $installClass->attach($events, $plugin);
			if (is_array($listeners))
			{
				foreach ($listeners as $listener)
				{
					$this->listeners[] = $listener;
				}
			}
		}
	}
}