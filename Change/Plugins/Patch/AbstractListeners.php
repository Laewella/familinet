<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins\Patch;

/**
 * @name \Change\Plugins\Patch\AbstractListeners
 */
abstract class AbstractListeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Plugins\Patch\PatchManager::EVENT_APPLY,
			function ($event)
			{
				$this->onInit($event);
				$this->onApply($event);
			}, $priority);
	}

	/**
	 * @var \Change\Commands\Events\CommandResponseInterface|null
	 */
	protected $commandResponse;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @var \Change\Plugins\Patch\PatchManager
	 */
	protected $patchManager;

	/**
	 * @var bool
	 */
	protected $run = false;

	/**
	 * @var bool
	 */
	protected $devMode = false;

	/**
	 * @var \ArrayObject
	 */
	protected $patchInstalled;

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onInit($event)
	{
		$patchManager= $event->getTarget();
		$this->patchManager = $patchManager instanceof \Change\Plugins\Patch\PatchManager ? $patchManager : null;
		$commandResponse = $event->getParam('commandResponse');
		$this->commandResponse = $commandResponse instanceof \Change\Commands\Events\CommandResponseInterface ? $commandResponse : null;
		$this->applicationServices = $event->getApplicationServices();
		$this->devMode = $event->getApplication()->inDevelopmentMode();
		$this->run = $event->getParam('run');
		$this->patchInstalled = $event->getParam('patchInstalled');
	}

	/**
	 * @param string $code
	 * @param string $description
	 * @param callable $callable
	 */
	protected function executePatch($code, $description, $callable)
	{
		$patch = $this->patchManager->getPatch($code);
		if (!$patch->installed())
		{
			$this->sendDescription($patch, $description);
			$this->patchInstalled[] = $patch;
			if ($this->run)
			{
				$this->sendRunning($patch);
				$callable($patch);
				$this->sendDone($patch);
			}
		}
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @param string $description
	 */
	protected function sendDescription(\Change\Plugins\Patch\Patch $patch, $description)
	{
		$patch->setDescription($description);
		$this->sendInfo($patch->getCode() . ': ' . $patch->getDescription());
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 */
	protected function sendRunning(\Change\Plugins\Patch\Patch $patch)
	{
		$this->sendInfo($patch->getCode() . ': running...');
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @return \Change\Plugins\Patch\Patch
	 */
	protected function sendDone(\Change\Plugins\Patch\Patch $patch)
	{
		if ($patch->installed())
		{
			$this->sendInfo($patch->getCode() . ': done.');
		}
		else
		{
			$this->sendError('Error on applying patch ' . $patch->getCode());
			$installationData = $patch->getInstallationData();
			if (($e = $installationData['exception'] ?? null) && ($e instanceof \Exception))
			{
				$this->sendError($e->getMessage());
				if ($this->devMode)
				{
					$this->sendError($e->getTraceAsString());
				}
			}
			elseif (isset($installationData['error']) && is_string($installationData['error']))
			{
				$this->sendError($installationData['error']);
			}
		}
		return $patch;
	}


	protected function sendInfo($message)
	{
		if ($this->commandResponse)
		{
			$this->commandResponse->addInfoMessage($message);
		}
		else
		{
			echo $message, PHP_EOL;
		}
	}

	protected function sendError($error)
	{
		if ($this->commandResponse)
		{
			$this->commandResponse->addErrorMessage($error);
		}
		else
		{
			echo $error, PHP_EOL;
		}
	}


	/**
	 * @param \Change\Events\Event $event
	 */
	protected abstract function onApply(\Change\Events\Event $event);
}