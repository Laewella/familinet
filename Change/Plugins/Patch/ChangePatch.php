<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins\Patch;

/**
 * @name \Change\Plugins\Patch\ChangePatch
 */
class ChangePatch
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onApply(\Change\Events\Event $event)
	{
		$patchManager = $event->getTarget();
		if ($patchManager instanceof \Change\Plugins\Patch\PatchManager)
		{

			$patchInstalled = $event->getParam('patchInstalled');
			$run = $event->getParam('run');

			//Change_0001 v1.1 Update publication and activation default dates.
			//$patchInstalled[] = $this->patch0001($patchManager, $run, $event->getApplicationServices());
		}
	}
}