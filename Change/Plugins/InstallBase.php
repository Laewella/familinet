<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins;

/**
 * @name \Change\Plugins\InstallBase
 */
class InstallBase
{
	/**
	 * @var \Change\Plugins\Plugin
	 */
	protected $plugin;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param \Change\Plugins\Plugin $plugin
	 * @return callable[]
	 */
	public function attach($events, $plugin)
	{
		
		$this->setPlugin($plugin);
		$priority = $plugin->getType() == \Change\Plugins\Plugin::TYPE_MODULE ? 10 : 5;
		if ($priority === 5 && $plugin->getName() === 'Rbs_Base')
		{
			$priority = 7;
		}
		$callable = [];
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_INITIALIZE, [$this, 'onSetupInitialize'], $priority);

		$callable[] = $events->attach(PluginManager::EVENT_SETUP_APPLICATION, [$this, 'onSetupApplication'], $priority);

		$callable[] = $events->attach(PluginManager::EVENT_SETUP_DB_SCHEMA, [$this, 'onSetupDbSchema'], $priority);

		$callable[] = $events->attach(PluginManager::EVENT_SETUP_SERVICES, [$this, 'onSetupServices'], $priority);

		$callable[] = $events->attach(PluginManager::EVENT_SETUP_FINALIZE, function (\Change\Events\Event $event) use ($plugin)
		{
			if ($this->isValid($event, $plugin))
			{
				$this->finalize($plugin);
			}
		}, $priority);
		
		return $callable;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @param Plugin $plugin
	 * @return boolean
	 */
	public function isValid(\Change\Events\Event $event, \Change\Plugins\Plugin $plugin)
	{
		$vendor = $event->getParam('vendor');
		$name = $event->getParam('name');
		switch ($event->getParam('type'))
		{
			case PluginManager::EVENT_TYPE_PACKAGE:
				return $vendor === $plugin->getVendor() && $name === $plugin->getPackage();
			case PluginManager::EVENT_TYPE_MODULE:
				return $plugin->isModule() && $vendor === $plugin->getVendor() && $name === $plugin->getShortName();
			case PluginManager::EVENT_TYPE_THEME:
				return $plugin->isTheme() && $vendor === $plugin->getVendor() && $name === $plugin->getShortName();
		}
		return false;
	}

	/**
	 * @return \Change\Plugins\Plugin
	 */
	public function getPlugin()
	{
		return $this->plugin;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @return $this
	 */
	public function setPlugin($plugin)
	{
		$this->plugin = $plugin;
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	public function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager($pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return Plugin|null
	 */
	public function onSetupInitialize(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			$this->initialize($plugin);
			return $plugin;
		}
		return null;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function initialize($plugin)
	{

	}

	/**
	 * @param \Change\Events\Event $event
	 * @return boolean|null
	 */
	public function onSetupApplication(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$this->setPluginManager($pluginManager);

			/* @var $app \Change\Application */
			$app = $event->getApplication();
			$this->executeApplication($plugin, $app, $app->getConfiguration());

			$this->executeThemeAssets($plugin, $pluginManager, $app);

			return true;
		}
		return null;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{

	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		if ($plugin->isModule())
		{
			$workspace = $application->getWorkspace();
			$webThemeAssetsBaseDirectory = $workspace->composePath($pluginManager->getWebAssetsBaseDirectory(), 'Theme');

			$themeAssets = new \Change\Presentation\Themes\ThemeAssets($application);
			$defaultTheme = new PluginTheme(\Change\Presentation\Themes\ThemeManager::DEFAULT_THEME_NAME, $workspace);

			$themeAssets->installPluginTemplates($plugin, $defaultTheme);

			$assetBaseDir =  $workspace->composePath($webThemeAssetsBaseDirectory, $defaultTheme->getVendor(), $defaultTheme->getShortName());
			$themeAssets->installPluginAssets($plugin, $assetBaseDir);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return boolean|null
	 */
	public function onSetupDbSchema(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$this->setPluginManager($pluginManager);

			$this->executeDbSchema($plugin, $event->getApplicationServices()->getDbProvider()->getSchemaManager());
			return true;
		}
		return null;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
	}

	/**
	 * @var \Zend\Stdlib\Parameters|null
	 */
	protected $services;

	/**
	 * @param \Change\Events\Event $event
	 * @return boolean|null
	 */
	public function onSetupServices(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$this->setPluginManager($pluginManager);
			$this->setServices($event->getServices());
			$this->executeServices($plugin, $event->getApplicationServices());
			return true;
		}
		return null;
	}

	/**
	 * @return null|\Zend\Stdlib\Parameters
	 */
	public function getServices()
	{
		return $this->services;
	}

	/**
	 * @param null|\Zend\Stdlib\Parameters $services
	 * @return $this
	 */
	public function setServices($services)
	{
		$this->services = $services;
		return $this;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function executeServices($plugin, $applicationServices)
	{
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function finalize($plugin)
	{
	}
}