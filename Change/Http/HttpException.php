<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http;

/**
 * @name \Change\Http\HttpException
 */
class HttpException extends \RuntimeException
{
	/**
	 * @var integer
	 */
	protected $httpStatus;

	/**
	 * @var array|null
	 */
	protected $data;

	/**
	 * @param string $message
	 * @param integer $code
	 * @param \Exception $previous
	 * @param int $httpStatus
	 * @param $data
	 */
	public function __construct($message, $code, \Exception $previous = null, $httpStatus = \Zend\Http\Response::STATUS_CODE_500, $data = null)
	{
		parent::__construct($message, $code, $previous);
		$this->httpStatus = (int)$httpStatus;
		$this->data = $data ?: null;
	}

	/**
	 * @return integer
	 */
	public function getHttpStatus()
	{
		return $this->httpStatus;
	}

	/**
	 * @param int $httpStatus
	 * @return $this
	 */
	public function setHttpStatus($httpStatus)
	{
		$this->httpStatus = $httpStatus;
		return $this;
	}

	/**
	 * @return mixed|null
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param mixed|null $data
	 * @return $this
	 */
	public function setData($data)
	{
		$this->data = $data && is_array($data) ? $data : null;
		return $this;
	}
}