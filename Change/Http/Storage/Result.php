<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Storage;

/**
* @name \Change\Http\Storage\Result
*/
class Result extends \Change\Http\Result
{
	/**
	 * @var string
	 */
	protected $string;

	/**
	 * @return string
	 */
	public function getString()
	{
		return $this->string;
	}

	/**
	 * @param string $string
	 * @return $this
	 */
	public function setString($string)
	{
		$this->string = $string;
		return $this;
	}


	public function __construct($string, $httpStatusCode = \Zend\Http\Response::STATUS_CODE_200)
	{
		parent::__construct($httpStatusCode);
		$this->string = $string;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return (string)$this->string;
	}
}