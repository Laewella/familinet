<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http;

/**
 * @name \Change\Http\InitHttpFiles
 */
class InitHttpFiles
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Application $application)
	{
		$this->application = $application;
	}

	/**
	 * @param string $webBaseDirectory
	 * @param string $assetsVersion
	 */
	public function initializeControllers($webBaseDirectory, &$assetsVersion)
	{
		$editConfig = new \Change\Configuration\EditableConfiguration([]);
		$editConfig->import($this->application->getConfiguration());
		$workspace = $this->application->getWorkspace();

		if ($webBaseDirectory && !$workspace->isAbsolutePath($webBaseDirectory))
		{
			$requirePath = implode(DIRECTORY_SEPARATOR, array_fill(0, count(explode(DIRECTORY_SEPARATOR, $webBaseDirectory)), '..'));
		}
		else
		{
			$requirePath = $workspace->projectPath();
		}
		$rootPath = $workspace->composeAbsolutePath($webBaseDirectory);

		$srcPath = $workspace->changePath('Http', 'Assets', 'rest.php');
		$content = \Change\Stdlib\FileUtils::read($srcPath);
		$content = str_replace('__DIR__', var_export($requirePath, true), $content);
		\Change\Stdlib\FileUtils::write($rootPath . DIRECTORY_SEPARATOR . basename($srcPath), $content);

		$srcPath = $workspace->changePath('Http', 'Assets', 'rest.V1.php');
		$content = \Change\Stdlib\FileUtils::read($srcPath);
		$content = str_replace('__DIR__', var_export($requirePath, true), $content);
		\Change\Stdlib\FileUtils::write($rootPath . DIRECTORY_SEPARATOR . basename($srcPath), $content);

		$srcPath = $workspace->changePath('Http', 'Assets', 'ajax.php');
		$content = \Change\Stdlib\FileUtils::read($srcPath);
		$content = str_replace('__DIR__', var_export($requirePath, true), $content);
		\Change\Stdlib\FileUtils::write($rootPath . DIRECTORY_SEPARATOR . basename($srcPath), $content);

		$srcPath = $workspace->changePath('Http', 'Assets', 'ajax.V1.php');
		$content = \Change\Stdlib\FileUtils::read($srcPath);
		$content = str_replace('__DIR__', var_export($requirePath, true), $content);
		\Change\Stdlib\FileUtils::write($rootPath . DIRECTORY_SEPARATOR . basename($srcPath), $content);

		$srcPath = $workspace->changePath('Http', 'Assets', 'storage.php');
		$content = \Change\Stdlib\FileUtils::read($srcPath);
		$content = str_replace('__DIR__', var_export($requirePath, true), $content);
		\Change\Stdlib\FileUtils::write($rootPath . DIRECTORY_SEPARATOR . basename($srcPath), $content);

		$editConfig->addPersistentEntry('Change/Install/webBaseDirectory', $webBaseDirectory, \Change\Configuration\Configuration::PROJECT);

		if (!$assetsVersion)
		{
			$assetsVersion = substr(sha1((string)time()),0 , 4);
		}
		$editConfig->addPersistentEntry('Change/Install/assetsVersion', $assetsVersion, \Change\Configuration\Configuration::AUTOGEN);
		$editConfig->save();

		$assetsRootPath = $rootPath . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . $assetsVersion;
		\Change\Stdlib\FileUtils::mkdir($assetsRootPath);
	}
}