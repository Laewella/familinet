<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http;

/**
 * @name \Change\Http\Controller
 */
class Controller implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	/**
	 * @var BaseResolver
	 */
	protected $actionResolver;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Application $application)
	{
		$this->setApplication($application);
	}

	/**
	 * @api
	 * @return string
	 */
	public function getApiVersion()
	{
		return '';
	}

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Http'];
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		$classes = [];
		foreach ($this->getEventManagerIdentifier() as $name)
		{
			$entry = $this->getApplication()->getConfiguredListenerClassNames('Change/Events/' . str_replace('.', '/', $name));
			if (is_array($entry))
			{
				foreach ($entry as $className)
				{
					if (is_string($className))
					{
						$classes[] = $className;
					}
				}
			}
		}
		return array_unique($classes);
	}

	/**
	 * @param BaseResolver $actionResolver
	 */
	public function setActionResolver(BaseResolver $actionResolver)
	{
		$this->actionResolver = $actionResolver;
	}

	/**
	 * @return BaseResolver
	 */
	public function getActionResolver()
	{
		if ($this->actionResolver === null)
		{
			$this->actionResolver = new BaseResolver();
		}
		return $this->actionResolver;
	}

	/**
	 * @param Request $request
	 * @throws \RuntimeException
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function handle(Request $request)
	{
		$event = $this->createEvent($request);
		try
		{
			$this->doSendRequest($event);

			if (!($event->getResult() instanceof Result))
			{
				$this->getActionResolver()->resolve($event);

				$this->doSendAction($event);

				if ($this->checkAuthorization($event))
				{
					$action = $event->getAction();
					if (is_callable($action))
					{
						$action($event);
					}
				}

				$this->doSendResult($event);
				/** @noinspection NotOptimalIfConditionsInspection */
				if (!($event->getResult() instanceof Result))
				{
					$this->notFound($event);
				}
			}
		}
		catch (\Exception $exception)
		{
			$this->doSendException($event, $exception);
		}
		catch (\Throwable $throwable)
		{
			$this->doSendThrowable($event, $throwable);
		}

		$this->doSendResponse($event);

		if ($event->getResponse() instanceof \Zend\Http\PhpEnvironment\Response)
		{
			return $event->getResponse();
		}

		return $this->getDefaultResponse($event);
	}

	/**
	 * @param Event $event
	 * @return boolean
	 */
	protected function checkAuthorization(Event $event)
	{
		$authorization = $event->getAuthorization();
		if (is_callable($authorization))
		{
			$permissionsManager = $event->getPermissionsManager();
			$this->doSendAuthenticate($event);
			if (!$permissionsManager->allow())
			{
				$user = $event->getAuthenticationManager()->getCurrentUser();
				$permissionsManager->setUser($user);
				$authorized = call_user_func($authorization, $event);
				if (!$authorized)
				{
					if ($user->authenticated())
					{
						$this->forbidden($event);
						return false;
					}
					else
					{
						$this->unauthorized($event);
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * @api
	 * @param Event $event
	 * @return Result
	 */
	public function notFound($event)
	{
		$notFound = new Result();
		$notFound->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_404);
		$event->setResult($notFound);
		return $notFound;
	}

	/**
	 * @api
	 * @param Event $event
	 * @return Result
	 */
	public function unauthorized($event)
	{
		$unauthorized = new Result();
		$unauthorized->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_401);
		$event->setResult($unauthorized);
		return $unauthorized;
	}

	/**
	 * @api
	 * @param Event $event
	 * @return Result
	 */
	public function forbidden($event)
	{
		$forbidden = new Result();
		$forbidden->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_403);
		$event->setResult($forbidden);
		return $forbidden;
	}

	/**
	 * @api
	 * @param Event $event
	 * @return Result
	 */
	public function error($event)
	{
		$error = new Result();
		$error->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_500);
		$event->setResult($error);
		return $error;
	}

	/**
	 * @param string $notAllowed
	 * @param string[] $allow
	 * @return Result
	 */
	public function notAllowedError($notAllowed, array $allow)
	{
		$result = new Result(\Zend\Http\Response::STATUS_CODE_405);
		$header = \Zend\Http\Header\Allow::fromString('allow: ' . implode(', ', $allow));
		$result->getHeaders()->addHeader($header);
		return $result;
	}

	/**
	 * @param Event $event
	 */
	protected function doSendRequest(Event $event)
	{
		$event->setName(Event::EVENT_REQUEST);
		$event->setTarget($this);

		$results = $this->getEventManager()->triggerEventUntil(function ($result)
		{
			return ($result instanceof Result);
		}, $event);

		if ($results->stopped() && ($results->last() instanceof Result))
		{
			$event->setResult($results->last());
		}
	}

	/**
	 * @param Event $event
	 */
	protected function doSendAuthenticate(Event $event)
	{
		$event->setName(Event::EVENT_AUTHENTICATE);
		$event->setTarget($this);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @param Event $event
	 */
	protected function doSendAction(Event $event)
	{
		$event->setName(Event::EVENT_ACTION);
		$event->setTarget($this);

		$results = $this->getEventManager()->triggerEventUntil(function ($result)
		{
			return ($result !== null) && is_callable($result);
		}, $event);
		$last = $results->last();
		if ($results->stopped() && ($last !== null && is_callable($last)))
		{
			$event->setAction($last);
		}
	}

	/**
	 * @param Event $event
	 */
	protected function doSendResult(Event $event)
	{
		$event->setName(Event::EVENT_RESULT);
		$event->setTarget($this);
		$results = $this->getEventManager()->triggerEventUntil(function ($result)
		{
			return ($result instanceof Result);
		}, $event);
		if ($results->stopped() && ($results->last() instanceof Result))
		{
			$event->setResult($results->last());
		}
	}

	/**
	 * @param Event $event
	 */
	protected function doSendResponse(Event $event)
	{
		try
		{
			$event->setName(Event::EVENT_RESPONSE);
			$event->setTarget($this);

			$results = $this->getEventManager()->triggerEventUntil(function ($result)
			{
				return ($result instanceof \Zend\Http\PhpEnvironment\Response);
			}, $event);
			if ($results->stopped() && ($results->last() instanceof \Zend\Http\PhpEnvironment\Response))
			{
				$event->setResponse($results->last());
			}
		}
		catch (\Exception $exception)
		{
			$this->doSendException($event, $exception);
		}
		catch (\Throwable $throwable)
		{
			$this->doSendThrowable($event, $throwable);
		}
	}

	/**
	 * @param Event $event
	 * @param \Exception $exception
	 */
	protected function doSendException($event, $exception)
	{
		try
		{
			$event->getApplication()->getLogging()->exception($exception);
			$event->setParam('Exception', $exception);
			$event->setName(Event::EVENT_EXCEPTION);
			$event->setTarget($this);
			$this->getEventManager()->triggerEvent($event);
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
		}
		catch (\Throwable $t)
		{
			$event->getApplication()->getLogging()->throwable($t);
		}
	}

	/**
	 * @param Event $event
	 * @param \Throwable $throwable
	 */
	protected function doSendThrowable($event, $throwable)
	{
		try
		{
			$event->getApplication()->getLogging()->throwable($throwable);
			$exception = new \LogicException($throwable->getMessage(), $throwable->getCode());
			$event->setParam('Exception', $exception);
			$event->setName(Event::EVENT_EXCEPTION);
			$event->setTarget($this);
			$this->getEventManager()->triggerEvent($event);
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
		}
		catch (\Throwable $t)
		{
			$event->getApplication()->getLogging()->throwable($t);
		}
	}

	/**
	 * @api
	 * @param Request $request
	 * @param Result $result
	 * @return boolean
	 */
	public function resultNotModified(Request $request, $result)
	{
		if (($result instanceof Result) && ($result->getHttpStatusCode() === \Zend\Http\Response::STATUS_CODE_200))
		{
			$etag = $result->getHeaderEtag();
			$ifNoneMatch = $request->getIfNoneMatch();
			if ($etag && $ifNoneMatch && $etag == $ifNoneMatch)
			{
				return true;
			}

			$lastModified = $result->getHeaderLastModified();
			$ifModifiedSince = $request->getIfModifiedSince();
			if ($lastModified && $ifModifiedSince && $lastModified <= $ifModifiedSince)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @api
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function createResponse()
	{
		return new \Zend\Http\PhpEnvironment\Response();
	}

	/**
	 * @param Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	protected function getDefaultResponse($event)
	{
		$result = $this->error($event);
		$response = $this->createResponse();
		$response->setStatusCode($result->getHttpStatusCode());
		$response->setHeaders($result->getHeaders());
		return $response;
	}

	/**
	 * @param Request $request
	 * @return Event
	 */
	protected function createEvent($request)
	{
		$event = new Event();
		$event->setRequest($request);

		$script = $request->getServer('SCRIPT_NAME');
		if (strpos($request->getRequestUri(), $script) !== 0)
		{
			$script = null;
		}

		$urlManager = new UrlManager($request->getUri(), $script);
		$event->setUrlManager($urlManager);
		return $event;
	}
}