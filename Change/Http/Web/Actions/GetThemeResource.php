<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Web\Actions;

/**
 * @name \Change\Http\Web\Actions\GetThemeResource
 */
class GetThemeResource
{
	/**
	 * Use Required Event Params: theme
	 * @param \Change\Http\Web\Event $event
	 */
	public function getNgTemplates($event)
	{
		$theme = $event->getParam('theme');
		$result = new \Change\Http\Web\Result\Resource('ng-templates.js');
		if ($theme instanceof \Change\Presentation\Interfaces\Theme)
		{
			$themeManager = $event->getApplicationServices()->getThemeManager();
			$themeManager->setCurrent($theme);
			$templateManager = $event->getApplicationServices()->getTemplateManager();
			$loader = $templateManager->buildThemeTwigLoader();
			$filePath = $loader->getTemplateFilePath('ng-templates.js');
			if ($filePath)
			{
				$templateManager->setTwigLoader($loader);
				$md = \Change\Stdlib\FileUtils::getModificationDate($filePath);
				$result->setHeaderLastModified($md);
				$result->getHeaders()->addHeaderLine('Cache-Control', 'public');
				$ifModifiedSince = $event->getRequest()->getIfModifiedSince();
				if ($ifModifiedSince && $ifModifiedSince == $md)
				{
					$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_304);
					$result->setRenderer(function () {return null;});
				}
				else
				{
					$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
					$result->getHeaders()->addHeaderLine('Content-Type', \Change\Stdlib\FileUtils::getMimeType($filePath));
					$result->setHeaderLastModified($md);
					$result->setRenderer(function () use ($templateManager)
					{
						return $templateManager->renderThemeTemplateFile('ng-templates.js', []);
					});
				}
				$event->setResult($result);
				return;
			}
		}
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_404);
		$result->setRenderer(function () {return null;});
		$event->setResult($result);
	}
}