<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\OAuth;

/**
 * @name \Change\Http\Rest\OAuth\ListenerAggregate
 * @ignore
 */
class ListenerAggregate extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_REQUEST, function ($event)
		{
			$l = new AuthenticationListener();
			$l->onRequest($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_RESPONSE, function ($event)
		{
			$l = new AuthenticationListener();
			$l->onResponse($event);
		}, 10);

		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_AUTHENTICATE, function (\Change\Http\Event $event)
		{
			$l = new AuthenticationListener();
			try
			{
				$l->onAuthenticate($event);
			}
			catch (\RuntimeException $e)
			{
				$event->getApplication()->getLogging()->info('Authenticate Exception', $e->getCode(), $e->getMessage());
				$event->getApplication()->getLogging()->debug($e->getTraceAsString());
				throw $e;
			}
		}, 10);
	}
}
