<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\ResourcesCode;

/**
 * @name \Change\Http\Rest\V1\ResourcesCode\ResourcesCodeResolver
 */
class ResourcesCodeResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	const RESOLVER_NAME = 'resourcescode';

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		$namespaces = [];
		$base = implode('.', $namespaceParts);
		if (count($namespaceParts) == 1)
		{
			$contextIds = $event->getApplicationServices()->getDocumentCodeManager()->getAllContextIds();
			foreach ($contextIds as $contextId)
			{
				$namespaces[] = $base . '.' . $contextId;
			}
		}
		else
		{
			foreach (['codes', 'ids'] as $name)
			{
				$namespaces[] = $base . '.' . $name;
			}
		}
		return $namespaces;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$isDirectory = $event->getParam('isDirectory');
		$nbParts = count($resourceParts);
		if ($method === \Change\Http\Rest\Request::METHOD_GET)
		{
			if (($nbParts == 0 || ($nbParts == 1 && $isDirectory)))
			{
				array_unshift($resourceParts, static::RESOLVER_NAME);
				$event->setParam('namespace', implode('.', $resourceParts));
				$event->setParam('resolver', $this);
				$action = function ($event)
				{
					$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
					$action->execute($event);
				};
				$event->setAction($action);
				$event->setAuthorization(null);
				return;
			}
			elseif ($nbParts == 1 && !$isDirectory)
			{
				$event->setParam('contextId', $resourceParts[0]);
				$event->setAction([$this, 'getContextInfo']);
				return;
			}
			elseif ($nbParts == 2)
			{
				$event->setParam('contextId', $resourceParts[0]);
				if ($resourceParts[1] == 'codes')
				{
					$documentId = $event->getRequest()->getQuery('id');
					$event->setParam('documentId', $documentId);
					$event->setAction([$this, 'getCodes']);
					return;
				}
				elseif ($resourceParts[1] == 'ids')
				{
					$code = $event->getRequest()->getQuery('code');
					$event->setParam('code', $code);
					$event->setAction([$this, 'getIds']);
					return;
				}
			}
		}
		elseif ($method === \Change\Http\Rest\Request::METHOD_POST || $method === \Change\Http\Rest\Request::METHOD_PUT)
		{
			if ($nbParts == 2)
			{
				$event->setParam('contextId', $resourceParts[0]);
				if ($resourceParts[1] == 'codes')
				{
					$request = $event->getRequest();
					$documentId = $request->getQuery('id');
					if (!$documentId)
					{
						$documentId = $request->getPost('id');
					}
					$event->setParam('documentId', $documentId);
					$event->setParam('codes', $request->getPost('codes'));
					$event->setAction([$this, 'setCodes']);
					return;
				}
				elseif ($resourceParts[1] == 'ids')
				{
					$request = $event->getRequest();
					$code = $request->getQuery('code');
					if (!$code)
					{
						$code = $request->getPost('code');
					}
					$event->setParam('code', $code);
					$event->setParam('ids', $request->getPost('ids'));
					$event->setAction([$this, 'setIds']);
					return;
				}
			}
		}
		elseif ($method === \Change\Http\Rest\Request::METHOD_DELETE)
		{
			if ($nbParts == 2)
			{
				$event->setParam('contextId', $resourceParts[0]);
				if ($resourceParts[1] == 'codes')
				{
					$documentId = $event->getRequest()->getQuery('id');
					$event->setParam('documentId', $documentId);
					$event->setAction([$this, 'deleteCodes']);
					return;
				}
				elseif ($resourceParts[1] == 'ids')
				{
					$code = $event->getRequest()->getQuery('code');
					$event->setParam('code', $code);
					$event->setAction([$this, 'deleteIds']);
					return;
				}
			}
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getContextInfo(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId = $event->getParam('contextId');
		$info = ['id' => $documentCodeManager->resolveContextId($contextId)];
		$info['name'] = $documentCodeManager->getContextById($info['id']);
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getIds(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId = $event->getParam('contextId');
		$code = $event->getParam('code');
		$info = ['contextId' => $documentCodeManager->resolveContextId($contextId)];
		$info['context'] = $documentCodeManager->getContextById($info['contextId']);
		$info['code'] = $code;
		$info['ids'] = [];
		if ($code)
		{
			$documents = $documentCodeManager->getDocumentsByCode($code, $info['contextId']);
			foreach ($documents as $document)
			{
				$documentLink = new \Change\Http\Rest\V1\Resources\DocumentLink($event->getUrlManager(), $document);
				$info['ids'][] = ['id' => $document->getId(), 'model' => $document->getDocumentModelName(),
					'link' => $documentLink->toArray()];
			}
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getCodes(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId = $event->getParam('contextId');
		$documentId = $event->getParam('documentId');
		$info = ['contextId' => $documentCodeManager->resolveContextId($contextId)];
		$info['context'] = $documentCodeManager->getContextById($info['contextId']);
		$info['id'] = $documentId;
		$info['codes'] = [];
		if (is_numeric($documentId))
		{
			$info['codes'] = $documentCodeManager->getCodesByDocument($documentId, $info['contextId']);
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function deleteIds(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId =$documentCodeManager->resolveContextId($event->getParam('contextId'));
		$code = $event->getParam('code');
		$info = ['contextId' => $contextId];
		$info['context'] = $documentCodeManager->getContextById($contextId);
		$info['code'] = $code;
		$info['ids'] = [];
		if ($code)
		{
			foreach ($documentCodeManager->getDocumentsByCode($code, $contextId) as $document)
			{
				$documentCodeManager->removeDocumentCode($document, $code, $contextId);
			}
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function deleteCodes(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId = $documentCodeManager->resolveContextId($event->getParam('contextId'));
		$documentId = $event->getParam('documentId');
		$info = ['contextId' => $contextId];
		$info['context'] = $documentCodeManager->getContextById($contextId);
		$info['id'] = $documentId;
		$info['codes'] = [];
		if (is_numeric($documentId))
		{
			foreach ($documentCodeManager->getCodesByDocument($documentId, $contextId) as $code)
			{
				$documentCodeManager->removeDocumentCode($documentId, $code, $contextId);
			}
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function setIds(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId =$documentCodeManager->resolveContextId($event->getParam('contextId'));
		$code = $event->getParam('code');
		$info = ['contextId' => $contextId];
		$info['context'] = $documentCodeManager->getContextById($contextId);
		$info['code'] = $code;
		$info['ids'] = [];
		if ($code)
		{
			$newIds = $event->getParam('ids');
			$newIds = array_filter(array_unique($newIds));
			$finalIds = [];

			// Remove existing ids for code.
			foreach ($documentCodeManager->getDocumentsByCode($code, $contextId) as $document)
			{
				if (!in_array($document->getId(), $newIds))
				{
					$documentCodeManager->removeDocumentCode($document, $code, $contextId);
				}
				else
				{
					$finalIds[] = $document->getId();
				}
			}

			// Add new ids.
			foreach ($newIds as $id)
			{
				if (!in_array($id, $finalIds))
				{
					$result = $documentCodeManager->addDocumentCode($id, $code, $contextId);
					if ($result)
					{
						$finalIds[] = (int)$id;
					}
				}
			}
			$info['ids'] = $finalIds;
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function setCodes(\Change\Http\Event $event)
	{
		$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
		$contextId = $event->getParam('contextId');
		$documentId = (int)$event->getParam('documentId');
		$info = ['contextId' => false, 'context' => '', 'id' => $documentId, 'codes' => []];
		if ($documentId)
		{
			$newCodes = $event->getParam('codes');
			$newCodes = array_filter(array_unique($newCodes));
			$finalCodes = [];

			// Remove existing codes for id.
			foreach ($documentCodeManager->getCodesByDocument($documentId, $contextId) as $code)
			{
				if (!in_array($code, $newCodes))
				{
					$documentCodeManager->removeDocumentCode($documentId, $code, $contextId);
				}
				else
				{
					$finalCodes[] = $code;
				}
			}

			// Add new Codes.
			foreach ($newCodes as $code)
			{
				if (!in_array($code, $finalCodes))
				{
					$result = $documentCodeManager->addDocumentCode($documentId, $code, $contextId);
					if ($result)
					{
						$finalCodes[] = $code;
					}
				}
			}
			$info['codes'] = $finalCodes;

			if ($finalCodes)
			{
				$info['contextId'] = $documentCodeManager->resolveContextId($contextId);
				$info['context'] = $documentCodeManager->getContextById($info['contextId']);
			}
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($info);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}
}