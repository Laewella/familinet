<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Synchronization;

/**
* @name \Change\Http\Rest\V1\Synchronization\Import
*/
class Import
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute($event)
	{
		$request = $event->getRequest();
		$post = $request->getPost()->toArray();
		if (!is_array($post) || !isset($post['data']) || !is_array($post['data']))
		{
			$errorResult = new \Change\Http\Rest\V1\ErrorResult('INVALID-DATA', 'Invalid import data',
				\Zend\Http\Response::STATUS_CODE_409);
			$event->setResult($errorResult);
			return;
		}
		$JSON = $post['data'];
		$basePath = $request->getQuery('basePath');

		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		$contextCode = $request->getQuery('context');

		$importEngine = new \Change\Synchronization\ImportEngine($event->getApplication(),
			['contextCode' => $contextCode ?: 'Synchronization', 'basePath' => $basePath ?: null]);

		$items = [];
		$index = -1;
		$verbose = $request->getQuery('verbose', false);
		foreach(array_chunk($JSON, 10) as $itemsData)
		{
			try
			{
				$transactionManager->begin();
				foreach ($itemsData as $itemData)
				{
					$index++;
					try
					{
						$item = $importEngine->importItem($itemData);
						if ($verbose)
						{
							$items[] = '(' . $itemData['__model'] . ' ' . $itemData['__id'] . ') [' . $item . ']';
						}
					}
					catch (\Change\Synchronization\ImportException $importException)
					{
						$items[] = $importException->getImportError(is_array($itemData) ? $itemData : null);
					}
				}
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$transactionManager->rollBack($e);
				$message = '( Exception at index ' . $index . ') : ' . $e->getMessage();
				$errorResult = new \Change\Http\Rest\V1\ErrorResult($e->getCode(), $message,
					\Zend\Http\Response::STATUS_CODE_500);
				$event->setResult($errorResult);
				return;
			}
		}
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($items);
		$event->setResult($result);
	}
}