<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Jobs;

/**
 * @name \Change\Http\Rest\V1\Jobs\JobsResolver
 */
class JobsResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	const RESOLVER_NAME = 'jobs';

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		$namespaces = [];
		$names = [\Change\Job\JobInterface::STATUS_WAITING, \Change\Job\JobInterface::STATUS_RUNNING,
			\Change\Job\JobInterface::STATUS_SUCCESS, \Change\Job\JobInterface::STATUS_FAILED];

		$base = implode('.', $namespaceParts);
		foreach ($names as $name)
		{
			$namespaces[] = $base . '.' . $name;
		}
		return $namespaces;
	}

	/**
	 * Set Event params: resourcesActionName, documentId, LCID
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$nbParts = count($resourceParts);
		if ($method === \Change\Http\Rest\Request::METHOD_GET)
		{
			if ($nbParts == 0)
			{
				array_unshift($resourceParts, static::RESOLVER_NAME);
				$event->setParam('namespace', implode('.', $resourceParts));
				$event->setParam('resolver', $this);
				$action = function ($event)
				{
					$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
					$action->execute($event);
				};
				$event->setAction($action);
				$event->setAuthorization(null);
				return;
			}
			elseif ($nbParts == 1)
			{
				if ($event->getParam('isDirectory'))
				{
					$status = $resourceParts[0];
					$statues = [\Change\Job\JobInterface::STATUS_WAITING, \Change\Job\JobInterface::STATUS_RUNNING,
						\Change\Job\JobInterface::STATUS_SUCCESS, \Change\Job\JobInterface::STATUS_FAILED];
					if (in_array($status, $statues))
					{
						$event->setParam('status', $status);
						$event->setAction([$this, 'getJobCollection']);
					}
				}
				else
				{
					$jobId = $resourceParts[0];
					if (is_numeric($jobId))
					{
						$event->setParam('jobId', (int)$jobId);
						$event->setAction([$this, 'getJob']);
					}
				}
				return;
			}
			elseif ($nbParts == 2)
			{
				list($jobId, $run) = $resourceParts;
				if (is_numeric($jobId) && $run === 'run')
				{
					$event->setParam('jobId', (int)$jobId);
					$event->setAction([$this, 'runJob']);
				}
				return;
			}
		}
		elseif ($method === \Change\Http\Rest\Request::METHOD_DELETE && $nbParts == 1)
		{
			$jobId = $resourceParts[0];
			if (is_numeric($jobId))
			{
				$event->setParam('jobId', (int)$jobId);
				$event->setAction([$this, 'deleteJob']);
			}
			return;
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function runJob(\Change\Http\Event $event)
	{
		$jobId = $event->getParam('jobId');
		$jobManager = $event->getApplicationServices()->getJobManager();

		$job = $jobManager->getJob($jobId);
		if ($job)
		{
			$jobStatus = $job->getStatus();
			if ($jobStatus === \Change\Job\JobInterface::STATUS_WAITING || $jobStatus === \Change\Job\JobInterface::STATUS_FAILED)
			{
				$jobManager->setTransactionManager($event->getApplicationServices()->getTransactionManager());
				$jobManager->run($job);
				$this->getJob($event);
			}
			else
			{
				$errorCode = 999999;
				$errorMessage =
					'Invalid job status "' . $jobStatus . '", "' . \Change\Job\JobInterface::STATUS_WAITING . '" expected';
				$httpStatusCode = \Zend\Http\Response::STATUS_CODE_409;
				$result = new \Change\Http\Rest\V1\ErrorResult($errorCode, $errorMessage, $httpStatusCode);
				$event->setResult($result);
			}
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function deleteJob(\Change\Http\Event $event)
	{
		$jobId = $event->getParam('jobId');
		$jobManager = $event->getApplicationServices()->getJobManager();
		$job = $jobManager->getJob($jobId);
		if ($job)
		{
			$jobStatus = $job->getStatus();
			if ($jobStatus === \Change\Job\JobInterface::STATUS_SUCCESS || $jobStatus === \Change\Job\JobInterface::STATUS_FAILED)
			{
				$jobManager->setTransactionManager($event->getApplicationServices()->getTransactionManager());
				$jobManager->deleteJob($job);
				$job = $jobManager->getJob($jobId);
			}
			else
			{
				$errorCode = 999999;
				$errorMessage = 'Invalid job status "' . $job->getStatus() . '", "' .
					\Change\Job\JobInterface::STATUS_SUCCESS . '" or "' .
					\Change\Job\JobInterface::STATUS_FAILED . '" expected';
				$httpStatusCode = \Zend\Http\Response::STATUS_CODE_409;
				$result = new \Change\Http\Rest\V1\ErrorResult($errorCode, $errorMessage, $httpStatusCode);
				$event->setResult($result);
				return;
			}
		}

		if (!$job)
		{
			$result = new \Change\Http\Result();
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_204);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getJob(\Change\Http\Event $event)
	{
		$urlManager = $event->getUrlManager();
		$jobId = $event->getParam('jobId');
		$jobManager = $event->getApplicationServices()->getJobManager();
		$job = $jobManager->getJob($jobId);

		if ($job)
		{
			$result = new JobResult($urlManager);
			$result->addLink(new \Change\Http\Rest\V1\Link($urlManager, static::RESOLVER_NAME . '/' . $job->getId()));
			$result->setProperty('id', $job->getId());
			$result->setProperty('name', $job->getName());
			$result->setProperty('startDate', $job->getStartDate()->format(\DateTime::ATOM));
			$result->setProperty('status', $job->getStatus());
			$lastModDate = $job->getLastModificationDate();
			if ($lastModDate)
			{
				$result->setProperty('lastModificationDate', $lastModDate->format(\DateTime::ATOM));
			}
			$result->setProperty('arguments', $job->getArguments());

			if ($job->getStatus() === \Change\Job\JobInterface::STATUS_WAITING)
			{
				$result->addLink(new \Change\Http\Rest\V1\Link($urlManager, static::RESOLVER_NAME . '/' . $job->getId() . '/run', 'run'));
			}
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getJobCollection(\Change\Http\Event $event)
	{
		$status = $event->getParam('status');
		$jobManager = $event->getApplicationServices()->getJobManager();

		$count = $jobManager->getCountJobIds($status);

		$urlManager = $event->getUrlManager();
		$result = new \Change\Http\Rest\V1\CollectionResult();
		$result->setCount($count);
		$result->setSort($event->getRequest()->getQuery('sort', 'start_date'));
		$result->setDesc($event->getRequest()->getQuery('desc') !== 'false');

		if (($offset = $event->getRequest()->getQuery('offset')) !== null)
		{
			$result->setOffset((int)$offset);
		}
		if (($limit = $event->getRequest()->getQuery('limit')) !== null)
		{
			$result->setLimit((int)$limit);
		}

		$selfLink = new \Change\Http\Rest\V1\Link($urlManager, $event->getRequest()->getPath());
		$selfLink->setQuery($this->buildQueryArray($result));
		$result->addLink($selfLink);

		if ($count)
		{
			$ids = $jobManager->getJobIds($status, $result->getOffset(), $result->getLimit(), $result->getSort(),
				$result->getDesc());
			foreach ($ids as $id)
			{
				$job = $jobManager->getJob($id);
				if ($job)
				{
					$row = new JobResult($urlManager);
					$row->addLink(new \Change\Http\Rest\V1\Link($urlManager, static::RESOLVER_NAME . '/' . $job->getId()));
					$row->setProperty('id', $job->getId());
					$row->setProperty('name', $job->getName());
					$row->setProperty('startDate', $job->getStartDate()->format(\DateTime::ATOM));
					$row->setProperty('status', $job->getStatus());
					$row->setProperty('arguments', $job->getArguments());
					$result->addResource($row);
				}
			}
		}
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Rest\V1\CollectionResult $result
	 * @return array
	 */
	protected function buildQueryArray($result)
	{
		$array = ['limit' => $result->getLimit(), 'offset' => $result->getOffset()];
		if ($result->getSort())
		{
			$array['sort'] = $result->getSort();
			$array['desc'] = ($result->getDesc()) ? 'true' : 'false';
		}
		return $array;
	}
}