<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
 * @name \Change\Storage\Engines\FtpStorage
 */
class FtpStorage extends AbstractStorage
{
	/**
	 * @var string
	 */
	protected $dsn;

	/**
	 * @var resource
	 */
	protected $resource;

	/**
	 * @return string
	 */
	public function getDsn()
	{
		return $this->dsn;
	}

	/**
	 * @param string $dsn
	 * @return $this
	 */
	public function setDsn($dsn)
	{
		$this->dsn = rtrim((string)$dsn, '/');
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getMimeType()
	{
		$mimeType = null;
		$parsedUrl = $this->parsedURL;
		if ($parsedUrl && isset($parsedUrl['path']) && $parsedUrl['path'])
		{
			$cacheFileName = $this->getLocalCacheFileName($parsedUrl['path']);
			$mimeType = $this->getMimeTypeForLocalFile($cacheFileName);
			@unlink($cacheFileName);
		}
		return $mimeType;
	}

	/**
	 * @param $path
	 * @return string
	 */
	protected function getFtpName($path)
	{
		return $this->getDsn() . $path;
	}

	/**
	 * @param $path
	 * @return string
	 */
	protected function buildLocalFileName($path)
	{
		$workspace = $this->getStorageManager()->getWorkspace();
		$cacheFileName = $workspace->tmpPath('ftpStorage', ltrim($path, '/'));
		\Change\Stdlib\FileUtils::mkdir(dirname($cacheFileName));
		return $cacheFileName;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	protected function getLocalCacheFileName($path)
	{
		$mediaName = substr($path, 1);
		$cacheFileName = $this->buildLocalFileName($mediaName);
		@copy($this->getFtpName($path), $cacheFileName);
		return $cacheFileName;
	}

	/**
	 * @param string $fileName
	 * @return string
	 */
	protected function getMimeTypeForLocalFile($fileName)
	{
		if (is_readable($fileName) && class_exists('finfo', false))
		{
			$fi = new \finfo(FILEINFO_MIME_TYPE);
			$mimeType = $fi->file($fileName);
			if ($mimeType)
			{
				return $mimeType;
			}
		}
		return 'application/octet-stream';
	}

	/**
	 * @return string|null
	 */
	public function getPublicURL()
	{
		return null;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	public function normalizePath($path)
	{
		$pathParts = [];
		$oldParts = explode('/', \Change\Stdlib\StringUtils::stripAccents(str_replace(DIRECTORY_SEPARATOR, '/', $path)));
		foreach ($oldParts as $part)
		{
			if ($part !== '.' && $part !== '..' && $part !== '')
			{
				$pathParts[] = preg_replace('#[^a-zA-Z0-9.]+#', '_', $part);
			}
		}
		return implode('/', $pathParts);
	}

	/**
	 * @param string $mode
	 * @param integer $options
	 * @param string $opened_path
	 * @param resource $context
	 * @return boolean
	 */
	public function stream_open($mode, $options, &$opened_path, &$context)
	{
		if ($mode && substr($mode, 0, 1) === 'w')
		{
			$opts = ['ftp' => ['overwrite' => true]];
			$context = stream_context_create($opts);
		}
		$fileName = $this->getFtpName($this->parsedURL['path']);
		$this->resource = @fopen($fileName, $mode, false, $context);
		return is_resource($this->resource);
	}

	/**
	 * @param integer $count
	 * @return string
	 */
	public function stream_read($count)
	{
		return fread($this->resource, $count);
	}

	/**
	 * @param   string $data
	 * @return  integer
	 */
	public function stream_write($data)
	{
		return fwrite($this->resource, $data);
	}

	/**
	 * @return void
	 */
	public function stream_close()
	{
		fclose($this->resource);
		unset($this->resource);
	}

	/**
	 * @return array
	 */
	public function stream_stat()
	{
		return fstat($this->resource);
	}

	/**
	 * @return array
	 */
	public function stream_flush()
	{
		return fflush($this->resource);
	}

	/**
	 * @return array
	 */
	public function stream_eof()
	{
		return feof($this->resource);
	}

	/**
	 * @param $offset
	 * @param int $whence
	 * @return array|int
	 */
	public function stream_seek($offset, $whence = SEEK_SET)
	{
		return false;
	}

	/**
	 * @param integer $flags
	 * @return array mixed
	 */
	public function url_stat($flags)
	{
		$path = isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '/';
		$filename = $this->getFtpName($path);
		if ((STREAM_URL_STAT_QUIET & $flags) === STREAM_URL_STAT_QUIET && !file_exists($filename))
		{
			return false;
		}
		return stat($filename);
	}

	/**
	 * @param integer $options
	 * @return boolean
	 */
	public function dir_opendir($options)
	{
		$path = isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '/';
		$dirName = $this->getFtpName($path);
		$this->resource = @opendir($dirName);
		return is_resource($this->resource);
	}

	/**
	 * @return  string|false
	 */
	public function dir_readdir()
	{
		return readdir($this->resource);
	}

	/**
	 * @return boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_rewinddir()
	{
		rewinddir($this->resource);
		return true;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_closedir()
	{
		closedir($this->resource);
		$this->resource = null;
		return true;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function unlink()
	{
		$path = isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '/';
		$filename = $this->getFtpName($path);
		return @unlink($filename);
	}

	/**
	 * @param   integer $mode The value passed to {@see mkdir()}.
	 * @param   integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return  boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function mkdir($mode, $options)
	{
		$path = rtrim(isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '', '/');
		if (!$path)
		{
			return false;
		}

		$recursive = (STREAM_MKDIR_RECURSIVE & $options) === STREAM_MKDIR_RECURSIVE;
		if ($recursive)
		{
			$parts = explode('/', $path);
			$countPart = count($parts);
			if ($countPart > 2)
			{
				for ($i = 2; $i < $countPart; $i++)
				{
					$subPath = $this->getFtpName(implode('/', array_slice($parts, 0, $i)));
					if (is_dir($subPath))
					{
						continue;
					}
					if (!@mkdir($subPath, $mode))
					{
						return false;
					}
				}
			}
		}

		$filename = $this->getFtpName($path);
		return @mkdir($filename, $mode);
	}

	/**
	 * @param string $pathTo The URL which the $path_from should be renamed to.
	 * @throws \RuntimeException
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function rename($pathTo)
	{
		$path = isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '/';
		$fromFileName = $this->getFtpName($path);
		if (!file_exists($fromFileName))
		{
			throw new \RuntimeException('Invalid From file', 999999);
		}

		$toItem = $this->getStorageManager()->getItemInfo($pathTo);
		if ($toItem === null)
		{
			throw new \RuntimeException('Invalid Storage', 999999);
		}

		if ($toItem->getStorageEngine()->getName() === $this->getName())
		{
			$parsedUrlTo = $toItem->getStorageEngine()->getParsedURL();
			$toFileName = $this->getFtpName($parsedUrlTo['path']);
			return @rename($fromFileName, $toFileName);
		}

		if (is_dir($fromFileName))
		{
			throw new \RuntimeException('Invalid From directory', 999999);
		}

		if (file_exists($pathTo))
		{
			throw new \RuntimeException('Destination already exist', 999999);
		}

		if (@copy($fromFileName, $pathTo))
		{
			if (@unlink($fromFileName))
			{
				return true;
			}
			else
			{
				@unlink($pathTo);
				return false;
			}
		}
		return false;
	}

	/**
	 * @param integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function rmdir($options)
	{
		$path = isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '/';
		$filename = $this->getFtpName($path);
		return @rmdir($filename);
	}

	/**
	 * @param   integer $option One of:
	 *                                  STREAM_META_TOUCH (The method was called in response to touch())
	 *                                  STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
	 *                                  STREAM_META_OWNER (The method was called in response to chown())
	 *                                  STREAM_META_GROUP_NAME (The method was called in response to chgrp())
	 *                                  STREAM_META_GROUP (The method was called in response to chgrp())
	 *                                  STREAM_META_ACCESS (The method was called in response to chmod())
	 * @param   array $var If option is
	 *                                  PHP_STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
	 *                                  PHP_STREAM_META_OWNER_NAME or PHP_STREAM_META_GROUP_NAME: The name of the owner
	 *                                      user/group as string.
	 *                                  PHP_STREAM_META_OWNER or PHP_STREAM_META_GROUP: The value owner user/group argument as integer.
	 *                                  PHP_STREAM_META_ACCESS: The argument of the chmod() as integer.
	 * @return  boolean             Returns TRUE on success or FALSE on failure. If option is not implemented, FALSE should be returned.
	 */
	public function stream_metadata($option, $var)
	{
		if ($option === STREAM_META_TOUCH)
		{
			$path = isset($this->parsedURL['path']) ? $this->parsedURL['path'] : '/';
			$filename = $filename = $this->getFtpName($path);
			return @touch($filename, isset($var[0]) ? $var[0] : null, isset($var[1]) ? $var[1] : null);
		}
		return false;
	}

	/**
	 * @return  integer     Should return the current position of the stream.
	 */
	public function stream_tell()
	{
		return ftell($this->resource);
	}

	/**
	 * @param integer $new_size
	 * @return boolean Returns TRUE on success or FALSE on failure.
	 */
	public function stream_truncate($new_size)
	{
		return false;
	}
}