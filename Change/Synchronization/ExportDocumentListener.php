<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ExportDocumentListener
 * @ignore
 */
class ExportDocumentListener extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * @var \Change\Synchronization\ExportEngine
	 */
	protected $exportEngine;

	/**
	 * @param \Change\Synchronization\ExportEngine $exportEngine
	 */
	public function __construct(\Change\Synchronization\ExportEngine $exportEngine)
	{
		$this->exportEngine = $exportEngine;
	}

	/**
	 * @return \Change\Synchronization\ExportEngine
	 */
	public function getExportEngine()
	{
		return $this->exportEngine;
	}

	/**
	 * @param \Change\Synchronization\ExportEngine $exportEngine
	 * @return $this
	 */
	public function setExportEngine(\Change\Synchronization\ExportEngine $exportEngine)
	{
		$this->exportEngine = $exportEngine;
		return $this;
	}

	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Synchronization\ExportEngine::EVENT_RESOLVE, function(ExportEvent $event) {
			$this->onResolveDocument($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Synchronization\ExportEngine::EVENT_POPULATE, function(ExportEvent $event) {
			$this->onPopulateDocument($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Synchronization\ExportEngine::EVENT_CHUNK, function(ExportEvent $event) {
			$this->onChunkDocument($event);
		}, 5);
	}

	/**
	 * @param ExportEvent $event
	 */
	protected function onResolveDocument(ExportEvent $event)
	{
		$exportData = $event->getExportData();
		if ($exportData->offsetExists('__id'))
		{
			return;
		}

		$exportEngine = $this->getExportEngine();
		$applicationServices = $event->getApplicationServices();

		/** @var \Change\Documents\AbstractDocument|int $document */
		$document = $event->getItem();
		if (is_int($document))
		{
			$document = $applicationServices->getDocumentManager()->getDocumentInstance($document);
			if ($document)
			{
				$event->setParam('item', $document);
			}
		}

		if (!$document instanceof \Change\Documents\AbstractDocument)
		{
			return;
		}

		$model = $document->getDocumentModelName();
		if ($model !== $event->getParam('expectedModel'))
		{
			$exportData->set('__model', $model);
		}
		$code = $exportEngine->getNewCode($document->getId());
		if ($code)
		{
			$exportData->set('__id', $code);
			return;
		}
		$codes = $event->getApplicationServices()
			->getDocumentCodeManager()->getCodesByDocument($document, $exportEngine->getContextCode());

		$code = array_shift($codes);
		if ($code)
		{
			$exportData->set('__id', $code);
			return;
		}

		$property = $document->getDocumentModel()->getProperty('code');
		if ($property && !$property->getLocalized() && !$property->getStateless())
		{
			$code = $property->getValue($document);
			if ($code)
			{
				$exportData->set('__id', $code);
				return;
			}
		}

		$exportData->set('__id', '__REF_ID_' . $document->getId());
	}

	/**
	 * @param ExportEvent $event
	 */
	protected function onPopulateDocument(ExportEvent $event)
	{
		/** @var \Change\Documents\AbstractDocument|\Change\Documents\Interfaces\Localizable $item */
		$item = $event->getItem();
		if (!($item instanceof \Change\Documents\AbstractDocument) || $event->getParam('populated', false))
		{
			return;
		}

		$excludedPropertyNames = array_count_values((array)$event->getParam('excludedPropertyNames', []));
		$excludedPropertyNames = array_merge($excludedPropertyNames, [
			'id' => 1, 'model' => 1, 'LCID' => 1,
			'modificationDate' => 1, 'authorName' => 1, 'documentVersion' => 1
		]);

		$exportData = $event->getExportData();
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$model = $item->getDocumentModel();
		$refLCID = ($model->isLocalized()) ? $model->getPropertyValue($item, 'refLCID') : null;
		if ($refLCID)
		{
			try
			{
				$documentManager->pushLCID($refLCID);

				$this->populateProperties($item, $exportData, $excludedPropertyNames);

				$LCIDArray = [];
				foreach ($item->getLCIDArray() as $LCID)
				{
					if ($LCID === $refLCID)
					{
						continue;
					}

					$itemLCIDData = [];
					try
					{
						$documentManager->pushLCID($LCID);
						$this->populateLocalizedProperties($item, $itemLCIDData, $excludedPropertyNames);
						if ($itemLCIDData)
						{
							$LCIDArray[$LCID] = $itemLCIDData;
						}
						$documentManager->popLCID();
					}
					catch (\Exception $e)
					{
						$documentManager->popLCID($e);
					}
				}

				if ($LCIDArray)
				{
					$exportData->set('LCID', $LCIDArray);
				}

				$documentManager->popLCID();
			}
			catch (\Exception $e)
			{
				$documentManager->popLCID($e);
			}
		}
		else
		{
			$this->populateProperties($item, $exportData, $excludedPropertyNames);
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $item
	 * @param \Zend\Stdlib\Parameters $exportData
	 * @param array $excludedPropertyNames
	 */
	protected function populateProperties($item, $exportData, $excludedPropertyNames)
	{
		$model = $item->getDocumentModel();

		foreach ($model->getProperties() as $property)
		{
			$name = $property->getName();
			if (array_key_exists($name, $excludedPropertyNames) || $property->getInternal())
			{
				continue;
			}

			if ($this->populateModelProperty($item, $property, $jsonValue))
			{
				$exportData->set($property->getName(), $jsonValue);
			}
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $item
	 * @param array $itemLCIDData
	 * @param array $excludedPropertyNames
	 */
	protected function populateLocalizedProperties($item, &$itemLCIDData, $excludedPropertyNames)
	{
		$model = $item->getDocumentModel();
		foreach ($model->getProperties() as $property)
		{
			$name = $property->getName();
			if (array_key_exists($name, $excludedPropertyNames) || !$property->getLocalized() || $property->getInternal())
			{
				continue;
			}

			if ($this->populateModelProperty($item, $property, $jsonValue))
			{
				$itemLCIDData[$property->getName()] = $jsonValue;
			}
		}
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractDocument $document
	 * @param \Change\Documents\Property $property
	 * @param mixed $jsonValue
	 * @return boolean
	 */
	public function populateModelProperty(\Change\Documents\AbstractDocument $document,
		\Change\Documents\Property $property, &$jsonValue)
	{
		$jsonValue = null;

		$value = $property->getValue($document);

		switch ($property->getType())
		{
			case \Change\Documents\Property::TYPE_BOOLEAN:
			case \Change\Documents\Property::TYPE_INTEGER:
			case \Change\Documents\Property::TYPE_STRING:
			case \Change\Documents\Property::TYPE_STORAGEURI:
			case \Change\Documents\Property::TYPE_FLOAT:
			case \Change\Documents\Property::TYPE_DECIMAL:
			case \Change\Documents\Property::TYPE_LONGSTRING:
			case \Change\Documents\Property::TYPE_LOB:
			case \Change\Documents\Property::TYPE_JSON:
				$jsonValue = $value;
				return true;
			case \Change\Documents\Property::TYPE_RICHTEXT:
				if ($value instanceof \Change\Documents\RichtextProperty && !$value->isEmpty())
				{
					$jsonValue = $value->toArray();
				}
				return true;
			case \Change\Documents\Property::TYPE_DATE:
				if ($value instanceof \DateTime)
				{
					$jsonValue = $value->format('Y-m-d');
				}
				return true;
			case \Change\Documents\Property::TYPE_DATETIME:
				if ($value instanceof \DateTime)
				{
					$jsonValue = $value->format(\DateTime::ATOM);
				}
				return true;
			case \Change\Documents\Property::TYPE_DOCUMENTID:
				$name = $property->getName();
				if (strlen($name) > 2 && substr($name, -2) === 'Id')
				{
					$name = substr($name, 0, strlen($name) - 2);
					$property->setName($name);
				}

				if ($value)
				{
					$jsonValue = $this->exportEngine->resolveItem((int)$value, $property->getDocumentType());
				}
				return true;
			case \Change\Documents\Property::TYPE_DOCUMENT:
				if ($value)
				{
					$jsonValue = $this->exportEngine->resolveItem($value, $property->getDocumentType());
				}
				return true;
			case \Change\Documents\Property::TYPE_DOCUMENTARRAY:
				if ($value instanceof \Change\Documents\DocumentArrayProperty)
				{
					$arrayItemType = $property->getDocumentType();
					$jsonValue = [];
					foreach ($value as $arrayItem)
					{
						$jsonValueArrayItem = $this->exportEngine->resolveItem($arrayItem, $arrayItemType);
						if ($jsonValueArrayItem)
						{
							$jsonValue[] = $jsonValueArrayItem;
						}
					}
				}
				return true;
			case \Change\Documents\Property::TYPE_OBJECT:
				if (is_object($value))
				{
					if (is_callable([$value, 'toArray']))
					{
						$arrayValue = call_user_func([$value, 'toArray']);
						if (is_array($arrayValue))
						{
							$jsonValue = $value;
							return true;
						}
					}
					return false;
				}
				elseif ($value === null || is_array($value))
				{
					$jsonValue = $value;
					return true;
				}
				return false;
			case \Change\Documents\Property::TYPE_INLINE:
			case \Change\Documents\Property::TYPE_INLINEARRAY:
				return false;
		}
		return false;
	}


	/**
	 * Event input params: model, chunk, filter.ids, filter.offset, filter.firstId
	 * Event output params: items
	 * @param \Change\Synchronization\ExportEvent  $event
	 */
	public function onChunkDocument(\Change\Synchronization\ExportEvent $event)
	{
		if ($event->getParam('items') !== null)
		{
			return;
		}

		$modelName = $event->getParam('model');
		$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
		if ($model && !$model->isInline() && !$model->isStateless())
		{
			$filter = $event->getParam('filter') + ['ids' => null, 'offset' => 0, 'firstId' => null];
			$firstId = (int)$filter['firstId'];
			$offset = (int)$filter['offset'];
			$codes = $filter['ids'];

			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$query = $documentManager->getNewQuery($model)->addOrder('id', false);

			if ($firstId)
			{
				$query->andPredicates($query->lte('id', $firstId));
			}

			if (is_string($codes))
			{
				$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
				$inArgs = [];
				foreach (explode(',', $codes) as $code)
				{
					$code = trim((string)$code);
					if ($code)
					{
						$id = $this->getDocumentId($code, $model, $documentCodeManager, $documentManager);
						if ($id)
						{
							$inArgs[] = $id;
						}
					}
				}

				if ($inArgs)
				{
					$filter['ids'] = $inArgs;
				}
				else
				{
					$filter['ids'] = false;
				}

				$codes = $filter['ids'];
			}

			if (is_array($codes))
			{
				$query->andPredicates($query->in('id', $codes));
			}

			$ids = $query->getDocumentIds($offset, (int)$event->getParam('chunk'));
			if (!$firstId && $ids)
			{
				$filter['firstId'] = $ids[0];
			}
			$filter['offset'] += count($ids);

			$event->setParam('filter', $filter);
			$event->setParam('items', $ids);
		}
	}

	/**
	 * @param string $code
	 * @param \Change\Documents\AbstractModel $model
	 * @param \Change\Documents\DocumentCodeManager $documentCodeManager
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return integer
	 */
	protected function getDocumentId($code, $model, $documentCodeManager, $documentManager)
	{
		$doc = $documentCodeManager->getFirstDocumentByCode($code, $this->exportEngine->getContextCode(), $model);
		if ($doc)
		{
			return $doc->getId();
		}

		if (strpos($code, '__REF_ID_') === 0)
		{
			$docId = substr($code, 9);
			if (is_numeric($docId))
			{
				return (int)$docId;
			}
		}

		$property = $model->getProperty('code');
		if ($property && !($property->getLocalized() || $property->getStateless()))
		{
			$q = $documentManager->getNewQuery($model);
			$doc = $q->andPredicates($q->eq('code', $code))->addOrder('id')->getFirstDocument();
			if ($doc)
			{
				return $doc->getId();
			}
		}

		return 0;
	}
}