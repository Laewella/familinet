<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization\CSV;

/**
 * @name \Change\Synchronization\CSV\CSVEngine
 */
class CSVEngine implements \Zend\EventManager\EventsCapableInterface
{

	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'CSVEngine';

	const EVENT_AGGREGATE_FILES = 'aggregateFiles';
	const EVENT_PRE_IMPORT_FILE = 'preImportFile';
	const EVENT_POST_IMPORT_FILE = 'postImportFile';
	const EVENT_VALIDATE_LINE = 'validateLine';
	const EVENT_IMPORT = 'importValidatedLines';
	const EVENT_RESOLVE_TYPE = 'resolveImportType';
	const EVENT_ADD_TYPOLOGY = 'addTypology';
	const EVENT_GET_FILES = 'getFiles';
	const EVENT_RESOLVE_DOCUMENT = 'resolveDocument';
	const EVENT_POPULATE = 'populate';
	const EVENT_BUILD_VALIDATORS = 'buildValidators';
	const EVENT_GET_SORTED_IMPORT = 'getSortedImport';
	const EVENT_PRE_TREATMENT = 'preTreatment';

	/**
	 * @var \Change\Storage\StorageManager
	 */
	protected $storageManager;

	/**
	 * @var string $additionalFilePart
	 */
	protected $additionalFilePart = '/';

	/**
	 * @var \Change\Logging\Logging
	 */
	protected $logging;

	/**
	 * @var \Zend\Log\Logger
	 */
	protected $errorLogger;

	/**
	 * @var \Zend\Log\Logger
	 */
	protected $validationLogger;

	/**
	 * @var \Change\Workspace
	 */
	protected $workspace;

	/**
	 * @var \Change\Configuration\Configuration
	 */
	protected $configuration;

	/**
	 * @var string
	 */
	protected $loggerName = 'import';

	/**
	 * @var string
	 */
	protected $logFile;

	/**
	 * @var integer[] $loadedDocuments
	 */
	protected $loadedDocuments = [];

	/**
	 * @var [string[]]
	 */
	protected $errorStack = [];

	/**
	 * CSVEngine constructor.
	 * @param \Change\Application $application
	 * @param array $config
	 */
	public function __construct(\Change\Application $application, array $config = [])
	{
		$this->setApplication($application);
		if (is_array($config) && $config)
		{
			$this->setConfig($config);
		}
	}

	/**
	 * @param array $config
	 * @return $this
	 */
	protected function setConfig(array $config = [])
	{
		foreach ($config as $key => $value)
		{
			$callback = [$this, 'set' . ucfirst($key)];
			if (is_callable($callback))
			{
				call_user_func($callback, $value);
			}
		}
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return array
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/CSVEngine');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		(new \Change\Synchronization\CSV\CSVImportListeners($this))->attach($eventManager);
	}

	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @return $this
	 */
	protected function setStorageManager($storageManager)
	{
		$this->storageManager = $storageManager;
		return $this;
	}

	/**
	 * @return \Change\Storage\StorageManager
	 */
	protected function getStorageManager()
	{
		if (!$this->storageManager)
		{
			$this->storageManager = new \Change\Storage\StorageManager();
			$this->storageManager->setApplication($this->getApplication());
		}
		return $this->storageManager;
	}

	/**
	 * @param string $part
	 * @return $this
	 */
	public function setAdditionalFilePart($part)
	{
		if (substr($part, 0, 1) != '/')
		{
			$part = '/' . $part;
		}
		if (substr($part, -1, 1) != '/')
		{
			$part = $part . '/';
		}
		$this->additionalFilePart = $part;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAdditionalFilePart()
	{
		return $this->additionalFilePart;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param string $code
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function getLoadedDocument($documentManager, $code)
	{
		return isset($this->loadedDocuments[$code]) ? $documentManager->getDocumentInstance($this->loadedDocuments[$code]) : null;
	}

	/**
	 * @param string $code
	 * @param \Change\Documents\AbstractDocument $document
	 * @return $this
	 */
	public function pushLoadedDocument($code, $document)
	{
		$this->loadedDocuments[$code] = $document->getId();
		return $this;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setLoggerName($name = 'validation')
	{
		$this->errorLogger = null;
		$this->loggerName = $name;
		$this->logFile = null;
		return $this;
	}

	/**
	 * @return \Zend\Log\Logger
	 */
	public function getErrorLogger()
	{
		if (!$this->errorLogger)
		{
			$logger = new \Zend\Log\Logger();
			$directory = $this->getStorageManager()->buildChangeURI('synchro', '/in/logs' . $this->getAdditionalFilePart())->toString();
			$filePath = $directory . $this->loggerName . '.log';
			if (!file_exists($filePath))
			{
				\Change\Stdlib\FileUtils::mkdir(dirname($filePath));
				$this->logFile = $filePath;
			}
			$writer = new \Zend\Log\Writer\Stream($filePath);
			$writer->setFormatter(new \Zend\Log\Formatter\Simple('%timestamp% %priorityName%: %extra% %message%'));
			$this->errorLogger = $logger->addWriter($writer);
		}
		return $this->errorLogger;
	}

	/**
	 * @link http://tools.ietf.org/html/rfc3164
	 * @param int $level default value is 0 as Alert defined from the BSD Syslog message severities
	 * @param string $string
	 * @param array|\Traversable $extra
	 */
	public function pushErrors($level, $string, $extra = [])
	{
		if ($level > 7 || $level < 0)
		{
			$level = 0;
		}
		$this->errorStack[$level][] = ['message' => $string, 'extra' => $extra];
	}

	/**
	 * Flush all stacked errors in file
	 */
	public function flushErrorStack()
	{
		foreach ($this->errorStack as $severity => $errors)
		{
			foreach ($errors as $error)
			{
				$this->getErrorLogger()->log($severity, $error['message'], $error['extra']);
			}
		}
		$this->errorStack = [];
	}

	/**
	 * @return string
	 */
	public function getLogFile()
	{
		$this->flushErrorStack();
		return $this->logFile;
	}

	/**
	 * @param string $filePattern
	 * @param string $modelName
	 * @param string $storageName
	 * @return array
	 */
	public function getFiles($filePattern, $modelName = null, $storageName = 'synchro')
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['filePattern' => $filePattern, 'modelName' => $modelName, 'storageName' => $storageName]);
		$event = new CSVImportEvent(static::EVENT_GET_FILES, $this, $args);
		$em->triggerEvent($event);
		if ($args['files'])
		{
			$result = ['files' => $args['files']];
			if (array_key_exists('i18nFiles', $args))
			{
				$result['i18nFiles'] = $args['i18nFiles'];
			}
			return $result;
		}
		return [];
	}

	/**
	 * @param string[] $csvLine
	 * @param \Change\Documents\AbstractDocument $doc
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function populateDocumentData($csvLine, $doc)
	{
		if (!is_array($csvLine) || !($doc instanceof \Change\Documents\AbstractDocument))
		{
			return null;
		}
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['data' => $csvLine, 'document' => $doc]);
		$event = new CSVImportEvent(static::EVENT_POPULATE, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['populated']) && $args['populated'])
		{
			return $args['document'];
		}
		return null;
	}

	/**
	 * @param string $code
	 * @param \Change\Documents\AbstractModel|string|null $model
	 * @param boolean $useCache
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function resolveDocument($code, $model = null, $useCache = false)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['code' => $code, 'model' => $model, 'useCache' => $useCache]);
		$event = new CSVImportEvent(static::EVENT_RESOLVE_DOCUMENT, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['document']) && $args['document'] && $args['document'] instanceof \Change\Documents\AbstractDocument)
		{
			return $args['document'];
		}
		return null;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $doc
	 * @param \Change\Documents\Attributes\Interfaces\Typology $typology
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function addTypology($doc, $typology)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['document' => $doc, 'typologyCode' => $typology]);
		$event = new CSVImportEvent(static::EVENT_ADD_TYPOLOGY, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['document']) && $args['document'] && $args['document'] instanceof \Change\Documents\AbstractDocument)
		{
			return $args['document'];
		}
		return null;
	}

	/**
	 * @param string $type
	 * @return array
	 */
	public function resolveImportType($type)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['type' => $type]);
		$event = new CSVImportEvent(static::EVENT_RESOLVE_TYPE, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['validationFile'], $args['filePattern'], $args['modelName']) && $args['modelName'])
		{
			return ['validationFile' => $args['validationFile'], 'filePattern' => $args['filePattern'], 'modelName' => $args['modelName']];
		}
		return [];
	}

	/**
	 * @return array
	 */
	public function getSortedImport()
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs([]);
		$event = new CSVImportEvent(static::EVENT_GET_SORTED_IMPORT, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['sortedImport']) && $args['sortedImport'])
		{
			return $args['sortedImport'];
		}
		return [];
	}

	/**
	 * @param string[] $fields
	 * @return mixed|null
	 */
	public function buildValidators($fields)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['fields' => $fields]);
		$event = new CSVImportEvent(static::EVENT_BUILD_VALIDATORS, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['validators']) && $args['validators'])
		{
			return $args['validators'];
		}
		return null;
	}

	/**
	 * @param string[] $csvLine
	 * @param string $modelName
	 * @param array $validators
	 * @param string $file
	 * @param int $lineNumber
	 * @return string[]
	 */
	public function validateLine($csvLine, $modelName, $validators, $file, $lineNumber)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['csvLine' => $csvLine, 'modelName' => $modelName, 'validators' => $validators, 'file' => $file, 'index' => $lineNumber]);
		$event = new CSVImportEvent(static::EVENT_VALIDATE_LINE, $this, $args);
		$em->triggerEvent($event);
		if ($args['isValid'] === true)
		{
			return $args['csvLine'];
		}
		return null;
	}

	/**
	 * @param string[] $validatedLines
	 * @param string $modelName
	 * @return array|mixed
	 */
	public function importValidatedLines($validatedLines, $modelName)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['validatedLines' => $validatedLines, 'modelName' => $modelName]);
		$event = new CSVImportEvent(static::EVENT_IMPORT, $this, $args);
		$em->triggerEvent($event);
		if (isset($args['hasError']) && $args['hasError'])
		{
			return $args['hasError'];
		}
		return [];
	}

	/**
	 * @param array $reportData
	 * @param string $storageName
	 * @return array
	 */
	public function aggregateFiles($reportData = [], $storageName = 'synchro')
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['reportData' => $reportData, 'storageName' => $storageName]);
		$event = new CSVImportEvent(static::EVENT_AGGREGATE_FILES, $this, $args);
		$em->triggerEvent($event);
		if ($args['reportData'])
		{
			return $args['reportData'];
		}
		return [];
	}

	/**
	 * @param string[] $reportData
	 * @param string $storageName
	 * @return array
	 */
	public function preTreatment($reportData = [], $storageName = 'synchro')
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['reportData' => $reportData, 'storageName' => $storageName]);
		$event = new CSVImportEvent(static::EVENT_PRE_TREATMENT, $this, $args);
		$em->triggerEvent($event);
		if ($args['reportData'])
		{
			return $args['reportData'];
		}
		return [];
	}

	/**
	 * @return array
	 */
	public function import()
	{
		$reportData = [];

		$reportData = $this->preTreatment($reportData);
		$this->setLoggerName('pretreatment');
		$this->flushErrorStack();
		$reportData = $this->aggregateFiles($reportData);
		$this->setLoggerName('aggregate');
		$this->flushErrorStack();

		$sortedImport = $this->getSortedImport();
		foreach ($sortedImport as $index => $import)
		{
			$time = -microtime(true);
			$data = $this->resolveImportType($import);
			$modelName = isset($data['modelName']) ? $data['modelName'] : null;
			$validationFile = isset($data['validationFile']) ? $data['validationFile'] : null;
			unset($sortedImport[$index]);
			if (!$modelName)
			{
				continue;
			}
			if (isset($validationFile, $data['filePattern']))
			{
				if (!is_file($validationFile) || !is_readable($validationFile))
				{
					$this->getErrorLogger()->crit('Missing required definition file to know file headers.');
					continue;
				}
				$allFiles = $this->getFiles($data['filePattern'], $data['modelName']);
				$em = $this->getEventManager();
				$args = $em->prepareArgs(['modelName' => $modelName, 'storageName' => 'synchro']);
				$event = new CSVImportEvent(static::EVENT_PRE_IMPORT_FILE, $this, $args);
				$em->triggerEvent($event);
				if (!$allFiles || !array_key_exists('files', $allFiles))
				{
					continue;
				}
				$files = $allFiles['files'];
				$rules = json_decode(file_get_contents($validationFile), true);
				if (!is_array($rules) && !isset($rules['properties']))
				{
					continue;
				}
				foreach ($files as $file)
				{
					$pathInfo = pathinfo($file);
					$this->setLoggerName($pathInfo['filename']);
					$handle = fopen($file, 'r');
					if (!$handle)
					{
						continue;
					}
					if (array_key_exists('i18nFiles', $allFiles) && in_array($file, $allFiles['i18nFiles']))
					{
						if (isset($rules['i18nProperties']))
						{
							$requiredFields = $this->getRequiredFields($rules['i18nProperties']);
						}
						else
						{
							fclose($handle);
							$this->flushErrorStack();
							$time += microtime(true);
							$reportData['fileStats']['import'][basename($file)] = [
									'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
									'excutionTime' => number_format($time, 2) . ' seconds',
									'structureErrorsCount' => 1,
									'logFile' => $this->getLogFile()
							];
							$this->getErrorLogger()->crit('Missing i18nProperties for file ' . $data['modelName']);
							continue;
						}
					}
					else
					{
						$requiredFields = $this->getRequiredFields($rules['properties']);
					}
					$validators = $this->buildValidators($requiredFields);
					$csvHeaders = $this->getHeaders($handle);
					if (count($csvHeaders) === 1 && preg_match("/\t/", $csvHeaders[0]) || is_string($csvHeaders) === 1 && preg_match("/\t/", $csvHeaders))
					{
						fclose($handle);
						$backup = $this->backupFile($file);
						unlink($file);
						$this->pushErrors(2, '[' . basename($file)
							. '] Found separator tab in headers. Separator should be \';\'. Can\'t process the file.');
						$this->flushErrorStack();
						$time += microtime(true);
						$reportData['fileStats']['import'][basename($file)] = [
							'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
							'excutionTime' => number_format($time, 2) . ' seconds',
							'structureErrorsCount' => 1,
							'logFile' => $this->getLogFile(),
							'backup' => $backup
						];
						continue;
					}
					$result = $this->validateHeaders($csvHeaders, $requiredFields);
					$missingRequired = $result['missingRequired'];
					$csvHeaders = $result['csvHeaders'];
					if ($missingRequired)
					{
						fclose($handle);
						$backup = $this->backupFile($file);
						unlink($file);
						$this->pushErrors(2, '[' . basename($file) . '] Missing required fields ' . implode(', ', $missingRequired) . ' in header.');
						$this->flushErrorStack();
						$time += microtime(true);
						$reportData['fileStats']['import'][basename($file)] = [
							'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
							'excutionTime' => number_format($time, 2) . ' seconds',
							'structureErrorsCount' => 1,
							'logFile' => $this->getLogFile(),
							'backup' => $backup
						];
						continue;
					}

					$headerCount = count($csvHeaders);
					$validatedLines = [];
					$lineNumber = 1;
					$treatedLines = 0;
					$dataErrorsCount = 0;
					$structureErrorsCount = 0;
					$chunkSize = isset($rules['chunkSize']) && is_int($rules['chunkSize']) ? $rules['chunkSize'] : 50;
					while (!feof($handle))
					{
						$csvLine = fgetcsv($handle, null, ';', '"');
						$treatedLines++;
						$lineNumber++;
						if (is_array($csvLine) && count($csvLine) === $headerCount)
						{
							$csvLine = array_combine($csvHeaders, $csvLine);
							$missingFields = $this->validateRequiredFields($csvLine, $requiredFields);
							if ($missingFields)
							{
								if (count($missingFields) > 1)
								{
									$this->pushErrors(2, '[' . basename($file) . '] Missing required values for columns '
										. implode(', ', $missingFields) . ' at line ' . $lineNumber);
									$structureErrorsCount++;
								}
								if (count($missingFields) == 1)
								{
									$this->pushErrors(2, '[' . basename($file) . '] Missing required value for column ' . $missingFields[0]
										. ' at line ' . $lineNumber);
									$structureErrorsCount++;
								}
								$this->flushErrorStack();
							}
							else
							{
								$validatedLine = $this->validateLine($csvLine, $modelName, $validators, $file, $lineNumber);
								if ($validatedLine)
								{
									$validatedLines[$lineNumber] = $validatedLine;
								}
								else
								{
									$dataErrorsCount += count($this->errorStack);
								}
								$this->flushErrorStack();
							}
						}
						if (count($validatedLines) == $chunkSize)
						{
							$hasError = $this->importValidatedLines($validatedLines, $modelName);
							if ($hasError)
							{
								$this->pushErrors(3, 'An error occur while importing lines ' . ($lineNumber - $treatedLines) . ' to ' . $lineNumber
									. '. Skipping to next lines.');
								$dataErrorsCount += count($this->errorStack);
								$this->flushErrorStack();
							}
							$treatedLines = 0;
							$validatedLines = [];
						}
					}
					if ($validatedLines)
					{
						$hasError = $this->importValidatedLines($validatedLines, $modelName);
						if ($hasError)
						{
							$this->pushErrors(3, 'An error occur while importing lines ' . ($lineNumber - $treatedLines) . ' to ' . $lineNumber
								. '. Skipping to next lines.');
							$dataErrorsCount += count($this->errorStack);
						}
						$this->flushErrorStack();
						unset($validatedLines);
					}
					$em = $this->getEventManager();
					$args = $em->prepareArgs(['modelName' => $modelName]);
					$event = new CSVImportEvent(static::EVENT_POST_IMPORT_FILE, $this, $args);
					$em->triggerEvent($event);
					$this->flushErrorStack();
					fclose($handle);
					$backup = $this->backupFile($file);
					unlink($file);
					$time += microtime(true);
					$reportData['fileStats']['import'][basename($file)] = [
						'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
						'treatedDataLines' => $lineNumber - 1,
						'executionTime' => number_format($time, 2),
						'dataErrorsCount' => $dataErrorsCount,
						'structureErrorsCount' => $structureErrorsCount,
						'backup' => $backup,
						'logFile' => $this->getLogFile()
					];
					if ($this->logFile)
					{
						$reportData['fileStats']['import'][basename($file)]['logFile'] = $this->getLogFile();
					}
				}
			}
		}
		return $reportData;
	}

	/**
	 * @return array
	 */
	public function validate()
	{
		$reportData = [];
		$sortedImport = $this->getSortedImport();
		foreach ($sortedImport as $import)
		{
			$time = -microtime(true);
			$data = $this->resolveImportType($import);
			$typeParts = explode('_', $import);
			$this->setLoggerName('validation.' . $typeParts[count($typeParts) - 1]);
			if (!array_key_exists('filePattern', $data))
			{
				$this->getErrorLogger()->err('File pattern is not defined for import ' . $import);
				continue;
			}
			$allFiles = $this->getFiles($data['filePattern'], $data['modelName'], 'synchro');
			if (!$allFiles || !array_key_exists('files', $allFiles))
			{
				continue;
			}
			$files = $allFiles['files'];
			$validationFile = isset($data['validationFile']) ? $data['validationFile'] : null;
			if (!is_file($validationFile))
			{
				$this->getErrorLogger()->err('No validation file present for import ' . $import);
				continue;
			}
			$rules = json_decode(file_get_contents($validationFile), true);
			foreach ($files as $file)
			{
				$handle = fopen($file, 'r');
				$lineNumber = 1;
				$dataErrorsCount = 0;
				$structureErrorsCount = 0;
				if (!$handle)
				{
					continue;
				}
				if (array_key_exists('i18nFiles', $allFiles) && in_array($file, $allFiles['i18nFiles']))
				{
					if (isset($rules['i18nProperties']))
					{
						$fields = $rules['i18nProperties'];
					}
					else
					{
						fclose($handle);
						$this->getErrorLogger()->err('Missing i18nProperties for file ' . $data['modelName']);
						continue;
					}
				}
				else
				{
					$fields = $rules['properties'];
				}
				$validators = $this->buildValidators($fields);
				$csvHeaders = $this->getHeaders($handle);
				$requiredFields = $this->getRequiredFields($fields);
				$result = $this->validateHeaders($csvHeaders, $requiredFields);
				$missingRequired = $result['missingRequired'];
				$csvHeaders = $result['csvHeaders'];
				if ($missingRequired)
				{
					fclose($handle);
					$this->pushErrors(2, '[' . basename($file) . '] Missing required fields ' . implode(', ', $missingRequired)
						. ' in header. Skipping the file');
					$time += microtime(true);
					$reportData['fileStats']['validation'][basename($file)] = [
						'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
						'treatedDataLines' => $lineNumber - 1,
						'dataErrorsCount' => $dataErrorsCount,
						'structureErrorsCount' => $structureErrorsCount + 1,
						'executionTime' => number_format($time, 2),
						'logFile' => $this->getLogFile()
					];
					continue;
				}
				$isValid = count($csvHeaders) <= count($fields);
				if (!$isValid)
				{
					$this->pushErrors(4, '[' . basename($file) . '] Too much headers than fields. Please check them.');
					$this->pushErrors(4, '[' . basename($file) . '] Expected ' . implode(', ', array_keys($fields)) . ' got ' . implode(', ',
							$csvHeaders));
				}
				while (!feof($handle))
				{
					$line = fgetcsv($handle, null, ';', '"');
					$lineNumber++;
					if (count($line) === count($csvHeaders))
					{
						$line = array_combine($csvHeaders, $line);
						$missingRequired = $this->validateRequiredFields($line, $requiredFields);
						if ($missingRequired)
						{
							if (count($missingRequired) > 1)
							{
								$this->pushErrors(2, '[' . basename($file) . '] Missing required values for columns ' . implode(', ', $missingRequired)
									. ' at line ' . $lineNumber);
								$this->pushErrors(6, '[' . basename($file) . '] LineData: ' . implode(';', $line));
							}
							if (count($missingRequired) == 1)
							{
								$this->pushErrors(2, '[' . basename($file) . '] Missing required value for column ' . $missingRequired[0] . ' at line '
									. $lineNumber);
								$this->pushErrors(6, '[' . basename($file) . '] LineData: ' . implode(';', $line));
							}
							$dataErrorsCount += count($this->errorStack);
							$this->flushErrorStack();
						}
						else
						{
							if (!$this->validateLine($line, $data['modelName'], $validators, $file, $lineNumber))
							{
								$dataErrorsCount += count($this->errorStack);
								$this->flushErrorStack();
							}
						}
					}
					else
					{
						$this->pushErrors(2, '[' . basename($file) . '] Line ' . $lineNumber
							. ' does not contain the same number of columns as header');
						$structureErrorsCount++;
					}
				}
				$time += microtime(true);
				$reportData['fileStats']['validation'][basename($file)] = [
					'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
					'treatedDataLines' => $lineNumber - 1,
					'dataErrorsCount' => $dataErrorsCount,
					'structureErrorsCount' => $structureErrorsCount,
					'executionTime' => number_format($time, 2) . ' seconds'
				];
				if ($this->logFile)
				{
					$reportData['fileStats']['validation'][basename($file)]['logFile'] = $this->getLogFile();
				}
			}
		}
		return $reportData;
	}

	/**
	 * @param $rules
	 * @return array
	 */
	public function getRequiredFields($rules)
	{
		$requiredFields = [];
		foreach ($rules as $name => $rule)
		{
			$rule = array_change_key_case($rule, CASE_LOWER);
			if (isset($rule['required']) && $rule['required'])
			{
				$requiredFields[$name] = $rule;
			}
		}
		return $requiredFields;
	}

	/**
	 * @param string[] $line
	 * @param string[] $requiredFields
	 * @return array
	 */
	public function validateRequiredFields($line, $requiredFields)
	{
		$missingRequired = [];
		foreach ($requiredFields as $fieldName => $requiredField)
		{
			$name = strtolower($fieldName);

			if (!isset($line[$name]) || ($line[$name] !== '0' && !$line[$name]))
			{
				if (isset($requiredField['substitute']))
				{
					$substitute = strtolower($requiredField['substitute']);
					if (!empty($line[$substitute]))
					{
						continue;
					}
				}
				if (!isset($requiredField['equivalent']))
				{
					$missingRequired[] = $fieldName;
				}
				else
				{
					$equivalent = strtolower($requiredField['equivalent']);
					if (!isset($line[$equivalent]) || ($line[$equivalent] !== '0' && !$line[$equivalent]))
					{
						$missingRequired[] = $fieldName;
					}
				}
			}
		}
		return $missingRequired;
	}

	/**
	 * @param $handle
	 * @return array
	 */
	public function getHeaders($handle)
	{
		$headers = fgetcsv($handle, null, ';');
		foreach ($headers as $index => $header)
		{
			// trim trim($val, "\xef\xbb\xbf") will remove the BOM
			$headers[$index] = trim(strtolower($header), "\xef\xbb\xbf");
		}
		return $headers;
	}

	/**
	 * @param $csvHeaders
	 * @param $requiredFields
	 * @return array
	 */
	protected function validateHeaders($csvHeaders, $requiredFields)
	{
		$missingRequired = [];
		foreach ($requiredFields as $fieldName => $requiredField)
		{
			$substitute = $requiredField['substitute'] ?? null;
			if (in_array(strtolower($fieldName), $csvHeaders) || ($substitute && in_array(strtolower($substitute), $csvHeaders)))
			{
				continue;
			}
			if (!isset($requiredField['equivalent']))
			{
				$missingRequired[] = $fieldName;
			}
			else
			{
				$equivalent = strtolower($requiredField['equivalent']);
				$index = array_search(strtolower($equivalent), $csvHeaders, true);
				if ($index === false)
				{
					$missingRequired[] = $fieldName;
				}
				else
				{
					$csvHeaders[$index] = strtolower($fieldName);
					$this->pushErrors(6, 'Required field is using equivalent. Please consider changing this field : ' . $equivalent
						. ' to the required one  : ' . $fieldName);
				}
			}
		}
		return ['missingRequired' => $missingRequired, 'csvHeaders' => $csvHeaders];
	}

	/**
	 * @param string $source
	 * @return string
	 */
	protected function backupFile($source)
	{
		$this->getStorageManager();
		$backupURI = $this->getStorageManager()->buildChangeURI('synchro', '/in/backups' . $this->getAdditionalFilePart());
		$backupPath = $backupURI->toString();
		if (!is_dir($backupPath))
		{
			\Change\Stdlib\FileUtils::mkdir($backupPath);
		}
		$backupFile = $backupPath . basename($source);
		copy($source, $backupFile);
		return $backupFile;
	}

	/**
	 * @param string $status
	 * @return string
	 */
	public function getPublicationStatus($status)
	{
		$allowedPublicationStatus =
			[\Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE, \Change\Documents\Interfaces\Publishable::STATUS_DRAFT,
				\Change\Documents\Interfaces\Publishable::STATUS_FROZEN];
		return $status && in_array($status, $allowedPublicationStatus)
			? $status : \Change\Documents\Interfaces\Publishable::STATUS_DRAFT;
	}

}