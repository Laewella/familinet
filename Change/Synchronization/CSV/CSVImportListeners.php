<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization\CSV;

/**
 * @name \Change\Synchronization\CSV\CSVImportListeners
 * @ignore
 */
class CSVImportListeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * @var \Change\Synchronization\CSV\CSVEngine
	 */
	protected $importEngine;

	/**
	 * DefaultImportDocument constructor.
	 * @param \Change\Synchronization\CSV\CSVEngine $importEngine
	 */
	public function __construct(\Change\Synchronization\CSV\CSVEngine $importEngine)
	{
		$this->importEngine = $importEngine;
	}

	/**
	 * @return \Change\Synchronization\CSV\CSVEngine
	 */
	public function getCSVEngine()
	{
		return $this->importEngine;
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVEngine $importEngine
	 * @return $this
	 */
	public function setImportEngine(\Change\Synchronization\CSV\CSVEngine $importEngine)
	{
		$this->importEngine = $importEngine;
		return $this;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 * @param int $priority
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $eventManager, $priority = 1)
	{
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_RESOLVE_DOCUMENT, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onResolveDocument($event);
		}, 5);
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_POPULATE, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onPopulateDocumentData($event);
		}, 5);
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_GET_FILES, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onGetFiles($event);
		}, 5);
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_ADD_TYPOLOGY, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onAddTypology($event);
		}, 5);
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_VALIDATE_LINE, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onValidateLine($event);
		}, 5);
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_BUILD_VALIDATORS, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onBuildValidators($event);
		}, 5);
		$this->listeners[] = $eventManager->attach(CSVEngine::EVENT_PRE_TREATMENT, function (\Change\Synchronization\CSV\CSVImportEvent $event)
		{
			$this->onPreTreatment($event);
		}, 25);
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function onPreTreatment(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$storageManager = $applicationServices->getStorageManager();
		$storage = $event->getParam('storageName', 'synchro');
		if ($event->getParam('filesToProcess') !== null && $storageManager->getStorageByName($storage))
		{
			$path = $storageManager->buildChangeURI($storage, '/in' . $event->getTarget()->getAdditionalFilePart())->toString();
			$files = array_diff(scandir($path), ['.', '..']);
			$event->setParam('filesToProcess', $files);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function onResolveDocument(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		if ($event->getParam('document'))
		{
			return;
		}
		$model = $event->getParam('model');
		$code = $event->getParam('code');
		$applicationServices = $event->getApplicationServices();
		$csvEngine = $event->getCSVEngine();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();
		$formattedCode = null;
		$doc = null;
		$useCache = $event->getParam('useCache');
		if (is_string($model))
		{
			$model = $applicationServices->getModelManager()->getModelByName($model);
			if (!$model)
			{
				return;
			}
		}
		$documentManager = $applicationServices->getDocumentManager();
		if ($model instanceof \Change\Documents\AbstractModel && $useCache)
		{
			$formattedCode = $code . $model->getShortName();
			if ($doc = $csvEngine->getLoadedDocument($documentManager, $formattedCode))
			{
				$event->setParam('document', $doc);
				return;
			}
		}
		$doc = $documentCodeManager->getFirstDocumentByCode($code, 'Synchronization', $model);
		if (!$doc)
		{
			if ($model && !($model->isAbstract() || $model->isInline()))
			{
				$property = $model->getProperty('code');
				if ($property && !($property->getLocalized() || $property->getStateless()))
				{
					$q = $applicationServices->getDocumentManager()->getNewQuery($model);
					$doc = $q->andPredicates($q->eq('code', $code))->addOrder('id')->getFirstDocument();
				}
			}
		}
		if ($doc && $useCache)
		{
			$csvEngine->pushLoadedDocument($formattedCode, $doc);
		}
		$event->setParam('document', $doc);
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function onPopulateDocumentData(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		//TODO See how to import options
		if ($event->getParam('populated', false))
		{
			return;
		}
		/** @var \Change\Documents\AbstractDocument $origin */
		$doc = $event->getParam('document');
		if (!$doc)
		{
			return;
		}
		$data = $event->getParam('data');
		$properties = $doc->getDocumentModel()->getProperties();
		/** @var \Change\Documents\Property[] $properties */
		$properties = array_change_key_case($properties, CASE_LOWER);
		$doc->useCorrection(false);
		$csvEngine = $event->getCSVEngine();
		foreach ($data as $header => $dataValue)
		{
			$dataValue = is_string($dataValue) ? trim($dataValue) : $dataValue;
			if (isset($properties[$header]) && ($property = $properties[$header]))
			{
				if ($header === 'publicationstatus')
				{
					continue;
				}
				if ($property->getType() === \Change\Documents\Property::TYPE_DATETIME)
				{
					if ($dataValue === '')
					{
						$dataValue = $property->getDefaultValue();
						if ($property->getRequired() && !$dataValue)
						{
							$csvEngine->pushErrors(2, 'Can\'t set ' . $property->getName() . ' for document ' . $data['code']);
						}
					}
					$property->setValue($doc, $dataValue);
				}
				else if ($property->getType() === \Change\Documents\Property::TYPE_INLINE
					|| $property->getType() === \Change\Documents\Property::TYPE_INLINEARRAY
				)
				{
					continue;
				}
				else if ($property->getType() === \Change\Documents\Property::TYPE_DOCUMENTARRAY)
				{
					$array = explode('|', $dataValue);
					$values = [];
					if (!$array)
					{
						continue;
					}
					foreach ($array as $arrayPart)
					{
						$value = $csvEngine->resolveDocument((string)$arrayPart, $property->getDocumentType());
						if ($value)
						{
							$values[] = $value;
						}
						elseif ($arrayPart !== '' && $value)
						{
							$csvEngine->pushErrors(4, 'Document with ' . $arrayPart . ' can\'t be found for ' . $data['code'] . '.');
						}
					}
					if (count($values) || !$property->getRequired())
					{
						$property->setValue($doc, $values);
					}
					else
					{
						if ($property->getRequired())
						{
							$csvEngine->pushErrors(2, 'Can\'t set ' . $property->getName() . ' for document ' . $data['code']);
						}
					}
				}
				else if ($property->getType() === \Change\Documents\Property::TYPE_DOCUMENTID
					|| $property->getType() === \Change\Documents\Property::TYPE_DOCUMENT
				)
				{
					$value = $csvEngine->resolveDocument((string)$dataValue, $property->getDocumentType());
					if ($value || !$property->getRequired())
					{
						$property->setValue($doc, $value);
					}
					else
					{
						if ($property->getRequired())
						{
							$csvEngine->pushErrors(2, 'Can\'t set ' . $property->getName() . ' for document ' . $data['code']);
						}
						elseif ($dataValue !== '' && $value)
						{
							$csvEngine->pushErrors(4, 'Can\'t set ' . $property->getName() . ' for document ' . $data['code'] . ' with value '
								. $dataValue);
						}
					}
				}
				else
				{
					$property->setValue($doc, $dataValue);
				}
			}
		}
		$event->setParam('document', $doc);
		$event->setParam('populated', true);
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function onGetFiles(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		if ($event->getParam('files'))
		{
			return;
		}
		$storage = $event->getParam('storageName', 'synchro');
		$storageManager = $event->getApplicationServices()->getStorageManager();
		$files = [];
		if ($storageManager->getStorageByName($storage))
		{
			$filePattern = $event->getParam('filePattern');
			$ftpPath = $storageManager->buildChangeURI($storage, '/in' . $event->getTarget()->getAdditionalFilePart())->toString();
			$tmpInFiles = array_diff(scandir($ftpPath), ['.', '..']);
			foreach ($tmpInFiles as $file)
			{
				if (preg_match('/^' . $filePattern . '$/i', $file))
				{
					$files[] = $ftpPath . $file;
				}
			}
		}
		$event->setParam('files', $files);
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function onValidateLine(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		if ($event->getParam('isValid'))
		{
			$csvLine = $event->getParam('csvLine');
			$lineNumber = $event->getParam('index');
			$file = $event->getParam('file');
			$validators = $event->getParam('validators');
			if (!$validators)
			{
				$event->setParam('isValid', false);
				$this->getCSVEngine()->pushErrors(2, 'Missing validators. Line will not be imported');
			}
			$errorStack = [];
			foreach ($csvLine as $header => &$value)
			{
				$value = trim($value);
				if (isset($validators[$header]))
				{
					if (!$this->isValid($validators[$header], $value, $header))
					{
						$this->getCSVEngine()->pushErrors(4, '[' . basename($file) . '] Value: ' . $value . ' in column ' . $header . ' line '
							. $lineNumber . ' is not valid. Please check the file.');
						$errorStack[$header] = $value;
						$event->setParam('isValid', false);
					}
				}
			}
			$event->setParam('errors', $errorStack);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function onAddTypology(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$typologyCode = $event->getParam('typologyCode', '');
		$typology = $event->getParam('typology');
		$doc = $event->getParam('document');
		if (!($typology instanceof \Change\Documents\Attributes\Interfaces\Typology) || !($doc instanceof \Change\Documents\AbstractDocument))
		{
			$event->getCSVEngine()->pushErrors(4, 'Typology with code ' . $typologyCode . ' not found.');
			return;
		}
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$documentManager->saveAttributeValues($doc, $typology, null);
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 * @return array
	 */
	protected function onBuildValidators(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$fields = $event->getParam('fields');
		if (!$fields || $event->getParam('validators') !== null)
		{
			return;
		}
		$validators = [];
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		foreach ($fields as $fieldName => $rule)
		{
			if (isset($rule['type']))
			{
				$required = isset($rule['required']) && $rule['required'];
				$callable = null;
				switch ($rule['type'])
				{
					case 'String':
						if (isset($rule['pattern']))
						{
							$regex = $rule['pattern'];
							$options = isset($rule['options']) ? $rule['options'] : '';
							$callable = function ($value) use ($regex, $rule, $options, $required)
							{
								if ($required && (!$value || $value === ''))
								{
									return false;
								}
								if (!$required && empty($value))
								{
									return true;
								}
								return preg_match('/' . $regex . '/' . $options, $value);
							};
						}
						else
						{
							$val['min'] = isset($rule['minLength']) ? $rule['minLength'] : null;
							$val['max'] = isset($rule['maxLength']) ? $rule['maxLength'] : null;
							$callable = function ($value) use ($val)
							{
								$isValid = $val['min'] ? strlen($value) >= $val['min'] : true;
								$isValid = $val['max'] ? $isValid && strlen($value) <= $val['max'] : true;
								return $isValid && is_string($value);
							};
						}
						break;
					case 'Enum':
						$values = array_map('strtolower', $rule['values']);
						$callable = function ($value) use ($values, $required)
						{
							if ($required && (!$value || $value === ''))
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							return in_array(strtolower($value), $values);
						};
						break;
					case 'Boolean':
						$callable = function ($value) use ($required)
						{
							if ($required && (empty($value) && $value != '0'))
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							return $value === '1' || $value === '0';
						};
						break;
					case 'Integer':
						$val['min'] = isset($rule['minVal']) ? $rule['minVal'] : null;
						$val['max'] = isset($rule['maxVal']) ? $rule['maxVal'] : null;
						$callable = function ($value) use ($required, $val)
						{
							if ($required && is_nan($value) !== false && strlen($value) > 0)
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							$isValid = $val['min'] ? $value >= $val['min'] : true;
							$isValid = $val['max'] ? $isValid && $value <= $val['max'] : true;
							return $isValid && preg_match('/[0-9]+/', $value);
						};
						break;
					case 'Float':
						$val['min'] = isset($rule['minVal']) ? $rule['minVal'] : null;
						$val['max'] = isset($rule['maxVal']) ? $rule['maxVal'] : null;
						$callable = function ($value) use ($required, $val)
						{
							if ($required && is_nan($value) !== false && strlen($value) > 0)
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							$isValid = $val['min'] ? $value >= $val['min'] : true;
							$isValid = $val['max'] ? $isValid && $value <= $val['max'] : true;
							return $isValid && preg_match('/[-+]?[0-9]*\.?[0-9]+/', $value);
						};
						break;
					case 'Document':
						$model = $rule['model'];
						$callable = function ($value) use ($model, $required, $modelManager)
						{
							if ($required && (!$value || $value === ''))
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							return ($model && $modelManager->getModelByName($model) !== null) || (!$model && $value);
						};
						break;
					case 'DocumentArray':
						$model = $rule['model'];
						$callable = function ($value) use ($model, $required, $modelManager)
						{
							if ($required && (!$value || $value === ''))
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							return ($model && $modelManager->getModelByName($model) !== null) || (!$model && $value);
						};
						break;
					case 'Collection':
						$collectionName = $rule['name'];
						$collection = $applicationServices->getCollectionManager()->getCollection($collectionName);
						$callable = function ($value) use ($collection, $required)
						{
							if ($required && (!$value || $value === ''))
							{
								return false;
							}
							if (!$required && empty($value))
							{
								return true;
							}
							return $collection->getItemByValue($value) !== null;
						};
						break;
					case 'Configuration':
						$entry = $rule['value'];
						$config = $event->getApplication()->getConfiguration()->getEntry($entry);
						if (!$config)
						{
							$config = [];
						}
						$config = array_map('strtolower', $config);
						$callable = function ($value) use ($config)
						{
							return in_array(strtolower($value), $config);
						};
						break;
					case 'Regex':
						if (isset($rule['regex']))
						{
							$regex = $rule['regex'];
							$options = isset($rule['options']) ? $rule['options'] : '';
							//TODO validate options
							$callable = function ($value) use ($regex, $rule, $options, $required)
							{
								if ($required && (!$value || $value === ''))
								{
									return false;
								}
								if (!$required && empty($value))
								{
									return true;
								}
								return preg_match('/' . $regex . '/' . $options, $value);
							};
						}
						break;
					default:
						break;
				}

				$validators[strtolower($fieldName)] = $callable;
			}
		}
		$event->setParam('validators', $validators);
	}

	/**
	 * @param $callable
	 * @param $value
	 * @param $header
	 * @return bool|mixed
	 */
	protected function isValid($callable, $value, $header)
	{
		if (is_callable($callable))
		{
			return call_user_func($callable, $value);
		}
		$this->getCSVEngine()->pushErrors(4, $header . ' can\'t be validated');
		return true;
	}
}