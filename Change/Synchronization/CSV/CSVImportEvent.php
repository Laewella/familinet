<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization\CSV;

/**
 * @name \Change\Synchronization\CSV\CSVEvents
 */
class CSVImportEvent extends \Change\Events\Event
{

	/**
	 * @return CSVEngine
	 */
	public function getCSVEngine()
	{
		return $this->getTarget();
	}

}