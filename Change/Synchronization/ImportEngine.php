<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ImportEngine
 */
class ImportEngine implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const OPTION_CONTEXT_CODE = 'contextCode';

	const EVENT_MANAGER_IDENTIFIER = 'ImportEngine';

	const EVENT_RESOLVE = 'resolve';
	const EVENT_CREATE = 'create';
	const EVENT_POPULATE = 'populate';
	const EVENT_SAVE = 'save';
	const EVENT_DELETE = 'delete';

	const ACTION_SAVE = 'save';
	const ACTION_PATCH = 'patch';
	const ACTION_DELETE = 'delete';

	/**
	 * @var \Zend\Stdlib\Parameters
	 */
	protected $options;

	/**
	 * @param \Change\Application $application
	 * @param array $options
	 */
	public function __construct(\Change\Application $application, array $options = null)
	{
		$this->setApplication($application);
		if ($options)
		{
			$this->getOptions()->fromArray($options);
		}
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getOptions()
	{
		if ($this->options === null)
		{
			$this->options = new \Zend\Stdlib\Parameters();
		}
		return $this->options;
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return array
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/ImportEngine');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		(new ImportDocumentListener($this))->attach($eventManager);
	}

	/**
	 * @return mixed
	 */
	public function getContextCode()
	{
		return $this->getOptions()->get(static::OPTION_CONTEXT_CODE, 0);
	}

	/**
	 * @api
	 * @param array $itemData
	 * @return mixed|null
	 */
	public function importItem($itemData)
	{
		if (!$itemData || !is_array($itemData) )
		{
			throw new ImportException('Invalid "itemData"', 0);
		}

		$itemData += ['__model' => null, '__id' => null, '__action' => static::ACTION_SAVE];
		if (!is_string($itemData['__model']) || !is_string($itemData['__id']))
		{
			throw new ImportException('Invalid "__model" or "__id")', 0);
		}

		$action = $itemData['__action'];
		if ($action !== static::ACTION_SAVE && $action !== static::ACTION_PATCH && $action !== static::ACTION_DELETE)
		{
			throw new ImportException('Invalid "__action"', 1);
		}

		$itemData['__action'] = $action;
		$eventManager = $this->getEventManager();

		$event = new ImportEvent(static::EVENT_RESOLVE, $this, ['itemData' => $itemData, 'item' => null, 'action' => $action]);
		$this->dispatchResolve($event);
		$item = $event->getItem();

		if ($action === static::ACTION_SAVE)
		{
			if (!$item)
			{
				$event->setName(static::EVENT_CREATE);
				$this->getEventManager()->triggerEvent($event);
				$item = $event->getItem();
				if (!$item)
				{
					throw new ImportException('Unable to create item', 2);
				}
			}

			$event->setName(static::EVENT_POPULATE);
			$this->getEventManager()->triggerEvent($event);
			$item = $event->getItem();
			if (!$item)
			{
				throw new ImportException('Unable to populate item', 3);
			}

			$event->setName(static::EVENT_SAVE);
			$eventManager->triggerEvent($event);
			$item = $event->getItem();
			if (!$item)
			{
				throw new ImportException('Unable to save item', 4);
			}
		}
		elseif ($action === static::ACTION_PATCH)
		{
			if (!$item)
			{
				throw new ImportException('Unable to resolve item', 5);
			}

			$event->setName(static::EVENT_POPULATE);
			$this->getEventManager()->triggerEvent($event);
			$item = $event->getItem();
			if (!$item)
			{
				throw new ImportException('Unable to populate item', 3);
			}

			$event->setName(static::EVENT_SAVE);
			$eventManager->triggerEvent($event);
			$item = $event->getItem();
			if (!$item)
			{
				throw new ImportException('Unable to save item', 4);
			}
		}
		elseif ($action === static::ACTION_DELETE)
		{
			if ($item)
			{
				$event->setName(static::EVENT_DELETE);
				$eventManager->triggerEvent($event);
				$item = $event->getItem();
				if (!$item)
				{
					throw new ImportException('Unable to delete item', 6);
				}
			}
		}
		else
		{
			throw new ImportException('Invalid "__action"', 1);
		}

		return $item ?: null;
	}

	/**
	 * @api
	 * @param array $itemData
	 * @param boolean $throwInvalidFormat
	 * @return mixed|null
	 */
	public function resolveItem(array $itemData, $throwInvalidFormat = true)
	{
		if (!isset($itemData['__model'], $itemData['__id']) || !is_string($itemData['__model']) || !is_string($itemData['__id']))
		{
			if ($throwInvalidFormat)
			{
				$e = new ImportException('Invalid "itemData" format ("__model" or "__id" not set)', 0);
				$e->setItemData($itemData);
				throw $e;
			}
			return null;
		}
		$event = new ImportEvent(static::EVENT_RESOLVE, $this, ['itemData' => $itemData, 'item' => null, 'action' => 'resolve']);
		$this->dispatchResolve($event);
		$item = $event->getItem();
		if ($throwInvalidFormat && !$item)
		{
			$e = new ImportException('Unable to resolve item', 5);
			$e->setItemData($itemData);
			throw $e;
		}
		return ($item) ?: null;
	}

	/**
	 * @param ImportEvent $event
	 */
	protected function dispatchResolve(ImportEvent $event)
	{
		$event->setName(static::EVENT_RESOLVE);
		$this->getEventManager()->triggerEvent($event);
	}
}