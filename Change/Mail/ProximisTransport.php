<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Mail;

/**
 * @name \Change\Mail\ProximisTransport
 */
class ProximisTransport implements \Zend\Mail\Transport\TransportInterface
{
	/**
	 * @var array
	 */
	protected $configuration = ['token' => null, 'url' => null];

	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var boolean
	 */
	protected $valid = false;

	/**
	 * @param array $configuration
	 * @param \Change\Application $application
	 */
	public function __construct(array $configuration, \Change\Application $application)
	{
		$this->configuration = array_merge($this->configuration, $configuration);
		$this->application = $application;
		$this->valid = isset($this->configuration['token'], $this->configuration['url']);
	}


	/**
	 * @return string
	 */
	protected function getToken()
	{
		return $this->configuration['token'];
	}

	/**
	 * @return string
	 */
	protected function getUrl()
	{
		return $this->configuration['url'];
	}

	/**
	 * Send a mail message
	 * @param \Zend\Mail\Message $message
	 */
	public function send(\Zend\Mail\Message $message)
	{
		if (!$this->valid)
		{
			$this->application->getLogging()->error(__METHOD__, 'Invalid configuration');
			return;
		}

		$sender = $message->getFrom();
		$data = [
			'sender' => $this->addressToJson($sender),
			'to' => $this->addressToJson($message->getTo()),
			'subject' => $message->getSubject(),
			'html' => null,
		];

		$cc = $message->getCc();
		if ($cc->count())
		{
			$data['cc'] = $this->addressToJson($cc);
		}

		$bcc = $message->getBcc();
		if ($bcc->count())
		{
			$data['bcc'] = $this->addressToJson($bcc);
		}

		$replyTo = $message->getReplyTo();
		if ($replyTo->count() === 1)
		{
			$data['replyTo'] = $this->addressToJson($replyTo);
		}

		$body = $message->getBody();
		if ($body instanceof \Zend\Mime\Message)
		{
			foreach ($body->getParts() as $part)
			{
				if ($part instanceof \Zend\Mime\Part)
				{
					if ($part->getDisposition() === \Zend\Mime\Mime::DISPOSITION_ATTACHMENT)
					{
						$data['attachments'][] = [
							'filename' => $part->getFileName(),
							'content' => $part->getContent(),
							'encoding' => $part->getEncoding()
						];
					}
					else
					{
						if ($part->getType() === \Zend\Mime\Mime::TYPE_HTML)
						{
							$data['html'] = $part->getRawContent();
						}
						elseif ($part->getType() === \Zend\Mime\Mime::TYPE_TEXT)
						{
							$data['text'] = $part->getRawContent();
						}
					}
				}
			}
		}
		elseif (is_string($body))
		{
			$data['html'] = $body;
		}

		if ($data['html'])
		{
			$this->postEmail($data);
		}

	}

	protected function addressToJson($address)
	{
		if ($address === null || is_string($address))
		{
			return $address;
		}
		elseif ($address instanceof \Zend\Mail\Address\AddressInterface)
		{
			return $address->getName() ? $address->toString() : $address->getEmail();
		}
		elseif ($address instanceof \Zend\Mail\AddressList)
		{
			$array = [];
			foreach ($address as $a)
			{
				$array[] = $this->addressToJson($a);
			}

			if (count($array) === 1)
			{
				return $array[0];
			}
			return $array ?: null;
		}
		return null;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	protected function postEmail($data)
	{
		$dataString = json_encode(['email' => $data]);
		$url = $this->getUrl() . '/notification';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $this->getToken(),
				'Content-Length: ' . strlen($dataString)]
		);

		$result = curl_exec($ch);
		if ($result === false)
		{
			$errorData = ['no' => curl_errno($ch), 'error' => curl_error($ch), 'type' => 'curl_error'];
			$this->application->getLogging()->error(__METHOD__, 'curl_error', $errorData['no'], $errorData['error']);
			$this->application->getLogging()->debug($dataString);
		}
		else
		{
			$httpCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (!$result || $httpCode >= 400)
			{
				$errorData = ['no' => $httpCode, 'error' => $result, 'type' => 'http_error'];
				$this->application->getLogging()->error(__METHOD__, 'http_error', $errorData['no'], $errorData['error']);
				$this->application->getLogging()->debug($dataString);
				$result = false;
			}
			else
			{
				$result = json_decode($result, true);
			}
		}

		curl_close($ch);
		return $result ?: [];
	}


}