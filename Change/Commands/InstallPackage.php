<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;


/**
 * @name \Change\Commands\InstallPackage
 */
class InstallPackage
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		$vendor = $event->getParam('vendor');
		$shortName = $event->getParam('name');

		$response = $event->getCommandResponse();

		$pluginManager = $applicationServices->getPluginManager();

		$allSteps = $pluginManager->getInstallSteps();
		$inputSteps = array_count_values(array_filter(array_map(function ($s)
		{
			return strtolower(trim($s));
		}, explode(',', $event->getParam('steps')))));

		if (isset($inputSteps['all']))
		{
			$steps = $allSteps;
		}
		else
		{
			$steps = [];
			foreach ($allSteps as $step)
			{
				if (isset($inputSteps[strtolower($step)]))
				{
					$steps[] = $step;
				}
			}
			if (!$steps)
			{
				$response->addErrorMessage('No valid step defined!');
				return;
			}
		}

		$response->addInfoMessage('Installation steps: ' . implode(', ', $steps));
		$plugins = $pluginManager->installPackage($vendor, $shortName, ['steps' => $steps]);
		if (count($plugins))
		{
			foreach ($plugins as $plugin)
			{
				$response->addInfoMessage($plugin . ' installed');
			}
			$response->addInfoMessage(count($plugins) . ' plugin(s) installed.');
		}
		else
		{
			$response->addInfoMessage('Package not installed.');
		}
	}
}