<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

/**
 * @name \Change\Commands\Job
 */
class Job
{
	/**
	 * @var boolean
	 */
	protected $run;

	/**
	 * @var boolean
	 */
	protected $verbose;

	/**
	 * @var \Change\Commands\Events\CommandResponseInterface
	 */
	protected $response;

	/**
	 * @var \Change\Job\JobManager
	 */
	protected $jobManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$this->run = $event->getParam('run');
		$this->response = $event->getCommandResponse();
		$this->jobManager = $event->getApplicationServices()->getJobManager();
		$this->i18nManager = $event->getApplicationServices()->getI18nManager();
		$this->verbose = $event->getParam('verbose');
		$transactionManager = $event->getApplicationServices()->getTransactionManager();

		$selector = $event->getParam('selector');
		$operation = $event->getParam('operation');
		switch ($operation)
		{
			case 'engine':
				$stop = $selector === 'stop';
				$editConfig = new \Change\Configuration\EditableConfiguration([]);
				$editConfig->import($event->getApplication()->getConfiguration());
				$editConfig->addPersistentEntry('Change/Job/stop', $stop,
					\Change\Configuration\Configuration::AUTOGEN);
				$editConfig->save();
				if ($stop)
				{
					$this->response->addInfoMessage('Job engine STOPPED. Run "change:job engine start" to restart it.');
				}
				else
				{
					$this->response->addInfoMessage('Job engine started.');
				}
				break;

			case 'cleanup':
				if (is_numeric($selector))
				{
					$callback = function (\Change\Job\JobInterface $job)
					{
						$this->jobManager->deleteJob($job);
						$this->response->addInfoMessage('Job ' . $job->getId() . ' deleted.');
					};
					$this->singleJobOperation((int)$selector, $operation, $callback);
					return;
				}
				elseif ($selector === \Change\Job\JobInterface::STATUS_SUCCESS
					|| $selector === \Change\Job\JobInterface::STATUS_FAILED
				)
				{
					$date = $this->getDate($event->getParam('for', '7d'));
					if (!$date)
					{
						$this->response->addErrorMessage('Invalid for option: ' . $event->getParam('for'));
						return;
					}

					try
					{
						$transactionManager->begin();
						$count = $this->jobManager->cleanup($selector, $date, !$this->run);
						$transactionManager->commit();
					}
					catch (\Exception $e)
					{
						throw $transactionManager->rollBack($e);
					}

					if ($this->run)
					{
						$this->response->addInfoMessage($count . ' jobs deleted.');
					}
					else
					{
						$this->response->addInfoMessage($count . ' ' . $selector . ' jobs since '
							. $this->i18nManager->transDateTime($date) . ' to delete.');
						$this->response->addWarningMessage('No operation performed. To perform ' . $operation
							. ', please add --run (or -r) option.');
					}
				}
				else
				{
					$this->response->addErrorMessage('Invalid selector: ' . $selector
						. ' (allowed ones for deletion are success, failed and specific job ids)');
				}
				break;

			case 'start':
				if (is_numeric($selector))
				{
					$callback = function (\Change\Job\JobInterface $job)
					{
						$status = $job->getStatus();
						if ($status !== \Change\Job\JobInterface::STATUS_WAITING)
						{
							$this->response->addErrorMessage('Job ' . $job->getId() . ' has invalid status: ' . $status);
							return;
						}
						$this->jobManager->restartJob($job);
						$this->verbose = true;
						$this->response->addInfoMessage('Job start date updated');
						$this->jobInfos($job);
					};
					$this->singleJobOperation((int)$selector, $operation, $callback);
					return;
				}
				else
				{
					$this->response->addErrorMessage('Invalid selector: ' . $selector . ' (allowed specific job ids)');
				}
				break;
			case 'restart':
				if (is_numeric($selector))
				{
					$callback = function (\Change\Job\JobInterface $job)
					{
						$status = $job->getStatus();
						if ($status !== \Change\Job\JobInterface::STATUS_FAILED
							&& $status !== \Change\Job\JobInterface::STATUS_RUNNING
						)
						{
							$this->response->addErrorMessage('Job ' . $job->getId() . ' has invalid status: ' . $status);
							return;
						}
						$this->jobManager->restartJob($job);
					};
					$this->singleJobOperation((int)$selector, $operation, $callback);
					return;
				}
				elseif ($selector === \Change\Job\JobInterface::STATUS_RUNNING
					|| $selector === \Change\Job\JobInterface::STATUS_FAILED
				)
				{
					$date = $this->getDate($event->getParam('for', '1h'));
					if (!$date)
					{
						$this->response->addErrorMessage('Invalid for option: ' . $event->getParam('for'));
						return;
					}
					$ids = $this->jobManager->restart($selector, $date, !$this->run);
					if ($this->run)
					{
						$this->response->addInfoMessage(count($ids) . ' jobs restarted.');
						foreach ($ids as $id)
						{
							$job = $this->jobManager->getJob($id);
							if ($job)
							{
								$this->jobInfos($job);
							}
						}
					}
					else
					{
						$this->response->addInfoMessage(count($ids) . ' ' . $selector . ' jobs since '
							. $this->i18nManager->transDateTime($date) . ' to restart.');
						foreach ($ids as $id)
						{
							$job = $this->jobManager->getJob($id);
							if ($job)
							{
								$this->jobInfos($job);
							}
						}
						$this->response->addWarningMessage('No operation performed. To perform ' . $operation
							. ', please add --run (or -r) option.');
					}
				}
				else
				{
					$this->response->addErrorMessage('Invalid selector: ' . $selector
						. ' (allowed ones for deletion are running, failed and specific job ids)');
				}
				break;

			default:
				$this->response->addErrorMessage('Invalid operation: ' . $operation . ' (allowed ones are engine, cleanup, start and restart)');
				return;
		}
	}

	/**
	 * @param string $for
	 * @return \DateTime|null
	 */
	protected function getDate($for)
	{
		$date = new \DateTime();
		if ($for == 'now')
		{
			return $date;
		}

		if (is_string($for))
		{
			$for = strtoupper($for);
			if (preg_match('/\d+[YMD]/', $for))
			{
				$date->sub(new \DateInterval('P' . $for));
				return $date;
			}
			elseif (preg_match('/\d+[H]/', $for))
			{
				$date->sub(new \DateInterval('PT' . $for));
				return $date;
			}
		}
		return null;
	}

	/**
	 * @param integer $jobId
	 * @param string $operation
	 * @param callable $callback
	 */
	protected function singleJobOperation($jobId, $operation, $callback)
	{
		$job = $this->jobManager->getJob($jobId);
		if (!$job)
		{
			$this->response->addErrorMessage('Invalid jobId: ' . $jobId);
			return;
		}

		if ($this->run)
		{
			$callback($job);
			return;
		}
		else
		{
			$this->response->addInfoMessage('Selected job data:');
			$this->jobInfos($job);
			$this->response->addWarningMessage('No operation performed. To perform ' . $operation
				. ', please add --run (or -r) option.');
			return;
		}
	}

	/**
	 * @param \Change\Job\JobInterface $job
	 */
	protected function jobInfos($job)
	{
		$response = $this->response;
		if ($this->verbose)
		{
			$response->addInfoMessage('');
			$response->addInfoMessage('  id: ' . $job->getId());
			$response->addInfoMessage('  status: ' . $job->getStatus());
			$response->addInfoMessage('  name: ' . $job->getName());
			$response->addInfoMessage('  startDate: ' . $this->i18nManager->transDateTime($job->getStartDate()));
			$mDate = $job->getLastModificationDate();
			$response->addInfoMessage('  lastModificationDate: ' . ($mDate ? $this->i18nManager->transDateTime($mDate) : '-'));
			$response->addInfoMessage('  arguments: ' . json_encode($job->getArguments()));
		}
		else
		{
			$response->addInfoMessage('  id: ' . $job->getId() . ', name: ' . $job->getName());
		}
	}
}