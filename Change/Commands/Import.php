<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

/**
 * @name \Change\Commands\Import
 */
class Import
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		//Enable Change storage system
		$event->getApplicationServices()->getStorageManager();

		$fileName = $event->getParam('file');
		$response = $event->getCommandResponse();
		if (substr($fileName, 0, 9) !== 'change://')
		{
			$fileName = $event->getApplication()->getWorkspace()->composeAbsolutePath($fileName);
		}

		if (!file_exists($fileName) || !is_readable($fileName))
		{
			$response->addErrorMessage('File not found: ' . $fileName);
			return;
		}

		$JSON = json_decode(file_get_contents($fileName), true);
		if (!is_array($JSON) || !$JSON)
		{
			$response->addErrorMessage('Invalid JSON File: ' . $fileName);
			return;
		}
		$applicationServices = $event->getApplicationServices();

		$transactionManager = $applicationServices->getTransactionManager();

		$importEngine = new \Change\Synchronization\ImportEngine($event->getApplication(),
			['contextCode' => $event->getParam('context'), 'basePath' => dirname($fileName)]);

		$logging = $event->getApplication()->getLogging();
		$verbose = $event->getParam('verbose');
		$itemIndex = 0;
		foreach (array_chunk($JSON, 10) as $itemsData)
		{
			try
			{
				$transactionManager->begin();

				foreach ($itemsData as $itemData)
				{
					try
					{
						$item = $importEngine->importItem($itemData);
						$message = '(' . $itemData['__model'] . ' ' . $itemData['__id'] . ') [' . $item . ']';
						$logging->info('Import', $message);
						if ($verbose)
						{
							$response->addInfoMessage($message);
						}
					}
					catch (\Change\Synchronization\ImportException $importException)
					{
						if ($verbose)
						{
							$message = 'Error at index ' . $itemIndex;
							$response->addErrorMessage($message);
							$logging->error($message);
							$response->addErrorMessage(json_encode($itemData,
								JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES + JSON_PRETTY_PRINT));
						}
						$message = $importException->getImportError(is_array($itemData) ? $itemData : null);
						$response->addErrorMessage($message);
						$logging->error($message);
					}
				}
				$transactionManager->commit();
				$itemIndex++;
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}
		}
	}
}