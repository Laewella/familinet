<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
require_once(dirname(dirname(dirname(__DIR__))) . '/Change/Application.php');

$application = new \Change\Application();
$application->useSession(false);
$application->start();

class cron
{
	/**
	 * @var \Change\Events\EventManager
	 */
	protected $eventManager;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct($application)
	{
		$this->eventManager = $application->getNewEventManager(['cron']);
		if ($application->getConfiguration('Change/Job/stop'))
		{
			return;
		}
		$this->eventManager->attach('execute',  function($event) {$this->onExecutePrioritized($event);}, 10);
		$this->eventManager->attach('execute',  function($event) {$this->onExecute($event); }, 5);

		$this->eventManager->attach('executeJob', function ($event) { $this->onExecuteJob($event); }, 5);
	}

	public function execute($argv)
	{
		if ($argv && count($argv) === 2 && is_numeric($argv[1]))
		{
			$this->eventManager->trigger('executeJob', $this, ['jobId' => $argv[1]]);
			return;
		}
		$startDate = new \DateTime();
		$this->eventManager->trigger('execute', $this, ['startDate' => $startDate]);
	}

	protected function onExecutePrioritized(\Change\Events\Event $event)
	{
		$prioritizedJobNames = $event->getApplication()->getConfiguration('Change/Job/prioritized');
		if ($prioritizedJobNames && is_array($prioritizedJobNames))
		{
			$logging = $event->getApplication()->getLogging();
			$logging->info('Cron check ', implode(', ', $prioritizedJobNames), ' runnable jobs...');

			$startDate = $event->getParam('startDate');
			$startTimestamp = $startDate->getTimestamp();
			$applicationServices = $event->getApplicationServices();
			$jobManager = $applicationServices->getJobManager();
			$runnableJobIds = $jobManager->getRunnableNamedJobIds($startDate, array_values($prioritizedJobNames));

			$maxExecutionTime = max(60, (int)$event->getApplication()->getConfiguration('Change/Job/maxExecutionTime'));
			if (count($runnableJobIds))
			{
				foreach($runnableJobIds as $jobId)
				{
					$this->executeJobId($jobManager, $jobId, $logging);

					/** @noinspection DisconnectedForeachInstructionInspection */
					$workingTime = (new \DateTime())->getTimestamp() - $startTimestamp;
					if ($workingTime >= $maxExecutionTime)
					{
						$logging->warn('Max Cron Working time : ' . $workingTime . 's');
						$event->stopPropagation();
						break;
					}
				}
			}
		}
	}

	protected function onExecute(\Change\Events\Event $event)
	{
		$startDate = $event->getParam('startDate');
		$startTimestamp = $startDate->getTimestamp();

		$logging = $event->getApplication()->getLogging();
		$logging->info('Cron check runnable jobs...');
		$applicationServices = $event->getApplicationServices();

		$jobManager = $applicationServices->getJobManager();
		$runnableJobIds = $jobManager->getRunnableJobIds($startDate);
		if (count($runnableJobIds))
		{
			$maxExecutionTime = max(60, (int)$event->getApplication()->getConfiguration('Change/Job/maxExecutionTime'));
			foreach($runnableJobIds as $jobId)
			{
				$this->executeJobId($jobManager, $jobId, $logging);

				/** @noinspection DisconnectedForeachInstructionInspection */
				$workingTime = (new \DateTime())->getTimestamp() - $startTimestamp;
				if ($workingTime >= $maxExecutionTime)
				{
					$logging->warn('Max Cron Working time:', $workingTime . 's');
					break;
				}
			}
		}
	}

	protected function onExecuteJob(\Change\Events\Event $event)
	{
		$jobId = $event->getParam('jobId');
		$logging = $event->getApplication()->getLogging();
		$applicationServices = $event->getApplicationServices();
		$jobManager = $applicationServices->getJobManager();
		$this->executeJobId($jobManager, $jobId, $logging);
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 * @param $jobId
	 * @param $logging
	 */
	public function executeJobId(\Change\Job\JobManager $jobManager, $jobId, \Change\Logging\Logging $logging = null)
	{
		$job = $jobManager->getJob($jobId);
		if ($job)
		{
			$startJobTimestamp = (new \DateTime())->getTimestamp();
			$startJobMemory = memory_get_usage(true);

			$logger = $logging->getLoggerByName('job');
			$logger->info('Run: ' . $job->getName() . ' ' . $job->getId());

			$jobManager->run($job);

			$jobDuration = (new \DateTime())->getTimestamp() - $startJobTimestamp;
			$jobMemory = memory_get_usage(true) - $startJobMemory;
			$fmt = \NumberFormatter::create('en_US', \NumberFormatter::DECIMAL);
			$msg = implode(' ', ['Stats:', $job->getName(), $jobId, '-- PID:', getmypid(), 'Status:', $job->getStatus(), 'Duration:', $jobDuration . 's', 'Memory:',
				$fmt->format($jobMemory)]);
			$logger->info($msg, ['userId' => 0]);
		}
		else
		{
			$logging->warn('Invalid job:', $jobId);
		}
	}

}
$cron = new cron($application);
$cron->execute($argv);
