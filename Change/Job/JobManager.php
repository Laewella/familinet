<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Job;

/**
 * @name \Change\Job\JobManager
 */
class JobManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'JobManager';
	const EVENT_PROCESS = 'process';
	const EVENT_THROW = 'throw';

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Transaction\TransactionManager
	 */
	protected $transactionManager;

	/**
	 * @var boolean
	 */
	protected $enabled;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider(\Change\Db\DbProvider $dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @return \Change\Db\DbProvider
	 */
	public function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	protected function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @return $this
	 */
	public function setTransactionManager(\Change\Transaction\TransactionManager $transactionManager)
	{
		$this->transactionManager = $transactionManager;
		return $this;
	}

	/**
	 * @return \Change\Transaction\TransactionManager
	 */
	protected function getTransactionManager()
	{
		return $this->transactionManager;
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/JobManager');
	}

	/**
	 * @api
	 * $step in \Change\Job\JobManager::EVENT_*
	 * @param string $step
	 * @param string $jobName
	 * @return string
	 */
	public static function composeEventName($step, $jobName)
	{
		return $step . '_' . $jobName;
	}

	/**
	 * @return boolean
	 */
	public function getEnabled()
	{
		if ($this->enabled === null)
		{
			$this->enabled = !$this->getApplication()->getConfiguration('Change/Job/stop');
		}
		return $this->enabled;
	}

	/**
	 * @param boolean $enabled
	 * @return $this
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled ? true : false;
		return $this;
	}

	/**
	 * @api
	 * @param JobInterface $job
	 */
	public function run(JobInterface $job)
	{
		if (!$this->getEnabled())
		{
			return;
		}

		if (!$this->setJobRunning($job))
		{
			$logging = $this->getApplication()->getLogging();
			$logging->warn('Job:', $job->getId(), 'not in valid state');
			return;
		}

		$em = $this->getEventManager();
		$event = new Event(static::composeEventName(static::EVENT_PROCESS, $job->getName()), $this, $em->prepareArgs(['job' => $job]));

		try
		{
			$em->triggerEvent($event);
			$status = $event->getParam('executionStatus', JobInterface::STATUS_SUCCESS);
			$arguments = $event->getParam('arguments', null);
			$this->getDbProvider()->setReadOnly(false);
		}
		catch (\Throwable $t)
		{
			$this->getDbProvider()->setReadOnly(false);
			$trace = $this->formatTrace($t->getTrace(), $this->getApplication()->getWorkspace()->projectPath());

			$this->getApplication()->getLogging()->getLoggerByName('job')->err(get_class($t) . ': ' . $job->getName() . ' ' . $job->getId() . ': '
				. $t->getMessage() . ' (' . $t->getCode() . ') Arguments: ' . \json_encode($job->getArguments()) . PHP_EOL . $t->getTraceAsString());

			$this->getApplication()->getLogging()->throwable($t);

			$event->setParam('executionStatus', JobInterface::STATUS_FAILED);

			$arguments = ['Exception' => ['code' => $t->getCode(), 'message' => $t->getMessage(), 'trace' => $trace]];
			$event->setParam('arguments', $arguments);
			$event->setParam('throw', $t);

			$event->setName(static::composeEventName(static::EVENT_THROW, $job->getName()));
			$em->triggerEvent($event);

			$status = $event->getParam('executionStatus', JobInterface::STATUS_FAILED);
			$arguments = $event->getParam('arguments', $arguments);
		}
		$this->updateJobStatus($job, $status, $arguments);
	}

	/**
	 * @param array $trace
	 * @param string $basePath
	 * @return array
	 */
	protected function formatTrace($trace, $basePath)
	{
		if (count($trace) > 50)
		{
			$trace = array_slice($trace, 0, 50);
		}

		foreach ($trace as $key => &$value)
		{
			if (isset($value['args']))
			{
				$value['args'] = $this->formatArgs($value['args']);
			}
			if (isset($value['file']) && strpos($value['file'], $basePath) === 0)
			{
				$value['file'] = substr($value['file'], strlen($basePath) + 1);
			}
		}
		return $trace;
	}

	/**
	 * @param mixed $item
	 * @return array|string
	 */
	protected function formatArgs($item)
	{
		if (is_array($item))
		{
			$result = [];
			foreach ($item as $key => $value)
			{
				$result[$key] = $this->formatArgs($value);
			}
			return $result;
		}
		elseif (is_object($item))
		{
			return 'Object(' . (is_callable([$item, '__toString']) ? $item : str_replace('\\\\', '\\', get_class($item))) . ')';
		}
		return $item;
	}

	/**
	 * @api
	 * @param $name
	 * @param array $argument
	 * @param \DateTime $startDate
	 * @param boolean $startTransaction
	 * @throws \Exception
	 * @return JobInterface
	 */
	public function createNewJob($name, array $argument = null, \DateTime $startDate = null, $startTransaction = false)
	{
		$job = new Job();
		$job->setName($name)
			->setStartDate($startDate ?? new \DateTime())
			->setStatus(JobInterface::STATUS_WAITING);

		if ($argument)
		{
			$job->setArguments($argument);
		}
		else
		{
			$argument = [];
		}
		$transactionManager = $this->getTransactionManager();

		if ($startTransaction || !$transactionManager->count())
		{

			try
			{
				$transactionManager->begin();
				$this->insertJob($job, $argument);
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}
		}
		else
		{
			$this->insertJob($job, $argument);
		}
		return $job;
	}

	/**
	 * @api
	 * @param \Change\Job\JobInterface $job
	 * @throws \Exception
	 */
	public function restartJob($job)
	{
		if (!($job instanceof \Change\Job\JobInterface) || $job->getId() <= 0)
		{
			return;
		}
		$arguments = $job->getArguments();
		if (!is_array($arguments))
		{
			$arguments = [];
		}
		$arguments['reportedAt'] = new \DateTime();
		$this->reportJob($job, $arguments);
	}

	/**
	 * @api
	 * @param \Change\Job\JobInterface $job
	 * @param string $status
	 * @param array|null $arguments
	 * @param \DateTime $lastModificationDate
	 * @throws \Exception
	 */
	public function updateJobStatus($job, $status, array $arguments = null, \DateTime $lastModificationDate = null)
	{
		if ($job->getId() <= 0)
		{
			return;
		}

		if ($lastModificationDate === null)
		{
			$lastModificationDate = new \DateTime();
		}

		$jobArguments = $job->getArguments();
		unset($jobArguments['Exception']);

		$jobArguments = $arguments ? array_merge($jobArguments, $arguments) : $jobArguments;
		$jobArguments = array_filter($jobArguments, function ($v) { return $v !== null; });

		if ($status !== JobInterface::STATUS_RUNNING && $status !== JobInterface::STATUS_SUCCESS)
		{
			if ($status === JobInterface::STATUS_WAITING && isset($arguments['reportedAt']) && $arguments['reportedAt'] instanceof \DateTime)
			{
				$this->reportJob($job, $jobArguments);
				return;
			}
			$status = JobInterface::STATUS_FAILED;
		}
		$transactionManager = $this->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$qb = $this->getDbProvider()->getNewStatementBuilder('updateJobStatus');
			if (!$qb->isCached())
			{
				$fb = $qb->getFragmentBuilder();
				$qb->update('change_job');
				$qb->assign($fb->column('status'), $fb->parameter('status'));
				$qb->assign($fb->column('last_modification_date'), $fb->dateTimeParameter('lastModificationDate'));
				$qb->assign($fb->column('arguments'), $fb->lobParameter('arguments'));
				$qb->where($fb->eq($fb->column('id'), $fb->integerParameter('id')));
			}

			$uq = $qb->updateQuery();
			$uq->bindParameter('status', $status);
			$uq->bindParameter('lastModificationDate', $lastModificationDate);
			$uq->bindParameter('arguments', $jobArguments ? json_encode($jobArguments) : null);
			$uq->bindParameter('id', $job->getId());
			$uq->execute();
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}

		if ($job instanceof Job)
		{
			$job->setStatus($status);
			$job->setLastModificationDate($lastModificationDate);
			if ($jobArguments)
			{
				$job->setArguments($jobArguments);
			}
		}
	}

	/**
	 * @param \Change\Job\JobInterface $job
	 * @return boolean
	 * @throws \Exception
	 */
	protected function setJobRunning($job)
	{
		if ($job->getId() <= 0)
		{
			return false;
		}
		$lastModificationDate = new \DateTime();
		$transactionManager = $this->getTransactionManager();
		$qb = $this->getDbProvider()->getNewStatementBuilder('setJobRunning');
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update('change_job');
			$qb->assign($fb->column('status'), $fb->string(\Change\Job\JobInterface::STATUS_RUNNING));
			$qb->assign($fb->column('last_modification_date'), $fb->dateTimeParameter('lastModificationDate'));
			$qb->assign($fb->column('arguments'), $fb->lobParameter('arguments'));
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('id'), $fb->integerParameter('id')),
				$fb->eq($fb->column('status'), $fb->string(\Change\Job\JobInterface::STATUS_WAITING))
			));
		}

		try
		{
			$transactionManager->begin();
			$uq = $qb->updateQuery();
			$uq->bindParameter('lastModificationDate', $lastModificationDate);
			$uq->bindParameter('id', $job->getId());
			$updated = $uq->execute();
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}

		if ($updated)
		{
			if ($job instanceof Job)
			{
				$job->setStatus(\Change\Job\JobInterface::STATUS_RUNNING);
				$job->setLastModificationDate($lastModificationDate);
			}
			return true;
		}
		return false;
	}

	/**
	 * @api
	 * @param JobInterface $job
	 * @param array $arguments
	 * @throws \Exception
	 */
	protected function reportJob($job, $arguments)
	{
		if ($job->getId() <= 0)
		{
			return;
		}

		$reportedAt = $arguments['reportedAt'];
		unset($arguments['reportedAt']);

		$lastModificationDate = new \DateTime();
		$status = JobInterface::STATUS_WAITING;

		$transactionManager = $this->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$qb = $this->getDbProvider()->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->update('change_job');
			$qb->assign($fb->column('status'), $fb->parameter('status'));
			$qb->assign($fb->column('start_date'), $fb->dateTimeParameter('startDate'));
			$qb->assign($fb->column('last_modification_date'), $fb->dateTimeParameter('lastModificationDate'));
			$qb->assign($fb->column('arguments'), $fb->lobParameter('arguments'));
			$qb->where($fb->eq($fb->column('id'), $fb->integerParameter('id')));

			$uq = $qb->updateQuery();
			$uq->bindParameter('status', $status);
			$uq->bindParameter('startDate', $reportedAt);
			$uq->bindParameter('lastModificationDate', $lastModificationDate);
			$uq->bindParameter('arguments', json_encode($arguments));
			$uq->bindParameter('id', $job->getId());
			$uq->execute();

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}

		if ($job instanceof Job)
		{
			$job->setStatus($status);
			$job->setLastModificationDate($lastModificationDate);
			$job->setStartDate($reportedAt);
			$job->setArguments($arguments);
		}
	}

	/**
	 * @api
	 * @param integer $jobId
	 * @return JobInterface|null
	 */
	public function getJob($jobId)
	{
		$id = (int)$jobId;
		if ($id > 0)
		{
			$qb = $this->getDbProvider()->getNewQueryBuilder('JobManager.getJob');
			if (!$qb->isCached())
			{
				$fb = $qb->getFragmentBuilder();
				$qb->select($fb->column('name'), $fb->column('start_date'), $fb->column('arguments'),
					$fb->column('status'), $fb->column('last_modification_date'));
				$qb->from('change_job');
				$qb->where($fb->eq($fb->column('id'), $fb->integerParameter('id')));
			}

			$sq = $qb->query();
			$sq->bindParameter('id', $id);
			$rc = $sq->getRowsConverter();
			$rc->addStrCol('name', 'status')->addTxtCol('arguments')->addDtCol('start_date', 'last_modification_date');
			$data = $sq->getFirstResult($rc);
			if ($data)
			{
				$job = new Job();
				$job->setId($jobId);
				$job->setName($data['name']);
				$job->setStartDate($data['start_date']);
				if ($arguments = $data['arguments'] ?? null)
				{
					$job->setArguments(json_decode($arguments, true));
				}
				$job->setStatus($data['status']);
				$job->setLastModificationDate($data['last_modification_date']);
				return $job;
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param \DateTime $startDate
	 * @return integer[]
	 */
	public function getRunnableJobIds(\DateTime $startDate = null)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('id'))->from('change_job');
		$qb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('status'), $fb->parameter('status')),
				$fb->lt($fb->column('start_date'), $fb->dateTimeParameter('startDate'))
			));
		$qb->orderAsc($fb->column('id'));
		$sq = $qb->query();
		$sq->bindParameter('status', JobInterface::STATUS_WAITING);
		$startDate = $startDate ?: new \DateTime();
		$startDate->add(new \DateInterval('PT' . (60 - $startDate->format('s')) . 'S'));
		$sq->bindParameter('startDate', $startDate);
		return $sq->getResults($sq->getRowsConverter()->addIntCol('id'));
	}

	/**
	 * @api
	 * @param \DateTime $startDate
	 * @param string[] $names
	 * @return \integer[]
	 */
	public function getRunnableNamedJobIds(\DateTime $startDate = null, array $names)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('id'))->from('change_job');
		$qb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('status'), $fb->parameter('status')),
				$fb->lt($fb->column('start_date'), $fb->dateTimeParameter('startDate')),
				$fb->in($fb->column('name'), $names)
			));
		$qb->orderAsc($fb->column('id'));
		$sq = $qb->query();
		$sq->bindParameter('status', JobInterface::STATUS_WAITING);
		$startDate = $startDate ?: new \DateTime();
		$startDate->add(new \DateInterval('PT' . (60 - $startDate->format('s')) . 'S'));
		$sq->bindParameter('startDate', $startDate);
		return $sq->getResults($sq->getRowsConverter()->addIntCol('id'));
	}

	/**
	 * @api
	 * @param string $status
	 * @return integer
	 */
	public function getCountJobIds($status = JobInterface::STATUS_WAITING)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->alias($fb->func('count', $fb->column('id')), 'count'))
			->from('change_job');
		$qb->where($fb->eq($fb->column('status'), $fb->parameter('status')));
		$sq = $qb->query();
		$sq->bindParameter('status', $status);
		return $sq->getFirstResult($sq->getRowsConverter()->addIntCol('count'));
	}

	/**
	 * @api
	 * @param string $status
	 * @param int $offset
	 * @param int $limit
	 * @param string|null $sortColumn
	 * @param boolean $desc
	 * @return integer[]
	 */
	public function getJobIds($status = JobInterface::STATUS_WAITING, $offset = 0, $limit = 20, $sortColumn = null, $desc = true)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('id'))->from('change_job');
		$qb->where($fb->eq($fb->column('status'), $fb->parameter('status')));
		if ($desc)
		{
			$qb->orderDesc($fb->column($sortColumn ?: 'start_date'));
		}
		else
		{
			$qb->orderAsc($fb->column($sortColumn ?: 'start_date'));
		}
		$sq = $qb->query();
		$sq->bindParameter('status', $status);
		$sq->setStartIndex($offset);
		$sq->setMaxResults($limit);
		return $sq->getResults($sq->getRowsConverter()->addIntCol('id'));
	}

	/**
	 * @api
	 * @param string $name
	 * @param integer $offset
	 * @param integer $limit
	 * @param string|null $sortColumn
	 * @param boolean $desc
	 * @return integer[]
	 */
	public function getJobIdsByName($name, $offset = 0, $limit = 20, $sortColumn = null, $desc = true)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('id'))->from('change_job');
		$qb->where($fb->eq($fb->column('name'), $fb->parameter('name')));
		if ($desc)
		{
			$qb->orderDesc($fb->column($sortColumn ?: 'start_date'));
		}
		else
		{
			$qb->orderAsc($fb->column($sortColumn ?: 'start_date'));
		}
		$sq = $qb->query();
		$sq->bindParameter('name', $name);
		$sq->setStartIndex($offset);
		$sq->setMaxResults($limit);
		return $sq->getResults($sq->getRowsConverter()->addIntCol('id'));
	}

	/**
	 * @api
	 * @param JobInterface $job
	 * @throws \Exception
	 */
	public function deleteJob(JobInterface $job)
	{
		$transactionManager = $this->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$qb = $this->getDbProvider()->getNewStatementBuilder('JobManager.deleteJob');
			$fb = $qb->getFragmentBuilder();
			$qb->delete('change_job');
			$qb->where($fb->eq($fb->column('id'), $fb->integerParameter('id')));
			$dq = $qb->deleteQuery();
			$dq->bindParameter('id', $job->getId());
			$dq->execute();

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param Job $job
	 * @param array $argument
	 */
	protected function insertJob($job, array $argument)
	{
		$argumentJSON = $argument ? json_encode($argument) : null;
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->insert('change_job', $fb->column('name'),
			$fb->column('start_date'),
			$fb->column('arguments'),
			$fb->column('status'));

		$qb->addValues($fb->parameter('name'),
			$fb->dateTimeParameter('startDate'),
			$fb->lobParameter('arguments'),
			$fb->parameter('status'));
		$iq = $qb->insertQuery();

		$iq->bindParameter('name', $job->getName());
		$iq->bindParameter('startDate', $job->getStartDate());
		$iq->bindParameter('arguments', $argumentJSON);
		$iq->bindParameter('status', $job->getStatus());
		$iq->execute();
		$job->setId((int)$iq->getDbProvider()->getLastInsertId('change_job'));
		$this->getLogging()->info('New Job: ' . $job->getName() . ', ' . $job->getId() . ', ' . $argumentJSON);
	}

	/**
	 * Need started Transaction if not dry run.
	 * @api
	 * @param string $status
	 * @param \DateTime $before
	 * @param boolean $dryRun
	 * @return integer The job count.
	 */
	public function cleanup($status, \DateTime $before, $dryRun = true)
	{
		if ($status !== JobInterface::STATUS_SUCCESS && $status !== JobInterface::STATUS_FAILED)
		{
			return 0;
		}

		if ($dryRun)
		{
			$qb = $this->getDbProvider()->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->count('id'), 'count'))->from('change_job');
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->column('status'), $fb->parameter('status')),
					$fb->lte($fb->column('start_date'), $fb->dateTimeParameter('startDate'))
				));
			$sq = $qb->query();
			$sq->bindParameter('status', $status);
			$sq->bindParameter('startDate', $before);
			return (int)$sq->getFirstResult($sq->getRowsConverter()->addIntCol('count')->singleColumn('count'));
		}
		else
		{
			$qb = $this->getDbProvider()->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->delete('change_job');
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->column('status'), $fb->parameter('status')),
					$fb->lte($fb->column('start_date'), $fb->dateTimeParameter('startDate'))
				));
			$dq = $qb->deleteQuery();
			$dq->bindParameter('status', $status);
			$dq->bindParameter('startDate', $before);
			return (int)$dq->execute();
		}
	}

	/**
	 * @api
	 * @param string $status
	 * @param \DateTime $before
	 * @param boolean $dryRun
	 * @return integer[] The job ids.
	 */
	public function restart($status, \DateTime $before, $dryRun = true)
	{
		if ($status !== JobInterface::STATUS_RUNNING && $status !== JobInterface::STATUS_FAILED)
		{
			return 0;
		}

		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('id'))->from('change_job');
		$qb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('status'), $fb->parameter('status')),
				$fb->lte($fb->column('start_date'), $fb->dateTimeParameter('startDate'))
			));
		$qb->orderAsc($fb->column('id'));
		$sq = $qb->query();
		$sq->bindParameter('status', $status);
		$sq->bindParameter('startDate', $before);
		$jobIds = $sq->getResults($sq->getRowsConverter()->addIntCol('id')->singleColumn('id'));

		if (!$dryRun)
		{
			foreach ($jobIds as $id)
			{
				$job = $this->getJob($id);
				$this->restartJob($job);
			}
		}

		return $jobIds;
	}
}