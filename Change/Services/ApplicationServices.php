<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Services;

/**
 * @name \Change\Services\ApplicationServices
 */
class ApplicationServices extends \Zend\ServiceManager\ServiceManager
{
	use ServicesCapableTrait;

	/**
	 * @return array<alias => className>
	 */
	protected function loadInjectionClasses()
	{
		$classes = $this->application->getConfiguration('Change/Services');
		return is_array($classes) ? $classes : [];
	}

	/**
	 * @param \Change\Application $application
	 */
	public function __construct($application)
	{
		$this->setApplication($application);
		parent::__construct(['shared_by_default' => true]);

		//DbProvider : Application
		$class = $this->getInjectedClassName('DbProvider', \Change\Db\Mysql\DbProvider::class);
		$this->setAlias('DbProvider', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Db\Mysql\DbProvider $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//TransactionManager : Application
		$class = $this->getInjectedClassName('TransactionManager', \Change\Transaction\TransactionManager::class);
		$this->setAlias('TransactionManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Transaction\TransactionManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//PluginManager : Application
		$class = $this->getInjectedClassName('PluginManager', \Change\Plugins\PluginManager::class);
		$this->setAlias('PluginManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var  \Change\Plugins\PluginManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//I18nManager : Application, PluginManager, CacheManager
		$class = $this->getInjectedClassName('I18nManager', \Change\I18n\I18nManager::class);
		$this->setAlias('I18nManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\I18n\I18nManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			$manager->setPluginManager($container->get('PluginManager'));
			return $manager;
		});

		//StorageManager : Application
		$class = $this->getInjectedClassName('StorageManager', \Change\Storage\StorageManager::class);
		$this->setAlias('StorageManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Storage\StorageManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});


		//MailManager : Application
		$class = $this->getInjectedClassName('MailManager', \Change\Mail\MailManager::class);
		$this->setAlias('MailManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Mail\MailManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			$manager->setStorageManager($container->get('StorageManager'));
			return $manager;
		});


		//SmsManager : Application
		$class = $this->getInjectedClassName('SmsManager', \Change\Sms\SmsManager::class);
		$this->setAlias('SmsManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Sms\SmsManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//SviManager : Application
		$class = $this->getInjectedClassName('SviManager', \Change\Svi\SviManager::class);
		$this->setAlias('SviManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Svi\SviManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//ModelManager : Application, PluginManager
		$class = $this->getInjectedClassName('ModelManager', \Change\Documents\ModelManager::class);
		$this->setAlias('ModelManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Documents\ModelManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setPluginManager($container->get('PluginManager'));
			return $manager;
		});

		//DocumentManager : Application, ModelManager, DbProvider, I18nManager
		$class = $this->getInjectedClassName('DocumentManager', \Change\Documents\DocumentManager::class);
		$this->setAlias('DocumentManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Documents\DocumentManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setModelManager($container->get('ModelManager'))
				->setDbProvider($container->get('DbProvider'))
				->setI18nManager($container->get('I18nManager'));
			return $manager;
		});

		//DocumentCodeManager : DbProvider, DocumentManager, TransactionManager
		$class = $this->getInjectedClassName('DocumentCodeManager', \Change\Documents\DocumentCodeManager::class);
		$this->setAlias('DocumentCodeManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Documents\DocumentCodeManager $manager */
			$manager = new $requestedName;
			$manager->setTransactionManager($container->get('TransactionManager'))
				->setDocumentManager($container->get('DocumentManager'))
				->setDbProvider($container->get('DbProvider'));
			return $manager;
		});


		//TreeManager : ModelManager, DocumentManager, DbProvider, CacheManager
		$class = $this->getInjectedClassName('TreeManager', \Change\Documents\TreeManager::class);
		$this->setAlias('TreeManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Documents\TreeManager $manager */
			$manager = new $requestedName;
			$manager->setModelManager($container->get('ModelManager'))
				->setDocumentManager($container->get('DocumentManager'))
				->setDbProvider($container->get('DbProvider'))
				->setCacheManager($container->get('CacheManager'));
			return $manager;
		});


		//ConstraintsManager : I18nManager
		$class = $this->getInjectedClassName('ConstraintsManager', \Change\Documents\Constraints\ConstraintsManager::class);
		$this->setAlias('ConstraintsManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Documents\Constraints\ConstraintsManager $manager */
			$manager = new $requestedName;
			$manager->setI18nManager($container->get('I18nManager'));
			return $manager;
		});



		//CollectionManager : Application
		$class = $this->getInjectedClassName('CollectionManager', \Change\Collection\CollectionManager::class);
		$this->setAlias('CollectionManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Collection\CollectionManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//JobManager : Application, DbProvider, TransactionManager
		$class = $this->getInjectedClassName('JobManager', \Change\Job\JobManager::class);
		$this->setAlias('JobManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Job\JobManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbProvider($container->get('DbProvider'))
				->setTransactionManager($container->get('TransactionManager'));
			return $manager;
		});



		//BlockManager: Application, CacheManager
		$class = $this->getInjectedClassName('BlockManager', \Change\Presentation\Blocks\BlockManager::class);
		$this->setAlias('BlockManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Presentation\Blocks\BlockManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setCacheManager($container->get('CacheManager'));
			return $manager;
		});

		//RichTextManager : Application
		$class = $this->getInjectedClassName('RichTextManager', \Change\Presentation\RichText\RichTextManager::class);
		$this->setAlias('RichTextManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Presentation\RichText\RichTextManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//ThemeManager : Application
		$class = $this->getInjectedClassName('ThemeManager', \Change\Presentation\Themes\ThemeManager::class);
		$this->setAlias('ThemeManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Presentation\Themes\ThemeManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//TemplateManager : Application, ThemeManager
		$class = $this->getInjectedClassName('TemplateManager', \Change\Presentation\Templates\TemplateManager::class);
		$this->setAlias('TemplateManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Presentation\Templates\TemplateManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setThemeManager($container->get('ThemeManager'));
			return $manager;
		});


		//PageManager : Application, CacheManager, AuthenticationManager, PermissionsManager
		$class = $this->getInjectedClassName('PageManager', \Change\Presentation\Pages\PageManager::class);
		$this->setAlias('PageManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Presentation\Pages\PageManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setCacheManager($container->get('CacheManager'))
				->setAuthenticationManager($container->get('AuthenticationManager'))
				->setPermissionsManager($container->get('PermissionsManager'));
			return $manager;
		});

		//WorkflowManager : Application
		$class = $this->getInjectedClassName('WorkflowManager', \Change\Workflow\WorkflowManager::class);
		$this->setAlias('WorkflowManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Workflow\WorkflowManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});


		//AuthenticationManager : Application
		$class = $this->getInjectedClassName('AuthenticationManager', \Change\User\AuthenticationManager::class);
		$this->setAlias('AuthenticationManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\User\AuthenticationManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//ProfileManager : Application
		$class = $this->getInjectedClassName('ProfileManager', \Change\User\ProfileManager::class);
		$this->setAlias('ProfileManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\User\ProfileManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//PermissionsManager : DbProvider, TransactionManager
		$class = $this->getInjectedClassName('PermissionsManager', \Change\Permissions\PermissionsManager::class);
		$this->setAlias('PermissionsManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Permissions\PermissionsManager $manager */
			$manager = new $requestedName;
			$manager->setDbProvider($container->get('DbProvider'))
				->setTransactionManager($container->get('TransactionManager'));
			return $manager;
		});




		//OAuthManager : Application, DbProvider, TransactionManager
		$class = $this->getInjectedClassName('OAuthManager', \Change\Http\OAuth\OAuthManager::class);
		$this->setAlias('OAuthManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Http\OAuth\OAuthManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbProvider($container->get('DbProvider'))
				->setTransactionManager($container->get('TransactionManager'));
			return $manager;
		});

		//PathRuleManager : Application, DbProvider
		$class = $this->getInjectedClassName('PathRuleManager', \Change\Http\Web\PathRuleManager::class);
		$this->setAlias('PathRuleManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Http\Web\PathRuleManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbProvider($container->get('DbProvider'));
			return $manager;
		});


		//CacheManager : Application
		$class = $this->getInjectedClassName('CacheManager', \Change\Cache\CacheManager::class);
		$this->setAlias('CacheManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Change\Cache\CacheManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});
	}

	public function shutdown()
	{
		$this->application = null;
	}

	/**
	 * @api
	 * @return \Change\Logging\Logging
	 */
	public function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @api
	 * @return \Change\Db\DbProvider
	 */
	public function getDbProvider()
	{
		return $this->get('DbProvider');
	}

	/**
	 * @api
	 * @return \Change\Transaction\TransactionManager
	 */
	public function getTransactionManager()
	{
		return $this->get('TransactionManager');
	}

	/**
	 * @api
	 * @return \Change\I18n\I18nManager
	 */
	public function getI18nManager()
	{
		return $this->get('I18nManager');
	}

	/**
	 * @api
	 * @return \Change\Plugins\PluginManager
	 */
	public function getPluginManager()
	{
		return $this->get('PluginManager');
	}

	/**
	 * @api
	 * @return \Change\Storage\StorageManager
	 */
	public function getStorageManager()
	{
		return $this->get('StorageManager');
	}

	/**
	 * @api
	 * @return \Change\Mail\MailManager
	 */
	public function getMailManager()
	{
		return $this->get('MailManager');
	}

	/**
	 * @api
	 * @return \Change\Sms\SmsManager
	 */
	public function getSmsManager()
	{
		return $this->get('SmsManager');
	}

	/**
	 * @api
	 * @return \Change\Svi\SviManager
	 */
	public function getSviManager()
	{
		return $this->get('SviManager');
	}

	/**
	 * @api
	 * @return \Change\Documents\ModelManager
	 */
	public function getModelManager()
	{
		return $this->get('ModelManager');
	}

	/**
	 * @api
	 * @return \Change\Documents\DocumentManager
	 */
	public function getDocumentManager()
	{
		return $this->get('DocumentManager');
	}

	/**
	 * @api
	 * @return \Change\Documents\DocumentCodeManager
	 */
	public function getDocumentCodeManager()
	{
		return $this->get('DocumentCodeManager');
	}

	/**
	 * @api
	 * @return \Change\Documents\TreeManager
	 */
	public function getTreeManager()
	{
		return $this->get('TreeManager');
	}

	/**
	 * @api
	 * @return \Change\Documents\Constraints\ConstraintsManager
	 */
	public function getConstraintsManager()
	{
		return $this->get('ConstraintsManager');
	}

	/**
	 * @api
	 * @return \Change\Collection\CollectionManager
	 */
	public function getCollectionManager()
	{
		return $this->get('CollectionManager');
	}

	/**
	 * @api
	 * @return \Change\Job\JobManager
	 */
	public function getJobManager()
	{
		return $this->get('JobManager');
	}

	/**
	 * @api
	 * @return \Change\Presentation\Blocks\BlockManager
	 */
	public function getBlockManager()
	{
		return $this->get('BlockManager');
	}

	/**
	 * @api
	 * @return \Change\Presentation\RichText\RichTextManager
	 */
	public function getRichTextManager()
	{
		return $this->get('RichTextManager');
	}

	/**
	 * @api
	 * @return \Change\Presentation\Themes\ThemeManager
	 */
	public function getThemeManager()
	{
		return $this->get('ThemeManager');
	}

	/**
	 * @api
	 * @return \Change\Presentation\Templates\TemplateManager
	 */
	public function getTemplateManager()
	{
		return $this->get('TemplateManager');
	}

	/**
	 * @api
	 * @return \Change\Presentation\Pages\PageManager
	 */
	public function getPageManager()
	{
		return $this->get('PageManager');
	}

	/**
	 * @api
	 * @return \Change\Workflow\WorkflowManager
	 */
	public function getWorkflowManager()
	{
		return $this->get('WorkflowManager');
	}

	/**
	 * @api
	 * @return \Change\User\AuthenticationManager
	 */
	public function getAuthenticationManager()
	{
		return $this->get('AuthenticationManager');
	}

	/**
	 * @api
	 * @return \Change\User\ProfileManager
	 */
	public function getProfileManager()
	{
		return $this->get('ProfileManager');
	}

	/**
	 * @api
	 * @return \Change\Permissions\PermissionsManager
	 */
	public function getPermissionsManager()
	{
		return $this->get('PermissionsManager');
	}

	/**
	 * @api
	 * @return \Change\Http\OAuth\OAuthManager
	 */
	public function getOAuthManager()
	{
		return $this->get('OAuthManager');
	}

	/**
	 * @api
	 * @return \Change\Http\Web\PathRuleManager
	 */
	public function getPathRuleManager()
	{
		return $this->get('PathRuleManager');
	}

	/**
	 * @api
	 * @return \Change\Cache\CacheManager
	 */
	public function getCacheManager()
	{
		return $this->get('CacheManager');
	}
}