<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Cache;

/**
 * @name \Change\Cache\SimpleMemoryOptions
 */
class SimpleMemoryOptions
{
	/**
	 * @var \Change\Cache\SimpleMemoryAdapter
	 */
	protected $adapter;

	/**
	 * @var integer
	 */
	protected $ttl = 0;

	/**
	 * @var string
	 */
	protected $namespace = '';

	/**
	 * @param array $options
	 * @param \Change\Cache\SimpleMemoryAdapter $adapter
	 */
	public function __construct(array $options, \Change\Cache\SimpleMemoryAdapter $adapter)
	{
		$this->adapter = $adapter;
		if (isset($options['namespace']) && $options['namespace'] && is_string($options['namespace']))
		{
			$this->setNamespace(trim($options['namespace']));
		}
	}

	/**
	 * @return \Change\Cache\SimpleMemoryAdapter
	 */
	public function getAdapter()
	{
		return $this->adapter;
	}

	/**
	 * @param SimpleRedisAdapter $adapter
	 * @return $this
	 */
	public function setAdapter($adapter)
	{
		$this->adapter = $adapter;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getTtl()
	{
		return $this->ttl;
	}

	/**
	 * @param int $ttl
	 * @return $this
	 */
	public function setTtl($ttl)
	{
		$this->ttl = $ttl;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNamespace()
	{
		return $this->namespace;
	}

	/**
	 * @param string $namespace
	 * @return $this
	 */
	public function setNamespace($namespace)
	{
		return $this;
	}
}