<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Images;

/**
 * @name \Change\Presentation\Images\ImagickResizerEngine
 */
class ImagickResizerEngine
{
	/**
	 * @param $path
	 * @return array
	 */
	public function getImageSize($path)
	{
		$imagick = new \Imagick();
		$blob = file_get_contents($path);
		$imagick->readImageBlob($blob);
		$returnValue = $imagick->valid() ? ['width' => $imagick->getImageWidth(), 'height' => $imagick->getImageHeight()] :
			['height' => null, 'width' => null];
		return $returnValue;
	}

	/**
	 * @param string $inputFileName
	 * @param string $formattedFileName
	 * @param integer $maxWidth
	 * @param integer $maxHeight
	 * @param string $compression
	 */
	public function resize($inputFileName, $formattedFileName, $maxWidth, $maxHeight, $compression)
	{
		switch ($compression)
		{
			case 'HIGH' :
				$quality = 85;
				break;
			case 'MEDIUM' :
				$quality = 90;
				break;
			case 'LOW' :
				$quality = 95;
				break;
			default :
				$quality = 100;
		}

		$imagick = new \Imagick();
		$blob = file_get_contents($inputFileName);
		$imagick->readImageBlob($blob);
		if (!$imagick->valid())
		{
			copy($inputFileName, $formattedFileName);
			return;
		}
		$origWidth = $imagick->getImageWidth();
		$origHeight = $imagick->getImageHeight();
		if ($quality != 100)
		{
			$imagick->setImageCompressionQuality(min($quality, $imagick->getImageCompressionQuality()));
		}
		list ($width, $height) = $this->computeImageSize($origWidth, $origHeight, $maxWidth, $maxHeight);
		if ($width == $origWidth && $height == $origHeight)
		{
			if (!file_put_contents($formattedFileName, $imagick))
			{
				/** @noinspection PhpUsageOfSilenceOperatorInspection */
				@unlink($formattedFileName);
			}
			return;
		}
		if ($imagick->getNumberImages() > 1)
		{
			$imagick = $imagick->coalesceImages();
			foreach ($imagick as $frame)
			{
				$frame->thumbnailImage($width, $height, true);
				$frame->setImagePage($width, $height, 0, 0);
			}
			if (!file_put_contents($formattedFileName, $imagick->getImagesBlob()))
			{
				/** @noinspection PhpUsageOfSilenceOperatorInspection */
				@unlink($formattedFileName);
			}
		}
		else
		{
			$imagick->thumbnailImage($width, $height, true);
			if (!file_put_contents($formattedFileName, $imagick))
			{
				/** @noinspection PhpUsageOfSilenceOperatorInspection */
				@unlink($formattedFileName);
			}
		}
	}

	/**
	 * @param $originalWidth
	 * @param $originalHeight
	 * @param $maxWidth
	 * @param $maxHeight
	 * @return array
	 */
	protected function computeImageSize($originalWidth, $originalHeight, $maxWidth, $maxHeight)
	{
		$resourceWidth = $originalWidth;
		$resourceHeight = $originalHeight;
		if ($maxWidth && ($originalWidth > $maxWidth))
		{
			$resourceWidth = $maxWidth;
			$resourceHeight = $resourceWidth * $originalHeight / $originalWidth;
		}

		if ($maxHeight && ($resourceHeight > $maxHeight))
		{
			$resourceHeight = $maxHeight;
			$resourceWidth = $resourceHeight * $originalWidth / $originalHeight;
		}
		$resourceWidth = round($resourceWidth);
		$resourceHeight = round($resourceHeight);
		return [min($resourceWidth, $originalWidth), min($resourceHeight, $originalHeight)];
	}
}