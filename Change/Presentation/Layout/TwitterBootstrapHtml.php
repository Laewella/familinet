<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Layout;

/**
 * @name \Change\Presentation\Layout\TwitterBootstrapHtml
 */
class TwitterBootstrapHtml
{
	/**
	 * @var string
	 */
	protected $prefixKey = '<!-- ';

	/**
	 * @var string
	 */
	protected $suffixKey = ' -->';

	/**
	 * @var bool
	 */
	protected $fluid = true;

	/**
	 * @param string $prefixKey
	 * @return $this
	 */
	public function setPrefixKey($prefixKey)
	{
		$this->prefixKey = $prefixKey;
		return $this;
	}

	/**
	 * @param string $suffixKey
	 * @return $this
	 */
	public function setSuffixKey($suffixKey)
	{
		$this->suffixKey = $suffixKey;
		return $this;
	}

	/**
	 * @param \Change\Presentation\Layout\Block $item
	 * @return null
	 */
	public function getBlockClass(\Change\Presentation\Layout\Block $item)
	{
		$vi = $item->getVisibility();
		if ($vi == 'raw')
		{
			return 'raw';
		}

		$classes = ['block'];

		$class = $item->getClass();
		if ($class)
		{
			$classes[] = $class;
		}

		if ($vi)
		{
			$visibilities = str_split($vi, 1);
			$display = $item->getDisplay() ?: 'block';
			foreach ($visibilities as $visibility)
			{
				switch ($visibility)
				{
					case 'X':
						$classes[] = 'visible-xs-' . $display;
						break;
					case 'S':
						$classes[] = 'visible-sm-' . $display;
						break;
					case 'M':
						$classes[] = 'visible-md-' . $display;
						break;
					case 'L':
						$classes[] = 'visible-lg-' . $display;
						break;
				}
			}
		}

		return implode(' ', $classes);
	}

	/**
	 * @param \Change\Presentation\Layout\Layout $templateLayout
	 * @param \Change\Presentation\Layout\Layout $pageLayout
	 * @param Callable $callableBlockHtml
	 * @return array
	 */
	public function getHtmlParts($templateLayout, $pageLayout, $callableBlockHtml)
	{
		$prefixKey = $this->prefixKey;
		$suffixKey = $this->suffixKey;

		$twigLayout = [];
		foreach ($templateLayout->getItems() as $item)
		{
			$twigPart = null;
			if ($item instanceof \Change\Presentation\Layout\Block)
			{
				$twigPart = $callableBlockHtml($item);
			}
			elseif ($item instanceof \Change\Presentation\Layout\Container)
			{
				$container = $pageLayout->getById($item->getId());
				if ($container instanceof \Change\Presentation\Layout\Container)
				{
					$twigPart = $this->getItemHtml($container, $callableBlockHtml);
				}
			}

			if ($twigPart)
			{
				$twigLayout[$prefixKey . $item->getId() . $suffixKey] = $twigPart;
			}
		}
		return $twigLayout;
	}

	/**
	 * @return array
	 */
	public function getResourceParts()
	{
		/** @noinspection HtmlUnknownTarget */
		$cssHead = '{% for cssAsset in pageResult.getCssAssets() %}
	<link rel="stylesheet" type="text/css" href="{{ resourceURL(cssAsset) }}" />
{% endfor %}';

		/** @noinspection HtmlUnknownTarget */
		$jsFooter = '{% for jsFooter in pageResult.getJsAssets() %}
	<script type="text/javascript" src="{{ resourceURL(jsFooter) }}"></script>
{% endfor %}';
		return ['<!-- cssHead -->' => $cssHead, '<!-- jsFooter -->' => $jsFooter];
	}

	/**
	 * @param \Change\Presentation\Layout\Item $item
	 * @param Callable $callableTwigBlock
	 * @return string|null
	 */
	protected function getItemHtml($item, $callableTwigBlock)
	{
		if ($item instanceof \Change\Presentation\Layout\Block)
		{
			return $callableTwigBlock($item);
		}

		$innerHTML = '';
		foreach ($item->getItems() as $childItem)
		{
			$innerHTML .= $this->getItemHtml($childItem, $callableTwigBlock);
		}

		$class = $item->getClass();
		$classes = $class ? (' ' . $class) : '';

		if ($item instanceof \Change\Presentation\Layout\Cell)
		{
			$gridMode = 'md';
			switch ($item->getDisplayColumnsFrom())
			{
				case 'X':
					$gridMode = 'xs';
					break;
				case 'S':
					$gridMode = 'sm';
					break;
				case 'M':
					$gridMode = 'md';
					break;
				case 'L':
					$gridMode = 'lg';
					break;
			}
			if ($item->getOffset())
			{
				$classes = ' col-' . $gridMode . '-offset-' . $item->getOffset() . $classes;
			}
			$classes = 'col-' . $gridMode . '-' . $item->getSize() . $classes;
			return '<div class="' . $classes . '" data-id="' . $item->getId() . '">' . $innerHTML . '</div>';
		}
		elseif ($item instanceof \Change\Presentation\Layout\Row)
		{
			return '<div class="row' . $classes . '" data-id="' . $item->getId() . '" data-grid="' . $item->getGrid() . '">' . $innerHTML . '</div>';
		}
		elseif ($item instanceof \Change\Presentation\Layout\Container)
		{
			return '<div class="editable-zone' . $classes . '" data-id="' . $item->getId() . '">' . $innerHTML . '</div>';
		}
		return $innerHTML ?: null;
	}
}