<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Layout;

/**
 * @package Change\Presentation\Layout
 * @name \Change\Presentation\Layout\Layout
 */
class Layout
{
	/**
	 * @var \Change\Presentation\Layout\Item[]
	 */
	protected $items = [];

	/**
	 * @var string|null
	 */
	protected $defaultDisplayColumnsFrom;

	/**
	 * @param array $array
	 * @param string|null $defaultDisplayColumnsFrom
	 */
	public function __construct(array $array = null, $defaultDisplayColumnsFrom = null)
	{
		$this->defaultDisplayColumnsFrom = $defaultDisplayColumnsFrom;
		if ($array)
		{
			$this->items = $this->fromArray($array);
		}
	}

	/**
	 * @return \Change\Presentation\Layout\Item[]
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param array $items
	 */
	public function setItems(array $items)
	{
		$this->items = [];
		foreach ($items as $value)
		{
			if ($value instanceof \Change\Presentation\Layout\Item)
			{
				$this->items[$value->getId()] = $value;
			}
		}
	}

	/**
	 * @param \Change\Presentation\Layout\Item $item
	 */
	public function addItem(Item $item)
	{
		$this->items[$item->getId()] = $item;
	}

	/**
	 * @param string $type
	 * @return \Change\Presentation\Layout\Item[]
	 */
	public function getItemsByType($type)
	{
		$result = [];
		foreach ($this->items as $item)
		{
			/* @var $item \Change\Presentation\Layout\Item */
			$result = array_merge($result, $item->getItemsByType($type));
		}
		return $result;
	}

	/**
	 * @param string $id
	 * @return \Change\Presentation\Layout\Item|null
	 */
	public function getById($id)
	{
		foreach ($this->items as $item)
		{
			$result = $item->getById($id);
			if ($result)
			{
				return $result;
			}
		}
		return null;
	}

	/**
	 * @return \Change\Presentation\Layout\Block[]
	 */
	public function getBlocks()
	{
		return $this->getItemsByType('block');
	}

	/**
	 * @param string $blocId
	 * @return \Change\Presentation\Layout\Block|null
	 */
	public function getBlockById($blocId)
	{
		foreach ($this->getBlocks() as $block)
		{
			if ($block->getId() == $blocId)
			{
				return $block;
			}
		}
		return null;
	}

	/**
	 * @return \Change\Presentation\Layout\Container[]
	 */
	public function getContainers()
	{
		return $this->getItemsByType('container');
	}

	/**
	 * @param array $array
	 * @param string|null $idPrefix
	 * @return \Change\Presentation\Layout\Item[]
	 */
	public function fromArray(array $array, $idPrefix = null)
	{
		$result = [];
		foreach ($array as $key => $data)
		{
			if (isset($data['idPrefix']))
			{
				$idPrefix = $data['idPrefix'];
			}
			elseif ($idPrefix)
			{
				$data['idPrefix'] = $idPrefix;
			}

			$type = $data['type'];
			$id = $data['id'];
			$item = $this->getNewItem($type, $id);
			if ($item === null)
			{
				continue;
			}
			$item->initialize($data);
			if (isset($data['items']) && count($data['items']))
			{
				$children = $this->fromArray($data['items']);
				if ($item instanceof \Change\Presentation\Layout\Row)
				{
					$displayColumnsFrom = $item->getDisplayColumnsFrom() ?: $this->defaultDisplayColumnsFrom;
					foreach ($children as $child)
					{
						/** @var \Change\Presentation\Layout\Cell $child */
						$child->setDisplayColumnsFrom($displayColumnsFrom);
					}
				}
				$item->setItems($children);
			}
			else
			{
				$item->setItems([]);
			}
			$result[$key] = $item;
		}
		return $result;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$result = [];
		foreach ($this->items as $key => $item)
		{
			if ($item instanceof \Change\Presentation\Layout\Item)
			{
				$result[$key] = $item->toArray();
			}
		}
		return $result;
	}

	/**
	 * @param string $type
	 * @param string $id
	 * @throws \InvalidArgumentException
	 * @return \Change\Presentation\Layout\Item
	 */
	public function getNewItem($type, $id)
	{
		switch ($type)
		{
			case 'container':
				$item = new \Change\Presentation\Layout\Container();
				break;
			case 'row':
				$item = new \Change\Presentation\Layout\Row();
				break;
			case 'cell':
				$item = new \Change\Presentation\Layout\Cell();
				break;
			case 'block':
				$item = new \Change\Presentation\Layout\Block();
				break;
			case 'block-chooser':
				return null;
			default:
				throw new \InvalidArgumentException('Argument 1 must be a valid type', 999999);
		}

		$item->setId($id);
		return $item;
	}
}