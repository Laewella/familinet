<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Templates\Twig;

/**
 * @name \Change\Presentation\Templates\Twig\Extension
 */
class Extension implements \Twig_ExtensionInterface
{
	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Configuration\Configuration|null
	 */
	protected $configuration;

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param \Change\Configuration\Configuration $configuration
	 */
	public function __construct(\Change\I18n\I18nManager $i18nManager, \Change\Configuration\Configuration $configuration = null)
	{
		$this->i18nManager = $i18nManager;
		$this->configuration = $configuration;
	}

	/**
	 * Returns the name of the extension.
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'Change';
	}

	/**
	 * Initializes the runtime environment.
	 * This is where you can load some file that contains filter functions for instance.
	 * @param \Twig_Environment $environment The current Twig_Environment instance
	 */
	public function initRuntime(\Twig_Environment $environment)
	{
	}

	/**
	 * Returns the token parser instances to add to the existing list.
	 * @return array An array of Twig_TokenParserInterface or Twig_TokenParserBrokerInterface instances
	 */
	public function getTokenParsers()
	{
		return [];
	}

	/**
	 * Returns the node visitor instances to add to the existing list.
	 * @return array An array of Twig_NodeVisitorInterface instances
	 */
	public function getNodeVisitors()
	{
		return [];
	}

	/**
	 * Returns a list of filters to add to the existing list.
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new \Twig_SimpleFilter('transDate', [$this, 'transDate']),
			new \Twig_SimpleFilter('transDateTime', [$this, 'transDateTime']),
			new \Twig_SimpleFilter('formatDate', [$this, 'formatDate']),
			new \Twig_SimpleFilter('boolean', [$this, 'boolean']),
			new \Twig_SimpleFilter('float', [$this, 'float']),
			new \Twig_SimpleFilter('integer', [$this, 'integer']),
			new \Twig_SimpleFilter('jsPackedString', [$this, 'jsPackedString'])
		];
	}

	/**
	 * Returns a list of tests to add to the existing list.
	 * @return array An array of tests
	 */
	public function getTests()
	{
		return [];
	}

	/**
	 * Returns a list of functions to add to the existing list.
	 * @return array An array of functions
	 */
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('i18n', [$this, 'i18n']),
			new \Twig_SimpleFunction('i18nAttr', [$this, 'i18nAttr'], ['is_safe' => ['html', 'html_attr']]),
			new \Twig_SimpleFunction('formatText', [$this, 'formatText']),
			new \Twig_SimpleFunction('formatTextAttr', [$this, 'formatTextAttr'], ['is_safe' => ['html', 'html_attr']]),
			new \Twig_SimpleFunction('configEntry', [$this, 'configEntry']),
			new \Twig_SimpleFunction('number', [$this, 'number']),
			new \Twig_SimpleFunction('integer', [$this, 'integer']),
			new \Twig_SimpleFunction('float', [$this, 'float']),
			new \Twig_SimpleFunction('currency', [$this, 'currency'])
		];
	}

	/**
	 * Returns a list of operators to add to the existing list.
	 * @return array An array of operators
	 */
	public function getOperators()
	{
		return [];
	}

	/**
	 * Returns a list of global variables to add to the existing list.
	 * @return array An array of global variables
	 */
	public function getGlobals()
	{
		return [];
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param string $i18nKey
	 * @param string[] $formatters
	 * @param array $replacementArray
	 * @return string
	 */
	public function i18n($i18nKey, $formatters = [], $replacementArray = null)
	{
		if (!is_array($replacementArray))
		{
			$replacementArray = [];
		}
		return $this->getI18nManager()->trans($i18nKey, $formatters, $replacementArray);
	}

	/**
	 * @param string $i18nKey
	 * @param string[] $formatters
	 * @param array $replacementArray
	 * @return string
	 */
	public function i18nAttr($i18nKey, $formatters = [], $replacementArray = null)
	{
		$formatters[] = 'attr';
		return $this->i18n($i18nKey, $formatters, $replacementArray);
	}

	/**
	 * @param string $string
	 * @param string[] $formatters
	 * @param array $replacementArray
	 * @return string
	 */
	public function formatText($string, $formatters = [], $replacementArray = null)
	{
		if (!is_array($replacementArray))
		{
			$replacementArray = [];
		}
		return $this->getI18nManager()->formatText($this->getI18nManager()->getLCID(), $string, $formatters, $replacementArray);
	}

	/**
	 * @param string $string
	 * @param string[] $formatters
	 * @param array $replacementArray
	 * @return string
	 */
	public function formatTextAttr($string, $formatters = [], $replacementArray = null)
	{
		$formatters[] = 'attr';
		return $this->formatText($string, $formatters, $replacementArray);
	}

	/**
	 * @param string $entry
	 * @return string|null
	 */
	public function configEntry($entry)
	{
		if ($this->configuration && $entry && is_string($entry))
		{
			$val = $this->configuration->getEntry($entry);
			if (!is_array($val))
			{
				return $val;
			}
		}
		return null;
	}

	/**
	 * @param int|float $number
	 * @return string
	 */
	public function number($number)
	{
		if (is_numeric($number))
		{
			return $this->getI18nManager()->transNumber($number);
		}
		return htmlspecialchars((string)$number);
	}

	/**
	 * @param float $amount
	 * @param string|array $currencyCode
	 * @return string|null
	 */
	public function currency($amount, $currencyCode)
	{
		if (is_array($currencyCode))
		{
			$currencyCode = $currencyCode['code'] ?? '';
		}
		if ($amount === null || !$currencyCode)
		{
			return null;
		}
		return $this->getI18nManager()->transCurrency($amount, $currencyCode);
	}

	/**
	 * @param \DateTime|string $dateTime
	 * @return string
	 */
	public function transDate($dateTime)
	{
		if (is_string($dateTime))
		{
			$dateTime = new \DateTime($dateTime);
		}
		if ($dateTime instanceof \DateTime)
		{
			return $this->getI18nManager()->transDate($dateTime);
		}
		return htmlspecialchars((string)$dateTime);
	}

	/**
	 * @param \DateTime|string $dateTime
	 * @return string
	 */
	public function transDateTime($dateTime)
	{
		if (is_string($dateTime))
		{
			$dateTime = new \DateTime($dateTime);
		}
		if ($dateTime instanceof \DateTime)
		{
			return $this->getI18nManager()->transDateTime($dateTime);
		}
		return htmlspecialchars((string)$dateTime);
	}

	/**
	 * @param \DateTime|string $dateTime
	 * @param string $format using this syntax: http://userguide.icu-project.org/formatparse/datetime
	 * @return string
	 */
	public function formatDate($dateTime, $format)
	{
		if (is_string($dateTime))
		{
			$dateTime = new \DateTime($dateTime);
		}
		if ($dateTime instanceof \DateTime)
		{
			$i18n = $this->getI18nManager();
			return $i18n->formatDate($i18n->getLCID(), $dateTime, $format);
		}
		return htmlspecialchars((string)$dateTime);
	}

	/**
	 * @param boolean $boolean
	 * @return string
	 */
	public function boolean($boolean)
	{
		if ($boolean === true)
		{
			return $this->getI18nManager()->trans('c.types.yes', ['ucf']);
		}
		elseif ($boolean === false)
		{
			return $this->getI18nManager()->trans('c.types.no', ['ucf']);
		}
		return htmlspecialchars((string)$boolean);
	}

	/**
	 * @param float $float
	 * @return string
	 */
	public function float($float)
	{
		if (is_numeric($float))
		{
			return $this->getI18nManager()->transFloat($float);
		}
		return htmlspecialchars((string)$float);
	}

	/**
	 * @param integer $integer
	 * @return string
	 */
	public function integer($integer)
	{
		if (is_numeric($integer))
		{
			return $this->getI18nManager()->transInteger($integer);
		}
		return htmlspecialchars((string)$integer);
	}

	/**
	 * @param string $string
	 * @return string
	 */
	public function jsPackedString($string)
	{
		$str = (string)$string;
		if ($str)
		{
			$str = str_replace("'", '\\\'', $str);
			$str = preg_replace('/[\s]+/', ' ', $str);
		}
		return "'" . trim($str) . "'";
	}
}