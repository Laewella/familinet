<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Themes;

/**
 * @name \Change\Presentation\Themes\ThemeAssets
 */
class ThemeAssets
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var boolean
	 */
	protected $devMode = false;

	/**
	 * @var string
	 */
	protected $webBaseDirectory;

	/**
	 * @var string
	 */
	protected $assetsVersion;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Application $application)
	{
		$this->application = $application;
		$configuration = $application->getConfiguration();
		$this->devMode = $configuration->inDevelopmentMode();
		$config = $configuration->getEntry('Change/Install');
		$this->webBaseDirectory = $config['webBaseDirectory'] ?? '';
		$this->assetsVersion = $config['assetsVersion'] ?? '';
	}

	/**
	 * @return \Change\Application
	 */
	public function getApplication()
	{
		return $this->application;
	}

	/**
	 * @param \Change\Application $application
	 * @return $this
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getDevMode()
	{
		return $this->devMode;
	}

	/**
	 * @param boolean $devMode
	 * @return $this
	 */
	public function setDevMode($devMode)
	{
		$this->devMode = $devMode;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getWebBaseDirectory()
	{
		return $this->webBaseDirectory;
	}

	/**
	 * @param string $webBaseDirectory
	 * @return $this
	 */
	public function setWebBaseDirectory($webBaseDirectory)
	{
		$this->webBaseDirectory = $webBaseDirectory;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAssetsVersion()
	{
		return $this->assetsVersion;
	}

	/**
	 * @param string $assetsVersion
	 * @return $this
	 */
	public function setAssetsVersion($assetsVersion)
	{
		$this->assetsVersion = $assetsVersion;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAssetsBasePath()
	{
		return '/Assets/' . $this->assetsVersion . '/';
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @param boolean $cleanupBefore
	 */
	public function installPluginTemplates($plugin, $theme, $cleanupBefore = true)
	{
		$moduleName = $plugin->isTheme() ? null : $plugin->getName();
		if ($cleanupBefore)
		{
			$theme->removeTemplatesContent($moduleName);
		}

		$includedExtensions = ['twig'];
		$paths = [$plugin->getTwigAssetsPath(), $plugin->getThemeAssetsPath()];

		foreach ($paths as $path)
		{
			if ($path)
			{
				$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path,
					\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
				while ($it->valid())
				{
					/* @var $current \RecursiveDirectoryIterator */
					$current = $it->current();
					if ($current->isFile() && strpos($current->getBasename(), '.') !== 0 && in_array($current->getExtension(), $includedExtensions))
					{
						$subPathname = $current->getSubPathname();
						if (strpos($subPathname, 'Layout') !== 0 && strpos($subPathname, 'Templates') !== 0 && strpos($subPathname, 'Twig') !== 0)
						{
							$theme->installTemplateContent($moduleName, $subPathname, file_get_contents($current->getPathname()));
						}
					}
					$it->next();
				}
			}
		}
	}

	/**
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @return array
	 */
	public function getNgTemplatesPath(\Change\Presentation\Interfaces\Theme $theme)
	{
		$basePath = $theme->getTemplateBasePath() . '/';
		if (is_dir($basePath))
		{
			$templatesPaths = glob($basePath . '*/ng-template/*.twig');
			if (is_array($templatesPaths))
			{
				$l = strlen($basePath);
				$result = [];
				foreach ($templatesPaths as $path)
				{
					$name = basename($path);
					$result['/' . $name] = substr($path, $l);
				}
				return $result;
			}
		}
		return [];
	}

	/**
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @param array $templates
	 */
	public function writeNgTemplates(\Change\Presentation\Interfaces\Theme $theme, array $templates)
	{
		$path = $theme->getTemplateBasePath() . '/ng-templates.js';
		$content = ['(function() {
	\'use strict\';
	var app = angular.module(\'RbsChangeApp\');
{% macro templateHtml(name) %}{% include name %}{% endmacro %}
{% import _self as ng %}
	app.run([\'$templateCache\' , function($templateCache) {'];
		foreach ($templates as $id => $name)
		{
			$content[] = '		$templateCache.put(\'' . $id . '\', {{ ng.templateHtml(\'' . $name . '\') | jsPackedString | raw}});';
		}
		$content[] = '	}]);
})();';
		\Change\Stdlib\FileUtils::mkdir(dirname($path));
		file_put_contents($path, implode(PHP_EOL, $content));
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param string $themeWebAssetBaseDirectory webBaseDirectory /Assets/Theme/THEME_VENDOR/THEME_NAME
	 */
	public function installPluginAssets($plugin, $themeWebAssetBaseDirectory)
	{
		$srcAssetPath = $plugin->getThemeAssetsPath();
		if (!$srcAssetPath)
		{
			return;
		}
		$themeWebAssetBaseDirectory = rtrim($themeWebAssetBaseDirectory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
		if ($plugin->isModule())
		{
			$themeWebAssetBaseDirectory .= $plugin->getName() . DIRECTORY_SEPARATOR;
		}

		$excludedExtensions = ['twig', 'less'];
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcAssetPath,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && strpos($current->getBasename(), '.') !== 0 && !in_array($current->getExtension(), $excludedExtensions))
			{
				$subPathname = $current->getSubPathname();
				if ($subPathname !== 'assets.json' && strpos($subPathname, 'Layout') !== 0 && strpos($subPathname, 'Templates') !== 0
					&& strpos($subPathname, 'I18n') !== 0
				)
				{
					$targetAssetPath = $themeWebAssetBaseDirectory . $subPathname;
					\Change\Stdlib\FileUtils::mkdir(dirname($targetAssetPath));
					file_put_contents($targetAssetPath, file_get_contents($current->getPathname()));
				}
			}
			$it->next();
		}
	}

	/**
	 * @param \Change\Presentation\Interfaces\Theme $pluginTheme
	 * @param \Change\Plugins\Plugin[] $modulesPlugins
	 * @return array
	 */
	public function buildAssetConfiguration($pluginTheme, array $modulesPlugins = null)
	{
		$defaultConfig = ['templates' => [], 'blocks' => []];
		$filePath = $pluginTheme->getResourceFilePath('assets.json');
		if (file_exists($filePath) && is_readable($filePath))
		{
			$config = json_decode(file_get_contents($filePath), true);
			if (!is_array($config))
			{
				return $defaultConfig;
			}
		}
		else
		{
			return $defaultConfig;
		}

		$config = array_merge($defaultConfig, $config);

		if (!$modulesPlugins)
		{
			return $config;
		}

		foreach ($modulesPlugins as $plugin)
		{
			if ($plugin->isModule() && ($moduleAssetPath = $plugin->getThemeAssetsPath()))
			{
				$filePath = $moduleAssetPath . '/assets.json';
				if (file_exists($filePath) && is_readable($filePath))
				{
					$moduleConfig = json_decode(file_get_contents($filePath), true);
					if (!is_array($moduleConfig))
					{
						continue;
					}
					$moduleConfig = array_merge($defaultConfig, $moduleConfig);

					$blockConfigurations = [];
					$pluginName = $plugin->getName();
					foreach ($moduleConfig['blocks'] as $blockName => $blockConfiguration)
					{
						if (preg_match('/^' . $pluginName . '/', $blockName) && count(explode('_', $blockName)) === 3)
						{
							$blockConfigurations[$blockName] = $blockConfiguration;
						}
						else
						{
							$blockConfigurations[$pluginName . '_' . $blockName] = $blockConfiguration;
						}
					}
					$moduleConfig['blocks'] = $blockConfigurations;

					//@deprecated since 1.8.0 with no replacement
					if (isset($moduleConfig['jsCollections']))
					{
						trigger_error($filePath . ' jsCollections is deprecated', E_USER_WARNING);
						$jsCollections = ['jsAssets' => []];
						foreach ($moduleConfig['jsCollections'] as $jsNames)
						{
							$jsCollections['jsAssets'] = array_merge($jsCollections['jsAssets'], $jsNames);
						}
						$moduleConfig['blocks'][$pluginName . '_jsCollections'] = $jsCollections;
					}

					//@deprecated since 1.8.0 with no replacement
					if (isset($moduleConfig['blocksExtend']))
					{
						trigger_error($filePath . ' blocksExtend is deprecated', E_USER_WARNING);
						$blocksExtend = [];
						foreach ($moduleConfig['blocksExtend'] as $blockExtend)
						{
							$blocksExtend = array_merge_recursive($blocksExtend, $blockExtend);
						}
						$moduleConfig['blocks'][$pluginName . '_blocksExtend'] = $blocksExtend;
					}
					$config = $this->appendConfiguration($config, $moduleConfig);
				}
			}
		}
		return $config;
	}

	/**
	 * @param array $config
	 * @param array $configToAppend
	 * @return array
	 */
	public function appendConfiguration($config, $configToAppend)
	{
		$config['templates'] = array_merge($config['templates'], $configToAppend['templates']);
		$config['blocks'] = array_merge($config['blocks'], $configToAppend['blocks']);
		return $config;
	}

	/**
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @param array $configuration
	 * @return \Assetic\AssetManager
	 */
	public function getAsseticManager($theme, array $configuration = null)
	{
		if ($configuration === null)
		{
			$configuration = $this->compileAssetConfiguration($theme->getAssetConfiguration());
		}
		$application = $this->application;
		$workspace = $application->getWorkspace();
		$importPaths = [
			$workspace->projectThemesPath(),
			$workspace->pluginsThemesPath(),
			$workspace->compilationPath('Themes')
		];

		$am = new \Assetic\AssetManager();
		$devMode = $this->devMode;

		$assetBasePath = '/Assets/' . $this->assetsVersion . '/';
		$basePath = $assetBasePath . 'Theme/' . str_replace('_', '/', $theme->getName()) . '/';

		$jsAssets = new \Assetic\Asset\AssetCollection();
		$jsAssets->setTargetPath($basePath . 'blocks.js');

		$cssAssets = new \Assetic\Asset\AssetCollection();
		$cssAssets->setTargetPath($basePath . 'blocks.css');

		$cacheDir = $workspace->cachePath('less');
		\Change\Stdlib\FileUtils::mkdir($cacheDir);
		$idx = 0;

		/** @noinspection ForeachSourceInspection */
		foreach ($configuration as $treeName => $block)
		{
			foreach ($block as $assetType => $assetList)
			{
				foreach ($assetList as $assetUrl)
				{
					if (preg_match('/^Theme\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/(.+)$/', $assetUrl, $matches))
					{
						list(, $themeVendor, $themeShortName, $path) = $matches;
						$targetPath = $assetBasePath . $assetUrl;

						$resourceFilePath = $this->getThemeAssetFilePath($workspace, $themeVendor, $themeShortName, $path);
						if (file_exists($resourceFilePath))
						{
							$asset = new \Assetic\Asset\FileAsset($resourceFilePath);
							$extension = substr($resourceFilePath, -5);
							if ($extension === '.less')
							{
								$filter = new \Change\Presentation\Themes\AsseticLessFilter($cacheDir, $importPaths, $assetBasePath . 'Theme/');
								$filter->setFormatter($devMode ? 'classic' : 'compressed');
								$asset->ensureFilter($filter);
								$asset->setTargetPath($targetPath . '.css');
							}
							elseif ($extension === '.scss')
							{
								$filter = new \Change\Presentation\Themes\AsseticScssFilter($cacheDir, $importPaths, $assetBasePath . 'Theme/');
								$filter->setFormatter($devMode ? 'expanded' : 'crunched');
								$asset->ensureFilter($filter);
								$asset->setTargetPath($targetPath . '.css');
							}

							if (!$asset->getTargetPath())
							{
								$asset->setTargetPath($targetPath);
							}
							$idx++;
							$common = (strpos($treeName, '*') === 0);
							$an = str_replace(['_', '*'], '', $treeName);
							if ($assetType === 'jsAssets')
							{
								if ($common)
								{
									if (!$devMode)
									{
										$asset->ensureFilter(new \Assetic\Filter\JSMinFilter());
									}
									$am->set('common_scripts_' . $an . '_' . $idx, $asset);
								}
								else
								{
									$jsAssets->add($asset);
								}
							}
							elseif ($assetType === 'cssAssets')
							{
								if ($common)
								{
									$am->set('common_styles_' . $an . '_' . $idx, $asset);
								}
								else
								{
									$cssAssets->add($asset);
								}
							}
						}
					}
				}
			}
		}

		if (count($jsAssets->all()))
		{
			if (!$devMode)
			{
				$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
			}
			$am->set('combined_scripts_' . $idx, $jsAssets);
		}
		if (count($cssAssets->all()))
		{
			$am->set('combined_styles_' . $idx, $cssAssets);
		}
		return $am;
	}

	/**
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @param array $configuration
	 * @return \Assetic\AssetManager
	 */
	public function getCompiledAsseticManager($theme, array $configuration)
	{
		$application = $this->application;
		$workspace = $application->getWorkspace();
		$am = new \Assetic\AssetManager();
		$assetBasePath = '/Assets/' . $this->assetsVersion . '/';
		list($vendor, $shortName) = explode('_', $theme->getName());

		$basePath = $assetBasePath . 'Theme/' . $vendor . '/' . $shortName . '/';
		$baseFilePath =$this->getAssetBaseDirectory();

		$idx = 0;
		/** @noinspection ForeachSourceInspection */
		foreach ($configuration as $treeName => $block)
		{
			if (strpos($treeName, '*') !== 0)
			{
				continue;
			}
			foreach ($block as $assetType => $assetList)
			{
				foreach ($assetList as $assetUrl)
				{
					if (preg_match('/^Theme\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/(.+)$/', $assetUrl, $matches))
					{
						list(, $themeVendor, $themeShortName, $path) = $matches;

						if (in_array(substr($path, -5), ['.less', '.scss']))
						{
							$path .= '.css';
							$assetUrl .= '.css';
						}
						$filePath = $workspace->composePath($baseFilePath, 'Theme', $themeVendor, $themeShortName, $path);
						if (file_exists($filePath))
						{
							$asset = new \Assetic\Asset\FileAsset($filePath);
							$asset->setTargetPath($assetBasePath . $assetUrl);

							$an = str_replace(['_', '*'], '', $treeName);
							if ($assetType === 'jsAssets')
							{
								$idx++;
								$am->set('common_scripts_' . $an . '_' . $idx, $asset);
							}
							elseif ($assetType === 'cssAssets')
							{
								$idx++;
								$am->set('common_styles_' . $an . '_' . $idx, $asset);
							}
						}
					}
				}
			}
		}
		$filePath = $workspace->composePath($baseFilePath, 'Theme', $vendor, $shortName, 'blocks.js');
		if (file_exists($filePath))
		{
			$asset = new \Assetic\Asset\FileAsset($filePath);
			$asset->setTargetPath($basePath . 'blocks.js');
			$am->set('combined_scripts_' . $idx, $asset);
			$idx++;
		}

		$filePath = $workspace->composePath($baseFilePath, 'Theme', $vendor, $shortName, 'blocks.css');
		if (file_exists($filePath))
		{
			$asset = new \Assetic\Asset\FileAsset($filePath);
			$asset->setTargetPath($basePath . 'blocks.css');
			$am->set('combined_styles_' . $idx, $asset);
		}
		return $am;
	}

	/**
	 * @return string
	 */
	protected function getAssetBaseDirectory()
	{
		return $this->application->getWorkspace()->composeAbsolutePath($this->webBaseDirectory, 'Assets', $this->assetsVersion);
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function write(\Assetic\AssetManager $assetsManager)
	{
		$writer = new \Assetic\AssetWriter($this->application->getWorkspace()->composeAbsolutePath($this->webBaseDirectory));
		$writer->writeManagerAssets($assetsManager);
	}

	/**
	 * @param \Change\Workspace $workspace
	 * @param string $themeVendor
	 * @param string $themeShortName
	 * @param string $path
	 * @return string
	 */
	protected function getThemeAssetFilePath($workspace, $themeVendor, $themeShortName, $path)
	{
		$themeName = $themeVendor . '_' . $themeShortName;
		if ($themeName === ThemeManager::DEFAULT_THEME_NAME)
		{
			if (preg_match('/^([A-Z][A-Aa-z0-9]+)_([A-Z][A-Aa-z0-9]+)\/(.+)$/', $path, $matches))
			{
				list(, $vendor, $moduleShortName, $resourceModulePath) = $matches;
				if ($vendor === 'Project')
				{
					$modulePath = $workspace->projectModulesPath($vendor, $moduleShortName, 'Assets', 'Theme');
				}
				else
				{
					$modulePath = $workspace->pluginsModulesPath($vendor, $moduleShortName, 'Assets', 'Theme');
				}
				if (is_dir($modulePath))
				{
					return $workspace->composePath($modulePath, $resourceModulePath);
				}
			}
			elseif (preg_match('/^ua\/(.+)$/', $path, $matches))
			{
				$modulePath = $workspace->pluginsModulesPath('Rbs', 'Ua', 'Assets');
				if (is_dir($modulePath))
				{
					return $workspace->composePath($modulePath, $matches[1]);
				}
			}
		}
		if ($themeVendor === 'Project')
		{
			return $workspace->projectThemesPath($themeVendor, $themeShortName, 'Assets', $path);
		}
		return $workspace->pluginsThemesPath($themeVendor, $themeShortName, 'Assets', $path);
	}

	/**
	 * @param array $assetConfiguration
	 * @param string|null $LCID
	 * @param string|null $templateCode
	 * @return array
	 */
	public function compileAssetConfiguration(array $assetConfiguration, $LCID = null, $templateCode = null)
	{
		$configuration = [];

		$i18nFiles = null;
		if ($LCID && isset($assetConfiguration['templates']['i18n_' . $LCID]))
		{
			$i18nFiles = $assetConfiguration['templates']['i18n_' . $LCID];
		}

		if ($templateCode === 'all' && isset($assetConfiguration['templates']))
		{
			$templateConfigurationName = '*';
			$templateConfiguration = $assetConfiguration['templates']['*'];
			foreach ($assetConfiguration['templates'] as $aTemplateCode => $aTemplateConfiguration)
			{
				if (count(explode('_', $aTemplateCode)) === 3)
				{
					$templateConfiguration = array_merge_recursive($templateConfiguration, $aTemplateConfiguration);
				}
			}
		}
		elseif ($templateCode && isset($assetConfiguration['templates'][$templateCode]))
		{
			$templateConfigurationName = $templateCode;
			$templateConfiguration = $assetConfiguration['templates'][$templateCode];
		}
		else
		{
			$templateConfigurationName = '*';
			$templateConfiguration = $assetConfiguration['templates']['*'];
		}

		if ($i18nFiles)
		{
			$templateConfiguration = array_merge_recursive($templateConfiguration, $i18nFiles);
		}

		if ($templateConfigurationName != '*')
		{
			$templateConfigurationName = '*' . $templateConfigurationName . '*';
		}
		$configuration[$templateConfigurationName] = $templateConfiguration;

		foreach ($assetConfiguration['blocks'] as $blockName => $blockConfiguration)
		{
			$jsAssets = [];
			$cssAssets = [];
			if (isset($blockConfiguration['jsAssets']))
			{
				$jsAssets = $blockConfiguration['jsAssets'];
			}
			if (isset($blockConfiguration['cssAssets']))
			{
				$cssAssets = $blockConfiguration['cssAssets'];
			}

			$configuration[$blockName] = ['jsAssets' => $jsAssets, 'cssAssets' => $cssAssets];
		}

		return $configuration;
	}

	/**
	 * @param array $formats
	 * @return string
	 */
	public function generateImageLessContent($formats)
	{
		$contents = [];
		if (is_array($formats))
		{
			foreach ($formats as $name => $format)
			{
				if (!strpos($format, 'x'))
				{
					continue;
				}

				list($width, $height) = explode('x', $format);
				$contents[] = '/* Values from configuration entry: Rbs/Media/namedImageFormats/' . $name . ' */
@image-format-' . $name . '-width: ' . $width . 'px;
@image-format-' . $name . '-height: ' . $height . 'px;

.image-format-' . $name . '-size {
	width: @image-format-' . $name . '-width;
	height: @image-format-' . $name . '-height;
}
.image-format-' . $name . '-max-size {
	max-width: @image-format-' . $name . '-width;
	max-height: @image-format-' . $name . '-height;
}
.image-format-' . $name . '-icon-size {
	font-size: @image-format-' . $name . '-height / 2;
}';
			}
		}
		return implode(PHP_EOL . PHP_EOL, $contents);
	}

	/**
	 * @param array $formats
	 * @return string
	 */
	public function generateImageScssContent($formats)
	{
		$contents = [];
		if (is_array($formats))
		{
			foreach ($formats as $name => $format)
			{
				if (!strpos($format, 'x'))
				{
					continue;
				}

				list($width, $height) = explode('x', $format);
				$contents[] = '/* Values from configuration entry: Rbs/Media/namedImageFormats/' . $name . ' */
$image-format-' . $name . '-width: ' . $width . 'px;
$image-format-' . $name . '-height: ' . $height . 'px;

.image-format-' . $name . '-size {
	width: $image-format-' . $name . '-width;
	height: $image-format-' . $name . '-height;
}
.image-format-' . $name . '-max-size {
	max-width: $image-format-' . $name . '-width;
	max-height: $image-format-' . $name . '-height;
}
.image-format-' . $name . '-icon-size {
	font-size: $image-format-' . $name . '-height / 2;
}';
			}
		}
		return implode(PHP_EOL . PHP_EOL, $contents);
	}
}