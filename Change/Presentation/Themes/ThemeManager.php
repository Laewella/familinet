<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Themes;

/**
 * @api
 * @name \Change\Presentation\Themes\ThemeManager
 */
class ThemeManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const DEFAULT_THEME_NAME = 'Rbs_Base';
	const EVENT_LOADING = 'loading';
	const EVENT_MAIL_TEMPLATE_LOADING = 'mail.template.loading';

	const EVENT_MANAGER_IDENTIFIER = 'Presentation.Themes';
	const EVENT_GET_ASSET_CONFIGURATION = 'getAssetConfiguration';

	const EVENT_ADD_PAGE_RESOURCES = 'addPageResources';

	/**
	 * @var \Change\Presentation\Interfaces\Theme
	 */
	protected $default;

	/**
	 * @var \Change\Presentation\Interfaces\Theme
	 */
	protected $current;

	/**
	 * @var \Change\Presentation\Interfaces\Theme[]
	 */
	protected $themes = [];

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @return \Change\Workspace
	 */
	protected function getWorkspace()
	{
		return $this->getApplication()->getWorkspace();
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/ThemeManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(static::EVENT_LOADING, [$this, 'onLoading'], 5);
		$eventManager->attach(static::EVENT_GET_ASSET_CONFIGURATION, [$this, 'onDefaultGetAssetConfiguration'], 10);
		$eventManager->attach(static::EVENT_GET_ASSET_CONFIGURATION, [$this, 'onDefaultCompileGetAssetConfiguration'], 5);
		$eventManager->attach(static::EVENT_ADD_PAGE_RESOURCES, [$this, 'onDefaultAddPageResources'], 5);
	}

	/**
	 * @param string $themeName
	 * @return \Change\Presentation\Interfaces\Theme|null
	 */
	protected function dispatchLoading($themeName)
	{
		$event = new \Change\Events\Event(static::EVENT_LOADING, $this, ['themeName' => $themeName]);
		$this->getEventManager()->triggerEvent($event);
		return $event->getParam('theme');
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return DefaultTheme|null
	 */
	public function onLoading(\Change\Events\Event $event)
	{
		if ($event->getParam('themeName') === static::DEFAULT_THEME_NAME)
		{
			$defaultTheme = new DefaultTheme($event->getApplication());
			$defaultTheme->setPluginManager($event->getApplicationServices()->getPluginManager())
				->setThemeManager($this);
			$event->setParam('theme', $defaultTheme);
		}
	}

	/**
	 * @api
	 * @param \Change\Presentation\Interfaces\Theme $current
	 */
	public function setCurrent(\Change\Presentation\Interfaces\Theme $current = null)
	{
		$this->current = $current;
		if ($current !== null)
		{
			$this->addTheme($current);
		}
	}

	/**
	 * @api
	 * @return \Change\Presentation\Interfaces\Theme
	 */
	public function getCurrent()
	{
		return $this->current !== null ? $this->current : $this->getDefault();
	}

	/**
	 * @api
	 * @throws \RuntimeException
	 * @return \Change\Presentation\Themes\DefaultTheme
	 */
	public function getDefault()
	{
		if ($this->default === null)
		{
			$this->default = $this->getByName(static::DEFAULT_THEME_NAME);
			if (!($this->default instanceof \Change\Presentation\Themes\DefaultTheme))
			{
				throw new \RuntimeException('Theme ' . static::DEFAULT_THEME_NAME . ' not found', 999999);
			}
		}
		return $this->default;
	}

	/**
	 * @api
	 * @param string $name
	 * @return \Change\Presentation\Interfaces\Theme|null
	 */
	public function getByName($name)
	{
		if ($name === null)
		{
			return $this->getCurrent();
		}
		elseif (!array_key_exists($name, $this->themes))
		{
			$theme = $this->dispatchLoading($name);
			if ($theme instanceof \Change\Presentation\Interfaces\Theme)
			{
				$this->addTheme($theme);
			}
			else
			{
				$this->themes[$name] = null;
			}
		}
		return $this->themes[$name];
	}

	/**
	 * @api
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 */
	public function addTheme(\Change\Presentation\Interfaces\Theme $theme)
	{
		if (!isset($this->themes[$theme->getName()]))
		{
			$this->getEventManager()->trigger('addTheme', $this, ['theme' => $theme]);
		}
		$this->themes[$theme->getName()] = $theme;
		$theme->setThemeManager($this);
		$parentTheme = $theme->getParentTheme();
		if ($parentTheme && !isset($this->themes[$parentTheme->getName()]))
		{
			$this->addTheme($parentTheme);
		}
	}


	/**
	 * @api
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @return string[]
	 */
	public function getThemeTwigBasePaths($theme = null)
	{
		$paths = [];
		if (!$theme)
		{
			$theme = $this->getCurrent();
		}
		while (true)
		{
			$basePath = $theme->getTemplateBasePath();
			if (is_dir($basePath))
			{
				$paths[] = $basePath;
			}

			if ($theme === $this->getDefault())
			{
				break;
			}
			elseif ($theme->getParentTheme())
			{
				$theme = $theme->getParentTheme();
			}
			else
			{
				$theme = $this->getDefault();
			}
		}
		return $paths;
	}


	/**
	 * @param \Change\Presentation\Interfaces\Theme $theme
	 * @param string|null $templateCode
	 * @return array|mixed
	 */
	public function getAssetConfiguration($theme, $templateCode = null)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['theme' => $theme, 'templateCode' => $templateCode]);
		$this->getEventManager()->trigger(static::EVENT_GET_ASSET_CONFIGURATION, $this, $args);
		if (isset($args['configuration']))
		{
			return $args['configuration'];
		}

		return [];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultGetAssetConfiguration($event)
	{
		$theme = $event->getParam('theme');
		if ($theme instanceof \Change\Presentation\Interfaces\Theme
			&& $event->getParam('configurationRules') === null)
		{
			$configurationRules = $theme->getAssetConfiguration();
			$event->setParam('configurationRules', $configurationRules);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultCompileGetAssetConfiguration($event)
	{
		$assetConfiguration = $event->getParam('configurationRules');
		if (!is_array($assetConfiguration) || $event->getParam('configuration') !== null)
		{
			return;
		}
		$LCID = $event->getApplicationServices()->getI18nManager()->getLCID();
		$themeAssets = new ThemeAssets($event->getApplication());

		$configuration = $themeAssets->compileAssetConfiguration($assetConfiguration, $LCID, $event->getParam('templateCode'));
		$event->setParam('configuration', $configuration);
	}

	/**
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getAssetRootPath()
	{
		$root = $this->getConfiguration()->getEntry('Change/Install/webBaseDirectory', false);
		if ($root === false)
		{
			throw new \RuntimeException('Change/Install/webBaseDirectory not defined', 999999);
		}
		return $this->getWorkspace()->composeAbsolutePath($root, 'Assets');
	}

	/**
	 * @api
	 * @param \Change\Http\Web\Result\Page $pageResult
	 * @param \Change\Presentation\Interfaces\Template $template
	 * @param \Change\Presentation\Layout\Block[] $blocks
	 */
	public function addPageResources(\Change\Http\Web\Result\Page $pageResult,
		\Change\Presentation\Interfaces\Template $template, array $blocks)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['pageResult' => $pageResult, 'template' => $template, 'blocks' => $blocks]);
		$eventManager->trigger(static::EVENT_ADD_PAGE_RESOURCES, $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultAddPageResources(\Change\Events\Event $event)
	{
		/** @var \Change\Http\Web\Result\Page $result */
		$result = $event->getParam('pageResult');

		/** @var \Change\Presentation\Interfaces\Template $template */
		$template = $event->getParam('template');

		$theme = $this->getCurrent();
		$configuration = $this->getAssetConfiguration($theme, $template->getCode());

		$themeAssets = new \Change\Presentation\Themes\ThemeAssets($this->getApplication());
		$asseticManager = $themeAssets->getCompiledAsseticManager($theme, $configuration);

		$event->setParam('configuration', $configuration);
		$templateCode = str_replace('_', '', $template->getCode());
		$names = $asseticManager->getNames();
		foreach ($names as $name)
		{
			$p = explode('_', $name);
			if ($p[0] === 'common' && $p[2] !== '' && $p[2] !== $templateCode)
			{
				continue;
			}
			$a = $asseticManager->get($name);
			if ($p[1] === 'scripts')
			{
				$result->addJsAsset($a->getTargetPath());
			}
			else
			{
				$result->addCssAsset($a->getTargetPath());
			}
		}
		$ctx = $result->getNavigationContext();
		$ctx['assetBasePath'] = $themeAssets->getAssetsBasePath();
		$result->setNavigationContext($ctx);

		$website = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($ctx['websiteId']);
		$path = '/';
		if ($website instanceof \Change\Presentation\Interfaces\Website)
		{
			$relativePath = trim((string)$website->getRelativePath(), '/');
			if ($relativePath) {
				$path .=  $relativePath . '/';
			}
		}
		$result->addJsAsset($path . 'Theme/' . str_replace('_', '/', $theme->getName()). '/ng-templates.js');
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return boolean|null
	 */
	public function getCombineAssets()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return true;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return $this
	 */
	public function setCombineAssets()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this;
	}
}