<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Themes;

use Change\Presentation\Interfaces\Page;
use Change\Presentation\Interfaces\Template;
use Change\Presentation\Layout\Layout;
use Zend\Http\Response as HttpResponse;

/**
 * @name \Change\Presentation\Themes\DefaultPage
 */
class DefaultPage implements Page
{
	/**
	 * @var ThemeManager
	 */
	protected $themeManager;

	/**
	 * @var string
	 */
	protected $identifier;

	/**
	 * @var string
	 */
	protected $layoutJsonFilePath;

	/**
	 * @var \Change\Presentation\Interfaces\Section
	 */
	protected $section;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var integer
	 */
	protected $TTL = 0;

	/**
	 * @param ThemeManager $themeManager
	 * @param string $identifier
	 */
	public function __construct(ThemeManager $themeManager, $identifier = 'default')
	{
		$this->themeManager = $themeManager;
		$this->identifier = $identifier;
	}

	/**
	 * @api
	 * @return string
	 */
	public function getIdentifier()
	{
		return $this->identifier;
	}

	/**
	 * @throws \RuntimeException
	 * @return string
	 */
	protected function getLayoutJsonFilePath()
	{
		if ($this->layoutJsonFilePath === null)
		{
			$id = 'Layout/Page/' . $this->getIdentifier() . '.json';
			$filePath = $this->themeManager->getDefault()->getResourceFilePath($id);
			if (!is_readable($filePath))
			{
				throw new \RuntimeException($this->getIdentifier() . '.json resource not found', 999999);
			}
			$this->layoutJsonFilePath = $filePath;
		}
		return $this->layoutJsonFilePath;
	}

	/**
	 * @return \Datetime
	 */
	public function getModificationDate()
	{
		return \DateTime::createFromFormat('U', filemtime($this->getLayoutJsonFilePath()));
	}

	/**
	 * @api
	 * @return Template
	 */
	public function getTemplate()
	{
		return $this->themeManager->getDefault()->getPageTemplate('default');
	}

	/**
	 * @return Layout
	 */
	public function getContentLayout()
	{
		$config = file_get_contents($this->getLayoutJsonFilePath());
		return new Layout(json_decode($config, true));
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param \Change\Presentation\Interfaces\Section $section
	 * @return $this
	 */
	public function setSection($section)
	{
		$this->section = $section;
		return $this;
	}

	/**
	 * @return \Change\Presentation\Interfaces\Section
	 */
	public function getSection()
	{
		return $this->section;
	}

	/**
	 * @param integer $TTL
	 * @return $this
	 */
	public function setTTL($TTL)
	{
		$this->TTL = $TTL;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getTTL()
	{
		return $this->TTL;
	}
}