#!/bin/sh
chmod g+w log/project/ -R
chmod g+w tmp/ -R
rm -rf App/Config/project.autogen.json Compilation/* tmp/* www/Assets/*

echo "Build image"
php bin/change.phar change:set-document-root www
php bin/change.phar change:clear-cache
php bin/change.phar change:register-plugin -a -f

php bin/change.phar change:install-package --steps="Application" --vendor=Rbs Core
php bin/change.phar change:install-plugin --steps="Application" --vendor=Project Familinet
php bin/change.phar change:install-plugin --steps="Application" --vendor=Project Library
php bin/change.phar change:install-plugin --steps="Application" --type=theme --vendor=Project Project
while [ "$1" != "" ]; do
	case $1 in
		-d | --dev )	php bin/change.phar change:install-plugin --steps="Application" --vendor=Rbs Dev
						;;
	esac
	shift
done

php bin/change.phar change:compile-documents

echo "Mount instance"
php bin/change.phar change:generate-db-schema -m
php bin/change.phar change:refresh-i18n

php bin/change.phar change:install-package --steps="Services" --vendor=Rbs Core
php bin/change.phar change:install-plugin --steps="Services" --vendor=Project Familinet
php bin/change.phar change:install-plugin --steps="Services" --vendor=Project Library
php bin/change.phar change:install-plugin --steps="Services" --type=theme --vendor=Project Project
while [ "$1" != "" ]; do
	case $1 in
		-d | --dev )	php bin/change.phar change:install-plugin --steps="Services" --vendor=Rbs Dev
						;;
	esac
	shift
done

php bin/change.phar change:refresh-i18n
php bin/change.phar change:patch -r
php bin/change.phar rbs_notification:install-notifications Rbs_Common_Mail
php bin/change.phar rbs_notification:configure

php bin/change.phar change:generate-db-indexes -r -c

php bin/change.phar project_familinet:update-web-app -a

chmod g+w tmp/ -R