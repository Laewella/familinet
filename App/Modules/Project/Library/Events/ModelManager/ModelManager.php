<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Events\ModelManager;

/**
 * @name \Project\Library\Events\ModelManager\ModelManager
 */
class ModelManager
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function getRestriction(\Change\Events\Event $event)
	{
		if ($event->getParam('restriction'))
		{
			return;
		}

		$filter = $event->getParam('filter');
		if (!\is_array($filter) || !isset($filter['parameters']) || !\is_array($filter['parameters']))
		{
			return;
		}

		$parameters = $filter['parameters'];
		if (!isset($parameters['operator'], $parameters['propertyName']) && $parameters['propertyName'] !== 'indexedAliases')
		{
			return;
		}

		$query = $event->getParam('documentQuery');
		if (!($query instanceof \Change\Documents\Query\Query))
		{
			return;
		}

		$model = $query->getModel();
		switch ($model->getName())
		{
			case 'Project_Library_Work':
				$indexTable = \Project\Library\Documents\Work::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\Work::ALIASES_INDEX_PREFIX;
				break;

			case 'Project_Library_WorkGroup':
				$indexTable = \Project\Library\Documents\WorkGroup::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\WorkGroup::ALIASES_INDEX_PREFIX;
				break;

			case 'Project_Library_Contributor':
				$indexTable = \Project\Library\Documents\Contributor::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\Contributor::ALIASES_INDEX_PREFIX;
				break;

			case 'Project_Library_Publisher':
				$indexTable = \Project\Library\Documents\Publisher::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\Publisher::ALIASES_INDEX_PREFIX;
				break;

			default:
				return;
		}

		$searchString = \Project\Library\Indexer\AliasesIndexer::cleanUpAlias($parameters['value']);
		$exactMatchSearchString = \Project\Library\Indexer\AliasesIndexer::cleanUpExactMatchAlias($parameters['value']);

		$dbQb = $query->dbQueryBuilder();
		$dbFb = $dbQb->getFragmentBuilder();

		$select = $event->getApplicationServices()->getDbProvider()->getNewQueryBuilder();
		$fb = $select->getFragmentBuilder();
		$select->select($fb->allColumns());
		$select->from($fb->alias($fb->table($indexTable), 'index'));
		$select->where(
			$fb->logicAnd(
				$fb->eq($dbFb->column($fieldPrefix . 'id', 'index'), $query->getColumn('id')),
				$fb->logicOr(
					$fb->logicAnd(
						$fb->eq('exact_match', 0),
						$fb->like($fb->column($fieldPrefix . 'alias'), $fb->string($searchString), $parameters['operator'])
					),
					$fb->logicAnd(
						$fb->eq('exact_match', 1),
						$fb->like(
							$fb->column($fieldPrefix . 'alias'), $fb->string($exactMatchSearchString), \Change\Db\Query\Predicates\Like::EXACT
						)
					)
				)
			)
		);

		$restriction = $dbFb->exists($dbFb->subQuery($select->query()));
		$event->setParam('restriction', $restriction);
	}

	/**
	 * @api
	 * @param \Change\Db\Query\Expressions\AbstractExpression $tableOrIdentifier
	 * @param \Change\Db\Query\Expressions\AbstractExpression|null $joinCondition
	 * @return \Change\Db\Query\Expressions\Join
	 */
	protected function leftJoin($tableOrIdentifier, $joinCondition = null)
	{
		return new \Change\Db\Query\Expressions\Join($tableOrIdentifier, \Change\Db\Query\Expressions\Join::LEFT_OUTER_JOIN,
			$this->processJoinCondition($joinCondition));
	}

	/**
	 * @param \Change\Db\Query\Expressions\AbstractExpression|null $joinCondition
	 * @return \Change\Db\Query\Expressions\UnaryOperation|null
	 */
	protected function processJoinCondition($joinCondition = null)
	{
		$joinExpr = null;
		if ($joinCondition instanceof \Change\Db\Query\Predicates\InterfacePredicate)
		{
			$joinExpr = new \Change\Db\Query\Expressions\UnaryOperation($joinCondition, 'ON');
		}
		elseif ($joinCondition instanceof \Change\Db\Query\Expressions\Column || $joinCondition instanceof \Change\Db\Query\Expressions\ExpressionList)
		{
			$p = new \Change\Db\Query\Expressions\Parentheses($joinCondition);
			$joinExpr = new \Change\Db\Query\Expressions\UnaryOperation($p, 'USING');
		}
		return $joinExpr;
	}
} 