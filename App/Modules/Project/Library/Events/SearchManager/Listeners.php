<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Events\SearchManager;

/**
 * @name \Project\Library\Events\SearchManager\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 * @throws \Exception
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function ($event)
		{
			(new \Project\Library\Events\SearchManager\SearchDocuments())->execute($event);
		};
		$this->listeners[] = $events->attach('searchDocuments', $callback, 10);
	}
}