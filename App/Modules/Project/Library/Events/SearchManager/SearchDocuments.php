<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Events\SearchManager;

/**
 * @name \Project\Library\Events\SearchManager\SearchDocuments
 */
class SearchDocuments
{
	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Events\Event $event)
	{
		if ($event->getParam('documents') !== null || $event->getParam('query') !== null)
		{
			return;
		}

		$modelName = $event->getParam('modelName');
		switch ($modelName)
		{
			case 'Project_Library_Work':
				$indexTable = \Project\Library\Documents\Work::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\Work::ALIASES_INDEX_PREFIX;
				break;

			case 'Project_Library_WorkGroup':
				$indexTable = \Project\Library\Documents\WorkGroup::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\WorkGroup::ALIASES_INDEX_PREFIX;
				break;

			case 'Project_Library_Contributor':
				$indexTable = \Project\Library\Documents\Contributor::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\Contributor::ALIASES_INDEX_PREFIX;
				break;

			case 'Project_Library_Publisher':
				$indexTable = \Project\Library\Documents\Publisher::ALIASES_INDEX_TABLE;
				$fieldPrefix = \Project\Library\Documents\Publisher::ALIASES_INDEX_PREFIX;
				break;

			default:
				return;
		}

		$this->searchDocuments($event, $indexTable, $fieldPrefix);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @param string $table
	 * @param string $fieldPrefix
	 */
	protected function searchDocuments(\Change\Events\Event $event, $table, $fieldPrefix)
	{
		$applicationServices = $event->getApplicationServices();
		$dbProvider = $applicationServices->getDbProvider();
		$documentManager = $applicationServices->getDocumentManager();

		$searchString = \Project\Library\Indexer\AliasesIndexer::cleanUpAlias($event->getParam('searchString'));
		$exactMatchSearchString = \Project\Library\Indexer\AliasesIndexer::cleanUpExactMatchAlias($event->getParam('searchString'));

		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$predicate = $fb->logicOr(
			$fb->logicAnd(
				$fb->eq('exact_match', 0),
				$fb->like($fb->column($fieldPrefix . 'alias'), $fb->string($searchString), $event->getParam('mode'))
			),
			$fb->logicAnd(
				$fb->eq('exact_match', 1),
				$fb->like(
					$fb->column($fieldPrefix . 'alias'), $fb->string($exactMatchSearchString), \Change\Db\Query\Predicates\Like::EXACT
				)
			)
		);
		$qb->select($fb->alias($fb->countDistinct($fb->column($fieldPrefix . 'id')), 'count'))
			->from($table)
			->where($predicate);
		$sq = $qb->query();
		$count = $sq->getFirstResult($sq->getRowsConverter()->addIntCol('count')->singleColumn('count'));
		$event->setParam('count', $count);

		$documents = [];
		if ($count)
		{
			$qb = $dbProvider->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->column($fieldPrefix . 'id'), 'id'))
				->from($table)
				->where($predicate)
				->group($fb->column($fieldPrefix . 'id'))->group($fb->column($fieldPrefix . 'label'))
				->orderAsc($fb->column($fieldPrefix . 'label'))->orderAsc($fb->column('id'));
			$sq = $qb->query();
			$sq->setStartIndex($event->getParam('offset'));
			$sq->setMaxResults($event->getParam('limit'));
			$ids = $sq->getResults($sq->getRowsConverter()->addIntCol('id')->singleColumn('id'));

			$qb = $dbProvider->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->column($fieldPrefix . 'id'), 'id'), $fb->alias($fb->column($fieldPrefix . 'alias_title'), 'alias'))
				->from($table)
				->where($fb->logicAnd(
					$fb->in($fb->column($fieldPrefix . 'id'), $ids),
					$predicate
				))
				->orderAsc($fb->column($fieldPrefix . 'label'))->orderAsc($fb->column('id'))->orderDesc($fb->column('priority'));
			$sq = $qb->query();
			$rows = $sq->getResults($sq->getRowsConverter()->addIntCol('id')->addStrCol('alias'));

			$lastId = 0;
			foreach ($rows as $row)
			{
				$id = $row['id'];
				if ($lastId === $id)
				{
					continue;
				}
				$lastId = $id;

				/** @var \Project\Library\Indexer\AliasedDocument $document */
				$document = $documentManager->getDocumentInstance($id);
				if ($document)
				{
					if ($row['alias'] !== $document->getLabel())
					{
						$document->setFoundAlias($row['alias']);
					}
					$documents[] = $document;
				}
			}
		}

		$event->setParam('documents', $documents);
	}
} 