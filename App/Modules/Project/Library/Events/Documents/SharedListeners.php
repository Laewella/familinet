<?php
/**
 * Copyright (C) 2015 Laewella
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Events\Documents;

/**
 * @name \Project\Library\Events\Documents\SharedListeners
 */
class SharedListeners
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Project\Library\Events\Documents\Documents())->onDefaultSetLockedVisibilityContexts($event);
		};
		$events->attach('Documents', 'setLockedVisibilityContexts', $callback, 5);
	}
}