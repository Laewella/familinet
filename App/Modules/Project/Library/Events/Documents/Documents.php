<?php
/**
 * Copyright (C) 2015 Laewella
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Events\Documents;

/**
 * @name \Project\Library\Events\Documents\Documents
 */
class Documents
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultSetLockedVisibilityContexts(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if (!($document instanceof \Rbs\Generic\Documents\Typology))
		{
			return;
		}
		if (\Change\Stdlib\StringUtils::beginsWith($document->getModelName(), 'Project_Library_'))
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$label = $i18nManager->trans('m.project.library.admin.attribute_context_mass', ['ucf']);
			(new \Rbs\Generic\Events\Documents\LockedContextSetter())->set($event, 'mass', true, $label);
			$label = $i18nManager->trans('m.project.library.admin.attribute_context_main', ['ucf']);
			(new \Rbs\Generic\Events\Documents\LockedContextSetter())->set($event, 'main', true, $label);
		}
	}
}