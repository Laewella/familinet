<?php
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\AliasedDocumentTrait
 */
trait AliasedDocumentTrait
{
	/**
	 * @var \Project\Library\Indexer\AliasesIndexer
	 */
	protected static $aliasesIndexer;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Project\Library\Indexer\AliasesIndexer
	 */
	public static function getAliasesIndexer(\Change\Db\DbProvider $dbProvider)
	{
		if (!self::$aliasesIndexer)
		{
			self::$aliasesIndexer = new \Project\Library\Indexer\AliasesIndexer($dbProvider, self::ALIASES_INDEX_TABLE, self::ALIASES_INDEX_PREFIX);
		}
		return self::$aliasesIndexer;
	}

	/**
	 * @var string
	 */
	protected $foundAlias;

	/**
	 * @return string
	 */
	public function getFoundAlias()
	{
		return $this->foundAlias;
	}

	/**
	 * @param string $foundAlias
	 * @return $this
	 */
	public function setFoundAlias($foundAlias)
	{
		$this->foundAlias = $foundAlias;
		return $this;
	}

	protected function onDeleteAliasesIndex()
	{
		self::getAliasesIndexer($this->dbProvider)->delete($this->getId());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onCreateAliasesIndex(\Change\Documents\Events\Event $event)
	{
		$services = $event->getServices('Project_FamilinetServices');
		if (!($services instanceof \Project\Familinet\FamilinetServices))
		{
			throw new \RuntimeException('FamilinetServices not set', 999999);
		}
		$this->createAliasesIndex(self::getAliasesIndexer($this->dbProvider), $services->getAttributeManager());
	}

	/**
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function reIndexAliases(\Project\Familinet\AttributeManager $attributeManager)
	{
		$indexer = self::getAliasesIndexer($this->dbProvider);
		$indexer->delete($this->getId());
		return $this->createAliasesIndex($indexer, $attributeManager);
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	abstract public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer,
		\Project\Familinet\AttributeManager $attributeManager);
}