<?php
namespace Project\Library\Admin;

/**
 * @name \Project\Library\Admin\GetModelTwigAttributes
 */
class GetModelTwigAttributes
{
	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Events\Event $event)
	{
		$view = $event->getParam('view');
		$model = $event->getParam('model');

		$adminManager = $event->getTarget();
		if ($adminManager instanceof \Rbs\Admin\AdminManager && $model instanceof \Change\Documents\AbstractModel)
		{
			$attributes = $event->getParam('attributes');

			$modelName = $model->getName();
			if ($view === 'edit')
			{
				$asideDirectives = [];
				if ($modelName === 'Project_Library_Genre' || $modelName === 'Rbs_Geo_Country' || $modelName === 'Project_Library_ContributionType'
					|| $modelName === 'Project_Library_WorkStatus' || $modelName === 'Project_Library_Publisher'
					|| $modelName === 'Rbs_Generic_Typology')
				{
					$asideDirectives[] = [
						'name' => 'rbs-aside-code',
						'attributes' => [
							['name' => 'context-id', 'value' => \Project\Library\WebSiteParsers\AbstractParser::CODE_CONTEXT],
							['name' => 'icon', 'value' => 'icon-globe'],
							['name' => 'document', 'value' => 'document']
						]
					];
				}
				$attributes['asideDirectives'] = array_merge($attributes['asideDirectives'], $asideDirectives);
			}

			$event->setParam('attributes', $attributes);
		}
	}
}