<?php
namespace Project\Library\Admin;

/**
 * @name \Project\Library\Admin\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 * @throws \Exception
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function ($event)
		{
			(new \Project\Library\Admin\GetModelTwigAttributes())->execute($event);
		};
		$this->listeners[] = $events->attach('getModelTwigAttributes', $callback, 0);
	}
}