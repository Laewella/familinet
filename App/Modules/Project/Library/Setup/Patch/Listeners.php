<?php
/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Setup\Patch;

/**
 * @name \Project\Library\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		$this->executePatch('Project_Library_001', 'Fill birth and death years.', [$this, 'patch0001']);
		// Patch 002 and 003 deleted.
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Exception
	 */
	protected function patch0001(\Change\Plugins\Patch\Patch $patch)
	{
		$transactionManager = $this->applicationServices->getTransactionManager();
		$updated = 0;
		$lastId = 0;

		do
		{
			try
			{
				$transactionManager->begin();
				$dqb = $this->applicationServices->getDocumentManager()->getNewQuery('Project_Library_Contributor');
				$fb = $dqb->getFragmentBuilder();
				$dqb->andPredicates(
					$fb->logicOr($dqb->isNotNull('birthDate'), $dqb->isNotNull('deathDate')),
					$dqb->gt($fb->getDocumentColumn('id'), $lastId)
				);
				$dqb->addOrder($fb->getDocumentColumn('id'));
				$documents = $dqb->getDocuments(0, 50)->toArray();

				/** @var \Project\Library\Documents\Contributor $document */
				foreach ($documents as $document)
				{
					$birthDate = $document->getBirthDate();
					if ($birthDate)
					{
						$document->setBirthYear($birthDate->format('Y'));
					}

					$deathDate = $document->getDeathDate();
					if ($deathDate)
					{
						$document->setDeathYear($deathDate->format('Y'));
					}
					$document->save();

					$updated++;
					$lastId = $document->getId();
				}
				echo '.';

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->sendError($e->getMessage());
				$patch->setException($e);
				throw $transactionManager->rollBack($e);
			}
		}
		while ($documents);

		echo \PHP_EOL;
		$this->sendInfo('documents updated ' . $updated);
		$patch->addInstallationData('documentsUpdated', $updated);

		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}
}