<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Setup;

/**
 * @name \Project\Library\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			$td = $schemaManager->newTableDefinition('project_library_dat_work_aliases');
			$td->addField($schemaManager->newIntegerFieldDefinition('work_id')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('work_label')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('work_alias')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('work_alias_title')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('priority')->setNullable(false))
				->addField($schemaManager->newBooleanFieldDefinition('exact_match')->setNullable(false)->setDefaultValue(0))
				->addKey($this->newPrimaryKey()
					->addField($td->getField('work_id'))
					->addField($td->getField('work_alias'))
					->addField($td->getField('exact_match'))
				);
			$this->tables['project_library_dat_work_aliases'] = $td;

			$td = $schemaManager->newTableDefinition('project_library_dat_work_group_aliases');
			$td->addField($schemaManager->newIntegerFieldDefinition('group_id')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('group_label')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('group_alias')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('group_alias_title')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('priority')->setNullable(false))
				->addField($schemaManager->newBooleanFieldDefinition('exact_match')->setNullable(false)->setDefaultValue(0))
				->addKey($this->newPrimaryKey()
					->addField($td->getField('group_id'))
					->addField($td->getField('group_alias'))
					->addField($td->getField('exact_match'))
				);
			$this->tables['project_library_dat_work_group_aliases'] = $td;

			$td = $schemaManager->newTableDefinition('project_library_dat_contributor_aliases');
			$td->addField($schemaManager->newIntegerFieldDefinition('contributor_id')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('contributor_label')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('contributor_alias')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('contributor_alias_title')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('priority')->setNullable(false))
				->addField($schemaManager->newBooleanFieldDefinition('exact_match')->setNullable(false)->setDefaultValue(0))
				->addKey($this->newPrimaryKey()
					->addField($td->getField('contributor_id'))
					->addField($td->getField('contributor_alias'))
					->addField($td->getField('exact_match'))
				);
			$this->tables['project_library_dat_contributor_aliases'] = $td;

			$td = $schemaManager->newTableDefinition('project_library_dat_publisher_aliases');
			$td->addField($schemaManager->newIntegerFieldDefinition('publisher_id')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('publisher_label')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('publisher_alias')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('publisher_alias_title')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('priority')->setNullable(false))
				->addField($schemaManager->newBooleanFieldDefinition('exact_match')->setNullable(false)->setDefaultValue(0))
				->addKey($this->newPrimaryKey()
					->addField($td->getField('publisher_id'))
					->addField($td->getField('publisher_alias'))
					->addField($td->getField('exact_match'))
				);
			$this->tables['project_library_dat_publisher_aliases'] = $td;
		}
		return $this->tables;
	}
}
