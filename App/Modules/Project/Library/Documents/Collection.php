<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Collection
 */
class Collection extends \Compilation\Project\Library\Documents\Collection
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event); 
		$restResult = $event->getParam('restResult');

		/** @var $document \Project\Library\Documents\Collection */
		$document = $event->getDocument();
		if ($document->getPublisher())
		{
			$restResult->setProperty('publisherId', $document->getPublisher()->getId());
		}
	}
}
