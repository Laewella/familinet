<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Loan
 */
class Loan extends \Compilation\Project\Library\Documents\Loan
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');
			
		/** @var $document Loan */
		$document = $event->getDocument();
		if ($document->getPossession())
		{
			$restResult->setProperty('possessionId', $document->getPossession()->getId());
			$restResult->setProperty('possessionLabel', $document->getPossession()->getLabel());
			
			if ($document->getPossession()->getWork())
			{
				$restResult->setProperty('workId', $document->getPossession()->getWork()->getId());
				$restResult->setProperty('workLabel', $document->getPossession()->getWork()->getLabel());
			}
		}
	}
	
	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->getBorrowerName();
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Nothing to do because Label is state less
		return $this;
	}
}
