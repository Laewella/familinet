<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\WorkGroup
 */
class WorkGroup extends \Compilation\Project\Library\Documents\WorkGroup implements \Project\Library\Indexer\AliasedDocument
{
	use \Project\Library\Indexer\AliasedDocumentTrait;

	const ALIASES_INDEX_TABLE = 'project_library_dat_work_group_aliases';
	const ALIASES_INDEX_PREFIX = 'group_';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->onCreateAliasesIndex($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function () { $this->onDeleteAliasesIndex(); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->onCreateAliasesIndex($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, function () { $this->onDeleteAliasesIndex(); }, 10);
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer, \Project\Familinet\AttributeManager $attributeManager)
	{
		$label = $this->getLabel();
		$aliases = [$label];
		foreach ($this->getAliases() as $aliasDocument)
		{
			$aliases[] = $aliasDocument->getLabel();
		}

		return $indexer->index($this->getId(), $label, $aliases);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$foundAlias = $this->getFoundAlias();
			if ($foundAlias)
			{
				$restResult->setProperty('foundAlias', $foundAlias);
			}
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('aliasesData', $this->getAliasesInfos());
		}
	}

	/**
	 * @return array
	 */
	protected function getAliasesInfos()
	{
		$aliasesInfos = [];
		foreach ($this->getAliases() as $alias)
		{
			$aliasesInfos[] = [
				'label' => $alias->getLabel(), 'nationalitiesData' => $alias->getNationalitiesInfos(), 'comment' => $alias->getComment()
			];
		}
		return $aliasesInfos;
	}
}
