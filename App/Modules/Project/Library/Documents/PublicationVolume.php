<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\PublicationVolume
 */
class PublicationVolume extends \Compilation\Project\Library\Documents\PublicationVolume
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event); 
		$restResult = $event->getParam('restResult');

		if ($this->getPublication())
		{
			$number = $this->getNumber();
			if ($number)
			{
				$restResult->setProperty('number', $number);
				$restResult->setProperty('optionLabel', '(' . $number . ') ' . $this->getLabel());
			}
			$restResult->setProperty('publicationId', $this->getPublication()->getId());
			if ($this->getPublication()->getWork())
			{
				$restResult->setProperty('workLabel', $this->getPublication()->getWork()->getLabel());
				$restResult->setProperty('workId', $this->getPublication()->getWork()->getId());
			}
		}
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->refreshOrder();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->refreshOrder();
	}

	/**
	 * Refresh the label property.
	 */
	protected function refreshOrder()
	{
		$this->setOrder($this->getNumber());
	}
}
