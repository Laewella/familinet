<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\LoanStatus
 */
class LoanStatus extends \Compilation\Project\Library\Documents\LoanStatus
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		/** @var $document LoanStatus */
		$document = $event->getDocument();
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->setProperty('iconName', $document->getIconName());
			$restResult->setProperty('textClass', $document->getTextClass());
			$restResult->setProperty('code', $document->getCode());
		}
	}
}
