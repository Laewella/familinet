<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Work
 */
class Work extends \Compilation\Project\Library\Documents\Work implements \Project\Library\Indexer\AliasedDocument
{
	use \Project\Library\Indexer\AliasedDocumentTrait;

	const ALIASES_INDEX_TABLE = 'project_library_dat_work_aliases';
	const ALIASES_INDEX_PREFIX = 'work_';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->onCreateAliasesIndex($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function () { $this->onDeleteAliasesIndex(); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->onCreateAliasesIndex($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, function () { $this->onDeleteAliasesIndex(); }, 10);
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer, \Project\Familinet\AttributeManager $attributeManager)
	{
		$label = $this->getLabel();
		$aliases = [$label];
		$subtitle = $this->getSubtitle();
		if ($subtitle)
		{
			$aliases[] = $subtitle;
		}
		foreach ($this->getAliases() as $aliasDocument)
		{
			$aliases[] = $aliasDocument->getLabel();
			$subtitle = $aliasDocument->getSubtitle();
			if ($subtitle)
			{
				$aliases[] = $subtitle;
			}
		}

		$documentIds = [$this->getId()];
		$query = $this->documentManager->getNewQuery('Project_Library_Publication');
		$query->andPredicates($query->eq('work', $this));
		$publicationIds = $query->getDocumentIds();
		if ($publicationIds)
		{
			$documentIds = \array_merge($documentIds, $publicationIds);
			$volumeQuery = $this->documentManager->getNewQuery('Project_Library_PublicationVolume');
			$volumeQuery->andPredicates($volumeQuery->in('publication', $publicationIds));
			$volumeIds = $volumeQuery->getDocumentIds();
			if ($volumeIds)
			{
				$documentIds = \array_merge($documentIds, $volumeIds);
			}
		}
		$exactMatchAliases = $attributeManager->getValuesByCodesAndDocumentIds(['code_barre', 'id_amazon', 'ean', 'isbn'], $documentIds);

		return $indexer->index($this->getId(), $label, $aliases, $exactMatchAliases);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$restResult->setProperty('workType', $this->getWorkType());
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$foundAlias = $this->getFoundAlias();
			if ($foundAlias)
			{
				$restResult->setProperty('foundAlias', $foundAlias);
			}
			$year = $this->getYear();
			if ($year)
			{
				$restResult->setProperty('year', $year);
			}
			$endYear = $this->getEndYear();
			if ($endYear)
			{
				$restResult->setProperty('endYear', $endYear);
			}
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('aliasesData', $this->getAliasesInfos());
			$contributorsInfos = $this->getContributionsInfos();
			if ($contributorsInfos)
			{
				$restResult->setProperty('contributions', $contributorsInfos);
			}

			$cm = $event->getApplicationServices()->getCollectionManager();
			$collection = $cm->getCollection('Project_Library_Languages');
			if ($collection !== null)
			{
				$language = $collection->getItemByValue($this->getOriginalLanguage());
				if ($language !== null)
				{
					$restResult->setProperty('originalLanguageLabel', $language->getLabel());
				}
			}
			$collection = $cm->getCollection('Project_Library_Audiences');
			if ($collection !== null)
			{
				$audience = $collection->getItemByValue($this->getAudience());
				if ($audience !== null)
				{
					$restResult->setProperty('audienceLabel', $audience->getLabel());
				}
			}
		}

		$nationalitiesCount = $this->getNationalitiesCount();
		$restResult->setProperty('nationalitiesCount', $nationalitiesCount);
		if ($nationalitiesCount)
		{
			$restResult->setProperty('nationalitiesData', $this->getNationalitiesInfos());
		}
	}

	/**
	 * @return String|null
	 */
	public function getWorkType()
	{
		$typology = $this->getDocumentManager()->getTypologyByDocument($this);
		if ($typology)
		{
			return $typology->getTitle();
		}
		return null;
	}

	/**
	 * @return array
	 */
	protected function getAliasesInfos()
	{
		$aliasesInfos = [];
		foreach ($this->getAliases() as $alias)
		{
			$aliasesInfos[] = [
				'label' => $alias->getLabel(), 'subtitle' => $alias->getSubtitle(), 'original' => $alias->getOriginal(),
				'nationalitiesData' => $alias->getNationalitiesInfos(), 'comment' => $alias->getComment()
			];
		}
		return $aliasesInfos;
	}

	/**
	 * @return array
	 */
	public function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}

	/**
	 * @return array
	 */
	protected function getContributionsInfos()
	{
		$query = $this->getDocumentManager()->getNewQuery('Project_Library_Contribution');
		$query->andPredicates($query->getPredicateBuilder()->eq('work', $this));
		$contributions = $query->getDocuments();

		$contributionsInfos = array();
		foreach ($contributions as $contribution)
		{
			/* @var $contribution \Project\Library\Documents\Contribution */
			$contributor = $contribution->getContributor();
			if (!$contributor)
			{
				continue;
			}

			$contributorData = [
				'id' => $contributor->getId(),
				'model' => $contributor->getDocumentModelName(),
				'label' => $contributor->getLabel(),
				'detail' => $contribution->getDetail(),
				'nationalitiesData' => []
			];
			foreach ($contributor->getNationalities() as $nationality)
			{
				$contributorData['nationalitiesData'][] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
			}

			foreach ($contribution->getContributionTypes() as $contributionType)
			{
				if (!isset($contributionsInfos[$contributionType->getLabel()]))
				{
					$contributionsInfos[$contributionType->getLabel()]['type'] = [
						'id' => $contributionType->getId(),
						'model' => $contributionType->getDocumentModelName(),
						'label' => $contributionType->getLabel(),
						'description' => $contributionType->getDescription()
					];
				}

				$contributionsInfos[$contributionType->getLabel()]['contributors'][] = $contributorData;
			}
		}
		ksort($contributionsInfos);
		return array_values($contributionsInfos);
	}
}
