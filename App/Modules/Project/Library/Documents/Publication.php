<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Publication
 */
class Publication extends \Compilation\Project\Library\Documents\Publication
{
	/**
	 * @return string
	 */
	public function getLabel()
	{
		$label = '';

		if ($this->getPublisher())
		{
			$label .= $this->getPublisher()->getLabel();
		}

		if ($this->getCollection())
		{
			$label = $label . ' - ' . $this->getCollection()->getLabel();
		}

		$formatLabel = $this->getFormatLabel();
		if ($formatLabel)
		{
			$label = $label . ' (' . $formatLabel . ')';
		}
		
		if ($this->getName())
		{
			$label = $label . ', ' . $this->getName();
		}

		return $label;
	}

	/**
	 * @return string
	 */
	protected function getFormatLabel()
	{
		$formatLabel = null;

		$format = $this->getDocumentManager()->getAttributeValues($this)->get('format');
		if ($format)
		{
			$attribute = $this->getDocumentManager()->getTypologyByDocument($this)->getAttributeByName('format');
			if ($attribute)
			{
				$fct = $attribute->getAJAXFormatter();
				if (\is_callable($fct))
				{
					$res = $fct($format, []);
					if (isset($res['common']['title']))
					{
						$formatLabel = $res['common']['title'];
					}
				}
				
			}
		}

		return $formatLabel;
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Nothing to do because Label is state less
		return $this;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event);
		$restResult = $event->getParam('restResult');

		/** @var $document \Project\Library\Documents\Publication */
		$document = $event->getDocument();
		if ($document->getWork())
		{
			$restResult->setProperty('workId', $document->getWork()->getId());
		}

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('languagesSelection', $this->getLanguagesSelection());

			$languagesInfos = $this->getLanguagesInfos($event->getApplicationServices()->getCollectionManager());
			if ($languagesInfos)
			{
				$restResult->setProperty('languagesInfos', $languagesInfos);
			}
		}

		$nationalitiesCount = $this->getNationalitiesCount();
		$restResult->setProperty('nationalitiesCount', $nationalitiesCount);
		if ($nationalitiesCount)
		{
			$restResult->setProperty('nationalitiesData', $this->getNationalitiesInfos());
		}
	}

	/**
	 * @return array
	 */
	public function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->normalizeVolumesDetail();
		$this->refreshLanguageLabel();
		$this->handleOneShot();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->normalizeVolumesDetail();
		$this->refreshLanguageLabel();
		$this->handleOneShot();
	}

	/**
	 * Normalise les description des détails de la possession
	 */
	protected function normalizeVolumesDetail()
	{
		/** @var \Project\Library\Tools\VolumesDetailTools $tools */
		$tools = new \Project\Library\Tools\VolumesDetailTools();
		$tools->addDescription($this->getVolumesDetail());
		$this->setVolumesDetail($tools->getLabel());
	}

	/**
	 * Refresh the label property.
	 */
	protected function refreshLanguageLabel()
	{
		foreach ($this->getLanguages() as $language)
		{
			$language->setLabel($language->getLanguage() . ' ' . $language->getLanguageFunction());
		}
	}

	/**
	 * Handle one shot.
	 */
	protected function handleOneShot()
	{
		if ($this->getOneShot())
		{
			$this->setVolumesDetail('1');
			$query = $this->getDocumentManager()->getNewQuery('Project_Library_WorkStatus');
			$query->andPredicates($query->eq('code', 'ended'));
			$status = $query->getFirstDocument();
			if ($status instanceof \Project\Library\Documents\WorkStatus)
			{
				$this->setStatus($status);
			}
		}
	}

	/**
	 *
	 * @param \Change\Collection\CollectionManager $cm
	 * @return array
	 */
	protected function getLanguagesInfos($cm)
	{
		$languages = $this->getLanguages();

		$languagesCollection = $cm->getCollection('Project_Library_Languages');
		$functionsCollection = $cm->getCollection('Project_Library_LanguageFunctions');
		if (!$languagesCollection || !$functionsCollection)
		{
			return [];
		}

		$languagesInfos = [];
		foreach ($languages as $language)
		{
			$languageLanguage = $languagesCollection->getItemByValue($language->getLanguage());
			$languageFunction = $functionsCollection->getItemByValue($language->getLanguageFunction());

			if ($languageLanguage !== null && $languageFunction !== null)
			{
				if (!isset($languagesInfos[$languageFunction->getLabel()]))
				{
					$languagesInfos[$languageFunction->getLabel()]['function'] = $languageFunction->getLabel();
				}

				$languagesInfos[$languageFunction->getLabel()]['languages'][] = $languageLanguage->getLabel();
			}
		}

		ksort($languagesInfos);
		return array_values($languagesInfos);
	}

	/**
	 * @return array
	 */
	protected function getLanguagesSelection()
	{
		$languages = $this->getLanguages();

		$languagesSelection = ['fr' => []];
		foreach ($languages as $language)
		{
			$languageLanguage = $language->getLanguage();
			$languageFunction = $language->getLanguageFunction();
			if ($languageLanguage && $languageFunction)
			{
				$languagesSelection[$languageLanguage][$languageFunction] = true;
			}
		}

		return $languagesSelection;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'languagesSelection')
		{
			$this->$this->ignoredPropertiesForRestEvents['languaes'];

			$collectionManager = $event->getApplicationServices()->getCollectionManager();
			$languagesCollection = $collectionManager->getCollection('Project_Library_Languages');
			if (!$languagesCollection)
			{
				$event->getApplication()->getLogging()->warn(__METHOD__, 'Project_Library_Languages collection missing.');
				return false;
			}
			$functionsCollection = $collectionManager->getCollection('Project_Library_LanguageFunctions');
			if (!$functionsCollection)
			{
				$event->getApplication()->getLogging()->warn(__METHOD__, 'Project_Library_LanguageFunctions collection missing.');
				return false;
			}

			$languages = $this->getLanguages();
			$languages->removeAll();
			if ($value  && \is_array($value))
			{
				foreach ($value as $lang => $functions)
				{
					if (!$languagesCollection->getItemByValue($lang))
					{
						$event->getApplication()->getLogging()->warn(__METHOD__, 'Unknown language code:', $lang);
						continue;
					}

					foreach ($functions as $languageFunction => $bool)
					{
						if (!$functionsCollection->getItemByValue($languageFunction))
						{
							$event->getApplication()->getLogging()->warn(__METHOD__, 'Unknown language function:', $languageFunction);
							continue;
						}
						$languages->add($this->newPublicationLanguage()->setLanguage($lang)->setLanguageFunction($languageFunction));
					}
				}
			}
			return true;
		}
		return parent::processRestData($name, $value, $event);
	}
}
