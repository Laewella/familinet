<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Contribution
 */
class Contribution extends \Compilation\Project\Library\Documents\Contribution
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}

		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		if ($this->getContributionYear())
		{
			$restResult->setProperty('year', $this->getContributionYear());
			$restResult->setProperty('endYear', $this->getContributionEndYear());
		}
		else
		{
			$work = $this->getWork();
			if ($work)
			{
				$restResult->setProperty('year', $work->getYear());
				$restResult->setProperty('endYear', $work->getEndYear());
			}
		}

		$importance = $this->getImportance();
		if ($importance)
		{
			$collection = $event->getApplicationServices()->getCollectionManager()->getCollection('Project_Library_ContributionImportance');
			if ($collection)
			{
				$item = $collection->getItemByValue($importance);
				if ($item)
				{
					$restResult->setProperty('importanceTitle', $item->getTitle());
				}
			}
		}

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->setProperty('contributionTypes', $this->getContributionTypesInfos());
		}
	}


	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}

		parent::onDefaultRouteParamsRestResult($event); 
		$restResult = $event->getParam('restResult');

		if ($this->getWork())
		{
			$restResult->setProperty('workId', $this->getWorkId());
		}
		if ($this->getContributor())
		{
			$restResult->setProperty('contributorId', $this->getContributorId());
		}
	}

	/**
	 * @return array
	 */
	protected function getContributionTypesInfos()
	{
		$contributionTypesInfos = array();
		foreach ($this->getContributionTypes() as $type)
		{
			$contributionTypesInfos[] = [ 'label' => $type->getLabel() ];
		}
		return $contributionTypesInfos;
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->refreshLabel();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->refreshLabel();
	}

	/**
	 * Refresh the label property.
	 */
	protected function refreshLabel()
	{
		$this->setLabel($this->getContributor()->getLabel() . ' - ' . $this->getWork()->getLabel());
	}
}
