<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\GetAvailableWebsites
 */
class GetAvailableWebsites
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$type = $request->getQuery('type');
		if (!$type)
		{
			throw new \RuntimeException('No type!', 999999);
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$query = $documentManager->getNewQuery('Project_Library_WebSite');
		$query->addOrder('label');

		$result = ['websites' => []];
		foreach ($query->getDocuments() as $webSite)
		{
			/** @var \Project\Library\Documents\WebSite $webSite */
			foreach ($webSite->getParsers() as $parser)
			{
				$parserClass = $parser->getParserClass();
				if (!class_exists($parserClass))
				{
					throw new \RuntimeException('Invalid parser class: ' . $parserClass, 999999);
				}

				$callable = [$parserClass, 'getType'];
				if (\is_callable($callable) && $callable() === $type)
				{
					$data = $webSite->getInfos();
					$callableMinimal = [$parserClass, 'isMinimal'];
					$data['minimal'] = $callableMinimal();
					$result['websites'][] = $data;
					break;
				}
			}
		}

		$event->setResult($this->generateResult($result));
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}
}