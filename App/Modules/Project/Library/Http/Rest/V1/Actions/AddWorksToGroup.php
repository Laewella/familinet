<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\AddWorksToGroup
 */
class AddWorksToGroup
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();

		$group = $documentManager->getDocumentInstance($request->getPost('groupId'));
		if (!($group instanceof \Project\Library\Documents\WorkGroup))
		{
			throw new \RuntimeException('Invalid groupId', 999999);
		}

		$workIds = $request->getPost('workIds');
		if (!$workIds || !\is_array($workIds))
		{
			throw new \RuntimeException('Invalid workIds', 999999);
		}

		$countWorks = 0;
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();

			foreach ($workIds as $workId)
			{
				$work = $documentManager->getDocumentInstance($workId);
				if ($work instanceof \Project\Library\Documents\Work)
				{
					$work->getWorkGroups()->add($group);
					$work->save();
					$countWorks++;
				}
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->generateResult(['message' => 'Works added.', 'count' => $countWorks]));
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}
}