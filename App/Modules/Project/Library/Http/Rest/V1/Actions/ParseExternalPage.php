<?php
/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\ParseExternalPage
 */
class ParseExternalPage
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$url = $request->getQuery('url');
		if (!$url)
		{
			throw new \RuntimeException('No URL!', 999999);
		}

		$type = $request->getQuery('importType');
		if (!$type)
		{
			throw new \RuntimeException('No importType!', 999999);
		}

		$matches = [];
		if (!preg_match('#^[^/]+://([^/]+)/.*$#', $url, $matches) || !$matches[1])
		{
			throw new \RuntimeException('Invalid URL: ' . $url, 999999);
		}
		$domain = $matches[1];

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$geoManager = $genericServices->getGeoManager();

		$query = $documentManager->getNewQuery('Project_Library_WebSite');
		$query->andPredicates($query->eq('domain', $domain));
		foreach ($query->getDocuments() as $webSite)
		{
			/** @var \Project\Library\Documents\WebSite $webSite */
			foreach ($webSite->getParsers() as $parser)
			{
				$pattern = $parser->getPattern();
				if ($pattern && preg_match('/' . str_replace('/', '\/', $pattern) . '/', $url))
				{
					$parserClass = $parser->getParserClass();
					if (!class_exists($parserClass))
					{
						throw new \RuntimeException('Invalid parser class: ' . $parserClass, 999999);
					}

					$parserObject = new $parserClass($documentManager, $documentCodeManager, $geoManager, $url, $webSite);
					if (!($parserObject instanceof \Project\Library\WebSiteParsers\AbstractParser))
					{
						throw new \RuntimeException('Invalid parser class: ' . $parserClass, 999999);
					}

					if ($parserObject->getType() !== $type)
					{
						continue;
					}

					$event->setResult($this->generateResult($parserObject->parseData()));
					return;
				}
			}
		}

		throw new \RuntimeException('No matching web site definition!', 999999);
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}
}