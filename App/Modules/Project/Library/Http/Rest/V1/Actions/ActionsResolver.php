<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\ActionsResolver
 */
class ActionsResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	const RESOLVER_NAME = 'projectLibrary';

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @var array
	 */
	protected $actionClasses = [];

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
		$this->registerActionClass('parseExternalPage', \Project\Library\Http\Rest\V1\Actions\ParseExternalPage::class);
		$this->registerActionClass('getAvailableWebsites', \Project\Library\Http\Rest\V1\Actions\GetAvailableWebsites::class);
		$this->registerActionClass('addWorksToGroup', \Project\Library\Http\Rest\V1\Actions\AddWorksToGroup::class);
	}

	/**
	 * @return array
	 */
	public function getActionClasses()
	{
		return $this->actionClasses;
	}

	/**
	 * @param $actionName
	 * @param $class
	 */
	public function registerActionClass($actionName, $class)
	{
		$this->actionClasses[$actionName] = $class;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		$namespaces = [];
		$names = \array_keys($this->actionClasses);
		$base = \implode('.', $namespaceParts);
		foreach ($names as $name)
		{
			$namespaces[] = $base . '.' . $name;
		}
		return $namespaces;
	}

	/**
	 * Set Event params: resourcesActionName, documentId, LCID
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$nbParts = \count($resourceParts);
		if ($nbParts === 0 && $method === \Change\Http\Rest\Request::METHOD_GET)
		{
			\array_unshift($resourceParts, static::RESOLVER_NAME);
			$event->setParam('namespace', implode('.', $resourceParts));
			$event->setParam('resolver', $this);
			$action = function ($event)
			{
				$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
				$action->execute($event);
			};
			$event->setAction($action);
			$event->setAuthorization(null);
			return;
		}
		if ($nbParts === 1)
		{
			$actionClasses = $this->getActionClasses();
			$actionName = $resourceParts[0];
			if (!isset($actionClasses[$actionName]))
			{
				//Action not found
				return;
			}
			$actionClass = $actionClasses[$actionName];
			if (!\class_exists($actionClass))
			{
				//Action Class not
				return;
			}
			$instance = new $actionClass();
			$callable = [$instance, 'execute'];
			if (!\is_callable($callable))
			{
				//Callable Not found
				return;
			}
			$event->setParam('actionName', $actionName);
			$event->setAction(function ($event) use ($callable) { $callable($event); });
			return;
		}
	}
}