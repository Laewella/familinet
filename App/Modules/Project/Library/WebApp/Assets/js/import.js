(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.service('projectLibraryImport', ['RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter',
		'RbsChange.ErrorFormatter', projectLibraryImport]);

	function projectLibraryImport(REST, i18n, NotificationCenter, ErrorFormatter) {
		var me = this;

		this.initImportManager = function (scope, type, applyImportedData) {
			scope.importManager = {
				url: null,
				importing: false,
				import: function(doc) {
					scope.importManager.importing = true;
					var params = { url: scope.importManager.url, importType: type };
					REST.call(REST.getBaseUrl('projectLibrary/parseExternalPage'), params).then(
						function(data) {
							applyImportedData(doc, data);
							scope.importManager.importing = false;
						},
						function(data) {
							NotificationCenter.error(
								i18n.trans('m.project.library.app.error_loading_data|ucf'),
								ErrorFormatter.format(data),
								'EDITOR'
							);
							scope.importManager.importing = false;
						}
					);
				}
			}
		};

		this.isInArray = function(array, value, propertyName) {
			if (!angular.isArray(array)) return false;
			var result = false;
			angular.forEach(array, function(element) {
				if (element.hasOwnProperty(propertyName) && element[propertyName] === value[propertyName]) {
					result = true;
				}
			});
			return result;
		};

		this.addSubDocuments = function(doc, property, subDocuments, checkProperty, single) {
			if (single) {
				var subDocument = angular.isArray(subDocuments) ? subDocuments[0] : subDocuments;
				if (angular.isObject(subDocument)) {
					doc[property] = subDocument;
				}
			}
			else {
				angular.forEach(subDocuments, function(subDocument) {
					if (angular.isObject(subDocument)) {
						if (!angular.isArray(doc[property])) {
							doc[property] = [];
						}
						if (!me.isInArray(doc[property], subDocument, checkProperty)) {
							doc[property].push(subDocument);
						}
					}
				});
			}
		};

		this.setTypology = function(doc, typology, attributes) {
			if (!angular.isObject(doc.typology$)) {
				doc.typology$ = {};
			}
			doc.typology$.__id = typology;

			if (attributes) {
				angular.forEach(attributes, function(value, attribute) {
					doc.typology$[attribute] = value;
				});
			}
		};

		this.applyImportedData = function (doc, data, properties) {
			if (angular.isArray(properties.scalar)) {
				angular.forEach(properties.scalar, function(property) {
					if (data[property]) {
						doc[property] = data[property];
					}
				});
			}

			if (angular.isArray(properties.richText)) {
				angular.forEach(properties.richText, function(property) {
					if (data[property]) {
						doc[property] = { e: 'Markdown', t: data[property], h: null };
					}
				});
			}

			if (angular.isArray(properties.subDoc)) {
				angular.forEach(properties.subDoc, function(propertyData) {
					if (data[propertyData.name]) {
						me.addSubDocuments(doc, propertyData.name, data[propertyData.name], propertyData.check, propertyData.single || false);
					}
				});
			}

			if (data.typology) {
				me.setTypology(doc, data.typology, data.attributes);
			}
		}
	}
})();