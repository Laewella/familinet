/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('projectLibraryWorkAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/work-aliases.html',
			scope: { aliases: '=projectLibraryWorkAliases' }
		};
	});

	app.directive('projectLibraryWorkGroupAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/work-group-aliases.html',
			scope: { aliases: '=projectLibraryWorkGroupAliases' }
		};
	});

	app.directive('projectLibraryContributorAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/contributor-aliases.html',
			scope: { aliases: '=projectLibraryContributorAliases' },
			link: function(scope) {
				scope.$watch('aliases', function() {
					scope.standard = [];
					scope.websiteSpecific = [];
					angular.forEach(scope.aliases, function(alias) {
						if (alias.forWebsite) {
							scope.websiteSpecific.push(alias);
						}
						else {
							scope.standard.push(alias);
						}
					});
				});
			}
		};
	});

	app.directive('projectLibraryPublisherAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/publisher-aliases.html',
			scope: { aliases: '=projectLibraryPublisherAliases' }
		};
	});

	app.directive('projectLibraryStatus', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/status.html',
			scope: { status: '=projectLibraryStatus', mode: '@' },
			link: function(scope, element) {
				element.find('.title-tooltip').tooltip();
			}
		};
	});

	app.directive('projectLibraryNationality', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/nationality.html',
			scope: { nationality: '=projectLibraryNationality', mode: '@' }
		};
	});

	app.directive('projectLibraryPublication', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/publication.html',
			scope: { publication: '=projectLibraryPublication', mode: '@' }
		};
	});

	app.directive('projectLibraryLinks', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/links.html',
			scope: { links: '=projectLibraryLinks' }
		};
	});

	app.directive('projectLibraryLinksEditor', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/links-editor.html',
			scope: { modelName: '@', document: '=' }
		};
	});

	app.directive('projectLibraryUserLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/user-link.html',
			scope: { user: '=projectLibraryUserLink' }
		};
	});

	app.directive('projectLibraryUserLinks', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/user-links.html',
			scope: { users: '=projectLibraryUserLinks' }
		};
	});

	app.directive('projectLibraryContributorLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/contributor-link.html',
			scope: { contributor: '=projectLibraryContributorLink' }
		};
	});

	app.directive('projectLibraryWorkLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/work-link.html',
			scope: { work: '=projectLibraryWorkLink' }
		};
	});

	app.directive('projectLibraryContributionRoles', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/contribution-roles.html',
			scope: { contribution: '=projectLibraryContributionRoles' }
		};
	});

	app.directive('projectLibraryImportForm', ['RbsChange.REST', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', 'RbsChange.i18n',
		function(REST, ErrorFormatter, NotificationCenter, i18n) {
			return {
				restrict: 'A',
				templateUrl: 'Project_Library/html/display-directives/import-form.html',
				scope: false,
				link: function(scope, element, attrs) {
					var type = attrs.type;
					if (type) {
						REST.call(REST.getBaseUrl('projectLibrary/getAvailableWebsites'), { type: type }).then(
							function(data) {
								scope.importManager.websites = data.websites;
							},
							function(data) {
								NotificationCenter.error(
									i18n.trans('m.project.library.app.error_loading_data|ucf'),
									ErrorFormatter.format(data),
									'EDITOR'
								);
								scope.importManager.importing = false;
							}
						);
					}
					else {
						console.error('Missing type for projectLibraryImportForm');
					}
				}
			};
		}
	]);

	app.directive('projectLibraryLanguageFunctionSelector', ['$timeout', function($timeout) {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/html/display-directives/language-function-selector.html',
			scope: {
				selection: '=projectLibraryLanguageFunctionSelector',
				ready: '='
			},
			link: function(scope) {
				scope.languageToSelect = null;
				scope.collections = {};
				scope.selector = {
					languages: [],
					toAdd: null
				};

				scope.addLanguage = function() {
					if (!scope.selector.toAdd) {
						return;
					}

					var alreadySelected = false;
					angular.forEach(scope.selector.languages, function(item) {
						if (item.value === scope.selector.toAdd.value) {
							alreadySelected = true;
						}
					});

					if (!alreadySelected) {
						scope.selector.languages.push(scope.selector.toAdd);
					}
				};

				scope.$watch('collections.languages', function initSelector() {
					if (!scope.ready() || !angular.isObject(scope.selection)) {
						$timeout(initSelector, 100);
						return;
					}
					scope.selector.languages = [];
					angular.forEach(scope.collections.languages, function(item) {
						if (item.value === 'fr' || item.value === 'en' || item.value === 'ja' || scope.selection[item.value]) {
							scope.selector.languages.push(item);
						}
					});
				});
			}
		};
	}]);
})();