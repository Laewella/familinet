/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');
	var data_save = null;
	var recent_add = [];
	var loading = false;

	app.directive('projectLibraryWorkNew', [
		'RbsChange.REST', 'RbsChange.Settings', '$location', 'RbsChange.ErrorFormatter', 'RbsChange.i18n', '$filter',
		'RbsChange.NotificationCenter', '$anchorScroll', 'RbsChange.ArrayUtils', 'projectLibraryImport',
		function(REST, Settings, $location, ErrorFormatter, i18n, $filter, NotificationCenter, $anchorScroll, ArrayUtils, projectLibraryImport) {
			return {
				restrict: 'A',
				require: '^appDocumentEditor',
				scope: false,
				priority: 900,

				link: function(scope, iElement, iAttrs, ctrl) {
					prepareEditor(REST.newResource(ctrl.getDocumentModelName(), Settings.get('LCID')));

					REST.call(REST.getBaseUrl('projectLibrary/getAvailableWebsites'), {type: 'work'}).then(
						function(data) {
							scope.importManager.websites = data.websites;
						},
						function(data) {
							NotificationCenter.error(
								i18n.trans('m.project.library.app.error_loading_data|ucf'),
								ErrorFormatter.format(data),
								'EDITOR'
							);
							scope.importManager.importing = false;
						}
					);

					function prepareEditor(doc) {
						ctrl.prepareCreation(doc);
						// set the location.hash to the id of the element you wish to scroll to.
						$location.hash('top');
						$anchorScroll();
					}

					scope.persist = { languages: [], contributions: [] };
					scope.publication = REST.newResource('Project_Library_Publication', Settings.get('LCID'));
					scope.publication.description = { e: 'Markdown', t: '', h: null };
					scope.publication.languagesSelection = {};
					scope.possession = REST.newResource('Project_Library_Possession', Settings.get('LCID'));
					scope.contributions = [];
					scope.volumes = [];
					scope.description = { possession: '' };
					scope.configuration = {
						createPossession: true, createPublication: true, createVolumes: false, allVolumesOwned: false
					};

					scope.$watch('possession.complete', completePossessionDescription);
					scope.$watch('publication.volumesDetail', completePossessionDescription);
					scope.$watchCollection('volumes', completePossessionDescription);

					scope.$watch('configuration.allVolumesOwned', function() {
						angular.forEach(scope.volumes, function(attrValue) {
							attrValue.owned = scope.configuration.allVolumesOwned;
						});
					});

					scope.$watch('document.label', function() {
						if (scope.document.label && scope.document.label.length >= 3) {
							if (!loading) {
								loading = true;
								setTimeout(loadWorks, 500);
							}
						}
						else {
							scope.works = [];
						}
					});

					scope.generateVolumes = function() {
						scope.configuration.createVolumes = true;
						scope.volumes = [];
						if (scope.publication.volumesDetail) {
							angular.forEach(scope.publication.volumesDetail.split('/'), function(attrValue) {
								var volume;
								var values = attrValue.trim().split('-');
								if (values.length === 1) {
									volume = REST.newResource('Project_Library_PublicationVolume', Settings.get('LCID'));
									volume.number = values[0];
									volume.label = values[0];
									scope.volumes.push(volume);
								}
								else {
									var debut = parseInt(values[0]);
									var fin = parseInt(values[1]);
									if (Number.isInteger(debut) && Number.isInteger(fin) && debut < fin) {
										for (var i = debut; i <= fin; i++) {
											volume = REST.newResource('Project_Library_PublicationVolume',
												Settings.get('LCID'));
											volume.number = i;
											volume.label = 'Tome ' + i;
											scope.volumes.push(volume);
										}
									}
								}
							});
						}
					};

					scope.addContribution = function() {
						scope.contributions.push(REST.newResource('Project_Library_Contribution', Settings.get('LCID')));
					};

					scope.removeContribution = function(index) {
						scope.contributions.splice(index, 1);
						scope.persist.contributions.splice(index, 1);
					};

					scope.addVolume = function() {
						scope.volumes.push(REST.newResource('Project_Library_PublicationVolume', Settings.get('LCID')));
					};

					scope.removeVolume = function(index) {
						scope.volumes.splice(index, 1);
					};

					scope.moveUpLanguage = function(index) {
						ArrayUtils.move(scope.persist.languages, index, index - 1);
					};

					scope.moveDownLanguage = function(index) {
						ArrayUtils.move(scope.persist.languages, index, index + 1);
					};

					scope.deleteLanguage = function(index) {
						scope.persist.languages.splice(index, 1);
					};

					function loadWorks() {
						loading = false;
						REST.collection('Project_Library_Work',  {
							offset: 0,
							limit: 10,
							sort: 'label',
							desc: false,
							column: null,
							filter: {
								name: 'group',
								parameters: { all: 0, configured: 0 },
								operator: 'AND',
								filters: [ {
									name: 'label',
									parameters: {
										propertyName: 'label',
										operator: 'contains',
										value: scope.document.label
									}
								} ]
							}
						}).then(function(data) {
							scope.works = data;
						});
					}

					function completePossessionDescription() {
						if (scope.possession.complete) {
							if (scope.configuration.createVolumes) {
								angular.forEach(scope.volumes, function(attrValue) {
									attrValue.owned = true;
								});
							}
							else {
								scope.description.possession = scope.publication.volumesDetail;
							}
						}
					}

					scope.onLoad = function() {
						if (data_save) {
							// Restauration des coches de persistance
							scope.persist = data_save.persist;

							// Restauration des données persistées
							angular.forEach(scope.persist, function(value, key) {
								if (key === 'contributions') {
									var persistContributions = scope.persist.contributions;
									scope.persist.contributions = [];
									angular.forEach(persistContributions, function(attrValue, attrKey) {
										if (attrValue) {
											scope.contributions.push(data_save.contributions[attrKey]);
											scope.persist.contributions.push(true);
										}
									});
								}
								else if (key === 'languages') {
									var persistLanguages = scope.persist.languages;
									scope.persist.languages = [];
									angular.forEach(persistLanguages, function(attrValue, attrKey) {
										if (attrValue) {
											scope.publication.languages.push(data_save.publication.languages[attrKey]);
											scope.persist.languages.push(true);
										}
									});
								}
								else {
									if (scope.persist.hasOwnProperty(key) && data_save.hasOwnProperty(key)) {
										angular.forEach(scope.persist[key], function(subValue, subKey) {
											if (!subValue || !scope.persist[key].hasOwnProperty(subKey)) {
												return;
											}

											if (subKey === 'typology$') {
												scope[key]['typology$'] = {};
												angular.forEach(subValue, function(attrValue, attrKey) {
													if (attrValue && subValue.hasOwnProperty(attrKey)) {
														scope[key]['typology$'][attrKey] = data_save[key]['typology$'][attrKey];
													}
												});
											}
											else {
												scope[key][subKey] = data_save[key][subKey];
											}
										});
									}
								}
							});

							// Si une valeur de l'édition est retenue, on coche l'édition
							angular.forEach(scope.persist.publication, function(value) {
								if (value) {
									scope.configuration.createPublication = true;
								}
							});

							// Réinitialisation de la variable de stockage
							data_save = null;
						}

						if (recent_add) {
							scope.recent_add = recent_add;
						}
					};

					scope.preSave = function(doc) {
						data_save = {};
						data_save.persist = scope.persist;
						data_save.document = doc;
						data_save.publication = scope.publication;
						data_save.possession = scope.possession;
						data_save.contributions = scope.contributions;
					};

					scope.postSave = function(doc, newDoc) {
						// Save other documents.
						var _chgFieldsInfo = angular.copy(scope._chgFieldsInfo);
						var publication = angular.copy(scope.publication);
						var possession = angular.copy(scope.possession);
						var contributions = angular.copy(scope.contributions);
						var volumes = angular.copy(scope.volumes);
						var description = angular.copy(scope.description);
						var configuration = angular.copy(scope.configuration);

						var savedWork = newDoc;
						var savedPublication = null;
						var savedVolumes = [];
						var indexVolume = 0;

						function savePublication() {
							publication.work = savedWork;
							return REST.save(publication, []).then(savePublicationVolumes, saveErrorHandler)
						}

						function savePublicationVolumes(doc) {
							savedPublication = doc;
							if (configuration.createVolumes) {
								savePublicationVolume(null);
							}
							else if (configuration.createPossession) {
								savePossession();
							}
						}

						function savePublicationVolume(doc) {
							if (doc !== null) {
								savedVolumes.push(doc);
								indexVolume++;
							}
							if (volumes.length > indexVolume) {
								volumes[indexVolume].publication = savedPublication;
								REST.save(volumes[indexVolume], []).then(savePublicationVolume, saveErrorHandler);
							}
							else if (configuration.createPossession) {
								savePossession();
							}
						}

						function savePossession() {
							possession.work = savedWork;
							possession.details = [];
							possession.details.push({ model: 'Project_Library_PossessionDetail' });

							if (configuration.createPublication) {
								possession.details[0].publication = savedPublication;

								if (configuration.createVolumes) {
									possession.details[0].volumes = [];
									angular.forEach(savedVolumes, function(volume, index) {
										if (volumes[index].owned === true) {
											possession.details[0].volumes.push(volume);
										}
									});
								}
								else {
									possession.details[0].description = description.possession;
								}
							}
							else {
								possession.details[0].description = description.possession;
							}

							return REST.save(possession, []).then(saveSuccessHandler, saveErrorHandler)
						}

						function saveSuccessHandler(doc) {
						}

						function saveErrorHandler(reason) {
							NotificationCenter.error(
								i18n.trans('m.rbs.admin.admin.save_error'),
								ErrorFormatter.format(reason),
								'EDITOR',
								{ $propertyInfoProvider: _chgFieldsInfo }
							);
						}

						angular.forEach(contributions, function(contribution) {
							contribution.work = savedWork;
							REST.save(contribution, []).then(saveSuccessHandler, saveErrorHandler);
						});

						var promise;
						if (configuration.createPublication) {
							promise = savePublication();
						}
						else if (configuration.createPossession) {
							promise = savePossession();
						}
						return promise;
					};

					// Function called when a creation is done.
					scope.terminateSave = function(doc) {
						// Ajout du document à l'historique
						recent_add.push(doc);
						// Suppression des trop anciens documents
						if (recent_add.length > 5) {
							recent_add.splice(0, 1);
						}
						$location.path('Library/Work/newDone');
					};

					function applyImportedData(data) {
						if (angular.isObject(data.work)) {
							projectLibraryImport.applyImportedData(scope.document, data.work, {
									scalar: ['label', 'originalLanguage', 'year', 'endYear', 'audience'],
									richText: ['description'],
									subDoc: [
										{ name: 'status', check: 'id', single: true },
										{ name: 'genres', check: 'id' },
										{ name: 'nationalities', check: 'id' },
										{ name: 'aliases', check: 'label' },
										{ name: 'links', check: 'url' }
									]
								}
							);
						}

						if (angular.isObject(data.publication)) {
							scope.configuration.createPublication = true;

							projectLibraryImport.applyImportedData(scope.publication, data.publication, {
									scalar: ['label', 'year', 'endYear'],
									richText: ['description'],
									subDoc: [
										{ name: 'status', check: 'id', single: true },
										{ name: 'publisher', check: 'id', single: true },
										{ name: 'collection', check: 'id', single: true },
										{ name: 'nationalities', check: 'id' }
									]
								}
							);
						}

						if (angular.isObject(data.contributions)) {
							// TODO
						}
					}

					scope.importManager = {
						url: null,
						importing: false,
						import: function() {
							scope.importManager.importing = true;
							var params = { url: scope.importManager.url, importType: 'work' };
							REST.call(REST.getBaseUrl('projectLibrary/parseExternalPage'), params).then(
								function(data) {
									applyImportedData(data);
									scope.importManager.importing = false;
								},
								function(data) {
									NotificationCenter.error(
										i18n.trans('m.project.library.app.error_loading_data|ucf'),
										ErrorFormatter.format(data),
										'EDITOR'
									);
									scope.importManager.importing = false;
								}
							);
						}
					}
				}
			};
		}
	]);
})();