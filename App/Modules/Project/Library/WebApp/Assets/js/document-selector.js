/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.run(['$templateCache', function($templateCache) {
		$templateCache.put(
			'picker-item-Project_Library_Contributor.html',
			'(= item.label =)' +
			' <small data-ng-if="item.foundAlias">[ (= item.foundAlias =) ]</small>' +
			' <span data-ng-repeat="nationality in item.nationalitiesData" data-project-library-nationality="nationality" data-mode="compact"></span>' +
			' <small class="text-muted" data-ng-if="item.birthYear || item.deathYear">((= item.birthYear =) - (= item.deathYear =))</small>'
		);
		$templateCache.put(
			'picker-item-Project_Library_Work.html',
			'(= item.label =)' +
			' <small data-ng-if="item.foundAlias">[ (= item.foundAlias =) ]</small>' +
			' <small class="text-muted" data-ng-if="item.workType">(= item.workType =)</small>' +
			' <span data-ng-repeat="nationality in item.nationalitiesData" data-project-library-nationality="nationality" data-mode="compact"></span>' +
			' <small class="text-muted" data-ng-if="item.year || item.endYear">(<span data-ng-show="item.year">(= item.year =)</span><span data-ng-show="item.year && item.endYear && item.year !== item.endYear"> - </span><span data-ng-show="item.endYear && item.year !== item.endYear">(= item.endYear =)</span>)</span>'
		);
		$templateCache.put(
			'picker-item-Project_Library_Publisher.html',
			'(= item.label =)' +
			' <small data-ng-if="item.foundAlias">[ (= item.foundAlias =) ]</small>' +
			' <span data-ng-repeat="nationality in item.nationalitiesData" data-project-library-nationality="nationality" data-mode="compact"></span>'
		);
		$templateCache.put(
			'picker-item-Project_Library_Volume.html',
			'<span data-ng-show="item.number" class="text-muted">(= item.number =)</span> (= item.label =)'
		);
	}]);
})();