(function() {
	'use strict';

	var app = angular.module('RbsChange');
	var moduleName = 'Project_Library';

	__change.routes['/Library/'] = {
		name: 'home',
		module: 'Project_Library',
		rule: {
			labelKey: 'm.project.library.app.library | ucf',
			icon: 'book',
			templateUrl: moduleName + '/html/home.html',
			controller: 'EmptyController'
		}
	};

	// USER

	__change.routes['/Library/User/'] = {
		name: 'list',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.app.user_list | ucf',
			templateUrl: moduleName + '/html/user-list.html',
			controller: 'LibraryUserListController'
		}
	};

	__change.routes['/Library/User/:id'] = {
		name: 'detail',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.app.user | ucf',
			templateUrl: moduleName + '/html/user-detail.html',
			controller: 'LibraryUserDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/User/:id/Possession/'] = {
		name: 'possessions',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.app.possession_list | ucf',
			templateUrl: moduleName + '/html/user-possession-list.html',
			controller: 'LibraryUserDetailController'
		}
	};

	__change.routes['/Library/User/:id/Reading/'] = {
		name: 'readings',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.app.reading | ucf',
			templateUrl: moduleName + '/html/user-reading-list.html',
			controller: 'LibraryUserDetailController'
		}
	};

	app.controller('LibraryUserListController', ['$scope', LibraryUserListController]);

	function LibraryUserListController(scope) {
		scope.staticFilters = [
			{
				name: 'groups.realm',
				parameters: { propertyName: 'groups.realm', value: 'familinet', operator: 'eq' }
			}
		];
	}

	app.controller('LibraryUserDetailController', ['$rootScope', '$scope', 'RbsChange.Document', '$routeParams', LibraryUserDetailController]);

	function LibraryUserDetailController($rootScope, scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Rbs_User_User');

		scope.possessionStaticFilters = [
			{
				name: 'owner',
				parameters: { propertyName: 'owners', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.readingStaticFilters = [
			{
				name: 'reading',
				parameters: { propertyName: 'reader', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.newSubDocumentUrlParameters = { userId: $routeParams.id };

		scope.isCurrent = $rootScope.user.id === $routeParams.id;
	}

	// WORK

	__change.routes['/Library/Work/'] = {
		name: 'list',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.app.work_list | ucf',
			templateUrl: moduleName + '/html/work-list.html',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Library/Work/new'] = {
		name: 'new',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			templateUrl: moduleName + '/html/work-new.html',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Library/Work/newDone'] = {
		name: 'newDone',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			templateUrl: moduleName + '/html/work-new-done.html',
			controller: 'LibraryWorkNewDoneController'
		}
	};

	__change.routes['/Library/Work/:id'] = {
		name: 'detail',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.app.work | ucf',
			templateUrl: moduleName + '/html/work-detail.html',
			controller: 'LibraryWorkDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/work-edit.html',
			controller: 'LibraryWorkEditController'
		}
	};

	__change.routes['/Library/Work/:id/Publication/'] = {
		name: 'publications',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.app.publication_list | ucf',
			templateUrl: moduleName + '/html/work-publication-list.html',
			controller: 'LibraryWorkDetailController'
		}
	};

	__change.routes['/Library/Work/:id/Possession/'] = {
		name: 'possessions',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.app.possession_list | ucf',
			templateUrl: moduleName + '/html/work-possession-list.html',
			controller: 'LibraryWorkDetailController'
		}
	};

	__change.routes['/Library/Work/:id/Reading/'] = {
		name: 'readings',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.library.app.reading_list | ucf',
			templateUrl: moduleName + '/html/work-reading-list.html',
			controller: 'LibraryWorkDetailController'
		}
	};

	app.controller('LibraryWorkDetailController', ['$scope', 'RbsChange.Document', '$routeParams', LibraryWorkDetailController]);

	function LibraryWorkDetailController(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_Work');

		scope.publicationStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'work', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.possessionStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'work', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.readingStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'work', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.newSubDocumentUrlParameters = { workId: $routeParams.id };
	}

	app.controller('LibraryWorkEditController', ['$scope', 'RbsChange.Document', LibraryWorkEditController]);

	function LibraryWorkEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Work');
	}

	app.controller('LibraryWorkNewDoneController', ['$location', LibraryWorkNewDoneController]);

	function LibraryWorkNewDoneController($location) {
		$location.path('Library/Work/new');
	}

	// WORK GROUP

	__change.routes['/Library/WorkGroup/'] = {
		name: 'list',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.library.app.work_group_list | ucf',
			templateUrl: moduleName + '/html/work-group-list.html',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/WorkGroup/new'] = {
		name: 'new',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			templateUrl: moduleName + '/html/work-group-new.html',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/WorkGroup/:id'] = {
		name: 'detail',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.library.app.work_group | ucf',
			templateUrl: moduleName + '/html/work-group-detail.html',
			controller: 'LibraryWorkGroupDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/WorkGroup/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/work-group-edit.html',
			controller: 'LibraryWorkGroupEditController'
		}
	};

	app.controller('LibraryWorkGroupDetailController', ['$scope', 'RbsChange.Document', 'RbsChange.i18n', 'RbsChange.ErrorFormatter',
		'RbsChange.NotificationCenter', 'RbsChange.REST', '$routeParams', LibraryWorkGroupDetailController]);

	function LibraryWorkGroupDetailController(scope, Document, i18n, ErrorFormatter, NotificationCenter, REST, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_WorkGroup');

		scope.workStaticFilters = [
			{
				name: 'workGroups',
				parameters: { propertyName: 'workGroups', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.works = {
			busy: false,
			list: [],
			add: function() {
				scope.works.busy = true;

				var params = {
					groupId: scope.document.id,
					workIds: []
				};
				angular.forEach(scope.works.list, function(work) {
					params.workIds.push(work.id);
				});

				REST.apiPut('projectLibrary/addWorksToGroup', params).then(
					function() {
						scope.$broadcast('AppReloadList');
						scope.works.list = [];
						scope.works.busy = false;
					},
					function(data) {
						NotificationCenter.error(
							i18n.trans('m.project.library.app.error_adding_works|ucf'),
							ErrorFormatter.format(data),
							'ADD_WORKS'
						);
						scope.works.busy = false;
					}
				);
			}
		};
	}

	app.controller('LibraryWorkGroupEditController', ['$scope', 'RbsChange.Document', LibraryWorkGroupEditController]);

	function LibraryWorkGroupEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_WorkGroup');
	}

	// CONTRIBUTOR

	__change.routes['/Library/Contributor/'] = {
		name: 'list',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.library.app.contributor_list | ucf',
			templateUrl: moduleName + '/html/contributor-list.html',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Contributor/new'] = {
		name: 'new',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/contributor-new.html',
			controller: 'LibraryContributorNewController'
		}
	};
	__change.routes['/Library/Contributor/:id'] = {
		name: 'detail',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.library.app.contributor | ucf',
			templateUrl: moduleName + '/html/contributor-detail.html',
			controller: 'LibraryContributorDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Contributor/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/contributor-edit.html',
			controller: 'LibraryContributorEditController'
		}
	};

	__change.routes['/Library/Contributor/:id/Contribution/'] = {
		name: 'contributions',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.library.app.contribution_list | ucf',
			templateUrl: moduleName + '/html/contributor-contribution-list.html',
			controller: 'LibraryContributorDetailController'
		}
	};

	app.controller('LibraryContributorDetailController', ['$scope', '$routeParams', 'RbsChange.Document', LibraryContributorDetailController]);

	function LibraryContributorDetailController(scope, $routeParams, Document) {
		Document.initDetailView(scope, 'Project_Library_Contributor');

		scope.contributionStaticFilters = [
			{
				name: 'contribution',
				parameters: { propertyName: 'contributor', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.newSubDocumentUrlParameters = { contributorId: $routeParams.id };
	}

	app.controller('LibraryContributorEditController', ['$scope', 'RbsChange.Document', 'projectLibraryImport', LibraryContributorEditController]);

	function LibraryContributorEditController(scope, Document, projectLibraryImport) {
		Document.initEditView(scope, 'Project_Library_Contributor');

		projectLibraryImport.initImportManager(scope, 'contributor', function(doc, data) {
			if (angular.isObject(data.contributor)) {
				projectLibraryImport.applyImportedData(doc, data.contributor, {
						scalar: ['label', 'birthDate', 'deathDate'],
						richText: ['description'],
						subDoc: [
							{ name: 'nationalities', check: 'id' },
							{ name: 'aliases', check: 'label' },
							{ name: 'links', check: 'url' }
						]
					}
				);
			}
		});
	}

	app.controller('LibraryContributorNewController', ['$scope', 'projectLibraryImport', LibraryContributorNewController]);

	function LibraryContributorNewController(scope, projectLibraryImport) {
		projectLibraryImport.initImportManager(scope, 'contributor', function(doc, data) {
			if (angular.isObject(data.contributor)) {
				projectLibraryImport.applyImportedData(doc, data.contributor, {
						scalar: ['label', 'birthDate', 'deathDate'],
						richText: ['description'],
						subDoc: [
							{ name: 'nationalities', check: 'id' },
							{ name: 'aliases', check: 'label' },
							{ name: 'links', check: 'url' }
						]
					}
				);
			}
		});
	}

// CONTRIBUTION

	__change.routes['/Library/Work/:workId/Contribution/'] = {
		name: 'list',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.library.app.contribution_list | ucf',
			templateUrl: moduleName + '/html/contribution-list.html',
			controller: 'LibraryContributionListController'
		}
	};
	__change.routes['/Library/Work/:workId/Contribution/new'] = {
		name: 'new',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/contribution-new.html',
			controller: 'LibraryContributionNewController'
		}
	};
	__change.routes['/Library/Work/:workId/Contribution/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/contribution-edit.html',
			controller: 'LibraryContributionEditController'
		}
	};
	__change.routes['/Library/Work/:workId/Contribution/:id'] = {
		name: 'detail',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.library.app.contribution | ucf',
			templateUrl: moduleName + '/html/contribution-detail.html',
			controller: 'LibraryContributionDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Contributor/:contributorId/Contribution/new'] = {
		name: 'newForContributor',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/contribution-new.html',
			controller: 'LibraryContributionNewForContributorController'
		}
	};
	__change.routes['/Library/Contributor/:contributorId/Contribution/:id'] = {
		name: 'detailForContributor',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.library.app.contribution | ucf',
			templateUrl: moduleName + '/html/contribution-detail.html',
			controller: 'LibraryContributionDetailController',
			labelId: 'id'
		}
	};

	app.controller('LibraryContributionListController', ['$scope', '$routeParams', LibraryContributionListController]);

	function LibraryContributionListController(scope, $routeParams) {
		scope.contributionStaticFilters = [
			{
				name: 'contribution',
				parameters: { propertyName: 'work', value: $routeParams.workId, operator: 'eq' }
			}
		];
		scope.newSubDocumentUrlParameters = { workId: $routeParams.workId };
	}

	app.controller('LibraryContributionDetailController', ['$scope', 'RbsChange.Document', LibraryContributionDetailController]);

	function LibraryContributionDetailController(scope, Document) {
		Document.initDetailView(scope, 'Project_Library_Contribution');
	}

	app.controller('LibraryContributionNewController', ['$scope', '$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryContributionNewController]);

	function LibraryContributionNewController(scope, $routeParams, $rootScope, Events, REST, Breadcrumb, i18n, ErrorFormatter, NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			REST.resource('Project_Library_Work', $routeParams.workId).then(
				function(doc) {
					parameters.document.work = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.workId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});

		scope.calculateEpisodeCount = calculateEpisodeCount;
		scope.evaluateDescription = evaluateDescription;
	}

	app.controller('LibraryContributionNewForContributorController', ['$scope', '$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter',
		LibraryContributionNewForContributorController]);

	function LibraryContributionNewForContributorController(scope, $routeParams, $rootScope, Events, REST, Breadcrumb, i18n,
		ErrorFormatter, NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			REST.resource('Project_Library_Contributor', $routeParams.contributorId).then(
				function(doc) {
					parameters.document.contributor = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.contributorId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});

		scope.calculateEpisodeCount = calculateEpisodeCount;
		scope.evaluateDescription = evaluateDescription;
	}

	app.controller('LibraryContributionEditController', ['$scope', 'RbsChange.Document', LibraryContributionEditController]);

	function LibraryContributionEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Contribution');

		scope.calculateEpisodeCount = calculateEpisodeCount;
		scope.evaluateDescription = evaluateDescription;
	}

	function calculateEpisodeCount(document) {
		var count = 0;
		if (angular.isString(document.description.t)) {
			var lines = document.description.t.split(/\r\n|\r|\n/);
			angular.forEach(lines, function(line) {
				if (/^\s*[\-*]/.test(line)) {
					count++;
				}
			});
		}
		document.episodeCount = count;
	}

	function evaluateDescription(document, source) {
		if (angular.isString(document.description.t)) {
			if (source === 'IMDB') {
				var count = 0;
				var beginYear = 9999;
				var endYear = 0;
				var detail = [];

				var lines = document.description.t.split(/\r\n|\r|\n/);
				angular.forEach(lines, function(line) {
					if (/^\s*[\-*]/.test(line)) {
						var matches = /\((\d{4})\) ... (.*)/.exec(line);
						if (matches && matches.length === 3) {
							var toAdd = matches[2];

							var suffixMatches = /(.*) \((.*)\)$/.exec(toAdd);
							if (suffixMatches) {
								var suffix = suffixMatches[2];
								if (suffix === 'credit only') {
									return;
								}
								if (suffix === 'uncredited' || suffix.startsWith('as ')) {
									toAdd = suffixMatches[1];
								}
							}

							var year = parseInt(matches[1], 10);
							if (year < beginYear) {
								beginYear = year;
							}
							if (year > endYear) {
								endYear = year;
							}

							for (var i = 0; i < detail.length; i++) {
								if (toAdd === detail[i]) {
									toAdd = null;
									break;
								}
							}
							if (toAdd) {
								detail.push(toAdd);
							}
						}
						count++;
					}
				});

				document.episodeCount = count;
				if (beginYear !== 9999) {
					document.contributionYear = beginYear;
				}
				if (endYear !== 0) {
					document.contributionEndYear = endYear;
				}
				if (detail.length) {
					document.detail = detail.join(' / ');
				}
			}
			else {
				console.error('Unknown source', source);
			}
		}
	}

// PUBLISHER

	__change.routes['/Library/Publisher/'] = {
		name: 'list',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.app.publisher_list | ucf',
			templateUrl: moduleName + '/html/publisher-list.html',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Publisher/new'] = {
		name: 'new',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/publisher-new.html',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Publisher/:id'] = {
		name: 'detail',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.app.publisher | ucf',
			templateUrl: moduleName + '/html/publisher-detail.html',
			controller: 'LibraryPublisherDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Publisher/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/publisher-edit.html',
			controller: 'LibraryPublisherEditController'
		}
	};

	__change.routes['/Library/Publisher/:id/Publication/'] = {
		name: 'publications',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.app.publication_list | ucf',
			templateUrl: moduleName + '/html/publisher-publication-list.html',
			controller: 'LibraryPublisherDetailController'
		}
	};

	__change.routes['/Library/Publisher/:id/Collection/'] = {
		name: 'collections',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.app.collection_list | ucf',
			templateUrl: moduleName + '/html/publisher-collection-list.html',
			controller: 'LibraryPublisherDetailController'
		}
	};

	LibraryPublisherDetailController.$inject = ['$scope', 'RbsChange.Document', '$routeParams'];
	app.controller('LibraryPublisherDetailController', LibraryPublisherDetailController);

	function LibraryPublisherDetailController(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_Publisher');

		scope.publicationStaticFilters = [
			{
				name: 'publisher',
				parameters: { propertyName: 'publisher', value: $routeParams.id, operator: 'eq' }
			}
		];
		scope.collectionStaticFilters = [
			{
				name: 'publisher',
				parameters: { propertyName: 'publisher', value: $routeParams.id, operator: 'eq' }
			}
		];

		// Paramètre pour l'url pour les documents enfants
		scope.newSubDocumentUrlParameters = { publisherId: $routeParams.id };
	}

	LibraryPublisherEditController.$inject = ['$scope', 'RbsChange.Document'];
	app.controller('LibraryPublisherEditController', LibraryPublisherEditController);

	function LibraryPublisherEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Publisher');
	}

// COLLECTION

	__change.routes['/Library/Publisher/:publisherId/Collection/new'] = {
		name: 'new',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/collection-new.html',
			controller: 'LibraryCollectionNewController'
		}
	};
	__change.routes['/Library/Publisher/:publisherId/Collection/:id'] = {
		name: 'detail',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.library.app.collection | ucf',
			templateUrl: moduleName + '/html/collection-detail.html',
			controller: 'LibraryCollectionDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Publisher/:publisherId/Collection/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/collection-edit.html',
			controller: 'LibraryCollectionEditController'
		}
	};

	app.controller('LibraryCollectionDetailController', ['$scope', 'RbsChange.Document', '$routeParams', LibraryCollectionDetailController]);

	function LibraryCollectionDetailController(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_Collection');

		scope.publicationStaticFilters = [
			{
				name: 'collection',
				parameters: { propertyName: 'collection', value: $routeParams.id, operator: 'eq' }
			}
		];
	}

	app.controller('LibraryCollectionNewController', ['$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryCollectionNewController]);

	function LibraryCollectionNewController($routeParams, $rootScope, Events, REST, Breadcrumb, i18n, ErrorFormatter,
		NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			REST.resource('Project_Library_Publisher', $routeParams.publisherId).then(
				function(doc) {
					parameters.document.publisher = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.publisherId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});
	}

	app.controller('LibraryCollectionEditController', ['$scope', 'RbsChange.Document', LibraryCollectionEditController]);

	function LibraryCollectionEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Collection');
	}

// PUBLICATION

	__change.routes['/Library/Work/:workId/Publication/'] = {
		name: 'list',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.app.publication_list | ucf',
			redirectTo: '/Library/Work/:workId'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/new'] = {
		name: 'new',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/publication-new.html',
			controller: 'LibraryPublicationNewController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id'] = {
		name: 'detail',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.app.publication | ucf',
			templateUrl: moduleName + '/html/publication-detail.html',
			controller: 'LibraryPublicationDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/publication-edit.html',
			controller: 'LibraryPublicationEditController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/Reading'] = {
		name: 'readings',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.app.reading_list | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/work-reading-list.html',
			controller: 'LibraryPublicationDetailController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/Possession'] = {
		name: 'possessions',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.app.possession_list | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/work-possession-list.html',
			controller: 'LibraryPublicationDetailController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/Volume/'] = {
		name: 'volumes',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.app.publication_volume_list | ucf',
			templateUrl: moduleName + '/html/publication-volume-list.html',
			controller: 'LibraryPublicationDetailController'
		}
	};

	app.controller('LibraryPublicationNewController', ['$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryPublicationNewController]);

	function LibraryPublicationNewController($routeParams, $rootScope, Events, REST, Breadcrumb, i18n, ErrorFormatter,
		NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			if (!angular.isObject(parameters.document.languagesSelection)) {
				parameters.document.languagesSelection = {};
			}

			REST.resource('Project_Library_Work', $routeParams.workId).then(
				function(doc) {
					parameters.document.work = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.workId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});
	}

	app.controller('LibraryPublicationDetailController', ['$scope', 'RbsChange.Document', '$routeParams', LibraryPublicationDetailController]);

	function LibraryPublicationDetailController(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_Publication');

		scope.possessionStaticFilters = [
			{
				name: 'publication',
				parameters: { propertyName: 'publications', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.publicationVolumeStaticFilters = [
			{
				name: 'volume',
				parameters: { propertyName: 'publication', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.readingStaticFilters = [
			{
				name: 'reading',
				parameters: { propertyName: 'publication', value: $routeParams.id, operator: 'eq' }
			}
		];

		// Paramètre pour l'url du pret
		scope.newSubDocumentUrlParameters = { workId: $routeParams.workId, publicationId: $routeParams.id };
	}

	app.controller('LibraryPublicationEditController', ['$scope', 'RbsChange.Document', LibraryPublicationEditController]);

	function LibraryPublicationEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Publication');
	}

// PUBLICATION VOLUME

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/'] = {
		name: 'list',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.library.app.publication_volume_list | ucf',
			redirectTo: '/Library/Work/:workId/Publication/:publicationId/Volume/'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/new'] = {
		name: 'new',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/publication-volume-new.html',
			controller: 'LibraryPublicationVolumeNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/:id'] = {
		name: 'detail',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.library.app.publication_volume | ucf',
			templateUrl: moduleName + '/html/publication-volume-detail.html',
			controller: 'LibraryPublicationVolumeDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/publication-volume-edit.html',
			controller: 'LibraryPublicationVolumeEditController'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/:id/Reading'] = {
		name: 'readings',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.library.app.reading_list | ucf',
			templateUrl: moduleName + '/html/work-reading-list.html',
			controller: 'LibraryPublicationVolumeDetailController'
		}
	};

	app.controller('LibraryPublicationVolumeNewController', ['$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryPublicationVolumeNewController]);

	function LibraryPublicationVolumeNewController($routeParams, $rootScope, Events, REST, Breadcrumb, i18n, ErrorFormatter,
		NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			REST.resource('Project_Library_Publication', $routeParams.publicationId).then(
				function(doc) {
					parameters.document.publication = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.workId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});
	}

	app.controller('LibraryPublicationVolumeDetailController', ['$scope', 'RbsChange.Document', '$routeParams',
		LibraryPublicationVolumeDetailController]);

	function LibraryPublicationVolumeDetailController(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_PublicationVolume');

		scope.readingStaticFilters = [
			{
				name: 'reading',
				parameters: { propertyName: 'volumes', value: $routeParams.id, operator: 'eq' }
			}
		];
	}

	app.controller('LibraryPublicationVolumeEditController', ['$scope', 'RbsChange.Document', LibraryPublicationVolumeEditController]);

	function LibraryPublicationVolumeEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_PublicationVolume');
	}

// POSSESSION

	__change.routes['/Library/Possession/'] = {
		name: 'list',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.library.app.possession_list | ucf',
			templateUrl: moduleName + '/html/possession-list.html',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/new'] = {
		name: 'new',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/possession-new.html',
			controller: 'LibraryPossessionNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/Possession/new'] = {
		name: 'newOnPossession',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/possession-new.html',
			controller: 'LibraryPossessionNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:id'] = {
		name: 'detail',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.library.app.possession | ucf',
			templateUrl: moduleName + '/html/possession-detail.html',
			controller: 'LibraryPossessionDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/possession-edit.html',
			controller: 'LibraryPossessionEditController'
		}
	};

	app.controller('LibraryPossessionNewController', ['$scope', '$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryPossessionNewController]);

	function LibraryPossessionNewController(scope, $routeParams, $rootScope, Events, REST, Breadcrumb, i18n,
		ErrorFormatter, NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			if ($routeParams.publicationId) {
				REST.resource('Project_Library_Publication', $routeParams.publicationId).then(
					function(doc) {
						parameters.document.work = doc.work;
						parameters.document.publication = doc;
					},
					function(reason) {
						NotificationCenter.error([
							i18n.trans('m.project.familinet.app.load_error'),
							ErrorFormatter.format(reason),
							'LOAD_DOCUMENT'
						]);
						console.warn('Can\'t load the document with id ' + $routeParams.workId +
							', so redirect to list.\nError message: ' + reason.message);
						Breadcrumb.goParent();
					}
				);
			}
			else {
				REST.resource('Project_Library_Work', $routeParams.workId).then(
					function(doc) {
						parameters.document.work = doc;
					},
					function(reason) {
						NotificationCenter.error([
							i18n.trans('m.project.familinet.app.load_error'),
							ErrorFormatter.format(reason),
							'LOAD_DOCUMENT'
						]);
						console.warn('Can\'t load the document with id ' + $routeParams.workId +
							', so redirect to list.\nError message: ' + reason.message);
						Breadcrumb.goParent();
					}
				);
			}
		});

		// Pour l'affichage des listes de sélection des détails
		scope.publicationStaticFilters = [
			{
				name: 'publication',
				parameters: { propertyName: 'work', value: $routeParams.workId, operator: 'eq' }
			}
		];
	}

	app.controller('LibraryPossessionDetailController', ['$scope', 'RbsChange.Document', '$routeParams', LibraryPossessionDetailController]);

	function LibraryPossessionDetailController(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_Possession');

		// Pour l'affichage des	prêts en bas du détail
		scope.loanStaticFilters = [
			{
				name: 'possession',
				parameters: { propertyName: 'possession', value: $routeParams.id, operator: 'eq' }
			}
		];

		// Paramètre pour l'url du pret
		scope.newSubDocumentUrlParameters = { workId: $routeParams.workId, possessionId: $routeParams.id };
	}

	app.controller('LibraryPossessionEditController', ['$scope', 'RbsChange.Document', '$routeParams', LibraryPossessionEditController]);

	function LibraryPossessionEditController(scope, Document, $routeParams) {
		Document.initEditView(scope, 'Project_Library_Possession');

		// Pour l'affichage des listes de sélection des détails
		scope.publicationStaticFilters = [
			{
				name: 'publication',
				parameters: { propertyName: 'work', value: $routeParams.workId, operator: 'eq' }
			}
		];
	}

	app.directive('projectLibraryPossessionDetailEdit', function() {
		return {
			restrict: 'A',
			scope: true,

			link: function(scope) {
				scope.volumesStaticFilters = [
					{
						name: 'volumes',
						parameters: { propertyName: 'publication', value: 0, operator: 'eq' }
					}
				];
				scope.$watch('detail.publication', function(newValue) {
					if (newValue) {
						scope.volumesStaticFilters[0].parameters.value = newValue.id;
					}
					else {
						scope.volumesStaticFilters[0].parameters.value = 0;
					}
				});
			}
		};
	});

// LOAN

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/'] = {
		name: 'list',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.library.app.loan_list | ucf',
			redirectTo: '/Library/Work/:workId/Possession/:possessionId'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/new'] = {
		name: 'new',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/loan-new.html',
			controller: 'LibraryLoanNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/:id'] = {
		name: 'detail',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.library.app.loan | ucf',
			templateUrl: moduleName + '/html/loan-detail.html',
			controller: 'LibraryLoanDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/loan-edit.html',
			controller: 'LibraryLoanEditController'
		}
	};

	app.controller('LibraryLoanNewController', ['$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryLoanNewController]);

	function LibraryLoanNewController($routeParams, $rootScope, Events, REST, Breadcrumb, i18n, ErrorFormatter,
		NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			REST.resource('Project_Library_Possession', $routeParams.possessionId).then(
				function(doc) {
					parameters.document.possession = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.workId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
			REST.resource('Project_Library_Work', $routeParams.workId).then(
				function(doc) {
					parameters.document.work = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.workId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});
	}

	app.controller('LibraryLoanDetailController', ['$scope', 'RbsChange.Document', 'RbsChange.REST', LibraryLoanDetailController]);

	function LibraryLoanDetailController(scope, Document, REST) {
		Document.initDetailView(scope, 'Project_Library_Loan').then(function(doc) {
			scope.document = doc;

			REST.resource('Project_Library_Possession', doc.possession.id).then(function(possession) {
				scope.possession = possession;
			});
		});
	}

	app.controller('LibraryLoanEditController', ['$scope', 'RbsChange.Document', LibraryLoanEditController]);

	function LibraryLoanEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Loan');
	}

// READING

	__change.routes['/Library/Reading/'] = {
		name: 'list',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.library.app.reading | ucf',
			templateUrl: moduleName + '/html/reading-list.html',
			controller: 'LibraryReadingListController'
		}
	};

	__change.routes['/Library/User/:userId/Reading/new'] = {
		name: 'new',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.familinet.app.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/html/reading-new.html',
			controller: 'LibraryReadingNewController'
		}
	};

	__change.routes['/Library/User/:userId/Reading/:id'] = {
		name: 'detail',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.library.app.reading | ucf',
			templateUrl: moduleName + '/html/reading-detail.html',
			controller: 'LibraryReadingDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/User/:userId/Reading/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.familinet.app.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/html/reading-edit.html',
			controller: 'LibraryReadingEditController'
		}
	};

	app.controller('LibraryReadingListController', ['$rootScope', '$scope', LibraryReadingListController]);

	function LibraryReadingListController($rootScope, scope) {
		scope.newSubDocumentUrlParameters = { userId: $rootScope.user.id };
	}

	app.controller('LibraryReadingNewController', ['$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', LibraryReadingNewController]);

	function LibraryReadingNewController($routeParams, $rootScope, Events, REST, Breadcrumb, i18n,
		ErrorFormatter, NotificationCenter) {
		$rootScope.$on(Events.EditorReady, function(event, parameters) {
			REST.resource('Rbs_User_User', $routeParams.userId).then(
				function(doc) {
					parameters.document.reader = doc;
				},
				function(reason) {
					NotificationCenter.error([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.userId +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
		});
	}

	app.controller('LibraryReadingDetailController', ['$scope', 'RbsChange.Document', LibraryReadingDetailController]);

	function LibraryReadingDetailController(scope, Document) {
		Document.initDetailView(scope, 'Project_Library_Reading');
	}

	app.controller('LibraryReadingEditController', ['$scope', 'RbsChange.Document', LibraryReadingEditController]);

	function LibraryReadingEditController(scope, Document) {
		Document.initEditView(scope, 'Project_Library_Reading');
	}

	app.directive('projectLibraryReadingDetailEdit', function() {
		return {
			restrict: 'A',
			scope: true,

			link: function(scope) {
				scope.publicationsStaticFilters = [
					{
						name: 'publications',
						parameters: { propertyName: 'work', value: 0, operator: 'eq' }
					}
				];
				scope.volumesStaticFilters = [
					{
						name: 'volumes',
						parameters: { propertyName: 'publication', value: 0, operator: 'eq' }
					}
				];
				scope.$watch('document.work', function(newValue) {
					if (newValue) {
						scope.publicationsStaticFilters[0].parameters.value = newValue.id;
					}
					else {
						scope.publicationsStaticFilters[0].parameters.value = 0;
					}
				});
				scope.$watch('document.publication', function(newValue) {
					if (newValue) {
						scope.volumesStaticFilters[0].parameters.value = newValue.id;
					}
					else {
						scope.volumesStaticFilters[0].parameters.value = 0;
					}
				});
				scope.copyToEndDate = function () {
					scope.document.endDate = scope.document.beginDate;
				}
			}
		};
	});
})
();
