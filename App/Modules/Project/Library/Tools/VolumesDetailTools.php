<?php
namespace Project\Library\Tools;

/**
 * @name \Project\Library\Tools\VolumesDetailTools
 */
class VolumesDetailTools
{
	/**
	 * @var integer[]
	 */
	protected $intVolumes = [];

	/**
	 * @var string[]
	 */
	protected $stringVolumes = [];

	/**
	 * @param string $description
	 */
	public function addDescription($description)
	{
		foreach (\explode('/', $description) as $value)
		{
			$value = trim($value);
			if (\strlen($value) > 0)
			{
				$numbers = \explode('-', $value);
				if (\count($numbers) === 1)
				{
					$this->addVolume(trim($numbers[0]));
				}
				else if (\count($numbers) === 2)
				{
					$debut = \trim($numbers[0]);
					$intDebut = (int)$debut;
					$fin = \trim($numbers[1]);
					$intFin = (int)$fin;
					if ($intDebut < $intFin && $this->isNumber($debut) && $this->isNumber($fin))
					{
						for ($i = $intDebut; $i <= $intFin; $i++)
						{
							$this->addVolume((string)$i);
						}
					}
				}
			}
		}
	}

	/**
	 * @param \Project\Library\Documents\PublicationVolume|string|integer $number
	 */
	public function addVolume($number)
	{
		if ($number instanceof \Project\Library\Documents\PublicationVolume)
		{
			$number = $number->getNumber();
		}

		$intNumber = (int)$number;
		if ((string)$number === (string)$intNumber)
		{
			if (!\in_array($intNumber, $this->intVolumes, true))
			{
				$this->intVolumes[] = $intNumber;
			}
		}
		else if(!\in_array($number, $this->stringVolumes, true))
		{
			$this->stringVolumes[] = $number;
		}
	}

	/**
	 * @param \Project\Library\Documents\PublicationVolume[]|string[]|integer[] $volumes
	 */
	public function addVolumes($volumes)
	{
		foreach ($volumes as $volume)
		{
			$this->addVolume($volume);
		}
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		\asort($this->intVolumes);
		\asort($this->stringVolumes);

		$label = '';
		$start = -1;
		$previous = -1;
		foreach($this->intVolumes as $number)
		{
			if ($previous === -1)
			{
				$label = $number;
				$previous = $number;
				$start = $number;
			}
			else
			{
				if ($number - 1 === $previous)
				{
					$previous = $number;
				}
				else
				{
					if ($start === $previous)
					{
						$label .= ' / ' . $number;
					}
					else
					{
						$label .= '-' . $previous . ' / ' . $number;
					}
					$previous = $number;
					$start = $number;
				}
			}
		}
		if ($start !== $previous)
		{
			$label .= '-' . $previous;
		}
		if ($label)
		{
			\array_unshift($this->stringVolumes, $label);
		}

		return \implode(' / ', $this->stringVolumes);
	}

	/**
	 * @param integer|string $number
	 * @return bool
	 */
	protected function isNumber($number)
	{
		return $number === (string)(int)$number;
	}
}