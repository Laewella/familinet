<?php
/**
 * Copyright (C) 2014 Laewella
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Collection;

/**
 * @name \Project\Library\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addCollections(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$publisherId = $event->getParam('publisher');
			$documentManager = $applicationServices->getDocumentManager();
			$query = $documentManager->getNewQuery('Project_Library_Collection');
			$query->andPredicates($query->eq('publisher', $publisherId));
			$query->addOrder('label');

			$collection = [];
			foreach ($query->getDocuments() as $doc)
			{
				/** @var $doc \Project\Library\Documents\Collection */
				$collection[$doc->getId()] = $doc->getLabel();
			}

			$collection = new \Change\Collection\CollectionArray('Project_Library_Collections', $collection);
			$event->setParam('collection', $collection);
			$event->stopPropagation();
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addWebSiteParsers(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				\Project\Library\WebSiteParsers\MangaSanctuaryWorkParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.manga_sanctuary_work_parser', ['ucf']),
				\Project\Library\WebSiteParsers\AllocineSerieParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.allocine_serie_parser', ['ucf']),
				\Project\Library\WebSiteParsers\AllocineMovieParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.allocine_movie_parser', ['ucf']),
				\Project\Library\WebSiteParsers\AllocinePersonParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.allocine_person_parser', ['ucf']),
				\Project\Library\WebSiteParsers\ImdbWorkParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.imdb_work_parser', ['ucf']),
				\Project\Library\WebSiteParsers\ImdbPersonParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.imdb_person_parser', ['ucf']),
				\Project\Library\WebSiteParsers\DecitreWorkParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.decitre_work_parser', ['ucf']),
				\Project\Library\WebSiteParsers\DecitrePersonParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.decitre_person_parser', ['ucf']),
				\Project\Library\WebSiteParsers\TricTracGameParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.trictrac_game_parser', ['ucf']),
				\Project\Library\WebSiteParsers\MusicBrainzCDParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.musicbrainz_cd_parser', ['ucf']),
				\Project\Library\WebSiteParsers\MusicBrainzPersonParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.musicbrainz_person_parser', ['ucf']),
				\Project\Library\WebSiteParsers\SimpleWorkParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.simple_work_parser', ['ucf']),
				\Project\Library\WebSiteParsers\SimpleContributorParser::class =>
					new \Change\I18n\I18nString($i18n, 'm.project.library.admin.simple_contributor_parser', ['ucf']),
                \Project\Library\WebSiteParsers\BabelioPersonParser::class =>
                    new \Change\I18n\I18nString($i18n, 'm.project.library.admin.babelio_person_parser', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Project_Library_WebSiteParsers', $collection);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addContributionImportance(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'major' => new \Change\I18n\I18nString($i18n, 'm.project.library.admin.importance_major', ['ucf']),
				'minor' => new \Change\I18n\I18nString($i18n, 'm.project.library.admin.importance_minor', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Project_Library_ContributionImportance', $collection);
			$event->setParam('collection', $collection);
		}
	}
}