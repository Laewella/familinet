<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\SimpleWorkParser
 */
class SimpleWorkParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return true;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		return true;
	}
}