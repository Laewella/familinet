<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\DecitrePersonParser
 */
class DecitrePersonParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$this->data['contributor']['typology'] = 'person';
		$this->setStringByXPath('contributor', 'label', '', "//div[contains(@class, 'nbr_result')]/h2");

		/** @var DOMNode $translatorNode */
		foreach ($this->DOMXPath->query("//div[contains(@class, 'resume_result_search')]//p") as $bio)
		{
			$this->appendToDescription('contributor', $this->cleanupContent($bio->nodeValue));
		}

		return true;
	}
}