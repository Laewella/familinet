<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\TricTracGameParser
 */
class TricTracGameParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}


		$this->data['work']['typology'] = 'jeu_societe';
		$this->data['work']['status'] = 'ended';

		$label = $this->getValueByXPathSpecific('//div[contains(@class, \'block_title\')]/h2/span[@class=\'title item\']');
		if ($label)
		{
			$this->data['work']['label'] = \Change\Stdlib\StringUtils::ucfirst(\trim($label));
		}

		$this->setStringByXPathSpecific('work', 'year', null, '//div[contains(@class, \'block_title\')]/h2/span[@class=\'years\']');
		$this->setStringByXPathSpecific('work', 'public', null, '//div[contains(@class, \'block_bags\')]/a[@title=\'Filtrer par age\']');

		$nationality = $this->getValueByXPathSpecific('//div[contains(@class, \'block_title_others\')]/img/@alt');
		if ($nationality)
		{
			$this->data['work']['nationalities'][] = \Change\Stdlib\StringUtils::toLower(\trim($nationality));
		}

		$this->appendToDescription('work', $this->getValueByXPathSpecific('//div[contains(@class, \'award_box\')]'));

		$this->setStringByXPathSpecific('work', 'attributes', 'duree_partie', '//div[contains(@class, \'block_bags\')]/a[@title=\'Filtrer par durée\']');
		$this->setStringByXPathSpecific('work', 'attributes', 'nb_joueurs', '//div[contains(@class, \'block_bags\')]/a[@title=\'Filtrer par joueur\']');

		$this->setStringByXPathSpecific('publication', 'publisher', null, '//div[contains(@class, \'block_editors\')]/a/@title');
		$this->data['publication']['status'] = 'ended';
		$this->data['publication']['oneShot'] = true;
		$this->appendToDescription('publication', $this->getValueByXPathSpecific('//div[contains(@class, \'block_fourth_cover\')]'));

		return true;
	}

	private function setStringByXPathSpecific($objectName, $propertyName, $subPropertyName, $xPath)
	{
		$this->setValue($objectName, $propertyName, $subPropertyName, $this->getValueByXPathSpecific($xPath));
	}

	private function getValueByXPathSpecific($xPath)
	{
		$value = null;
		foreach ($this->DOMXPath->query($xPath) as $node)
		{
			$value = $this->cleanupContent($node->nodeValue);
		}
		return $value;
	}
}