<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AbstractParser
 */
abstract class AbstractParser
{
	const CODE_CONTEXT = 'External sites aliases';

	/**
	 * @var \DOMDocument
	 */
	protected $DOMDocument;

	/**
	 * @var \DOMXPath
	 */
	protected $DOMXPath;

	/**
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var string
	 */
	protected $url;

	/**
	 * @var \Project\Library\Documents\WebSite
	 */
	protected $webSite;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Documents\DocumentCodeManager
	 */
	protected $documentCodeManager;

	/**
	 * @var \Rbs\Geo\GeoManager
	 */
	protected $geoManager;

	/**
	 * @var array
	 */
	protected $StatusType = [
		'work' => ['Project_Library_WorkStatus', \Project\Library\Documents\WorkStatus::class],
		'publication' => ['Project_Library_WorkStatus', \Project\Library\Documents\WorkStatus::class]
	];

	/**
	 * @var array
	 */
	protected $AliasType = [
		'work' => ['Project_Library_WorkAlias', \Project\Library\Documents\WorkAlias::class],
		'contributor' => ['Project_Library_ContributorAlias', \Project\Library\Documents\ContributorAlias::class]
	];

	/**
	 * @var array
	 */
	protected $LinkType = [
		'work' => ['Project_Library_WorkLink', \Project\Library\Documents\WorkLink::class],
		'contributor' => ['Project_Library_ContributorLink', \Project\Library\Documents\ContributorLink::class]
	];

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Documents\DocumentCodeManager $documentCodeManager
	 * @param \Rbs\Geo\GeoManager $geoManager
	 * @param string $url
	 * @param \Project\Library\Documents\WebSite $webSite
	 */
	public function __construct($documentManager, $documentCodeManager, $geoManager, $url, $webSite)
	{
		$this->documentManager = $documentManager;
		$this->documentCodeManager = $documentCodeManager;
		$this->geoManager = $geoManager;
		$this->url = $url;
		$this->webSite = $webSite;
		$content = file_get_contents($url);
		if ($content)
		{
			$this->DOMDocument = new \DOMDocument();
			@$this->DOMDocument->loadHTML('<?xml encoding="UTF-8">' . $content);
			$this->DOMXPath = new \DOMXPath($this->DOMDocument);
		}
	}

	/**
	 * @return string
	 */
	abstract public static function getType();

	/**
	 * @return bool true if the parser only adds the link but don't parse anything in the page.
	 */
	abstract public static function isMinimal();

	/**
	 * @return array|null
	 */
	public function parseData()
	{
		$this->addLink($this->webSite->getLabel());
		return $this->doParseData() ? $this->postProcess() : null;
	}

	/**
	 * Post process data.
	 * @return array
	 */
	protected function postProcess()
	{
		foreach ($this->data as $code => $value)
		{
			$this->postProcessOnObject($code);
		}

		if (isset($this->data['work']))
		{
			$this->postProcessOnWork();
		}

		if (isset($this->data['publication']))
		{
			$this->postProcessOnPublication();
		}

		return $this->data;
	}

	protected function postProcessOnObject($object)
	{
		// Typology.
		$typology = null;
		if (isset($this->data[$object]['typology']))
		{
			$query = $this->documentManager->getNewQuery('Rbs_Generic_Typology');
			$query->andPredicates($query->eq('name', $this->data[$object]['typology']));
			/** @var \Rbs\Generic\Documents\Typology|null $typology */
			$typology = $query->getFirstDocument();
			$this->data[$object]['typology'] = $typology ? $typology->getId() : null;
		}

		// Nationalities.
		if (isset($this->data[$object]['nationalities']))
		{
			/** @var string[] $countryCodes */
			$nationalities = $this->data[$object]['nationalities'];
			$this->data[$object]['nationalities'] = [];
			$notFound = [];
			foreach ($nationalities as $nationality)
			{
				$infos = $this->getDocumentInfos('Rbs_Geo_Country', \Rbs\Geo\Documents\Country::class,
					['label' => $nationality, 'code' => $nationality, 'isoCode' => $nationality], $nationality);
				if ($infos)
				{
					$this->data[$object]['nationalities'][] = $infos;
				}
				else
				{
					$notFound[] = $nationality;
				}
			}
			if ($notFound)
			{
				$this->appendToDescription($object, 'Autres nationalités : ' . \implode(', ', $notFound));
			}
		}

		// Status.
		if (isset($this->StatusType[$object], $this->data[$object]['status']))
		{
			$infos = $this->getDocumentInfos($this->StatusType[$object][0], $this->StatusType[$object][1],
				['code' => $this->data[$object]['status']], $this->data[$object]['status']);
			if ($infos)
			{
				$this->data[$object]['status'] = $infos;
			}
		}

		// Aliases.
		if (isset($this->AliasType[$object], $this->data[$object]['aliases']))
		{
			/** @var array $aliases */
			$aliases = $this->data[$object]['aliases'];
			foreach ($aliases as $key => $alias)
			{
				$alias['model'] = $this->AliasType[$object][0];
				$nationalities = $alias['nationalities'] ?? [];
				$alias['nationalities'] = [];
				foreach ($nationalities as $nationality)
				{
					$infos = $this->getDocumentInfos('Rbs_Geo_Country', \Rbs\Geo\Documents\Country::class,
						['label' => $nationality, 'code' => $nationality, 'isoCode' => $nationality], $nationality);
					if ($infos)
					{
						$alias['nationalities'][] = $infos;
					}
				}
				$this->data[$object]['aliases'][$key] = $alias;
			}
		}

		// Links.
		if (isset($this->LinkType[$object], $this->data[$object]['links']))
		{
			/** @var array $links */
			$links = $this->data[$object]['links'];
			foreach ($links as $key => $link)
			{
				$link['model'] = $this->LinkType[$object][0];
				$this->data[$object]['links'][$key] = $link;
			}
		}

		// Attributes.
		if ($typology && isset($this->data[$object]['attributes']))
		{
			/** @var array $attributes */
			$attributes = $this->data[$object]['attributes'];
			foreach ($attributes as $code => $value)
			{
				$this->data[$object]['attributes'][$code] = $this->getAttributeValue($typology, $code, $value);
			}
		}
	}

	protected function postProcessOnWork()
	{
		// Oeuvre : langue originale
		if (isset($this->data['work']['originalLanguage']))
		{
			$value = $this->getValueFromCollection('Project_Library_Languages', $this->data['work']['originalLanguage']);
			if (!$value)
			{
				$this->appendToDescription('work', 'Langue origniale : ' . $this->data['work']['originalLanguage']);
			}
			$this->data['work']['originalLanguage'] = $value;
		}

		// Oeuvre : genres
		if (isset($this->data['work']['genres']))
		{
			/** @var string[] $genreLabels */
			$genreLabels = $this->data['work']['genres'];
			$notFound = [];
			$this->data['work']['genres'] = [];
			foreach ($genreLabels as $genreLabel)
			{
				$infos = $this->getDocumentInfos('Project_Library_Genre', \Project\Library\Documents\Genre::class,
					['label' => $genreLabel], $genreLabel);
				if ($infos)
				{
					$this->data['work']['genres'][] = $infos;
				}
				else
				{
					$notFound[] = $genreLabel;
				}
			}
			if (\count($notFound))
			{
				$this->appendToDescription('work', 'Autres genres : ' . \implode(', ', $notFound));
			}
		}
	}

	protected function postProcessOnPublication()
	{
		$publisherValue = null;
		$collectionValue = null;
		$publisher = null;
		$collection = null;

		if (isset($this->data['publication']['publisher']))
		{
			$publisherValue = $this->data['publication']['publisher'];
			unset($this->data['publication']['publisher']);
			$publisher = $this->getDocumentInfos('Project_Library_Publisher', \Project\Library\Documents\Publisher::class,
				['label' => $publisherValue], $publisherValue);
		}

		if (isset($this->data['publication']['collection']))
		{
			$collectionValue = $this->data['publication']['collection'];
			unset($this->data['publication']['collection']);
			if ($collectionValue === $publisherValue)
			{
				$collectionValue = null;
			}
			elseif ($publisher)
			{
				$collection = $this->getDocumentInfos('Project_Library_Collection', \Project\Library\Documents\Collection::class,
					['label' => $collectionValue, 'publisher' => $publisher['id']], $collectionValue);
			}
		}

		if ($publisher)
		{
			$this->data['publication']['publisher'] = $publisher;
		}
		elseif ($publisherValue)
		{
			$this->appendToDescription('publication', 'Editeur : ' . $publisherValue);
		}

		if ($collection)
		{
			$this->data['publication']['collection'] = $collection;
		}
		elseif ($collectionValue)
		{
			$this->appendToDescription('publication', 'Collection : ' . $collectionValue);
		}
	}

	/**
	 * @return boolean
	 */
	abstract protected function doParseData();

	/**
	 * @param string $content
	 * @return string
	 */
	protected function cleanupContent($content)
	{
		return \trim($content);
	}

	/**
	 * @param string $label
	 * @param string $section
	 */
	protected function addLink($label, $section = null)
	{
		$this->data[$section ?: static::getType()]['links'][] = [
			'label' => $label,
			'url' => $this->url
		];
	}

	/**
	 * Add link if a website can be identify by url domain
	 * @param string $url
	 * @param string $section
	 */
	protected function addLinkByUrl($url, $section = null)
	{
		$domain = parse_url($url, PHP_URL_HOST);
		$website = $this->getDocumentInfos('Project_Library_WebSite', \Project\Library\Documents\WebSite::class,
			['domain' => $domain]);
		if ($website)
		{
			$this->data[$section ?: static::getType()]['links'][] = ['label' => $website['label'], 'url' => $url];
		}
	}

	/**
	 * @param string $section
	 * @param string $text
	 */
	protected function appendToDescription($section, $text)
	{
		if (!isset($this->data[$section]['description']) || !$this->data[$section]['description'])
		{
			$this->data[$section]['description'] = '';
		}
		elseif (!\Change\Stdlib\StringUtils::endsWith($this->data[$section]['description'], "\n\n"))
		{
			$this->data[$section]['description'] .= "\n\n";
		}
		$this->data[$section]['description'] .= $text;
	}

	/**
	 * @param string $type
	 * @param string $class
	 * @param array $properties
	 * @param string|null $code
	 * @return array|null
	 */
	protected function getDocumentInfos($type, $class, $properties, $code = null)
	{
		$query = $this->documentManager->getNewQuery($type);
		foreach ($properties as $property => $value)
		{
			$query->orPredicates($query->like($property, $value));
		}
		$result = $query->getFirstDocument();
		if (!($result instanceof $class) && $code)
		{
			foreach ($this->documentCodeManager->getDocumentsByCode($code, self::CODE_CONTEXT) as $document)
			{
				if ($document instanceof $class)
				{
					$result = $document;
					break;
				}
			}
		}

		if ($result instanceof $class && $result instanceof \Change\Documents\AbstractDocument
			&& $result instanceof \Change\Documents\Interfaces\Editable
		)
		{
			return [
				'model' => $result->getDocumentModelName(),
				'id' => $result->getId(),
				'label' => $result->getLabel()
			];
		}
		return null;
	}

	/**
	 * @param \Rbs\Generic\Documents\Typology|null $typology
	 * @param string $attributeCode
	 * @param mixed $value
	 * @return mixed
	 */
	protected function getAttributeValue($typology, $attributeCode, $value)
	{
		if (!($typology instanceof \Rbs\Generic\Documents\Typology))
		{
			return $value;
		}
		$collectionCode = null;
		foreach ($typology->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				if ($attribute->getName() === $attributeCode)
				{
					$collectionCode = $attribute->getCollectionCode();
					break;
				}
			}
		}
		return $this->getValueFromCollection($collectionCode, $value);
	}

	/**
	 * @param string $collectionCode
	 * @param string $value
	 * @return mixed
	 */
	protected function getValueFromCollection($collectionCode, $value)
	{
		$convertedValue = $value;
		if ($collectionCode)
		{
			$query = $this->documentManager->getNewQuery('Rbs_Collection_Collection');
			$query->andPredicates($query->eq('code', $collectionCode));
			$collection = $query->getFirstDocument();
			if ($collection instanceof \Rbs\Collection\Documents\Collection && !$collection->getItemByValue($value))
			{
				$converterId = $this->documentManager->getAttributeValues($collection)->get('converter');
				$converter = $this->documentManager->getDocumentInstance($converterId);
				if ($converter instanceof \Project\Familinet\Documents\Mapping)
				{
					$convertedValue = $converter->getMappedValue($value);
				}
			}
		}
		return $convertedValue;
	}

	/**
	 * @param string $format
	 * @param string $value
	 * @return null|string
	 */
	protected function getDate($format, $value)
	{
		$date = \DateTime::createFromFormat($format, $value);
		return $date ? $date->format(\DateTime::ATOM) : null;
	}

	/**
	 * @param string $value
	 * @return int|null
	 */
	protected function getInteger($value)
	{
		$nb = \preg_replace('/\D/', '', $value);
		return $nb ? (int)$nb : null;
	}

	/**
	 * @param string $objectName
	 * @param string $propertyName
	 * @param string $subPropertyName
	 * @param string $xPath
	 * @param string[] $ignoredValues
	 */
	protected function setStringByXPath($objectName, $propertyName, $subPropertyName, $xPath, array $ignoredValues = null)
	{
		$value = $this->getValueByXPath($xPath);
		if ($ignoredValues)
		{
			if ($value && (!\is_array($ignoredValues) || !\in_array($value, $ignoredValues, true)))
			{
				$this->setValue($objectName, $propertyName, $subPropertyName, $value);
			}
		}
		else
		{
			$this->setValue($objectName, $propertyName, $subPropertyName, $value);
		}
	}

	/**
	 * @param string $objectName
	 * @param string $propertyName
	 * @param string $subPropertyName
	 * @param string $xPath
	 */
	protected function setNumberByXPath($objectName, $propertyName, $subPropertyName, $xPath)
	{
		$value = $this->getValueByXPath($xPath);
		if ($value)
		{
			$value = $this->getInteger($value);
		}
		if ($value)
		{
			$this->setValue($objectName, $propertyName, $subPropertyName, $value);
		}
	}

	/**
	 * @param string $objectName
	 * @param string $propertyName
	 * @param string $subPropertyName
	 * @param string $xPath
	 * @param string $format
	 */
	protected function setDateByXPath($objectName, $propertyName, $subPropertyName, $xPath, $format)
	{
		$value = $this->getValueByXPath($xPath);
		if ($value)
		{
			if (false !== \strpos($format, 'F'))
			{
				$value = $this->replaceMonths($value);
			}
			$value = $this->getDate($format, $value);
		}
		if ($value)
		{
			$this->setValue($objectName, $propertyName, $subPropertyName, $value);
		}
	}

	/**
	 * @param string $value
	 * @return string
	 */
	protected function replaceMonths($value)
	{
		return \str_replace(
			['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
			['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			$value
		);
	}

	/**
	 * @param string $objectName
	 * @param string $propertyName
	 * @param string $subPropertyName
	 * @param mixed $value
	 */
	protected function setValue($objectName, $propertyName, $subPropertyName, $value)
	{
		if ($value)
		{
			if ($subPropertyName)
			{
				$this->data[$objectName][$propertyName][$subPropertyName] = $value;
			}
			else
			{
				$this->data[$objectName][$propertyName] = $value;
			}
		}
	}

	/**
	 * @param string $xPath
	 * @return string
	 */
	protected function getValueByXPath($xPath)
	{
		$value = null;
		foreach ($this->DOMXPath->query($xPath) as $node)
		{
			if ($node->childNodes->length === 0)
			{
				$value = $this->cleanupContent($node->nodeValue);
			}
			else
			{
				foreach ($node->childNodes as $element)
				{
					if ($element instanceof \DOMElement && $element->tagName === 'br')
					{
						$value .= "\n\n";
					}
					else
					{
						$value .= $this->cleanupContent($element->nodeValue);
					}
				}
			}
			break;
		}
		return $this->cleanupContent($value);
	}

	/**
	 * @param string $object
	 * @param string $typologyCode
	 */
	protected function setTypology($object, $typologyCode)
	{
		$this->data[$object]['typology'] = $typologyCode;
	}

	/**
	 * @param string $class
	 * @param string $content
	 * @return string
	 */
	protected function getFilter($class, $content)
	{
		if ($class && $content)
		{
			return 'contains(@class, \'' . $class . '\') and normalize-space(.) = \'' . $content . '\'';
		}
		if ($class)
		{
			return 'contains(@class, \'' . $class . '\')';
		}
		if ($content)
		{
			return 'normalize-space(.) = \'' . $content . '\'';
		}
		return '';
	}

	/**
	 * @param string $node
	 * @param string $class
	 * @param string $content
	 * @return string
	 */
	protected function getFilterByBrother($node, $class, $content)
	{
		return '../' . $node . '[' . $this->getFilter($class, $content) . ']';
	}

	/**
	 * @param $contribution Contribution (mise à jour avec le rôle et le contributeur s'ils sont trouvés)
	 */
	protected function updateContribution(&$contribution)
	{
		$role = $this->getDocumentInfos('Project_Library_ContributionType', \Project\Library\Documents\ContributionType::class,
			['label' => $contribution['roleCode']], $contribution['roleCode']);
		if ($role)
		{
			$contribution['role'] = $role;
			unset($contribution['roleCode']);

			// Rechercher le contributeur par son nom et vérification du lien
			$infos = $this->getDocumentInfos('Project_Library_Contributor', \Project\Library\Documents\Contributor::class,
				['label' => $contribution['label']]);
			if ($infos)
			{
				/** @var \Project\Library\Documents\Contributor $contributor */
				$contributor = $this->documentManager->getDocumentInstance($infos['id'], $infos['model']);

				/** @var \Project\Library\Documents\ContributorLink $link */
				foreach ($contributor->getLinks() as $link)
				{
					$url1 = $link->getUrl();
					if (preg_match('/http(s)*:\/\/(.*)(\/)*/', $url1, $matches))
					{
						$url1 = $matches[2];
					}
					$url2 = $contribution['link'];
					if (preg_match('/http(s)*:\/\/(.*)(\/)*/', $url2, $matches))
					{
						$url2 = $matches[2];
					}
					if ($url1 == $url2)
					{
						$contribution['contributor'] = $infos;
					}
				}
			}
		}
	}
}