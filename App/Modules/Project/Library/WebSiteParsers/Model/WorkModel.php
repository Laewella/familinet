<?php
namespace Project\Library\WebSiteParsers\Model;

/**
 * @name \Project\Library\WebSiteParsers\Model\WorkModel
 */
class WorkModel extends \Project\Library\WebSiteParsers\Model\AbstractModel
{
	/** @var string */
	protected $label;

	/** @var int */
	protected $year;

	/** @var int */
	protected $endYear;

	/** @var string */
	protected $description;

	/** @var string[] */
	protected $genres = [];

	/** @var string */
	protected $image;

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		if ($label)
		{
			$this->label = (string)$label;
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getYear()
	{
		return $this->year;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setYear($year)
	{
		if ($year !== null)
		{
			$this->year = (int)$year;
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getEndYear()
	{
		return $this->endYear;
	}

	/**
	 * @param int $endYear
	 * @return $this
	 */
	public function setEndYear($endYear)
	{
		if ($endYear !== null)
		{
			$this->endYear = (int)$endYear;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		if ($description)
		{
			$this->description = (string)$description;
		}
		return $this;
	}

	/**
	 * @return string[]
	 */
	public function getGenres()
	{
		return $this->genres;
	}

	/**
	 * @param string $genre
	 * @return $this
	 */
	public function addGenre($genre)
	{
		if ($genre && \is_string($genre))
		{
			$this->genres[] = \Change\Stdlib\StringUtils::ucfirst($genre);
		}
		return $this;
	}

	/**
	 * @param string|string[] $genre
	 * @return $this
	 */
	public function setGenres($genre)
	{
		$this->genres = [];
		if (\is_string($genre))
		{
			$this->addGenre($genre);
		}
		else if (\is_array($genre))
		{
			foreach ($genre as $oneGenre)
			{
				$this->addGenre($oneGenre);
			}
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param string $image
	 * @return $this
	 */
	public function setImage($image)
	{
		if ($image)
		{
			$this->image = (string)$image;
		}
		return $this;
	}

	/**
	 * @param int $number
	 * @return $this
	 */
	public function setNumberOfSeasons($number)
	{
		if ($number)
		{
			$this->setAttribute('nb_saisons', (int)$number);
		}
		return $this;
	}

	/**
	 * @param int $number
	 * @return $this
	 */
	public function setNumberOfEpisodes($number)
	{
		if ($number)
		{
			$this->setAttribute('nb_episodes', (int)$number);
		}
		return $this;
	}
}