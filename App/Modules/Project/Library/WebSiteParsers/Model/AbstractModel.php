<?php
namespace Project\Library\WebSiteParsers\Model;

/**
 * @name \Project\Library\WebSiteParsers\Model\AbstractModel
 */
abstract class AbstractModel
{
	/**
	 * @var string
	 */
	protected $typology;

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * @return string
	 */
	public function getTypology()
	{
		return $this->typology;
	}

	/**
	 * @param string $typology
	 * @return $this
	 */
	public function setTypology(string $typology)
	{
		$this->typology = $typology;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * @param array $attributes
	 * @return $this
	 */
	public function setAttributes(array $attributes)
	{
		$this->attributes = $attributes;
		return $this;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function setAttribute(string $name, $value)
	{
		$this->attributes[$name] = $value;
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return \get_object_vars($this);
	}
}