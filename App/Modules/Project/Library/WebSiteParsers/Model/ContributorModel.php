<?php
namespace Project\Library\WebSiteParsers\Model;

/**
 * @name \Project\Library\WebSiteParsers\Model\ContributorModel
 */
class ContributorModel extends \Project\Library\WebSiteParsers\Model\AbstractModel
{
	/** @var string */
	protected $label;

	/** @var string */
	protected $birthDate;

	/** @var int */
	protected $birthYear;

	/** @var string */
	protected $deathDate;

	/** @var int */
	protected $deathYear;

	/** @var string */
	protected $description;

	/** @var string */
	protected $image;

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		if ($label)
		{
			$this->label = (string)$label;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getBirthDate()
	{
		return $this->birthDate;
	}

	/**
	 * @param string|\DateTime $birthDate
	 * @return $this
	 */
	public function setBirthDate($birthDate)
	{
		if (\is_string($birthDate))
		{
			$birthDate = new \DateTime($birthDate);
		}
		if ($birthDate instanceof \DateTime)
		{
			$this->birthDate = $birthDate->format(\DateTime::ATOM);
			$this->birthYear = $birthDate->format('Y');
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getBirthYear()
	{
		return $this->birthYear;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setBirthYear($year)
	{
		if ($year !== null)
		{
			$this->birthYear = (int)$year;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDeathDate()
	{
		return $this->deathDate;
	}

	/**
	 * @param string|\DateTime $deathDate
	 * @return $this
	 */
	public function setDeathDate($deathDate)
	{
		if (\is_string($deathDate))
		{
			$deathDate = new \DateTime($deathDate);
		}
		if ($deathDate instanceof \DateTime)
		{
			$this->deathDate = $deathDate->format(\DateTime::ATOM);
			$this->birthYear = $deathDate->format('Y');
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getDeathYear()
	{
		return $this->deathYear;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setDeathYear($year)
	{
		if ($year !== null)
		{
			$this->deathYear = (int)$year;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		if ($description)
		{
			$this->description = (string)$description;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param string $image
	 * @return $this
	 */
	public function setImage($image)
	{
		if ($image)
		{
			$this->image = (string)$image;
		}
		return $this;
	}
}