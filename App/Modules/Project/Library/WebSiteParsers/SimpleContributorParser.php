<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\SimpleContributorParser
 */
class SimpleContributorParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return true;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		return true;
	}
}