<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\ImdbPersonParser
 */
class ImdbPersonParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument, $this->data);

		unset($this->data['contributor']['description']); // Incomplete and in english...

		return true;
	}
}