<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocineCastingParser
 */
class AllocineCastingParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributions';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$contributors = $this->getJsonData();

		foreach ($this->DOMXPath->query("//*[contains(@class, 'section')]") as $section)
		{
			foreach ($this->DOMXPath->query("div[contains(@class, 'titlebar')]", $section) as $titleNode)
			{
				$title = $this->cleanupContent($titleNode->nodeValue);
				foreach ($this->DOMXPath->query("div/div[contains(@class, 'person-card')]/div[contains(@class, 'meta')]", $section) as
						 $contributionNode)
				{
					$contribution = $this->parsePersonCard($contributionNode, $title, $contributors);
					$this->addContribution($contribution, true);
				}
				foreach ($this->DOMXPath->query("div[contains(@class, 'md-table-row')]", $section) as $contributionNode)
				{
					$contribution = $this->parseTableRow($contributionNode, $title, $contributors);
					$this->addContribution($contribution, false);
				}
				foreach ($this->DOMXPath->query("div/div[contains(@class, 'md-table-row')]", $section) as $contributionNode)
				{
					$contribution = $this->parseTableRow($contributionNode, $title, $contributors);
					$this->addContribution($contribution, false);
				}
			}
		}

		return true;
	}

	protected function getJsonData()
	{
		$contributors = [];

		foreach ($this->DOMXPath->query("//script[@type='application/ld+json']") as $jsonld)
		{
			$object = json_decode($jsonld, true);
			$actors = $object['actor'] ?? [];
			foreach ($actors as $actor)
			{
				if ($actor['url'])
				{
					$contributors[$actor['name']] = $actor['url'];
				}
			}
		}

		return $contributors;
	}

	protected function addContribution($contribution, $forceAdd)
	{
		if (!empty($contribution))
		{
			$this->updateContribution($contribution);

			if ($contribution['role'] && ($forceAdd || $contribution['contributor']))
			{
				$this->data['contributions']['contributions'][] = $contribution;
			}
		}
	}

	protected function parsePersonCard($contributionNode, $title, $contributors)
	{
		$contribution = [];

		// Personne
		foreach ($this->DOMXPath->query("div[contains(@class, 'meta-title')]/*", $contributionNode) as $personNode)
		{
			switch ($personNode->tagName)
			{
				case 'a':
					$contribution = [];
					$contribution['label'] = $this->cleanupContent($personNode->nodeValue);
					$contribution['link'] = $this->webSite->getDomain() . $personNode->getAttribute('href');
					break;
				case 'span':
					$contribution = [];
					$contribution['label'] = $this->cleanupContent($personNode->nodeValue);
					if (key_exists($contribution['label'], $contributors))
					{
						$contribution['link'] = $contributors[$contribution['label']];
					}
					break;
			}
			break;
		}

		// Rôle
		$role = '';
		foreach ($this->DOMXPath->query("div[contains(@class, 'meta-sub')]", $contributionNode) as $roleNode)
		{
			$role = $this->cleanupContent($roleNode->nodeValue);

			if (preg_match('/Rôle : (.*)/', $role, $matches))
			{
				$roleDetail = $this->cleanupContent($matches[1]);
				$role = "Acteur";
				$contribution['roleDetail'] = $roleDetail;
			}
			break;
		}

		$contribution['roleCode'] = empty($role) ? $title : $role;
		return $contribution;
	}

	protected function parseTableRow($contributionNode, $title, $contributors)
	{
		$contribution = [];
		$role = '';

		foreach ($contributionNode->childNodes as $child)
		{
			if ($child instanceof \DOMElement)
			{
				if (strpos($child->getAttribute('class'), 'light') !== false)
				{
					$role = $this->cleanupContent($child->nodeValue);
					if ($title == 'Acteurs et actrices')
					{
						$contribution['roleDetail'] = $role;
						$role = 'Acteur';
					}
					$contribution['roleCode'] = $role;
				}
				elseif (strpos($child->getAttribute('class'), 'link') !== false)
				{
					$contribution['label'] = $this->cleanupContent($child->nodeValue);
					if ($child->getAttribute('href'))
					{
						$contribution['link'] = $this->webSite->getDomain() . $child->getAttribute('href');
					}
				}
			}
		}

		$contribution['roleCode'] = $role ?? $title;
		return $contribution;
	}
}