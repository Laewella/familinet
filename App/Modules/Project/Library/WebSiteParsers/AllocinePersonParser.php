<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocinePersonParser
 */
class AllocinePersonParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$this->data['contributor']['typology'] = 'person';

		foreach ($this->DOMXPath->query('//div[contains(@class, \'titlebar-page\')]//div[contains(@class, \'titlebar-title\')]') as $node)
		{
			$this->specificSetValue('label', $this->cleanupContent($node->nodeValue));
		}

		foreach ($this->DOMXPath->query('//div[contains(@class, \'meta-body-item\')]') as $node)
		{
			/** @var \DOMElement $node */
			$key = null;
			foreach ($node->getElementsByTagName('span') as $spanNode)
			{
				/** @var \DOMElement $spanNode */
				if (\strpos($spanNode->getAttribute('class'), 'light') !== false)
				{
					switch ($this->cleanupContent($spanNode->nodeValue))
					{
						case 'Nom de naissance': // Richard Armitage
							foreach ($node->getElementsByTagName('h2') as $h2Node)
							{
								$this->specificSetValue('civilName', $this->cleanupContent($h2Node->nodeValue));
							}
							break;

						case 'Pseudo': // Wood Harris
						case 'Pseudos':
							foreach ($node->getElementsByTagName('h2') as $h2Node)
							{
								$this->specificSetValue('alias', $this->cleanupContent($h2Node->nodeValue));
							}
							break;

						case 'Nationalité':
						case 'Nationalités': // C.S. Lee
							$node->removeChild($spanNode);
							$value = $this->cleanupContent($node->nodeValue);
							if ($value !== 'Indéfini' && $value !== 'Indéfinie')
							{
								$this->specificSetValue('nationalities', $value);
							}
							break;

						case 'Naissance': // Bill McKinney
							foreach ($node->getElementsByTagName('strong') as $aNode)
							{
								$birthDate = $this->getDate('j F Y',$this->replaceMonths($this->cleanupContent($aNode->nodeValue)));
								if ($birthDate)
								{
									$this->specificSetValue('birthDate', $birthDate);
								}
							}
							break;

						case 'Décès': // Bill McKinney
							foreach ($node->getElementsByTagName('strong') as $aNode)
							{
								$deathDate = $this->getDate('j F Y',$this->replaceMonths($this->cleanupContent($aNode->nodeValue)));
								if ($deathDate)
								{
									$this->specificSetValue('deathDate', $deathDate);
								}
							}
							break;
					}
				}
			}
		}

		return true;
	}

	/**
	 * @param string $key
	 * @param string $value
	 */
	protected function specificSetValue($key, $value)
	{
		switch ($key)
		{
			case 'civilName':
				$this->data['contributor']['aliases'][] = [
					'label' => $value,
					'civilName' => true
				];
				break;

			case 'alias':
				foreach (\explode(',', $value) as $alias)
				{
					$this->data['contributor']['aliases'][] = [
						'label' => $this->cleanupContent($alias),
						'civilName' => false
					];
				}
				break;

			case 'nationalities':
				foreach (\explode(',', $value) as $nationality)
				{
					$this->data['contributor']['nationalities'][] = \Change\Stdlib\StringUtils::ucfirst(trim($nationality));
				}
				break;

			case 'birthDate':
				$this->data['contributor']['birthDate'] = $value;
				break;

			case 'deathDate':
				$this->data['contributor']['deathDate'] = $value;
				break;

			default:
				$this->data['contributor'][$key] = $value;
				break;
		}
	}
}