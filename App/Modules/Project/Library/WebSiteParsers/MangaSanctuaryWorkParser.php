<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\MangaSanctuaryWorkParser
 */
class MangaSanctuaryWorkParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$contentNode = $this->DOMDocument->getElementById('content');
		foreach ($contentNode->getElementsByTagName('h1') as $h1Node)
		{
			/** @var \DOMElement $h1Node */
			$this->data['work']['label'] = $this->cleanupContent($h1Node->nodeValue);
			$this->parseNames($h1Node->nextSibling);
		}

		$synopsisNode = $this->DOMDocument->getElementById('synopsis');
		if ($synopsisNode)
		{
			$synopsis = $synopsisNode->nodeValue;
			$synopsis = str_replace('Synopsis : ', '**Synopsis :** ', $synopsis);
			$this->appendToDescription('work', $synopsis);
		}

		$infosNode = $this->DOMDocument->getElementById('infos');
		$key = null;
		foreach ($infosNode->childNodes as $childNode)
		{
			/** @var \DOMElement $childNode */
			switch ($childNode->tagName)
			{
				case 'dt':
					$key = $this->getKeyByLabel($this->cleanupContent($childNode->nodeValue));
					break;

				case 'dd':
					if ($key)
					{
						$this->setValueSpecific($key, $childNode);
						$key = null;
					}
			}
		}
		return true;
	}

	/**
	 * @param \DOMNode $node
	 */
	protected function parseNames($node)
	{
		$countryCode = null;
		foreach ($node->childNodes as $childNode)
		{
			if ($childNode instanceof \DOMElement)
			{
				switch ($childNode->tagName)
				{
					case 'span':
						/** @var \DOMElement $childNode */
						foreach ($childNode->getElementsByTagName('img') as $imgNode)
						{
							/** @var \DOMElement $imgNode */
							$countryCode = $this->getCountryCode($imgNode->getAttribute('src'));
						}

						break;

					case 'dl':
						$this->parseNames($childNode);
						break;
				}
			}
			else
			{
				if ($countryCode)
				{
					$this->data['work']['aliases'][] = [
						'label' => $this->cleanupContent($childNode->nodeValue),
						'nationalities' => [$countryCode]
					];
					$countryCode = null;
				}
			}
		}
	}

	/**
	 * @param string $src
	 * @return string
	 */
	protected function getCountryCode($src)
	{
		switch ($src)
		{
			case '/design/img/flags/24.png':
				return 'BE';
			case '/design/img/flags/45.png':
				return 'CN';
			case '/design/img/flags/54.png':
				return 'KR';
			case '/design/img/flags/71.png':
				return 'US';
			case '/design/img/flags/77.png':
				return 'FR';
			case '/design/img/flags/112.png':
				return 'JP';
		}
		return null;
	}

	/**
	 * @param string $label
	 * @return null|string[]
	 */
	protected function getKeyByLabel($label)
	{
		if (preg_match('/(.*) :/', $label, $matches))
		{
			$label = $matches[1];
		}

		switch ($label)
		{
			case 'Année':
				return ['work', 'year'];
			case 'Catégories':
			case 'Genres':
				return ['work', 'genres'];
			case 'Public':
				return ['work', 'audience'];
			default:
				return ['*', $label];
		}
	}

	/**
	 * @param string[] $key
	 * @param \DOMNode $childNode
	 */
	protected function setValueSpecific($key, $childNode)
	{
		list($document, $property) = $key;
		if ($document === 'work')
		{
			$value = $this->cleanupContent($childNode->nodeValue);
			switch ($property)
			{
				case 'genres':
					foreach (explode(',', $value) as $genre)
					{
						if ($genre === 'Inconnue')
						{
							continue;
						}
						$this->data['work']['genres'][] = \Change\Stdlib\StringUtils::ucfirst(trim($genre));
					}
					break;

				case 'year':
					$this->data['work'][$property] = (int)$value;
					break;

				case 'audience':
					$this->data['work'][$property] = \preg_replace('/\D*(\d+)\D*/', 'audience_$1+', $value);
					break;

				default:
					$this->data['work'][$property] = $value;
					break;
			}
		}
		else
		{
			/** @var \DOMElement $a */
			foreach ($this->DOMXPath->query('a', $childNode) as $a)
			{
				// Name.
				$label = $this->cleanupContent($a->nodeValue);
				$parts = explode(' ', $label);
				foreach ($parts as &$part)
				{
					$part = \Change\Stdlib\StringUtils::ucfirst(\Change\Stdlib\StringUtils::toLower($part));
				}
				unset($part);
				$label = implode(' ', $parts);

				$contribution = [];
				$contribution['label'] = $label;
				$contribution['link'] = $a->getAttribute('href');
				$contribution['roleCode'] = $property;
				$this->updateContribution($contribution);

				if ($contribution['role'])
				{
					$this->data['contributions'][] = $contribution;
				}
			}
		}
	}
}