<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\MusicBrainzCDParser
 */
class MusicBrainzCDParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Exemples:
		// - Avec deux auteurs : https://musicbrainz.org/release/6a661fb2-a950-491d-bcbd-308aa6e24ca1

		$this->setTypology('work', 'musique');
		$this->setValue('work', 'status', null, 'ended');
		$this->setStringByXPath('work', 'label', null, '//h1');
		$this->setStringByXPath('work', 'originalLanguage', null, '//dd[@class=\'language\']');

		$this->setTypology('publication', 'support_audio');
		$this->setValue('publication', 'status', null, 'ended');
		$this->setValue('publication', 'oneShot', null, true);
		$this->setStringByXPath('publication', 'publisher', null, '//h2[@class=\'labels\']/following::ul/li/a');
		$this->setTypeSupport();
		$this->setStringByXPath('publication', 'attributes', 'detail_format', '//dd[@class=\'format\']');
		$this->setStringByXPath('publication', 'attributes', 'duree_totale', '//dd[@class=\'length\']');
		$this->setStringByXPath('publication', 'attributes', 'code_barre', '//dd[@class=\'barcode\']');
		$this->setStringByXPath('publication', 'attributes', 'type_album', '//dd[@class=\'type\']');
		$this->setStringByXPath('publication', 'attributes', 'numero_catalogue', '//span[@class=\'catalog-number\']');
		$this->setDateByXPath('publication', 'attributes', 'date_publication', '//span[@class=\'release-date\']', 'Y-m-j');
		$this->setStringByXPath('publication', 'attributes', 'id_amazon', '//*[@class=\'amazon-favicon\']');
		//$this->setValue('publication', 'attributes', 'liste_piste', $this->generatePlaylistMarkdown());

		$this->appendToDescription('publication', $this->generatePlaylistDescription());

		$this->setContributions();
		$this->setLinks();

		return true;
	}

	private function setTypeSupport()
	{
		$value = $this->getValueByXPath('//dd[@class=\'format\']');
		if ($value)
		{
			if (preg_match('/(.*)×(.*)/u', $value, $matches))
			{
				$this->setValue('publication', 'attributes', 'type_support_audio', $matches[2]);
			}
			else
			{
				$this->setValue('publication', 'attributes', 'type_support_audio', $value);
			}
		}
	}

	private function readPlaylist()
	{
		$playlist = [];
		foreach ($this->DOMXPath->query('//div[@id=\'content\']//table[@class=\'tbl medium\']') as $table)
		{
			// CD
			foreach ($this->DOMXPath->query('thead/tr/th/a[text()]', $table) as $node)
			{
				$cd = $this->cleanupContent(\trim(\str_replace('▼', '', $node->nodeValue)));
				if ($cd)
				{
					$playlist[$cd] = [];
					break;
				}
			}

			// PISTES
			foreach ($this->DOMXPath->query('tbody/tr[@class!=\'subh\']', $table) as $tr)
			{
				$pos = null;
				$titre = null;
				$lien = null;
				foreach ($this->DOMXPath->query('td[@class=\'pos t\']', $tr) as $node)
				{
					$pos = $this->cleanupContent($node->nodeValue);
					break;
				}
				foreach ($this->DOMXPath->query('td[@class=\'pos t\']/following::td/a', $tr) as $node)
				{
					$titre = $this->cleanupContent($node->nodeValue);
					$lien = $node->getAttribute('href');
					break;
				}
				if ($pos && $titre)
				{
					$playlist[$cd][$pos]['titre'] = $titre;
					$playlist[$cd][$pos]['lien'] = $lien;
				}
			}
		}
		return $playlist;
	}

	private function generatePlaylistMarkdown()
	{
		$markdown = '';
		$playlist = $this->readPlaylist();
		foreach ($playlist as $cd => $pistes)
		{
			$markdown .= '_' . $cd . '_' . "\n\n";
			foreach ($pistes as $pos => $piste)
			{
				$markdown .= $pos . '.' . ' [' . $piste['titre'] . '](' . $piste['lien'] . ')' . "\n";
			}
			$markdown .= "\n\n";
		}
		return $markdown;
	}

	private function generatePlaylistDescription()
	{
		$text = '';
		$playlist = $this->readPlaylist();
		foreach ($playlist as $cd => $pistes)
		{
			if ($text)
			{
				$text .= "\n";
			}

			$text .= $cd . "\n\n";
			foreach ($pistes as $pos => $piste)
			{
				$text .= $pos . '. ' . $piste['titre'] . "\n";
			}
		}
		return $text;
	}

	protected function setLinks()
	{
		/** @var \DOMElement $a */
		foreach ($this->DOMXPath->query("//ul[contains(@class, 'external_links')][position()=1]/li/a") as $a)
		{
			$url = $a->getAttribute('href');
			$this->addLinkByUrl($url);
		}
	}

	protected function setContributions()
	{
		/** @var \DOMElement $a */
		foreach ($this->DOMXPath->query("//p[contains(@class, 'subheader')]/a") as $a)
		{
			$contribution = [
				'link' => $a->getAttribute('href'),
				'label' => $this->cleanupContent($a->nodeValue),
				'roleCode' => 'Auteur'
			];
			$this->updateContribution($contribution);

			$this->data['contributions'][] = $contribution;
		}
	}
}