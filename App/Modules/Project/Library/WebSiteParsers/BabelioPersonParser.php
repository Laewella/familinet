<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\BabelioPersonParser
 */
class BabelioPersonParser extends \Project\Library\WebSiteParsers\AbstractParser
{

    /**
     * @return string
     */
    public static function getType()
    {
        return 'contributor';
    }

    /**
     * @return bool true if the parser only adds the link but don't parse anything in the page.
     */
    public static function isMinimal()
    {
        return false;
    }

    /**
     * @return boolean
     */
    protected function doParseData()
    {
        if (!$this->DOMDocument)
        {
            return false;
        }

        $this->data['contributor']['typology'] = 'person';

        $this->setStringByXPath('contributor', 'label', '', "//*[@itemprop='name']/a");
        $this->setDateByXPath('contributor', 'birthDate', '', "//*[@itemprop='birthDate']", 'j/m/Y');
        $this->setDateByXPath('contributor', 'deathDate', '', "//*[@itemprop='deathDate']", 'j/m/Y');

        $this->setNationality();

        $value = $this->getValueByXPath("//*[@id='d_bio']");
        $value = str_replace("\n\n\n\n", "\n\n", $value);
        $this->appendToDescription('contributor', $value);

        // Exemple date de décès : https://www.babelio.com/auteur/Charles-Baudelaire/2184

        return true;
    }

    /**
     * Remplit la nationnalité
     */
    protected function setNationality()
    {
        foreach ($this->DOMXPath->query('//div[@class=\'livre_resume\']') as $div)
        {
            foreach ($div->childNodes as $node)
            {
                if (!($node instanceof \DOMElement))
                {
                    $value = $this->cleanupContent($node->nodeValue);
                    if (preg_match('/Nationalité(\s*):(\s*)(\w+)/', $value, $matches))
                    {
                        $this->data['contributor']['nationalities'][] = \Change\Stdlib\StringUtils::ucfirst(trim($matches[3]));
                    }
                }
            }
        }
    }
}