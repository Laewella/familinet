<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocineMovieParser
 */
class AllocineMovieParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$this->setTypology('work', 'film');
		$this->setValue('work', 'status', null, 'ended');
		$this->appendToDescription('work', '**Synopsis :** '
			. $this->getValueByXPath('//section[@id=\'synopsis-details\']/div[@class=\'content-txt \'] | //section[@id=\'synopsis-details\']/div[@class=\'content-txt\']'));
		$this->setStringByXPath('work', 'label', null, '//div[' . $this->getFilter('titlebar-title', null) . ']');
		$this->setNumberByXPath('work', 'year', null,
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Année de production') . ']');

		$this->setStringByXPath('work', 'attributes', 'type_film',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Type de film') . ']', ['-']);
		$this->setStringByXPath('work', 'attributes', 'budget',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Budget') . ']', ['-']);
		$this->setStringByXPath('work', 'attributes', 'format_production',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Format production') . ']', ['-']);
		$this->setStringByXPath('work', 'attributes', 'mode_couleur',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Couleur') . ']', ['-']);
		$this->setStringByXPath('work', 'attributes', 'format_audio',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Format audio') . ']', ['-']);
		$this->setStringByXPath('work', 'attributes', 'format_projection',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Format de projection') . ']', ['-']);
		$this->setStringByXPath('work', 'attributes', 'visa',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'N° de Visa') . ']', ['-']);

		$this->setDateByXPath('work', 'attributes', 'date_sortie_VOD',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Date de sortie VOD') . ']', 'j/m/Y');
		$this->setDateByXPath('work', 'attributes', 'date_sortie_DVD',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Date de sortie DVD') . ']', 'j/m/Y');
		$this->setDateByXPath('work', 'attributes', 'date_sortie_BR',
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Date de sortie Blu-ray') . ']',
			'j/m/Y');
		$this->setDateByXPath('work', 'attributes', 'date_sortie_cinema',
			'//strong[' . $this->getFilterByBrother('span', 'light', 'Date de sortie') . ']', 'j F Y');
		$this->setDateByXPath('work', 'attributes', 'date_reprise',
			'//span[' . $this->getFilterByBrother('../span', 'light', 'Date de reprise') . ']', 'j F Y');

		$this->setAliases();
		$this->setPublicationDate();
		$this->setNationalities();
		$this->setGenres();
		$this->setDuree();
		$this->setOriginalLanguage();
		$this->setPublic();

		$this->setContributions();

		return true;
	}

	protected function setPublic()
	{
		$value = $this->getValueByXPath('//span[' . $this->getFilter('ovw-synopsis-certificate', null) . ']');
		if ($value)
		{
			$this->appendToDescription('work', $value);
		}
	}

	protected function setOriginalLanguage()
	{
		$value = $this->getValueByXPath(
			'//span[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Langues') . ']'
		);
		if (\substr_count($value, ','))
		{
			$this->appendToDescription('work', 'Langues originales : ' . $value);
		}
		else
		{
			$this->setValue('work', 'originalLanguage', null, $value);
		}
	}

	protected function setGenres()
	{
		foreach ($this->DOMXPath->query('//span[' . $this->getFilterByBrother('span', 'light', 'Genres') . ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			if ($value !== 'Genres')
			{
				$this->data['work']['genres'][] = $value;
			}
		}
		foreach ($this->DOMXPath->query('//span[' . $this->getFilterByBrother('span', 'light', 'Genre') . ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			if ($value !== 'Genre')
			{
				$this->data['work']['genres'][] = $value;
			}
		}
	}

	protected function setNationalities()
	{
		foreach ($this->DOMXPath->query('//span[' . $this->getFilterByBrother('span', 'light', 'Nationalités') . ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			if ($value !== 'Nationalités')
			{
				$this->data['work']['nationalities'][] = $value;
			}
		}
		foreach ($this->DOMXPath->query('//span[' . $this->getFilterByBrother('span', 'light', 'Nationalité') . ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			if ($value !== 'Nationalité' && $value !== 'Indéfini')
			{
				$this->data['work']['nationalities'][] = $value;
			}
		}
	}

	protected function setPublicationDate()
	{
		foreach ($this->DOMXPath->query('//span[' . $this->getFilterByBrother('span', 'light', 'Date de sortie') . ']') as $node)
		{
			$value = $this->cleanupContent($node->nodeValue);
			if ($value && $value !== 'Date de sortie')
			{
				$value = $this->replaceMonths($value);
				$value = \DateTime::createFromFormat('j F Y', $value);
				if ($value instanceof \DateTime)
				{
					$this->setValue('work', 'attributes', 'date_sortie_cinema', $value->format(\DateTime::ATOM));
					$this->setValue('work', 'year', null, (int)$value->format('Y'));
				}
			}
		}
	}

	protected function setAliases()
	{
		$alias = $this->getValueByXPath(
			'//h2[' . $this->getFilter('that', null) . ' and ' . $this->getFilterByBrother('span', 'what', 'Titre original') . ']'
		);
		if ($alias)
		{
			$this->data['work']['aliases'][] = [
				'label' => $alias,
				'nationalities' => $this->data['work']['nationalities'],
				'original' => true
			];
		}
	}

	protected function setDuree()
	{
		$duree = '';
		foreach ($this->DOMXPath->query('//span[' . $this->getFilter(null, 'Date de sortie') . ']/parent::*/child::text()') as $node)
		{
			$duree .= $this->cleanupContent($node->nodeValue);
		}
		$duree = str_replace(['(', ')'], '', $duree);

		$this->setValue('work', 'attributes', 'duree_cinema', $duree);
	}

	protected function setContributions()
	{
		$this->data['url'] = $this->url;
		if (preg_match('/.*\/film\/fichefilm_gen_cfilm=([0-9]+).html/', $this->url, $matches))
		{
			$contributions = [];
			$url = 'https://' . $this->webSite->getDomain() . '/film/fichefilm-' . $matches[1] . '/casting/';

			$parserObject = new AllocineCastingParser($this->documentManager, $this->documentCodeManager, $this->geoManager, $url, $this->webSite);
			if (!($parserObject instanceof \Project\Library\WebSiteParsers\AbstractParser))
			{
				throw new \RuntimeException('Invalid parser class: \Project\Library\WebSiteParsers\AllocineCastingParser', 999999);
			}

			$data = $parserObject->parseData();
			foreach ($data['contributions']['contributions'] as $contribution)
			{
				if (!$this->contributorExists($contribution, $contributions))
				{
					$contributions[] = $contribution;
				}
			}

			$this->data['debug'] = $data['contributions']['debug'];
			$this->data['contributions'] = array_values($contributions);
		}
	}

	protected function contributorExists($contribution, $array)
	{
		foreach ($array as $c)
		{
			if ($c['role'] == $contribution['role'] && $c['roleDetail'] == $contribution['roleDetail']
				&& ($c['link'] == $contribution['link'] || $c['label'] == $contribution['label']))
			{
				return true;
			}
		}
		return false;
	}
}