<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\DecitreWorkParser
 */
class DecitreWorkParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	protected $isSerie = false;

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Exemples:
		// - Plusieurs auteurs : https://www.decitre.fr/livres/ma-ville-interdite-9782862666143.html
		// - Traducteur : https://www.decitre.fr/livres/surprends-moi-9782714479228.html
		// - Les deux : https://www.decitre.fr/livres/love-and-the-city-9782266208482.html
		// - Plusieurs annotateurs : https://www.decitre.fr/livres/cyrano-de-bergerac-9782081408630.html

		// Description
		$value = null;
		foreach ($this->DOMXPath->query("//div[@id='resume']/div[@class='content']") as $node)
		{
			$value = $this->cleanupContent($node->nodeValue);
			break;
		}
		if ($value)
		{
			$this->appendToDescription('work', '**Résumé :** ' . $value);
		}

		$this->setWorkLabel();
		$this->setWorkContributions();

		if ($this->isSerie)
		{
			$this->setTypology('publication', 'serie_livres');
		}
		else
		{
			$this->setTypology('publication', 'livre');
			$this->data['work']['status'] = 'ended';
			$this->data['publication']['status'] = 'ended';
		}

		$key = null;
		$value = null;
		foreach ($this->DOMXPath->query("//li[contains(@class, 'information')]") as $li)
		{
			foreach ($li->childNodes as $node)
			{
				if (!$key)
				{
					$key = $this->getKeyByLabel($this->cleanupContent($node->nodeValue));
				}
				else
				{
					$value = $this->cleanupContent($node->nodeValue);
				}
				if ($key && $value)
				{
					$this->setValueSpecific($key, $value);
					$key = null;
					$value = null;
				}
			}
		}

		$this->setContributions();

		return true;
	}

	protected function setWorkLabel()
	{
		$value = null;
		foreach ($this->DOMXPath->query('//h1[@class=\'product-title\']') as $h1)
		{
			foreach ($h1->childNodes as $node)
			{
				$value = $this->cleanupContent($node->nodeValue);
				break;
			}
		}
		if ($value)
		{
			if (preg_match('/.*( Tome \d+)/', $value, $matches))
			{
				$this->isSerie = true;
				$value = str_replace($matches[1], '', $value);
			}
			$this->data['work']['label'] = $value;
		}

		$value = null;
		foreach ($this->DOMXPath->query('//h1[@class=\'product-title\']/following-sibling::h2') as $h2)
		{
			if (!$h2->getAttribute('class'))
			{
				$value = $this->cleanupContent($h2->nodeValue);
			}
			break;
		}

		if ($value)
		{
			$this->data['work']['subtitle'] = $value;
		}
	}

	protected function setWorkContributions()
	{
		foreach ($this->DOMXPath->query("//a[contains(@class, 'author')]") as $node)
		{
			$value = $this->cleanupContent($node->nodeValue);
			if ($value)
			{
				$this->data['work']['contributions'][$value] = ['Auteur'];
			}
		}
		foreach ($this->DOMXPath->query('//div[@class=\'secondary-authors\']') as $div)
		{
			$individu = null;
			foreach ($div->childNodes as $node)
			{
				if ($node instanceOf \DOMElement)
				{
					switch ($node->tagName)
					{
						case 'h3':
							$individu = $this->cleanupContent($node->nodeValue);
							if ($individu === 'Collectif')
							{
								$individu = null;
							}
							else
							{
								$this->data['work']['contributions'][$individu] = ['Auteur'];
							}
							break;
						case 'h4':
							if ($individu)
							{
								$role = $this->cleanupContent($node->nodeValue);
								$role = \substr($role, 1, -1);
								$this->data['work']['contributions'][$individu] = [$role];
							}
							break;
					}
				}
			}
		}
	}

	/**
	 * @param string $label
	 * @return null|string[]
	 */
	protected function getKeyByLabel($label)
	{
		switch ($label)
		{
			case 'Date de parution':
				return $this->isSerie ? null : ['publication', 'attributes', 'date_publication'];
			case 'Editeur':
				return ['publication', 'publisher', null];
			case 'Collection':
				return ['publication', 'collection', null];
			case 'ISBN':
				return $this->isSerie ? null : ['publication', 'attributes', 'isbn'];
			case 'EAN':
				return $this->isSerie ? null : ['publication', 'attributes', 'ean'];
			case 'Nb. de pages':
				return $this->isSerie ? null : ['publication', 'attributes', 'nb_pages'];
			case 'Poids':
				return $this->isSerie ? null : ['publication', 'attributes', 'poids'];
			case 'Dimensions':
				return $this->isSerie ? null : ['publication', 'attributes', 'dimensions'];
			case 'Format':
				return ['publication', 'attributes', 'format'];
			default:
				return null;
		}
	}

	/**
	 * @param string[] $key
	 * @param string|int|null $value
	 */
	protected function setValueSpecific($key, $value)
	{
		if ($key === null)
		{
			return;
		}
		list($document, $property, $attribute) = $key;

		if ($property === 'attributes')
		{
			if ($attribute === 'date_publication')
			{
				$value = $this->getDate('j/m/Y', $value);
			}
			if ($attribute === 'nb_pages')
			{
				$value = $this->getInteger($value);
			}
		}

		if ($attribute)
		{
			$this->data[$document][$property][$attribute] = $value;
		}
		else
		{
			$this->data[$document][$property] = $value;
		}
	}

	/**
	 * @param string $object
	 * @param string $typologyCode
	 */
	protected function setTypology($object, $typologyCode)
	{
		$this->data[$object]['typology'] = $typologyCode;
	}

	protected function setContributions()
	{
		$this->data['contributions'] = [];

		/** @var \DOMElement $div */
		foreach ($this->DOMXPath->query("//div[contains(@class, 'authors-container')]/div") as $div)
		{
			$contribution = null;
			if (strpos($div->getAttribute('class'), 'authors--main') !== false)
			{
				/** @var \DOMNode $child */
				foreach ($div->childNodes as $child)
				{
					$contribution = $this->initializeContribution($child);
					if (!empty(contribution))
					{
						$contribution['roleCode'] = "Auteur";
						$this->saveContribution($contribution);
					}
				}
			}
			elseif (strpos($div->getAttribute('class'), 'authors') !== false)
			{
				/** @var \DOMNode $child */
				foreach ($div->childNodes as $child)
				{
					if ($child instanceof \DOMElement)
					{
						switch ($child->tagName)
						{
							case 'h3':
								$contribution = $this->initializeContribution($child);
								break;
							case 'h4':
								if (!empty($contribution))
								{
									$role = $this->cleanupContent($child->nodeValue);
									if (preg_match('/\((.*)\)/', $role, $matches))
									{
										$role = $matches[1];
									}
									$contribution['roleCode'] = $role;
								}
								break;
						}
					}
					elseif ($this->cleanupContent($child->nodeValue) == ",")
					{
						$this->saveContribution($contribution);
						$contribution = null;
					}
				}
			}

			$this->saveContribution($contribution);
		}
	}

	/**
	 * @param \DOMNode $container
	 * @return array|null
	 */
	protected function initializeContribution($container)
	{
		$contribution = [];
		if ($container instanceof \DOMElement && $container->hasChildNodes())
		{
			$contribution['label'] = $this->cleanupContent($container->nodeValue);

			/** @var \DOMElement $child */
			foreach ($this->DOMXPath->query("a", $container) as $a)
			{
				$contribution['link'] = $a->getAttribute('href');
			}
		}
		return $contribution;
	}

	/**
	 * @param array $contribution
	 */
	protected function saveContribution($contribution)
	{
		if (!empty($contribution))
		{
			$this->updateContribution($contribution);
			if ($contribution['role'])
			{
				$this->data['contributions'][] = $contribution;
			}
		}
	}
}