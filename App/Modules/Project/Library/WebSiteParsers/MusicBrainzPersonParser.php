<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\MusicBrainzPersonParser
 */
class MusicBrainzPersonParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Exemples:
		// - Avec date de décès : https://musicbrainz.org/release/78f6e111-d8cc-3f37-ba58-de5988c9c5a0
		// - Groupe : https://musicbrainz.org/artist/3c18062e-27bd-4532-8a91-9889477a3533

		$this->setStringByXPath('contributor', 'label', '', "//div[contains(@class, 'artistheader')]/h1");
		$this->setInfos();
		$this->setLinks();

		return true;
	}

	protected function setInfos()
	{
		$key = null;
		/** @var \DOMElement $child */
		foreach ($this->DOMXPath->query("//div[@id='sidebar']//dl[contains(@class, 'properties')]/*") as $child)
		{
			switch ($child->tagName)
			{
				case 'dt':
					$title = \strtolower(\trim($child->nodeValue, "\t\n\r\0\x0B:"));

					switch ($title)
					{
						case 'type':
							$key = ['typology', ''];
							break;
						case 'gender':
						case 'genre':
							$key = ['attributes', 'sex'];
							break;
						case 'born':
						case 'date de naissance':
							$key = ['birthDate', ''];
							break;
						case 'died':
						case 'date de décès':
							$key = ['deathDate', ''];
							break;
					}
					break;

				case 'dd':
					if ($key)
					{
						$value = $this->cleanupContent($child->nodeValue);
						list($property, $subProperty) = $key;

						if ($property == 'attributes' && $subProperty == 'sex')
						{
							switch ($value)
							{
								case 'Female':
									$value = 'f';
									break;
								case 'Male':
									$value = 'h';
									break;
								default:
									$value = null;
									break;
							}
						}
						elseif ($property == 'birthDate' || $property == 'deathDate')
						{
							if (\preg_match('/.*([0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])).*/', $value, $matches))
							{
								$value = $matches[1];
							}
							else
							{
								$value = null;
							}
						}

						if ($value)
						{
							if ($subProperty)
							{
								$this->data['contributor'][$property][$subProperty] = $value;
							}
							else
							{
								$this->data['contributor'][$property] = $value;
							}
						}
					}
					$key = null;
					break;
			}
		}
	}

	protected function setLinks()
	{
		/** @var \DOMElement $a */
		foreach ($this->DOMXPath->query("//ul[contains(@class, 'external_links')]/li/a") as $a)
		{
			$url = $a->getAttribute('href');
			$this->addLinkByUrl($url);
		}

		/** @var \DOMElement $a */
		foreach ($this->DOMXPath->query("//dd[contains(@class, 'isni-code')]/a") as $a)
		{
			$url = $a->getAttribute('href');
			$this->addLinkByUrl($url);
		}

		/** @var \DOMElement $a */
		foreach ($this->DOMXPath->query("//div[contains(@class, 'wikipedia-extract')]/a") as $a)
		{
			$url = $a->getAttribute('href');
			$this->addLinkByUrl($url);
		}
	}
}