<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocineSerieParser
 */
class AllocineSerieParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument, $this->data);

		$this->setStatus();
		$this->setMetaInfos();
		$this->setNationalities();
		$this->setAliases();

		$this->setContributions();

		return true;
	}

	protected function setMetaInfos()
	{
		$value = $this->getValueByXPath('//div[' . $this->getFilter('meta-body-info', null) . ']');
		list($dates, $episodeDuration,) = \explode('/', $value);
		if (\preg_match('/(\d{4}) - (\d{4})/', $dates, $matches))
		{
			$this->setValue('work', 'year', null, (int)$matches[1]);
			$this->setValue('work', 'endYear', null, (int)$matches[2]);
		}
		elseif (\preg_match('/(\d{4})/', $dates, $matches))
		{
			$this->setValue('work', 'year', null, (int)$matches[1]);
		}
		if ($episodeDuration)
		{
			$this->setValue('work', 'attributes', 'duree_episode', $episodeDuration);
		}
	}

	protected function setStatus()
	{
		$value = $this->getValueByXPath('//div[' . $this->getFilter('label-status', null) . ']');
		if ($value === 'En cours')
		{
			$this->setValue('work', 'status', null, 'ongoing');
		}
		elseif ($value === 'Terminée') // http://www.allocine.fr/series/ficheserie_gen_cserie=144.html
		{
			$this->setValue('work', 'status', null, 'ended');
		}
		elseif ($value === 'Annulée') // http://www.allocine.fr/series/ficheserie_gen_cserie=18468.html
		{
			$this->setValue('work', 'status', null, 'canceled');
		}
	}

	protected function setNationalities()
	{
		foreach ($this->DOMXPath->query('//*[' . $this->getFilter('meta-body-nationality', null) . ']/*[' . $this->getFilter('nationality', null)
			. ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			if ($value !== 'Indéfini')
			{
				$this->data['work']['nationalities'][] = $value;
			}
		}
	}

	protected function setAliases()
	{
		$alias = $this->getValueByXPath('//div[' . $this->getFilter('meta-body-original-title', null) . ']/strong');
		if ($alias)
		{
			$this->data['work']['aliases'][] = [
				'label' => $alias,
				'nationalities' => $this->data['work']['nationalities'],
				'original' => true
			];
		}
	}

	protected function setContributions()
	{
		$contributions = [];

		$urls = [];
		foreach ($this->DOMXPath->query("//section[@id='synopsis-details']//a[contains(@class, 'end-section-link')]") as $node)
		{
			$urlSaison = $node->getAttribute('href');
			if (preg_match('/(\/series\/ficheserie-[0-9]*\/)(saison-[0-9]*\/)/', $urlSaison, $matches))
			{
				$urls[] = 'https://' . $this->webSite->getDomain() . $matches[1] . 'casting/' . $matches[2];
			}
		}
		foreach ($urls as $url)
		{
			$parserObject = new AllocineCastingParser($this->documentManager, $this->documentCodeManager, $this->geoManager, $url, $this->webSite);
			if (!($parserObject instanceof \Project\Library\WebSiteParsers\AbstractParser))
			{
				throw new \RuntimeException('Invalid parser class: \Project\Library\WebSiteParsers\AllocineSerieCastingParser', 999999);
			}

			$data = $parserObject->parseData();
			foreach ($data['contributions']['contributions'] as $contribution)
			{
				if (!$this->contributorExists($contribution, $contributions))
				{
					$contributions[] = $contribution;
				}
			}
		}

		$this->data['contributions'] = array_values($contributions);
	}

	protected function contributorExists($contribution, $array)
	{
		foreach ($array as $c)
		{
			if ($c['role'] == $contribution['role'] && $c['roleDetail'] == $contribution['roleDetail']
				&& ($c['link'] == $contribution['link'] || $c['label'] == $contribution['label']))
			{
				return true;
			}
		}
		return false;
	}
}