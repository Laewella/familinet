<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\PersonParser
 */
class PersonParser extends \Project\Library\WebSiteParsers\JsonLd\SchemaParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Model\ContributorModel
	 */
	protected function doParse(array $jsonData)
	{
		$model = new \Project\Library\WebSiteParsers\Model\ContributorModel();

		$model->setTypology('person');
		$model->setLabel($jsonData['name'] ?? null);
		$model->setImage($jsonData['image'] ?? null);
		$model->setBirthDate($jsonData['birthDate'] ?? null);
		$model->setDeathDate($jsonData['deathDate'] ?? null);

		$description = $jsonData['description'] ?? null;
		if ($description)
		{
			while (\strpos($description, '&nbsp;') !== false || \strpos($description, '&amp;') !== false)
			{
				$description = \html_entity_decode($description);
			}
			$model->setDescription($description);
		}

		return $model;
	}
}