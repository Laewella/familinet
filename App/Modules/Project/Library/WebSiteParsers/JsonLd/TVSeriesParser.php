<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\TVSeriesParser
 */
class TVSeriesParser extends \Project\Library\WebSiteParsers\JsonLd\SchemaParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Model\WorkModel
	 */
	protected function doParse(array $jsonData)
	{
		$model = new \Project\Library\WebSiteParsers\Model\WorkModel();

		$model->setTypology('serietv');
		$model->setLabel($jsonData['name'] ?? null);
		$model->setGenres($jsonData['genre'] ?? null);
		$model->setImage($jsonData['image'] ?? null);
		$model->setYear($this->parseYearFromDate($jsonData['startYear'] ?? null));
		$model->setEndYear($this->parseYearFromDate($jsonData['endYear'] ?? null));
		$model->setNumberOfEpisodes($jsonData['numberOfEpisodes'] ?? null);
		$model->setNumberOfSeasons($jsonData['numberOfSeasons'] ?? null);

		$description = $jsonData['description'] ?? null;
		if ($description)
		{
			while (\strpos($description, '&nbsp;') !== false || \strpos($description, '&amp;') !== false)
			{
				$description = \html_entity_decode($description);
			}
			$model->setDescription('**Synopsis :** ' . $description);
		}

		return $model;
	}
}