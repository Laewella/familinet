<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\SchemaParser
 */
abstract class SchemaParser
{
	/**
	 * @param string $json
	 * @param array $data
	 * @return bool
	 */
	public static function parse(string $json, array &$data)
	{
		$jsonData = \json_decode($json, true);
		switch ($jsonData['@type'] ?? null)
		{
			case 'TVSeries' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\TVSeriesParser($data);
				$data['work'] = \array_merge_recursive($parser->doParse($jsonData)->toArray(), $data['work'] ?? []);
				return true;

			case 'Person' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\PersonParser($data);
				$data['contributor'] = \array_merge_recursive($parser->doParse($jsonData)->toArray(), $data['contributor'] ?? []);
				return true;

			default :
				return false;
		}
	}

	/**
	 * @param \DOMDocument $DOMDocument
	 * @param array $data
	 * @return int
	 */
	public static function parseAll(\DOMDocument $DOMDocument, array &$data)
	{
		$parsedSchemas = 0;
		/** @var \DOMElement $script */
		foreach ($DOMDocument->getElementsByTagName('script') as $script)
		{
			if (($script->getAttribute('type') === 'application/ld+json') && self::parse($script->textContent, $data))
			{
				$parsedSchemas++;
			}
		}
		return $parsedSchemas;
	}

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @param array $data
	 */
	protected function __construct(array &$data)
	{
		$this->data = $data;
	}

	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Model\AbstractModel
	 */
	abstract protected function doParse(array $jsonData);

	/**
	 * @param mixed $value
	 * @param string $objectName
	 * @param string $propertyName
	 * @param string|null $subPropertyName
	 */
	protected function addValueIfSet($value, string $objectName, string $propertyName, string $subPropertyName = null)
	{
		if (isset($value))
		{
			if ($subPropertyName)
			{
				$this->data[$objectName][$propertyName][$subPropertyName] = $value;
			}
			else
			{
				$this->data[$objectName][$propertyName] = $value;
			}
		}
	}

	/**
	 * @param string|null $dateString
	 * @return string|null
	 * @throws \Exception
	 */
	protected function parseYearFromDate($dateString)
	{
		if ($dateString)
		{
			return (new \DateTime($dateString))->format('Y');
		}
		return null;
	}
}