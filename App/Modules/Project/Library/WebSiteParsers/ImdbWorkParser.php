<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\ImdbWorkParser
 */
class ImdbWorkParser extends \Project\Library\WebSiteParsers\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$this->setStringByXPath('work', 'label', null, '//h1[@itemprop=\'name\']/text()');
		$this->setNumberByXPath('work', 'year', null, '//h1[@itemprop=\'name\']/span[@id=\'titleYear\']');
		$this->appendToDescription('work', '**Synopsis :** ' . $this->getValueByXPath('//div[@itemprop=\'description\']'));
		$this->setGenres();
		$this->setNationalities();
		$this->setLanguages();
		$this->appendToDescription('work', 'Date de sortie : ' 
			. $this->getValueByXPath('//div[@id=\'titleDetails\']/div/h4[' . $this->getFilter('inline', 'Release Date:') . ']/../text()[2]'));
		$this->setOriginalTitle();
		

		
		$typology = $this->getTypology();

		// Séries TV
		if ($typology == 'serietv')
		{
			$this->setTypology('work', 'serietv');
			$this->setStringByXPath('work', 'attributes', 'duree_episode', '//time[@itemprop=\'duration\']');
		}
		// Les autres : des films
		else
		{
			$this->setTypology('work', 'film');
			$this->setStringByXPath('work', 'attributes', 'duree_cinema', '//time[@itemprop=\'duration\']');
		}

		return true;
	}

	protected function getTypology()
	{
		// Si c'est une série, on a "TV Series" écrit dans le lien qui a pour titre "See more release dates"
		$value = $this->getValueByXPath('//div[' . $this->getFilter('title_wrapper', null) . ']//div[' . $this->getFilter('subtext', null) . ']' 
			. '//a[@title=\'See more release dates\']');
		if ($value && strpos($value, 'TV Series') !== false)
		{
			return 'serietv';
		}
		return null;
	}

	protected function setGenres()
	{
		foreach ($this->DOMXPath->query('//span[@itemprop=\'genre\']') as $node)
		{
			$this->data['work']['genres'][] = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
		}
	}

	protected function setNationalities()
	{	
		foreach ($this->DOMXPath->query('//div[@id=\'titleDetails\']/div/a[' . $this->getFilterByBrother('h4', 'inline', 'Country:') . ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			if ($value !== 'Indéfini')
			{
				$this->data['work']['nationalities'][] = $value;
			}
		}
	}

	protected function setLanguages()
	{	
		foreach ($this->DOMXPath->query('//div[@id=\'titleDetails\']/div/a[' . $this->getFilterByBrother('h4', 'inline', 'Language:') . ']') as $node)
		{
			$value = \Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($node->nodeValue));
			$this->setValue('work', 'originalLanguage', null, $value);
			break;
		}
	}

	protected function setOriginalTitle()
	{
		$alias = $this->getValueByXPath('//div[' . $this->getFilter('originalTitle', null) . ']/text()');
		if ($alias)
		{
			$this->data['work']['aliases'][] = [
				'label' => $alias,
				'nationalities' => $this->data['work']['nationalities'],
				'original' => true
			];
		}
	}
}