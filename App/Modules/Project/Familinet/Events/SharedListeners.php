<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Events;

/**
 * @name \Project\Familinet\Events\SharedListeners
 */
class SharedListeners
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$events->attach('Application', 'setServices', function($event) {
			if ($event instanceof \Change\Events\Event)
			{
				$services = new \Project\Familinet\FamilinetServices($event->getApplication(), $event->getApplicationServices());
				$event->getServices()->set('Project_FamilinetServices', $services);
			}
			return true;
		}, 9990);
	}
}