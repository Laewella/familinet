<?php
/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Events\ModelManager;

/**
 * @name \Project\Familinet\Events\ModelManager\ModelManager
 */
class ModelManager
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function getRestriction(\Change\Events\Event $event)
	{
		if ($event->getParam('restriction'))
		{
			return;
		}

		$filter = $event->getParam('filter');
		if (!\is_array($filter) || !isset($filter['parameters']) || !\is_array($filter['parameters']))
		{
			return;
		}

		$parameters = $filter['parameters'];
		if (!isset($parameters['operator'], $parameters['propertyName']) || !\strpos($parameters['propertyName'], '.'))
		{
			return;
		}

		$parts = \explode('.', $parameters['propertyName']);
		if (\count($parts) !== 2)
		{
			return;
		}

		$documentQuery = $event->getParam('documentQuery');
		if (!($documentQuery instanceof \Change\Documents\Query\Query))
		{
			return;
		}

		$property = $documentQuery->getModel()->getProperty($parts[0]);
		if (!$property || !$property->getDocumentType())
		{
			$event->getApplication()->getLogging()->warn('Invalid property name:', $parts[0]);
			return;
		}

		$subModel = $event->getApplicationServices()->getModelManager()->getModelByName($property->getDocumentType());
		if (!$subModel)
		{
			$event->getApplication()->getLogging()->warn('Invalid model name:', $property->getDocumentType(), ', in property:', $parts[0]);
			return;
		}

		$subProperty = $subModel->getProperty($parts[1]);
		if (!$subProperty)
		{
			$event->getApplication()->getLogging()->warn('Invalid sub property name:', $parts[1], ', in model:', $subModel->getName());
			return;
		}

		$subQuery = $documentQuery->getPropertyBuilder($property->getName());
		$restriction = $this->getSubRestriction($subQuery, $subProperty, $parameters);
		if ($restriction)
		{
			$event->setParam('restriction', $restriction);
		}
	}

	/**
	 * @param \Change\Documents\Query\ChildBuilder $childQuery
	 * @param \Change\Documents\Property $property
	 * @param array $parameters
	 * @return \Change\Db\Query\Predicates\InterfacePredicate|null
	 */
	protected function getSubRestriction($childQuery, $property, array $parameters)
	{
		$predicateBuilder = $childQuery->getPredicateBuilder();
		if ($property->getName() === 'LCID')
		{
			if (isset($parameters['value']))
			{
				$childQuery->setLCID($parameters['value']);
				return $predicateBuilder->eq($property, $parameters['value']);
			}
		}
		else
		{
			switch ($parameters['operator'])
			{
				case 'isNull':
					return $predicateBuilder->isNull($property);
				case 'isNotNull':
					return $predicateBuilder->isNotNull($property);
			}
			if (isset($parameters['value']))
			{
				switch ($parameters['operator'])
				{
					case 'eq':
						return $predicateBuilder->eq($property, $parameters['value']);
					case 'neq':
						return $predicateBuilder->neq($property, $parameters['value']);
					case 'lte':
						return $predicateBuilder->lte($property, $parameters['value']);
					case 'lt':
						return $predicateBuilder->lt($property, $parameters['value']);
					case 'gte':
						return $predicateBuilder->gte($property, $parameters['value']);
					case 'gt':
						return $predicateBuilder->gt($property, $parameters['value']);
					case 'contains':
						return $predicateBuilder->like($property, $parameters['value'],
							\Change\Db\Query\Predicates\Like::ANYWHERE);
					case 'beginsWith':
						return $predicateBuilder->like($property, $parameters['value'],
							\Change\Db\Query\Predicates\Like::BEGIN);
					case 'endsWith':
						return $predicateBuilder->like($property, $parameters['value'],
							\Change\Db\Query\Predicates\Like::END);
				}
			}
		}
		return null;
	}
} 