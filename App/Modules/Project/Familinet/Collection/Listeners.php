<?php
/**
 * Copyright (C) 2014 Laewella
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Collection;

/**
 * @name \Project\Familinet\Collection\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			switch ($event->getParam('code'))
			{
				case 'Project_Familinet_Collection_IconNames':
					(new \Project\Familinet\Collection\Collections())->addIconNames($event);
					break;
				case 'Project_Familinet_Collection_TextClasses':
					(new \Project\Familinet\Collection\Collections())->addTextClasses($event);
					break;
			}
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_COLLECTION, $callback, 10);

		$callback = function (\Change\Events\Event $event)
		{
			$codes = $event->getParam('codes', array());
			$codes = array_merge($codes, array(
				'Project_Familinet_Collection_IconNames',
				'Project_Familinet_Collection_TextClasses'
			));
			$event->setParam('codes', $codes);
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_CODES, $callback, 1);
	}
}