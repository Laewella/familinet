<?php
/**
 * Copyright (C) 2014 Laewella
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Collection;

use Change\I18n\I18nString;

/**
 * @name \Project\Familinet\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addIconNames(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = array(
								'adjust' => 'adjust',
				'align-center' => 'align-center',
				'align-justify' => 'align-justify',
				'align-left' => 'align-left',
				'align-right' => 'align-right',
				'arrow-down' => 'arrow-down',
				'arrow-left' => 'arrow-left',
				'arrow-right' => 'arrow-right',
				'arrow-up' => 'arrow-up',
				'asterisk' => 'asterisk',
				'backward' => 'backward',
				'ban-circle' => 'ban-circle',
				'barcode' => 'barcode',
				'bell' => 'bell',
				'bold' => 'bold',
				'book' => 'book',
				'bookmark' => 'bookmark',
				'briefcase' => 'briefcase',
				'bullhorn' => 'bullhorn',
				'calendar' => 'calendar',
				'camera' => 'camera',
				'certificate' => 'certificate',
				'check' => 'check',
				'chevron-down' => 'chevron-down',
				'chevron-left' => 'chevron-left',
				'chevron-right' => 'chevron-right',
				'chevron-up' => 'chevron-up',
				'circle-arrow-down' => 'circle-arrow-down',
				'circle-arrow-left' => 'circle-arrow-left',
				'circle-arrow-right' => 'circle-arrow-right',
				'circle-arrow-up' => 'circle-arrow-up',
				'cloud' => 'cloud',
				'cloud-download' => 'cloud-download',
				'cloud-upload' => 'cloud-upload',
				'cog' => 'cog',
				'collapse-down' => 'collapse-down',
				'collapse-up' => 'collapse-up',
				'comment' => 'comment',
				'compressed' => 'compressed',
				'copyright-mark' => 'copyright-mark',
				'credit-card' => 'credit-card',
				'cutlery' => 'cutlery',
				'dashboard' => 'dashboard',
				'download' => 'download',
				'download-alt' => 'download-alt',
				'earphone' => 'earphone',
				'edit' => 'edit',
				'eject' => 'eject',
				'envelope' => 'envelope',
				'euro' => 'euro',
				'exclamation-sign' => 'exclamation-sign',
				'expand' => 'expand',
				'export' => 'export',
				'eye-close' => 'eye-close',
				'eye-open' => 'eye-open',
				'facetime-video' => 'facetime-video',
				'fast-backward' => 'fast-backward',
				'fast-forward' => 'fast-forward',
				'file' => 'file',
				'film' => 'film',
				'filter' => 'filter',
				'fire' => 'fire',
				'flag' => 'flag',
				'flash' => 'flash',
				'floppy-disk' => 'floppy-disk',
				'floppy-open' => 'floppy-open',
				'floppy-remove' => 'floppy-remove',
				'floppy-save' => 'floppy-save',
				'floppy-saved' => 'floppy-saved',
				'folder-close' => 'folder-close',
				'folder-open' => 'folder-open',
				'font' => 'font',
				'forward' => 'forward',
				'fullscreen' => 'fullscreen',
				'gbp' => 'gbp',
				'gift' => 'gift',
				'glass' => 'glass',
				'globe' => 'globe',
				'hand-down' => 'hand-down',
				'hand-left' => 'hand-left',
				'hand-right' => 'hand-right',
				'hand-up' => 'hand-up',
				'hd-video' => 'hd-video',
				'hdd' => 'hdd',
				'header' => 'header',
				'headphones' => 'headphones',
				'heart' => 'heart',
				'heart-empty' => 'heart-empty',
				'home' => 'home',
				'import' => 'import',
				'inbox' => 'inbox',
				'indent-left' => 'indent-left',
				'indent-right' => 'indent-right',
				'info-sign' => 'info-sign',
				'italic' => 'italic',
				'leaf' => 'leaf',
				'link' => 'link',
				'list' => 'list',
				'list-alt' => 'list-alt',
				'lock' => 'lock',
				'log-in' => 'log-in',
				'log-out' => 'log-out',
				'magnet' => 'magnet',
				'map-marker' => 'map-marker',
				'minus' => 'minus',
				'minus-sign' => 'minus-sign',
				'move' => 'move',
				'music' => 'music',
				'new-window' => 'new-window',
				'off' => 'off',
				'ok' => 'ok',
				'ok-circle' => 'ok-circle',
				'ok-sign' => 'ok-sign',
				'open' => 'open',
				'paperclip' => 'paperclip',
				'pause' => 'pause',
				'pencil' => 'pencil',
				'phone' => 'phone',
				'phone-alt' => 'phone-alt',
				'picture' => 'picture',
				'plane' => 'plane',
				'play' => 'play',
				'play-circle' => 'play-circle',
				'plus' => 'plus',
				'plus-sign' => 'plus-sign',
				'print' => 'print',
				'pushpin' => 'pushpin',
				'qrcode' => 'qrcode',
				'question-sign' => 'question-sign',
				'random' => 'random',
				'record' => 'record',
				'refresh' => 'refresh',
				'registration-mark' => 'registration-mark',
				'remove' => 'remove',
				'remove-circle' => 'remove-circle',
				'remove-sign' => 'remove-sign',
				'repeat' => 'repeat',
				'resize-full' => 'resize-full',
				'resize-horizontal' => 'resize-horizontal',
				'resize-small' => 'resize-small',
				'resize-vertical' => 'resize-vertical',
				'retweet' => 'retweet',
				'road' => 'road',
				'save' => 'save',
				'saved' => 'saved',
				'screenshot' => 'screenshot',
				'sd-video' => 'sd-video',
				'search' => 'search',
				'send' => 'send',
				'share' => 'share',
				'share-alt' => 'share-alt',
				'shopping-cart' => 'shopping-cart',
				'signal' => 'signal',
				'sort' => 'sort',
				'sort-by-alphabet' => 'sort-by-alphabet',
				'sort-by-alphabet-alt' => 'sort-by-alphabet-alt',
				'sort-by-attributes' => 'sort-by-attributes',
				'sort-by-attributes-alt' => 'sort-by-attributes-alt',
				'sort-by-order' => 'sort-by-order',
				'sort-by-order-alt' => 'sort-by-order-alt',
				'sound-5-1' => 'sound-5-1',
				'sound-6-1' => 'sound-6-1',
				'sound-7-1' => 'sound-7-1',
				'sound-dolby' => 'sound-dolby',
				'sound-stereo' => 'sound-stereo',
				'star' => 'star',
				'star-empty' => 'star-empty',
				'stats' => 'stats',
				'step-backward' => 'step-backward',
				'step-forward' => 'step-forward',
				'stop' => 'stop',
				'subtitles' => 'subtitles',
				'tag' => 'tag',
				'tags' => 'tags',
				'tasks' => 'tasks',
				'text-height' => 'text-height',
				'text-width' => 'text-width',
				'th' => 'th',
				'th-large' => 'th-large',
				'th-list' => 'th-list',
				'thumbs-down' => 'thumbs-down',
				'thumbs-up' => 'thumbs-up',
				'time' => 'time',
				'tint' => 'tint',
				'tower' => 'tower',
				'transfer' => 'transfer',
				'trash' => 'trash',
				'tree-conifer' => 'tree-conifer',
				'tree-deciduous' => 'tree-deciduous',
				'unchecked' => 'unchecked',
				'upload' => 'upload',
				'usd' => 'usd',
				'user' => 'user',
				'volume-down' => 'volume-down',
				'volume-off' => 'volume-off',
				'volume-up' => 'volume-up',
				'warning-sign' => 'warning-sign',
				'wrench' => 'wrench',
				'zoom-in' => 'zoom-in',
				'zoom-out' => 'zoom-out',
			);
			$collection = new \Change\Collection\CollectionArray('Project_Familinet_Collection_IconNames', $collection);
			$event->setParam('collection', $collection);
			$event->stopPropagation();
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addTextClasses(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = array(
				'text-muted' => 'text-muted',
				'text-primary' => 'text-primary',
				'text-success' => 'text-success',
				'text-info' => 'text-info',
				'text-warning' => 'text-warning',
				'text-danger' => 'text-danger'
			);
			$collection = new \Change\Collection\CollectionArray('Project_Familinet_Collection_TextClasses', $collection);
			$event->setParam('collection', $collection);
			$event->stopPropagation();
		}
	}
}



