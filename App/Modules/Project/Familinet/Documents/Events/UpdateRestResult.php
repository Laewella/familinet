<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Documents\Events;

/**
 * @name \Rbs\Admin\Documents\Events\UpdateRestResult
 */
class UpdateRestResult
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function executeCountry($event)
	{
		$restResult = $event->getParam('restResult');
		$country = $event->getDocument();
		if ($country instanceof \Rbs\Geo\Documents\Country && $restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->setProperty('code', $country->getCode());
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function executeTypology(\Change\Documents\Events\Event $event)
	{
		$restResult = $event->getParam('restResult');

		/** @var $document \Rbs\Generic\Documents\Typology */
		$document = $event->getDocument();
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$attributeDefinitions = $restResult->getProperty('attributesDefinitions');
			$groupDefinitions = $restResult->getProperty('groups');
			foreach ($document->getGroups() as $index => $group)
			{
				$groupDefinitions[$index]['title'] = $group->getCurrentLocalization()->getTitle();
				foreach ($group->getAttributes() as $attribute)
				{
					$attributeDefinitions[$attribute->getId()]['title'] = $attribute->getCurrentLocalization()->getTitle();
				}
			}
			$restResult->setProperty('groups', $groupDefinitions);
			$restResult->setProperty('attributesDefinitions', $attributeDefinitions);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function executeUser(\Change\Documents\Events\Event $event)
	{
		$restResult = $event->getParam('restResult');

		/** @var $document \Rbs\User\Documents\User */
		$document = $event->getDocument();
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult
			|| $restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($document);
			$profile = $event->getApplicationServices()->getProfileManager()->loadProfile($authenticatedUser, 'Rbs_Website');
			if ($profile)
			{
				$pseudonym = $profile->getPropertyValue('pseudonym');
				if ($pseudonym)
				{
					$restResult->setProperty('pseudonym', $pseudonym);
					$restResult->setProperty('label', $pseudonym);
				}
			}
		}
	}
}