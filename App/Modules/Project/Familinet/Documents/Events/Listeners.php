<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Documents\Events;

/**
 * @name \Project\Familinet\Documents\Events\Listeners
 */
class Listeners
{
	/**
	 * @var \Rbs\Generic\GenericServices
	 */
	protected $genericServices;

	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$callback = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				(new \Project\Familinet\Documents\Events\UpdateRestResult())->executeCountry($event);
			}
		};
		$events->attach('Rbs_Geo_Country', 'updateRestResult', $callback, 5);

		$callback = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				(new \Project\Familinet\Documents\Events\UpdateRestResult())->executeTypology($event);
			}
		};
		$events->attach('Rbs_Generic_Typology', 'updateRestResult', $callback, 5);

		$callback = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				(new \Project\Familinet\Documents\Events\UpdateRestResult())->executeUser($event);
			}
		};
		$events->attach('Rbs_User_User', 'updateRestResult', $callback, 5);
	}
}