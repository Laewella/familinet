<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet;

/**
 * @name \Project\Familinet\AttributeManager
 */
class AttributeManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'SearchManager';

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Project/Familinet/Events/AttributeManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('searchDocuments', function ($event) { $this->onQuerySearchDocuments($event); }, 5);
		$eventManager->attach('searchDocuments', function ($event) { $this->onDefaultSearchDocuments($event); }, 1);
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider($dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @var array
	 */
	protected $idsByNames = [];

	/**
	 * @param string[] $names
	 * @return int[]
	 */
	protected function getIdsByNames(array $names)
	{
		if (!$names)
		{
			return [];
		}

		$key = implode(',', $names);
		if (!\array_key_exists($key, $this->idsByNames))
		{
			$query = $this->documentManager->getNewQuery('Rbs_Generic_Attribute');
			$query->andPredicates($query->in('name', $names));
			/** @noinspection PhpIncompatibleReturnTypeInspection */
			$this->idsByNames[$key] = $query->getDocumentIds();
		}
		return $this->idsByNames[$key];
	}

	/**
	 * @param string[] $names
	 * @param int[] $documentIds
	 * @return string[]
	 */
	public function getValuesByCodesAndDocumentIds(array $names, array $documentIds)
	{
		if (!$names || !$documentIds)
		{
			return [];
		}

		$attributeIds = $this->getIdsByNames($names);
		if (!$attributeIds)
		{
			return [];
		}

		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select('string_value');
		$qb->from('rbs_generic_dat_attributes_index');
		$qb->where($fb->logicAnd($fb->isNotNull('string_value'), $fb->in('attribute_id', $attributeIds), $fb->in('document_id', $documentIds)));
		$query = $qb->query();
		return $query->getResults($query->getRowsConverter()->addStrCol('string_value')->singleColumn('string_value'));
	}
}