<?php
/**
 * Copyright (C) 20187 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Http\Rest;

/**
 * @name \Project\Familinet\Http\Rest\FamilinetResolver
 */
class FamilinetResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		return ['familinet.searchDocuments'];
	}

	/**
	 * Set Event params: resourcesActionName, documentId, LCID
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$nbParts = \count($resourceParts);
		if ($nbParts === 0 && $method === \Change\Http\Rest\Request::METHOD_GET)
		{
			array_unshift($resourceParts, 'projectFamilinet');
			$event->setParam('namespace', implode('.', $resourceParts));
			$event->setParam('resolver', $this);
			$action = function ($event)
			{
				$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
				$action->execute($event);
			};
			$event->setAction($action);
			$event->setAuthorization(null);
			return;
		}
		if ($nbParts === 1)
		{
			$actionName = $resourceParts[0];
			if ($actionName === 'searchDocuments')
			{
				$event->setAction(function ($event)
				{
					(new \Project\Familinet\Http\Rest\Actions\SearchDocuments())->execute($event);
				});
			}
		}
	}
}