<?php
/**
 * Copyright (C) 2014 Gaël PORT, Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Http\Rest\Actions;

/**
 * Returns the list of all the functions declared in the blocks.
 * Parameters:
 * - modelName (string, required)
 * - searchString (string, required)
 * - limit (integer, optional, default 10)
 * - columns (string[], optional, default [])
 * @name \Rbs\Admin\Http\Rest\Actions\SearchDocuments
 */
class SearchDocuments
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();
		$services = $event->getServices('Project_FamilinetServices');
		if (!($services instanceof \Project\Familinet\FamilinetServices))
		{
			throw new \RuntimeException('FamilinetServices not set', 999999);
		}
		$manager = $services->getSearchManager();

		$result = new \Change\Http\Rest\V1\CollectionResult();
		$modelName = $request->getPost('modelName', $request->getQuery('modelName'));
		$searchString = $request->getPost('searchString', $request->getQuery('searchString'));
		if (!$modelName || !$searchString)
		{
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_400);
			$event->setResult($result);
			return;
		}

		$limit = (int)$request->getPost('limit', $request->getQuery('limit', 10));
		$offset = (int)$request->getPost('offset', $request->getQuery('offset', 0));
		$data = $manager->searchDocuments($modelName, $searchString, \Change\Db\Query\Predicates\Like::ANYWHERE, $offset, $limit);
		if ($data['documents'])
		{
			/** @var \Change\Documents\AbstractDocument[] $documents */
			$documents = $data['documents'];
			$extraColumns = $event->getRequest()->getQuery('columns', []);
			$urlManager = $event->getUrlManager();
			foreach ($documents as $document)
			{
				$result->addResource(new \Change\Http\Rest\V1\Resources\DocumentLink(
					$urlManager, $document, \Change\Http\Rest\V1\Resources\DocumentLink::MODE_PROPERTY, $extraColumns
				));
			}
		}

		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setLimit($data['limit']);
		$result->setOffset($data['offset']);
		$result->setCount($data['count']);
		$result->setSort('label');
		$result->setDesc(false);
		$event->setResult($result);
	}
}