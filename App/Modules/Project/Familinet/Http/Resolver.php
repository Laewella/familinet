<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Http;

use Change\Http\BaseResolver;
use Change\Http\Event;

/**
 * @name \Rbs\Admin\Http\Resolver
 */
class Resolver extends BaseResolver
{
	/**
	 * @param Event $event
	 * @return void
	 */
	public function resolve($event)
	{
		$request = $event->getRequest();
		$path = $request->getPath();
		if (strpos($path, '//') !== false)
		{
			return;
		}
		if ($path === $request->getServer('SCRIPT_NAME'))
		{
			$path = '/';
		}

		if ($path === '/')
		{
			$action = function($event) {
				(new \Rbs\Admin\Http\Actions\GetHome())->execute($event);
			};
			$event->setAction($action);
			return;
		}

		$relativePath = $this->getRelativePath($path);
		if ($relativePath === 'Project/Familinet/i18n.js')
		{
			$action = function($event) {
				(new \Rbs\Admin\Http\Actions\GetI18nPackage())->execute($event);
			};
			$event->setAction($action);
			return;
		}
		if ($relativePath === 'Project/Familinet/routes.js')
		{
			$action = function($event) {
				(new \Rbs\Admin\Http\Actions\GetRoutes())->execute($event);
			};
			$event->setAction($action);
			return;
		}
		if (preg_match('/^([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/(.+)\.([a-z]+)$/', $relativePath, $matches))
		{
			$event->setParam('resourcePath', $relativePath);
			list(,$vendor, $shortModuleName, $subPath, $extension) = $matches;
			$event->setParam('vendor', $vendor);
			$event->setParam('shortModuleName', $shortModuleName);
			$event->setParam('modulePath', $subPath);
			$event->setParam('extension', $extension);

			if ($extension === 'twig')
			{
				$action = function($event) {
					(new \Rbs\Admin\Http\Actions\GetHtmlFragment())->execute($event);
				};
				$event->setAction($action);
				return;
			}
			$action = function($event) {
				(new \Rbs\Admin\Http\Actions\GetResource())->execute($event);
			};
			$event->setAction($action);
			return;
		}
	}

	/**
	 * @param string $path
	 * @return string
	 */
	protected function getRelativePath($path)
	{
		if ($path && $path[0] === '/')
		{
			$path = substr($path, 1);
		}
		return $path;
	}
}