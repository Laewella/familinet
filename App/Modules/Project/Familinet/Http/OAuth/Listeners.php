<?php
namespace Project\Familinet\Http\OAuth;

/**
 * @name \Project\Familinet\Http\OAuth\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			(new \Project\Familinet\Http\OAuth\LoginForm())->execute($event);
		};
		$this->listeners[] = $events->attach('loginFormHtml', $callback, 5);
	}
}