<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Setup;

/**
 * @name \Project\Familinet\Setup\Install
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/OAuthManager/Project_Familinet',
			\Project\Familinet\Http\OAuth\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ListenerAggregateClasses/Project_Familinet',
			\Project\Familinet\Documents\Events\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Commands/Project_Familinet', \Project\Familinet\Commands\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/CollectionManager/Project_Familinet',
			\Project\Familinet\Collection\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ModelManager/Project_Familinet',
			\Project\Familinet\Events\ModelManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/Rest/Project_Familinet', \Project\Familinet\Http\Rest\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ListenerAggregateClasses/Project_Familinet',
			\Project\Familinet\Events\SharedListeners::class);

		// Image formats, to make base theme styles work...
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/listItem', '160x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/setItem', '80x60');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/pictogram', '60x45');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/detail', '540x405');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/detailThumbnail', '80x60');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/detailCompact', '450x300');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/attribute', '160x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/selectorItem', '30x30');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/cartItem', '160x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/shortCartItem', '80x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/modeThumbnail', '80x60');
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$OAuth = $applicationServices->getOAuthManager();
		$consumer = $OAuth->getConsumerByApplication('familinet');
		if ($consumer)
		{
			return;
		}

		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();

			$consumer = new \Change\Http\OAuth\Consumer($OAuth->generateConsumerKey(), $OAuth->generateConsumerSecret());
			$isb = $applicationServices->getDbProvider()->getNewStatementBuilder('Install::executeApplication');
			$fb = $isb->getFragmentBuilder();
			$isb->insert($fb->table($isb->getSqlMapping()->getOAuthApplicationTable()), $fb->column('application'),
				$fb->column('consumer_key'), $fb->column('consumer_secret'), $fb->column('timestamp_max_offset'),
				$fb->column('token_access_validity'), $fb->column('token_request_validity'), $fb->column('active'));
			$isb->addValues($fb->parameter('application'), $fb->parameter('consumer_key'), $fb->parameter('consumer_secret'),
				$fb->integerParameter('timestamp_max_offset'), $fb->parameter('token_access_validity'),
				$fb->parameter('token_request_validity'), $fb->booleanParameter('active'));
			$iq = $isb->insertQuery();
			$iq->bindParameter('application', 'familinet');
			$iq->bindParameter('consumer_key', $consumer->getKey());
			$iq->bindParameter('consumer_secret', $consumer->getSecret());
			$iq->bindParameter('timestamp_max_offset', 60);
			$iq->bindParameter('token_access_validity', 'P10Y');
			$iq->bindParameter('token_request_validity', 'P1D');
			$iq->bindParameter('active', true);
			$iq->execute();

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}
}
