<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Commands;

/**
 * @name \Project\Familinet\Commands\UpdateWebApp
 */
class UpdateWebApp
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$application = $event->getApplication();

		// Update all web apps.
		if ($event->getParam('all'))
		{
			$configurations = $application->getConfiguration()->getEntry('Project/Familinet/webApps');
			if (!is_array($configurations) || count($configurations) == 0)
			{
				$response->addErrorMessage('There is no declared web app in this project.');
				return;
			}

			$response->addInfoMessage(count($configurations) . ' web app(s) to update');
			foreach ($configurations as $appName => $configuration)
			{
				$response->addInfoMessage('Update web app: ' . $appName);
				$this->updateWebApp($configuration, $event);
			}
		}
		// Update a single web app.
		else
		{
			$webAppName = $event->getParam('webAppName');
			if (!$webAppName)
			{
				$response->addErrorMessage('You must specify a web app name or use option --all.');
				return;
			}

			$configuration = $application->getConfiguration()->getEntry('Project/Familinet/webApps/familinet');
			if (!is_array($configuration) || !isset($configuration['serverBaseUrl']) || !isset($configuration['modules']))
			{
				$response->addErrorMessage('You must specify a valid web app name.');
				return;
			}

			$response->addInfoMessage('Update web app: ' . $webAppName);
			$this->updateWebApp($configuration, $event);
		}

		$response->addInfoMessage('Done.');
	}

	/**
	 * @param array $configuration
	 * @param \Change\Commands\Events\Event $event
	 * @throws \RuntimeException
	 */
	protected function updateWebApp($configuration, $event)
	{
		$application = $event->getApplication();
		$applicationServices = $event->getApplicationServices();

		$webBaseDirectory = $application->getWorkspace()
			->composeAbsolutePath($application->getConfiguration()->getEntry('Change/Install/webBaseDirectory'));
		if (!is_dir($webBaseDirectory))
		{
			throw new \RuntimeException('Invalid document root path: ' . $webBaseDirectory .
				'. Check "Change/Install/webBaseDirectory" configuration entry.', 999999);
		}

		$appDirectory = $webBaseDirectory . DIRECTORY_SEPARATOR . $configuration['appPath'];
		$plugin = $applicationServices->getPluginManager()->getModule('Project', 'Familinet');
		$assertsPath = $plugin->getAbsolutePath() . '/WebApp/Assets';

		// Index.html.
		$srcPath = $assertsPath . '/index.html';
		$vars = [
			'{applicationShortName}',
			'{applicationPackageName}',
			'{SERVER_BASE_URL}',
			'{APP_PATH}',
			'{OAUTH}'
		];
		$realm = $configuration['realm'];
		$consumer = $event->getApplicationServices()->getOAuthManager()->getConsumerByApplication($realm);
		$values = [
			'Familinet',
			$event->getApplicationServices()->getI18nManager()->trans('m.rbs.ua.common.application_package_name', ['ucf']),
			$configuration['serverBaseUrl'],
			$configuration['appPath'],
			json_encode($consumer ? array_merge($consumer->toArray(), ['realm' => $realm]) : [])
		];
		$this->copyFile($srcPath, $appDirectory . DIRECTORY_SEPARATOR . basename($srcPath), $vars, $values);

		// File copy.
		$this->copyDirectory($assertsPath . '/app', $appDirectory . '/app');
		$this->copyDirectory($assertsPath . '/lib', $appDirectory . '/lib');
		$this->copyFile($assertsPath . '/LICENSE.txt', $appDirectory . '/LICENSE.TXT');

		// Update modules.
		foreach ($configuration['modules'] as $fullName)
		{
			list($vendorName, $moduleName) = explode('_', $fullName);
			$plugin = $applicationServices->getPluginManager()->getModule($vendorName, $moduleName);
			if (!$plugin || !$plugin->getActivated())
			{
				continue;
			}
			$this->copyDirectory($plugin->getAbsolutePath() . '/WebApp/Assets', $appDirectory . '/' . $fullName);
		}
	}

	/**
	 * @param string $srcPath
	 * @param string $targetPath
	 * @param string[] $includedExtensions
	 */
	protected function copyDirectory($srcPath, $targetPath, $includedExtensions = [])
	{
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && strpos($current->getBasename(), '.') !== 0
				&& (count($includedExtensions) == 0 || in_array($current->getExtension(), $includedExtensions))
			)
			{
				$filePath = $current->getPathname();
				$this->copyFile($filePath, $targetPath . str_replace($srcPath, '', $filePath));
			}
			$it->next();
		}
	}

	/**
	 * @param string $srcPath
	 * @param string $targetPath
	 * @param string[] $vars
	 * @param string[] $values
	 */
	protected function copyFile($srcPath, $targetPath, $vars = [], $values = [])
	{
		if (count($vars) > 0 && count($vars) == count($values))
		{
			$content = str_replace($vars, $values, \Change\Stdlib\FileUtils::read($srcPath));
		}
		else
		{
			$content = \Change\Stdlib\FileUtils::read($srcPath);
		}
		\Change\Stdlib\FileUtils::write($targetPath, $content);
	}
}