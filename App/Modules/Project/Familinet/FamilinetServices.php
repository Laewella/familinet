<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet;

/**
 * @name \Project\Familinet\FamilinetServices
 */
class FamilinetServices extends \Zend\ServiceManager\ServiceManager
{
	use \Change\Services\ServicesCapableTrait;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @return $this
	 */
	public function setApplicationServices(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->applicationServices = $applicationServices;
		return $this;
	}

	/**
	 * @return \Change\Services\ApplicationServices
	 */
	protected function getApplicationServices()
	{
		return $this->applicationServices;
	}

	/**
	 * @return array<alias => className>
	 */
	protected function loadInjectionClasses()
	{
		$classes = $this->getApplication()->getConfiguration('Project/Familinet/Services');
		return \is_array($classes) ? $classes : [];
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function __construct(\Change\Application $application, \Change\Services\ApplicationServices $applicationServices)
	{
		$this->setApplication($application);
		$this->setApplicationServices($applicationServices);

		parent::__construct(['shared_by_default' => true]);

		//SearchManager : Application, DbProvider, DocumentManager
		$class = $this->getInjectedClassName('SearchManager', \Project\Familinet\Search\SearchManager::class);
		$this->setAlias('SearchManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Familinet\Search\SearchManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbProvider($this->applicationServices->getDbProvider())
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});

		//AttributeManager : Application, DbProvider, DocumentManager
		$class = $this->getInjectedClassName('AttributeManager', \Project\Familinet\AttributeManager::class);
		$this->setAlias('AttributeManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Familinet\AttributeManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbProvider($this->applicationServices->getDbProvider())
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});
	}

	/**
	 * @api
	 * @return \Project\Familinet\Search\SearchManager
	 */
	public function getSearchManager()
	{
		return $this->get('SearchManager');
	}

	/**
	 * @api
	 * @return \Project\Familinet\AttributeManager
	 */
	public function getAttributeManager()
	{
		return $this->get('AttributeManager');
	}
}