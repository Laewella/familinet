(function(__change) {
	"use strict";

	var app = angular.module('RbsChange');

	app.service('RbsChange.Settings', ['RbsChange.User', function(User) {
		var user = User.get();
		return {
			'set': function(key, value, save) {
				var isModified = user.profile[key] !== value;
				user.profile[key] = value;
				if (isModified && save !== false) {
					return User.saveProfile(user.profile);
				}
				return null;
			},

			'get': function(key, defaultValue) {
				return user.profile[key] || defaultValue;
			},

			'ready': function() {
				return User.ready();
			},

			'defaultLCID': function() {
				return __change['supportedLCIDs'][0];
			}
		};
	}]);

	app.service('RbsChange.User',
		['RbsChange.REST', 'RbsChange.Events', 'OAuthService', '$location', '$rootScope', '$cookies', '$http', '$q', '$window',
			function(REST, Events, OAuthService, $location, $rootScope, $cookies, $http, $q, $window) {
				var user = { 'profile': {} },
					self = this,
					readyQ = $q.defer();

				/**
				 * Starts the OAuth authentication process.
				 */
				this.startAuthentication = function() {
					var callbackUrl = document.getElementsByTagName('base')[0].href + 'authenticate?route=' +
						encodeURIComponent($location.url());
					OAuthService.startAuthentication(callbackUrl);
				};

				/**
				 * Load current user from the server.
				 */
				this.load = function() {
					var promise = REST.call(REST.getBaseUrl('admin/currentUser'));
					promise.then(
						// Success.
						function(result) {
							angular.extend(user, result.properties);
							var LCID = user.profile.LCID;
							REST.setLanguage(LCID);
							$rootScope.user = user;

							var date = new Date();
							date.setFullYear(date.getFullYear() + 1);
							$cookies.put('LCID', LCID, { expires: date });

							var currentLCID = __change.LCID;
							if (currentLCID && currentLCID !== LCID) {
								$window.location.reload();
							}

							readyQ.resolve(user);
						},
						// Error.
						function(error) {
							if (error.status != 401) {
								console.error(error);
							}
						}
					);

					return promise;
				};

				/**
				 * @returns {Promise}
				 */
				this.ready = function() {
					return readyQ.promise;
				};

				/**
				 * @param profile
				 */
				this.saveProfile = function(profile) {
					var p = $http.put(REST.getBaseUrl('admin/currentUser'), profile);
					p.then(
						function(result) {
							angular.extend(user, result.data.properties);
							var LCID = user.profile.LCID;
							var currentLCID = __change.LCID;
							if (currentLCID && currentLCID !== LCID) {

								var date = new Date();
								date.setFullYear(date.getFullYear() + 1);
								$cookies.put('LCID', LCID, { expires: date });

								$window.location.reload();
							}
						},
						function(result) { console.error(result); }
					);
					return p;
				};

				/**
				 * @returns User
				 */
				this.get = function() {
					return user;
				};

				/**
				 *
				 */
				this.logout = function() {
					// Remove all properties of our 'user' object, keeping the same reference.
					if (user) {
						var p, props = [];
						for (p in user) {
							if (user.hasOwnProperty(p)) {
								props.push(p);
							}
						}
						for (p = 0; p < props.length; p++) {
							delete user[props[p]];
						}
					}

					OAuthService.logout();
					this.startAuthentication();
				};

				$rootScope.$on(Events.Logout, function() {
					self.logout();
				});

				this.init = function() {
					$rootScope.user = this.get();
					if (OAuthService.hasOAuthData()) {
						this.load();
						return true;
					}
					else {
						this.startAuthentication();
						return false;
					}
				};
			}]);
})(window.__change);