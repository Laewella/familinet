/**
 * Copyright (C) 2014 Ready Business System & Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appSwitch
	 * @name Yes/No switch control
	 * @restrict A
	 *
	 * @description
	 * Displays a Yes/No switch control.
	 *
	 * @param {Boolean} ng-model The bound value.
	 * @param {String=} label-on Label for the "on" position
	 * @param {String=} label-off Label for the "off" position
	 * @param {String=} value-on Value for the "on" position
	 * @param {String=} value-off Value for the "off" position
	 */
	app.directive('appSwitch', ['RbsChange.i18n', function(i18n) {
		return {
			template: '<div class="switch-on-off switch">' +
			'<div class="switch-button"></div>' +
			'<label class="on">(= labelOn =)</label>' +
			'<label class="off">(= labelOff =)</label>' +
			'</div>',
			restrict: 'A',
			require: 'ngModel',
			replace: true,
			priority: -1, // Let `required=""` directive execute before this one.
			scope: true,

			link: function(scope, elm, attrs, ngModel) {
				var sw = jQuery(elm), valueOff, valueOn, acceptedValuesOn;
				scope.labelOn = attrs.labelOn || i18n.trans('m.project.familinet.app.yes');
				scope.labelOff = attrs.labelOff || i18n.trans('m.project.familinet.app.no');
				valueOff = attrs['valueOff'] || false;
				valueOn = attrs['valueOn'] || true;
				acceptedValuesOn = attrs['acceptedValuesOn'] || [];

				// Remove all parsers that could invalidate this widget (required=true for example).
				ngModel.$parsers.length = 0;

				ngModel.$render = function() {
					if (isON()) {
						sw.addClass('on');
					}
					else {
						sw.removeClass('on');
					}
				};

				function isON() {
					return ngModel.$viewValue === valueOn || acceptedValuesOn.indexOf(ngModel.$viewValue) !== -1;
				}

				function toggleState() {
					ngModel.$setViewValue(isON() ? valueOff : valueOn);
					ngModel.$render();
				}

				sw.click(function() {
					if (attrs.disabled) {
						return;
					}
					scope.$apply(toggleState);
				});
			}
		};
	}]);
})();