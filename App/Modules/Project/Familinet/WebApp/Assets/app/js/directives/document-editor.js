/**
 * Copyright (C) 2014 Ready Business System & Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange'),
		editorSectionDirective;

	/**
	 * Checks whether the given `obj` is a Promise or not.
	 * @param obj
	 * @returns {*}
	 */
	function isPromise(obj) {
		return angular.isObject(obj) && angular.isFunction(obj.then);
	}

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDocumentEditorBase
	 * @name Document editor (base controller)
	 * @scope
	 * @restrict A
	 *
	 * @description
	 * Base directive for Document editors.
	 *
	 * ### Public API of the Directive's Controller ###
	 * (these methods can be called from the child Directives that require this one):
	 *
	 * - `submit()`
	 * - `prepareCreation()`
	 * - `prepareEdition()`
	 * - `clearInvalidFields()`
	 * - `getCurrentSection()`
	 * - `addMenuEntry()`
	 * - `getMenuEntries()`
	 * - `getProperties()`
	 * - `addHeaderMessage()`
	 * - `getDocumentModelName()`
	 *
	 * ### Editor's Scope ###
	 *
	 * #### Methods ####
	 *
	 * - `isUnchanged()`
	 * - `reset()`
	 * - `submit()`
	 * - `hasStatus()`
	 *
	 * #### Properties ####
	 *
	 * - `document`: the edited Document
	 * - `original`: copy of the edited Document, used as a reference to check the changes
	 * - `changes`: array containing the name of the modified properties
	 * - `modelInfo`: object containing Model's information
	 *
	 * @param {String} model The full model name of the Documents that can be edited in this editor.
	 */
	appDocumentEditor.$inject = [
		'$rootScope', '$routeParams', '$q', '$location', '$compile',
		'RbsChange.Utils', 'RbsChange.ArrayUtils', 'RbsChange.i18n', 'RbsChange.REST',
		'RbsChange.Events', 'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter', 'RbsChange.Breadcrumb'
	];

	app.directive('appDocumentEditor', appDocumentEditor);

	function appDocumentEditor($rootScope, $routeParams, $q, $location, $compile, Utils, ArrayUtils, i18n, REST, Events,
		NotificationCenter, ErrorFormatter, Breadcrumb) {
		return {
			restrict: 'A',
			scope: true,

			controller: ['$scope', '$element', function(scope, element) {
				var initializedSections = {},
					menuEntries = [],
					properties = {},
					createNewDocumentId,
					modelInfoPromise,
					documentReady = false;

				//-----------------------------------------------//
				//                                               //
				// Scope methods and properties                  //
				//                                               //
				//-----------------------------------------------//

				scope.document = {};
				scope.changes = [];

				scope.isUnchanged = isUnchanged;
				scope.reset = reset;
				scope.submit = submit;
				scope.hasStatus = hasStatus;
				scope.confirmationOnSave = element.attr('data-confirmation-on-save');

				// Load Model's information.
				modelInfoPromise = REST.modelInfo(getDocumentModelName());
				modelInfoPromise.then(function(modelInfo) {
					scope.modelInfo = modelInfo;
					delete scope.modelInfo.links;
				});

				scope.canGoBack = function() {
					return scope.isUnchanged();
				};

				scope.goBack = function(event) {
					if (angular.isFunction(scope.onCancel)) {
						scope.onCancel(event);
					}
				};

				scope.onCancel = function() {
					Breadcrumb.goParent();
				};

				scope.isDocumentReady = function() {
					return documentReady;
				};

				scope.isPropertyModified = function(propertyName) {
					return angular.isArray(scope.changes) && scope.changes.indexOf(propertyName) !== -1;
				};

				//-----------------------------------------------//
				//                                               //
				// Save process                                  //
				//                                               //
				//-----------------------------------------------//

				scope.saveProgress = {
					"running": false,
					"error": false,
					"success": false,
					"operation": null
				};

				function saveOperation(op) {
					switch (op) {
						case 'error':
							scope.saveProgress.running = false;
							scope.saveProgress.error = true;
							scope.saveProgress.operation = null;
							scope.saveProgress.success = !scope.saveProgress.error;
							break;

						case 'success':
							scope.saveProgress.running = false;
							scope.saveProgress.error = false;
							scope.saveProgress.operation = null;
							scope.saveProgress.success = !scope.saveProgress.error;
							break;

						default :
							scope.saveProgress.running = true;
							scope.saveProgress.error = false;
							scope.saveProgress.success = false;
							scope.saveProgress.operation = op;
					}
				}

				/**
				 * Sends the changes to the server, via a POST (creation) or a PUT (update) request.
				 * @public
				 */
				function submit() {
					var promises = [];
					scope.$broadcast('Change:EditorPreSubmit', scope.document, promises);

					if (promises.length) {
						$q.all(promises).then(doSubmit, cancelSubmit);
					}
					else {
						doSubmit();
					}
				}

				function cancelSubmit() {
					saveOperation('success');
				}

				/**
				 * Submits the changes to the server.
				 * If there are files to upload, they will be processed before the document is really saved.
				 */
				function doSubmit() {
					var preSavePromises = [],
						promise;

					// Check for files to upload...
					if (element) {
						element.find('rbs-uploader,[rbs-uploader]').each(function() {
							var scope = angular.element(jQuery(this)).scope();
							if (angular.isFunction(scope.upload)) {
								if (isPromise(promise = scope.upload())) {
									preSavePromises.push(promise);
								}
							}
							else {
								throw new Error("Could not find 'upload()' method in rbsUploader's scope.");
							}
						});
					}

					// Call 'preSave' if present in the Scope: it should return null or a Promise.
					if (angular.isFunction(scope.preSave)) {
						if (isPromise(promise = scope.preSave(scope.document))) {
							preSavePromises.push(promise);
						}
					}

					// Broadcast an event before the document is saved.
					// The "promises" array can be filled in with promises that will be resolved BEFORE
					// the document is saved.
					saveOperation("Processing pre-save Promises");
					$rootScope.$broadcast(
						Events.EditorPreSave,
						{
							document: scope.document,
							promises: preSavePromises
						}
					);

					if (preSavePromises.length) {
						$q.all(preSavePromises).then(
							// Success
							executeSaveAction,
							// Error
							function(err) {
								saveOperation("error");
								console.warn("Editor: pre-save Promises error: ", err);
								NotificationCenter.error(i18n.trans('m.rbs.admin.admin.save_error'),
									ErrorFormatter.format(err), 'EDITOR');
							}
						);
					}
					else {
						executeSaveAction();
					}
				}

				/**
				 * Sends the POST/PUT request to the server and dispatches response to the callbacks.
				 */
				function executeSaveAction() {
					saveOperation("Saving Document");
					var pList = angular.copy(scope.changes);
					pList.push('documentVersion');
					REST.save(scope.document, pList).then(saveSuccessHandler, saveErrorHandler);
				}

				function saveSuccessHandler(doc) {
					var postSavePromises = [],
						result;

					if (!doc.META$.tags) {
						doc.META$.tags = [];
					}
					angular.extend(doc.META$.tags, scope.document.META$.tags);

					clearInvalidFields();

					// Call 'postSave' if present in the Scope: it should return null or a Promise.
					if (angular.isFunction(scope.postSave)) {
						result = scope.postSave(scope.document, doc);
						if (isPromise(result)) {
							postSavePromises.push(result);
						}
					}

					// Broadcast an event after the document has been successfully saved.
					// The "promises" array can be filled in with promises that will be resolved AFTER
					// the document is saved.
					saveOperation("Processing post-save Promises");
					$rootScope.$broadcast(Events.EditorPostSave, {
						"document": doc,
						"promises": postSavePromises
					});

					scope.original = angular.copy(doc);
					scope.reset();

					function terminateSave() {
						saveOperation("success");
						$rootScope.$broadcast('Change:DocumentSaved', doc);

						if (angular.isFunction(scope.terminateSave)) {
							scope.terminateSave(doc);
						}
						else if (angular.isFunction(scope.onReload)) {
							scope.onReload(scope.document);
						}
					}

					if (postSavePromises.length) {
						$q.all(postSavePromises).then(terminateSave);
					}
					else {
						terminateSave();
					}
				}

				function saveErrorHandler(reason) {
					saveOperation("error");
					NotificationCenter.error(
						i18n.trans('m.rbs.admin.admin.save_error'),
						ErrorFormatter.format(reason),
						'EDITOR',
						{
							$propertyInfoProvider: scope._chgFieldsInfo
						});

					if (angular.isObject(reason) && angular.isObject(reason.data)) {
						if (angular.isObject(reason.data['properties-errors'])) {
							angular.forEach(reason.data['properties-errors'], function(messages, propertyName) {
								markFieldAsInvalid(propertyName, messages);
							});
						}
						else if (reason.code === "INVALID-VALUE-TYPE") {
							var propertyName = reason.data.name;
							markFieldAsInvalid(propertyName, reason.message);
						}
					}
				}

				//-----------------------------------------------//
				//                                               //
				// Menu, sections and fields management          //
				//                                               //
				//-----------------------------------------------//

				/**
				 * Returns the section entry for the given named field.
				 * @param fieldName
				 * @returns {*}
				 */
				function getSectionOfField(fieldName) {
					var result = null;
					angular.forEach(menuEntries, function(entry) {
						if (angular.isArray(entry.fields)) {
							angular.forEach(entry.fields, function(field) {
								if (field.id === fieldName) {
									result = entry;
								}
							});
						}
					});
					return result;
				}

				/**
				 * Marks the given named field as invalid in this editor.
				 * @param fieldName
				 */
				function markFieldAsInvalid(fieldName) {
					element.find('.form-group[property="' + fieldName + '"]').addClass('error')
						.find('.controls :input').first().focus();
					getSectionOfField(fieldName).invalid.push(fieldName);
				}

				/**
				 * Removes all invalid fields markers.
				 */
				function clearInvalidFields() {
					element.find('.form-group.property.error').removeClass('error');
					angular.forEach(menuEntries, function(entry) {
						ArrayUtils.clear(entry.invalid);
					});
				}

				/**
				 * Call `scope.initSection(sectionName)` once per section to do some initialization for the given
				 * section.
				 *
				 * Implement the `initSection()` in your Editor's Scope to initialize the section given as argument.
				 * `initSection()` will be called only once for each section, when the user first switches to it.
				 *
				 * @param section
				 */
				function initSectionOnce(section) {
					if (!initializedSections[section] && angular.isFunction(scope.initSection)) {
						scope.initSection(section);
						initializedSections[section] = true;
					}
				}

				/**
				 * Returns the form section currently displayed.
				 * @returns {string}
				 */
				function getCurrentSection() {
					return $routeParams.section || $location.search()['section'] || '';
				}

				scope.$on('$routeUpdate', function() {
					var s = getCurrentSection();
					if (s !== scope.section) {
						scope.section = s;
						$rootScope.$broadcast('Change:EditorSectionChanged', scope.section);
					}
				});

				// Watch for section changes to initialize them if needed.
				scope.$watch('section', function(section, previousSection) {
					if (section !== undefined && section !== null) {
						initSectionOnce(section);
					}

					if (angular.isDefined(previousSection) && previousSection !== section &&
						angular.isFunction(scope.leaveSection)) {
						scope.leaveSection(previousSection);
					}
					if (angular.isDefined(section) && angular.isFunction(scope.enterSection)) {
						scope.enterSection(section);
					}
				});

				/**
				 * Adds an entry in the menu structure for this editor.
				 * @param entry
				 */
				function addMenuEntry(entry) {
					menuEntries.push(entry);
					angular.forEach(entry.fields, function(f) {
						properties[f.id] = f.label;
					});
					scope.$emit('Change:UpdateEditorMenu', menuEntries);
				}

				/**
				 * Returns currently registered menu entries for this editor.
				 * @returns {Array}
				 */
				function getMenuEntries() {
					return menuEntries;
				}

				scope.$on('Change:Editor:UpdateMenu', function() {
					// 1) Clear all menu entries.
					menuEntries.length = 0;
					properties = {};
					// 2) Ask every section to update itself with the fields it contains.
					scope.$broadcast('Change:Editor:SectionsUpdateMenu');
					// 3) Tell the aside to update.
					scope.$emit('Change:UpdateEditorMenu', menuEntries);
				});

				//-----------------------------------------------//
				//                                               //
				// Document management                           //
				//                                               //
				//-----------------------------------------------//

				/**
				 * Prepares the Editor for the edition of the given `doc`.
				 * @param doc
				 */
				function prepareEdition(doc) {
					scope.document = doc;
					scope.parentId = $routeParams.parentId || null;

					modelInfoPromise.then(function() {
						var loadedPromises = [];

						// Call `scope.onLoad()` if present.
						if (angular.isFunction(scope.onLoad)) {
							var p = scope.onLoad();
							if (isPromise(p)) {
								loadedPromises.push(p);
							}
						}

						// Trigger `Events.EditorLoaded`.
						$rootScope.$broadcast(Events.EditorLoaded, {
							scope: scope,
							document: scope.document,
							promises: loadedPromises
						});

						// At this point, `scope.document` has been loaded and may have been tweaked by the `onLoad()`
						// function in the Scope (if present) and by the handlers listening on `Events.EditorLoaded`.
						// We consider that the document is now ready: we make a copy of it to create the reference
						// document used to check for changes in the editor.
						if (loadedPromises.length) {
							$q.all(loadedPromises).then(initReferenceDocument);
						}
						else {
							initReferenceDocument();
						}
					});
				}

				/**
				 * Prepares the Editor for the edition of the given `doc`.
				 * @param doc
				 */
				function prepareCreation(doc) {
					scope.document = doc;
					scope.parentId = $routeParams.parentId || null;

					createNewDocumentId = scope.document.id;

					modelInfoPromise.then(function() {
						// Apply default values for new documents.
						applyDefaultValues(scope.document, scope.modelInfo);

						var loadedPromises = [];

						// Call `scope.onLoad()` if present.
						if (angular.isFunction(scope.onLoad)) {
							var p = scope.onLoad();
							if (isPromise(p)) {
								loadedPromises.push(p);
							}
						}

						// Trigger `Events.EditorLoaded`.
						$rootScope.$broadcast(Events.EditorLoaded, {
							scope: scope,
							document: scope.document,
							promises: loadedPromises
						});

						// At this point, `scope.document` has been loaded and may have been tweaked by the `onLoad()`
						// function in the Scope (if present) and by the handlers listening on `Events.EditorLoaded`.
						// We consider that the document is now ready: we make a copy of it to create the reference
						// document used to check for changes in the editor.
						if (loadedPromises.length) {
							$q.all(loadedPromises).then(initReferenceDocument);
						}
						else {
							initReferenceDocument();
						}
					});
				}

				/**
				 * Applies the default values defined in the ModelInfo on the given document.
				 * (only called for new documents).
				 *
				 * @param doc
				 * @param modelInfo
				 */
				function applyDefaultValues(doc, modelInfo) {
					angular.forEach(modelInfo.properties, function(propObject, name) {
						if (!doc[name] && propObject.hasOwnProperty('defaultValue') && propObject.defaultValue !== null) {
							doc[name] = propObject.defaultValue;
						}
					});
				}

				/**
				 * Creates the reference document (original) from the current document.
				 * Triggers the `Events.EditorReady` event.
				 */
				function initReferenceDocument() {
					scope.original = angular.copy(scope.document);
					documentReady = true;

					// Call `scope.onReady()` if present.
					if (angular.isFunction(scope.onReady)) {
						scope.onReady();
					}

					$rootScope.$broadcast(Events.EditorReady, {
						scope: scope,
						document: scope.document
					});

					// Computes a list of changes on the fields in each digest cycle.
					ArrayUtils.clear(scope.changes);
					scope.$watch('document', function editorDocumentWatch() {
						ArrayUtils.clear(scope.changes);
						angular.forEach(scope.document, function(value, name) {
							var original = angular.isDefined(scope.original[name]) ? scope.original[name] : '';
							if (name !== 'META$' && scope.changes.indexOf(name) === -1) {
								if (Utils.isDocument(original) && Utils.isDocument(value)) {
									if (original.id !== value.id) {
										scope.changes.push(name);
									}
								}
								else if (!angular.equals(original, value)) {
									scope.changes.push(name);
								}
							}
						});
					}, true);
				}

				/**
				 * Reset the form back to the originally loaded document (scope.original).
				 */
				function reset() {
					scope.document = angular.copy(scope.original);
					scope.saveProgress.error = false;
					clearInvalidFields();
					NotificationCenter.clear();
				}

				/**
				 * Tells whether the editor has changes or not.
				 * @return Boolean
				 */
				function isUnchanged() {
					var p, dv, ov;
					for (p in scope.document) {
						if (p !== 'META$' && scope.document.hasOwnProperty(p)) {
							dv = scope.document[p];
							ov = scope.original ? scope.original[p] : undefined;
							// For sub-documents, we only need to check the ID.
							if (Utils.isDocument(dv) && Utils.isDocument(ov)) {
								if (dv.id !== ov.id) {
									return false;
								}
							}
							else {
								if (!angular.equals(dv, ov)) {
									return false;
								}
							}
						}
					}
					return true;
				}

				/**
				 * Tells whether the current Document has the given publication status or not.
				 * @param status
				 * @returns {*}
				 */
				function hasStatus(status) {
					if (!scope.document) {
						return false;
					}
					var args = [scope.document];
					ArrayUtils.append(args, arguments);
					return Utils.hasStatus.apply(Utils, args);
				}

				function getDocumentModelName() {
					return element.attr('data-model');
				}

				scope.$on(Events.EditorUpdateDocumentProperties, function onUpdateDocumentPropertiesFn(event, properties) {
					angular.extend(scope.document, properties);
					submit();
				});

				// Pre-configured static filters

				scope.activeStaticFilters = [
					{
						name: 'active',
						parameters: { propertyName: 'active', value: true, operator: 'eq' }
					}
				];

				//-----------------------------------------------//
				//                                               //
				// Public API of Editor Controller               //
				//                                               //
				// These methods may be called from the other    //
				// directives bound to this editor.              //
				//                                               //
				//-----------------------------------------------//

				this.submit = submit;
				this.prepareCreation = prepareCreation;
				this.prepareEdition = prepareEdition;

				this.clearInvalidFields = clearInvalidFields;
				this.getCurrentSection = getCurrentSection;
				this.addMenuEntry = addMenuEntry;
				this.getMenuEntries = getMenuEntries;

				this.getProperties = function() {
					return properties;
				};

				this.addHeaderMessage = function(html) {
					var container = element.find('rbs-page-header');
					if (container.length) {
						return container.after($compile(html)(scope));
					}
					else {
						return element.prepend($compile('<div class="col-md-12">' + html + '</div>')(scope));
					}
				};

				this.getDocumentModelName = getDocumentModelName;
			}],

			link: function linkFn(scope, element, attrs, ctrl) {
				scope.section = ctrl.getCurrentSection();
			}
		};
	}

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDocumentEditorNew
	 * @name Document editor (creation)
	 * @restrict A
	 * @element form
	 *
	 * @description
	 * Directive used to create new Documents.
	 *
	 * This directive requires the {@link change/RbsChange.directive:rbsDocumentEditorBase `rbs-document-editor-base`}
	 * to be present on an ancestor.
	 *
	 * @example
	 * <pre>
	 *     <div rbs-document-editor-base="" model="...">
	 *         ...
	 *         <div rbs-document-editor-new="">
	 *             ...
	 *         </div>
	 *     </div>
	 * </pre>
	 */
	app.directive('appDocumentNew', [
		'RbsChange.REST', 'RbsChange.Settings', '$location', '$routeParams', 'RbsChange.NotificationCenter', 'RbsChange.i18n',
		function(REST, Settings, $location, $routeParams, NotificationCenter, i18n) {
			return {
				restrict: 'A',
				require: '^appDocumentEditor',
				scope: false,
				priority: 900,

				compile: function(tElement) {
					tElement.attr('name', 'form');
					tElement.addClass('form-horizontal');

					return function rbsDocumentEditorNewLink(scope, iElement, iAttrs, ctrl) {
						var doc = REST.newResource(ctrl.getDocumentModelName(), Settings.get('LCID'));

						// Check if there is a 'from' parameter in the route.
						// In that case, we load the corresponding reference Document and populate the new Document with
						// the properties of the reference Document.
						var fromId = $routeParams['from'] ? parseInt($routeParams['from'], 10) : NaN;
						if (!isNaN(fromId) && fromId) {
							ctrl.addHeaderMessage('<div data-app-document-editor-create-from-message="createFromReferenceDocument"></div>');

							REST.resource(ctrl.getDocumentModelName(), fromId).then(
								// Success
								function(refDoc) {
									scope.createFromReferenceDocument = refDoc;
									angular.forEach(ctrl.getProperties(), function(label, id) {
										doc[id] = angular.copy(refDoc[id]);
									});
									doc.typology$ = angular.copy(refDoc.typology$);
									ctrl.prepareCreation(doc);
								},
								// Error
								function() {
									NotificationCenter.error(i18n.trans('m.project.familinet.app.document_editor_reference_document_could_not_be_loaded'),
										'', 'EDITOR');
								}
							);
						}
						else {
							ctrl.prepareCreation(doc);
						}

						// Function called when a creation has been done.
						scope.terminateSave = function(doc) {
							$location.path(doc.url());
						};
					};
				}
			};
		}
	]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDocumentEditorEdit
	 * @name Document editor (edition)
	 * @restrict A
	 * @element form
	 *
	 * @description
	 * Directive used to edit an existing Document.
	 *
	 * This directive requires the {@link change/RbsChange.directive:rbsDocumentEditorBase `rbs-document-editor-base`}
	 * to be present on an ancestor.
	 *
	 * @example
	 * <pre>
	 *     <div rbs-document-editor-base="" model="...">
	 *         ...
	 *         <div rbs-document-editor-edit="">
	 *            ...
	 *         </div>
	 *     </div>
	 * </pre>
	 */
	app.directive('appDocumentEdit',
		['$filter', '$routeParams', '$location', 'RbsChange.NotificationCenter', 'RbsChange.REST', 'RbsChange.i18n',
			'RbsChange.Utils',
			function($filter, $routeParams, $location, NotificationCenter, REST, i18n, Utils) {
				return {
					restrict: 'A',
					require: '^appDocumentEditor',
					scope: false,
					priority: 900,

					controller: function() {},

					compile: function(tElement) {
						tElement.attr('name', 'form');
						tElement.addClass('form-horizontal');

						return function rbsDocumentEditorEditLink(scope, iElement, iAttrs, ctrl) {
							// Load Document from the server with id coming from the route's params.
							REST.resource(ctrl.getDocumentModelName(), parseInt($routeParams.id, 10)).then(
								// Success
								function(doc) {
									// Check the model name of the loaded Document:
									// It it's not the same as the expected one ("model" attribute), the user is redirected
									// to the right editor for the loaded Document.
									if (doc.model !== ctrl.getDocumentModelName()) {
										$location.path($filter('rbsURL')(doc, 'edit'));
									}
									else {
										ctrl.prepareEdition(doc);
									}
								},
								// Error
								function() {
									NotificationCenter.error(
										i18n.trans('m.rbs.admin.admin.document_does_not_exist | ucf') + ' ' +
										'<a href="' + $filter('rbsURL')(ctrl.getDocumentModelName(), 'new') + '">' +
										i18n.trans('m.rbs.admin.admin.create | ucf | etc') +
										'</a>', '', 'EDITOR'
									);
								}
							);

							scope.$on('Change:DocumentChanged', function(event, doc) {
								if (doc && scope.document.id === doc.id) {
									scope.reload();
								}
							});

							scope.reload = function() {
								if (Utils.isDocument(scope.document)) {
									REST.resource(scope.document).then(ctrl.prepareEdition);
								}
							};
						};
					}
				};
			}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentEditorSection
	 * @name Document editor (section)
	 * @restrict A
	 *
	 * @description
	 * Placed on a <code>&lt;fieldset/&gt;</code> to declare a section in the editor.
	 *
	 * This directive looks for the fields in the <code>&lt;fieldset/&gt;</code> and registers a menu entry in the
	 * {@link change/RbsChange.directive:appDocumentEditor editor's Controller}.
	 *
	 * This directive requires the <code>rbs-document-editor-base=""</code> to be present on an ancestor.
	 */
	editorSectionDirective = ['RbsChange.Utils', '$location', 'RbsChange.i18n', function(Utils, $location, i18n) {
		var defaultSectionIcons = {
			publication: 'icon-globe',
			activation: 'icon-time',
			systeminfo: 'icon-info-sign',
			permissions: 'icon-lock',
			'': 'icon-edit'
		};

		return {
			restrict: 'A',
			scope: false,
			require: '^appDocumentEditor',

			compile: function(tElement) {
				// First, the Section is hidden.
				// It will be shown when the user selects a section in the menu
				// (see event 'Change:EditorSectionChanged' below).
				tElement.hide();

				return function rbsEditorSectionLink(scope, iElement, iAttrs, ctrl) {
					var sectionId = iAttrs['rbsEditorSection'] || '',
						entry = {
							'id': sectionId,
							'label': iAttrs['editorSectionLabel'] ||
							i18n.trans('m.project.familinet.app.document_editor_section_main_properties|ucf'),
							'icon': iAttrs['editorSectionIcon'] || defaultSectionIcons[sectionId] || 'icon-edit',
							'fields': [],
							'required': [],
							'invalid': [],
							'index': iElement.index()
						}, p;

					entry.url = Utils.makeUrl($location.absUrl(), { section: (sectionId.length ? sectionId : null) });
					if ((p = entry.url.indexOf('#')) !== -1) {
						entry.url = entry.url.substring(0, p);
					}

					function refreshEntry() {
						entry.fields.length = 0;
						// Search for fields (properties) in this section.
						iElement.find('[data-property]').each(function(index, ctrlGrp) {
							var $ctrlGrp = jQuery(ctrlGrp),
								$lbl = $ctrlGrp.find('label[for]').first(),
								propertyName = $ctrlGrp.attr('data-property');

							entry.fields.push({
								id: propertyName,
								label: $lbl.text()
							});
							if ($ctrlGrp.hasClass('required')) {
								entry.required.push(propertyName);
							}
						});
						ctrl.addMenuEntry(entry);
					}

					refreshEntry();
					scope.$on('Change:Editor:SectionsUpdateMenu', refreshEntry);

					// Show/hide the section
					function update(section) {
						if (section === sectionId) {
							iElement.show();
						}
						else {
							iElement.hide();
						}
					}

					update(ctrl.getCurrentSection());

					scope.$on('Change:EditorSectionChanged', function(event, section) {
						update(section);
					});
				};
			}

		};
	}];
	app.directive('appDocumentEditorSection', editorSectionDirective);

	/**
	 * @name RbsChange.directive:rbsDocumentEditorCreateFromMessage
	 * @restrict A
	 *
	 * @description
	 * Displays a message when creating a Document from another one.
	 *
	 * @param {Document} rbs-document-editor-create-from-message The reference Document.
	 */
	app.directive('appDocumentEditorCreateFromMessage', ['RbsChange.i18n', function(i18n) {
		return {
			restrict: 'A',
			template: '<div class="alert alert-info">' +
			'<p data-ng-if="refDoc">' +
			i18n.trans('m.project.familinet.app.document_editor_creating_a_new_document_from') +
			' <strong><a href target="_blank" data-ng-href="(= refDoc | rbsURL =)">' +
			'<span data-ng-bind="refDoc.label"></span>' +
			' <span class="glyphicon glyphicon-new-window"></span></a>' +
			'</strong>.<br/>' +
			i18n.trans('m.project.familinet.app.document_editor_creating_a_new_document_from_tip') +
			'</p>' +
			'<p data-ng-if="!refDoc">' +
			i18n.trans('m.project.familinet.app.document_editor_loading_reference_document | ucf | etc') +
			'</p>' +
			'</div>',
			scope: {
				refDoc: '=appDocumentEditorCreateFromMessage'
			}
		};
	}]);

	/**
	 * Button bar.
	 */
	app.directive('appDocumentEditorFooter', ['$rootScope', '$compile', 'RbsChange.Utils', 'RbsChange.Settings',
		'RbsChange.Events', 'RbsChange.i18n', 'RbsChange.Navigation', 'RbsChange.REST', 'RbsChange.Breadcrumb', '$location',
		function($rootScope, $compile, Utils, Settings, Events, i18n, Navigation, REST, Breadcrumb, $location) {
			return {
				restrict: 'A',
				transclude: true,
				templateUrl: 'app/html/document-editor-footer.html',
				require: '^appDocumentEditor',

				link: function(scope, element, attrs) {
					scope.disableDelete = true;
					scope.disableNewFrom = true;

					function updateDisableActions() {
						scope.disableDelete = !Utils.isDocument(scope.document) || scope.document.isNew() ||
							!scope.document.isActionAvailable('delete') || attrs.disableDelete === 'true';
						scope.disableNewFrom = !Utils.isDocument(scope.document) || scope.document.isNew() ||
							attrs.disableNewFrom === 'true';
					}

					attrs.$observe('disableDelete', updateDisableActions);
					attrs.$observe('disableNewFrom', updateDisableActions);
					scope.$watch('document.id', updateDisableActions);

					scope.confirmReset = function() {
						if (confirm(i18n.trans('m.project.familinet.app.document_editor_confirm_reset'))) {
							scope.reset();
						}
					};

					scope.confirmDelete = function() {
						if (confirm(i18n.trans('m.project.familinet.app.document_editor_confirm_delete'))) {
							REST['delete'](scope.document).then(function() {
								var entries = Breadcrumb.pathEntries();
								var entry = entries[entries.length - 2];
								$location.url(entry.url());
							});
						}
					};
				}
			};
		}]);
})(window.jQuery);