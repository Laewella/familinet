(function() {
	// Declares the main module and its dependencies.
	// /!\ This must be done BEFORE the inclusion or RBSChange services.
	var app = angular.module('RbsChange',
		['ngRoute', 'ngResource', 'ngSanitize', 'ngTouch', 'ngCookies', 'ngAnimate', 'OAuthModule', 'ui.bootstrap']);

	/**
	 * Events used by Change, where you can attach your own handlers.
	 */
	app.constant('RbsChange.Events', {
		// Raised to provoke a trigger.
		'Logout': 'Change:Logout',

		// Raised when an Editor has finished loading its document.
		// Single argument is the edited document.
		'EditorLoaded': 'Change:Editor.Loaded',

		// Raised when an Editor is ready.
		// Single argument is the edited document.
		'EditorReady': 'Change:Editor.Ready',

		// Raised when an Editor is about to save a Document.
		// Single argument is a hash object with:
		// - document: the edited document that is about to be saved
		// - promises: array of promises that should be resolved before the save process is called.
		'EditorPreSave': 'Change:Editor.RegisterPreSavePromises',

		// Raised when an Editor has just saved a Document.
		// Single argument is a hash object with:
		// - document: the edited document that has been saved
		// - promises: array of promises that should be resolved before the edit process is terminated.
		'EditorPostSave': 'Change:Editor.RegisterPostSavePromises',

		// The following events are less useful for you...
		'EditorDocumentUpdated': 'Change:Editor.DocumentUpdated',
		'EditorCorrectionChanged': 'Change:CorrectionChanged',
		'EditorCorrectionRemoved': 'Change:CorrectionRemoved',
		'EditorUpdateDocumentProperties': 'Change:UpdateDocumentProperties',

		// Raised from the <rbs-document-list/> directive when a filter parameter is present in the URL.
		// Listeners should fill in the 'predicates' recieved in the args.
		'DocumentListApplyFilter': 'Change:DocumentList.ApplyFilter',

		// Raised from the <rbs-document-list/> directive when a converter has been requested on a column.
		// {
		//    "converter" : "...",
		//    "params"    : "...",
		//    "promises"  : [],
		//    "values"    : {}
		// }
		// Listeners should fill in the "promises" array and the "values" hash object.
		'DocumentListConverterGetValues': 'Change:DocumentList.ConverterGetValues',

		'DocumentListPreview': 'Change:DocumentList.Preview'
	});
})();