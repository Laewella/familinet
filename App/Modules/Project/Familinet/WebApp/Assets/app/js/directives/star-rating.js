(function() {
	"use strict";

	var app = angular.module('RbsChange');
	
	function appStarRating(i18n) {
		return {
			restrict: 'A',
			templateUrl: 'app/html/star-rating.html',
			scope: {
				rating: '=appStarRating',
				scale: '@'
			},
			link: function(scope, elm, attrs) {
				scope.stars = [];
				for (var i = 0; i < scope.scale; i++) {
					scope.stars.push(i);
				}

				scope.mode = attrs['mode'] || 'star';

				scope.$watch('rating', function() {
					if (attrs['scaled'] !== 'true') {
						scope.scaledRating = Math.floor(scope.rating / (100 / scope.scale));
					}
					else {
						scope.scaledRating = scope.rating;
					}
					scope.star_rating = scope.starRating(scope.scaledRating);
				});

				scope.starRating = function(value) {
					return i18n.trans('m.project.familinet.app.star_rating_labeled', { 'RATINGVALUE': value, 'BESTRATING': scope.scale });
				}
			}
		}
	}

	app.directive('appStarRating', ['RbsChange.i18n', appStarRating]);

	function appInputStarRating(i18n) {
		return {
			restrict: 'A',
			templateUrl: 'app/html/input-star-rating.html',
			require: '?ngModel',
			scope: {
				scale: '@'
			},
			link: function(scope, elm, attrs, ngModel) {
				if (!ngModel) {
					return;
				}
				scope.stars = [];
				for (var i = 0; i < scope.scale; i++) {
					scope.stars.push(i + 1);
				}
				scope.scaled = {
					rating: null
				};

				ngModel.$render = function() {
					if (ngModel.$viewValue === '' || ngModel.$viewValue === null || isNaN(ngModel.$viewValue)) {
						scope.scaled.rating = '';
					}
					else {
						scope.scaled.rating = Math.floor(ngModel.$viewValue / (100 / scope.scale));
					}
				};

				scope.$watch('scaled.rating', function(value, oldValue) {
					if (value !== oldValue) {
						if (value === '' || value === null || isNaN(value)) {
							ngModel.$setViewValue(null);
						}
						else {
							ngModel.$setViewValue(Math.ceil(value * (100 / scope.scale)));
						}
					}
					scope.ratingText = scope.starRating(value);
				});
				
				scope.starRating = function(value) {
					if (value === '') {
						return i18n.trans('m.project.familinet.app.star_rating_none');
					}
					else {
						return i18n.trans('m.project.familinet.app.star_rating', { 'RATINGVALUE': value, 'BESTRATING': scope.scale });
					}
				}
			}
		}
	}

	app.directive('appInputStarRating', ['RbsChange.i18n', appInputStarRating]);

	function appInputStarRatingItem() {
		return {
			restrict: 'A',
			scope: false,
			link: function(scope, elm, attrs) {
				var handlerIn = function handlerIn() {
					if (attrs['appInputStarRatingItem'] === '') {
						scope.scaled.hover = '';
					}
					else {
						scope.scaled.hover = parseInt(attrs['appInputStarRatingItem']);
					}
					//console.log(scope.scaled.hover);
					scope.$digest();
				};
				var handlerOut = function handlerOut() {
					scope.scaled.hover = -1;
					//console.log(scope.scaled.hover);
					scope.$digest();
				};
				elm.hover(handlerIn, handlerOut);
			}
		}
	}

	app.directive('appInputStarRatingItem', appInputStarRatingItem);
})();