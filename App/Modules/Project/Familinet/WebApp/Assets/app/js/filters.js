(function() {
	"use strict";

	var app = angular.module('RbsChange');

	//-------------------------------------------------------------------------
	//
	// Date formats.
	//
	//-------------------------------------------------------------------------

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appDateTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with date and time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('appDateTime', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'medium');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appDate
	 * @function
	 *
	 * @description
	 * Formats a Date object with date, without time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('appDate', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'mediumDate');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with time, without the date.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('appTime', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'mediumTime');
		};
	}]);

	//-------------------------------------------------------------------------
	//
	// Boolean formats.
	//
	//-------------------------------------------------------------------------

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appBoolean
	 * @function
	 *
	 * @description
	 * Formats a Boolean value with localized <em>yes</em> or <em>no</em>.
	 *
	 * @param {Boolean} value The boolean value to format.
	 */
	app.filter('appBoolean', ['RbsChange.i18n', function(i18n) {
		return function(input) {
			return i18n.trans(input ? 'm.project.familinet.app.yes' : 'm.project.familinet.app.no');
		};
	}]);

	//-------------------------------------------------------------------------
	//
	// URLs.
	//
	//-------------------------------------------------------------------------

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appURL
	 * @function
	 *
	 * @description
	 * Returns the URL for the input Document, Model name or Plugin name.
	 *
	 * Routes are defined in the `Assets/Admin/routes.json` file in each Plugin. Commonly used route names are:
	 *
	 * - `new` (with input = Model name)
	 * - `detail` (with input = Document)
	 * - `edit` (with input = Document)
	 * - `list` (with input = Model name)
	 *
	 * @param {Document|String} input Document, Model name or Plugin name.
	 * @param {String=} routeName Route name (defaults to <em>edit</em>).
	 *
	 * @example
	 * <pre>
	 *   <a ng-href="(= 'Rbs_Catalog_Product' | appURL:'new' =)">Create new product</a>
	 *   <a ng-href="(= product | appURL =)">Edit (= product.label =)</a>
	 * </pre>
	 */
	app.filter('appURL', ['$filter', 'RbsChange.Utils', function($filter, Utils) {
		return function(doc, routeName, params) {
			if (!routeName) {
				if (Utils.isDocument(doc)) {
					routeName = 'detail';
				}
				else if (Utils.isModelName(doc)) {
					routeName = 'list';
				}
				else if (Utils.isModuleName(doc)) {
					routeName = 'home';
				}
			}
			else if (routeName == 'newFrom') {
				routeName = 'createFrom';
			}
			return '#/' + $filter('rbsURL')(doc, routeName, params);
		};
	}]);
})();