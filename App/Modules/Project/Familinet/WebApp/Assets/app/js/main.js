(function() {
	// Services configuration.
	// /!\ This must be done AFTER the inclusion or RBSChange services.
	var app = angular.module('RbsChange');

	app.config(['$locationProvider', '$interpolateProvider', function($locationProvider, $interpolateProvider) {
		$interpolateProvider.startSymbol('(=').endSymbol('=)');
	}]);

	app.config(['OAuthServiceProvider', 'RbsChange.RESTProvider', function(OAuth, RESTProvider) {
		var restUrl = __change.serverBaseUrl + '/rest.php/';
		var oauthUrl = restUrl + 'OAuth/';
		OAuth.setBaseUrl(oauthUrl);
		OAuth.setRealm(__change.OAuth.realm);
		OAuth.setLocalStorageKeyName('Familinet_OAuthData');

		// Sign all the requests on our REST services...
		OAuth.setSignedUrlPatternInclude(restUrl);
		// ... but do NOT sign OAuth requests.
		OAuth.setSignedUrlPatternExclude(oauthUrl);

		// Configure REST provider.
		RESTProvider.setBaseUrl(restUrl);
	}]);

	/**
	 * RootController
	 *
	 * This Controller is bound to the <body/> tag and is, thus, the "root Controller".
	 * Mostly, it deals with user authentication and settings.
	 */
	app.controller('RootController', ['$scope', '$rootScope', 'RbsChange.User', '$location', 'RbsChange.Events',
		function(scope, $rootScope, User, $location, Events) {
			scope.hideLoadingPanel = false;
			if ($location.path() !== '/authenticate') {
				if (User.init()) {
					scope.hideLoadingPanel = true;
				}
			}

			$rootScope.$on('OAuth:AuthenticationSuccess', function() {
				User.load().then(function() {
					$location.url($location.search()['route']);
					scope.hideLoadingPanel = true;
				});
			});

			$rootScope.logout = function() {
				$rootScope.$emit(Events.Logout);
			};
		}
	]);
})();