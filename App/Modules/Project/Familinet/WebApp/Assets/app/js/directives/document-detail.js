(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('appDocumentDetailHeader', DocumentDetailHeader);
	function DocumentDetailHeader() {
		return {
			restrict: 'A',
			templateUrl: 'app/html/document-detail-header.html',
			replace: true,
			scope: {
				document: '=appDocumentDetailHeader'
			},
			link: function(scope, element, attrs) {
				attrs.$observe('subtitle', function(newValue) {
					scope.subtitle = newValue;
				});
				attrs.$observe('title', function(newValue) {
					scope.title = newValue;
				});

				scope.readonly = attrs.readonly && attrs.readonly !== 'false';
			}
		};
	}
})();