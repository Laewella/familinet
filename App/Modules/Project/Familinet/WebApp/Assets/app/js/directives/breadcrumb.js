(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('appBreadcrumb', ['RbsChange.Breadcrumb', BreadcrumbDirective]);
	function BreadcrumbDirective(Breadcrumb) {
		return {
			restrict: 'A',
			templateUrl: 'app/html/breadcrumb.html',
			replace: true,
			scope: true,

			link: function BreadcrumbDirectiveLink(scope) {
				scope.home = scope.entries = scope.current = null;

				scope.$on('Change:BreadcrumbUpdated', function() {
					scope.home = Breadcrumb.homeEntry();
					scope.entries = Breadcrumb.pathEntries();
					scope.current = Breadcrumb.currentEntry();
				});
			}
		};
	}
})();