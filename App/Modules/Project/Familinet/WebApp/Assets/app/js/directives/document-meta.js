(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('appDocumentMeta', function () {
		return {
			restrict: 'A',
			templateUrl: 'app/html/document-meta.html',

			link: function (scope) {
			}
		};
	});
})();