(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('appDocumentInline', ['RbsChange.ArrayUtils', function(ArrayUtils) {
		return {
			restrict: 'A',
			scope: true,

			link: function DocumentInlineDirectiveLink(scope, elm, attrs) {
				var propertyName = attrs.property;
				var documentName = attrs.document || 'document';
				var modelName = attrs['appDocumentInline'];
				var composedLabel = attrs['composedLabel'];

				scope.addInlineItem = function() {
					if (!angular.isArray(scope[documentName][propertyName])) {
						scope[documentName][propertyName] = [];
					}
					scope[documentName][propertyName].push({
						model: modelName
					});
				};

				scope.deleteInlineItem = function($index) {
					scope[documentName][propertyName].splice($index, 1);
				};

				scope.moveTopInlineItem = function($index) {
					if (angular.isArray(scope[documentName][propertyName])) {
						ArrayUtils.move(scope[documentName][propertyName], $index, 0);
					}
				};

				scope.moveUpInlineItem = function($index) {
					if (angular.isArray(scope[documentName][propertyName])) {
						ArrayUtils.move(scope[documentName][propertyName], $index, $index - 1);
					}
				};

				scope.moveBottomInlineItem = function($index) {
					if (angular.isArray(scope[documentName][propertyName])) {
						ArrayUtils.move(scope[documentName][propertyName], $index, scope[documentName][propertyName].length - 1);
					}
				};

				scope.moveDownInlineItem = function($index) {
					if (angular.isArray(scope[documentName][propertyName])) {
						ArrayUtils.move(scope[documentName][propertyName], $index, $index + 1);
					}
				};

				if (composedLabel) {
					var properties = composedLabel.split(',');
					scope.$watch('document.' + propertyName, function() {
						if (scope[documentName]) {
							refreshLabel(properties);
						}
					}, true);
				}

				function refreshLabel(properties) {
					var volumes = scope[documentName][propertyName];

					if (!angular.isArray(volumes)) {
						return;
					}

					for (var j = 0; j < volumes.length; j++) {
						var values = [];
						for (var i = 0; i < properties.length; i++) {
							if (scope[documentName][propertyName][j][properties[i]]) {
								values.push(scope[documentName][propertyName][j][properties[i]])
							}
						}
						scope[documentName][propertyName][j].label = values.join(" - ");
					}
				}
			}
		};
	}]);
})();