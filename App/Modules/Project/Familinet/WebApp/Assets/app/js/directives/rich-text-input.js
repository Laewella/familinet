(function() {
	"use strict";

	var app = angular.module('RbsChange');
	var editorIdCounter = 0;

	app.directive('appRichTextInput', ['$timeout', RichTextInput]);
	function RichTextInput() {
		return {
			restrict: 'A',
			templateUrl: 'app/html/rich-text-input.html',
			require: '?ngModel',
			scope: {},
			link: function(scope, element, attrs, ngModel) {
				scope.invalidMode = false;
				scope.editorId = ++editorIdCounter;
				scope.containerId = 'rbsInputMarkdownEditor' + scope.editorId;
				scope.values = { markdown: null };

				var textarea;
				var renderEditorValue = function(v) {}; // Will be overwritten later, depending on the editor mode.

				ngModel.$render = function() {
					if (angular.isObject(ngModel.$viewValue)) {
						if (!ngModel.$viewValue.e) {
							ngModel.$setViewValue({ e: 'Markdown', t: '', h: null });
						}

						scope.editorMode = ngModel.$viewValue.e;
						if (shouldInitEditors()) {
							if (scope.editorMode === 'Markdown') {
								scope.invalidMode = false;
								initWithTextarea();
								renderEditorValue(ngModel.$viewValue.t);
							}
							else {
								scope.invalidMode = true;
							}
						}
						else {
							renderEditorValue(ngModel.$viewValue.t);
						}
					}
				};

				function shouldInitEditors() {
					return !scope.invalidMode && !textarea;
				}

				// Initialize Textarea-based editor.
				function initWithTextarea() {
					textarea = jQuery('#' + scope.containerId + ' textarea');

					// If 'id' and 'input-id' attributes are found and equal, move this id to the real input field
					// so that the binding with the '<label/>' element works as expected.
					// (see Directives in 'Rbs/Admin/Assets/js/directives/form-fields.js').
					if (attrs.id && attrs.id === attrs['inputId']) {
						textarea.attr('id', attrs.id);
						element.removeAttr('id');
						element.removeAttr('input-id');
					}

					element.on('blur keyup change', function() {
						scope.$apply(function() {
							if (angular.isObject(ngModel.$viewValue)) {
								ngModel.$viewValue.t = scope.values.markdown;
								ngModel.$setViewValue(ngModel.$viewValue);
							}
							else {
								ngModel.$setViewValue(scope.values.markdown);
							}
						});
					});

					textarea.on('focus', function() {
						element.addClass('focused');
					});

					textarea.on('blur', function() {
						element.removeClass('focused');
					});

					textarea.on('drop', function(event) {
						event.stopPropagation();
						event.preventDefault();
					});

					renderEditorValue = function(v) {
						scope.values.markdown = v;
					};
				}
			}
		};
	}
})();