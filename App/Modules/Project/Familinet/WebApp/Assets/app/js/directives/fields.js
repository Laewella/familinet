/**
 * Copyright (C) 2014 Ready Business System, Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldText
	 * @name appFieldText
	 * @restrict A
	 *
	 * @description
	 * Displays a simple text field in a Document editor.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Text', '<input type="text" class="form-control"/>', 'input');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldEmail
	 * @name appFieldEmail
	 * @restrict A
	 *
	 * @description
	 * Displays a simple text field in a Document editor with email validation.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Email', '<input type="email" class="form-control"/>', 'input');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldUrl
	 * @name appFieldUrl
	 * @restrict A
	 *
	 * @description
	 * Displays a simple text field in a Document editor with URL validation.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Url', '<input type="url" class="form-control"/>', 'input');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldInteger
	 * @name appFieldInteger
	 * @restrict A
	 *
	 * @description
	 * Displays a simple text field in a Document editor with integer number validation.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Integer', '<input type="number" class="form-control" data-ng-pattern="/^\\-?[0-9]+$/"/>', 'input');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldFloat
	 * @name appFieldFloat
	 * @restrict A
	 *
	 * @description
	 * Displays a simple text field in a Document editor with floating number validation.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Float', '<input type="text" class="form-control" data-rbs-smart-float=""/>', 'input');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldBoolean
	 * @name appFieldBoolean
	 * @restrict A
	 *
	 * @description
	 * Displays a field in a Document editor to select a boolean value.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Boolean', '<div data-app-switch=""></div>', '[data-app-switch]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldRichText
	 * @name appFieldRichText
	 * @restrict A
	 *
	 * @description
	 * Displays a rich text field in a Document editor.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('RichText', '<div data-app-rich-text-input=""></div>', '[data-app-rich-text-input]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldDocument
	 * @name appFieldDocument
	 * @restrict A
	 *
	 * @description
	 * Displays a control in a Document editor to select another Document.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 * @param {String} accepted-model Model name of the Documents that can be selected.
	 */
	registerFieldDirective('Document', '<div data-app-document-selector=""></div>', '[data-app-document-selector]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldDocumentArray
	 * @name appFieldDocumentArray
	 * @restrict A
	 *
	 * @description
	 * Displays a control in a Document editor to select other Documents.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 * @param {String} accepted-model Model name of the Documents that can be selected.
	 */
	registerFieldDirective('DocumentArray', '<div data-app-document-array-selector=""></div>', '[data-app-document-array-selector]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldDocumentSelect
	 * @name appFieldDocumentSelect
	 * @restrict A
	 *
	 * @description
	 * Displays a control in a Document editor to select another Document.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 * @param {String} accepted-model Model name of the Documents that can be selected.
	 */
	registerFieldDirective('DocumentSelect', '<div data-app-document-select=""></div>', '[data-app-document-select]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldDate
	 * @name appFieldDate
	 * @restrict A
	 *
	 * @description
	 * Displays a control in a Document editor to select a date.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Date', '<div data-app-date-selector=""></div>', '[data-app-date-selector]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldStarRating
	 * @name appFieldStarRating
	 * @restrict A
	 *
	 * @description
	 * Displays a control in a starr rating to select a rate.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('StarRating', '<div data-app-input-star-rating="" class="form-control-static"></div>',
		'[data-app-input-star-rating]');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appFieldImage
	 * @name appFieldImage
	 * @restrict A
	 *
	 * @description
	 * Displays a control in a Document editor to select an image to upload.
	 *
	 * @param {String} property Name of the property of the Document.
	 * @param {String} label Label of the field.
	 */
	registerFieldDirective('Image', '<rbs-uploader rbs-image-uploader="" storage-name="images" file-accept="image/*"></div>',
		'[rbs-image-uploader]');

	var fieldIdCounter = 0;

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appField
	 * @name appField
	 * @restrict A
	 *
	 * @description
	 * Basic Directive to wrap custom fields.
	 */
	app.directive('appField', ['$compile', function($compile) {
		return {
			restrict: 'A',
			replace: true,
			transclude: true,
			template: fieldTemplate(''),
			scope: false,

			compile: function(element, attributes) {
				var $label = element.find('label').first();
				var fieldId = 'rbs_field_' + (++fieldIdCounter);
				var required = (attributes.required === 'true');
				$label.html(attributes.label);
				$label.attr('for', fieldId);

				return function link(scope, linkElement, linkAttributes, controller, transcludeFn) {
					transcludeFn(function(clone) {
						linkElement.find('.controls').append(clone);
						var $input = linkElement.find('.controls [ng-model], .controls [data-ng-model]').attr('id', fieldId);
						if (required) {
							linkElement.addClass('required');
							$input.attr('required', 'required');
						}

						if (linkAttributes['handlePersist'] == 'true') {
							var html;
							if (linkAttributes.property.indexOf('.') === -1) {
								html = '<span> <input type="checkbox" data-ng-model="persist.document.' + linkAttributes.property + '"';
							}
							else {
								html = '<span> <input type="checkbox" data-ng-model="persist.' + linkAttributes.property + '"';
							}
							html += ' title="(= \'m.project.library.app.handle_persist\'|i18n|ucf =)" /></span>';
							linkElement.find('label').first().append($compile(html)(scope));
						}
					});
				};
			}
		};
	}]);

	function fieldTemplate(contents, omitLabel) {
		if (omitLabel) {
			return '<div class="form-group property"><div class="col-lg-12 controls">' + contents + '</div></div>';
		}
		return '<div class="form-group property">' +
			'<label class="col-lg-3 control-label"></label>' +
			'<div class="col-lg-9 controls">' + contents + '</div>' +
			'</div>';
	}

	function registerFieldDirective(name, tpl, selector, omitLabel) {
		app.directive('appField' + name, ['RbsChange.Utils', function(Utils) {
			return {
				restrict: 'A',
				replace: true,
				transclude: true,
				template: fieldTemplate(tpl + '<div data-ng-transclude=""></div>', omitLabel),
				scope: false,

				compile: function(element, attributes) {
					fieldCompile(element, attributes, selector, Utils);
					return function() {};
				}
			};
		}]);
	}

	/**
	 * Generic compile function for all field Directives.
	 * @param element
	 * @param attributes
	 * @param inputSelector
	 * @param Utils
	 */
	function fieldCompile(element, attributes, inputSelector, Utils) {
		if (!attributes.property) {
			throw new Error("Missing 'property' attribute on app-field-* directive");
		}

		var $lbl = element.find('label').first(),
			$ipt = element.find(inputSelector).first(),
			fieldId, property, ngModel, p;

		// Determine property's name and ngModel value.
		if ((p = attributes.property.indexOf('.')) === -1) {
			property = attributes.property;
			ngModel = 'document.' + property;
		}
		else {
			ngModel = attributes.property;
			property = attributes.property.substr(p + 1);
		}

		// Bind label and input field (unique 'for' attribute).
		fieldId = 'app_field_' + property.replace(/[^a-z0-9]/ig, '_') + '_' + (++fieldIdCounter);
		var html = attributes.label;
		if (attributes['handlePersist']) {
			if (attributes.property.indexOf('.') === -1) {
				html += ' <input type="checkbox" data-ng-model="persist.document.' + attributes.property + '"';
			}
			else {
				html += ' <input type="checkbox" data-ng-model="persist.' + attributes.property + '"';
			}
			html += ' title="(= \'m.project.library.app.handle_persist\'|i18n|ucf =)" />';
		}
		$lbl.html(html).attr('for', fieldId);
		$ipt.attr('id', fieldId).attr('input-id', fieldId).attr('name', property);

		// Init input field.
		$ipt.attr('ng-model', ngModel);

		// Transfer most attributes to the input field.
		angular.forEach(attributes, function(value, name) {
			if (name === 'required') {
				if (value === 'true' || value === 'required') {
					$ipt.attr('required', 'required');
					element.addClass('required');
				}
				element.removeAttr(name);
			}
			else if (name === 'inputClass') {
				$ipt.addClass(value);
				element.removeAttr(name);
			}
			else if (name === 'label') {
				$ipt.attr('property-label', value);
			}
			else if (shouldTransferAttribute(name)) {
				name = Utils.normalizeAttrName(name);
				$ipt.attr(name, value);
				element.removeAttr(name);
			}
		});
	}

	function shouldTransferAttribute(name) {
		return name !== 'id'
			&& name !== 'class'
			&& name !== 'property'
			&& name !== 'label'
			&& name !== 'ngHide' && name !== 'dataNgHide'
			&& name !== 'ngShow' && name !== 'dataNgShow'
			&& name !== 'ngIf' && name !== 'dataNgIf'
			&& name !== 'ngSwitchWhen' && name !== 'dataNgSwitchWhen'
			&& name.charAt(0) !== '$'
			&& name.substring(0, 8) !== 'appField';
	}
})();