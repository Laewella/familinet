(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc service
	 * @name RbsChange.service:Document
	 * @description Document service.
	 */
	documentService.$inject = ['$routeParams', '$rootScope', '$templateCache', 'RbsChange.REST', 'RbsChange.Breadcrumb', 'RbsChange.i18n',
		'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter'];
	app.service('RbsChange.Document', documentService);

	function documentService($routeParams, $rootScope, $templateCache, REST, Breadcrumb, i18n, NotificationCenter, ErrorFormatter) {
		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#initDetailView
		 *
		 * @description Init the scope for a document detail view.
		 *
		 * @param {Object} scope The controller's scope.
		 * @param {String} modelName The document model name.
		 * @return {Object} The promise of document load.
		 */
		this.initDetailView = function(scope, modelName) {
			var promise = REST.resource(modelName, $routeParams.id);
			promise.then(
				function(doc) {
					scope.modelName = modelName;
					scope.document = doc;
				},
				function(reason) {
					pendingNotifications.push([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.id +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
			return promise;
		};

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#initDetailView
		 *
		 * @description Init the scope for a document edition view.
		 *
		 * @param {Object} scope The controller's scope.
		 * @param {String} modelName The document model name.
		 * @return {Object} The promise of document load.
		 */
		this.initEditView = function(scope, modelName) {
			var promise = REST.resource(modelName, $routeParams.id);
			promise.then(
				function(doc) {
					scope.modelName = modelName;
					scope.document = doc;
				},
				function(reason) {
					pendingNotifications.push([
						i18n.trans('m.project.familinet.app.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.id +
						', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
			return promise;
		};

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#getItemTemplateName
		 *
		 * @param {Object} item
		 * @returns {string}
		 */
		this.getItemTemplateName = function(item) {
			if (!item || !item.model) {
				return 'picker-item-default.html';
			}

			var tplName = 'picker-item-' + item.model + '.html';
			return $templateCache.get(tplName) ? tplName : 'picker-item-default.html';
		};

		var pendingNotifications = [];

		$rootScope.$on('$routeChangeSuccess', function() {
			for (var i = 0; i < pendingNotifications.length; i++) {
				NotificationCenter.error(pendingNotifications[i][0], pendingNotifications[i][1], pendingNotifications[i][2]);
			}
			pendingNotifications = [];
		});
	}
})();