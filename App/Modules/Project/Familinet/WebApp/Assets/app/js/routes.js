(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * Routes and URL definitions.
	 */
	app.config(['$provide', function($provide) {
		$provide.decorator('RbsChange.UrlManager', ['$delegate', function($delegate) {
			__change.routes['/'] = {
				name: 'home',
				module: 'Project_Familinet',
				rule: {
					templateUrl: 'app/html/home.html',
					labelKey: 'm.project.familinet.app.home | ucf',
					icon: 'home',
					controller: 'EmptyController'
				}
			};
			$delegate.applyConfig(__change.routes);

			return $delegate;
		}]);
	}]);

	app.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/authenticate', {
			template: '<div></div>',
			controller: ['$location', 'OAuthService', function($location, OAuthService) {
				var oauth_token, oauth_verifier;
				if ($location.search()['oauth_token'] && $location.search()['oauth_verifier']) {
					oauth_token = $location.search()['oauth_token'];
					oauth_verifier = $location.search()['oauth_verifier'];
				}
				else if (window.location.search.substr(0, 1) == '?') {
					var params = window.location.search.substr(1).split('&');
					for (var i = 0; i < params.length; i++) {
						var elem = params[i].split('=');
						if (elem[0] == 'oauth_token') {
							oauth_token = elem[1];
						}
						else if (elem[0] == 'oauth_verifier') {
							oauth_verifier = elem[1];
						}
					}
				}
				OAuthService.getAccessToken(oauth_token, oauth_verifier);
			}]
		});
		$routeProvider.otherwise({ redirectTo: '/' });
	}]);

	app.controller('EmptyController', ['$scope', '$http', '$routeParams', EmptyController]);
	function EmptyController($scope, $http, $routeParams) {
	}
})();