(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('appDateSelector', ['$filter', function($filter) {
		return {
			restrict: 'A',
			templateUrl: 'app/html/date-selector.html',
			require: 'ngModel',
			replace: true,
			scope: true,
			transclude: true,

			link: function(scope, elm, attrs, ngModel) {
				scope.showNowButton = attrs.showNowButton === 'true';

				scope.dateOptions = {
					formatYear: 'yy',
					startingDay: 1
				};

				scope.clear = function() {
					ngModel.$setViewValue(null);
					ngModel.$render();
				};

				scope.now = function() {
					ngModel.$setViewValue(new Date());
					ngModel.$render();
				};

				scope.open = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					scope.opened = true;
				};

				// modelValue => viewValue
				// string => Date
				ngModel.$formatters.unshift(function(modelValue) {
					if (modelValue) {
						return new Date(modelValue);
					}
					return null;
				});

				scope.updateDate = function() {
					scope.$evalAsync(function() {
						ngModel.$setViewValue(scope.date ? angular.copy(scope.date) : null);
					});
				};

				// viewValue => modelValue
				// Date => string
				ngModel.$parsers.unshift(function(viewValue) {
					if (viewValue instanceof Date && (!isNaN(viewValue.getTime()))) {
						// Date format: "2014-06-02T12:24:03+0000".
						return $filter('date')(viewValue, 'yyyy-MM-ddThh:mm:ss') + '+0000'; // Using Z format for the timezone leads to unexpected results.
					}
					return null;
				});

				ngModel.$render = function() {
					scope.date = ngModel.$viewValue;
				};

				// If 'id' and 'input-id' attributes are found are equal, move this id to the real input field
				// so that the binding with the '<label/>' element works as expected
				// (see Directives in 'fields.js').
				if (attrs.id && attrs.id === attrs.inputId) {
					elm.find('[data-role="input-date"]').first().attr('id', attrs.id);
					elm.removeAttr('id');
					elm.removeAttr('input-id');
				}
			}
		};
	}]);
})();