<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Documents;

/**
 * @name \Rbs\Collection\Documents\CollectionItem
 */
class CollectionItem extends \Compilation\Rbs\Collection\Documents\CollectionItem implements \Change\Collection\ItemInterface
{
	/**
	 * @return string|null
	 */
	public function getLabel()
	{
		return $this->getTitle();
	}

	/**
	 * @return string|null
	 */
	public function getTitle()
	{
		$localization = $this->getCurrentLocalization();
		if ($localization->isNew())
		{
			$localization = $this->getRefLocalization();
		}
		$title = $localization->getTitle();
		return $title === null ? $this->getValue() : $title;
	}

	/**
	 * @param array $restValue
	 * @param \Change\Http\UrlManager $urlManager
	 */
	public function processRestValue($restValue, $urlManager)
	{
		unset($restValue['locked']);
		parent::processRestValue($restValue, $urlManager);
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getAJAXData', function ($event) {$this->onGetAJAXData($event);}, 5);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetAJAXData(\Change\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Collection\Presentation\CollectionItemDataComposer($event))->toArray());
	}
}