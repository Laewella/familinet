<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Documents;

/**
 * @name \Rbs\Collection\Documents\Collection
 */
class Collection extends \Compilation\Rbs\Collection\Documents\Collection implements \Change\Collection\CollectionInterface
{
	/**
	 * @param string $value
	 * @return \Rbs\Collection\Documents\CollectionItem|null
	 */
	public function getItemByValue($value)
	{
		foreach ($this->getItems() as $item)
		{
			if ($item->getValue() == $value)
			{
				return $item;
			}
		}
		return null;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) {$this->onSave($event);}, 3);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onSave($event); }, 3);
		$invalidateCacheCallBack = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Collection\Events\CollectionResolver())->invalidateCodesCache($event->getApplicationServices()->getCacheManager());
		};
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, $invalidateCacheCallBack, 1);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, $invalidateCacheCallBack, 1);

		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onSave(\Change\Documents\Events\Event $event)
	{
		if ($this->isNew() || $this->isPropertyModified('items'))
		{
			$error = false;
			$values = [];
			$duplicatedItems = null;
			$items = $this->getItems();
			foreach ($items as $item)
			{
				$value = $item->getValue();
				if (in_array($value, $values))
				{
					$error = true;
					$duplicatedItems = [
						'item1Label' => $item->getLabel(),
						'item2Label' => array_search($value, $values),
						'value' => $item->getValue()
					];
					break;
				}
				else
				{
					$values[$item->getLabel()] = $value;
				}
			}
			if ($error)
			{
				$errors = $event->getParam('propertiesErrors', []);
				$errors['items'][] = new \Change\I18n\PreparedKey(
					'm.rbs.collection.admin.collection_error_duplicated_item_value', ['ucf'], $duplicatedItems
				);
				$event->setParam('propertiesErrors', $errors);
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Collection\Presentation\CollectionDataComposer($event))->toArray());
	}

	protected function onCreate()
	{
		if (\Change\Stdlib\StringUtils::isEmpty($this->getCode()))
		{
			$this->setCode(uniqid('COLLECTION-', false));
		}
	}

	protected function onUpdate()
	{
		if ($this->isPropertyModified('code') && ($this->getLocked() || \Change\Stdlib\StringUtils::isEmpty($this->getCode())))
		{
			$this->setCode($this->getCodeOldValue());
		}

		$oldItems = $this->getItems()->getDefaultDocuments();
		if (is_array($oldItems) && count($oldItems))
		{
			/** @var $oldItem \Rbs\Collection\Documents\CollectionItem */
			foreach ($oldItems as $oldItem)
			{
				if ($oldItem->getLocked())
				{
					$item = $this->getItemByValue($oldItem->getValue());
					if ($item)
					{
						$item->setLocked(true);
					}
					else
					{
						$this->getItems()->add($oldItem);
					}
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		$document = $event->getDocument();
		if (!$document instanceof \Rbs\Collection\Documents\Collection)
		{
			return;
		}

		if ($document->getLocked())
		{
			$restResult = $event->getParam('restResult');
			if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
			{
				$restResult->removeRelAction('delete');
			}
			elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
			{
				$restResult->removeRelAction('delete');
			}
		}
	}

	/**
	 * @param $name
	 * @param $value
	 * @param \Change\Http\Event $event
	 * @return bool
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		$result = parent::processRestData($name, $value, $event);
		if ($this->isPropertyModified('code') && $this->getLocked() === true)
		{
			$result = new \Change\Http\Rest\V1\ErrorResult('COLLECTION-LOCKED', 'Can not modify the code of a locked collection',
				\Zend\Http\Response::STATUS_CODE_409);
			$event->setResult($result);
			return false;
		}
		if ($this->isPropertyModified('locked') && $this->getLockedOldValue() === true)
		{
			$result = new \Change\Http\Rest\V1\ErrorResult('COLLECTION-LOCKED', 'Can not unlock locked collection',
				\Zend\Http\Response::STATUS_CODE_409);
			$event->setResult($result);
			return false;
		}
		return $result;
	}

	/**
	 * @return array
	 */
	public function getInputValues()
	{
		$eventManager = $this->getEventManager();
		$params = $eventManager->prepareArgs(['inputValues' => []]);
		$eventManager->triggerEvent(new \Change\Documents\Events\Event('getInputValues', $this, $params));
		$inputValues = array_values(array_unique($params['inputValues']));
		sort($inputValues);
		return $inputValues;
	}
}
