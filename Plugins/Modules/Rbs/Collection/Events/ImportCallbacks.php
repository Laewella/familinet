<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Events;

/**
 * @name \Rbs\Collection\Events\ImportCallbacks
 */
class ImportCallbacks
{
	/**
	 * @param array $jsonDocument
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return \Rbs\Collection\Documents\Collection
	 */
	public function resolveCollection($jsonDocument, $documentManager)
	{
		$code = isset($jsonDocument['code']) ? $jsonDocument['code'] : null;
		if (!$code)
		{
			return null;
		}
		$query = $documentManager->getNewQuery('Rbs_Collection_Collection');
		return $query->andPredicates($query->eq('code', $code))->getFirstDocument();
	}
}