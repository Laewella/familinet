(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsCollectionCollectionNew', Editor);
	app.directive('rbsDocumentEditorRbsCollectionCollectionEdit', Editor);
	function Editor() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.items)) {
						scope.document.items = [];
					}
				};
			}
		};
	}

	app.directive('rbsCollectionCollectionItems', ['RbsChange.InlineDocument', 'RbsChange.REST', rbsCollectionCollectionItems]);
	function rbsCollectionCollectionItems(InlineDocument, REST) {
		return {
			restrict: 'A',
			link: function(scope, element, attributes) {
				var itemsModelName = attributes['rbsCollectionCollectionItems'];
				var intputValuesLoadRequested = false;

				var newItem = {
					value: null,
					isUsed: false
				};

				var pagination = {
					currentPage: 1,
					totalItems: 0,
					itemsPerPage: null,
					currentItems: []
				};

				function canAddNewItem() {
					if (!newItem.value) {
						return false;
					}
					for (var i = 0; i < scope.document.items.length; i++) {
						if (scope.document.items[i].value == newItem.value) {
							newItem.isUsed = true;
							return false;
						}
					}
					newItem.isUsed = false;
					return true;
				}

				function addNewItem() {
					var item = doAddNewItem(newItem.value);
					newItem.value = null;
					return item;
				}

				function doAddNewItem(value) {
					var inline = {
						model: itemsModelName,
						value: value,
						label: value
					};

					var LCID = scope.itemsManager.LCID;
					InlineDocument.defineLocalization(inline, LCID);
					inline.LCID[LCID].title = value;

					scope.document.items.push(inline);
					
					return inline;
				}

				function canDeleteItem(item) {
					return item && !item.locked;
				}

				function buildPageItems() {
					if (angular.isArray(scope.document.items)) {
						pagination.offset = (pagination.currentPage - 1) * pagination.itemsPerPage;
						var maxOffset = pagination.offset + pagination.itemsPerPage;
						if (maxOffset > scope.document.items.length) {
							maxOffset = scope.document.items.length;
						}
						pagination.currentItems = scope.document.items.slice(pagination.offset, maxOffset);
					}
					else {
						pagination.offset = 0;
						pagination.currentItems = [];
					}
				}

				function buildNewInputValue() {
					scope.itemsManager.inputValues = [];
					var check = {};
					angular.forEach(scope.document.items, function(item) {
						if (angular.isString(item.value)) {
							check[item.value] = true;
						}
					});
					angular.forEach(scope.itemsManager.allInputValues, function(v) {
						if (angular.isString(v) && !check[v]) {
							scope.itemsManager.inputValues.push(v);
						}
					});
				}

				function loadInputValues() {
					intputValuesLoadRequested = true;
					scope.itemsManager.inputValues = [];
					var p = REST.call(scope.document.META$.url + '/InputValues/', {});
					p.then(function(data) {
						if (angular.isArray(data)) {
							scope.itemsManager.allInputValues = data;
							buildNewInputValue();
						}
					});
				}

				function addInputValues() {
					buildNewInputValue();
					var entries = {};
					angular.forEach(scope.document.items, function(item) {
						entries[item.value] = true;
					});

					angular.forEach(scope.itemsManager.inputValues, function(v) {
						if (!entries.hasOwnProperty(v)) {
							doAddNewItem(v);
						}
					});
				}

				scope.itemsManager = {
					LCID: null,
					newItem: newItem,
					canAddNewItem: canAddNewItem,
					addNewItem: addNewItem,
					canDeleteItem: canDeleteItem,
					pagination: pagination,
					inputValues: [],
					allInputValues: [],
					addInputValues: addInputValues
				};

				scope.$watchCollection('document.items', function (items) {
					pagination.totalItems = angular.isArray(items) ? items.length : 0;
					buildPageItems();
					buildNewInputValue();
				});

				scope.$watch('itemsManager.pagination.currentPage', function () {
					buildPageItems();
				});

				scope.$watch('itemsManager.pagination.itemsPerPage', function () {
					buildPageItems();
				});

				scope.$watch('document', function (doc) {
					if (!intputValuesLoadRequested && angular.isObject(doc) && doc.META$) {
						loadInputValues();
					}
				});
			}
		};
	}

	app.directive('rbsCollectionCollectionItem', ['RbsChange.InlineDocument', function(InlineDocument) {
		return {
			restrict: 'A',
			link: function(scope) {
				var item = scope.item;

				scope.$watch('item.value', function(value, oldValue) {
					if (item) {
						var LCID = scope.itemsManager.LCID;
						if (LCID) {
							if (item.LCID && item.LCID[LCID] && item.LCID[LCID].title == oldValue) {
								item.LCID[LCID].title = value;
							}
						}
					}
				});

				scope.$watch('itemsManager.LCID', function(LCID) {
					InlineDocument.defineLocalization(item, LCID);
					if (LCID && item) {
						if (item.LCID && item.LCID[LCID] && !item.LCID[LCID].title) {
							item.LCID[LCID].title = item.value;
						}
					}
				});
			}
		};
	}]);
})();