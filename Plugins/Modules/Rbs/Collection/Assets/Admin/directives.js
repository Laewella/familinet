(function() {
	'use strict';

	var app = angular.module('RbsChange');

	function parseParamsAttr(paramsAttr, collectionCode) {
		var params = {
			code: collectionCode
		};
		if (paramsAttr) {
			var parts = paramsAttr.split(';');
			angular.forEach(parts, function(value) {
				var values = value.split(':');
				params[values[0].trim()] = values[1].trim();
			});
		}
		return params;
	}

	/**
	 * @ngdoc controller
	 * @id RbsChange.controller:RbsCollectionController
	 * @name RbsCollectionController
	 *
	 * @description
	 * This controller loads the items of a collection.
	 *
	 * @param {string} collectionCode The collection code.
	 * @param {string=} collectionParams The collection parameters (in form: `param1:value1;param2:value2;...`).
	 * @param {string=} collectionFilter If set, only items with label or value containing the given string will be returned.
	 * @param {string=} variableName The variable name in the scope where the items are set (default: `pxCollection`).
	 *
	 * @example
	 * Minimal example:
	 * ```html
	 * <select class="form-control"
	 *     data-ng-controller="RbsCollectionController"
	 *     data-collection-code="Rbs_Geo_All_Countries_Codes"
	 *     data-ng-options="item.value as item.label for item in pxCollection"
	 *     data-ng-model="country">
	 * </select>
	 * ```
	 *
	 * Full example:
	 * ```html
	 * <select class="form-control"
	 *     data-ng-controller="RbsCollectionController"
	 *     data-collection-code="Rbs_Generic_Typologies"
	 *     data-collection-params="modelName:Rbs_Catalog_Product"
	 *     data-collection-filter="(= filter =)"
	 *     data-variable-name="typologyOptions"
	 *     data-ng-options="item.value as item.label for item in typologyOptions"
	 *     data-ng-model="typology">
	 * </select>
	 * ```
	 */
	app.controller('RbsCollectionController', ['$scope', '$attrs', '$parse', 'RbsChange.REST', 'RbsChange.Utils', RbsCollectionController]);
	function RbsCollectionController(scope, attrs, $parse, REST, Utils) {
		var currentKey, itemsFilter, items = [];
		//noinspection JSUnresolvedVariable
		var variableSetter = $parse(attrs.variableName || 'pxCollection').assign;

		attrs.$observe('collectionCode', loadCollection);
		attrs.$observe('collectionParams', loadCollection);
		attrs.$observe('collectionFilter', function(filter) {
			itemsFilter = filter;
			loadCollection();
		});

		// Load Collection's items.
		function loadCollection() {
			var collectionCode = attrs.collectionCode;
			if (!collectionCode) {
				return;
			}

			//noinspection JSUnresolvedVariable
			var collectionParams = attrs.collectionParams;
			var key = collectionCode + '||' + collectionParams;
			if (currentKey === key) {
				setFilteredItems();
				return;
			}
			currentKey = key;

			var params = parseParamsAttr(collectionParams, collectionCode);
			params.asArray = true;

			REST.action('collectionItems', params).then(
				function(data) {
					items = data.items;
					setFilteredItems();
				},
				function(data) {
					console.error('Unable to load Collection "' + collectionCode + '".', data);
				}
			);
		}

		function setFilteredItems() {
			items = items || [];
			var currentItems = [];
			if (itemsFilter) {
				angular.forEach(items, function(item) {
					if (Utils.containsIgnoreCase(item.label, itemsFilter) || Utils.containsIgnoreCase(item.value, itemsFilter)) {
						currentItems.push(item);
					}
				});
			}
			else {
				currentItems = items;
			}
			variableSetter(scope, currentItems);
		}
	}

	/**
	 * <rbs-collection-item-selector ng-model="" collection-code="" [collection-params="param1:value1;param2:value2;..."] />
	 */
	app.directive('rbsCollectionItemSelector', ['RbsChange.REST', rbsCollectionItemSelectorDirective]);

	function rbsCollectionItemSelectorDirective(REST) {
		return {
			restrict: 'E',
			require: 'ngModel',
			scope: true,
			template: '<div class="radio" data-ng-repeat="(value, item) in List.items"><label>' +
			'	<input type="radio" data-ng-model="List.value" value="(= value =)"/> (= item.label =)' +
			'</label></div>',

			link: function(scope, elm, attrs, ngModel) {
				if (!attrs.collectionCode) {
					throw new Error("Missing required attribute: 'collection-code'.");
				}

				// Load Collection's items.
				scope.List = { loading: true, value: null };

				var collectionParamsReady = !elm.is('[collectionParams]');
				if (!collectionParamsReady) {
					attrs.$observe('collectionParams', function(value) {
						if (value) {
							collectionParamsReady = true;
							loadCollection();
						}
					});
				}

				var ngModelReady = false;

				ngModel.$render = function() {
					ngModelReady = true;
					loadCollection();
				};

				function loadCollection() {
					if (!ngModelReady || !collectionParamsReady) {
						return;
					}

					REST.action('collectionItems', parseParamsAttr(attrs['collectionParams'], attrs.collectionCode)).then(function(data) {
						scope.List.loading = false;
						scope.List.items = data.items;
						scope.List.value = ngModel.$viewValue;
					});

					scope.$watch('List.value', function(value, oldValue) {
						//noinspection JSValidateTypes
						if (value !== null && value !== oldValue) {
							ngModel.$setViewValue(value);
						}
					}, true);
				}
			}
		};
	}

	/**
	 * <rbs-collection-items-selector ng-model="" collection-code="" [collection-params="param1:value1;param2:value2;..."] />
	 *
	 * @field ngModel is array of selected values
	 */
	app.directive('rbsCollectionItemsSelector', ['RbsChange.REST', '$filter', rbsCollectionItemsSelectorDirective]);

	function rbsCollectionItemsSelectorDirective(REST, $filter) {
		return {
			restrict: 'E',
			require: 'ngModel',
			scope: true,
			template: '<div class="checkbox" data-ng-repeat="(value, item) in List.items"><label>' +
			'	<input type="checkbox" data-ng-model="List.values[value]" value="true"/> (= item.label =)' +
			'</label></div>',

			link: function(scope, elm, attrs, ngModel) {
				if (!attrs.collectionCode) {
					throw new Error("Missing required attribute: 'collection-code'.");
				}

				// Load Collection's items.
				scope.List = { loading: true, values: null };

				var collectionParamsReady = !elm.is('[collectionParams]');
				if (!collectionParamsReady) {
					attrs.$observe('collectionParams', function(value) {
						if (value) {
							collectionParamsReady = true;
							loadCollection();
						}
					});
				}

				var ngModelReady = false;

				ngModel.$render = function() {
					ngModelReady = true;
					loadCollection();
				};

				function loadCollection() {
					if (!ngModelReady || !collectionParamsReady) {
						return;
					}

					REST.action('collectionItems', parseParamsAttr(attrs['collectionParams'], attrs.collectionCode)).then(function(data) {
						scope.List.loading = false;
						scope.List.items = data.items;
						scope.$parent.$parent.hasCollectionItemsSelectorValues = Object.keys(data.items).length > 0;
						var n = {};
						for (var i in ngModel.$viewValue) {
							n[ngModel.$viewValue[i]] = true;
						}
						scope.List.values = n;
					});

					scope.$watch('List.values', function(value, oldValue) {
						//noinspection JSValidateTypes
						if (value !== null && value !== oldValue) {
							var n = [];
							for (var v in value) {
								if (value[v] === true) {
									n.push(v);
								}
							}
							ngModel.$setViewValue(n);
						}
					}, true);
				}
			}
		};
	}
})();