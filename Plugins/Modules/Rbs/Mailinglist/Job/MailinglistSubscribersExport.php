<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Job;

/**
 * @name \Rbs\Mailinglist\Job\MailinglistSubscribersExport
 */
class MailinglistSubscribersExport
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();
		/* @var $dm \Change\Documents\DocumentManager */
		/* @var $tm \Change\Transaction\TransactionManager */
		$dm = $applicationServices->getDocumentManager();
		$i18n = $applicationServices->getI18nManager();
		$applicationServices->getStorageManager(); //Needed for StreamWrapper
		$tm = $applicationServices->getTransactionManager();

		$mailingListId = $job->getArgument('mailingListId');
		$LCID = $job->getArgument('LCID');
		$callerId = $job->getArgument('callerId');
		$chunk = $job->getArgument('chunk') ?: 1000;

		/** @var \Rbs\User\Documents\User $callerUser */
		$callerUser = $dm->getDocumentInstance($callerId);
		/** @var \Rbs\Mailinglist\Documents\MailingList $mailingList */
		$mailingList = $dm->getDocumentInstance($mailingListId, 'Rbs_Mailinglist_MailingList');

		$uid = 0;
		$website = $mailingList->getWebsite();
		$um = $website->getUrlManager($LCID);
		$mailingListName = $mailingList->getCurrentLocalization()->getTitle();

		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$query = $this->getQuery($dbProvider, $mailingListId, $LCID, $chunk);
		$rc = $query->getRowsConverter()
			->addIntCol('uid', 'listId')
			->addStrCol('email', 'subscriberData', 'secret');

		$fileName = str_replace([' ', '/', '*', '(', ')', '[', ']', '|', '\\', '\'', '"'], '_', $mailingListName);
		$fileName = \Change\Stdlib\StringUtils::stripAccents($fileName);
		$csvStorageURI = 'change://Rbs_Mailinglist/export' . $fileName . '.csv';
		$csvFileHandle = fopen($csvStorageURI, 'w+');

		$profileFields = $event->getParam('profileFields', ['firstName', 'lastName', 'phone', 'birthDate']);

		if (!fputcsv($csvFileHandle, $this->getHeader($profileFields), ';', '"', '\\'))
		{
			$this->sendNotification($dm, $tm, $i18n, $callerUser, $mailingListName, $mailingListId, $LCID, false);
			$event->setResultArgument('error', ' File Generation Failure Append data');
			$event->success();
			return;
		}

		do
		{
			$query = $query->bindParameter('document_id', $uid);
			$matchRows = $query->getResults($rc);
			$countRows = count($matchRows);
			foreach ($matchRows as $subscriberRow)
			{
				$csvData = $this->convertRow($um, $subscriberRow, $LCID, $profileFields);
				if (!fputcsv($csvFileHandle, $csvData, ';', '"', '\\'))
				{
					$this->sendNotification($dm, $tm, $i18n, $callerUser, $mailingListName, $mailingListId, $LCID, false);
					$event->setResultArgument('error', ' File Generation Failure Append data');
					$event->success();
					return;
				}
				$uid = $subscriberRow['uid'];
			}
		}
		while ($countRows === $chunk);
		fclose($csvFileHandle);
		try
		{
			$tm->begin();
			$metaArray = $mailingList->getMeta('export');
			if (!$metaArray)
			{
				$metaArray = [];
			}
			$metaArray['exportDate'] = (new \DateTime())->format(\DateTime::ATOM);
			$metaArray['fileURI'] = $csvStorageURI;
			$mailingList->setMeta('export', $metaArray);
			$mailingList->saveMetas();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
		}
		$this->sendNotification($dm, $tm, $i18n, $callerUser, $mailingListName, $mailingListId, $LCID, true);
		$event->setResultArgument('csvFile', $csvStorageURI);
		$event->success();
	}

	/**
	 * @param \Change\Documents\DocumentManager $dm
	 * @param \Change\Transaction\TransactionManager $tm
	 * @param \Change\I18n\I18nManager $i18n
	 * @param \Rbs\User\Documents\User $user
	 * @param string $mailingListName
	 * @param int $mailingListId
	 * @param string $LCID
	 * @param bool $isSuccess
	 */
	protected function sendNotification($dm, $tm, $i18n, $user, $mailingListName, $mailingListId, $LCID, $isSuccess)
	{
		$params = ['MAILINGLISTNAME' => $mailingListName, 'targetId' => $mailingListId];

		/* @var $notification \Rbs\Notification\Documents\Notification */
		$notification = $dm->getNewDocumentInstanceByModelName('Rbs_Notification_Notification');
		$notification->setUserId($user->getId());
		$notification->setCode('Rbs_Simpleform_Response');
		$notification->getCurrentLocalization()->setMessage($i18n->transForLCID($LCID,
			($isSuccess ? 'm.rbs.segmentation.admin.snapshot_export_message_ok' :
				'm.rbs.segmentation.admin.snapshot_export_message_ko'), ['ucf'], $params));
		$notification->setParams($params);
		try
		{
			$tm->begin();
			$notification->save();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
		}
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param int $mailingListId
	 * @param string $LCID
	 * @param int $limit
	 * @return \Change\Db\Query\SelectQuery
	 */
	protected function getQuery($dbProvider, $mailingListId, $LCID, $limit)
	{
		$nqb = $dbProvider->getNewQueryBuilder();
		$fb = $nqb->getFragmentBuilder();
		$nqb->select(
			$fb->alias($fb->column('document_id', 's'), 'uid'),
			$fb->alias($fb->column('document_id', 'm'), 'listId'),
			$fb->alias($fb->column('email', 's'), 'email'),
			$fb->alias($fb->column('subscriberData', 's'), 'subscriberData'),
			$fb->alias($fb->column('secret', 's'), 'secret')
		);
		$nqb->from($fb->alias($fb->table('rbs_mailinglist_rel_subscriber'), 'r'));
		$nqb->innerJoin(
			$fb->alias($fb->table('rbs_mailinglist_doc_subscriber'), 's'),
			$fb->eq($fb->column('document_id', 'r'), $fb->column('document_id', 's'))
		);
		$nqb->innerJoin(
			$fb->alias($fb->table('rbs_mailinglist_doc_mailinglist_i18n'), 'm'),
			$fb->logicAnd(
				$fb->eq($fb->column('document_id', 'm'), $fb->number($mailingListId)),
				$fb->eq($fb->column('lcid', 'm'), $fb->string($LCID))
			)
		);
		$nqb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('userlcid', 's'), $fb->string($LCID)),
				$fb->eq($fb->column('active', 's'), true),
				$fb->eq($fb->column('relatedid', 'r'), $fb->number($mailingListId)),
				$fb->gt($fb->column('document_id', 's'), $fb->integerParameter('document_id'))
			)
		);
		$nqb->orderAsc($fb->column('document_id', 's'));
		$query = $nqb->query();
		$query->setStartIndex(0);
		$query->setMaxResults($limit);
		return $query;
	}

	/**
	 * @param array $profileFields
	 * @return array
	 */
	protected function getHeader($profileFields)
	{
		$csvHeader = ['email', 'unsubscribe url', 'manage subscriptions url'];
		$csvHeader = array_merge(array_values($csvHeader), array_values($profileFields));
		return $csvHeader;
	}

	/**
	 * @param \Change\Http\Web\UrlManager $um
	 * @param array $subscriberRow
	 * @param string $LCID
	 * @param array $profileFields
	 * @return array
	 */
	protected function convertRow($um, $subscriberRow, $LCID, $profileFields)
	{
		$subscriberData = json_decode($subscriberRow['subscriberData'], true);
		$csvData = [];
		$csvData[] = $subscriberRow['email'];
		$old = $um->absoluteUrl(true);
		$location = $um->getByFunction('Rbs_Mailinglist_ManageSubscriptions', [$subscriberRow['listId']], $LCID);
		$csvData[] = $location ? $location->normalize()->toString() : '';
		$location = $um->getByFunction('Rbs_Mailinglist_ManageSubscriptions',
			['token' => $subscriberRow['secret'], 'email' => $subscriberRow['email']], $LCID);
		$csvData[] = $location ? $location->normalize()->toString() : '';
		$um->absoluteUrl($old);
		foreach ($profileFields as $field)
		{
			$csvData[] =
				array_key_exists($field, $subscriberData['profileData']) ? $subscriberData['profileData'][$field] : '';
		}
		return $csvData;
	}
}