<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Job;

/**
 * @name \Rbs\Mailinglist\Job\CleanSubscriptionRequest
 */
class CleanSubscriptionRequestTable
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$tm = $event->getApplicationServices()->getTransactionManager();

		try
		{
			$tm->begin();

			$qb = $event->getApplicationServices()->getDbProvider()->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();

			$qb->delete($fb->table('rbs_mailinglist_subscription_request'));
			$qb->where($fb->lt($fb->column('max_activation_date'), $fb->dateTimeParameter('now')));
			$iq = $qb->deleteQuery();
			$iq->bindParameter('now', new \DateTime());
			$iq->execute();

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		//Reschedule the job in 24h
		$now = new \DateTime();
		$event->reported($now->add(new \DateInterval('PT24H')));
	}
}