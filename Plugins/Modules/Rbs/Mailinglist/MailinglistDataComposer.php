<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist;

/**
 * @name \Rbs\Mailinglist\MailinglistDataComposer
 */
class MailinglistDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/** @var \Rbs\Mailinglist\Documents\MailingList $mailinglist */
	private $mailinglist;

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function __construct(\Change\Documents\Events\Event $event)
	{
		$this->mailinglist = $event->getDocument();
		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());
	}

	/**
	 * Fill dataSets for getAJAXData method.
	 */
	protected function generateDataSets()
	{
		if (!$this->mailinglist)
		{
			$this->dataSets = [];
			return;
		}
		$this->dataSets = [
			'common' => [
				'id' => $this->mailinglist->getId(),
				'LCID' => $this->mailinglist->getCurrentLCID(),
				'title' => $this->mailinglist->getCurrentLocalization()->getTitle(),
				'description' => $this->formatRichText($this->mailinglist->getCurrentLocalization()->getDescription()),
				'priority' => $this->mailinglist->getPriority()
			]
		];
	}
}