<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Setup;

/**
 * @name \Rbs\Mailinglist\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			$this->tables['rbs_mailinglist_subscription_request'] =
			$td = $schemaManager->newTableDefinition('rbs_mailinglist_subscription_request');
			$requestId = $schemaManager->newIntegerFieldDefinition('request_id')->setNullable(false)->setAutoNumber(true);
			$td->addField($requestId)
				->addField($schemaManager->newVarCharFieldDefinition('email', ['length'=>255])->setNullable(false))
					->addField($schemaManager->newIntegerFieldDefinition('website_id')->setNullable(false))
				->addField($schemaManager->newTimeStampFieldDefinition('request_date')->setNullable(false))
				->addField($schemaManager->newDateFieldDefinition('max_activation_date')->setNullable(false))
				->addField($schemaManager->newTextFieldDefinition('data')->setNullable(true))
					->addKey($this->newPrimaryKey()->addField($requestId))
					->setOption('AUTONUMBER', 1)
			->addField($schemaManager->newVarCharFieldDefinition('token', ['length'=>64])->setNullable(false));
		}
		return $this->tables;
	}
}