<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Setup;

/**
 * @name \Rbs\Notification\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$csvFileStorage = $configuration->getEntry('Change/Storage/Rbs_Mailinglist', []);
		$csvFileStorage = array_merge([
				'class' => \Change\Storage\Engines\LocalStorage::class,
				'basePath' => 'App/Storage/Rbs_Mailinglist/files'
		], $csvFileStorage);

		$configuration->addPersistentEntry('Change/Storage/Rbs_Mailinglist', $csvFileStorage);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$jobManager = $applicationServices->getJobManager();
		if (count($jobManager->getJobIdsByName('Rbs_Mailinglist_CleanSubscriptionRequestTable')) == 0)
		{
			$jobManager->createNewJob('Rbs_Mailinglist_CleanSubscriptionRequestTable');
		}

	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

}
