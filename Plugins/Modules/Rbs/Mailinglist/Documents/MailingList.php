<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Documents;

/**
 * @name \Rbs\Mailinglist\Documents\MailingList
 */
class MailingList extends \Compilation\Rbs\Mailinglist\Documents\MailingList
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); });
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->onDefaultCreated($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, function ($event) { $this->onDefaultDeleted($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Mailinglist\MailinglistDataComposer($event))->toArray());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultDeleted(\Change\Documents\Events\Event $event)
	{
		/** @var \Rbs\Media\Documents\Image $document */
		$document = $event->getDocument();
		$exportData = $document->getMeta('export');

		if (is_array($exportData) && count($exportData) && $exportData['fileURI'])
		{
			$arguments = ['storageURI' => $exportData['fileURI']];
			$job = $event->getApplicationServices()->getJobManager()
					->createNewJob('Change_Storage_URICleanUp', $arguments);
			if ($job->getId() !== 0)
			{
				$document->setMeta('export', null);
				$document->saveMetas();
			}
		}
	}

	/**
	 * @param \Change\Http\Rest\V1\Resources\DocumentResult $documentResult
	 * @param \Change\Http\UrlManager $urlManager
	 * @param string $baseUrl
	 */
	protected function addLinkOnResult($documentResult, $urlManager, $baseUrl)
	{
			$documentResult->addLink(new \Change\Http\Rest\V1\Link($urlManager, $baseUrl . '/Export', 'export'));
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		if ($this !== $event->getDocument())
		{
			return;
		}

		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$exportData = $this->getMeta('export');
			if (is_array($exportData) && count($exportData))
			{
				$restResult->setProperty('export', $exportData);
			}
			else
			{
				$restResult->setProperty('export', null);
			}

			$genData = $this->getMeta('jobGen');
			if (is_array($genData) && count($genData))
			{
				$restResult->setProperty('jobGen', $genData);
			}
			else
			{
				$restResult->setProperty('jobGen', ['jobId' => null]);
			}
			$um = $restResult->getUrlManager();
			$selfLinks = $restResult->getRelLink('self');
			$selfLink = array_shift($selfLinks);
			if ($selfLink instanceof \Change\Http\Rest\V1\Link)
			{
				$this->addLinkOnResult($restResult, $um, $selfLink->getPathInfo());
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultCreated($event)
	{
		$mailingList = $event->getDocument();
		if ($mailingList instanceof \Rbs\Mailinglist\Documents\MailingList)
		{
			$arguments = ['mailingListId' => $this->getId(), 'userId' => $mailingList->getCurrentLocalization()->getAuthorId(), 'LCID' => $mailingList->getCorrectionLCID()];
			$job = $event->getApplicationServices()->getJobManager()->createNewJob('Rbs_Segmentation_SnapShot', $arguments);
			if ($job->getId() !== 0)
			{
				$this->setMeta('jobGen', ['jobId' => $job->getId()]);
				$this->saveMetas();
			}
		}
	}
}
