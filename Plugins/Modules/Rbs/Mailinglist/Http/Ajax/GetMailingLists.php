<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Http\Ajax;

/**
 * @name \Rbs\Mailinglist\Http\Ajax\GetMailingLists
 */
class GetMailingLists
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$website = $event->getParam('website');
		$genericServices = $event->getServices('genericServices');
		$dataToSent = [];
		$mailingListsData = [];
		$profileData = [];
		if ($website instanceof \Rbs\Website\Documents\Website && $genericServices instanceof \Rbs\Generic\GenericServices)
		{
			$mailingListManager = $genericServices->getMailinglistManager();
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$mailingLists = $mailingListManager->getMailinglistsForWebsite($website);
			$context = new \Change\Http\Ajax\V1\Context($event->getApplication(), $documentManager);
			$context->setWebsite($event->getParam('website'));
			$contextArray = $context->toArray();
			foreach ($mailingLists as $mailingList)
			{
				$mailingListsData[] = $mailingList->getAJAXData($contextArray);
			}
			$dataToSent['mailingLists'] = $mailingListsData;
			$params = $event->getParam('data');
			if (isset($params['transactionId']))
			{
				$commerceServices = $event->getServices('commerceServices');
				if ($commerceServices instanceof \Rbs\Commerce\CommerceServices)
				{
					$billingAddress = null;
					$transaction = $documentManager->getDocumentInstance($params['transactionId'], 'Rbs_Payment_Transaction');

					/** @var \Rbs\Payment\Documents\Transaction $transaction */
					if ($transaction)
					{
						$targetIdentifier = $transaction->getTargetIdentifier();

						$cart = $commerceServices->getCartManager()->getCartByIdentifier($targetIdentifier);
						$billingAddress = $cart ? $cart->getCustomer()->getBillingAddress() : null;
						if (!$billingAddress)
						{
							$q = $documentManager->getNewQuery('Rbs_Order_Order');
							$q->andPredicates($q->eq('targetIdentifier', $targetIdentifier));

							/** @var \Rbs\Order\Documents\Order $order */
							$order = $q->getFirstDocument();
							if ($order instanceof \Rbs\Order\Documents\Order)
							{
								$billingAddress = $order->getCustomer()->getBillingAddress();
							}
						}
					}
					if ($billingAddress)
					{
						$profileData['lastName'] = $billingAddress->getFieldValue('lastName');
						$profileData['firstName'] = $billingAddress->getFieldValue('firstName');
						$profileData['phone'] = $billingAddress->getFieldValue('phone');
						$profileData['birthDate'] = $billingAddress->getFieldValue('birthData');
					}
					$dataToSent['profileData'] = $profileData;
				}
			}
		}
		$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/Mailinglist/GetMailingLists', $dataToSent);
		$event->setResult($result);
	}
}