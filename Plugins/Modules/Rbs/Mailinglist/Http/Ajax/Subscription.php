<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Http\Ajax;

/**
 * @name \Rbs\Mailinglist\Http\Ajax\Subscription
 */
class Subscription
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function hasSubscription(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		$listCount = 0;
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();
		if ($userId)
		{
			$user = $applicationServices->getDocumentManager()->getDocumentInstance($userId);
			if ($user instanceof \Rbs\User\Documents\User)
			{
				/* @var $genericServices \Rbs\Generic\GenericServices */
				$genericServices = $event->getServices('genericServices');
				$subscribers = $genericServices->getMailinglistManager()->getSubscribersByUser($user);
				foreach ($subscribers as $subscriber)
				{
					if ($subscriber instanceof \Rbs\Mailinglist\Documents\Subscriber)
					{
						$listCount += $subscriber->getMailingListsCount();
					}
				}
			}
		}

		$requestResult = new \Change\Http\Ajax\V1\ItemResult('Rbs/Mailinglist/HasSubscription',
			['common' => ['userId' => $userId, 'hasSubscription' => $listCount > 0, 'subscribedLists' => $listCount]]
		);
		$event->setResult($requestResult);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function createRequest(\Change\Http\Event $event)
	{
		$params = $event->getParam('data');
		$website = $event->getParam('website');
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$session = $event->getApplication()->getSessionContainer('Rbs_Mailinglist_Token');
		if ($session && is_array($params) && array_key_exists('requestToken', $params) && isset($session['token']) && $session['token']
			&& $session['token'] === $params['requestToken']
		)
		{
			unset($params['requestToken']);
			if ($website instanceof \Rbs\Website\Documents\Website && array_key_exists('email', $params))
			{
				/** @var \Rbs\Generic\GenericServices $genericServices */
				$genericServices = $event->getServices('genericServices');
				$params += ['ipAddress' => $_SERVER['REMOTE_ADDR']];

				$email = trim($params['email']);
				$mailinglistManager = $genericServices->getMailinglistManager();
				$LCID = $website->getCurrentLCID();
				if ($subscriber = $mailinglistManager->getSubscriberByEmail($email, null, $LCID))
				{
					$urlManager = $website->getUrlManager($LCID);
					$old = $urlManager->absoluteUrl(true);
					$location = $urlManager->getByFunction('Rbs_Mailinglist_ManageSubscriptions', [], $LCID);
					$params['link'] = $location ? $location->normalize()->toString() : null;
					$urlManager->absoluteUrl($old);
					$linkLocale = $i18nManager->trans('m.rbs.mailinglist.front.manage_subscription_link_title', ['lc'], ['link' => $location]);
					$message = $i18nManager->trans('m.rbs.mailinglist.front.already_subscribed', ['ucf']) . $linkLocale;
					$processing = true;
					$result = true;
				}
				else
				{
					$params += ['ip_address' => $_SERVER['REMOTE_ADDR']];
					unset($params['email']);
					if (!$params['userId'])
					{
						$result = $mailinglistManager->createSubscriptionRequest($email, $params, $website);
						$message = $i18nManager->trans('m.rbs.mailinglist.front.subscription_in_progress', ['ucf']);
						$processing = true;
					}
					else
					{
						$result = $mailinglistManager->insertLoggedInSubscriber($email, $params, $website);
						$message = $i18nManager->trans('m.rbs.mailinglist.front.subscription_success', ['ucf']);
						$processing = false;
					}
				}
				if ($result)
				{
					unset($session['token']);
					$requestResult = new \Change\Http\Ajax\V1\ItemResult('Rbs/Mailinglist/Subscribe', [
						'email' => $email,
						'optsIn' => $params['optsIn'],
						'success' => ['message' => $message, 'isProcessing' => $processing]
					]);

					$event->setResult($requestResult);
				}
				else
				{
					$this->setErrorResult($event, 'CreateSubscriptionRequestError', $mailinglistManager->getErrors(),
						\Zend\Http\Response::STATUS_CODE_409);
				}
			}
			else
			{
				$this->setErrorResult($event, 'Bad Request', [$i18nManager->trans('m.rbs.mailinglist.front.bad_request', ['ucf'])]);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', [$i18nManager->trans('m.rbs.mailinglist.front.bad_request', ['ucf'])]);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function confirmRequest(\Change\Http\Event $event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$params = $event->getParam('data');
		$website = $event->getParam('website');
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		if (is_array($params) && array_key_exists('email', $params) && $params['token'])
		{
			$mailinglistManager = $genericServices->getMailinglistManager();
			$result = $mailinglistManager->confirmSubscription($params, $website);
			if ($result)
			{
				$message = $i18nManager->trans('m.rbs.mailinglist.front.subscription_success', ['ucf']);
				$requestResult = new \Change\Http\Ajax\V1\ItemResult('Rbs/Mailinglist/ConfirmSubscription', [
					'confirmed' => true,
					'success' => ['message' => $message, 'isProcessing' => false]
				]);
				$event->setResult($requestResult);
			}
			else
			{
				$this->setErrorResult($event, 'ConfirmSubscriptionError', $mailinglistManager->getErrors(), \Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', [$i18nManager->trans('m.rbs.mailinglist.front.bad_request', ['ucf'])]);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function updateSubscriptions(\Change\Http\Event $event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$params = $event->getParam('data');
		$website = $event->getParam('website');
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		if (is_array($params) && array_key_exists('email', $params))
		{
			$email = $params['email'];
			unset($params['email'], $params['profileData']['email'], $params['profileData']['subscriberId']);
			$mailinglistManager = $genericServices->getMailinglistManager();
			$result = $mailinglistManager->updateSubscriptions($email, $params, $website);
			if ($result)
			{
				$message = $i18nManager->trans('m.rbs.mailinglist.front.update_success', ['ucf']);
				$resultData = [
					'confirmed' => true,
					'success' => ['message' => $message, 'isProcessing' => false]
				];
				$resultData += $result;
				$requestResult = new \Change\Http\Ajax\V1\ItemResult('Rbs/Mailinglist/UpdateSubscriptions', $resultData);
				$event->setResult($requestResult);
			}
			else
			{
				$this->setErrorResult($event, 'UpdateSubscriptionsError', $mailinglistManager->getErrors(), \Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', [$i18nManager->trans('m.rbs.mailinglist.front.bad_request', ['ucf'])]);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function requestManage(\Change\Http\Event $event)
	{
		$params = array_merge(['token' => null, 'requestToken' => null, 'email' => null], (array)$event->getParam('data'));

		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$session = $event->getApplication()->getSessionContainer('Rbs_Mailinglist_Token');
		if ($params['email'] && ($params['token'] || ($session && isset($session['token']) && $session['token'] === $params['requestToken'])))
		{
			/** @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			unset($params['requestToken']);
			$website = $event->getParam('website');
			if ($website instanceof \Rbs\Website\Documents\Website)
			{
				$email = $params['email'];
				unset($params['email'], $params['profileData']['email']);
				$mailinglistManager = $genericServices->getMailinglistManager();
				if (!$params['token'])
				{
					$result = $mailinglistManager->getManageSubscriptionRequest($email, $params, $website);
					$returnData = [
						'confirmed' => true,
						'success' => [
							'message' => $i18nManager->trans('m.rbs.mailinglist.front.email_is_sent', ['ucf'], ['email' => $email]),
							'isProcessing' => true
						]
					];
				}
				else
				{
					$result = $mailinglistManager->confirmManageSubscriptionRequest($email, $params, $website);
					$returnData = ['confirmed' => true];
					$returnData += $result;
				}

				if ($result)
				{
					$requestResult = new \Change\Http\Ajax\V1\ItemResult('Rbs/Mailinglist/RequestSubscriptions', $returnData);
					$event->setResult($requestResult);
				}
				else
				{
					$this->setErrorResult($event, 'RequestSubscriptions', $mailinglistManager->getErrors(), \Zend\Http\Response::STATUS_CODE_409);
				}
			}
		}
		else
		{
			$this->setErrorResult($event, 'RequestSubscriptions', [$i18nManager->trans('m.rbs.mailinglist.front.bad_request', ['ucf'])],
				\Zend\Http\Response::STATUS_CODE_409);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string $code
	 * @param string $message
	 * @param integer $httpStatus
	 * @return \Change\Http\Ajax\V1\ErrorResult
	 */
	public function setErrorResult(\Change\Http\Event $event, $code, $message, $httpStatus = \Zend\Http\Response::STATUS_CODE_400)
	{
		$errorResult = new \Change\Http\Ajax\V1\ErrorResult($code, $message, $httpStatus);
		$event->setResult($errorResult);
		return $errorResult;
	}
}