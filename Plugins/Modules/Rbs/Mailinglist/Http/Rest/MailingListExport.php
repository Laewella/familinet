<?php

/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Http\Rest;

class MailingListExport
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$mailingListId = $event->getParam('mailingListId');
		$applicationServices = $event->getApplicationServices();
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();
		$dm = $applicationServices->getDocumentManager();
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			$mailingList = $dm->getDocumentInstance($mailingListId);
			if ($mailingList instanceof \Rbs\Mailinglist\Documents\MailingList)
			{
				$LCID = $mailingList->getCurrentLCID();
				$arguments = ['mailingListId' => $mailingListId, 'LCID' => $LCID, 'callerId' => $userId, 'chunk' => 5000];
				$job = $event->getApplicationServices()->getJobManager()
					->createNewJob('Rbs_Mailinglist_SubscribersExport', $arguments);
				$mailingList->setMeta('export', ['jobId' => $job->getId()]);
				$mailingList->saveMetas();
				$result->setArray(['jobId' => $job->getId()]);
				$event->setResult($result);
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
		}
	}
}