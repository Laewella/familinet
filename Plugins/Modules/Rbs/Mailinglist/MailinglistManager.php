<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist;

/**
 * @name \Rbs\Mailinglist\MailinglistManager
 */
class MailinglistManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'MailinglistManager';

	/**
	 * @var \Change\Documents\DocumentManager $documentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Transaction\TransactionManager
	 */
	protected $transactionManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var string[]
	 */
	protected $errors;

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Generic/Events/MailinglistManager');
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager(\Change\Documents\DocumentManager $documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @return $this
	 */
	public function setTransactionManager(\Change\Transaction\TransactionManager $transactionManager)
	{
		$this->transactionManager = $transactionManager;
		return $this;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager $documentManager
	 */
	public function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	public function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @return \Change\Transaction\TransactionManager $transactionManager
	 */
	public function getTransactionManager()
	{
		return $this->transactionManager;
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('createSubscriptionRequest', [$this, 'onDefaultCheckCreateSubscriptionRequest'], 10);
		$eventManager->attach('insertLoggedInSubscriber', [$this, 'onDefaultCheckCreateSubscriptionRequest'], 10);
		$eventManager->attach('updateSubscriptions', [$this, 'onDefaultCheckCreateSubscriptionRequest'], 10);
		$eventManager->attach('getManageSubscriptionRequest', [$this, 'onDefaultCheckCreateSubscriptionRequest'], 10);
		
		$eventManager->attach('createSubscriptionRequest', [$this, 'onDefaultCreateSubscriptionRequest'], 5);
		$eventManager->attach('confirmSubscription', [$this, 'onDefaultConfirmSubscription'], 5);
		$eventManager->attach('insertLoggedInSubscriber', [$this, 'onDefaultInsertLoggedInSubscriber'], 5);
		$eventManager->attach('updateSubscriptions', [$this, 'onDefaultUpdateSubscriptions'], 5);
		$eventManager->attach('getManageSubscriptionRequest', [$this, 'onDefaultGetManageSubscriptionRequest'], 5);
		$eventManager->attach('confirmManageSubscriptionRequest', [$this, 'onDefaultConfirmManageSubscriptionRequest'], 5);
	}

	/**
	 * @param string $email
	 * @param array $params
	 * @param \Rbs\Website\Documents\Website $website
	 * @return int|boolean
	 * $data:
	 *   - email
	 *   - website
	 *   - _LCID
	 *   - newsLetterOptIn
	 *   - partnersOptIn
	 *   - profileData
	 * @return boolean
	 */
	public function createSubscriptionRequest($email, $params, $website)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['email' => $email, 'requestParameters' => $params, 'website' => $website]);
		$em->trigger('createSubscriptionRequest', $this, $args);
		if (isset($args['token']))
		{
			return true;
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultCreateSubscriptionRequest(\Change\Events\Event $event)
	{
		if ($event->getParam('token') || $this->hasErrors())
		{
			return;
		}
		$website = $event->getParam('website');
		if ($website instanceof \Rbs\Website\Documents\Website)
		{
			$email = $event->getParam('email');
			if (isset($requestParameters['_LCID']))
			{
				$LCID = $requestParameters['_LCID'];
			}
			else
			{
				$LCID = $website->getCurrentLCID();
			}
			$requestParameters = $event->getParam('requestParameters');
			$requestParameters += ['websiteId' => $website->getId(), 'LCID' => $LCID];
			if (!isset($requestParameters['optsIn']))
			{
				$i18nManager = $this->getI18nManager();
				$this->addError($i18nManager->trans('m.rbs.mailinglist.front.must_check_one_list', ['ucf']));
				return;
			}
			$applicationServices = $event->getApplicationServices();
			try
			{
				$this->transactionManager->begin();
				$dbProvider = $applicationServices->getDbProvider();
				$token = $this->insertSubscriptionRequest($dbProvider, $email, $requestParameters);
				$event->setParam('token', $token);

				/** @var \Rbs\Generic\GenericServices $genericServices */
				$genericServices = $event->getServices('genericServices');
				if ($genericServices)
				{

					$genericServices->getNotificationManager()->mailingListSubscriptionRequest($email, $token, $website, $LCID);
				}
				$this->transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$applicationServices->getLogging()->exception($e);
				throw $this->transactionManager->rollBack($e);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultCheckCreateSubscriptionRequest($event)
	{
		$email = $event->getParam('email');
		$requestParams = $event->getParam('requestParameters');
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $this->getI18nManager();
		if (!is_string($email) || \Change\Stdlib\StringUtils::isEmpty($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_email', ['ucf']));
		}

		$validator = new \Zend\Validator\EmailAddress();
		if (!$validator->isValid($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_email_invalid', ['ucf'], ['EMAIL' => $email]));
		}

		if ($this->subscriptionRequestExists($applicationServices->getDbProvider(), $email, $requestParams,
				$event->getParam('website')->getId())
			!== null
		)
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_request_already_done', ['ucf'], ['EMAIL' => $email]));
		}
		if (!$event->getParam('unsubscribe') && !$event->getParam('manage'))
		{
			if (!isset($requestParams['optsIn']) || count($requestParams['optsIn']) === 0)
			{
				$this->addError($i18nManager->trans('m.rbs.mailinglist.front.must_check_one_list', ['ucf']));
				return;
			}
		}
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param string $email
	 * @param array $parameters
	 * @param \DateTime $validRequestDate
	 * @return integer
	 */
	protected function insertSubscriptionRequest(\Change\Db\DbProvider $dbProvider, $email, array $parameters = [],
		\DateTime $validRequestDate = null)
	{
		$qb = $dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		if (!$validRequestDate)
		{
			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('P7D'));
		}
		$token = substr(md5(uniqid($email, true)), 0, 10);
		$qb->insert($fb->table('rbs_mailinglist_subscription_request'));
		$qb->addColumns($fb->column('email'), $fb->column('website_id'), $fb->column('max_activation_date'), $fb->column('data'),
			$fb->column('token'));
		$qb->addValues($fb->parameter('email'), $fb->parameter('websiteId'), $fb->dateTimeParameter('max_activation_date'),
			$fb->parameter('data'), $fb->parameter('token'));

		$iq = $qb->insertQuery();

		$iq->bindParameter('email', $email);
		$iq->bindParameter('websiteId', (int)$parameters['websiteId']);
		$iq->bindParameter('max_activation_date', $validRequestDate);
		$iq->bindParameter('data', json_encode($parameters));
		$iq->bindParameter('token', $token);
		$iq->execute();

		return $token;
	}

	/**
	 * @param string $email
	 * @param array $params
	 *   - email
	 *   - website
	 *   - _LCID
	 *   - newsLetterOptIn
	 *   - partnersOptIn
	 *   - profileData
	 * @param \Rbs\Website\Documents\Website $website
	 * @return boolean
	 */
	public function insertLoggedInSubscriber($email, $params, $website)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['email' => $email, 'requestParameters' => $params, 'website' => $website]);
		$em->trigger('insertLoggedInSubscriber', $this, $args);
		if ($args['confirmed'])
		{
			return true;
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultInsertLoggedInSubscriber(\Change\Events\Event $event)
	{
		if ($this->hasErrors() || $event->getParam('confirmed'))
		{
			return;
		}

		$requestParams = $event->getParam('requestParameters');
		$profileData = [
			'profileData' => $requestParams['profileData'],
			'extraData' => [
					'device' => isset($requestParams['device']) ? $requestParams['device'] : 'unknown',
					'ipAddress' => isset($requestParams['ipAddress']) ? $requestParams['ipAddress'] : 'unknown'
			]
		];

		$mailinglists = [];
		$email = $event->getParam('email');

		$website = $event->getParam('website');
		$userParam = $event->getParam('user');
		$user = $userParam && $userParam instanceof \Rbs\User\Documents\User ? $userParam : $this->getUserByEmail($email);
		if ($user instanceof \Rbs\User\Documents\User && $website instanceof \Rbs\Website\Documents\Website)
		{
			foreach ($requestParams['optsIn'] as $index => $optIn)
			{
				if ($optIn !== null)
				{
					$mailingList = $this->getDocumentManager()->getDocumentInstance($optIn, 'Rbs_Mailinglist_MailingList');
					if ($mailingList && $mailingList instanceof \Rbs\Mailinglist\Documents\MailingList)
					{
						/** @var \Rbs\Mailinglist\Documents\MailingList[] $mailinglists */
						$mailinglists[] = $mailingList;
					}
				}
				else
				{
					unset($requestParams['optsIn'][$index]);
				}
			}

			try
			{
				$LCID = isset($requestParams['_LCID']) ? $requestParams['_LCID'] : $website->getCurrentLCID();
				$this->transactionManager->begin();
				/** @var \Rbs\Mailinglist\Documents\Subscriber $subscriber */
				$subscriber = $this->getDocumentManager()->getNewDocumentInstanceByModelName('Rbs_Mailinglist_Subscriber');
				$subscriber->setEmail($email);
				$subscriber->setUserLCID($LCID);
				$subscriber->setMailingLists($mailinglists);
				$subscriber->setUser($user);
				$subscriber->setSubscriberData($profileData);
				$subscriber->setActive(true);
				$subscriber->save();
				$event->setParam('confirmed', true);
				$event->setParam('subscriber', $subscriber);
				$this->transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->transactionManager->rollBack($e);
				throw $e;
			}
		}
		else
		{
			if (!$user || !($user instanceof \Rbs\User\Documents\User))
			{
				$this->addError($this->getI18nManager()
					->trans('m.rbs.mailinglist.front.undefined_user'));
			}
			if (count($requestParams['optsIn']) >= 1)
			{
				$this->addError($this->getI18nManager()
					->trans('m.rbs.mailinglist.front.already_subscribe_to_lists'));
			}
			else
			{
				$this->addError($this->getI18nManager()
					->trans('m.rbs.mailinglist.front.already_subscribe_to_list'));
			}
		}
	}

	/**
	 * @param $data
	 * @param \Rbs\Website\Documents\Website $website
	 * @return mixed|null
	 */
	public function confirmSubscription($data, $website)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['requestParams' => $data, 'website' => $website]);
		$em->trigger('confirmSubscription', $this, $args);
		if ($args['confirmed'])
		{
			return $args['confirmed'];
		}
		return null;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultConfirmSubscription(\Change\Events\Event $event)
	{
		if ($event->getParam('confirmed'))
		{
			return;
		}
		$website = $event->getParam('website');
		$requestParams = $event->getParam('requestParams');
		if (!isset($requestParams['email']))
		{
			$this->addError($this->getI18nManager()->trans('m.rbs.mailinglist.front.invalid_request'));
			return;
		}
		$email = $requestParams['email'];

		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$request = $this->subscriptionRequestExists($dbProvider, $email, $requestParams, $website->getId());
		if (is_array($request))
		{
			$requestData = json_decode($request['data'], true);
			$mailinglists = [];

			foreach ($requestData['optsIn'] as $optIn)
			{
				/** @var \Rbs\Mailinglist\Documents\MailingList $mailingList */
				$mailingList = $this->getDocumentManager()->getDocumentInstance($optIn, 'Rbs_Mailinglist_MailingList');
				if (in_array($requestData['LCID'], $mailingList->getLCIDArray()))
				{
					/** @var \Rbs\Mailinglist\Documents\MailingList[] $mailinglists */
					$mailinglists[] = $mailingList;
				}
			}

			if (count($mailinglists) === 0)
			{
				$this->addError($this->getI18nManager()->trans('m.rbs.mailinglist.front.invalid_subscribed_mailinglists'));
				try
				{
					$this->transactionManager->begin();
					$this->deleteSubscriptionRequest($dbProvider, $request['request_id']);
					$this->transactionManager->commit();
				}
				catch (\Exception $e)
				{
					$this->transactionManager->rollBack($e);
				}
				return;
			}

			try
			{
				$subscriberData['profileData'] = $requestData['profileData'];
				$subscriberData['extraData']['device'] = isset($requestData['device']) ? $requestData['device'] : 'unknown';
				$subscriberData['extraData']['ipAddress'] =
					isset($requestData['ipAddress']) ? $requestData['ipAddress'] : 'unknown';
				$user = $this->getUserByEmail($email);

				$this->transactionManager->begin();
				/** @var \Rbs\Mailinglist\Documents\Subscriber $subscriber */
				$subscriber = $this->getDocumentManager()->getNewDocumentInstanceByModelName('Rbs_Mailinglist_Subscriber');
				$subscriber->setEmail($email);
				$subscriber->setUserLCID($requestData['LCID']);
				$subscriber->setMailingLists($mailinglists);
				$subscriber->setUser($user);
				$subscriber->setSubscriberData($subscriberData);
				$subscriber->setActive(true);
				$subscriber->save();
				$this->deleteSubscriptionRequest($dbProvider, $request['request_id']);
				$event->setParam('confirmed', true);
				$event->setParam('subscriber', $subscriber);
				$this->transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->transactionManager->rollBack($e);
			}
		}
		else
		{
			$this->addError($this->getI18nManager()
				->trans('m.rbs.mailinglist.front.invalid_request'));
		}
	}

	/**
	 * @param string $email
	 * @param array $params
	 * @param \Rbs\Website\Documents\Website $website
	 * @return bool
	 */
	public function updateSubscriptions($email, $params, \Rbs\Website\Documents\Website $website)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['email' => $email, 'requestParameters' => $params, 'website' => $website,
			'unsubscribe' => true]);
		$em->trigger('updateSubscriptions', $this, $args);
		if ($args['confirmed'])
		{
			return ['confirmed' => $args['confirmed'], 'mailingLists' => $args['mailingLists']];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultUpdateSubscriptions(\Change\Events\Event $event)
	{
		if ($this->hasErrors() || $event->getParam('confirmed'))
		{
			return;
		}
		$requestParameters = $event->getParam('requestParameters');
		$subscriberId = $requestParameters['subscriberId'];
		$subscriber = $this->getDocumentManager()->getDocumentInstance($subscriberId, 'Rbs_Mailinglist_Subscriber');
		$email = $event->getParam('email');
		$i18nManager = $this->getI18nManager();
		$mailinglists = [];
		if ($subscriber instanceof \Rbs\Mailinglist\Documents\Subscriber)
		{
			if ($requestParameters['profileData'])
			{
				try
				{
					$this->transactionManager->begin();
					$subscriberData = $subscriber->getSubscriberData();
					$mailingListsData = [];
					$newSubscriberData = [
						'profileData' => $requestParameters['profileData'],
						'extraData' => isset($subscriberData['extraData']) ? $subscriberData['extraData'] : []
					];
					$subscriber->setSubscriberData($newSubscriberData);
					$context = new \Change\Http\Ajax\V1\Context($event->getApplication(), $this->getDocumentManager());
					$context->setWebsite($event->getParam('website'));
					$contextArray = $context->toArray();
					foreach ($requestParameters['optsIn'] as $optIn)
					{
						/** @var \Rbs\Mailinglist\Documents\MailingList[] $mailinglists */
						$mailinglists[] = $this->getDocumentManager()->getDocumentInstance($optIn, 'Rbs_Mailinglist_MailingList');
					}
					$subscriber->setMailingLists($mailinglists);
					if (!$mailinglists || count($mailinglists) === 0)
					{
						$subscriber->setActive(false);
					}
					if ($email !== $subscriber->getEmail())
					{
						$subscriber->setEmail($email);
					}
					$subscriber->save();
					$mailingListsData[$subscriber->getId()] =
						$this->getSubscribedMailingListsBySubscriber($subscriber, $contextArray, $event->getParam('website'));
					$event->setParam('confirmed', true);
					$event->setParam('mailingLists', $mailingListsData);
					$event->setParam('subscriber', $subscriber);
					$this->transactionManager->commit();
				}
				catch (\Exception $e)
				{
					$this->transactionManager->rollBack($e);
					throw $e;
				}
			}
		}
		else
		{
			$this->addError($i18nManager->trans('m.rbs.mailinglist.front.undefined_subscriber', ['ucf']));
		}
	}

	/**
	 * @param string $email
	 * @param array $params
	 * @param \Rbs\Website\Documents\Website $website
	 * @return bool
	 */
	public function getManageSubscriptionRequest($email, $params, $website)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['email' => $email, 'website' => $website, 'params' => $params, 'manage' => true]);
		$em->trigger('getManageSubscriptionRequest', $this, $args);
		return isset($args['confirmed']) ? true : false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultGetManageSubscriptionRequest(\Change\Events\Event $event)
	{
		if ($this->hasErrors())
		{
			return;
		}
		$email = $event->getParam('email');
		/** @var \Rbs\Website\Documents\Website $website */
		$website = $event->getParam('website');

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$dbq = $this->getDocumentManager()->getNewQuery('Rbs_Mailinglist_Subscriber');
		$dbq->andPredicates($dbq->eq('email', $email), $dbq->eq('userLCID', $website->getCurrentLCID()));
		/** @var \Rbs\Mailinglist\Documents\Subscriber[] $subscribers */
		$subscribers = $dbq->getDocuments()->toArray();
		if (count($subscribers) === 1)
		{
			$subscriber = $subscribers[0];

			// If there is no secret token, generate it.
			if (!$subscriber->getSecret())
			{
				try
				{
					$this->transactionManager->begin();
					$subscriber->setSecret(substr(md5(uniqid($email, true)), 0, 10));
					$subscriber->save();
					$this->transactionManager->commit();
				}
				catch (\Exception $e)
				{
					$this->transactionManager->rollBack($e);
					throw $e;
				}
			}

			$genericServices->getNotificationManager()->mailingListManageRequest($subscriber, $website);
			$event->setParam('confirmed', true);
		}
		else
		{
			$this->addError($this->getI18nManager()->trans('m.rbs.mailinglist.front.subscriber_not_found'));
		}
	}

	/**
	 * @param string $email
	 * @param array $params
	 * @param \Rbs\Website\Documents\Website $website
	 * @return array
	 */
	public function confirmManageSubscriptionRequest($email, $params, $website)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['email' => $email, 'token' => $params['token'], 'params' => $params, 'website' => $website]);
		$em->trigger('confirmManageSubscriptionRequest', $this, $args);
		if (isset($args['profileData'], $args['mailingLists'], $args['subscriberId']) && $args['subscriberId'])
		{
			return ['profileData' => $args['profileData'], 'mailingLists' => $args['mailingLists'], 'subscriberId' => $args['subscriberId']];
		}
		return [];
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onDefaultConfirmManageSubscriptionRequest(\Change\Events\Event $event)
	{
		$email = $event->getParam('email');
		$token = $event->getParam('token');
		$website = $event->getParam('website');
		$subscriber = $this->getSubscriberByEmail($email, $token);
		if ($subscriber instanceof \Rbs\Mailinglist\Documents\Subscriber)
		{
			// The secret token is consumed, so remove it.
			try
			{
				$this->transactionManager->begin();
				$subscriber->setSecret(null);
				$subscriber->save();
				$this->transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->transactionManager->rollBack($e);
				throw $e;
			}

			$subscriberId = $subscriber->getId();
			$subscriberData = $subscriber->getSubscriberData();
			$context = new \Change\Http\Ajax\V1\Context($event->getApplication(), $this->getDocumentManager());
			$context->setWebsite($event->getParam('website'));
			$contextArray = $context->toArray();
			$mailingLists = [$subscriber->getId() => $this->getSubscribedMailingListsBySubscriber($subscriber, $contextArray, $website)];
			$event->setParam('mailingLists', $mailingLists);
			$event->setParam('profileData', [$subscriberId => $subscriberData['profileData']]);
			$event->setParam('subscriberId', $subscriberId);
		}
		else
		{
			$this->addError($this->getI18nManager()->trans('m.rbs.mailinglist.front.subscriber_not_found'));
		}
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param $request_id
	 * @return int
	 */
	private function deleteSubscriptionRequest(\Change\Db\DbProvider $dbProvider, $request_id)
	{
		$sb = $dbProvider->getNewStatementBuilder();
		$fb = $sb->getFragmentBuilder();
		$sb->delete('rbs_mailinglist_subscription_request');
		$sb->where($fb->eq('request_id', $request_id));
		return $sb->deleteQuery()->execute();
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param string $email
	 * @param array $requestParameters
	 * @param int $websiteId
	 * @return array|null
	 */
	public function subscriptionRequestExists($dbProvider, $email, $requestParameters, $websiteId)
	{
		if (!isset($requestParameters['token']))
		{
			return null;
		}
		$qb = $dbProvider->getNewQueryBuilder();
		$pb = $qb->getFragmentBuilder();
		$qb->select();
		$qb->from('rbs_mailinglist_subscription_request');
		$qb->where($pb->logicAnd(
			$pb->eq('email', $pb->string($email)), $pb->eq('website_id', $websiteId),
			$pb->eq('token', $pb->string($requestParameters['token']))
		));
		return $qb->query()->getFirstResult();
	}

	/**
	 * @param $email
	 * @return \Rbs\User\Documents\User|null
	 */
	public function getUserByEmail($email)
	{
		$dqb = $this->getDocumentManager()->getNewQuery('Rbs_User_User');
		$dqb->andPredicates($dqb->eq('email', $email));
		return $dqb->getFirstDocument();
	}

	/**
	 * @param string $email
	 * @param string $secret optional
	 * @param string $LCID optional
	 * @return \Rbs\Mailinglist\Documents\Subscriber|null
	 */
	public function getSubscriberByEmail($email, $secret = null, $LCID = null)
	{
		$query = $this->getDocumentManager()->getNewQuery('Rbs_Mailinglist_Subscriber');
		$query->andPredicates($query->eq('email', $email));
		if ($secret)
		{
			$query->andPredicates($query->eq('secret', $secret));
		}
		if($LCID)
		{
			$query->andPredicates($query->eq('userLCID', $LCID));
		}
		return $query->getFirstDocument();
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @return \Rbs\Mailinglist\Documents\Subscriber[]
	 */
	public function getSubscribersByUser($user)
	{
		$query = $this->getDocumentManager()->getNewQuery('Rbs_Mailinglist_Subscriber');
		$query->andPredicates($query->eq('user', $user));
		return $query->getDocuments()->toArray();
	}

	/**
	 * @param string $user
	 * @param string $email
	 * @return \Rbs\Mailinglist\Documents\Subscriber|null
	 */
	public function getSubscriberByUserAndEmail($user, $email)
	{
		$query = $this->getDocumentManager()->getNewQuery('Rbs_Mailinglist_Subscriber');
		$query->andPredicates($query->eq('user', $user), $query->eq('email', $email));
		return $query->getFirstDocument();
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @return \Rbs\Mailinglist\Documents\MailingList[]|null
	 */
	public function getMailinglistsByUser($user)
	{
		$query = $this->getDocumentManager()->getNewQuery('Rbs_Mailinglist_Subscriber');
		$query->andPredicates($query->eq('user', $user));
		$subscribers = $query->getDocuments()->toArray();
		$lists = [];
		/** @var \Rbs\Mailinglist\Documents\Subscriber $subscriber */
		foreach ($subscribers as $subscriber)
		{
			$lists[] = $subscriber->getMailingLists()->toArray();
		}
		return $lists;
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website ~
	 * @param string $LCID
	 * @return \Rbs\Mailinglist\Documents\MailingList[]
	 */
	public function getMailinglistsForWebsite($website, $LCID = null)
	{
		$LCID = $LCID ?: $website->getCurrentLCID();
		$dqb = $this->getDocumentManager()->getNewQuery('Rbs_Mailinglist_MailingList', $LCID);
		$dqb->andPredicates($dqb->eq('website', $website->getId()), $dqb->activated(new \DateTime()));
		$dqb->addOrder('priority', false);
		$dqb->addOrder('id', true);
		return $dqb->getDocuments()->toArray();
	}

	/**
	 * @param \Rbs\Mailinglist\Documents\Subscriber $subscriber
	 * @param array $context
	 * @param \Rbs\Website\Documents\Website $website
	 * @return array
	 */
	public function getSubscribedMailingListsBySubscriber(\Rbs\Mailinglist\Documents\Subscriber $subscriber, array $context,
		\Rbs\Website\Documents\Website $website)
	{
		$subscribedMailingListsIds = $subscriber->getMailingListsIds();
		$websiteMailingLists = $this->getMailinglistsForWebsite($website, $subscriber->getUserLCID());
		$mailingListsData = [];
		/** @var \Rbs\Mailinglist\Documents\MailingList $websiteMailingList */
		foreach ($websiteMailingLists as $websiteMailingList)
		{
			$tmpData = $websiteMailingList->getAJAXData($context);
			$listId = $websiteMailingList->getId();
			if (in_array($listId, $subscribedMailingListsIds))
			{
				$tmpData['common']['subscribed'] = true;
			}
			else
			{
				$tmpData['common']['subscribed'] = false;
			}
			$mailingListsData[$listId] = $tmpData;
		}
		return $mailingListsData;
	}

	/**
	 * @param string $error
	 * @return $this
	 */
	public function addError($error)
	{
		if (is_string($error) && !\Change\Stdlib\StringUtils::isEmpty($error))
		{
			$this->errors[] = $error;
		}
		return $this;
	}

	/**
	 * @return bool
	 */
	public function hasErrors()
	{
		return count($this->errors) ? true : false;
	}

	/**
	 * @return array $errors
	 */
	public function getErrors()
	{
		return $this->errors;
	}
}