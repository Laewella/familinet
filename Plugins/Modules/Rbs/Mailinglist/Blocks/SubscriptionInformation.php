<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Blocks;

/**
 * @name \Rbs\Mailinglist\Blocks\RegistrationInformation
 */
class SubscriptionInformation extends \Change\Presentation\Blocks\Information
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.mailinglist.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.mailinglist.admin.registration_label', $ucf));

		$this->addParameterInformation('showTitle', \Change\Documents\Property::TYPE_BOOLEAN, false, true)
			->setLabel($i18nManager->trans('m.rbs.generic.admin.block_show_title', $ucf));
		$this->addParameterInformation('showTokenField', \Change\Documents\Property::TYPE_BOOLEAN, false, true)
			->setLabel($i18nManager->trans('m.rbs.mailinglist.admin.registration_show_token_field', $ucf));
		$this->addParameterInformation('showComplementaryFields', \Change\Documents\Property::TYPE_BOOLEAN, false, false)
			->setLabel($i18nManager->trans('m.rbs.mailinglist.admin.registration_show_complementary_fields', $ucf));
	}
}