<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Blocks;

/**
 * @name \Rbs\Mailinglist\Blocks\ManageSubscriptions
 */
class ManageSubscriptions extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$currentUser = $event->getAuthenticationManager()->getCurrentUser();
		$parameters->addParameterMeta('mustValidate', false);
		$parameters->addParameterMeta('userId', 0);
		$parameters->addParameterMeta('requestToken');
		$parameters->setLayoutParameters($event->getBlockLayout());
		$parameters->setNoCache();

		$httpRequest = $event->getHttpRequest();
		if ($httpRequest->getQuery('token') && $httpRequest->getQuery('email'))
		{
			$parameters->setParameterValue('token', $httpRequest->getQuery('token'));
			$parameters->setParameterValue('email', $httpRequest->getQuery('email'));
		}
		else
		{
			if ($currentUser->getId() !== 0)
			{
				$parameters->setParameterValue('userId', $currentUser->getId());
			}
			else
			{
				$genericServices = $event->getServices('genericServices');
				if ($genericServices instanceof \Rbs\Generic\GenericServices)
				{
					$securityManager = $genericServices->getSecurityManager();
					$token = $securityManager->getCSRFToken();

					$session = $event->getApplication()->getSessionContainer('Rbs_Mailinglist_Token');
					if ($session)
					{
						$session['token'] = $token;
					}
					$parameters->addParameterMeta('requestToken', $token);
				}
				$parameters->setParameterValue('mustValidate', true);
			}
		}
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();
		$genericServices = $event->getServices('genericServices');
		$mailingLists = null;
		$user = null;
		$subscribers = [];
		$website = $event->getParam('website');
		$userId = $parameters->getParameter('userId');
		$email = $parameters->getParameter('email');
		$attributes['subscriberId'] = 0;
		$attributes['user'] = $userId ?: 0;
		if ($genericServices instanceof \Rbs\Generic\GenericServices && $website instanceof \Rbs\Website\Documents\Website && ($email || $userId)
			&& !$parameters->getParameter('mustValidate')
		)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$mailingListManager = $genericServices->getMailinglistManager();

			if ($userId)
			{
				$user = $documentManager->getDocumentInstance($userId, 'Rbs_User_User');
				if ($user instanceof \Rbs\User\Documents\User)
				{
					$subscribers = $mailingListManager->getSubscribersByUser($user);
				}
			}
			elseif ($email)
			{
				$subscribers = [$mailingListManager->getSubscriberByEmail($email, $parameters->getParameter('token'), $website->getCurrentLCID())];
			}

			$contextArray = $this->getContextArray($event, $documentManager);

			foreach ($subscribers as $subscriber)
			{
				if ($subscriber instanceof \Rbs\Mailinglist\Documents\Subscriber)
				{
					$id = $subscriber->getId();
					if ($user && $user->getEmail() === $subscriber->getEmail())
					{
						$attributes['subscriberId'] = $id;
					}
					list($mailingLists, $profileData) = $this->getInformation($subscriber, $mailingListManager, $website, $contextArray);
					$attributes['mailingLists'][$id] = $mailingLists;
					$attributes['profileData'][$id] = $profileData;
					$attributes['subscribers'][$id] = $subscriber->getEmail();
				}
			}
			if (!isset($attributes['subscriberId']) && count($subscribers) !== 0)
			{
				$attributes['subscriberId'] = $subscribers[0]->getId();
			}
		}
		return 'manage-subscriptions.twig';
	}

	/**
	 * @param \Rbs\Mailinglist\Documents\Subscriber $subscriber
	 * @param \Rbs\Mailinglist\MailinglistManager $mailingListManager
	 * @param \Rbs\Website\Documents\Website $website
	 * @param array $contextArray
	 * @return array
	 */
	protected function getInformation($subscriber, $mailingListManager, $website, $contextArray)
	{
		$subscriberData = $subscriber->getSubscriberData();
		$mailingListData = $mailingListManager->getSubscribedMailingListsBySubscriber($subscriber, $contextArray, $website);
		$profileData = $subscriberData['profileData'];
		$profileData['email'] = $subscriber->getEmail();
		if (isset($profileData['birthDate']))
		{
			$birthDate = $profileData['birthDate'];
			if ($birthDate instanceof \DateTime)
			{
				$birthDate->setTimezone(new \DateTimeZone('UTC'));
				$profileData['birthDate'] = $birthDate->format(\DateTime::ATOM);
			}
		}
		return [$mailingListData, $profileData];
	}

	/**
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array
	 */
	protected function getContextArray(\Change\Presentation\Blocks\Event $event,
		\Change\Documents\DocumentManager $documentManager)
	{
		$context = new \Change\Http\Ajax\V1\Context($event->getApplication(), $documentManager);
		$context->setWebsite($event->getParam('website'));
		return $context->toArray();
	}
}