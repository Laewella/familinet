<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Blocks;

/**
 * @name \Rbs\Mailinglist\Blocks\Registration
 */
class Subscription extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->setLayoutParameters($event->getBlockLayout());

		$httpRequest = $event->getHttpRequest();
		if ($httpRequest && $httpRequest->getQuery('email'))
		{
			$parameters->setParameterValue('email', trim($httpRequest->getQuery('email')));
		}
		if ($httpRequest && $httpRequest->getQuery('token'))
		{
			$parameters->setParameterValue('token', trim($httpRequest->getQuery('token')));
			$parameters->setNoCache();
		}
		else
		{
			$website = $event->getParam('website');
			if ($website instanceof \Rbs\Website\Documents\Website)
			{
				$LCID = $website->getCurrentLCID();
				$parameters->addParameterMeta('_LCID', $LCID);
				$genericServices = $event->getServices('genericServices');
				if ($genericServices instanceof \Rbs\Generic\GenericServices)
				{
					$applicationServices = $event->getApplicationServices();
					$authManager = $applicationServices->getAuthenticationManager();
					$userId = $authManager->getCurrentUser()->getId();
					$parameters->addParameterMeta('userId', $userId);
					$securityManager = $genericServices->getSecurityManager();
					$token = $securityManager->getCSRFToken();

					$session = $event->getApplication()->getSessionContainer('Rbs_Mailinglist_Token');
					if ($session)
					{
						$session['token'] = $token;
					}

					$parameters->addParameterMeta('requestToken', $token);
					$parameters->setNoCache();
				}
			}
		}
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();
		$attributes['profileData'] = null;
		$attributes['mailingLists'] = [];
		if (!$parameters->getParameter('token'))
		{
			$genericServices = $event->getServices('genericServices');
			if ($genericServices instanceof \Rbs\Generic\GenericServices)
			{
				$mailingListManager = $genericServices->getMailinglistManager();
				$mailinglists = $mailingListManager->getMailinglistsForWebsite($event->getParam('website'), $parameters->getParameter('_LCID'));
				$applicationServices = $event->getApplicationServices();
				$documentManager = $applicationServices->getDocumentManager();

				$context = new \Change\Http\Ajax\V1\Context($event->getApplication(), $documentManager);
				$context->setWebsite($event->getParam('website'));
				$contextArray = $context->toArray();

				$userId = $parameters->getParameter('userId');
				if ($userId !== 0)
				{
					$user = $documentManager->getDocumentInstance($userId, 'Rbs_User_User');
					if ($user instanceof \Rbs\User\Documents\User)
					{
						$attributes['profileData'] = ['email' => $user->getEmail(), 'id' => $user->getId()];
						if ($parameters->getParameter('showComplementaryFields'))
						{
							$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);
							$profileManager = $event->getApplicationServices()->getProfileManager();
							$profile = $profileManager->loadProfile($authenticatedUser, 'Rbs_User');
							$attributes['profileData'] += ['lastName' => $profile->getPropertyValue('lastName')];
							$attributes['profileData'] += ['firstName' => $profile->getPropertyValue('firstName')];
							$attributes['profileData'] += ['phone' => $profile->getPropertyValue('phone')];
							$birthDate = $profile->getPropertyValue('birthDate');
							if ($birthDate instanceof \DateTime)
							{
								$birthDate->setTimezone(new \DateTimeZone('UTC'));
								$birthDate = $birthDate->format(\DateTime::ATOM);
							}
							$attributes['profileData'] += ['birthDate' => $birthDate];
						}
					}
				}
				else if ($parameters->getParameter('email'))
				{
					$attributes['profileData'] = ['email' => $parameters->getParameter('email')];
				}
				foreach ($mailinglists as $mailinglist)
				{
					$attributes['mailingLists'][] = $mailinglist->getAJAXData($contextArray);
				}
			}
		}
		return 'subscription.twig';
	}
}