/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.controller('rbsMailinglistShortSubscription', ['$scope', 'RbsChange.AjaxAPI', rbsMailinglistShortSubscriptionController]);

	function rbsMailinglistShortSubscriptionController(scope, AjaxAPI) {
		var userId = __change.userContext.accessorId;
		if (userId) {
			scope.mode = 'loading';
			AjaxAPI.getData('Rbs/Mailinglist/HasSubscription').then(
				function(result) {
					scope.mode = result.data.dataSets.common.hasSubscription ? 'subscription' : 'manage';
				},
				function() {
					scope.mode = 'subscription';
				}
			);
		}
		else {
			scope.mode = 'subscription';
		}
	}

	app.controller('rbsMailinglistSubscription', ['$scope', 'RbsChange.AjaxAPI', '$sce', '$cookies', rbsMailinglistSubscriptionController]);

	function rbsMailinglistSubscriptionController(scope, AjaxAPI, $sce, $cookies) {
		scope.mailingLists = {};
		scope.profileData = {};
		scope.optsIn = {};
		scope.requestToken = scope.blockParameters.requestToken;
		scope.disabled = false;
		scope.isConnected = false;

		if (scope.blockData.hasOwnProperty('profileData') && angular.isObject(scope.blockData.profileData)) {
			scope.profileData = scope.blockData.profileData;
			scope.isConnected = scope.profileData.hasOwnProperty('email');
		}
		scope.showComplementaryFields = scope.blockParameters.showComplementaryFields;

		scope.mailingLists = scope.blockData.mailingLists;
		if (scope.blockParameters.token) {
			scope.token = scope.blockParameters.token;
			if (scope.blockParameters.email) {
				if (!scope.isConnected) {
					scope.profileData = { email: scope.blockParameters.email };
				}
			}
			confirmSubscription();
		}

		scope.submit = function() {
			if (scope.requestToken !== null) {
				scope.disabled = true;
				AjaxAPI.openWaitingModal();
				var data = {
					'email': scope.profileData.email,
					'userId': scope.blockParameters.userId,
					'profileData': scope.profileData,
					'optsIn': extractOptsInValues(),
					'device': buildDevice(),
					'LCID': scope.blockParameters._LCID,
					'requestToken': scope.requestToken
				};
				AjaxAPI.postData('Rbs/Mailinglist/Subscribe', data).then(
					function(result) {
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
						if (!scope.isConnected) {
							var cookieName = 'rbsMailinglistSubscription-' + scope.blockParameters.website + '-' +
								scope.blockParameters._LCID;
							var subscriptionLists = result.data.dataSets.optsIn;
							$cookies.put(cookieName, JSON.stringify(subscriptionLists));
						}
						scope.success = result.data.dataSets.success;
						scope.errors = null;
					},
					function(result) {
						scope.errors = result.data.message;
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
						console.log(result);
					}
				);
			}
		};

		// @deprecated since 1.8.0, use the rbsTrustHtml filter instead.
		scope.trustHtml = function(html) {
			console.warn('trustHtml() is deprecated since 1.8.0, use the rbsTrustHtml filter instead.');
			return $sce.trustAsHtml(html);
		};
		// END @deprecated

		scope.confirmSubscriptionRequest = function() {
			confirmSubscription();
		};

		function confirmSubscription() {
			scope.disabled = true;
			AjaxAPI.openWaitingModal();
			var data = {
				email: scope.profileData.email,
				token: scope.token
			};
			AjaxAPI.postData('Rbs/Mailinglist/ConfirmSubscription', data).then(
				function(result) {
					AjaxAPI.closeWaitingModal();
					scope.disabled = false;
					scope.success = result.data.dataSets.success;
					scope.errors = null;
				},
				function(result) {
					scope.errors = result.data.message;
					AjaxAPI.closeWaitingModal();
					scope.disabled = false;
					console.log(result);
				}
			);
		}

		function extractOptsInValues() {
			var optsIn = [];
			angular.forEach(scope.optsIn, function(optIn) {
				if (optIn !== false) {
					optsIn.push(optIn);
				}
			});
			return optsIn;
		}

		function buildDevice() {
			var userAgent = window.navigator.userAgent;
			var system =
				userAgent.match(/windows/i) ? 'Windows' :
					userAgent.match(/kindle/i) ? 'Kindle' :
						userAgent.match(/android/i) ? 'Android' :
							userAgent.match(/ipad/i) ? 'iPad' :
								userAgent.match(/iphone/i) ? 'iPhone' :
									userAgent.match(/ipod/i) ? 'iPod' :
										userAgent.match(/mac/i) ? 'OS X' :
											userAgent.match(/(linux|x11)/i) ? 'Linux' :
												'unknown system';

			var webBrowser =
				userAgent.match(/firefox/i) && !userAgent.match(/seamonkey/i) ? 'Firefox' :
					userAgent.match(/seamonkey/i) ? 'Seamonkey' :
						userAgent.match(/chrome/i) && !userAgent.match(/chromium/i) ? 'Chrome' :
							userAgent.match(/chromium/i) ? 'Chromium' :
								userAgent.match(/safari/i) && !userAgent.match(/chrome/i) && !userAgent.match(/chromium/i) ? 'Safari' :
									userAgent.match(/msie/i) || userAgent.match(/rv/i) ? 'Internet Explorer' :
										'unknown web browser';
			return webBrowser + ' - ' + system;
		}
	}

	app.controller('rbsMailinglistManageSubscription',
		['$scope', 'RbsChange.AjaxAPI', '$sce', '$cookies', rbsMailinglistManageSubscriptionController]);

	function rbsMailinglistManageSubscriptionController(scope, AjaxAPI, $sce, $cookies) {
		scope.optsIn = [];
		scope.mailingLists = {};
		scope.profileData = {};
		scope.isConnected = !!scope.blockParameters.userId;
		scope.successMessage = null;
		scope.showComplementaryFields = scope.blockParameters.showComplementaryFields;
		scope.requestToken = scope.blockParameters.requestToken;
		scope.subscriberId = scope.blockData.subscriberId ? scope.blockData.subscriberId + '' : null;
		scope.subscribers = scope.blockData.subscribers;
		scope.showSelector = false;
		scope.disabled = false;
		// ManageSubscriptions requests
		scope.success = {};
		scope.errors = null;
		scope.email = null;

		// Pre-fill Subscriber form data
		if (scope.subscriberId) {
			if (scope.blockData.hasOwnProperty('mailingLists') && angular.isObject(scope.blockData.mailingLists)) {
				scope.mailingLists = scope.blockData.mailingLists;
				fillOptIns();
			}

			if (scope.blockData.hasOwnProperty('profileData') && angular.isObject(scope.blockData.profileData)) {
				scope.profileData = angular.copy(scope.blockData.profileData);
				scope.showSelector = Object.keys(scope.profileData).length > 1;
			}
		}

		// @deprecated since 1.8.0, use the rbsTrustHtml filter instead.
		scope.trustHtml = function(html) {
			console.warn('trustHtml() is deprecated since 1.8.0, use the rbsTrustHtml filter instead.');
			return $sce.trustAsHtml(html);
		};
		// END @deprecated

		scope.submit = function() {
			if (scope.subscriberId) {
				scope.disabled = true;
				AjaxAPI.openWaitingModal();
				var data = {
					profileData: scope.profileData[scope.subscriberId],
					optsIn: scope.optsIn,
					email: scope.profileData[scope.subscriberId].email,
					subscriberId: scope.subscriberId
				};
				AjaxAPI.postData('Rbs/Mailinglist/UpdateSubscriptions', data).then(
					function(result) {
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
						scope.success = result.data.dataSets.success;
						scope.errors = null;
						scope.mailingLists[scope.subscriberId] = result.data.dataSets.mailingLists[scope.subscriberId];
						if (!scope.blockParameters.userId) {
							var cookieName = 'rbsMailinglistSubscription-' + scope.blockParameters.website + '-' +
								scope.blockParameters._LCID;
							var subscriptionLists = result.data.dataSets.optsIn;
							$cookies.put(cookieName, JSON.stringify(subscriptionLists));
						}
					},
					function(result) {
						scope.success = null;
						scope.errors = result.data.message;
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
					}
				);
			}
		};

		scope.request = function() {
			if (scope.email) {
				scope.disabled = true;
				AjaxAPI.openWaitingModal();
				var data = {
					email: scope.email,
					requestToken: scope.requestToken,
					request: true
				};
				AjaxAPI.postData('Rbs/Mailinglist/RequestSubscriptions', data).then(
					function(result) {
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
						scope.success = result.data.dataSets.success;
						scope.errors = null;
					},
					function(result) {
						scope.success = null;
						scope.errors = result.data.message;
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
					}
				);
			}
		};

		scope.confirmSubscriptionRequest = function() {
			if (scope.email) {
				scope.disabled = true;
				AjaxAPI.openWaitingModal();
				var data = {
					email: scope.email,
					token: scope.token
				};
				AjaxAPI.postData('Rbs/Mailinglist/RequestSubscriptions', data).then(
					function(result) {
						scope.success = result.data.dataSets;
						scope.errors = null;
						scope.subscriberId = result.data.dataSets.subscriberId;
						scope.profileData = result.data.dataSets.profileData;
						scope.profileData[scope.subscriberId].email = scope.email;
						scope.mailingLists = result.data.dataSets.mailingLists;
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
						fillOptIns();
					},
					function(result) {
						scope.success = null;
						scope.errors = result.data.message;
						AjaxAPI.closeWaitingModal();
						scope.disabled = false;
					}
				);
			}
		};

		function fillOptIns() {
			scope.optsIn = [];
			if (scope.mailingLists && scope.mailingLists[scope.subscriberId]) {
				angular.forEach(scope.mailingLists[scope.subscriberId], function(mailingList) {
					if (mailingList.common.subscribed === true) {
						scope.optsIn.push(mailingList.common.id);
					}
					else {
						scope.optsIn.push(null);
					}
				});
			}
		}

		scope.fillOptIns = fillOptIns;

		if (scope.blockParameters.email && scope.blockParameters.token) {
			scope.email = scope.blockParameters.email;
			scope.token = scope.blockParameters.token;
			scope.confirmSubscriptionRequest();
		}
	}
})();
