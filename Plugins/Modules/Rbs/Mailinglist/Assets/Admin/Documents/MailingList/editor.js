(function() {
	"use strict";

	function Editor($timeout, REST) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, element, attrs, editorCtrl) {
				scope.onLoad = function() {
					if (angular.isObject(scope.document.export)) {
						scope.export = {};
						scope.export = angular.copy(scope.document.export);
						if (scope.export.jobId && !scope.export.fileURI) {
							scope.checkJob();
						}
					}
					else {
						scope.export = {};
					}
					scope.hasJobs = false;
				};
				scope.showExportButton = function() {
					if (scope.document && scope.document.META$ && scope.document.META$.links &&
						scope.document.META$.links.export) {
						return true;
					}
					return false;
				};
				scope.showFileAccessButton = function() {
					if (scope.export && scope.export.jobId && !scope.hasJobs && scope.export.fileURI && scope.export.exportDate) {
						return true;
					}
					return false;
				};
				scope.jobIsRunning = function() {
					return scope.hasJobs;
				};
				scope.launchExport = function() {
					REST.apiGet('resources/Rbs/Mailinglist/MailingList/' + scope.document.id + '/' + scope.document.LCID + '/Export').then(
						function(result) {
							scope.export.jobId = result.jobId;
							scope.export.fileURI = null;
							scope.export.exportDate = null;
							scope.checkJob();
						},
						function(result) {
							console.error(result);
						}
					);
				};

				scope.checkJob = function() {
					scope.hasJobs = true;
					REST.job(scope.export.jobId).then(function(job) {
						scope.hasJobs = job && job.properties && job.properties.id == scope.export.jobId
							&& (job.properties.status == 'waiting' || job.properties.status == 'running');
						if (scope.hasJobs) {
							$timeout(scope.checkJob, 10000);
						}
						else {
							if (job.properties.status == 'success') {
								scope.export.jobId = job.properties.id;
								scope.export.fileURI = job.properties['arguments'].csvFile;
								scope.export.exportDate = job.properties.lastModificationDate;
							}
							else {
								scope.export = {};
							}
						}
					}, function() {
						scope.hasJobs = false;
					});
				};
			}
		}
	}

	Editor.$inject = ['$timeout', 'RbsChange.REST'];
	angular.module('RbsChange').directive('rbsDocumentEditorRbsMailinglistMailingListNew', Editor);
	angular.module('RbsChange').directive('rbsDocumentEditorRbsMailinglistMailingListEdit', Editor);
})();