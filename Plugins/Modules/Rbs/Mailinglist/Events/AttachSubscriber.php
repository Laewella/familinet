<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mailinglist\Events;

/**
 * @name \Rbs\Mailinglist\Events\AttachSubscriber
 */
class AttachSubscriber
{
	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function attachSubscriber(\Change\Events\Event $event)
	{
		$user = $event->getParam('user');
		if ($user && $user instanceof \Rbs\User\Documents\User)
		{
			/** @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$mailinglistManager = $genericServices->getMailinglistManager();
			$requestParameters = $event->getParam('requestParameters');
			$applicationServices = $event->getApplicationServices();
			$requestParameters['ipAddress'] = $_SERVER['REMOTE_ADDR'];
			if (isset($requestParameters['profileData']) && isset($requestParameters['optsIn']))
			{
				if(isset($requestParameters['websiteId']))
				{
					$dm = $applicationServices->getDocumentManager();
					$website = $dm->getDocumentInstance($requestParameters['websiteId'], 'Rbs_Website_Website');
					if($website instanceof \Rbs\Website\Documents\Website)
					{
						if($mailinglistManager->insertLoggedInSubscriber($user->getEmail(), $requestParameters, $website))
						{
							$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);
							$profile = $applicationServices->getProfileManager()->loadProfile($authenticatedUser, 'Rbs_User');
							$profileData = $requestParameters['profileData'];
							$tm = $applicationServices->getTransactionManager();
							$profile->setPropertyValue('firstName', $profileData['firstName']);
							$profile->setPropertyValue('lastName', $profileData['lastName']);
							$profile->setPropertyValue('phone', $profileData['phone']);
							$profile->setPropertyValue('birthDate', new \DateTime($profileData['birthDate']));
							try
							{
								$tm->begin();
								$applicationServices->getProfileManager()->saveProfile($user, $profile);
								$tm->commit();
							}
							catch (\Exception $e)
							{
								$tm->rollBack($e);
								throw $e;
							}
						}
					}
				}
			}
			else
			{
				$subscriber = $mailinglistManager->getSubscriberByUserAndEmail($user, $user->getEmail());
				if ($subscriber)
				{
					$tm = $applicationServices->getTransactionManager();
					$subscriber->setUser($user);
					try
					{
						$tm->begin();
						$subscriber->save();
						$tm->commit();
					}
					catch (\Exception $e)
					{
						$tm->rollBack($e);
						throw $e;
					}
				}
			}
		}
	}
}