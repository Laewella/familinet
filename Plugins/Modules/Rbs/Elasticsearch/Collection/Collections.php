<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Collection;

/**
 * @name \Rbs\Elasticsearch\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 * @return \Rbs\Generic\GenericServices
	 */
	protected function getGenericServices(\Change\Events\Event $event)
	{
		return $event->getServices('genericServices');
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addIndicesName(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$items = [
			'documents' => $i18nManager->trans('m.rbs.elasticsearch.admin.index_documents', ['ucf'])
		];
		$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_IndicesName', $items);
		$event->setParam('collection', $collection);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addIndexMapping(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$items = [
			'document' => $i18nManager->trans('m.rbs.elasticsearch.admin.index_mapping_document', ['ucf'])
		];
		$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_IndexMapping', $items);
		$event->setParam('collection', $collection);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addCollectionIds(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$docQuery = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Collection_Collection');
			$qb = $docQuery->dbQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$query = $qb->addColumn($fb->alias($docQuery->getColumn('id'), 'id'))
				->addColumn($fb->alias($docQuery->getColumn('label'), 'label'))->query();
			$items = $query->getResults($query->getRowsConverter()->addIntCol('id')->addStrCol('label')->indexBy('id'));
			$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_CollectionIds', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAxisIds(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$docQuery = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Catalog_Axis');
			$docQuery->andPredicates($docQuery->eq('indexed', true));
			$docQuery->addOrder('label');

			$items = [];
			/** @var \Rbs\Catalog\Documents\Axis $axis */
			foreach ($docQuery->getDocuments() as $axis)
			{
				$items[$axis->getId()] = $axis->getLabel();
			}
			asort($items);

			$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_Collection_AxisIds', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAttributeIds(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$q = $documentManager->getNewQuery('Rbs_Generic_Attribute');
		$q->andPredicates($q->eq('indexed', true));
		/** @var \Rbs\Generic\Documents\Attribute[] $attributes */
		$attributes = $q->getDocuments()->preLoad()->toArray();

		$excludedTypes = [
			\Change\Documents\Attributes\Interfaces\Attribute::TYPE_RICHTEXT,
			\Change\Documents\Attributes\Interfaces\Attribute::TYPE_JSON
		];

		$items = [];
		foreach ($attributes as $attribute)
		{
			$valueType = $attribute->getValueType();
			if (in_array($valueType, $excludedTypes) || ($valueType == \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING
					&& $attribute->getLocalizedValue()))
			{
				continue;
			}
			$items[$attribute->getId()] = $attribute->getLabel();
		}
		asort($items);

		$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_Collection_AttributeIds', $items);
		$event->setParam('collection', $collection);

	}

	/**
	 * @param \Change\Events\Event $event
	 * @return \Rbs\Generic\Documents\Attribute[]
	 */
	protected function getProductAttributes(\Change\Events\Event $event)
	{
		$collectionManager = $event->getApplicationServices()->getCollectionManager();
		$params = ['modelName' => 'Rbs_Catalog_Product'];
		$typologyCollection = $collectionManager->getCollection('Rbs_Generic_Typologies', $params);
		if (!$typologyCollection || count($typologyCollection->getItems()) == 0)
		{
			return [];
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$attributes = [];
		foreach ($typologyCollection->getItems() as $item)
		{
			$typology = $documentManager->getDocumentInstance($item->getValue());
			if (!($typology instanceof \Rbs\Generic\Documents\Typology))
			{
				continue;
			}

			foreach ($typology->getGroups() as $group)
			{
				foreach ($group->getAttributes() as $attribute)
				{
					$attributes[$attribute->getId()] = $attribute;
				}
			}
		}
		return $attributes;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addFacetConfigurationType(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18nManager = $applicationServices->getI18nManager();
			$items = [];
			$items['Model'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_configuration_type_model', ['ucf']);
			$items['Attribute'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_configuration_type_attribute', ['ucf']);
			$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_FacetConfigurationType', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addFacetRenderingModes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18nManager = $applicationServices->getI18nManager();
			$items = [];
			$items['radio'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_radio', ['ucf']);
			$items['checkbox'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_checkbox', ['ucf']);
			$forType = $event->getParam('forType');
			if ($forType == 'price')
			{
				$items['interval'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_interval', ['ucf']);
			}
			elseif ($forType == 'attribute' || $forType == 'axis')
			{
				$items['buttonsSingle'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_buttons_single', ['ucf']);
				$items['buttonsMultiple'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_buttons_multiple', ['ucf']);
				$items['swatchSingle'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_swatch_single', ['ucf']);
				$items['swatchMultiple'] = $i18nManager->trans('m.rbs.elasticsearch.admin.facet_rendering_modes_swatch_multiple', ['ucf']);
			}
			$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_Collection_FacetRenderingModes', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addResultFunction(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$items = [];
		$items['Rbs_Elasticsearch_Result'] = $i18nManager->trans('m.rbs.elasticsearch.admin.result_function', ['ucf']);
		$collection = new \Change\Collection\CollectionArray('Rbs_Elasticsearch_Result_Functions', $items);
		$event->setParam('collection', $collection);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Events\Event $event
	 */
	public function addProductListResolutionModes(\Change\Events\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}
}