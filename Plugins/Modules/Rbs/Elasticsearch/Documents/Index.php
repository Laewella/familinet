<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Documents;


/**
 * @deprecated since 1.8.0 with no replacement
 * @name \Rbs\Elasticsearch\Documents\Index
 */
class Index implements \Rbs\Elasticsearch\Index\IndexDefinitionInterface
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 */
	public function __construct()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @see \Rbs\Elasticsearch\Manager::getFacetsDefinition()
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	public function getFacetsDefinition()
	{
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string 'front'
	 */
	public function getClientName()
	{
		return 'front';
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string
	 */
	public function getName()
	{
		return 'deprecated';
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string fulltext | store
	 */
	public function getCategory()
	{
		return 'deprecated';
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string
	 */
	public function getAnalysisLCID()
	{
		return 'deprecated';
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return array
	 */
	public function getConfiguration()
	{
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Elasticsearch\Index\IndexManager $indexManager
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param \Change\Documents\AbstractModel $model
	 * @return array [type => [propety => value]]
	 */
	public function getDocumentIndexData(\Rbs\Elasticsearch\Index\IndexManager $indexManager, $document, $model = null)
	{
		return [];
	}
}
