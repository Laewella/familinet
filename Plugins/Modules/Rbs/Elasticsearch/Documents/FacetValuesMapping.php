<?php
namespace Rbs\Elasticsearch\Documents;

/**
 * @name \Rbs\Elasticsearch\Documents\FacetValuesMapping
 */
class FacetValuesMapping extends \Compilation\Rbs\Elasticsearch\Documents\FacetValuesMapping
{
	/**
	 * @var array
	 */
	protected $mapping;

	/**
	 * @return array
	 */
	public function getMapping()
	{
		if ($this->mapping === null)
		{
			$this->mapping = $this->buildMapping($this->getMappingData());
		}
		return $this->mapping;
	}

	/**
	 * @param array $mappingData
	 * @return array
	 */
	protected function buildMapping($mappingData)
	{
		$result = [];
		if (is_array($mappingData))
		{
			foreach ($mappingData as $data)
			{
				if (isset($data['v']))
				{
					$result[$data['v']] = $data['m'] ?? null;
				}
			}
		}
		return $result;
	}

	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		if ($event->getDocument() === $this)
		{
			$restResult = $event->getParam('restResult');
			if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
			{
				$mappingData = $restResult->getProperty('mappingData');
				$restResult->setProperty('mappingData', is_array($mappingData) ? $mappingData : []);
			}
		}
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'mappingData')
		{
			$this->mapping = $this->buildMapping($value);
			$mappingData = [];
			foreach ($this->mapping as $v => $m)
			{
				$mappingData[] = ['v' => $v, 'm' => $m];
			}
			$this->setMappingData(count($mappingData) ? $mappingData : null);
			return true;
		}
		return parent::processRestData($name, $value, $event);
	}

	/**
	 * @param null|integer|string $value
	 * @return null|integer|string
	 */
	public function getMappedValue($value)
	{
		if ($value !== null)
		{
			$mapping = $this->getMapping();
			if (array_key_exists($value, $mapping))
			{
				return $mapping[$value];
			}
			return $this->getDefaultMappingValue();
		}
		return null;
	}
}
