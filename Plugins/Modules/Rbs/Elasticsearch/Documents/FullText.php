<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Documents;

/**
 * @deprecated since 1.8.0 with no replacement
 * @name \Rbs\Elasticsearch\Documents\FullText
 */
class FullText extends \Rbs\Elasticsearch\Documents\Index
{

}
