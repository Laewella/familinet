<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Documents;

/**
 * @name \Rbs\Elasticsearch\Documents\Facet
 */
class Facet extends \Compilation\Rbs\Elasticsearch\Documents\Facet
{
	/**
	 * @var \Rbs\Elasticsearch\Facet\FacetDefinitionInterface|null;
	 */
	protected $parent;

	/**
	 * @var \Zend\Stdlib\Parameters|null
	 */
	protected $parameters;

	/**
	 * @var \Rbs\Elasticsearch\Facet\FacetDefinitionInterface|boolean|null
	 */
	protected $facetDefinition = false;

	/**
	 * Attach specific document event
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onDefaultSave($event); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onDefaultSave($event); }, 10);
		$eventManager->attach('getFacetDefinition', function ($event) { $this->onDefaultGetFacetDefinition($event); }, 5);
		$eventManager->attach('getFacetDefinition', function ($event) { $this->onSetChildrenFacetDefinition($event); }, 1);
	}

	/**
	 * @param array $parameters
	 * @return $this
	 */
	public function setParameters($parameters = null)
	{
		$this->parameters = new \Zend\Stdlib\Parameters();
		if (is_array($parameters))
		{
			$this->parameters->fromArray($parameters);
		}
		elseif ($parameters instanceof \Traversable)
		{
			/** @noinspection ForeachSourceInspection */
			foreach ($parameters as $n => $v)
			{
				$this->parameters->set($n, $v);
			}
		}
		return $this;
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getParameters()
	{
		if ($this->parameters === null)
		{
			$v = $this->getParametersData();
			$this->parameters = new \Zend\Stdlib\Parameters(is_array($v) ? $v : null);
		}
		return $this->parameters;
	}

	/**
	 * @param null|\Rbs\Elasticsearch\Facet\\Rbs\Elasticsearch\Facet\FacetDefinitionInterface $parent
	 * @return $this
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
		return $this;
	}

	/**
	 * @return null|\Rbs\Elasticsearch\Facet\FacetDefinitionInterface
	 */
	protected function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		/** @var $facet Facet */
		$facet = $event->getDocument();
		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$documentResult = $restResult;
			$documentResult->setProperty('parameters', $facet->getParametersData());

			$collection = $event->getApplicationServices()->getCollectionManager()->getCollection('Rbs_Elasticsearch_IndicesName');
			$indexName = $facet->getIndexName();
			$item = $collection->getItemByValue($indexName);
			$documentResult->setProperty('indexNameLabel', $item ? $item->getLabel() : $indexName);
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$linkResult = $restResult;
			$linkResult->setProperty('facetsCount', $facet->getFacetsCount());
			$collection = $event->getApplicationServices()->getCollectionManager()->getCollection('Rbs_Elasticsearch_IndicesName');
			$indexName = $facet->getIndexName();
			$item = $collection->getItemByValue($indexName);
			$linkResult->setProperty('indexNameLabel', $item ? $item->getLabel() : $indexName);

			$collection = $event->getApplicationServices()->getCollectionManager()->getCollection('Rbs_Elasticsearch_FacetConfigurationType');
			$configurationType = $facet->getConfigurationType();
			$item = $collection->getItemByValue($configurationType);
			$linkResult->setProperty('configurationTypeLabel', $item ? $item->getLabel() : $configurationType);
		}
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'parameters')
		{
			$this->setParameters($value);
			return true;
		}
		else
		{
			return parent::processRestData($name, $value, $event);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultSave(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}

		if ($this->isPropertyModified('parametersData') || $this->isPropertyModified('configurationType'))
		{
			switch ($this->getConfigurationType())
			{
				case 'Model':
					$applicationServices =$event->getApplicationServices();
					$facetDefinition = new \Rbs\Elasticsearch\Facet\ModelFacetDefinition($this);
					$facetDefinition->setDocumentManager($this->getDocumentManager());
					$facetDefinition->setI18nManager($applicationServices->getI18nManager())
						->setModelManager($applicationServices->getModelManager());
					$facetDefinition->validateConfiguration($this);
					$this->saveWrappedProperties();
					break;
				case 'Attribute':
					$facetDefinition = new \Rbs\Elasticsearch\Facet\AttributeFacetDefinition($this);
					$facetDefinition->setDocumentManager($this->getDocumentManager());
					$facetDefinition->validateConfiguration($this);
					$this->saveWrappedProperties();
					break;
			}
		}
	}

	public function saveWrappedProperties()
	{
		if ($this->parameters)
		{
			$parametersData = $this->parameters->toArray();
			$this->parameters = null;
			$this->setParametersData(count($parametersData) ? $parametersData : null);
		}
	}

	protected function onCreate()
	{
		$this->saveWrappedProperties();
	}

	protected function onUpdate()
	{
		$this->saveWrappedProperties();
	}

	/**
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface|null
	 */
	public function getFacetDefinition()
	{
		if ($this->facetDefinition === false)
		{
			$this->facetDefinition = null;
			$event = new \Change\Documents\Events\Event('getFacetDefinition', $this, []);
			$this->getEventManager()->triggerEvent($event);
			$facetDefinition = $event->getParam('facetDefinition');
			if ($facetDefinition instanceof \Rbs\Elasticsearch\Facet\FacetDefinitionInterface)
			{
				$this->facetDefinition = $facetDefinition;
			}
		}
		return $this->facetDefinition;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetFacetDefinition(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('facetDefinition') !== null)
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$facetDefinition = null;
		switch ($this->getConfigurationType())
		{
			case 'Model':
				$facetDefinition = new \Rbs\Elasticsearch\Facet\ModelFacetDefinition($this);
				$facetDefinition->setI18nManager($applicationServices->getI18nManager())
					->setModelManager($applicationServices->getModelManager());
				$event->setParam('facetDefinition', $facetDefinition);
				break;
			case 'Attribute':
				$facetDefinition = new \Rbs\Elasticsearch\Facet\AttributeFacetDefinition($this);
				$event->setParam('facetDefinition', $facetDefinition);
				break;
		}
		$event->setParam('facetDefinition', $facetDefinition);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onSetChildrenFacetDefinition(\Change\Documents\Events\Event $event)
	{
		$facetDefinition = $event->getParam('facetDefinition');
		if ($facetDefinition instanceof \Rbs\Elasticsearch\Facet\DocumentFacetDefinition)
		{
			$applicationServices = $event->getApplicationServices();
			$facetDefinition->setDocumentManager($applicationServices->getDocumentManager());
			$facetDefinition->setParent($this->getParent());
			if ($this->getFacetsCount())
			{
				$children = [];
				foreach ($this->getFacets() as $child)
				{
					if (!$child->getParent())
					{
						$child->setParent($facetDefinition);
						$childFacetDefinition = $child->getFacetDefinition();
						if ($childFacetDefinition)
						{
							$children[] = $childFacetDefinition;
						}
						$child->setParent(null);
					}
				}
				$facetDefinition->setChildren($children);
			}
			$event->setParam('facetDefinition', $facetDefinition);
		}
	}

	/**
	 * @return string|null
	 */
	public function getIndexMapping()
	{
		$definition = $this->getFacetDefinition();
		if ($definition instanceof \Rbs\Elasticsearch\Facet\FacetDefinitionInterface)
		{
			return $definition->getType();
		}
		return null;
	}


	/**
	 * @deprecated since 1.8.0 use getIndexName()
	 * @return string
	 */
	public function getIndexCategory()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this->getIndexName();
	}

	/**
	 * @deprecated since 1.8.0 use getIndexName()
	 * @param string $indexCategory
	 * @return $this
	 */
	public function setIndexCategory($indexCategory)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return \Change\Documents\AbstractDocument
	 */
	public function getFacetValuesMapping()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\AbstractDocument $facetValuesMapping
	 * @return \Compilation\Rbs\Elasticsearch\Documents\Facet
	 */
	public function setFacetValuesMapping($facetValuesMapping)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this;
	}
}
