<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch;

/**
 * @name \Rbs\Elasticsearch\Manager
 */
class Manager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const DOCUMENTS_INDEX = 'documents';

	const EVENT_MANAGER_IDENTIFIER = 'Rbs_Elasticsearch_Manager';

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var array
	 */
	protected $configuration;
	/**
	 * @var array
	 */
	protected $indices = [];

	/**
	 * @var array
	 */
	protected $indicesExists = [];

	/**
	 * @var array
	 */
	protected $facetsDefinition;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Elasticsearch/Events/Manager');
	}

	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('getIndexConfiguration', function ($event) { $this->onGetDocumentsIndexConfiguration($event); }, 10);
		$eventManager->attach('getIndexConfiguration', function ($event) { $this->onGetDocumentsIndexLCIDConfiguration($event); }, 1);

		$eventManager->attach('getDocumentIndexGenerator', function ($event) { $this->onGetDocumentIndexGenerator($event); }, 10);
		$eventManager->attach('getIndexData', function ($event) { $this->onGetDocumentIndexData($event); }, 10);

		$eventManager->attach('updateDocumentsIndex', function ($event) { $this->onUpdateDocumentsIndex($event); }, 10);
		$eventManager->attach('updateDocumentsIndex', function ($event) { $this->onUpdateDocumentsDependencyIndex($event); }, 1);

		$eventManager->attach('getFacetsDefinition', function ($event) { $this->onGetDocumentsFacetsDefinition($event); }, 10);

		$eventManager->attach('getDocumentsResultFunctionsData', function ($event) { $this->onGetDocumentsResultFunctionsData($event); }, 10);
	}

	/**
	 * @return array
	 */
	protected function getConfiguration()
	{
		if ($this->configuration === null)
		{
			$this->setConfiguration($this->getApplication()->getConfiguration('Rbs/Elasticsearch') ?? []);
		}
		return $this->configuration;
	}

	/**
	 * @param array $configuration
	 * @return $this
	 */
	public function setConfiguration(array $configuration)
	{
		$this->configuration = $configuration;
		return $this;
	}

	/**
	 * @api
	 * @return string[]
	 */
	public function getIndicesName()
	{
		$indicesName = [];
		$configuration = $this->getConfiguration();
		/** @var array $indices */
		$indices = $configuration['indices']  ?? [];
		if ($indices && is_array($indices))
		{
			foreach ($indices as $name => $config)
			{
				if ($name && is_string($name) && $config && is_array($config))
				{
					$indicesName[] = $name;
				}
			}
		}
		return $indicesName;
	}

	/**
	 * @api
	 * @param string $name
	 * @param string|null $suffix
	 * @return \Elastica\Index|null
	 */
	public function getIndex($name, $suffix = null)
	{
		$k = $name . ($suffix ?: '');
		$index = $this->indices[$k] ?? null;
		if ($index === null)
		{
			$this->indices[$name] = false;
			$configuration = $this->getConfiguration();
			$clientConfig = $configuration['indices'][$name] ?? null;
			if (is_array($clientConfig))
			{
				$indexName = ($configuration['prefix'] ?? '') . $name . ($suffix ?: '');
				$client = new \Elastica\Client($clientConfig);
				$this->indices[$k] = $index = $client->getIndex($indexName);
			}
		}
		return $index instanceof \Elastica\Index ? $index : null;
	}

	/**
	 * @api
	 * @param string $name
	 * @return array
	 */
	public function getIndexConfiguration($name)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['name' => $name, 'configuration' => null]);
		$eventManager->trigger('getIndexConfiguration', $this, $args);
		$configuration = $args['configuration'];
		if ($configuration && isset($configuration['settings'], $configuration['mappings']))
		{
			return $configuration;
		}
		return [];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetDocumentsIndexConfiguration(\Change\Events\Event $event)
	{
		if ($event->getParam('configuration') !== null || $event->getParam('name') !== self::DOCUMENTS_INDEX)
		{
			return;
		}

		$configurationFile = __DIR__ . '/Assets/Config/documents.json';
		$configuration = json_decode(file_get_contents($configurationFile), true);
		$event->setParam('configuration', $configuration);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetDocumentsIndexLCIDConfiguration(\Change\Events\Event $event)
	{
		$configuration = $event->getParam('configuration');
		if ($configuration === null || !isset($configuration['mappings']['lcid']) || $event->getParam('name') !== self::DOCUMENTS_INDEX)
		{
			return;
		}
		$defaultLCIDMappings = $configuration['mappings']['lcid']['properties'];
		unset($configuration['mappings']['lcid']);

		$LCIDMappings = [];
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$sync = $i18nManager->getI18nSynchro();
		foreach ($i18nManager->getSupportedLCIDs() as $LCID)
		{
			if (isset($LCIDMappings[$LCID]))
			{
				continue;
			}

			if (isset($defaultLCIDMappings[$LCID]))
			{
				$LCIDMappings[$LCID] = $defaultLCIDMappings[$LCID];
			}
			else
			{
				$fromLCID = $sync[$LCID] ?? 'fr_FR';
				$mapping = $LCIDMappings[$fromLCID] ?? $defaultLCIDMappings[$fromLCID] ?? null;
				while (!$mapping)
				{
					$fromLCID = $sync[$fromLCID] ?? 'fr_FR';
					$mapping = $LCIDMappings[$fromLCID] ?? $defaultLCIDMappings[$fromLCID] ?? null;
				}
				$LCIDMappings[$LCID] = $mapping;
			}
		}

		/** @noinspection ForeachSourceInspection */
		foreach ($configuration['mappings'] as $type => &$mapping)
		{
			if ($type === '_default_')
			{
				continue;
			}
			foreach ($LCIDMappings as $LCID => $LCIDMapping)
			{
				$mapping['properties'][$LCID] = $LCIDMapping;
			}
		}
		unset($mapping);
		$event->setParam('configuration', $configuration);
	}

	/**
	 * @param string $name
	 * @return \Generator
	 */
	public function getDocumentIndexGenerator($name)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['name' => $name, 'generator' => null]);
		$eventManager->trigger('getDocumentIndexGenerator', $this, $args);

		$generator = $args['generator'];
		if (!is_callable($generator))
		{
			$generator = function ()
			{
				if (false)
				{
					yield;
				}
			};
		}
		return $generator();
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetDocumentIndexGenerator(\Change\Events\Event $event)
	{
		if ($event->getParam('generator') !== null)
		{
			return;
		}

		if ($event->getParam('name') === self::DOCUMENTS_INDEX)
		{
			$generator = new \Rbs\Elasticsearch\Indices\DocumentsGenerator($this->documentManager);
			$event->setParam('generator', $generator);
		}
	}

	/**
	 * @api
	 * @param string $indexName
	 * @param mixed $sourceDocument
	 * @return array
	 */
	public function getIndexData($indexName, $sourceDocument)
	{
		try
		{
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['indexName' => $indexName, 'sourceDocument' => $sourceDocument, 'data' => null]);
			$eventManager->trigger('getIndexData', $this, $args);
			$dataByType = $args['data'];
			return is_array($dataByType) ? $dataByType : [];
		}
		catch (\Exception $e)
		{
			$this->getApplication()->getLogging()->exception($e);
		}
		return [];
	}

	/**
	 * @var \Rbs\Elasticsearch\Indices\DocumentsData
	 */
	protected $documentsData;

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetDocumentIndexData(\Change\Events\Event $event)
	{
		if ($event->getParam('data') !== null || $event->getParam('indexName') !== self::DOCUMENTS_INDEX)
		{
			return;
		}
		$sourceDocument = $event->getParam('sourceDocument');
		if ($sourceDocument instanceof \Change\Documents\Interfaces\Publishable)
		{
			if (!$this->documentsData)
			{
				$this->documentsData = new \Rbs\Elasticsearch\Indices\DocumentsData($this->getDocumentManager(),
					$event->getApplicationServices()->getRichTextManager());
			}

			$data = $this->documentsData->addPublishableMetas($sourceDocument,
				['_id' => (string)$sourceDocument->getId(), '_type' => 'document']);

			$data = $this->documentsData->addTypologyData($sourceDocument, $data);

			$event->setParam('data', $data);
		}
	}

	/**
	 * @api
	 * @param \Elastica\Index $index
	 * @param array $ids
	 */
	public function updateDocumentsIndex(\Elastica\Index $index, array $ids)
	{
		$documentManager = $this->getDocumentManager();
		$documentIds = new \Rbs\Elasticsearch\Indices\DocumentIds($documentManager, $ids);
		$indexName = $index->getName();
		$eventManager = $this->getEventManager();
		$bulk = new \Elastica\Bulk($index->getClient());
		$bulkLength = 0;
		$logging = $this->getApplication()->getLogging();
		foreach ($documentIds->generator() as $id => $model)
		{
			$data = null;
			$doc = $documentManager->getDocumentInstance($id);
			if ($doc)
			{
				$data = $this->getIndexData(self::DOCUMENTS_INDEX, $doc);
			}

			try
			{
				$args =
					$eventManager->prepareArgs(['index' => $index, 'id' => $id, 'model' => $model, 'documentIds' => $documentIds, 'data' => $data]);
				$eventManager->trigger('updateDocumentsIndex', $this, $args);
				$data = $args['data'];

				if (isset($data['_id'], $data['_type'], $data['documentId']))
				{
					$id = $data['_id'];
					$type = $data['_type'];
					unset($data['_id'], $data['_type']);
					$bulk->addDocument(new \Elastica\Document($id, $data, $type, $indexName));
					$bulkLength++;
				}
				elseif (isset($data['_id'], $data['_type'], $data['_delete']))
				{
					$type = $data['_type'];
					$action = (new \Elastica\Bulk\Action(\Elastica\Bulk\Action::OP_TYPE_DELETE))
						->setId($data['_id'])->setIndex($indexName)->setType($type);
					$bulk->addAction($action);
					$bulkLength++;
				}
			}
			catch (\Throwable $t)
			{
				$this->getApplication()->getLogging()->throwable($t);
			}

			if ($bulkLength >= 100)
			{
				try
				{
					$bulk->send();
				}
				catch (\Throwable $t)
				{
					$this->getApplication()->getLogging()->throwable($t);
				}
				$bulk = new \Elastica\Bulk($index->getClient());
				$bulkLength = 0;
			}
		}
		if ($bulk->getActions())
		{
			try
			{
				$bulk->send();
			}
			catch (\Throwable $t)
			{
				$this->getApplication()->getLogging()->throwable($t);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * event input params:
	 *  - \Elastica\Index index
	 *  - integer id
	 *  - string model
	 *  - \Rbs\Elasticsearch\Indices\DocumentIds documentIds
	 *  - array|null data
	 */
	protected function onUpdateDocumentsIndex(\Change\Events\Event $event)
	{
		$id = $event->getParam('id');
		$modelName = $event->getParam('model');
		/** @var array $data */
		$data = $event->getParam('data');
		if (!$data)
		{
			$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
			if ($model && $model->isPublishable())
			{
				$data = ['_id' => (string)$id, '_type' => 'document', '_delete' => true];
				$event->setParam('data', $data);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * event input params:
	 *  - \Elastica\Index index
	 *  - integer id
	 *  - string model
	 *  - \Rbs\Elasticsearch\Indices\DocumentIds documentIds
	 *  - array|null data
	 *  - bool|array updateDependencies
	 */
	protected function onUpdateDocumentsDependencyIndex(\Change\Events\Event $event)
	{
		/** @var \Elastica\Index $index */
		$index = $event->getParam('index');
		$id = $event->getParam('id');
		$ids = $event->getParam('updateDependencies');

		/** @var \Rbs\Elasticsearch\Indices\DocumentIds $documentIds */
		$documentIds = $event->getParam('documentIds');
		$bool = new \Elastica\Query\BoolQuery();
		if ($ids && is_array($ids))
		{
			$bool->addMust(new \Elastica\Query\Terms('dependencies', $ids));
		}
		else
		{
			$bool->addMust(new \Elastica\Query\Term(['dependencies' => $id]));
		}
		$query = new \Elastica\Query($bool);
		$query->setSource(['documentId', 'model'])->setSort(['documentId' => ['order' => 'asc']])->setSize(100);
		$count = 1;
		$from = 0;
		while ($from < $count)
		{
			$query->setFrom($from);
			$rs = $index->search($query);
			$count = $rs->getTotalHits();
			$results = $rs->getResults();
			if (!$results)
			{
				break;
			}
			foreach ($results as $r)
			{
				$from++;
				$source = $r->getSource();
				if (($id = $source['documentId'] ?? 0) && ($model = $source['model'] ?? null))
				{
					$documentIds->addId($id, $model);
				}
			}
		}
	}

	/**
	 * @api
	 * @param string $indexName
	 * @param bool $exists
	 * @return \Elastica\Index|null
	 */
	public function getSearchIndex($indexName, $exists = false)
	{
		if (!$indexName)
		{
			return null;
		}
		$index = $this->indices[$indexName] ?? null;
		if ($index === null)
		{
			$this->indices[$indexName] = false;
			$this->indicesExists[$indexName] = false;
			$configuration = $this->getConfiguration();
			$clientConfig = $configuration['indices'][$indexName] ?? null;
			if (is_array($clientConfig))
			{
				$aliasName = ($configuration['prefix'] ?? '') . $indexName;
				$client = new \Elastica\Client($clientConfig);
				$this->indices[$indexName] = $index = $client->getIndex($aliasName);
				$this->indicesExists[$indexName] = $index->exists();
			}
		}
		if ($index && (!$exists || $this->indicesExists[$indexName]))
		{
			return $index;
		}
		return null;
	}

	/**
	 * @api
	 * @param bool $exists
	 * @return \Elastica\Index|null
	 */
	public function getDocumentsSearchIndex($exists = false)
	{
		return $this->getSearchIndex(self::DOCUMENTS_INDEX, $exists);
	}

	/**
	 * @api
	 * @param string $indexName
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	public function getFacetsDefinition($indexName)
	{
		if (!$indexName)
		{
			return [];
		}
		if (!isset($this->facetsDefinition[$indexName]))
		{
			$this->facetsDefinition[$indexName] = [];
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['indexName' => $indexName, 'facetsDefinition' => null]);
			$eventManager->trigger('getFacetsDefinition', $this, $args);
			$facetsDefinition = $args['facetsDefinition'];
			if ($facetsDefinition && is_array($facetsDefinition))
			{
				$facetsDefinition = array_filter($facetsDefinition, function ($def)
				{
					return $def instanceof \Rbs\Elasticsearch\Facet\FacetDefinitionInterface;
				});
				$this->facetsDefinition[$indexName] = array_values($facetsDefinition);
			}
		}
		return $this->facetsDefinition[$indexName];
	}

	/**
	 * @param \Change\Events\Event $event
	 * event input params:
	 *  - string indexName
	 * event output params:
	 *  - \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] facetsDefinition
	 */
	protected function onGetDocumentsFacetsDefinition(\Change\Events\Event $event)
	{
		if ($event->getParam('facetsDefinition') !== null || $event->getParam('indexName') !== self::DOCUMENTS_INDEX)
		{
			return;
		}
		$facetsDefinition = [];
		$q = $this->getDocumentManager()->getNewQuery('Rbs_Elasticsearch_Facet');
		$q->andPredicates($q->eq('indexName', self::DOCUMENTS_INDEX));
		/** @var \Rbs\Elasticsearch\Documents\Facet[] $facets */
		$facets = $q->getDocuments()->preLoad()->toArray();
		foreach ($facets as $facet)
		{
			$def = $facet->getFacetDefinition();
			if ($def instanceof \Rbs\Elasticsearch\Facet\FacetDefinitionInterface)
			{
				$facetsDefinition[] = $def;
			}
		}
		$event->setParam('facetsDefinition', $facetsDefinition);
	}

	/**
	 * @api
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	public function getDocumentsFacetsDefinition()
	{
		return $this->getFacetsDefinition(self::DOCUMENTS_INDEX);
	}

	/**
	 * @param array $ids
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] indexed by id
	 */
	public function getDocumentsFacetsDefinitionByIds(array $ids)
	{
		$facetsDefinition = [];
		foreach ($this->getDocumentsFacetsDefinition() as $facetDefinition)
		{
			if (in_array($facetDefinition->getId(), $ids))
			{
				$facetsDefinition[$facetDefinition->getId()] = $facetDefinition;
			}
		}
		return $facetsDefinition;
	}

	/**
	 * @param string $type
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	public function getDocumentsFacetsDefinitionByType(string $type)
	{
		$facetsDefinition = [];
		foreach ($this->getDocumentsFacetsDefinition() as $facetDefinition)
		{
			if ($facetDefinition->getType() === $type)
			{
				$facetsDefinition[] = $facetDefinition;
			}
		}
		return $facetsDefinition;
	}

	/**
	 * @param string $profile
	 * @return array
	 */
	public function getDocumentsResultFunctionsData($profile = null)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['profile' => $profile, 'resultFunctionsData' => null]);
		$eventManager->trigger('getDocumentsResultFunctionsData', $this, $args);
		$resultFunctionsData = $args['resultFunctionsData'];
		return is_array($resultFunctionsData) ? array_values($resultFunctionsData) : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 * event input params:
	 *  - string profile
	 * event output params:
	 *  - array resultFunctionsData
	 */
	protected function onGetDocumentsResultFunctionsData(\Change\Events\Event $event)
	{
		if ($event->getParam('resultFunctionsData') !== null)
		{
			return;
		}
		$resultFunctionsData = [
			'Rbs_Elasticsearch_Result' => [
				'name' => 'Rbs_Elasticsearch_Result',
				't0' => 'm.rbs.elasticsearch.front.no_content',
				't1' => 'm.rbs.elasticsearch.front.one_content',
				'tn' => 'm.rbs.elasticsearch.front.several_contents'
			]
		];
		$event->setParam('resultFunctionsData', $resultFunctionsData);
	}
}