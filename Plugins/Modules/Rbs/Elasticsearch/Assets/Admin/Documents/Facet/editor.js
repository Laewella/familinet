/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsElasticsearchFacetEdit', ['$compile', 'RbsChange.Utils', Editor]);
	app.directive('rbsDocumentEditorRbsElasticsearchFacetNew', ['$compile', 'RbsChange.Utils', Editor]);

	function Editor($compile, Utils) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',
			link: function(scope, elm) {
				scope.$watch('document.configurationType', function(configurationType) {
					redrawConfigurationOptions($compile, scope, configurationType);
				});

				var contentScopes = {};

				function redrawConfigurationOptions($compile, scope, configurationType) {
					var id = scope.$id;
					if (contentScopes[id]) {
						contentScopes[id].$destroy();
						contentScopes[id] = null;
					}

					var container = elm.find('#RbsElasticsearchFacetConfigurationOptions');
					container.children().remove();

					if (configurationType) {
						var directiveName = 'data-rbs-elasticsearch-facet-' + Utils.normalizeAttrName(configurationType);
						container.html('<div ' + directiveName + '="" data-facet="document"></div>');
						contentScopes[id] = scope.$new();
						$compile(container.children())(contentScopes[id]);
					}
				}
			}
		};
	}
})();