(function() {
	"use strict";

	function rbsElasticsearchFacetAttribute() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Elasticsearch/Documents/Facet/attribute-configuration.twig',
			scope: { facet: '=' },
			link: function(scope) {
				if (!angular.isObject(scope.facet.parameters) || angular.isArray(scope.facet.parameters)) {
					scope.facet.parameters = {};
				}
				var parameters = scope.facet.parameters;
				if (!angular.isDefined(parameters.renderingMode)) {
					parameters.renderingMode = 'checkbox';
				}
				if (!angular.isDefined(parameters.showEmptyItem)) {
					parameters.showEmptyItem = false;
				}
			}
		}
	}
	angular.module('RbsChange').directive('rbsElasticsearchFacetAttribute', rbsElasticsearchFacetAttribute);

	function rbsElasticsearchFacetModel() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Elasticsearch/Documents/Facet/model-configuration.twig',
			scope: { facet: '=' },
			link: function(scope) {
				if (!angular.isObject(scope.facet.parameters) || angular.isArray(scope.facet.parameters)) {
					scope.facet.parameters = {};
				}
				var parameters = scope.facet.parameters;
				if (!angular.isDefined(parameters.renderingMode)) {
					parameters.renderingMode = 'checkbox';
				}
				if (!angular.isDefined(parameters.showEmptyItem)) {
					parameters.showEmptyItem = false;
				}
			}
		}
	}
	angular.module('RbsChange').directive('rbsElasticsearchFacetModel', rbsElasticsearchFacetModel);

	function rbsElasticsearchFacetMaxDisplayedValues() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Elasticsearch/Documents/Facet/max-displayed-values-configuration.twig',
			scope: { facet: '=' },
			link: function(scope) {
				if (!angular.isObject(scope.facet.parameters) || angular.isArray(scope.facet.parameters)) {
					scope.facet.parameters = {};
				}
				var parameters = scope.facet.parameters;
				if (!angular.isDefined(parameters.showAllValues)) {
					parameters.showAllValues = false;
				}
				if (!angular.isDefined(parameters.maxDisplayedValues)) {
					parameters.maxDisplayedValues = 10;
				}

				scope.$watch('facet.parameters.showAllValues', function(showAllValues, oldValue) {
					if (showAllValues) {
						scope.facet.parameters.maxDisplayedValues = 0;
					}
					else if(showAllValues !== oldValue) {
						scope.facet.parameters.maxDisplayedValues = 10;
					}
				});
			}
		}
	}
	angular.module('RbsChange').directive('rbsElasticsearchFacetMaxDisplayedValues', rbsElasticsearchFacetMaxDisplayedValues);
})();


