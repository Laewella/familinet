/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	"use strict";

	function Editor(REST) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',
			link: function(scope) {
				scope.newMappingItem = { v: null, m: null };
				scope.assoc = { search: null, mapped: null, override: false, distinctMapped: {} };
				scope.allInputValues = [];
				scope.inputValues = [];
				scope.pagination = { size: 15, offset: 0, items: [] };

				scope.$watchCollection('document.mappingData', function(mappingData) {
					if (angular.isArray(mappingData)) {
						scope.buildDistinctMapped(mappingData);
						scope.buildPageItems(mappingData);
					}
					else {
						scope.assoc.distinctMapped = {};
						scope.pagination = { size: 15, offset: 0, items: [] };
					}
				});

				scope.changePage = function(page) {
					var length = scope.document.mappingData.length;
					if (length) {
						scope.pagination.offset += page * scope.pagination.size;
						if (scope.pagination.offset >= length) {
							scope.pagination.offset = length;
							scope.pagination.offset -= length % scope.pagination.size;
						}

						if (scope.pagination.offset < 0) {
							scope.pagination.offset = 0;
						}
						scope.buildPageItems(scope.document.mappingData);
					}
				};

				scope.buildPageItems = function(mappingData) {
					scope.pagination.items = [];
					var maxOffset = scope.pagination.offset + scope.pagination.size;
					if (maxOffset > mappingData.length) {
						maxOffset = mappingData.length
					}
					for (var i = scope.pagination.offset; i < maxOffset; i++) {
						scope.pagination.items.push(mappingData[i]);
					}
				};

				scope.buildDistinctMapped = function(mappingData) {
					scope.assoc.distinctMapped = {};
					angular.forEach(mappingData, function(mapping) {
						if (angular.isString(mapping.m)) {
							scope.assoc.distinctMapped[mapping.m] = mapping.m;
						}
					});
				};

				scope.buildNewInputValue = function() {
					var data = scope.allInputValues;
					scope.inputValues = [];
					var check = {};
					angular.forEach(scope.document.mappingData, function(ci) {
						if (angular.isString(ci.v)) {
							check[ci.v] = true;
						}
					});
					angular.forEach(data, function(v) {
						if (angular.isString(v) && !check[v]) {
							scope.inputValues.push(v);
						}
					});
				};

				scope.onLoad = function() {
					if (scope.document.META$.url) {
						var inputValuesURL = scope.document.META$.url + '/InputValues/';
						var p = REST.call(inputValuesURL, {});
						p.then(function(data) {
							scope.inputValues = [];
							if (angular.isArray(data)) {
								scope.allInputValues = data;
								scope.buildNewInputValue();
							}
						});
					}
				};

				scope.onReload = function() {
					scope.newMappingItem = { v: null, m: null };
					scope.buildNewInputValue();
				};

				scope.deleteMappingItem = function(item) {
					var mappingData = [];
					angular.forEach(scope.document.mappingData, function(ci) {
						if (ci !== item) {
							mappingData.push(ci)
						}
					});
					scope.document.mappingData = mappingData;
					scope.buildNewInputValue();
				};

				scope.deleteNullMapping = function() {
					var mappingData = [];
					angular.forEach(scope.document.mappingData, function(ci) {
						if (ci.m) {
							mappingData.push(ci);
						}
					});
					scope.document.mappingData = mappingData;
					scope.buildNewInputValue();
				};

				scope.addMappingItem = function() {
					if (scope.newMappingItem && scope.newMappingItem.v) {
						if (!angular.isArray(scope.document.mappingData)) {
							scope.document.mappingData = [];
						}
						scope.document.mappingData.push(scope.newMappingItem);
						scope.newMappingItem = { v: null, m: null };
						scope.buildNewInputValue();
					}
				};

				scope.addInputValues = function() {
					var entries = {};
					angular.forEach(scope.document.mappingData, function(mapping) {
						entries[mapping.v] = true;
					});

					angular.forEach(scope.inputValues, function(v) {
						if (!entries.hasOwnProperty(v)) {
							var newMappingItem = { v: v, m: null };
							scope.document.mappingData.push(newMappingItem);
						}
					});
				};

				scope.assocMapping = function(searchValue, mappedValue, override) {
					if (angular.isString(searchValue)) {
						searchValue = searchValue.toLowerCase();
						if (mappedValue === '') {
							mappedValue = null;
						}
						angular.forEach(scope.document.mappingData, function(mapping) {
							if (angular.isString(mapping.v)) {
								if (override || mapping.m === null) {
									var lv = mapping.v.toLowerCase();
									if (lv.indexOf(searchValue) >= 0) {
										mapping.m = mappedValue;
									}
								}
							}
						});

						scope.scope.buildDistinctMapped(scope.document.mappingData);
					}
				}
			}
		};
	}

	Editor.$inject = ['RbsChange.REST'];
	angular.module('RbsChange').directive('rbsDocumentEditorRbsElasticsearchFacetValuesMappingNew', Editor);
	angular.module('RbsChange').directive('rbsDocumentEditorRbsElasticsearchFacetValuesMappingEdit', Editor);
})();