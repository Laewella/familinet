(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.controller('RbsElasticsearchResultHeaderController', ['$scope', '$rootScope', 'RbsChange.AjaxAPI', function(scope, $rootScope, AjaxAPI) {
		scope.searchText = scope.blockParameters && scope.blockParameters.searchText;
		scope.result = scope.blockData && scope.blockData.result;

		$rootScope.$on('RbsElasticsearchSearchText', function(event, args) {
			var data = scope.blockData && scope.blockData.contextData || {};
			data.searchText = args.searchText;
			AjaxAPI.getData('Rbs/Elasticsearch/Header', data, scope.blockData && scope.blockData.contextParams)
				.then(function(res) {
					if (res.data && res.data.length) {
						scope.searchText = args.searchText;
						scope.result = res.data;
					}
				});
		});

		scope.$watchCollection('result', function(result) {
			scope.totalCount = 0;
			if (result && result.length) {
				var currentFunction = scope.blockParameters && scope.blockParameters.resultFunction;
				angular.forEach(result, function(r) {
					scope.totalCount += r.count;
					r.active = currentFunction == r.resultFunction;
				})
			}
		})
	}]);

	app.directive('rbsElasticsearchResult', ['$compile', function($compile) {
		return {
			restrict: 'A',
			scope: false,
			controller: ['$scope', '$element', '$rootScope', 'RbsChange.AjaxAPI', function(scope, $element, $rootScope, AjaxAPI) {
				scope.contextData = scope.blockData && scope.blockData.contextData || {};
				scope.items = scope.blockData && scope.blockData.items;
				scope.pagination = scope.blockData && scope.blockData.pagination ||
					{ offset: 0, count: 0, limit: scope.blockParameters.itemsPerPage };

				scope.updateOffset = function(offset) {
					var data = scope.contextData;
					if (data.offset != offset) {
						data.offset = offset;
						AjaxAPI.getData('Rbs/Elasticsearch/Search', scope.contextData, scope.blockData && scope.blockData.contextParams)
							.then(function(res) {
								if (res.data && res.data.pagination && res.data.items) {
									scope.items = res.data.items;
									scope.pagination = res.data.pagination;
								}
							});
					}
				};


				$rootScope.$on('RbsElasticsearchSearchText', function(event, args) {
					var data = scope.contextData;
					args.reload = false;
					if (data.searchText != args.searchText) {
						data.searchText = args.searchText;
						data.offset = 0;
						AjaxAPI.getData('Rbs/Elasticsearch/Search', data, scope.blockData && scope.blockData.contextParams)
							.then(function(res) {
								if (res.data && res.data.pagination && res.data.items) {
									scope.items = res.data.items;
									scope.pagination = res.data.pagination;
								}
							});
					}
				});

				$rootScope.$on('RbsElasticsearchFacetFilters', function(event, args) {
					var data = scope.contextData;
					args.reload = false;
					data.facetFilters = args.facetFilters;
					data.offset = 0;
					AjaxAPI.getData('Rbs/Elasticsearch/Search', data, scope.blockData && scope.blockData.contextParams)
						.then(function(res) {
							if (res.data && res.data.pagination && res.data.items) {
								scope.items = res.data.items;
								scope.pagination = res.data.pagination;
							}
						});
				});

				this.getBlockParameters = function() {
					return scope.blockParameters;
				};

				this.getBlockData = function() {
					return scope.blockData;
				};

				this.refresh = function() {
					var data = scope.contextData;
					AjaxAPI.getData('Rbs/Elasticsearch/Search', data, scope.blockData && scope.blockData.contextParams)
						.then(function(res) {
							if (res.data && res.data.pagination && res.data.items) {
								scope.items = res.data.items;
								scope.pagination = res.data.pagination;
							}
						});
				}
			}],
			link: function(scope, elem, attrs, controller) {
				var itemsScope = null;
				var itemsContainer = elem.find('[data-role="items-container"]');

				function directiveName(item) {
					return (item.document && item.document.searchResult && item.document.searchResult.directiveName) ||
						'data-rbs-elasticsearch-result-item'
				}

				scope.$watchCollection('items', function(items) {
					var html = [];
					if (items && items.length) {
						angular.forEach(scope.items, function(item, idx) {
							html.push('<div data-item="items[' + idx + ']" ' + directiveName(item) + '=""></div>');
						});
					}
					if (itemsScope) {
						itemsScope.$destroy();
						itemsScope = null;
					}
					itemsContainer.children().remove();
					if (html.length) {
						itemsScope = scope.$new();
						$compile(html.join(''))(itemsScope, function(clone) {
							itemsContainer.append(clone);
						});
					}
				})
			}
		}
	}]);



	//data-rbs-elasticsearch-result-item
	app.directive('rbsElasticsearchResultItem', function() {
		return {
			restrict: 'A',
			scope: { item: "=" },
			templateUrl: '/rbs-elasticsearch-result-item.twig',
			require: '^rbsElasticsearchResult',
			link: function(scope, elem, attrs, controller) {

			}
		}
	});
})();