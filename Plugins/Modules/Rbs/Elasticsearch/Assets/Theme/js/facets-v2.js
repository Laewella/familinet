(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.directive('rbsElasticsearchFacetContainerV2', ['$location', 'RbsChange.AjaxAPI', 'RbsChange.ModalStack',
		rbsElasticsearchFacetContainerV2]);

	function rbsElasticsearchFacetContainerV2($location, AjaxAPI, ModalStack) {
		return {
			restrict: 'A',
			scope: true,
			controllerAs: 'controller',
			controller: ['$scope', '$element', '$rootScope', function(scope, $element, $rootScope) {
				scope.parameters = scope.blockParameters;
				var data = scope.blockData;

				scope.facets = data && data.facetValues || null;
				if (scope.facets) {
					setParentProperty(scope.facets);
					var facetFilters = buildFacetFilters(scope.facets);
					scope.appliedCount = facetFilters ? Object.keys(facetFilters).length : 0;
				}
				else {
					scope.appliedCount = 0;
				}

				scope.viewButton = function() {
					var view = false;
					angular.forEach(scope.facets, function(facet) {
						if (!view && facet.values && facet.values.length) {
							view = true;
						}
					});
					return view;
				};

				scope.reset = function() {
					unSelectDescendant(scope.facets);
					refresh();
				};

				scope.responsiveModal = function() {
					var options = {
						templateUrl: '/rbs-elasticsearch-facet-responsive-modal.twig',
						backdropClass: 'modal-backdrop-rbs-elasticsearch-facet-responsive',
						windowClass: 'modal-responsive-summary modal-rbs-elasticsearch-facet-responsive',
						scope: scope
					};
					ModalStack.open(options);
				};

				if (scope.blockParameters.resultFunction) {
					$rootScope.$on('RbsElasticsearchSearchText', function(event, args) {
						var data = scope.blockData && scope.blockData.contextData;
						if (data) {
							data.searchText = args.searchText;
							refresh();
						}
					});
				}

				function refresh() {
					if (scope.facets && scope.facets.length) {
						var facetFilters = buildFacetFilters(scope.facets);
						scope.appliedCount = facetFilters ? Object.keys(facetFilters).length : 0;

						// Share facetFilters in a global var for the result block.
						var RBS_Elasticsearch = AjaxAPI.globalVar('RBS_Elasticsearch') || {};
						RBS_Elasticsearch.facetFilters = facetFilters;
						RBS_Elasticsearch.reload = true;
						AjaxAPI.globalVar('RBS_Elasticsearch', RBS_Elasticsearch);
						scope.$emit('RbsElasticsearchFacetFilters', RBS_Elasticsearch);

						var actionPath = scope.parameters.ajaxPath || 'Rbs/Elasticsearch/Facets';
						var data = scope.blockData && scope.blockData.contextData || {};
						data.facetFilters = facetFilters;

						var params = scope.blockData && scope.blockData.contextParams || {};

						AjaxAPI.getData(actionPath, data, params).then(
							function(result) {
								scope.facets = result.data.facetValues || null;
								setParentProperty(scope.facets);
								if (result.data.location) {
									$location.url(result.data.location);
									scope.$emit('RbsSeoUpdateMetas', result.data.metas);
									scope.$emit('RbsSeoUpdateDecorator', { decoratorId: result.data.decoratorId });
								} else if (result.data.hasOwnProperty('search')) {
									$location.search(result.data.search);
								}
							},
							function(result) {
								console.log('error', result);
							}
						);
					}
				}

				function setParentProperty(facets, parentFieldName) {
					if (facets) {
						angular.forEach(facets, function(facet) {
							if (parentFieldName) {
								facet.parent = parentFieldName;
							}
							if (facet.hasChildren && facet.values && facet.values.length) {
								angular.forEach(facet.values, function(value) {
									setParentProperty(value.aggregationValues, facet.fieldName)
								})
							}
						})
					}
				}

				this.refresh = refresh;

				this.getFacets = function() {
					return scope.facets;
				};

				function selectAncestors(facet) {
					var parent = getFacetByFieldName(facet.parent);
					for (var z = 0; z < parent.values.length; z++) {
						var value = parent.values[z];
						for (var i = 0; i < value['aggregationValues'].length; i++) {
							if (value['aggregationValues'][i] === facet) {
								value.selected = true;
								if (angular.isFunction(parent.selectionChange)) {
									parent.selectionChange(value, true);
								}
								return;
							}
						}
					}
				}

				this.selectAncestors = selectAncestors;

				function unSelectDescendant(facets) {
					for (var i = 0; i < facets.length; i++) {
						var facet = facets[i];
						if (angular.isFunction(facet.reset)) {
							facet.reset();
						}
						if (facet.values) {
							for (var z = 0; z < facet.values.length; z++) {
								var value = facet.values[z];
								value.selected = false;
								if (value['aggregationValues']) {
									unSelectDescendant(value['aggregationValues']);
								}
							}
						}
					}
				}

				this.unSelectDescendant = unSelectDescendant;

				function getFacetByFieldName(fieldName, facets) {
					if (facets === undefined) {
						facets = scope.facets;
					}
					for (var i = 0; i < facets.length; i++) {
						var facet = facets[i];
						if (facet.fieldName === fieldName) {
							return facet;
						}
						if (facet.values) {
							for (var z = 0; z < facet.values.length; z++) {
								var value = facet.values[z];
								if (value.aggregationValues) {
									var subFacet = getFacetByFieldName(fieldName, value.aggregationValues);
									if (subFacet) {
										return subFacet;
									}
								}
							}
						}

					}
					return null;
				}

				function buildFacetFilters(facets) {
					var facetFilters = null;
					for (var i = 0; i < facets.length; i++) {
						var facet = facets[i];
						if (angular.isFunction(facet.buildFilter)) {
							var filters = facet.buildFilter();
							if (filters) {
								if (!facetFilters) {
									facetFilters = {};
								}
								facetFilters[facet.fieldName] = filters;
							}
						}
						else if (facet.values) {
							for (var z = 0; z < facet.values.length; z++) {
								var value = facet.values[z];
								if (value.selected) {
									var key = '' + value.key;
									if (!facetFilters) {
										facetFilters = {};
									}
									if (!facetFilters[facet.fieldName]) {
										facetFilters[facet.fieldName] = {};
									}
									facetFilters[facet.fieldName][key] = 1;
									var filter = null;
									if (value.aggregationValues && value.aggregationValues.length) {
										filter = buildFacetFilters(value.aggregationValues);
										if (filter) {
											facetFilters[facet.fieldName][key] = filter;
										}
									}
								}
							}
						}
					}
					return facetFilters;
				}
			}]
		}
	}

	app.directive('rbsElasticsearchFacetV2', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-v2.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper, and return the linking function(s) which it returns.
				return RecursionHelper.compile(element, function(scope, element, attrs) {
					scope.isCollapsed = false;
					if (attrs['collapseNotSelected'] === 'true') {
						scope.isCollapsed = true;
						for (var i = 0; i < scope.facet.values.length; i++) {
							if (scope.facet.values[i].selected) {
								scope.isCollapsed = false;
								break;
							}
						}
					}

					var directiveName = scope.facet.parameters.renderingMode;
					if (!directiveName) {
						console.error('No rendering mode for facet!', scope.facet);
					}
					else if (directiveName.indexOf('-') < 0) {
						directiveName = 'rbs-elasticsearch-facet-' + directiveName;
					}

					var container = element.find('.facet-values-container');
					container.html('<div data-' + directiveName + '></div>');
					RecursionHelper.baseCompile(container.contents())(scope);
				});
			}
		};
	}]);
})();