(function(noUiSlider) {
	'use strict';

	var app = angular.module('RbsChangeApp');

	function linkFacetRadio(scope) {
		scope.selectionChange = function(value, ignoreRefresh) {
			for (var z = 0; z < scope.facet.values.length; z++) {
				if (value !== scope.facet.values[z]) {
					scope.facet.values[z].selected = false;
				}
			}
			if (!value.selected && value['aggregationValues']) {
				scope.controller.unSelectDescendant(value['aggregationValues']);
			}
			else if (value.selected && scope.facet.parent) {
				scope.controller.selectAncestors(scope.facet);
			}

			if (ignoreRefresh !== true) {
				scope.controller.refresh();
			}
		};

		scope.hasValue = function() {
			return scope.facet.values && scope.facet.values.length;
		}
	}

	app.directive('rbsElasticsearchFacetRadio', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-radio.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper,
				// And return the linking function(s) which it returns
				return RecursionHelper.compile(element, linkFacetRadio);
			}
		};
	}]);

	app.directive('rbsElasticsearchFacetButtonssingle', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-buttons-single.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper,
				// And return the linking function(s) which it returns
				return RecursionHelper.compile(element, linkFacetRadio);
			}
		};
	}]);

	app.directive('rbsElasticsearchFacetSwatchsingle', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-swatch-single.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper,
				// And return the linking function(s) which it returns
				return RecursionHelper.compile(element, linkFacetRadio);
			}
		};
	}]);

	function linkFacetCheckbox(scope) {
		scope.selectionChange = function (value, ignoreRefresh) {
			if (!value.selected && value['aggregationValues']) {
				scope.controller.unSelectDescendant(value['aggregationValues']);
			}
			else if (value.selected && scope.facet.parent) {
				scope.controller.selectAncestors(scope.facet);
			}

			if (ignoreRefresh !== true) {
				scope.controller.refresh();
			}
		};

		scope.hasValue = function() {
			return scope.facet.values && scope.facet.values.length;
		}
	}

	app.directive('rbsElasticsearchFacetCheckbox', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-checkbox.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper,
				// And return the linking function(s) which it returns
				return RecursionHelper.compile(element, linkFacetCheckbox);
			}
		};
	}]);

	app.directive('rbsElasticsearchFacetButtonsmultiple', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-buttons-multiple.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper,
				// And return the linking function(s) which it returns
				return RecursionHelper.compile(element, linkFacetCheckbox);
			}
		};
	}]);

	app.directive('rbsElasticsearchFacetSwatchmultiple', ['RbsChange.RecursionHelper', function(RecursionHelper) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-swatch-multiple.twig',
			scope: false,
			compile: function(element) {
				// Use the compile function from the RecursionHelper,
				// And return the linking function(s) which it returns
				return RecursionHelper.compile(element, linkFacetCheckbox);
			}
		};
	}]);

	app.directive('rbsElasticsearchFacetSwatchItem', function() {
		return {
			restrict: 'A',
			link: function (scope, element, attributes) {
				if (!angular.isObject(scope.value.skin)) {
					return;
				}

				var visualFormat = attributes['visualFormat'] || 'selectorItem';
				var background = scope.value.skin.colorCode || ''; // Concatenate undefined with a string would result in "undefined...".
				if (scope.value.skin.visual) {
					background += ' url("' + scope.value.skin.visual[visualFormat] + '") no-repeat center center';
				}
				scope.ngStyle = { background: background };
			}
		};
	});

	app.directive('rbsElasticsearchFacetInterval', ['$filter', function ($filter) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-facet-interval.twig',
			scope: false,
			link: function(scope, element) {
				scope.selectionChange = function (value, ignoreRefresh) {
					if (value.selected && scope.facet.parent) {
						scope.controller.selectAncestors(scope.facet);
					}

					if (ignoreRefresh !== true) {
						scope.controller.refresh();
					}
				};

				scope.hasValue = function() {
					return scope.facet.values && scope.facet.values.length;
				};

				var lastMin, lastMax, options, slider;

				scope.range = {
					interval: scope.facet.parameters.interval,
					absMin: null,
					absMax: null,
					min: null,
					max: null
				};

				refreshRange();

				options = {
					start: [scope.range.min, scope.range.max],
					behaviour: 'drag-tap',
					connect: true,
					margin: scope.range.interval,
					step: scope.range.interval,
					range: {
						'min': [scope.range.absMin ? scope.range.absMin : 0],
						'max': [scope.range.absMax ? scope.range.absMax : scope.range.interval]
					}
				};

				if (scope.facet.parameters['sliderShowTooltips'] !== false) {
					scope.showTooltips = true;
					var currencyCode = scope.blockData.contextData.currencyCode;
					var formatter = {
						to: function(value) {
							return '<div class="tooltip-arrow"></div>' +
								'<div class="tooltip-inner">' +
								'<span>' + $filter('rbsFormatPrice')(value, currencyCode, 0) + '</span>' +
								'</div>';
						}
					};
					options.tooltips = [formatter, formatter];
				}

				if (scope.facet.parameters['sliderShowLabels'] == true) {
					scope.showLabels = true;
				}

				if (scope.facet.parameters['sliderShowPips'] == true) {
					scope.showPips = true;

					options.pips = {
						mode: 'steps',
						values: countValues(),
						density: scope.range.interval
					};
				}

				createSlider();

				scope.$watch('facet', onFacetChanged);
				scope.$watch('facet.values', onFacetValuesChanged);

				function createSlider() {
					var classes = 'range-slider';
					if (scope.showTooltips) {
						classes += ' range-slider-tooltips';
					}
					if (scope.showPips) {
						classes += ' range-slider-pips';
					}

					element.find('.range-slider').replaceWith('<div class="' + classes + '"></div>');
					slider = element.find('.range-slider').get(0);
					noUiSlider.create(slider, options);

					slider.noUiSlider.on('slide', synchronizeValues);
					slider.noUiSlider.on('change', synchronizeValuesAndRefresh);

					// Replace the 'noUi-tooltip' class by the Bootstrap's one.
					element.find('.noUi-tooltip').addClass('tooltip top').removeClass('noUi-tooltip');
				}

				function synchronizeValues() {
					var values = slider.noUiSlider.get();
					var newMin = parseInt(values[0]);
					var newMax = parseInt(values[1]);
					if (scope.range.min != newMin || scope.range.max != newMax) {
						scope.range.min = newMin;
						scope.range.max = newMax;
						scope.$digest();
					}
				}

				function synchronizeValuesAndRefresh() {
					var values = slider.noUiSlider.get();
					var newMin = parseInt(values[0]);
					var newMax = parseInt(values[1]);
					if (lastMin != newMin || lastMax != newMax) {
						scope.range.min = lastMin = newMin;
						scope.range.max = lastMax = newMax;
						scope.$digest();
						for (var i = 0; i < scope.facet.values.length; i++) {
							var key = scope.facet.values[i].key;
							scope.facet.values[i].selected = (key >= scope.range.min && key < scope.range.max);
						}
						scope.facet.customValues = [
							{
								key: 'min',
								selected: true,
								value: scope.range.min
							},
							{
								key: 'max',
								selected: true,
								value: scope.range.max
							}
						];
						scope.controller.refresh();
					}
				}

				function onFacetChanged() {
					if (angular.isFunction(scope.facet.buildFilter)) {
						return;
					}

					scope.facet.buildFilter = function() {
						if (scope.range.min !== scope.range.absMin || scope.range.max !== scope.range.absMax) {
							return { min: scope.range.min, max: scope.range.max };
						}
						return null;
					};

					scope.facet.reset = function() {
						var range = scope.range;
						range.min = range.absMin;
						range.max = range.absMax;
					};
				}


				function onFacetValuesChanged() {
					var updateRange = refreshRange();
					if (updateRange) {
						options.start = [scope.range.min, scope.range.max];
						options.range = {
							'min': [scope.range.absMin ? scope.range.absMin : 0],
							'max': [scope.range.absMax ? scope.range.absMax : scope.range.interval]
						};
						if (scope.showPips) {
							options.pips.values = countValues();
						}
						createSlider();
					}
				}

				function refreshRange() {
					if (!scope.hasValue()) {
						return false;
					}

					var rangeUpdated = false;
					for (var i = 0; i < scope.facet.values.length; i++) {
						var value = scope.facet.values[i].key;
						var upValue = value + scope.range.interval;
						if (scope.range.absMin === null || value < scope.range.absMin) {
							scope.range.absMin = value;
							rangeUpdated = true;
						}
						if (scope.range.absMax === null || upValue > scope.range.absMax) {
							scope.range.absMax = upValue;
							rangeUpdated = true;
						}
						if (scope.facet.values[i].selected) {
							if (scope.range.min === null || value < scope.range.min) {
								scope.range.min = value;
								rangeUpdated = true;
							}
							if (scope.range.max === null || upValue > scope.range.max) {
								scope.range.max = upValue;
								rangeUpdated = true;
							}
						}
					}

					if (scope.range.min === null) {
						if (scope.facet.parameters['minFilter'] !== undefined && scope.facet.parameters['minFilter'] !== null) {
							scope.range.min = parseInt(scope.facet.parameters['minFilter']);
						}
						else {
							scope.range.min = scope.range.absMin;
						}
					}

					if (scope.range.max === null) {
						if (scope.facet.parameters['maxFilter'] !== undefined && scope.facet.parameters['maxFilter'] !== null) {
							scope.range.max = parseInt(scope.facet.parameters['maxFilter']);
						}
						else {
							scope.range.max = scope.range.absMax;
						}
					}

					lastMin = scope.range.min;
					lastMax = scope.range.max;

					return rangeUpdated;
				}

				function countValues() {
					return scope.facet.values ? scope.facet.values.length : 0
				}
			}
		}
	}]);
})(window.noUiSlider);
