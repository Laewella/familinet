(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.directive('rbsElasticsearchInstantSearch',
		['RbsChange.AjaxAPI', '$location', rbsElasticsearchInstantSearch]);
	function rbsElasticsearchInstantSearch(AjaxAPI, $location) {
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {
				var data = scope.blockData;
				scope.minLength = attrs.minLength;

				var search = $location.search();
				scope.instantSearchText = search.searchText || null;

				scope.$on('$locationChangeSuccess', function() {
					if ($location.search().searchText != scope.instantSearchText) {
						scope.instantSearchText = $location.search().searchText;
					}
				});

				scope.instantSearch = function instantSearch() {
					if (scope.instantSearchText && scope.instantSearchText.length && (scope.minLength === 0 ||
						scope.instantSearchText.length >= scope.minLength)) {

						var RBS_Elasticsearch = AjaxAPI.globalVar('RBS_Elasticsearch') || {};
						RBS_Elasticsearch.searchText = scope.instantSearchText;
						RBS_Elasticsearch.instantSearch = true;
						RBS_Elasticsearch.reload = false;
						AjaxAPI.globalVar('RBS_Elasticsearch', RBS_Elasticsearch);
						scope.$emit('RbsElasticsearchSearchText', RBS_Elasticsearch);

						$location.search('searchText', scope.instantSearchText);
					}
				};
			}
		};
	}
})();