(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	//data-rbs-elasticsearch-short-search
	app.directive('rbsElasticsearchShortSearch',
		['RbsChange.AjaxAPI', '$timeout', 'RbsChange.ResponsiveSummaries','$sce', '$location', rbsElasticsearchShortSearch]);
	function rbsElasticsearchShortSearch(AjaxAPI, $timeout, ResponsiveSummaries, $sce, $location) {
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {
				var data = scope.blockData;
				scope.waitMs = attrs.waitMs;
				scope.minLength = attrs.minLength;
				scope.timeoutPromise = null;

				var search = $location.search();
				scope.searchText = search.searchText || null;
				var searching = false;

				var resetMatches = function() {
					scope.categories = null;
					jQuery('#short-search-listbox').hide();
				};

				var getMatches = function(searchText) {
					resetMatches();
					if(searchText.length >= scope.minLength) {
						return AjaxAPI.getData('Rbs/Elasticsearch/Suggest', { searchText: searchText, nbElement: scope.blockParameters.nbElement }, { visualFormats: scope.blockParameters.thumbnailFormat })
							.then(function(res) {
								if (searching) {
									searching = false;
								}
								else if (res.data.categories && res.data.categories.length > 0) {
									jQuery('#short-search-listbox').show();
									scope.categories = res.data.categories;
								}
							});
					}
				};

				var scheduleSearchWithTimeout = function(inputValue) {
					scope.timeoutPromise = $timeout(function() {
						getMatches(inputValue);
					}, scope.waitMs);
				};

				var cancelPreviousTimeout = function() {
					if (scope.timeoutPromise) {
						$timeout.cancel(scope.timeoutPromise);
						scope.timeoutPromise = null;
					}
				};

				scope.loadSuggestions = function loadSuggestions(searchText) {
					if (scope.minLength === 0 || searchText && searchText.length >= scope.minLength) {
						if (scope.waitMs > 0) {
							cancelPreviousTimeout();
							scheduleSearchWithTimeout(searchText);
						}
						else {
							getMatches(searchText);
						}
					}
					else {
						cancelPreviousTimeout();
						resetMatches();
					}
				};

				scope.highlightSuggestion = function highlightSuggestion(item, searchText) {
					if (searchText) {
						var pattern = new RegExp(searchText, "i");
						var exactToken = item.match(pattern);
						return $sce.trustAsHtml(item.replace(pattern, '<strong>' + exactToken + '</strong>'));
					}
					return item;
				};

				scope.searchItem = function submitForm($item, $type, $url) {
					scope.searchText = $item;
					if (scope.searchText && scope.searchText.length) {
						cancelPreviousTimeout();
						resetMatches();
						searching = true;

						// Share facetFilters in a global var for the result block.
						var RBS_Elasticsearch = AjaxAPI.globalVar('RBS_Elasticsearch') || {};
						RBS_Elasticsearch.searchText = scope.searchText;
						RBS_Elasticsearch.reload = true;
						RBS_Elasticsearch.instantSearch = false;
						RBS_Elasticsearch.resultFunction = $type || false;
						AjaxAPI.globalVar('RBS_Elasticsearch', RBS_Elasticsearch);
						scope.$emit('RbsElasticsearchSearchText', RBS_Elasticsearch);
						if (RBS_Elasticsearch.reload) {
							window.location.href = $url + '?searchText=' + encodeURIComponent(scope.searchText) ;
						}
						else {
							$location.search('searchText', scope.searchText);
						}
					}
				};

				scope.submitForm = function submitForm() {
					if (scope.searchText && scope.searchText.length) {
						cancelPreviousTimeout();
						resetMatches();
						searching = true;

						var RBS_Elasticsearch = AjaxAPI.globalVar('RBS_Elasticsearch') || {};
						RBS_Elasticsearch.searchText = scope.searchText;
						RBS_Elasticsearch.reload = true;
						RBS_Elasticsearch.instantSearch = false;
						RBS_Elasticsearch.resultFunction = false;
						AjaxAPI.globalVar('RBS_Elasticsearch', RBS_Elasticsearch);
						scope.$emit('RbsElasticsearchSearchText', RBS_Elasticsearch);
						if (RBS_Elasticsearch.reload) {
							AjaxAPI.getData('Rbs/Elasticsearch/Header', { searchText: scope.searchText })
								.then(function(res) {
									if (res.data && res.data.length) {
										var url= null, count = -1;
										angular.forEach(res.data, function(d) {
											if (!url && count < d.count) {
												count = d.count;
												RBS_Elasticsearch.resultFunction = d.resultFunction;
												url = d.url
											}
										});
										if (url) {
											window.location.href = url;
										}
									}
								});
						}
						else {
							$location.search('searchText', scope.searchText);
						}
					}
				};

				if (!scope.inResponsiveSummary) {
					ResponsiveSummaries.registerItem(scope.blockId, scope, '<li data-rbs-elasticsearch-short-search-responsive-summary=""></li>');
				}
			}
		};
	}

	app.directive('rbsElasticsearchShortSearchResponsiveSummary', ['RbsChange.ModalStack',
		rbsElasticsearchShortSearchResponsiveSummary]);
	function rbsElasticsearchShortSearchResponsiveSummary(ModalStack) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-elasticsearch-short-search-responsive-summary.twig',
			link: function(scope) {
				scope.inResponsiveSummary = true;
				scope.onResponsiveSummaryClick = function() {
					var options = {
						templateUrl: '/rbs-elasticsearch-short-search-responsive-summary-modal.twig',
						backdropClass: 'modal-backdrop-rbs-elasticsearch-short-search-responsive-summary',
						windowClass: 'modal-responsive-summary modal-rbs-elasticsearch-short-search-responsive-summary',
						scope: scope
					};
					ModalStack.open(options);
				}
			}
		};
	}
})();