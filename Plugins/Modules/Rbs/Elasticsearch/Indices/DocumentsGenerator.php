<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Indices;

/**
 * @name \Rbs\Elasticsearch\Indices\DocumentsGenerator
 */
class DocumentsGenerator
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager)
	{
		$this->documentManager = $documentManager;
	}

	public function __invoke()
	{
		$documentManager = $this->documentManager;
		$modelManager = $documentManager->getModelManager();
		$documentManager->usePersistentCache(true);
		$filters = ['abstract' => false, 'stateless' => false, 'inline' => false, 'onlyInstalled' => true, 'publishable' => true];
		foreach ($modelManager->getFilteredModelsNames($filters) as $modelName)
		{
			$model = $modelManager->getModelByName($modelName);
			$id = 0;
			while (true)
			{
				$documentManager->reset();
				$q = $documentManager->getNewQuery($model);
				$q->andPredicates($q->gt('id', $id));
				$q->addOrder('id');
				$docs = $q->getDocuments(0, 50)->preLoad()->toArray();
				if (!count($docs))
				{
					break;
				}

				/** @var \Change\Documents\AbstractDocument $doc */
				foreach ($docs as $doc)
				{
					if ($doc instanceof \Change\Documents\AbstractDocument)
					{
						$id = $doc->getId();
						yield $doc;
					}
				}
			}
		}
	}
}