<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Indices;

/**
 * @name \Rbs\Elasticsearch\Indices\DocumentIndexing
 */
class DocumentIndexing
{
	/**
	 * @var array
	 */
	protected $ids;

	/**
	 * @param \Change\Events\Event $event
	 */
	public function start(\Change\Events\Event $event)
	{
		if ($event->getParam('primary'))
		{
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$manager = $genericServices->getElasticsearchManager();
			if ($manager->getIndicesName())
			{
				$this->ids = [];
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function addDocument(\Change\Documents\Events\Event $event)
	{
		if ($this->ids !== null)
		{
			$document = $event->getDocument();
			if ($event->getName() !== \Change\Documents\Events\Event::EVENT_UPDATED
				|| $event->getParam('forceIndex') || $event->getParam('modifiedPropertyNames'))
			{
				$this->ids[$document->getId()] = $document->getDocumentModelName();
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function end(\Change\Events\Event $event)
	{
		if ($this->ids && $event->getParam('primary'))
		{
			$jobManager = $event->getApplicationServices()->getJobManager();
			$jobManager->createNewJob('Rbs_Elasticsearch_Index', ['ids' => $this->ids], null, false);
			$this->ids = null;
		}
	}

	public function index(\Change\Job\Event $event)
	{
		/** @var array $ids */
		$ids = $event->getJob()->getArgument('ids');
		if ($ids && is_array($ids))
		{
			$applicationServices = $event->getApplicationServices();
			$dbProvider = $applicationServices->getDbProvider();
			$dbProvider->setReadOnly(true);

			$documentManager = $applicationServices->getDocumentManager();
			$documentManager->usePersistentCache(true);
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$manager = $genericServices->getElasticsearchManager();
			$index = $manager->getIndex(\Rbs\Elasticsearch\Manager::DOCUMENTS_INDEX);
			if (!$index || !$index->exists())
			{
				$event->failed('Index: ' . \Rbs\Elasticsearch\Manager::DOCUMENTS_INDEX . ' not found');
			}
			elseif ($index->getSettings()->get('blocks.read_only') === 'true')
			{
				$reportedCount = 1+ ($event->getJob()->getArgument('reportedCount') ?? 0);
				$dateTime = new \DateTime();
				$dateTime->add(new \DateInterval('PT10M'));
				$event->setResultArgument('reportedCount', $reportedCount);
				$event->reported($dateTime);
			}
			else
			{
				$manager->updateDocumentsIndex($index, $ids);
				$event->setResultArgument('indexed', true);
				$event->success();
			}

			$dbProvider->setReadOnly(false);
		}
	}
}