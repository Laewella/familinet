<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Indices;

/**
 * @name \Rbs\Elasticsearch\Indices\DocumentIds
 */
class DocumentIds
{

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var integer[]
	 */
	protected $ids;

	/**
	 * @var integer[]
	 */
	protected $consumed = [];

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \integer[] $ids
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, array $ids)
	{
		$this->documentManager = $documentManager;
		$this->addIds($ids);
	}

	/**
	 * @return \Generator
	 */
	public function generator()
	{
		$idx = 0;
		$oldValue = $this->documentManager->usePersistentCache(true);
		while ($this->ids)
		{
			$ids = $this->ids;
			$this->ids = [];
			foreach ($ids as $id => $model)
			{
				if (!isset($this->consumed[$id]))
				{
					$this->consumed[$id] = true;
					$idx++;
					if (!($idx % 100))
					{
						$this->documentManager->reset();
					}
					yield $id => $model;
				}
			}
		}
		$this->documentManager->usePersistentCache($oldValue);
	}

	/**
	 * @api
	 * @param int $id
	 * @param string $model
	 */
	public function addId(int $id, string $model)
	{
		if ($model && $id > 0)
		{
			$this->ids[$id] = $model;
		}
	}

	/**
	 * @api
	 * @param array $ids
	 */
	public function addIds(array $ids)
	{
		foreach ($ids as $id => $model)
		{
			$this->addId($id, $model);
		}
	}
}