<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Indices;

/**
 * @name \Rbs\Elasticsearch\Indices\DocumentsData
 */
class DocumentsData
{

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Presentation\RichText\RichTextManager
	 */
	protected $richTextManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, \Change\Presentation\RichText\RichTextManager $richTextManager)
	{
		$this->documentManager = $documentManager;
		$this->richTextManager = $richTextManager;
	}

	/**
	 * @param \Change\Documents\Interfaces\Publishable|\Change\Documents\AbstractDocument $document
	 * @param array $documentData
	 * @return array
	 */
	public function addPublishableMetas($document, array $documentData)
	{
		$documentData['_id'] = (string)$document->getId();
		$documentData['_type'] = 'document';
		$documentData['resultFunction'] = 'Rbs_Elasticsearch_Result';
		$model = $document->getDocumentModel();
		$documentData['model'] = $model->getName();
		$documentData['documentId'] = $document->getId();
		$website = null;
		$dependencies = [];
		foreach ($document->getPublicationSections() as $section)
		{
			$sectionWebsite = $section->getWebsite();
			if (!$sectionWebsite)
			{
				continue;
			}
			if (!$website)
			{
				$website = $sectionWebsite;
			}
			$documentData['sections'][] = [
				'websiteId' => $sectionWebsite->getId(),
				'sectionId' => $section->getId(),
			];
		}
		$documentData['dependencies'] = array_merge($documentData['dependencies'] ?? [], array_keys($dependencies));

		if ($document instanceof \Change\Documents\Interfaces\Localizable)
		{
			foreach ($document->getLCIDArray() as $LCID)
			{
				$documentData['LCID'][] = $LCID;
				try
				{
					$this->documentManager->pushLCID($LCID);
					$localizedDocument = $document->getCurrentLocalization();
					$LCIDData = [];
					$p = $model->getProperty('title');
					$LCIDData['title'] = $p ? $p->getLocalizedValue($localizedDocument) : null;

					$p = $model->getProperty('publicationStatus');
					$publishable =
						$p ? \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE == $p->getLocalizedValue($localizedDocument) : false;
					$LCIDData['publishable'] = $publishable;
					if ($LCIDData['publishable'])
					{
						$p = $model->getProperty('startPublication');
						$startPublication = $p ? $p->getLocalizedValue($localizedDocument) : null;
						if (!($startPublication instanceof \DateTime))
						{
							$startPublication = new \DateTime(\Change\Documents\Property::DEFAULT_START_DATE);
						}
						$LCIDData['startPublication'] = $startPublication->format(\DateTime::ATOM);

						$p = $model->getProperty('endPublication');
						$endPublication = $p ? $p->getLocalizedValue($localizedDocument) : null;
						if (!($endPublication instanceof \DateTime))
						{
							$endPublication = new \DateTime(\Change\Documents\Property::DEFAULT_END_DATE);
						}
						$LCIDData['endPublication'] = $endPublication->format(\DateTime::ATOM);
					}

					foreach ($model->getProperties() as $property)
					{
						if ($property->getType() === \Change\Documents\Property::TYPE_RICHTEXT && $property->getLocalized())
						{
							$value = $property->getLocalizedValue($localizedDocument);
							if ($value instanceof \Change\Documents\RichtextProperty)
							{
								$text = $this->richTextManager->render($value, 'Mail', null);
								$text = trim(strip_tags($text, '<p><br>'));
								if ($text)
								{
									$LCIDData['content'][] = $text;
								}
							}
						}
					}

					$documentData[$LCID] = $LCIDData;
					$this->documentManager->popLCID();
				}
				catch (\Exception $e)
				{
					$this->documentManager->popLCID($e);
				}
			}
		}
		return $documentData;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @param array $documentData
	 * @return array
	 */
	public function addTypologyData($document, array $documentData)
	{
		$typology = $this->documentManager->getTypologyByDocument($document);
		if (!$typology)
		{
			return $documentData;
		}
		$documentData['typologyId'] = $typology->getId();

		/** @var \Change\Documents\Attributes\Interfaces\Attribute[] $contentAttributes */
		$contentAttributes = [];
		/** @var \Change\Documents\Attributes\Interfaces\Attribute[] $keywordAttributes */
		$keywordAttributes = [];
		/** @var \Change\Documents\Attributes\Interfaces\Attribute[] $typologyAttributes */
		$typologyAttributes = [];
		foreach ($typology->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				$type = $attribute->getType();
				if ($attribute->getLocalized())
				{
					if ($type == \Change\Documents\Attributes\Interfaces\Attribute::TYPE_RICHTEXT)
					{
						$contentAttributes[] = $attribute;
					}
					elseif ($type == \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING)
					{
						$keywordAttributes[] = $attribute;
					}
				}
				else
				{
					switch ($type)
					{
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
							$typologyAttributes[] = $attribute;
							break;
						case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
							$typologyAttributes[] = $attribute;
							break;
					}
				}
			}
		}
		$idCallable = function ($typologyAttribute)
		{
			return (int)(is_callable([$typologyAttribute, 'getId']) ? $typologyAttribute->{'getId'}() : 0);
		};

		if ($typologyAttributes)
		{
			$values = $this->documentManager->getAttributeValues($document);
			foreach ($typologyAttributes as $typologyAttribute)
			{
				$value = $values->get($typologyAttribute->getName());
				if ($value === null || $value === [])
				{
					continue;
				}
				$id = $idCallable($typologyAttribute);
				$docAttribute = $this->documentManager->getDocumentInstance($id);
				if ($docAttribute instanceof \Rbs\Generic\Documents\Attribute)
				{
					if ($mappingId = $docAttribute->getIndexValuesMappingId())
					{
						$documentData['dependencies'][] = $mappingId;
					}
					$value = $this->getMappedValue($docAttribute, $value);
					if ($value === null)
					{
						continue;
					}
				}

				$fieldSuffix = 'Attribute_' . $id;
				switch ($typologyAttribute->getType())
				{
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
						$documentData['boolean' . $fieldSuffix][] = (bool)$value;
						break;
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
						$documentData['long' . $fieldSuffix][] = (int)$value;
						break;
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
						$documentData['double' . $fieldSuffix][] = (float)$value;
						break;
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
						if (is_array($value))
						{
							/** @var array $value */
							foreach ($value as $vi)
							{
								$documentData['long' . $fieldSuffix][] = (int)$vi;
							}
						}
						break;
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
						if ($value instanceof \DateTime)
						{
							$documentData['date' . $fieldSuffix][] = $value->format(\DateTime::ATOM);
						}
						break;
					case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
						$documentData['string' . $fieldSuffix][] = (string)$value;
						break;
				}
			}
		}
		/** @var array $LCIDArray */
		$LCIDArray = $documentData['LCID'] ?? [];
		if (($contentAttributes || $keywordAttributes) && $LCIDArray)
		{
			foreach ($LCIDArray as $LCID)
			{
				try
				{
					$this->documentManager->pushLCID($LCID);
					$values = $this->documentManager->getAttributeValues($document);
					foreach ($keywordAttributes as $keywordAttribute)
					{
						$value = $values->get($keywordAttribute->getName());
						if ($value)
						{
							$documentData[$LCID]['keywords'][] = $value;
						}
					}

					foreach ($contentAttributes as $contentAttribute)
					{
						$value = $values->get($contentAttribute->getName());
						if ($value instanceof \Change\Documents\RichtextProperty)
						{
							$text = $this->richTextManager->render($value, 'Mail', null);
							$text = trim(strip_tags($text, '<p><br>'));
							if ($text)
							{
								$documentData[$LCID]['content'][] = $text;
							}
						}
					}

					$this->documentManager->popLCID();
				}
				catch (\Exception $e)
				{
					$this->documentManager->popLCID($e);
				}
			}
		}
		return $documentData;
	}

	/**
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param mixed $value
	 * @return int|null|string
	 */
	protected function getMappedValue(\Rbs\Generic\Documents\Attribute $attribute, $value)
	{
		if ($attribute->getIndexed())
		{
			if ($vm = $attribute->getIndexValuesMapping())
			{
				return $vm->getMappedValue($value);
			}
		}
		else
		{
			return null;
		}
		return $value;
	}
}