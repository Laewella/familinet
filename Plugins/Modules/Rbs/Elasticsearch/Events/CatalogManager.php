<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Events;

/**
 * @deprecated since 1.8.0 with no replacement
 * @name \Rbs\Elasticsearch\Events\CatalogManager
 */
class CatalogManager
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 * Input params: context
	 * Output param: productsData, pagination
	 * @param \Change\Events\Event $event
	 */
	public function onGetProductsData(\Change\Events\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}
} 