<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Http\Ajax;

/**
 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Http\Ajax\Search or \Rbs\Catalog\Http\Ajax\Product
 * @name \Rbs\Elasticsearch\Http\Ajax\StoreFacet
 */
class StoreFacet
{
	/**
	 * @deprecated since 1.8.0 use \Rbs\Catalog\Http\Ajax\Product::getSearchFacets
	 *  or \Rbs\Catalog\Http\Ajax\Product::getProductListFacets
	 * @param \Change\Http\Event $event
	 */
	public function getFacetsData(\Change\Http\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}
}