<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Http\Ajax;

/**
 * @name \Rbs\Elasticsearch\Http\Ajax\Search
 */
class Search
{
	/**
	 * Default actionPath: Rbs/Elasticsearch/Suggest
	 * Event params:
	 *  - website, section, page
	 *  - dataSetNames
	 *  - visualFormats
	 *  - URLFormats
	 *  - data:
	 *      searchText
	 *      nbElement
	 * @param \Change\Http\Event $event
	 */
	public function onSuggest(\Change\Http\Event $event)
	{
		$result = ['categories' => []];
		$data = $event->getParam('data');
		$searchText = (string)($data['searchText'] ?? '');
		if ($searchText)
		{
			//Elastic search special chars
			$t = str_replace(['+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':', '\\',
				'/'], ' ', $searchText);

			//Other special chars
			$t = str_replace([',', '.'], ' ', $t);

			$t = array_map('trim', explode(' ', $t));
			$t = array_reduce($t, function ($r, $i)
			{
				if (mb_strlen($i) > 2)
				{
					$r[] = mb_substr($i, 0, 15);
				}
				return $r;
			}, []);
			$terms = array_unique($t);
			$result['terms'] = $terms; //array_map('utf8_encode', $terms);
			if (count($terms))
			{
				$LCID = $event->getRequest()->getLCID();
				/** @var \Rbs\Website\Documents\Website $website */
				$website = $event->getParam('website');
				$result['LCID'] = $LCID;
				$genericServices = $event->getServices('genericServices');

				/* @var $commerceServices \Rbs\Commerce\CommerceServices */
				$commerceServices = $event->getServices('commerceServices');
				$commerceContext = $commerceServices->getContext();
				$webStoreId =$commerceContext->getWebStoreId();
				$billingArea =  $commerceContext->getBillingArea();
				$zone = $commerceContext->getZone();

				$urlManager = $website->getUrlManager($LCID);

				$storageManager = $event->getApplicationServices()->getStorageManager();
				if ($genericServices instanceof \Rbs\Generic\GenericServices && $website instanceof \Rbs\Website\Documents\Website)
				{
					$indexManager = $genericServices->getElasticsearchManager();
					$index = $indexManager->getDocumentsSearchIndex(true);
					if ($index)
					{

						$q = $this->buildSuggestQuery($terms, $LCID, $website->getId(), $data['nbElement'] ?? 10);
						$resultSet = $index->search($q);
						if ($resultSet->count())
						{
							$items = [];
							$resultFunction = [];
							$i = 0;
							$currentDate = (new \DateTime('now', new \DateTimeZone('UTC')))->format('c');

							foreach ($resultSet->getResults() as $r)
							{
								$item = [];
								$source = $r->getSource();
								if ($modelName = $source['model'] ?? null)
								{
									$item['title'] = $source[$LCID]['title'] ?? null;
									if ($source['visualStorageURI'] ?? false)
									{
										$storageURI = new \Zend\Uri\Uri($source['visualStorageURI']);
										$visualFormat = array_values($event->getParam('visualFormats'))[0];
										$storageURI->setQuery(['max-width' => (int)$visualFormat[0], 'max-height' => (int)$visualFormat[1]]);
										$url = $storageURI->normalize()->toString();
										$visualUrl = $storageManager->getPublicURL($url);
										$item['thumbnail'] = $visualUrl ?? null;
									}

									if ($modelName == 'Rbs_Catalog_Product')
									{
										if ($source['prices'] && is_array($source['prices']))
										{
											foreach ($source['prices'] as $price)
											{
												if ($price['billingAreaId'] == $billingArea->getId() && $price['storeId'] == $webStoreId
													&& $price['zone'] == $zone
													&& $currentDate < $price['endActivation'] && $currentDate >= $price['startActivation']
												)
												{
													$item['price']['valueWithTax'] = $price['valueWithTax'];
													$item['price']['valueWithoutTax'] = $price['value'];
													$item['price']['hasDifferentPrices'] = $source['type'] != 'simple';
													$item['price']['currencyCode'] = $billingArea->getCurrencyCode();
												}
											}
										}
									}

									$url = $urlManager->getCanonicalByDocument($source['documentId']);
									$item['url'] = $url->normalize()->toString();

									$fn = $source['resultFunction'] ?? 'Rbs_Elasticsearch_Result';
									$resultFunction[$modelName] = is_array($fn) ? $fn[0] : $fn;
									$items[$modelName][] = $item;
								}
							}

							$uriByFunc = [];
							foreach ($resultFunction as $modelName => $func)
							{
								$uri = $uriByFunc[$func] ?? null;
								if ($uri === null)
								{
									$uri = $urlManager->getByFunction($func, []) ?? false;
									$uriByFunc[$func] = $uri;
								}
								if ($uri)
								{
									$resultFunction[$modelName] = [$func, $uri ? $uri->normalize()->toString() : null];
								}
							}

							$categories = [];
							$modelNames = array_keys($items);
							foreach ($modelNames as $modelName)
							{
								$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
								if (!$model || !isset($resultFunction[$modelName]))
								{
									continue;
								}
								$category = $event->getApplicationServices()->getI18nManager()->trans($model->getTitlePluralKey(), ['ucf']);
								$func = $resultFunction[$modelName];
								$categories[] = ['category' => $category, 'type' => $func[0], 'url' => $func[1], 'items' => $items[$modelName]];
							}
							$result['categories'] = $categories;
						}
					}
				}
			}
		}
		$ajaxResult = new \Change\Http\Web\Result\AjaxResult($result);
		$event->setResult($ajaxResult);
	}

	/**
	 * @param string[] $terms
	 * @param string $LCID
	 * @param string $websiteId
	 * @return \Elastica\Query
	 */
	protected function buildSuggestQuery($terms, $LCID, $websiteId, $nbElement)
	{
		$documentsQuery = new \Rbs\Elasticsearch\Query\Documents();
		$publish = $documentsQuery->getPublished(new \DateTime(), $LCID, $websiteId);
		foreach ($terms as $term)
		{
			$publish->addShould(new \Elastica\Query\Match($LCID . '.title.autocomplete', $term));
		}
		$publish->setMinimumNumberShouldMatch(max(1, count($terms) - 1));
		$query = new \Elastica\Query($publish);
		$query->setFrom(0);
		$query->setSize($nbElement);
		$query->setSource([$LCID . '.title', 'model', 'resultFunction', 'visualStorageURI', 'prices', 'documentId', 'type']);

		return $query;
	}

	/**
	 * Default actionPath: Rbs/Elasticsearch/Header
	 * Event params:
	 *  - website, section, page
	 *  - dataSetNames
	 *  - visualFormats
	 *  - URLFormats
	 *  - data:
	 *      searchText
	 * @param \Change\Http\Event $event
	 */
	public function onResultHeader(\Change\Http\Event $event)
	{
		$result = [];
		$data = $event->getParam('data');
		$result['searchText'] = $searchText = (string)($data['searchText'] ?? '');
		$website = $event->getParam('website');
		$applicationServices = $event->getApplicationServices();

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();
		if ($searchText && $website instanceof \Rbs\Website\Documents\Website
			&& ($index = $elasticsearchManager->getDocumentsSearchIndex(true))
		)
		{
			$i18nManager = $applicationServices->getI18nManager();
			$LCID = $i18nManager->getLCID();
			$block = new \Rbs\Elasticsearch\Blocks\ResultHeader();
			$resultFunctionsData = $block->prepareResultFunctionsData($elasticsearchManager->getDocumentsResultFunctionsData(),
				$data['resultFunctions'] ?? [], $website->getUrlManager($LCID));
			if ($resultFunctionsData)
			{
				$resultFunctions = [];
				foreach ($resultFunctionsData as $data)
				{
					$resultFunctions[] = $data['name'];
				}
				$resultData = $block->buildData($index, $resultFunctions, $searchText, $LCID, $website);
				if ($resultData)
				{
					$result = $block->formatData($resultData, $resultFunctionsData, $searchText, $i18nManager);
				}
			}
		}
		$ajaxResult = new \Change\Http\Web\Result\AjaxResult($result);
		$event->setResult($ajaxResult);
	}

	/**
	 * Default actionPath: Rbs/Elasticsearch/Search
	 * Event params:
	 *  - website, section, page
	 *  - dataSetNames
	 *  - visualFormats
	 *  - URLFormats
	 *  - data:
	 *      searchText
	 *      facetFilters
	 *      resultFunction
	 *      offset
	 *      limit
	 * @param \Change\Http\Event $event
	 */
	public function onSearch(\Change\Http\Event $event)
	{
		$result = [];
		$context = $event->paramsToArray();
		$data = $context['data'] ?? [];
		$searchText = (string)($data['searchText'] ?? '');
		$website = $context['website'] ?? null;
		$applicationServices = $event->getApplicationServices();

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();
		if ($searchText && $website instanceof \Rbs\Website\Documents\Website
			&& ($index = $elasticsearchManager->getDocumentsSearchIndex(true))
		)
		{
			$offset = (int)($data['offset'] ?? 0);
			$limit = (int)($data['limit'] ?? 1);
			$resultFunction = $data['resultFunction'];
			$facetFilters = $data['facetFilters'];
			$documentManager = $applicationServices->getDocumentManager();
			$i18nManager = $applicationServices->getI18nManager();
			$LCID = $i18nManager->getLCID();
			$facets = [];
			$block = new \Rbs\Elasticsearch\Blocks\Result();
			if ($facetFilters)
			{
				$block->buildFacets($elasticsearchManager->getDocumentsFacetsDefinition(), $facetFilters, $facets);
			}
			$result = $block->buildAjaxData($index, $searchText, $LCID, $website, $resultFunction, $facets, $facetFilters, $offset, $limit,
				$documentManager, $i18nManager, $context);
		}
		$ajaxResult = new \Change\Http\Web\Result\AjaxResult($result);
		$event->setResult($ajaxResult);
	}

	/**
	 * Default actionPath: Rbs/Elasticsearch/Facets
	 * Event params:
	 *  - website, section, page
	 *  - dataSetNames
	 *  - visualFormats
	 *  - URLFormats
	 *  - data:
	 *      searchText
	 *      resultFunction
	 *      facets
	 *      facetFilters
	 *      ...
	 * @param \Change\Http\Event $event
	 */
	public function onFacets(\Change\Http\Event $event)
	{
		$result = [];
		$context = $event->paramsToArray();
		$data = $context['data'] ?? [];
		$searchText = (string)($data['searchText'] ?? '');
		$resultFunction = $data['resultFunction'] ?? null;
		$facetIds = (array)($data['facets'] ?? []);
		$facetFilters = (array)($data['facetFilters'] ?? []);
		$website = $event->getParam('website');

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();
		if ($searchText && $website instanceof \Rbs\Website\Documents\Website
			&& ($index = $elasticsearchManager->getDocumentsSearchIndex(true))
		)
		{
			$facets = $elasticsearchManager->getDocumentsFacetsDefinitionByIds($facetIds);
			if ($facets)
			{
				$i18nManager = $event->getApplicationServices()->getI18nManager();
				$LCID = $i18nManager->getLCID();
				$block = new \Rbs\Elasticsearch\Blocks\Facets();
				$result['facetValues'] =
					$block->buildFacetsValues($index, $searchText, $LCID, $website, $resultFunction, $facets, $facetFilters, $event->paramsToArray());

				/** @var \Rbs\Website\Documents\Page $page */
				$page = $event->getParam('page');
				$pageId = $page ? $page->getId() : 0;

				/** @var \Rbs\Website\Documents\Section $section */
				$section = $event->getParam('section');
				$sectionId = $section ? $section->getId() : 0;

				/* @var $genericServices \Rbs\Generic\GenericServices */
				$genericServices = $event->getServices('genericServices');
				$applicationServices = $event->getApplicationServices();
				$resolver = new \Rbs\Seo\FacetUrl\PageDataResolver($applicationServices->getDocumentManager(),
					$applicationServices->getPathRuleManager(), $genericServices->getSeoManager());
				if ($resolver->resolve($website, $pageId, $sectionId, $LCID, $facetFilters))
				{
					$uri = $resolver->getUri();
					if ($searchText)
					{
						$uri->setQuery($uri->getQueryAsArray() + ['searchText' => $searchText]);
					}
					$result['location'] = $uri->normalize()->toString();
					$result['metas'] = $resolver->getMetas();
					$decorator = $resolver->getDecorator();
					$result['decoratorId'] = $decorator ? $decorator->getId() : 0;
				}
				else
				{
					$query = ['facetFilters' => $facetFilters];
					if ($searchText)
					{
						$query['searchText'] = $searchText;
					}
					$result['search'] = $query ? http_build_query($query) : '';
				}
			}
		}
		$ajaxResult = new \Change\Http\Web\Result\AjaxResult($result);
		$event->setResult($ajaxResult);
	}
}