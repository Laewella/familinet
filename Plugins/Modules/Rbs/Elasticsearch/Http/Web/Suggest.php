<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Http\Web;

/**
 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Http\Ajax\Search
 * @name \Rbs\Elasticsearch\Http\Web\Suggest
 */
class Suggest extends \Change\Http\Web\Actions\AbstractAjaxAction
{
	/**
	 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Http\Ajax\Search::onSuggest()
	 * @param \Change\Http\Web\Event $event
	 */
	public function execute(\Change\Http\Web\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}
}