<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Http\Rest;

use Change\Http\Rest\V1\ArrayResult;

/**
 * @name \Rbs\Elasticsearch\Http\Rest\FacetInputValues
 */
class FacetInputValues
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$distinctValues = [];
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$document = $documentManager->getDocumentInstance($event->getParam('documentId'));
		if ($document instanceof \Rbs\Elasticsearch\Documents\FacetValuesMapping)
		{
			$query = $documentManager->getNewQuery('Rbs_Generic_Attribute');
			$query->andPredicates($query->eq('indexValuesMapping', $document));
			/** @var \Rbs\Generic\Documents\Attribute[] $attributes */
			$attributes = $query->getDocuments()->preLoad()->toArray();
			foreach ($attributes as $attribute)
			{
				$distinctValues = array_merge($distinctValues, $attribute->getDistinctInputValues());
			}

			/* @var $commerceServices \Rbs\Commerce\CommerceServices */
			$commerceServices = $event->getServices('commerceServices');
			$model = $documentManager->getModelManager()->getModelByName('Rbs_Catalog_Axis');
			if ($commerceServices && $model)
			{
				$productManager = $commerceServices->getProductManager();
				$query = $documentManager->getNewQuery($model);
				$query->andPredicates($query->eq('indexValuesMapping', $document));
				/** @var \Rbs\Catalog\Documents\Axis[] $axes */
				$axes = $query->getDocuments()->preLoad()->toArray();
				foreach ($axes as $axis)
				{
					$axisValues = $productManager->getDistinctAxisValues($axis);
					foreach ($axisValues as $axisValue)
					{
						$distinctValues[] = $axisValue['value'] ?? null;
					}
				}
			}
		}
		$result = new ArrayResult();
		$distinctValues = array_keys(array_count_values(array_filter($distinctValues)));
		sort($distinctValues);
		$result->setArray($distinctValues);
		$event->setResult($result);
	}
}