<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Facet;

/**
 * @name \Rbs\Elasticsearch\Facet\DocumentFacetDefinition
 */
class DocumentFacetDefinition implements FacetDefinitionInterface
{
	const PARAM_MAPPING_NAME = 'mappingName';

	const DEFAULT_SIZE = 1000;
	/**
	 * @var int
	 */
	protected $id = 0;

	/**
	 * @var null|string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $fieldName;

	/**
	 * @var \Zend\Stdlib\Parameters
	 */
	protected $parameters;

	/**
	 * @var \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	protected $children = [];

	/**
	 * @var \Rbs\Elasticsearch\Facet\FacetDefinitionInterface
	 */
	protected $parent;

	/**
	 * @var string
	 */
	protected $mappingName;

	/**
	 * @var \Rbs\Collection\Documents\ObjectCollection|null
	 */
	protected $valuesSkin;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var integer
	 */
	protected $maxDisplayedValues;

	/**
	 * @param \Rbs\Elasticsearch\Documents\Facet|array|null $facet
	 */
	public function __construct($facet)
	{
		if ($facet instanceof \Rbs\Elasticsearch\Documents\Facet)
		{
			$this->initByFacet($facet);
		}
		elseif (is_array($facet))
		{
			$this->initByArray($facet);
		}
		else
		{
			$this->fieldName = 'f_' . $this->id;
			$this->parameters = new \Zend\Stdlib\Parameters();
			$this->maxDisplayedValues = 10;
		}
	}

	protected function initByFacet(\Rbs\Elasticsearch\Documents\Facet $facet)
	{
		$this->id = $facet->getId();
		$this->fieldName = 'f_' . $this->id;
		$this->parameters = $facet->getParameters();
		$this->parameters->set('configurationType', $facet->getConfigurationType());
		$this->title = $facet->getCurrentLocalization()->getTitle() ?? $facet->getRefLocalization()->getTitle();
		$this->valuesSkin = $facet->getFacetValuesSkin();

		/* Initializes the maximal number of values displayed with the total number of values.
		 * When you ask ElasticSearch to return a result size of 0 values for an aggregation, it returns all the possible values in the result */
		$this->maxDisplayedValues = 0;

		if (!$this->getParameters()->get('showAllValues'))
		{
			$this->maxDisplayedValues = $this->getParameters()->get('maxDisplayedValues', 10);
		}
	}

	protected function initByArray(array $data)
	{
		$this->id = $data['id'] ?? 0;
		$this->fieldName = $data['fieldName'] ??'f_' . $this->id;
		$parameters = $data['parameters'] ?? [];
		if ($parameters instanceof \Zend\Stdlib\Parameters)
		{
			$this->parameters = $parameters;
		}
		else
		{
			$this->parameters = new \Zend\Stdlib\Parameters(is_array($parameters) ? $parameters : []);
		}
		$this->mappingName = $data['mappingName'] ?? null;
		$this->title = $data['title'] ?? '';
		$valuesSkin = $data['valuesSkin'] ?? null;
		if ($valuesSkin instanceof \Rbs\Collection\Documents\Collection)
		{
			$this->valuesSkin = $valuesSkin;
		}

		/* Initializes the maximal number of values displayed with the total number of values.
		 * When you ask ElasticSearch to return a result size of 0 values for an aggregation, it returns all the possible values in the result */
		$this->maxDisplayedValues = 0;

		if (!$this->getParameters()->get('showAllValues'))
		{
			$this->maxDisplayedValues = $this->getParameters()->get('maxDisplayedValues', 10);
		}
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'document';
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param string $code
	 * @return \Rbs\Collection\Documents\Collection|null
	 */
	protected function getCollectionByCode($code)
	{
		$query = $this->getDocumentManager()->getNewQuery('Rbs_Collection_Collection');
		return $query->andPredicates($query->eq('code', $code))->getFirstDocument();
	}

	/**
	 * @return string
	 */
	protected function getMappingName()
	{
		return $this->mappingName;
	}

	/**
	 * @return string
	 */
	public function getFieldName()
	{
		return $this->fieldName;
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getParameters()
	{
		if ($this->parameters === null)
		{
			$this->parameters = new \Zend\Stdlib\Parameters();
		}
		return $this->parameters;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface|null
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @return boolean
	 */
	public function hasChildren()
	{
		return count($this->children) > 0;
	}

	/**
	 * @return FacetDefinitionInterface[]
	 */
	public function getChildren()
	{
		return $this->children;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $children
	 * @return $this
	 */
	public function setChildren(array $children)
	{
		$this->children = $children;
		return $this;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface|null $parent
	 * @return $this
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getMaxDisplayedValues()
	{
		return $this->maxDisplayedValues;
	}

	/**
	 * @param array $facetFilters
	 * @param array $context
	 * @return \Elastica\Query\BoolQuery|\Elastica\Query\Terms|null
	 */
	public function getFiltersQuery(array $facetFilters, array $context = [])
	{
		$terms = [];
		$orFilters = [];

		$filterName = $this->getFieldName();
		if (isset($facetFilters[$filterName]) && is_array($facetFilters[$filterName]))
		{
			/** @var array $facetFilter */
			$facetFilter = $facetFilters[$filterName];
			foreach ($facetFilter as $term => $subFacetFilter)
			{
				$andFilters = [];
				$terms[] = $term;
				if ($this->hasChildren())
				{
					$andFilters[] = new \Elastica\Query\Term([$this->getMappingName() => $term]);
					if (is_array($subFacetFilter))
					{
						foreach ($this->getChildren() as $childFacet)
						{
							if ($subFilter = $childFacet->getFiltersQuery($subFacetFilter, $context))
							{
								$andFilters[] = $subFilter;
							}
						}
					}
				}
				if (count($andFilters) == 1)
				{
					$orFilters[] = $andFilters[0];
				}
				elseif (count($andFilters) > 1)
				{
					$and = new \Elastica\Query\BoolQuery();
					foreach ($andFilters as $f)
					{
						$and->addMust($f);
					}
					$orFilters[] = $and;
				}
			}
		}

		if (count($orFilters) == 1)
		{
			return $orFilters[0];
		}
		elseif (count($orFilters) > 1)
		{
			$filter = new \Elastica\Query\BoolQuery();
			foreach ($orFilters as $orFilter)
			{
				$filter->addShould($orFilter);
			}
			return $filter;
		}
		elseif (count($terms))
		{
			return new \Elastica\Query\Terms($this->getMappingName(), $terms);
		}
		return null;
	}

	/**
	 * @param array $context
	 * @return \Elastica\Aggregation\Terms
	 */
	public function getAggregation(array $context = [])
	{
		$mappingName = $this->getMappingName();
		$aggregation = new \Elastica\Aggregation\Terms($mappingName);
		if ($this->getParameters()->get('showEmptyItem'))
		{
			$aggregation->setMinimumDocumentCount(0);
		}

		/*
		 * Gets all aggregation results from ElasticSearch.
		 * Results displayed will be computed in AggregationValues object, this allows to keep all values selected by the user.
		 */
		$aggregation->setSize(static::DEFAULT_SIZE);

		$aggregation->setField($mappingName);
		$this->aggregateChildren($aggregation, $context);
		return $aggregation;
	}

	/**
	 * @param \Elastica\Aggregation\AbstractAggregation $aggregation
	 * @param array $context
	 */
	protected function aggregateChildren($aggregation, array $context)
	{
		if ($this->hasChildren())
		{
			foreach ($this->getChildren() as $children)
			{
				$agg = $children->getAggregation($context);
				if ($agg instanceof \Elastica\Aggregation\AbstractAggregation)
				{
					$aggregation->addAggregation($agg);
				}
			}
		}
	}

	/**
	 * @param \Elastica\Aggregation\AbstractAggregation $aggregation
	 * @param array $context
	 */
	protected function aggregateRevertNestedChildren($aggregation, array $context)
	{
		if ($this->hasChildren())
		{
			$rv = new \Elastica\Aggregation\ReverseNested('_rn');
			$aggregation->addAggregation($rv);
			$this->aggregateChildren($rv, $context);
		}
	}

	/**
	 * @param array $aggregations
	 * @param array $context
	 * @return \Rbs\Elasticsearch\Facet\AggregationValues
	 */
	public function formatAggregation(array $aggregations, array $context)
	{
		$av = new \Rbs\Elasticsearch\Facet\AggregationValues($this);
		$buckets = $this->extractBuckets($aggregations);
		if (is_array($buckets))
		{
			$skinData = [];
			if ($this->valuesSkin instanceof \Rbs\Collection\Documents\Collection)
			{
				if ($this->getParameters()->get('showEmptyItem'))
				{
					$skinData = $this->valuesSkin->getAJAXData($context);
				}
				else
				{
					$context['data']['restrictToItems'] = [];
					/** @noinspection ForeachSourceInspection */
					foreach ($buckets as $bucket)
					{
						$context['data']['restrictToItems'][] = $bucket['key'];
					}
					$skinData = $this->valuesSkin->getAJAXData($context);
				}
			}

			if ($skinData)
			{
				$this->formatListAggregation($av, $skinData, $buckets, $context);
			}
			else
			{
				$this->formatKeyAggregation($av, $buckets, $context);
			}
		}
		return $av;
	}

	/**
	 * @param array $aggregations
	 * @return array|null
	 */
	protected function extractBuckets(array $aggregations)
	{
		$mappingName = $this->getMappingName();
		if (isset($aggregations[$mappingName]['buckets']))
		{
			return $aggregations[$mappingName]['buckets'];
		}
		return null;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\AggregationValues $av
	 * @param array $buckets
	 * @param array $context
	 */
	protected function formatKeyAggregation($av, $buckets, array $context)
	{
		foreach ($buckets as $bucket)
		{
			if (false !== ($title = $this->formatAggregationValueTitle($bucket['key'], $bucket['doc_count'])))
			{
				$v = new \Rbs\Elasticsearch\Facet\AggregationValue($bucket['key'], $bucket['doc_count'], $title);
				$av->addValue($v);
				$this->formatChildren($v, $bucket, $context);
			}
		}
	}

	/**
	 * @param string $key
	 * @param array $context
	 * @return string|null
	 */
	protected function formatAggregationValueTitle($key, $context)
	{
		return null;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\AggregationValues $av
	 * @param \Callable $callable
	 * @param array $buckets
	 * @param array $context
	 */
	protected function formatCallableTitleAggregation($av, $callable, $buckets, array $context)
	{
		foreach ($buckets as $bucket)
		{
			$title = call_user_func($callable, $bucket['key']);
			$v = new \Rbs\Elasticsearch\Facet\AggregationValue($bucket['key'], $bucket['doc_count'], $title);
			$av->addValue($v);
			$this->formatChildren($v, $bucket, $context);
		}
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\AggregationValues $av
	 * @param array $skinData
	 * @param array $buckets
	 * @param array $context
	 */
	protected function formatListAggregation($av, $skinData, $buckets, array $context)
	{
		$bucketByKey = [];
		foreach ($buckets as $bucket)
		{
			$bucketByKey[$bucket['key']] = $bucket;
		}

		if (!isset($skinData['items']) || !count($skinData['items']))
		{
			return;
		}

		$skins = $skinData['items'];
		$showEmptyItem = $this->getParameters()->get('showEmptyItem');
		/** @var array $skins */
		foreach ($skins as $skin)
		{
			$key = $skin['common']['value'];
			$title = $skin['common']['title'];
			$bucket = $bucketByKey[$key] ?? [];
			if ($showEmptyItem || count($bucket))
			{
				$count = isset($bucket['doc_count']) && $bucket['doc_count'] ? $bucket['doc_count'] : 0;
				$v = new \Rbs\Elasticsearch\Facet\AggregationValue($key, $count, $title);
				if (isset($skin['data']) && is_array($skin['data']) && count($skin['data']))
				{
					$v->setSkin($skin['data']);
				}
				$av->addValue($v);
				$this->formatChildren($v, $bucket, $context);
			}
		}
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\AggregationValue $aggregationValue
	 * @param array $bucket
	 * @param array $context
	 */
	protected function formatChildren($aggregationValue, array $bucket, array $context)
	{
		if ($this->hasChildren())
		{
			foreach ($this->getChildren() as $children)
			{
				$aggregationValue->addAggregationValues($children->formatAggregation($bucket['_rn'] ?? $bucket, $context));
			}
		}
	}

	/**
	 * @param array $buckets
	 * @return array
	 */
	public function buildAggregations(array $buckets)
	{
		return [$this->getMappingName() => ['buckets' => $buckets]];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\AbstractDocument $document
	 * @param array $documentData
	 * @return array
	 */
	public function addIndexData($document, array $documentData)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $documentData;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return array
	 */
	public function getMapping()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}
}