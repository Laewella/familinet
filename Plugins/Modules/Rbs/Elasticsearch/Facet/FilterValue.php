<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Facet;

/**
 * @name \Rbs\Elasticsearch\Facet\FilterValue
 */
class FilterValue
{
	/**
	 * @var \Rbs\Elasticsearch\Facet\FilterValue
	 */
	protected $parent;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $key;

	/**
	 * @var array|string
	 */
	protected $value;

	/**
	 * @param \Rbs\Elasticsearch\Facet\FilterValue $parent
	 * @param string $name
	 * @param string $key
	 * @param array|string $value
	 */
	public function __construct(\Rbs\Elasticsearch\Facet\FilterValue $parent = null, $name, $key , $value)
	{
		$this->parent = $parent;
		$this->name = $name;
		$this->key = $key;
		$this->value = $value;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$value = [$this->name => [$this->key => $this->value]];
		if ($this->parent)
		{
			$opv = $this->parent->value;
			$this->parent->value = $value;
			$value = $this->parent->toArray();
			$this->parent->value = $opv;
		}
		return $value;
	}
}