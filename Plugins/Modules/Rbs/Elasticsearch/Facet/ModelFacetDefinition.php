<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Facet;

/**
 * @name \Rbs\Elasticsearch\Facet\ModelFacetDefinition
 */
class ModelFacetDefinition extends \Rbs\Elasticsearch\Facet\DocumentFacetDefinition
{
	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @param \Rbs\Elasticsearch\Documents\Facet|array|null $facet
	 */
	public function __construct($facet)
	{
		parent::__construct($facet);
		$this->mappingName = 'model';
	}


	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager($modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	protected function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @return array
	 */
	protected function getDefaultParameters()
	{
		return ['showEmptyItem' => false, 'renderingMode' => 'checkbox',  'showAllValues' => false, 'maxDisplayedValues' => 10];
	}

	/**
	 * @param \Rbs\Elasticsearch\Documents\Facet $facet
	 */
	public function validateConfiguration($facet)
	{
		$facet->setIndexName(\Rbs\Elasticsearch\Manager::DOCUMENTS_INDEX);
		$validParameters = $this->getDefaultParameters();
		$currentParameters = $facet->getParameters();
		foreach ($currentParameters as $name => $value)
		{
			switch ($name)
			{
				case 'showAllValues':
				case 'showEmptyItem':
					$validParameters[$name] = $value === 'false' ? false : (bool)$value;
					break;
				case 'renderingMode':
					// TODO validate...
					$validParameters[$name] = $value;
					break;
				case 'maxDisplayedValues':
					$validParameters[$name] = is_int($value) && $value >= 0 ? $value : 10;
					break;
			}
		}
		if ($validParameters['showAllValues'])
		{
			$validParameters['maxDisplayedValues'] = 0;
		}
		$facet->getParameters()->fromArray($validParameters);
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		$title = parent::getTitle();
		return $title ?? $this->getI18nManager()->trans('m.rbs.elasticsearch.front.facet_model_title', ['ucf']);
	}

	protected function formatAggregationValueTitle($key, $context)
	{
		$model = $this->getModelManager()->getModelByName($key);
		if ($model)
		{
			return $this->getI18nManager()->trans($model->getLabelKey());
		}
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\AbstractDocument $document
	 * @param array $documentData
	 * @return array
	 */
	public function addIndexData($document, array $documentData)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $documentData;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return array
	 */
	public function getMapping()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}
}