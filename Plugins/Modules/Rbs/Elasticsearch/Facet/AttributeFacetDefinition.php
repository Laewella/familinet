<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Facet;

/**
 * @name \Rbs\Elasticsearch\Facet\AttributeFacetDefinition
 */
class AttributeFacetDefinition extends \Rbs\Elasticsearch\Facet\DocumentFacetDefinition
{

	/**
	 * @param \Rbs\Elasticsearch\Documents\Facet|array|null $facet
	 */
	public function __construct($facet)
	{
		parent::__construct($facet);
		$this->mappingName = $this->buildMappingName();
	}

	/**
	 * @return string
	 */
	protected function buildMappingName()
	{
		switch ($this->getValueType())
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
				return 'booleanAttribute_' . $this->getAttributeId();
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
				return 'longAttribute_' . $this->getAttributeId();
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
				return 'doubleAttribute_' . $this->getAttributeId();
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				return 'longAttribute_' . $this->getAttributeId();
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				return 'dateAttribute_' . $this->getAttributeId();
		}
		return 'stringAttribute_' . $this->getAttributeId();
	}

	/**
	 * @return integer
	 */
	protected function getValueType()
	{
		return $this->getParameters()->get('valueType', 'String');
	}

	/**
	 * @return integer
	 */
	protected function getAttributeId()
	{
		return $this->getParameters()->get('attributeId', 0);
	}

	/**
	 * @return \Rbs\Generic\Documents\Attribute|null
	 */
	protected function getAttribute()
	{
		$attribute = $this->getDocumentManager()->getDocumentInstance($this->getAttributeId());
		return $attribute instanceof \Rbs\Generic\Documents\Attribute ? $attribute : null;
	}

	/**
	 * @return array
	 */
	protected function getDefaultParameters()
	{
		return ['attributeId' => null, 'showEmptyItem' => false,
			'renderingMode' => 'checkbox', 'valueType' => false, 'showAllValues' => false, 'maxDisplayedValues' => 10];
	}

	/**
	 * @param \Rbs\Elasticsearch\Documents\Facet $facet
	 */
	public function validateConfiguration($facet)
	{
		$facet->setIndexName(\Rbs\Elasticsearch\Manager::DOCUMENTS_INDEX);
		$validParameters = $this->getDefaultParameters();
		$currentParameters = $facet->getParameters();

		/** @var $attribute \Rbs\Generic\Documents\Attribute */
		$attribute = null;
		foreach ($currentParameters as $name => $value)
		{
			switch ($name)
			{
				case 'attributeId':
					if ($value)
					{
						$attribute = $this->getDocumentManager()->getDocumentInstance($value, 'Rbs_Generic_Attribute');
						if ($attribute instanceof \Rbs\Generic\Documents\Attribute && $attribute->getIndexed())
						{
							$validParameters[$name] = $attribute->getId();
						}
						else
						{
							$attribute = null;
						}
					}
					break;
				case 'showAllValues':
				case 'showEmptyItem':
					$validParameters[$name] = $value === 'false' ? false : (bool)$value;
					break;
				case 'renderingMode':
					// TODO validate...
					$validParameters[$name] = $value;
					break;
				case 'maxDisplayedValues':
					$validParameters[$name] = is_int($value) && $value >= 0 ? $value : 10;
					break;
			}
		}

		if ($validParameters['showAllValues'])
		{
			$validParameters['maxDisplayedValues'] = 0;
		}

		if ($attribute)
		{
			$validParameters['valueType'] = $attribute->getValueType();
		}
		$facet->getParameters()->fromArray($validParameters);
	}

	/**
	 * @param string $key
	 * @param array $context
	 * @return string|null
	 */
	protected function formatAggregationValueTitle($key, $context)
	{
		$valueType = $this->getParameters()->get('valueType');
		if ($valueType === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID
			|| $valueType === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY)
		{
			$document = $this->getDocumentManager()->getDocumentInstance($key);
			if (!$document
				|| ($document instanceof \Change\Documents\Interfaces\Publishable && !$document->published())
				|| ($document instanceof \Change\Documents\Interfaces\Activable && !$document->activated())
			)
			{
				return null;
			}
			return $document->getDocumentModel()->getPropertyValue($document, 'title');
		}
		return null;
	}
}