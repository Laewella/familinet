<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Facet;

/**
 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Manager
 * @name \Rbs\Elasticsearch\Facet\FacetManager
 */
class FacetManager extends \Rbs\Elasticsearch\Manager
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Elasticsearch\Index\IndexDefinitionInterface $indexDefinition
	 * @return array
	 */
	public function getIndexMapping(\Rbs\Elasticsearch\Index\IndexDefinitionInterface $indexDefinition)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param array $indexMapping
	 * @param array $facetMapping
	 * @return array
	 */
	public function mergeFacetMapping(array $indexMapping, array $facetMapping = null)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param integer[] $facetIds
	 * @param \Rbs\Elasticsearch\Index\IndexDefinitionInterface $index
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	public function resolveFacetIds(array $facetIds, \Rbs\Elasticsearch\Index\IndexDefinitionInterface $index = null)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Elasticsearch\Documents\Facet $facet
	 * @return array
	 */
	public function extractInputValues(\Rbs\Elasticsearch\Documents\Facet $facet)
	{
		return [];
	}
}