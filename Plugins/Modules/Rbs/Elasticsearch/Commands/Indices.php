<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Commands;

/**
 * @name \Rbs\Elasticsearch\Commands\Indices
 */
class Indices
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$verbose = $event->getParam('verbose');
		$names = $event->getParam('names');
		$action = $event->getParam('action');
		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$manager = $genericServices->getElasticsearchManager();

		$response = $event->getCommandResponse();
		$indicesName = $this->normalizeIndicesName($names, $manager);
		if (!$indicesName)
		{
			$response->addInfoMessage('No index configured.');
			return;
		}

		$event->getApplicationServices()->getDbProvider()->setReadOnly(true);
		foreach ($indicesName as $name)
		{
			$index = $manager->getIndex($name);
			if ($action === 'stats')
			{
				$response->addInfoMessage('Stats of index: ' . $index->getName());
				if ($index->exists())
				{
					$indices = $index->getStats()->get('indices');
					foreach ($indices as $realName => $status)
					{
						$response->addInfoMessage('-- real name: ' . $realName);
						$numDocs = $size = 'undefined';
						if (isset($status['primaries']['docs']['count'], $status['primaries']['store']['size_in_bytes']))
						{
							$numDocs = $status['primaries']['docs']['count'];
							$size = ($status['primaries']['store']['size_in_bytes'] / 1024) . ' kb';
						}
						$response->addInfoMessage('-- documents: ' . $numDocs . ' , size: ' . $size);
					}
				}
				else
				{
					$response->addCommentMessage($index->getName() . ' is not defined on server.');
				}
			}
			elseif ($action === 'build')
			{
				$aliasName = $index->getName();

				/** @var \Elastica\Index|null $oldIndex */
				$oldIndex = null;
				$response->addInfoMessage('Building alias: ' . $aliasName);
				$newSuffix = '_0';
				if ($index->exists())
				{
					$aliasName = $index->getName();
					$status = new \Elastica\Status($index->getClient());
					$realIndices = $status->getIndicesWithAlias($aliasName);
					if ($realIndices)
					{
						$oldIndex = $realIndices[0];
						$oldIndex->getSettings()->set(['blocks.read_only' => 1]);
						$response->addInfoMessage('Current index: ' . $oldIndex->getName() . ' is now READ ONLY.');
						$suffix = substr($oldIndex->getName(), strlen($aliasName));

						if ($suffix === '_0')
						{
							$newSuffix = '_1';
						}
					}
				}

				$newIndex = $manager->getIndex($name, $newSuffix);
				$response->addInfoMessage('New index: ' . $newIndex->getName());
				if ($newIndex->exists())
				{
					$response->addWarningMessage('delete index: ' . $newIndex->getName());
					$newIndex->delete();
				}
				$configuration = $manager->getIndexConfiguration($name);
				if (!$configuration)
				{
					$response->addWarningMessage('Invalid configuration');
					continue;
				}

				$numberOfShards = $event->getParam('shards');
				/* Number of shards must be at least 1 */
				if (is_numeric($numberOfShards) && (int)$numberOfShards > 0)
				{
					$configuration['settings']['number_of_shards'] = $numberOfShards;
				}

				$numberOfReplicas = $event->getParam('replicas');
				/* Number of replicas can be set to 0 - no replicas of the primary shards will be created  */
				if (is_numeric($numberOfReplicas) && (int)$numberOfReplicas >= 0)
				{
					$configuration['settings']['number_of_replicas'] = $numberOfReplicas;
				}

				$newIndex->create($configuration);
				$response->addInfoMessage('Create index: ' . $newIndex->getName());

				$this->indexData($name, $newIndex, $manager, $response, $verbose);
				if ($oldIndex)
				{
					$oldIndex->getSettings()->set(['blocks.read_only' => 0]);
				}

				$newIndex->addAlias($aliasName, true);
				$response->addInfoMessage('Set alias ' . $aliasName . ' on index: ' . $newIndex->getName());

				if ($oldIndex)
				{
					$oldIndex->delete();
					$response->addInfoMessage('Delete old index:' . $oldIndex->getName());
				}
			}
		}

		$event->getApplicationServices()->getDbProvider()->setReadOnly(false);
		$response->addInfoMessage('Done.');
	}

	/**
	 * @param string $names
	 * @param \Rbs\Elasticsearch\Manager $manager
	 * @return array
	 */
	protected function normalizeIndicesName(string $names, \Rbs\Elasticsearch\Manager $manager)
	{
		$indicesName = [];
		$allowedNames = $manager->getIndicesName();
		if (!$names)
		{
			return $allowedNames;
		}
		foreach (explode(',', $names) as $name)
		{
			$n = strtolower(trim($name));
			if (in_array($n, $allowedNames))
			{
				$indicesName[$n] = $n;
			}
		}
		return array_values($indicesName);
	}

	/**
	 * @param string $name
	 * @param \Elastica\Index $index
	 * @param \Rbs\Elasticsearch\Manager $manager
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @param bool $verbose
	 */
	protected function indexData(string $name, \Elastica\Index $index, \Rbs\Elasticsearch\Manager $manager,
		\Change\Commands\Events\CommandResponseInterface $response, $verbose)
	{
		$count = 0;
		$generator = $manager->getDocumentIndexGenerator($name);
		$indexName = $index->getName();
		$bulk = new \Elastica\Bulk($index->getClient());
		$bulkLength = 0;
		foreach ($generator as $doc)
		{
			if ($doc && $data = $manager->getIndexData($name, $doc))
			{
				$id = $data['_id'] ?? '';
				$type = $data['_type'] ?? '';
				if (!$id || !$type)
				{
					continue;
				}
				$bulkLength++;
				$count++;
				unset($data['_id'], $data['_type']);
				$bulk->addDocument(new \Elastica\Document($id, $data, $type, $indexName));
				if ($bulkLength >= 100)
				{
					try
					{
						$bulk->send();
						if ($verbose)
						{
							$response->addCommentMessage($count . ' added.');
						}

					}
					catch (\Exception $e)
					{
						$response->addErrorMessage($e->getMessage());
					}
					$bulk = new \Elastica\Bulk($index->getClient());
					$bulkLength = 0;
				}
			}
		}

		if ($bulkLength)
		{
			try
			{
				$bulk->send();
				if ($verbose)
				{
					$response->addCommentMessage($count . ' added.');
				}
			}
			catch (\Exception $e)
			{
				$response->addErrorMessage($e->getMessage());
			}
		}
		$response->addCommentMessage($count . ' documents indexed.');
	}
}