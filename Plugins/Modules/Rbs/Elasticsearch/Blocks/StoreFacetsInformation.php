<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @deprecated since 1.8.0 use \Rbs\Catalog\Blocks\SearchFacetsInformation or \Rbs\Catalog\Blocks\ProductListFacetsInformation
 * @name \Rbs\Elasticsearch\Blocks\StoreFacetsInformation
 */
class StoreFacetsInformation extends \Change\Presentation\Blocks\Information
{
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.elasticsearch.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.store_facets_label', $ucf));
		$this->addParameterInformation('productListResolutionMode', \Change\Documents\Property::TYPE_STRING)
			->setLabel($i18nManager->trans('m.rbs.catalog.admin.product_list_type'))
			->setCollectionCode('Rbs_Catalog_ProductList_ResolutionModes');

		$this->addParameterInformationForDetailBlock('Rbs_Catalog_ProductList', $i18nManager)
			->setLabel($i18nManager->trans('m.rbs.catalog.admin.product_list_list', $ucf));

		$this->addParameterInformation('associatedDocumentId', \Change\Documents\Property::TYPE_DOCUMENTID)
			->setLabel($i18nManager->trans('m.rbs.catalog.admin.associated_product_list_document', $ucf))
			->setAllowedModelsNames('Rbs_Brand_Brand');

		$this->addParameterInformation('showUnavailable', \Change\Documents\Property::TYPE_BOOLEAN, false, true)
			->setLabel($i18nManager->trans('m.rbs.catalog.admin.product_list_show_unavailable', $ucf));

		$this->addParameterInformation('facets', \Change\Presentation\Blocks\ParameterInformation::TYPE_DOCUMENTIDARRAY)
			->setAllowedModelsNames('Rbs_Elasticsearch_Facet')
			->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.facets', $ucf));

		$this->setDefaultTTL(0);
	}
}
