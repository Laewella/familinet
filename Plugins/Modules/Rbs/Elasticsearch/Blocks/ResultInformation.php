<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @name \Rbs\Elasticsearch\Blocks\ResultInformation
 */
class ResultInformation extends \Change\Presentation\Blocks\Information
{
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.elasticsearch.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.result', $ucf));

		$this->addParameterInformation('itemsPerPage', \Change\Documents\Property::TYPE_INTEGER, false, 20)
			->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.result_items_per_page', $ucf))
			->setNormalizeCallback(function ($parametersValues) { return max(1, (int)($parametersValues['itemsPerPage'] ?? 20)); });
	}
}
