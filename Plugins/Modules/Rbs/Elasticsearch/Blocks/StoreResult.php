<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @deprecated since 1.8.0 use \Rbs\Catalog\Blocks\ProductResult
 * @name \Rbs\Elasticsearch\Blocks\StoreResult
 */
class StoreResult extends \Rbs\Catalog\Blocks\ProductResult
{
	/**
	 * @deprecated since 1.8.0 use \Rbs\Catalog\Blocks\ProductResult::parameterize($event)
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return parent::parameterize($event);
	}

	/**
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return string
	 */
	protected function resolveProductListResolutionMode(\Change\Presentation\Blocks\Parameters $parameters)
	{
		return parent::resolveProductListResolutionMode($parameters);
	}
}