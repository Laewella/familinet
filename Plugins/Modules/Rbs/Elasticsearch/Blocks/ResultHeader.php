<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @name \Rbs\Elasticsearch\Blocks\ResultHeader
 */
class ResultHeader extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('imageFormats');
		$parameters->setLayoutParameters($event->getBlockLayout());

		$request = $event->getHttpRequest();
		$searchText = $request->getQuery('searchText');
		if ($searchText && is_string($searchText))
		{
			$parameters->setParameterValue('searchText', trim($searchText));
		}

		/** @var $website \Rbs\Website\Documents\Website */
		$website = $event->getParam('website');
		if (!($website instanceof \Rbs\Website\Documents\Website))
		{
			return $parameters;
		}
		$parameters->setParameterValue('websiteId', $website->getId());
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		$logging = $event->getApplication()->getLogging();
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$websiteId = $parameters->getParameter('websiteId');

		/** @var \Rbs\Website\Documents\Website $website */
		$website = $websiteId ? $documentManager->getDocumentInstance($websiteId, 'Rbs_Website_Website') : null;
		if (!$website instanceof \Rbs\Website\Documents\Website)
		{
			$logging->warn(__METHOD__ . ': invalid website id ', $websiteId);
			return null;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();
		$index = $elasticsearchManager->getDocumentsSearchIndex(true);
		if (!$index)
		{
			return null;
		}

		$LCID = $documentManager->getLCID();
		$urlManager = $website->getUrlManager($LCID);
		$profile = $parameters->getParameter('resultFunctionsProfile');
		$resultFunctionsData = $this->prepareResultFunctionsData($elasticsearchManager->getDocumentsResultFunctionsData($profile), [], $urlManager);
		if (!$resultFunctionsData)
		{
			return null;
		}
		$resultFunctions = [];
		foreach ($resultFunctionsData as $resultFunctionData)
		{
			$resultFunctions[] = $resultFunctionData['name'];
		}

		$resultContext = $this->populateContext($event->getApplication(), $documentManager, $parameters);
		$contextParams = $resultContext->toArray();
		$contextData = $contextParams['data'] ?? [];
		$contextData['resultFunctions'] = $resultFunctions;
		unset($contextParams['data'], $contextParams['pagination'], $contextParams['website'], $contextParams['websiteUrlManager']);
		$blockData = ['contextParams' => $contextParams, 'contextData' => $contextData];
		$searchText = $parameters->getParameter('searchText');
		$i18nManager = $applicationServices->getI18nManager();
		if ($searchText)
		{
			$r = $this->buildData($index, $resultFunctions, $searchText, $LCID, $website);
			$blockData['result'] = $this->formatData($r, $resultFunctionsData, $searchText, $i18nManager);
		}
		$attributes['blockData'] = $blockData;
		return 'result-header.twig';
	}

	/**
	 * @param array $resultFunctionsData
	 * @param array $resultFunctions
	 * @param \Change\Http\Web\UrlManager $urlManager
	 * @return array
	 */
	public function prepareResultFunctionsData(array $resultFunctionsData, $resultFunctions, \Change\Http\Web\UrlManager $urlManager)
	{
		if ($resultFunctions && is_array($resultFunctions))
		{
			$resultFunctionsData = array_filter($resultFunctionsData,
				function ($i) use ($resultFunctions) { return in_array($i['name'], $resultFunctions); });
		}

		$result = [];
		foreach ($resultFunctionsData as $data)
		{
			$uri = $urlManager->getByFunction($data['name']);
			if ($uri)
			{
				$data['uri'] = $uri;
				$result[] = $data;
			}
		}
		return $result;
	}

	/**
	 * @param array $resultData
	 * @param array $resultFunctionsData
	 * @param string $searchText
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return array
	 */
	public function formatData(array $resultData, array $resultFunctionsData, $searchText, \Change\I18n\I18nManager $i18nManager)
	{
		$formatted = [];
		foreach ($resultFunctionsData as $resultFunctionData)
		{
			$functionName = $resultFunctionData['name'];
			$data = $resultData[$functionName] ?? ['resultFunction' => $functionName, 'count' => 0];

			/** @var \Zend\Uri\Http $uri */
			$uri = $resultFunctionData['uri'];
			$uri->setQuery($searchText ? ['searchText' => $searchText] : '');
			$data['url'] = $uri->normalize()->toString();
			$count = $data['count'];
			if ($count < 1)
			{
				$data['title'] = $i18nManager->trans($resultFunctionData['t0'], [], ['TOTAL' => $count]);
			}
			elseif ($count == 1)
			{
				$data['title'] = $i18nManager->trans($resultFunctionData['t1'], [], ['TOTAL' => $count]);
			}
			else
			{
				$data['title'] = $i18nManager->trans($resultFunctionData['tn'], [], ['TOTAL' => $count]);
			}
			$formatted[] = $data;
		}
		return $formatted;
	}

	/**
	 * @param \Elastica\Index $index
	 * @param array $resultFunctions
	 * @param string $searchText
	 * @param string $LCID
	 * @param \Rbs\Website\Documents\Website $website
	 * @return array
	 */
	public function buildData(\Elastica\Index $index, array $resultFunctions, string $searchText, string $LCID, \Rbs\Website\Documents\Website $website)
	{
		$result = [];
		$f = new \Rbs\Elasticsearch\Facet\DocumentFacetDefinition(['id' => 0, 'mappingName' => 'resultFunction', 'parameters' => [
			'showEmptyItem' => true
		]]);
		$documentsQuery = new \Rbs\Elasticsearch\Query\Documents();
		$facetFilters = [];
		foreach ($resultFunctions as $resultFunction)
		{
			$facetFilters[$f->getFieldName()][$resultFunction] = true;
		}

		$websiteId = $website->getId();
		$publish = $documentsQuery->getPublished(new \DateTime(), $LCID, $websiteId);
		$publish->addMust($documentsQuery->getSearchText($searchText, $LCID));
		if ($resultFilter = $documentsQuery->getFilters([$f], $facetFilters, []))
		{
			$publish->addMust($resultFilter);
		}
		$query = new \Elastica\Query($publish);
		$documentsQuery->setQueryAggregations($query, [$f], [], []);
		$rs = $index->search($query);
		$aggregationValues = $documentsQuery->formatAggregations($rs->getAggregations(), [$f], [], [])[0] ?? null;
		if ($aggregationValues)
		{
			foreach ($aggregationValues->getValues() as $aggregationValue)
			{
				$key = $aggregationValue->getKey();
				$result[$key] = ['resultFunction' => $key, 'count' => $aggregationValue->getValue()];
			}
		}
		return $result;
	}



	/**
	 * @param \Change\Application $application
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return \Change\Http\Ajax\V1\Context
	 */
	protected function populateContext($application, $documentManager, $parameters)
	{
		$context = new \Change\Http\Ajax\V1\Context($application, $documentManager);
		$context->setVisualFormats($parameters->getParameter('imageFormats'));
		$context->setURLFormats(['canonical']);
		$resultFunctions = $parameters->getParameter('resultFunctions');
		$context->addData('resultFunctions', is_array($resultFunctions) ? $resultFunctions : []);
		return $context;
	}
}