<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @deprecated since 1.8.0 use \Rbs\Catalog\Blocks\SearchFacets or \Rbs\Catalog\Blocks\ProductListFacets
 * @name \Rbs\Elasticsearch\Blocks\StoreFacets
 */
class StoreFacets extends \Rbs\Catalog\Blocks\ProductListFacets
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return parent::parameterize($event);
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		// Resolve product list resolution mode.
		$productListResolutionMode = $parameters->getParameterValue('productListResolutionMode');
		if ($productListResolutionMode === 'globalSearch')
		{
			$parameters->setParameterValue('resultFunction', 'Rbs_Catalog_ProductResult');
			$parameters->setParameterValue('ajaxPath', 'Rbs/Catalog/SearchFacets');
			$block = new \Rbs\Catalog\Blocks\SearchFacets();
			return $block->executeCompatibility($event, $attributes) ? 'store-facets-v2.twig' : null;
		}
		else
		{
			return parent::execute($event, $attributes) ? 'store-facets-v2.twig' : null;
		}
	}
}