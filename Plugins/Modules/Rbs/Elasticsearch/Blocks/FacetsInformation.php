<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @name \Rbs\Elasticsearch\Blocks\FacetsInformation
 */
class FacetsInformation extends \Change\Presentation\Blocks\Information
{
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.elasticsearch.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.facets_label', $ucf));
		$this->disableCache();

		$this->addParameterInformation('facets', \Change\Presentation\Blocks\ParameterInformation::TYPE_DOCUMENTIDARRAY)
			->setAllowedModelsNames('Rbs_Elasticsearch_Facet')
			->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.facets', $ucf));

		$this->addParameterInformation('resultFunction', \Change\Documents\Property::TYPE_STRING)
			->setDefaultValue('Rbs_Elasticsearch_Result')
			->setLabel($i18nManager->trans('m.rbs.elasticsearch.admin.result_function_label'))
			->setCollectionCode('Rbs_Elasticsearch_Result_Functions');
	}
}
