<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @name \Rbs\Elasticsearch\Blocks\Facets
 */
class Facets extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('searchText', '');
		$parameters->addParameterMeta('facetFilters', []);
		$parameters->addParameterMeta('facets');
		$parameters->addParameterMeta('websiteId', 0);
		$parameters->addParameterMeta('resultFunction', 'Rbs_Elasticsearch_Result');
		$parameters->addParameterMeta('imageFormats', 'selectorItem');
		$parameters->addParameterMeta('ajaxPath', 'Rbs/Elasticsearch/Facets');
		$parameters->setLayoutParameters($event->getBlockLayout());
		$parameters->setNoCache();

		/** @var $website \Rbs\Website\Documents\Website */
		$website = $event->getParam('website');
		if (!($website instanceof \Rbs\Website\Documents\Website))
		{
			return $parameters;
		}
		$parameters->setParameterValue('websiteId', $website->getId());

		$request = $event->getHttpRequest();
		$facetFilters = [];
		/** @var array $queryFilters */
		$queryFilters = $request->getQuery('facetFilters', null);
		if (is_array($queryFilters))
		{
			foreach ($queryFilters as $fieldName => $rawValue)
			{
				if ($rawValue && is_array($rawValue))
				{
					$facetFilters[$fieldName] = $rawValue;
				}
			}
		}
		$parameters->setParameterValue('facetFilters', $facetFilters);

		$searchText = $request->getQuery('searchText');
		if ($searchText && is_string($searchText))
		{
			$parameters->setParameterValue('searchText', $searchText);
		}
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		/** @var $website \Rbs\Website\Documents\Website */
		$website = $event->getParam('website');
		if (!($website instanceof \Rbs\Website\Documents\Website))
		{
			return null;
		}

		$parameters = $event->getBlockParameters();
		$facetIds = $parameters->getParameter('facets');
		if (!$facetIds || !is_array($facetIds))
		{
			return null;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();
		$index = $elasticsearchManager->getDocumentsSearchIndex(true);
		if (!$index)
		{
			return null;
		}
		$facetFilters = $parameters->getParameter('facetFilters');
		$facets = $this->normalizeFacets($elasticsearchManager, $facetIds, $facetFilters);
		if (!$facets)
		{
			return null;
		}
		elseif (count($facets) != count($facetIds))
		{
			$facetIds = [];
			foreach ($facets as $facet)
			{
				$facetIds[] = $facet->getId();
			}
			$parameters->setParameterValue('facets', $facetIds);
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$resultContext = $this->populateContext($event->getApplication(), $documentManager, $parameters);
		$resultContext->setWebsite($website);
		$resultContext->setSection($event->getParam('section'));
		$resultContext->setPage($event->getParam('page'));
		$blockData = $this->buildDefaultBlockData($resultContext, $documentManager);

		$context = $resultContext->toArray();
		$LCID = $documentManager->getLCID();
		$resultFunction = $parameters->getParameter('resultFunction');
		$searchText = $parameters->getParameter('searchText');
		$blockData['facetValues'] = $this->buildFacetsValues($index, $searchText, $LCID, $website, $resultFunction, $facets, $facetFilters, $context);
		$attributes['blockData'] = $blockData;
		return 'facets-v2.twig';
	}

	/**
	 * @param \Rbs\Elasticsearch\Manager $elasticsearchManager
	 * @param $facetIds
	 * @param $facetFilters
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	protected function normalizeFacets(\Rbs\Elasticsearch\Manager $elasticsearchManager, array $facetIds, &$facetFilters)
	{
		$facets = [];
		$facetsById = $elasticsearchManager->getDocumentsFacetsDefinitionByIds($facetIds);
		foreach ($facetIds as $facetId)
		{
			if (isset($facetsById[$facetId]))
			{
				$facets[] = $facetsById[$facetId];
			}
		}

		$rawFacetFilters = $facetFilters;
		$facetFilters = [];
		foreach ($facets as $facet)
		{
			$fieldName = $facet->getFieldName();
			$facetFilter = $rawFacetFilters[$fieldName] ?? null;
			if ($facetFilter && is_array($facetFilter))
			{
				$facetFilters[$fieldName] = $facetFilter;
			}
		}
		return $facets;
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return \Change\Http\Ajax\V1\Context
	 */
	protected function populateContext($application, $documentManager, $parameters)
	{
		$context = new \Change\Http\Ajax\V1\Context($application, $documentManager);
		$context->setVisualFormats($parameters->getParameter('imageFormats'));
		$context->setURLFormats(['canonical']);
		$context->setWebsiteId($parameters->getParameter('websiteId'));
		$context->addData('facets', $parameters->getParameter('facets'));
		$context->addData('facetFilters', $parameters->getParameter('facetFilters'));
		$context->addData('resultFunction', $parameters->getParameter('resultFunction'));
		$context->addData('searchText', $parameters->getParameter('searchText'));
		return $context;
	}

	/**
	 * @param \Elastica\Index $index
	 * @param string $searchText
	 * @param string $LCID
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string $resultFunction
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return array
	 */
	public function buildFacetsValues($index, $searchText, $LCID, $website, $resultFunction, $facets, $facetFilters, $context)
	{
		$documentsQuery = new \Rbs\Elasticsearch\Query\Documents();
		$bool = $documentsQuery->getPublished(new \DateTime(), $LCID, $website->getId());
		if ($searchText)
		{
			$bool->addMust($documentsQuery->getSearchText($searchText, $LCID));
		}
		if ($resultFunction)
		{
			$bool->addMust($documentsQuery->getResultFunctions([$resultFunction]));
		}
		$query = new \Elastica\Query($bool);
		$documentsQuery->setQueryAggregations($query, $facets, $facetFilters, $context['data'] ?? []);
		$searchResult = $index->search($query);
		$facetsValues = [];
		foreach ($documentsQuery->formatAggregations($searchResult->getAggregations(), $facets, $facetFilters, $context) as $facetValues)
		{
			$facetsValues[] = $facetValues->toArray();
		}
		return $facetsValues;
	}

	/**
	 * @param \Change\Http\Ajax\V1\Context $resultContext
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array
	 */
	protected function buildDefaultBlockData(\Change\Http\Ajax\V1\Context $resultContext, \Change\Documents\DocumentManager $documentManager)
	{
		$contextParams = $resultContext->toArray();
		$facetsData = [];
		if ($facetIds = $contextParams['data']['facets'] ?? [])
		{
			foreach ($facetIds as $facetId)
			{
				$documentFacet = $documentManager->getDocumentInstance($facetId);
				if ($documentFacet instanceof \Rbs\Elasticsearch\Documents\Facet)
				{
					$facetsData[$facetId] = $documentFacet->getAJAXData($contextParams);
				}
			}
		}

		$contextData = $contextParams['data'] ?? [];
		unset($contextParams['data'], $contextParams['pagination'],
			$contextParams['website'], $contextParams['section'], $contextParams['page'], $contextParams['websiteUrlManager']);
		return ['contextParams' => $contextParams, 'contextData' => $contextData, 'facets' => $facetsData];
	}
}