<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Blocks;

/**
 * @name \Rbs\Elasticsearch\Blocks\Result
 */
class Result extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('searchText');
		$parameters->addParameterMeta('itemsPerPage', 20);
		$parameters->addParameterMeta('searchOffset', 0);
		$parameters->addParameterMeta('facetFilters');
		$parameters->addParameterMeta('resultFunction', 'Rbs_Elasticsearch_Result');
		$parameters->addParameterMeta('imageFormats', 'listItem');
		$parameters->setLayoutParameters($event->getBlockLayout());

		$request = $event->getHttpRequest();
		/** @var array $queryFilters */
		$queryFilters = $request->getQuery('facetFilters', null);
		$facetFilters = $queryFilters && is_array($queryFilters) ? $queryFilters : [];
		$parameters->setParameterValue('facetFilters', $facetFilters);
		$searchText = $request->getQuery('searchText');
		if ($searchText && is_string($searchText))
		{
			$parameters->setParameterValue('searchText', trim($searchText));
			$parameters->setParameterValue('searchOffset', (int)$request->getQuery('searchOffset', 0));
		}
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();
		$website = $event->getParam('website');
		if (!$website instanceof \Rbs\Website\Documents\Website)
		{
			return null;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();

		$index = $elasticsearchManager->getDocumentsSearchIndex(true);
		if (!$index)
		{
			return null;
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$blockData = [];
		$resultContext = $this->populateContext($event->getApplication(), $documentManager, $parameters);
		$contextParams = $resultContext->toArray();
		if ($searchText = $parameters->getParameter('searchText'))
		{
			$resultFunction = $parameters->getParameter('resultFunction');
			$i18nManager = $applicationServices->getI18nManager();
			$LCID = $documentManager->getLCID();

			$offset = (int)$parameters->getParameter('searchOffset');
			$limit = $parameters->getParameter('itemsPerPage');

			$facets = [];
			$facetFilters = $parameters->getParameter('facetFilters');
			if ($facetFilters)
			{
				$this->buildFacets($elasticsearchManager->getDocumentsFacetsDefinition(), $facetFilters, $facets);
			}
			$blockData = $this->buildAjaxData($index, $searchText, $LCID, $website, $resultFunction, $facets, $facetFilters, $offset, $limit,
				$documentManager, $i18nManager, $contextParams);
		}

		$contextData = $contextParams['data'] ?? [];
		$contextData['searchText'] = $searchText;
		unset($contextParams['data'], $contextParams['pagination'], $contextParams['website'], $contextParams['websiteUrlManager']);

		$blockData['contextParams'] = $contextParams;
		$blockData['contextData'] = $contextData;
		$attributes['blockData'] = $blockData;
		return 'result.twig';
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return \Change\Http\Ajax\V1\Context
	 */
	protected function populateContext($application, $documentManager, $parameters)
	{
		$context = new \Change\Http\Ajax\V1\Context($application, $documentManager);
		$context->setDetailed(false);
		$context->setVisualFormats($parameters->getParameter('imageFormats'));
		$context->setDataSetNames(['searchResult']);
		$context->setURLFormats(['canonical']);
		$context->addData('resultFunction', $parameters->getParameter('resultFunction'));
		$context->addData('searchText', $parameters->getParameter('searchText'));
		$context->addData('facetFilters', $parameters->getParameter('facetFilters'));
		$context->addData('offset', $parameters->getParameter('searchOffset'));
		$context->addData('limit', $parameters->getParameter('itemsPerPage'));
		return $context;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facetsDefinition
	 * @param array $facetFilters
	 * @param array $facets
	 */
	public function buildFacets($facetsDefinition, &$facetFilters, &$facets)
	{
		$rawFacetFilters = $facetFilters;
		$facetFilters = [];
		$facets = [];
		if ($rawFacetFilters && is_array($rawFacetFilters))
		{
			foreach ($facetsDefinition as $facetDefinition)
			{
				$facetFilter = $rawFacetFilters[$facetDefinition->getFieldName()] ?? null;
				if ($facetFilter && is_array($facetFilter))
				{
					$facets[] = $facetDefinition;
					$facetFilters[$facetDefinition->getFieldName()] = $facetFilter;
				}
			}
		}
	}

	/**
	 * @param \Elastica\Index $index
	 * @param string $searchText
	 * @param string $LCID
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string $resultFunction
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param integer $offset
	 * @param integer $limit
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param array $context
	 * @return array
	 */
	public function buildAjaxData($index, $searchText, $LCID, $website, $resultFunction, $facets, $facetFilters, $offset, $limit,
		$documentManager, $i18nManager, array $context)
	{
		$documentsQuery = new \Rbs\Elasticsearch\Query\Documents();
		$bool = $documentsQuery->getPublished(new \DateTime(), $LCID, $website->getId());
		$bool->addMust($documentsQuery->getSearchText($searchText, $LCID));
		if ($resultFunction)
		{
			$bool->addMust($documentsQuery->getResultFunctions((array)$resultFunction));
		}

		if ($facets)
		{
			$filter = $documentsQuery->getFilters($facets, $facetFilters, []);
			if ($filter)
			{
				$bool->addMust($filter);
			}
		}
		$query = new \Elastica\Query($bool);
		$query->setHighlight($documentsQuery->getHighlight($LCID));
		$query->setFrom($offset)->setSize($limit)->setSource(false);
		$blockData['query'] = $query->toArray();
		$searchResult = $index->search($query);
		$totalHits = $searchResult->getTotalHits();
		$blockData['pagination'] = ['count' => $totalHits, 'offset' => $offset, 'limit' => $limit];
		$blockData['items'] = [];

		if ($results = $searchResult->getResults())
		{
			$separator = $i18nManager->trans('m.rbs.elasticsearch.front.fragment_separator');
			$maxScore = $searchResult->getMaxScore();
			$blockData['pagination']['pageCount'] = ceil($totalHits / $limit);
			$urlManager = $website->getUrlManager($LCID);
			/* @var $result \Elastica\Result */
			foreach ($results as $result)
			{
				$document = $documentManager->getDocumentInstance($result->getId());
				if ($document instanceof \Change\Documents\Interfaces\Publishable && $document->published())
				{
					$titleField = $LCID . '.title';
					$contentField = $LCID . '.content';
					$highlights = $result->getHighlights();
					if (isset($highlights[$titleField]))
					{
						$title = $highlights[$titleField][0];
					}
					else
					{
						$titleProperty = $document->getDocumentModel()->getProperty('title');
						$title = $titleProperty ? $i18nManager->transformHtml($titleProperty->getValue($document), $LCID) : null;
					}
					$content = $highlights[$contentField] ?? null;
					$content = $content && is_array($content) ? ($separator . implode($separator, $content) . $separator) : null;

					$score = ceil(($result->getScore() / $maxScore) * 100);
					$blockData['items'][] = [
						'id' => $result->getId(),
						'type' => $i18nManager->trans($document->getDocumentModel()->getLabelKey(), ['ucf']),
						'score' => $score,
						'title' => $title,
						'document' => $document->getAJAXData($context),
						'url' => $urlManager->getCanonicalByDocument($document)->normalize()->toString(),
						'content' => $content
					];
				}
			}
		}
		return $blockData;
	}

}