<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Query;

/**
 * @name \Rbs\Elasticsearch\Query\Documents
 */
class Documents
{
	/**
	 * @param string $searchText
	 * @param string $LCID
	 * @return \Elastica\Query\MatchAll|\Elastica\Query\MultiMatch
	 */
	public function getSearchText($searchText, $LCID)
	{
		if ($searchText)
		{
			/*
			 * Match all the words with
			 *  - document title
			 *  - OR document title exact match (case insensitive and ASCII folded) (*)
			 *  - OR document content
			 *  - OR any keyword linked to the document
			 *
			 * (*) Document title exact match is present to increase score when exact word is found in a document
			 * E.g: "Soluté" & "Solution" are stemmed the same way. Thus, they return the same results when searched.
			 * So, if "Soluté" is also matched exactly when doing the search on that word, results including exactly this keyword will be returned first.
			 */
			$multiMatch = new \Elastica\Query\MultiMatch();
			$multiMatch->setQuery($searchText);
			$multiMatch->setFields([$LCID . '.title', $LCID . '.title.exact_match', $LCID . '.content', $LCID . '.keywords']);
			$multiMatch->setOperator('and');
		}
		else
		{
			$multiMatch = new \Elastica\Query\MatchAll();
		}
		return $multiMatch;
	}

	/**
	 * @param \DateTime $at
	 * @param string $LCID
	 * @param integer $websiteId
	 * @param integer[] $sectionIds
	 * @return \Elastica\Query\BoolQuery
	 */
	public function getPublished($at, $LCID, $websiteId = 0, array $sectionIds = [])
	{
		if (!$at instanceof \DateTime)
		{
			$at = new \DateTime();
		}

		$date = $at->format(\DateTime::ATOM);
		$bool = new \Elastica\Query\BoolQuery();
		$bool->addMust(new \Elastica\Query\Term([$LCID . '.publishable' => true]));
		$bool->addMust(new \Elastica\Query\Range($LCID . '.startPublication', ['lte' => $date]));
		$bool->addMust(new \Elastica\Query\Range($LCID . '.endPublication', ['gt' => $date]));

		if ($websiteId || $sectionIds)
		{
			$sectionsQuery = new \Elastica\Query\Nested();
			$sectionsQuery->setPath('sections');
			$nestedBool = new \Elastica\Query\BoolQuery();
			if ($sectionIds)
			{
				$nestedBool->addMust(new \Elastica\Query\Terms('sections.sectionId', $sectionIds));
			}
			else
			{
				$nestedBool->addMust(new \Elastica\Query\Term(['sections.websiteId' => $websiteId]));
			}

			$sectionsQuery->setQuery($nestedBool);

			$bool->addMust($sectionsQuery);
		}

		return $bool;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Attribute $attribute
	 * @param mixed $value
	 * @return \Elastica\Query\Nested|null
	 */
	public function getAttribute(\Change\Documents\Attributes\Interfaces\Attribute $attribute, $value)
	{
		$valueFieldName = $this->getValueFieldName($attribute);
		if (!$valueFieldName)
		{
			return null;
		}
		$normalizedValue = $this->normalizeAttributeValue($valueFieldName, $value);
		if ($normalizedValue === null)
		{
			return null;
		}

		$typologyDataQuery = new \Elastica\Query\Nested();
		$typologyDataQuery->setPath('typologyData');
		$attribute->getName();
		$bool = new \Elastica\Query\BoolQuery();
		if (is_array($normalizedValue))
		{
			$bool->addMust(new \Elastica\Query\Terms('typologyData.' . $valueFieldName, $normalizedValue));
		}
		else
		{
			$bool->addMust(new \Elastica\Query\Term(['typologyData.' . $valueFieldName => $normalizedValue]));
		}
		$typologyDataQuery->setQuery($bool);
		return $typologyDataQuery;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Attribute $attribute
	 * @return null|string
	 */
	protected function getValueFieldName(\Change\Documents\Attributes\Interfaces\Attribute $attribute)
	{
		switch ($attribute->getType())
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
				return 'booleanValue';
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				return 'longValue';
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
				return 'doubleValue';
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				return 'dateValue';
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
				if (!$attribute->getLocalized())
				{
					return 'stringValue';
				}
				break;
		}
		return null;
	}

	/**
	 * @param $valueFieldName
	 * @param mixed|array $value
	 * @param bool $arrayItem
	 * @return mixed|null
	 */
	protected function normalizeAttributeValue($valueFieldName, $value, $arrayItem = false)
	{
		if ($value === null || $value === [] || (is_array($value) && $arrayItem))
		{
			return null;
		}
		if (is_array($value))
		{
			$nValues = [];
			foreach ($value as $v)
			{
				$nv = $this->normalizeAttributeValue($valueFieldName, $v, true);
				if ($nv !== null)
				{
					$nValues[] = $nv;
				}
			}
			return $nValues ?: null;
		}
		switch ($valueFieldName)
		{
			case 'booleanValue':
				return (bool)$value;
			case 'longValue':
				return (int)$value;
			case 'doubleValue':
				return (float)$value;
			case 'stringValue':
				return (string)$value;
			case 'dateValue':
				if (is_string($value))
				{
					$value = new \DateTime($value);
				}
				if ($value instanceof \DateTime)
				{
					return $value->format(\DateTime::ATOM);
				}
				break;
		}
		return null;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return \Elastica\Query\BoolQuery|null
	 */
	public function getFilters(array $facets, array $facetFilters, array $context)
	{
		$filters = [];
		foreach ($facets as $facet)
		{
			$filter = $facet->getFiltersQuery($facetFilters, $context);
			if ($filter)
			{
				$filters[] = $filter;
			}
		}

		if (count($filters))
		{
			$bool = new \Elastica\Query\BoolQuery();
			foreach ($filters as $filter)
			{
				$bool->addMust($filter);
			}
			return $bool;
		}

		return null;
	}

	/**
	 * @param string[] $resultFunctions
	 * @return \Elastica\Query\Term|\Elastica\Query\Terms|null
	 */
	public function getResultFunctions(array $resultFunctions)
	{
		if ($resultFunctions)
		{
			$terms = [];
			foreach ($resultFunctions as $resultFunction)
			{
				$term = trim((string)$resultFunction);
				if ($term)
				{
					$terms[] = $term;
				}
			}
			if ($terms)
			{
				if (count($terms) === 1)
				{
					return new \Elastica\Query\Term(['resultFunction' => $terms[0]]);
				}
				return new \Elastica\Query\Terms('resultFunction', $terms);
			}
		}
		return null;
	}

	/**
	 * @param string[] $models
	 * @return \Elastica\Query\Term|\Elastica\Query\Terms|null
	 */
	public function getDocumentModels(array $models)
	{
		if ($models)
		{
			$terms = [];
			foreach ($models as $model)
			{
				$term = trim((string)$model);
				if ($term)
				{
					$terms[] = $term;
				}
			}
			if ($terms)
			{
				if (count($terms) === 1)
				{
					return new \Elastica\Query\Term(['model' => $terms[0]]);
				}
				return new \Elastica\Query\Terms('model', $terms);
			}
		}
		return null;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return \Elastica\Aggregation\AbstractAggregation[]
	 */
	public function getAggregations(array $facets, array $facetFilters, array $context = [])
	{
		$aggregations = [];
		foreach ($facets as $facet)
		{
			$customFilter = $facetFilters;
			unset($customFilter[$facet->getFieldName()]);
			$aggregation = $facet->getAggregation($context);
			if ($aggregation instanceof \Elastica\Aggregation\AbstractAggregation)
			{
				if (count($customFilter))
				{
					$filter = $this->getFilters($facets, $customFilter, $context);
					if ($filter)
					{
						$aggFilter = new \Elastica\Aggregation\Filter('filtered__' . $aggregation->getName());
						$aggFilter->setFilter($filter);
						$aggFilter->addAggregation($aggregation);
						$aggregations[] = $aggFilter;
						continue;
					}
				}
				$aggregations[] = $aggregation;
			}
		}
		return $aggregations;
	}

	/**
	 * @param \Elastica\Query $query
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return $this
	 */
	public function setQueryAggregations(\Elastica\Query $query, array $facets, array $facetFilters, array $context = [])
	{
		$aggregations = $this->getAggregations($facets, $facetFilters, $context);
		foreach ($aggregations as $aggregation)
		{
			$query->addAggregation($aggregation);
		}
		$query->setSize(0);
		return $this;
	}

	/**
	 * @param array $rawAggregations
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return \Rbs\Elasticsearch\Facet\AggregationValues[]
	 */
	public function formatAggregations(array $rawAggregations, array $facets, array $facetFilters, array $context)
	{
		$aggregations = [];
		foreach ($rawAggregations as $name => $val)
		{
			if (strpos($name, 'filtered__') === 0)
			{
				$realName = substr($name, 10);
				if (is_array($val) && isset($val[$realName]))
				{
					$aggregations[$realName] = $val[$realName];
				}
			}
			else
			{
				$aggregations[$name] = $val;
			}
		}

		$formattedAggregations = [];
		foreach ($facets as $facet)
		{
			/** @var \Rbs\Elasticsearch\Facet\AggregationValues $aggValues */
			$aggValues = $facet->formatAggregation($aggregations, $context);
			if ($aggValues instanceof \Rbs\Elasticsearch\Facet\AggregationValues)
			{
				$this->applyFacetFilters($aggValues, $facetFilters, $context);
			}
			$formattedAggregations[] = $aggValues;
		}
		return $formattedAggregations;
	}

	/**
	 * @param \Rbs\Elasticsearch\Facet\AggregationValues $aggregationValues
	 * @param array $facetFilter
	 * @param array $context
	 */
	protected function applyFacetFilters(\Rbs\Elasticsearch\Facet\AggregationValues $aggregationValues, array $facetFilter, array $context = [])
	{
		$facetFilter = $facetFilter[$aggregationValues->getFieldName()] ?? [];
		if (!$facetFilter)
		{
			return;
		}

		foreach ($aggregationValues->getValues() as $aggValue)
		{
			$key = (string)$aggValue->getKey();
			if (isset($facetFilter[$key]))
			{
				$aggValue->setSelected(true);
				if (is_array($facetFilter[$key]) && $aggValue->hasAggregationValues())
				{
					foreach ($aggValue->getAggregationValues() as $subAggregationValues)
					{
						$this->applyFacetFilters($subAggregationValues, $facetFilter[$key], $context);
					}
				}
				unset($facetFilter[$key]);
			}
		}

		// All selected values were not found in the result
		if ($facetFilter)
		{
			$buckets = [];
			$facet = $aggregationValues->getFacet();
			foreach ($facetFilter as $key => $value)
			{
				$bucket = ['key' => $key, 'doc_count' => 0];
				$buckets[] = $bucket;
			}

			$aggregations = $facet->buildAggregations($buckets);
			$av = $facet->formatAggregation($aggregations, $context);
			foreach ($av->getValues() as $aggValue)
			{
				$aggValue->setSelected(true);
				$aggregationValues->addValue($aggValue);
				$key = $aggValue->getKey();
				if (is_array($facetFilter[$key]))
				{
					foreach ($aggValue->getAggregationValues() as $subAggregationValues)
					{
						$this->applyFacetFilters($subAggregationValues, $facetFilter[$key], $context);
					}
				}
			}
		}
	}

	/**
	 * @param string $LCID
	 * @return array
	 */
	public function getHighlight($LCID)
	{
		return ['tags_schema' => 'styled', 'fields' => [
			$LCID .'.title' => ['number_of_fragments' => 0],
			$LCID .'.content' => [
				'fragment_size' => 150,
				'number_of_fragments' => 3,
				'no_match_size' => 150
			]
		]];
	}
}