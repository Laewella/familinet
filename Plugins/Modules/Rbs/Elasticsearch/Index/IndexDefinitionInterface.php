<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Index;

/**
 * @deprecated since 1.8.0 with no replacement
 * @name \Rbs\Elasticsearch\Index\IndexDefinitionInterface
 */
interface IndexDefinitionInterface
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string
	 */
	public function getClientName();

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string
	 */
	public function getName();

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string fulltext | store
	 */
	public function getCategory();

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string
	 */
	public function getAnalysisLCID();

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return array
	 */
	public function getConfiguration();

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[]
	 */
	public function getFacetsDefinition();

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Elasticsearch\Index\IndexManager $indexManager
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param \Change\Documents\AbstractModel $model
	 * @return array [type => [propety => value]]
	 */
	public function getDocumentIndexData(\Rbs\Elasticsearch\Index\IndexManager $indexManager, $document, $model = null);
}