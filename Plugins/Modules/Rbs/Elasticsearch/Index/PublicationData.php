<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Index;

/**
 * @deprecated since 1.8.0 with no replacement
 * @see \Rbs\Elasticsearch\Indices\DocumentsData
 * @name \Rbs\Elasticsearch\Index\PublicationData
 */
class PublicationData
{

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\TreeManager $treeManager
	 * @return $this
	 */
	public function setTreeManager($treeManager)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\Interfaces\Publishable $document
	 * @param \Rbs\Website\Documents\Website $website
	 * @return integer|null
	 */
	public function getCanonicalSectionId($document, $website)
	{
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\Interfaces\Publishable|\Change\Documents\AbstractDocument $document
	 * @param array $documentData
	 * @return array
	 */
	public function addPublishableMetas($document, array $documentData)
	{
		return $documentData;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\AbstractDocument $document
	 * @param \Rbs\Website\Documents\Website $website
	 * @param array $documentData
	 * @param \Rbs\Elasticsearch\Documents\Index $index
	 * @param \Rbs\Elasticsearch\Index\IndexManager $indexManager
	 * @return array
	 */
	public function addPublishableContent($document, $website, array $documentData,
		\Rbs\Elasticsearch\Documents\Index $index = null, \Rbs\Elasticsearch\Index\IndexManager $indexManager = null)
	{
		return $documentData;
	}
} 