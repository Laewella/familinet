<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Index;

/**
 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Manager
 * @name \Rbs\Elasticsearch\Index\IndexManager
 */
class IndexManager extends \Rbs\Elasticsearch\Manager
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param array $clientsConfiguration
	 */
	public function setClientsConfiguration(array $clientsConfiguration)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return array
	 */
	protected function getClientsConfiguration()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string[]
	 */
	public function getClientsName()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param string|null $clientName
	 * @return \Elastica\Client|null
	 */
	public function getElasticaClient($clientName)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		$index = $this->getDocumentsSearchIndex();
		return $index ? $index->getClient() : null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param string $clientName
	 * @return IndexDefinitionInterface[]
	 */
	public function getIndexesDefinition($clientName = null)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param string $clientName
	 * @param string $indexName
	 * @return null
	 */
	public function findIndexDefinitionByName($clientName, $indexName)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param IndexDefinitionInterface $indexDefinition
	 * @return \Elastica\Index|null
	 */
	public function deleteIndex($indexDefinition)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param IndexDefinitionInterface $indexDefinition
	 * @param integer|null $numberOfShards
	 * @param integer|null $numberOfReplicas
	 * @return null
	 */
	public function createIndex($indexDefinition, $numberOfShards = null, $numberOfReplicas = null)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param IndexDefinitionInterface $indexDefinition
	 * @param array $facetsMappings
	 * @return null
	 */
	public function updateFacetsMappings($indexDefinition, array $facetsMappings)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param string $category
	 * @param string $analysisLCID
	 * @param array $propertyFilters
	 * @return null
	 */
	public function getIndexByCategory($category, $analysisLCID, array $propertyFilters = null)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Website\Documents\Website|integer $website
	 * @param string $analysisLCID
	 * @return null
	 */
	public function getFulltextIndexByWebsite($website, $analysisLCID)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Website\Documents\Website|integer $website
	 * @param string $analysisLCID
	 * @return null
	 */
	public function getStoreIndexByWebsite($website, $analysisLCID)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 */
	public function startBulk()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param array $toIndex
	 */
	public function documentsBulkIndex($toIndex)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\AbstractModel $model
	 * @param integer $id
	 * @param string|null $LCID
	 */
	public function documentBulkIndex(\Change\Documents\AbstractModel $model, $id, $LCID)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 */
	public function sendBulk()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}
}