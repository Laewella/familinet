<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Index;

/**
 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Query\Documents
 * @name \Rbs\Elasticsearch\Index\QueryHelper
 */
class QueryHelper
{

	/**
	 * @deprecated since 1.8.0 use \Rbs\Elasticsearch\Query\Documents
	 * @param IndexDefinitionInterface $index
	 * @param IndexManager $indexManager
	 */
	public function __construct(IndexDefinitionInterface $index, IndexManager $indexManager)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return \Elastica\Query
	 */
	public function getSearchQuery()
	{
		$multiMatch = new \Elastica\Query\MatchAll();
		$bool = new \Elastica\Query\BoolQuery();
		$bool->addMust($multiMatch);
		$query = new \Elastica\Query($bool);
		$query->setSource(false);
		return $query;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Elastica\Query $query
	 * @return $this
	 */
	public function addHighlight(\Elastica\Query $query)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Catalog\Documents\ProductList $productList
	 * @param integer $availableInWarehouseId
	 * @param string $searchText
	 * @return \Elastica\Query
	 */
	public function getProductListQuery($productList = null, $availableInWarehouseId = null, $searchText = null)
	{
		$bool = new \Elastica\Query\BoolQuery();
		$multiMatch = new \Elastica\Query\MatchAll();
		$bool->addMust($multiMatch);
		return new \Elastica\Query($bool);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Elastica\Query $query
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $context
	 * @return $this
	 */
	public function addFacets(\Elastica\Query $query, array $facets, array $context = [])
	{
		$query->setSize(0);
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Elastica\Query $query
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return $this
	 */
	public function addFilteredFacets(\Elastica\Query $query, array $facets, array $facetFilters, array $context = [])
	{
		$query->setSize(0);
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param array $aggregations
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $context
	 * @return \Rbs\Elasticsearch\Facet\AggregationValues[]
	 */
	public function formatAggregations(array $aggregations, array $facets, array $context)
	{
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Elasticsearch\Facet\AggregationValues[] $aggregationValues
	 * @param array $facetFilters
	 * @param array $context
	 */
	public function applyFacetFilters(array $aggregationValues, array $facetFilters, array $context = [])
	{
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facets
	 * @param array $facetFilters
	 * @param array $context
	 * @return null
	 */
	public function getFacetsFilter(array $facets, array $facetFilters, array $context)
	{
		return null;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Elastica\Query $query
	 * @param string $sortBy
	 * @param array $context Expected key listId, listSortBy, webStoreId, billingAreaId, zone
	 */
	public function addSortArgs($query, $sortBy, array $context)
	{
	}
}