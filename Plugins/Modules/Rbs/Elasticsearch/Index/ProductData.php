<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Index;

/**
 * @deprecated since 1.8.0 with no replacement
 * @see \Rbs\Catalog\Events\ElasticsearchManager
 * @name \Rbs\Elasticsearch\Index\ProductData
 */
class ProductData
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 */
	public function __construct()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Catalog\Product\ProductManager $productManager
	 * @return $this
	 */
	public function setProductManager($productManager)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Price\PriceManager $priceManager
	 * @return $this
	 */
	public function setPriceManager($priceManager)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Stock\StockManager $stockManager
	 * @return $this
	 */
	public function setStockManager($stockManager)
	{
		return $this;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Catalog\Documents\Product $product
	 * @param array $documentData
	 * @return array
	 */
	public function addListItems($product, array $documentData)
	{
		return $documentData;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Catalog\Documents\Product $product
	 * @return integer[]
	 */
	public function getSkuIdsByProduct($product)
	{
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Catalog\Documents\Product $product
	 * @param integer[] $skuIds
	 * @param array $documentData
	 * @return array
	 */
	public function addPrices($product, array $skuIds, array $documentData)
	{
		return $documentData;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param array $prices
	 * @return array
	 */
	public function buildTimeLinePrices($prices)
	{
		return [];
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param array $timeLinePrices
	 * @param array $timeLine
	 * @return array
	 */
	public function expandPricesTimeLineWithZone(array $timeLinePrices, array $timeLine)
	{
		return $timeLinePrices;
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Rbs\Catalog\Documents\Product $product
	 * @param integer[] $skuIds
	 * @param array $documentData
	 * @return array
	 */
	public function addStock($product, array $skuIds, array $documentData)
	{
		return $documentData;
	}
} 