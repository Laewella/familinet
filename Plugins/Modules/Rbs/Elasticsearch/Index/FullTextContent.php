<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Elasticsearch\Index;

/**
 * @deprecated since 1.8.0 with no replacement
 * @name \Rbs\Elasticsearch\Index\FullTextContent
 */
class FullTextContent
{
	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultFullTextContent(\Change\Documents\Events\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onAttributesFullTextContent(\Change\Documents\Events\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
	}
}