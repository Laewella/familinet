<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Workflow\Setup;

/**
 * @name \Rbs\Workflow\Setup\CorrectionPublicationProcessWorkflow
 */
class CorrectionPublicationProcessWorkflow
{
	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function __construct($applicationServices)
	{
		$this->applicationServices = $applicationServices;
	}

	/**
	 * @param \Rbs\Workflow\Documents\Workflow $workflow
	 * @return \Rbs\Workflow\Documents\Workflow
	 */
	public function install(\Rbs\Workflow\Documents\Workflow $workflow)
	{
		$workflow->setStartTask('correctionPublicationProcess')->setActive(true);
		$workflow->setLabel('Correction publication Process');
		$workflow->setItemsData(null);

		$draft = $workflow->getNewPlace()->setName('Draft')->setType(\Rbs\Workflow\Std\Place::TYPE_START);
		$validation = $workflow->getNewPlace()->setName('Validation');
		$validContent = $workflow->getNewPlace()->setName('ValidContent');
		$valid = $workflow->getNewPlace()->setName('Valid');
		$publishable = $workflow->getNewPlace()->setName('Publishable');
		$filed = $workflow->getNewPlace()->setName('Filed')->setType(\Rbs\Workflow\Std\Place::TYPE_END);

		$requestValidation = $workflow->getNewTransition()->setName('Request Validation')->setTaskCode('requestValidation')
			->setTrigger(\Rbs\Workflow\Std\Transition::TRIGGER_USER)->setRole('Creator');

		$contentValidation = $workflow->getNewTransition()->setName('Content Validation')->setTaskCode('contentValidation')
			->setTrigger(\Rbs\Workflow\Std\Transition::TRIGGER_USER)->setRole('Editor')->setShowInDashboard(true);

		$publicationValidation =
			$workflow->getNewTransition()->setName('Publication Validation')->setTaskCode('publicationValidation')
				->setTrigger(\Rbs\Workflow\Std\Transition::TRIGGER_USER)->setRole('Publisher')->setShowInDashboard(true);

		$contentMerging = $workflow->getNewTransition()->setName('Content Merging')->setTaskCode('contentMerging')
			->setTrigger(\Rbs\Workflow\Std\Transition::TRIGGER_AUTO);

		$delayedContentMerging = $workflow->getNewTransition()->setName('Delayed Content Merging')->setTaskCode('contentMerging')
			->setTrigger(\Rbs\Workflow\Std\Transition::TRIGGER_TIME)->setTimeLimit('P10Y');

		$cancel = $workflow->getNewTransition()->setName('Cancel')->setTaskCode('cancel')
			->setTrigger(\Rbs\Workflow\Std\Transition::TRIGGER_USER)->setRole('Publisher');

		$workflow->getNewArc()->connect($draft, $requestValidation);
		$workflow->getNewArc()->connect($requestValidation, $validation);
		$workflow->getNewArc()->connect($validation, $contentValidation);
		$workflow->getNewArc()->connect($contentValidation, $draft)->setPreCondition('NO');
		$workflow->getNewArc()->connect($contentValidation, $validContent)
			->setPreCondition(\Rbs\Workflow\Std\Arc::PRECONDITION_DEFAULT);
		$workflow->getNewArc()->connect($validContent, $publicationValidation);

		$workflow->getNewArc()->connect($publicationValidation, $publishable)
			->setPreCondition(\Rbs\Workflow\Std\Arc::PRECONDITION_DEFAULT);
		$workflow->getNewArc()->connect($publishable, $contentMerging);
		$workflow->getNewArc()->connect($contentMerging, $filed);

		$workflow->getNewArc()->connect($publicationValidation, $valid)->setPreCondition('DELAYED');

		$workflow->getNewArc()->connect($valid, $delayedContentMerging)->setType(\Rbs\Workflow\Std\Arc::TYPE_IMPLICIT_OR_SPLIT);
		$workflow->getNewArc()->connect($delayedContentMerging, $filed);

		$workflow->getNewArc()->connect($valid, $cancel)->setType(\Rbs\Workflow\Std\Arc::TYPE_IMPLICIT_OR_SPLIT);
		$workflow->getNewArc()->connect($cancel, $draft);

		if ($workflow->isValid())
		{
			$workflow->save();
			return $workflow;
		}
		else
		{
			throw new \RuntimeException($workflow->getErrors());
		}
	}
}