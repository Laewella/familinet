<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Healthcheck\Probes;

/**
 * @name \Rbs\Healthcheck\Probes\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Healthcheck\Probes\JobErrorProbe())->run($event);
			(new \Rbs\Healthcheck\Probes\JobRunningProbe())->run($event);
		};
		$this->listeners[] = $events->attach('Check', $callback, 5);

		// Register probe to check probes executions
		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Healthcheck\Probes\CheckProbesProbe())->run($event);
		};
		$this->listeners[] = $events->attach('Check', $callback, -99);

	}
}