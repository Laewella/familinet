<?php
namespace Rbs\Healthcheck\Probes;

	/**
	 * Copyright (C) 2016 Proximis
	 *
	 * This Source Code Form is subject to the terms of the Mozilla Public
	 * License, v. 2.0. If a copy of the MPL was not distributed with this
	 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
	 */

/**
 * @name \Rbs\Healthcheck\Probes\AbstractProbe
 */
abstract class AbstractProbe
{

	const CRITICAL_LEVEL_WARNING = 'WARNING';
	const CRITICAL_LEVEL_ERROR = 'ERROR';
	const CRITICAL_LEVEL_FATAL = 'FATAL';

	/**
	 * The period between two check in seconds
	 * @var integer|null
	 */
	protected $periodicity = null;

	/**
	 * The criticality when error occurs
	 * @var integer|null
	 */
	protected $criticality = null;

	/**
	 * Uuid of instance
	 * @var string|null
	 */
	protected $uuid = null;

	/**
	 * Code of probe for identification
	 * @var string|null
	 */
	protected $code = null;

	/**
	 * Array of messages used to resolve issue
	 * @var string[]|null
	 */
	protected $messages = null;

	/**
	 * Date and Time at the begin
	 * @var \DateTime|null
	 */
	protected $beginDate = null;

	/**
	 * Date and Time at the end
	 * @var \DateTime|null
	 */
	protected $endDate = null;

	/**
	 * Size of memory used
	 * @var float|null
	 */
	protected $memoryUsed = null;

	/**
	 * Determine if probe is in error
	 * @var boolean
	 */
	protected $hasError = false;

	/**
	 * Array of error
	 * @var array[]|null
	 */
	protected $errors = [];

	/**
	 * Array of last execution error
	 * @var array[]
	 */
	protected $lastExecutionErrors = [];

	/**
	 * Array of last execution error code
	 * @var array[]
	 */
	protected $lastExecutionErrorsCode = [];

	/**
	 * @param \Change\Events\Event $event
	 */
	public function run($event)
	{
		$this->uuid = $event->getParam('uuid');

		if (!$this->validate())
		{
			$event->getApplication()->getLogging()->error('The probe ' . __CLASS__ . ' doesn\'t valid.');
			return;
		}

		// Register probe
		$registeredProbes = $event->getParam('registeredProbes');
		$registeredProbes[$this->code] = $this->periodicity;
		$event->setParam('registeredProbes', $registeredProbes);

		// Test if need to execute
		$lastExecutionProbes = $event->getParam('lastExecutionProbes');

		// If probes already been executed, check if needs to execute too
		$needExecution = true;
		if (array_key_exists($this->code, $lastExecutionProbes))
		{
			$currentExecutionDate = $event->getParam('currentExecutionDate');

			$lastDate = new \DateTime($lastExecutionProbes[$this->code]);
			$interval = new \DateInterval('PT' . $this->periodicity . 'S');
			$now = new \DateTime($currentExecutionDate);
			$now->sub($interval);

			if ($now < $lastDate)
			{
				$needExecution = false;
			}
		}

		if ($needExecution)
		{
			// Load last execution errors
			$storageManager = $event->getApplicationServices()->getStorageManager();
			$probeResultStorage = $storageManager->buildChangeURI('HealthCheck', '/Probes/')->toString();
			$this->loadLastExecutionErrors($probeResultStorage . $this->code . '.json');

			// Launch Probe execution
			$this->beginDate = new \DateTime();
			$memoryBefore = memory_get_usage();
			$this->execute($event);
			$memoryAfter = memory_get_usage();
			$this->memoryUsed = ($memoryAfter - $memoryBefore) / 1024;
			$this->endDate = new \DateTime();

			// Construct result
			$probes = $event->getParam('probes');
			$result = [
				'code' => $this->code,
				'criticality' => $this->criticality,
				'begin' => $this->beginDate->format('Y-m-d H:i:s'),
				'end' => $this->endDate->format('Y-m-d H:i:s'),
				'memoryUsed' => $this->memoryUsed,
				'error' => (count($this->errors) ? true : false),
				'lastErrors' => $this->lastExecutionErrors,
				'errors' => $this->errors,
				'errorCount' => count($this->errors)
			];

			$probes[] = $result;

			$event->setParam('probes', $probes);

			// Save file of execution errors
			\Change\Stdlib\FileUtils::write($probeResultStorage . $this->code . '.json', json_encode($this->errors));

			// Manage execution time
			$lastExecutionProbes[$this->code] = $result['begin'];
			$event->setParam('lastExecutionProbes', $lastExecutionProbes);
		}
	}

	/**
	 * @return boolean
	 */
	protected function validate()
	{
		return $this->code !== null && $this->criticality !== null && $this->periodicity !== null && $this->uuid !== null;
	}

	/**
	 * @param $code
	 * @param $message
	 */
	protected function addError($code, $message)
	{
		if ($this->errors === null)
		{
			$this->errors = [];
		}

		if (isset($this->lastExecutionErrorsCode[$code]))
		{
			$uniqueId = $this->lastExecutionErrorsCode[$code];
		}
		else
		{
			$date = new \DateTime();
			$uniqueId = $this->uuid . '-' . $code . '-' . $date->getTimestamp();
		}

		$this->errors[] = ['code' => $code, 'message' => $message, 'uniqueId' => $uniqueId];
	}

	/**
	 * @param $filePath
	 */
	protected function loadLastExecutionErrors($filePath)
	{
		if (file_exists($filePath))
		{
			$lastExecutionErrorsJson = \Change\Stdlib\FileUtils::read($filePath);
			$this->lastExecutionErrors = json_decode($lastExecutionErrorsJson, true);

			foreach ($this->lastExecutionErrors as $e)
			{
				$this->lastExecutionErrorsCode[$e['code']] = $e['uniqueId'];
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected abstract function execute($event);

}