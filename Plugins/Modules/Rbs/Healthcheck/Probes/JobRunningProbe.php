<?php
namespace Rbs\Healthcheck\Probes;

	/**
	 * Copyright (C) 2016 Proximis
	 *
	 * This Source Code Form is subject to the terms of the Mozilla Public
	 * License, v. 2.0. If a copy of the MPL was not distributed with this
	 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
	 */

/**
 * @name \Rbs\Healthcheck\Probes\JobRunningProbe
 */
class JobRunningProbe extends AbstractProbe
{

	public function __construct()
	{
		$this->code = 'JobRunningProbe';
		$this->criticality = self::CRITICAL_LEVEL_ERROR;
		$this->periodicity = 300;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function execute($event)
	{
		$dbProvider = $event->getApplicationServices()->getDbProvider();

		$date = new \DateTime();
		$interval = new \DateInterval('PT120M');
		$date->sub($interval);

		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('id'))
			->from('change_job');
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('status'), $fb->parameter('status')),
			$fb->lt($fb->column('start_date'), $fb->dateTimeParameter('date')))
		);
		$sq = $qb->query();

		$sq->bindParameter('status', \Change\Job\JobInterface::STATUS_RUNNING);
		$sq->bindParameter('date', $date);

		$jobIds = $sq->getResults($sq->getRowsConverter()->addIntCol('id'));
		$countRunning = count($jobIds);

		if ($countRunning > 0)
		{
			$jobManager = $event->getApplicationServices()->getJobManager();

			// Get jobs information
			foreach ($jobIds as $jobId)
			{
				$job = $jobManager->getJob($jobId);
				$this->addError('JobRunningProbe:' . $jobId,
					'Job ' . $jobId . '/' . $job->getName() . ' started at ' . $job->getStartDate()->format('Y-m-d H:i:s'));
			}
		}
	}
}