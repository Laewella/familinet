<?php
namespace Rbs\Healthcheck\Probes;

/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @name \Rbs\Healthcheck\Probes\JobErrorProbe
 */
class JobErrorProbe extends AbstractProbe
{

	public function __construct()
	{
		$this->code = 'JobErrorProbe';
		$this->criticality = self::CRITICAL_LEVEL_ERROR;
		$this->periodicity = 300;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function execute($event)
	{
		$jobManager = $event->getApplicationServices()->getJobManager();
		$countFailed = $jobManager->getCountJobIds(\Change\Job\JobInterface::STATUS_FAILED);

		if ($countFailed > 0)
		{

			// Get ids of concerned jobs
			$jobIds = $jobManager->getJobIds(\Change\Job\JobInterface::STATUS_FAILED);

			// Get jobs information
			foreach($jobIds as $jobId)
			{	
				$job = $jobManager->getJob($jobId);
				$this->addError('JobErrorProbe:' . $jobId,
					'Job ' . $jobId . '/' . $job->getName() . ' started at ' . $job->getStartDate()->format('Y-m-d H:i:s'));
			}
		}
	}
}