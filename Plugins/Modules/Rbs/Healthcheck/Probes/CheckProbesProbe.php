<?php
namespace Rbs\Healthcheck\Probes;

/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @name \Rbs\Healthcheck\Probes\CheckProbesProbe
 */
class CheckProbesProbe extends AbstractProbe
{

	public function __construct()
	{
		$this->code = 'CheckProbesProbe';
		$this->criticality = self::CRITICAL_LEVEL_FATAL;
		$this->periodicity = 1;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function execute($event)
	{
		$currentExecutionDate = $event->getParam('currentExecutionDate');

		// Get registered probes
		$registeredProbes = $event->getParam('registeredProbes');

		// Get last execution probes
		$lastExecutionProbes = $event->getParam('lastExecutionProbes');

		// Check that probes are correctly executed
		$periodDate = [];
		foreach ($registeredProbes as $probeKey => $probePeriodicity)
		{
			// Except self
			if ($probeKey == 'CheckProbesProbe')
			{
				continue;
			}

			if (!isset($periodDate[$probePeriodicity . '']))
			{
				$interval = new \DateInterval('PT' . $probePeriodicity . 'S');
				$date = new \DateTime($currentExecutionDate);
				$date->sub($interval);
				$periodDate[$probePeriodicity . ''] = $date;
			}

			if (!isset($lastExecutionProbes[$probeKey]))
			{
				// Error, registered probe has no execution
				$this->addError('CheckProbesProbe:' . $probeKey, 'Probe ' . $probeKey . ' is registered but never executed.');
			}
			else
			{
				$lastExecutionDate = new \DateTime($lastExecutionProbes[$probeKey]);
				if ($periodDate[$probePeriodicity . ''] > $lastExecutionDate)
				{
					// Error, probe need execution
					$this->addError('CheckProbesProbe:' . $probeKey, 'Probe ' . $probeKey . ' need execution.');
				}
			}
		}
	}
}