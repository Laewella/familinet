<?php
namespace Rbs\Healthcheck\Setup;

/**
 * @name \Rbs\Healthcheck\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$storage = $configuration->getEntry('Change/Storage/HealthCheck', []);
		$storage = array_merge([
			'class' => \Change\Storage\Engines\LocalStorage::class,
			'basePath' => 'App/Storage/healthcheck',
		], $storage);
		$configuration->addPersistentEntry('Change/Storage/HealthCheck', $storage);

		$configuration->addPersistentEntry('Rbs/Healthcheck/Events/Healthcheck/Rbs_Healthcheck', \Rbs\Healthcheck\Probes\Listeners::class);
	}
}
