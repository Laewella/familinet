<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
require_once(dirname(dirname(dirname(__DIR__))) . '/../../Change/Application.php');

$application = new \Change\Application();
$application->useSession(false);
$application->start();

/**
 * Class HealthCheck
 */
class HealthCheck implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'HealthCheck';
	const EVENT_CHECK = 'Check';

	/**
	 * Date and Time at the begin
	 * @var \DateTime|null
	 */
	protected $beginDate = null;

	/**
	 * Date and Time at the end
	 * @var \DateTime|null
	 */
	protected $endDate = null;

	/**
	 * Size of memory used at begin
	 * @var float|null
	 */
	protected $memoryUsedAtBegin = null;

	/**
	 * Size of memory used at end
	 * @var float|null
	 */
	protected $memoryUsedAtEnd = null;

	/**
	 * Size of memory used
	 * @var float|null
	 */
	protected $memoryUsed = null;

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Healthcheck/Events/Healthcheck');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$this->eventManager->attach(self::EVENT_CHECK, function ($event) { $this->onStart($event); }, 100);
		$this->eventManager->attach(self::EVENT_CHECK, function ($event) { $this->onEnd($event); }, -10);

		// When all treatment finished, handle notifications
		$this->eventManager->attach(self::EVENT_CHECK, function ($event) { $this->handleNotifications($event); }, -20);
		$this->eventManager->attach(self::EVENT_CHECK, function ($event) { $this->saveEventsInES($event); }, -20);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onStart(\Change\Events\Event $event)
	{
		// Load lastExecutionProbes
		$storageManager = $event->getApplicationServices()->getStorageManager();
		$event->getApplicationServices()->getDbProvider()->setReadOnly(true);
		$rootStorage = $storageManager->buildChangeURI('HealthCheck', '/')->toString();
		$filePath = $rootStorage . '/lastExecutionProbes.json';

		if (file_exists($filePath))
		{
			$lastExecutionProbesJson = \Change\Stdlib\FileUtils::read($filePath);
			$event->setParam('lastExecutionProbes', json_decode($lastExecutionProbesJson, true));
		}

		$this->beginDate = new \DateTime();
		$event->setParam('currentExecutionDate', $this->beginDate->format('Y-m-d H:i:s'));

		$this->memoryUsedAtBegin = memory_get_usage();
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onEnd(\Change\Events\Event $event)
	{
		$this->memoryUsedAtEnd = memory_get_usage();
		$this->endDate = new \DateTime();

		// Write Files

		// Write File of global last execution
		// Store the list of probes and date of execution
		$storageManager = $event->getApplicationServices()->getStorageManager();
		$rootStorage = $storageManager->buildChangeURI('HealthCheck', '/')->toString();
		$lastExecutionProbes = $event->getParam('lastExecutionProbes');
		\Change\Stdlib\FileUtils::write($rootStorage . '/lastExecutionProbes.json', json_encode($lastExecutionProbes));

		// Construct Data for the global result of executed probes
		$probes = $event->getParam('probes');
		$uuid = $event->getParam('uuid');
		$result = [
			'uuid' => $uuid,
			'begin' => $this->beginDate->format('Y-m-d H:i:s'),
			'end' => $this->endDate->format('Y-m-d H:i:s'),
			'memoryUsed' => ($this->memoryUsedAtBegin - $this->memoryUsedAtEnd) / 1024,
			'probes' => $probes,
			'probesCount' => count($probes)
		];

		// Write File of global result in a directory corresponding at year/month/day
		$currentExecutionDate = new \DateTime($event->getParam('currentExecutionDate'));
		$currentExecutionYear = $currentExecutionDate->format('Y');
		$currentExecutionMonth = $currentExecutionDate->format('m');
		$currentExecutionDay = $currentExecutionDate->format('d');
		$currentExecutionTime = $currentExecutionDate->format('His');

		$resultStorage = $storageManager->buildChangeURI('HealthCheck', '/' . $currentExecutionYear . '/' . $currentExecutionMonth . '/'
			. $currentExecutionDay . '/')->toString();
		// \Change\Stdlib\FileUtils::write($resultStorage . $uuid . '_' . $currentExecutionTime . '.json', json_encode($result));

		// Prepare list of new and closed monitoring events
		$monitoringEvents = [];
		foreach ($probes as $probe)
		{
			// Analyse error of probe to generate events
			$m = $this->getMonitoringEvent($probe['lastErrors'], $probe['errors']);
			$newEvents = $m['new'];
			$closeEvents = $m['closed'];

			if (count($newEvents) > 0 || count($closeEvents) > 0)
			{
				if (!isset($monitoringEvents[$probe['criticality']]))
				{
					$monitoringEvents[$probe['criticality']] = [];
				}

				$monitoringEvents[$probe['criticality']][] = ['probe' => $probe, 'new' => $newEvents, 'closed' => $closeEvents];
			}
		}
		$event->setParam('monitoringEvents', $monitoringEvents);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function handleNotifications(\Change\Events\Event $event)
	{
		$monitoringEvents = $event->getParam('monitoringEvents');
		if (count($monitoringEvents) > 0)
		{
			foreach ($monitoringEvents as $criticality => $probeEvents)
			{
				$emails = $this->application->getConfiguration('Rbs/Healthcheck/Contacts/' . $criticality);
				$uuid = $event->getParam('uuid');

				$all = 0;
				$content = ['Notification for platform : <b>' . $uuid . '</b>'];
				$content[] = '';
				$content[] = 'Level of criticality : <b>' . $criticality . '</b>';

				foreach ($probeEvents as $probeEvent)
				{
					$countNew = count($probeEvent['new']);
					$countClosed = count($probeEvent['closed']);
					$allForProbe = $countNew + $countClosed;
					$all += $allForProbe;

					$content[] = '';
					$content[] =
						'<u>The probe <b>' . $probeEvent['probe']['code'] . '</b> has monitoring notification' . ($allForProbe > 1 ? 's</u>' :
							'</u>');

					if ($countNew > 0)
					{
						$content[] = '<b>' . $countNew . ' new error' . ($countNew > 1 ? 's</b>' : '</b>');
						foreach ($probeEvent['new'] as $newE)
						{
							$content[] = $newE['code'] . ' -- ' . $newE['message'];
						}
					}

					if ($countClosed > 0)
					{
						if ($countNew > 0)
						{
							$content[] = '';
						}

						$content[] = '<b>' . $countClosed . ' closed error' . ($countClosed > 1 ? 's</b>' : '</b>');
						foreach ($probeEvent['closed'] as $closedE)
						{
							$content[] = $closedE['code'] . ' -- ' . $closedE['message'];
						}
					}
				}

				$subject = $all . ' monitoring notification' . ($all > 1 ? 's' : '') . ' ' . $criticality . ' for platform ' . $uuid;

				// Send email
				$mailManager = $event->getApplicationServices()->getMailManager();
				$message = $mailManager->prepareMessage(['monitoring-omn@proximis.com'], $emails, $subject, implode('<br/>', $content));
				$mailManager->send($message);
			}
		}
	}

	protected function saveEventsInES(\Change\Events\Event $event)
	{
		$esUrl = $this->application->getConfiguration('Rbs/Healthcheck/Elasticsearch/url');
		if (!empty($esUrl))
		{
			$monitoringEvents = $event->getParam('monitoringEvents');
			if (count($monitoringEvents) > 0)
			{
				$errorBulk = '';
				$date = new \DateTime();
				$timestamp = $date->getTimestamp() * 1000;

				foreach ($monitoringEvents as $criticality => $probeEvents)
				{
					$uuid = $event->getParam('uuid');

					foreach ($probeEvents as $probeEvent)
					{
						foreach ($probeEvent['new'] as $newE)
						{
							$errorBulk .= json_encode(['create' => ["_index" => "monitoring", "_type" => "alerts", "_id" => $newE['uniqueId']]]);
							$errorBulk .= "\n";
							$errorBulk .= json_encode(['uuid' => $uuid, 'severity' => $criticality, 'probeCode' => $probeEvent['probe']['code'],
								'errorCode' => $newE['code'], 'message' => $newE['message'], 'closed' => false,
								'date' => ['opening' => $timestamp, 'closing' => null]]);
							$errorBulk .= "\n";
						}

						foreach ($probeEvent['closed'] as $closedE)
						{
							$errorBulk .= json_encode(['update' => ["_index" => "monitoring", "_type" => "alerts", "_id" => $closedE['uniqueId']]]);
							$errorBulk .= "\n";
							$errorBulk .= json_encode(['doc' => ['closed' => true, 'date' => ['closing' => $timestamp]]]);
							$errorBulk .= "\n";
						}
					}
				}

				$request = new \Zend\Http\Request();
				$request->setUri($esUrl. '_bulk');
				$request->setMethod('POST');
				$request->setContent($errorBulk);

				$client = new \Zend\Http\Client();
				$response = $client->send($request);

				if (!$response->isSuccess())
				{
					$event->getApplication()->getLogging()->error('HealthCheck Monitoring: Impossible to send alerts in elasticsearch. Status: ' . $response->getStatusCode());
				}
			}
		}
	}

	/**
	 * @param $lastErrors
	 * @param $currentErrors
	 * @return array
	 */
	protected function getMonitoringEvent($lastErrors, $currentErrors)
	{
		// Get new errors
		$newErrors = array_diff_assoc($currentErrors, $lastErrors);

		// Get closed errors
		$closedErrors = array_diff_assoc($lastErrors, $currentErrors);

		return ['new' => $newErrors, 'closed' => $closedErrors];
	}

	public function execute($argv)
	{
		if ($this->application->getConfiguration('Rbs/Healthcheck/stop'))
		{
			return;
		}

		$uuid = $this->application->getConfiguration('Change/Application/uuid');
		if (empty($uuid))
		{
			$this->application->getLogging()->error('Application uuid (Change/Application/uuid) must be defined in configuration.');
			return;
		}

		$warning = $this->application->getConfiguration('Rbs/Healthcheck/Contacts/WARNING');
		if (empty($warning))
		{
			$this->application->getLogging()
				->error('Email for warning notification must be defined in configuration. (Rbs/Healthcheck/Contacts/WARNING)');
			return;
		}

		$error = $this->application->getConfiguration('Rbs/Healthcheck/Contacts/ERROR');
		if (empty($error))
		{
			$this->application->getLogging()
				->error('Email for error notification must be defined in configuration. (Rbs/Healthcheck/Contacts/ERROR)');
			return;
		}

		$fatal = $this->application->getConfiguration('Rbs/Healthcheck/Contacts/FATAL');
		if (empty($fatal))
		{
			$this->application->getLogging()
				->error('Email for fatal notification must be defined in configuration. (Rbs/Healthcheck/Contacts/FATAL)');
			return;
		}

		// Get last check
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs(['uuid' => $uuid, 'registeredProbes' => [], 'probes' => [], 'lastExecutionProbes' => [],
			'currentExecutionDate' => '', 'monitoringEvents' => []]);
		$this->getEventManager()->trigger(self::EVENT_CHECK, $this, $eventArgs);
	}

}

$healthCheck = new HealthCheck();
$healthCheck->setApplication($application);
$healthCheck->execute($argv);