(function(__change) {
	"use strict";
	var app = angular.module('RbsChangeApp');

	app.provider('RbsGeo.GoogleMapService', GoogleMapServiceProvider);
	function GoogleMapServiceProvider() {

		this.googleConfig = (__change.Rbs_Geo_Config && __change.Rbs_Geo_Config.Google) || {};
		this.googleConfig.transport = 'auto';

		this.configure = function(options) {
			angular.extend(this.googleConfig, options);
		};

		function isGoogleMapsLoaded() {
			return angular.isDefined(window.google) && angular.isDefined(window.google.maps);
		}

		function getScriptUrl(options) {
			if (options.china) {
				return 'http://maps.google.cn/maps/api/js?';
			}
			else {
				if (options.transport === 'auto') {
					return '//maps.googleapis.com/maps/api/js?';
				}
				else {
					return options.transport + '://maps.googleapis.com/maps/api/js?';
				}
			}
		}

		function includeScript(options) {
			var query = ['v=3', 'libraries=places'];
			if (options.client) {
				query.push('client=' + options.client)
			}
			else if (options.APIKey) {
				query.push('key=' + options.APIKey)
			}
			else {
				console.error('GoogleMap authentication not provided');
				return null;
			}
			if (options.callback) {
				query.push('callback=' + options.callback)
			}
			query = query.join('&');
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.async = true;
			script.src = getScriptUrl(options) + query;
			return document.head.appendChild(script);
		}

		var _this = this;

		this.$get = ['$rootScope', '$q', function($rootScope, $q) {

			/**
			 * @description
			 * Check if the service has a valid configuration
			 *
			 * @returns {boolean}
			 */
			function valid() {
				return !!(_this.googleConfig.client || _this.googleConfig.APIKey);
			}

			/**
			 * @description
			 * get the google.maps object
			 *
			 * @returns {Promise}
			 */
			function maps() {
				var options = _this.googleConfig;
				var deferred, randomFn;
				deferred = $q.defer();
				if (isGoogleMapsLoaded()) {
					deferred.resolve(window.google.maps);
					return deferred.promise;
				}

				randomFn = options.callback = 'onGoogleMapsReady' + Math.round(Math.random() * 1000);
				window[randomFn] = function() {
					delete window[randomFn];
					deferred.resolve(window.google.maps);
				};

				if (window.navigator.connection && window.Connection
					&& window.navigator.connection.type === window.Connection.NONE && !options.preventLoad) {
					document.addEventListener('online', function() {
						if (!isGoogleMapsLoaded()) {
							return includeScript(options);
						}
					});
				}
				else if (!options.preventLoad) {
					includeScript(options);
				}
				options.randomFn = randomFn;
				return deferred.promise;
			}

			// Public API
			return {
				maps: maps,
				valid: valid
			};
		}];
	}

	app.provider('RbsGeo.LeafletMapService', LeafletMapServiceProvider);
	function LeafletMapServiceProvider() {
		this.leafletConfig = {};

		this.configure = function(options) {
			angular.extend(this.leafletConfig, options);
		};

		function isLeafletMapLoaded() {
			return angular.isDefined(window.L);
		}

		function includeScript(options) {
			var assetBasePath = __change.navigationContext.assetBasePath || '/Assets/';
			var link = document.createElement('link');
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = assetBasePath + 'Theme/Rbs/Base/lib/leaflet-1.0.2/leaflet.css';
			link.media = 'all';
			document.head.appendChild(link);

			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.async = true;
			script.onload = options.callback;
			script.src = assetBasePath + 'Theme/Rbs/Base/lib/leaflet-1.0.2/leaflet.js';
			return document.head.appendChild(script);
		}

		var _this = this;

		this.$get = ['$rootScope', '$q', function($rootScope, $q) {

			/**
			 * @description
			 * get the L object
			 *
			 * @returns {Promise}
			 */
			function maps() {
				var options = _this.leafletConfig;
				var deferred, randomFn;
				deferred = $q.defer();
				if (isLeafletMapLoaded()) {
					deferred.resolve(window.L);
					return deferred.promise;
				}

				options.randomFn = randomFn = 'onLeafletMapsReady' + Math.round(Math.random() * 1000);
				options.callback = window[randomFn] = function() {
					delete window[randomFn];
					deferred.resolve(window.L);
				};

				if (window.navigator.connection && window.Connection
					&& window.navigator.connection.type === window.Connection.NONE && !options.preventLoad) {
					document.addEventListener('online', function() {
						if (!isLeafletMapLoaded()) {
							return includeScript(options);
						}
					});
				}
				else if (!options.preventLoad) {
					includeScript(options);
				}
				return deferred.promise;
			}

			/**
			 * @returns {string}
			 */
			function defaultTileLayerName() {
				return '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
			}

			// Public API
			return {
				maps: maps,
				defaultTileLayerName : defaultTileLayerName
			};
		}];
	}
})(window.__change);
