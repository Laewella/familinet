(function() {
	"use strict";
	var app = angular.module('RbsChangeApp');

	app.provider('RbsGeo.PhoneService', PhoneProvider);

	function PhoneProvider() {
		var regionNumberConfig = null;

		var parsedPhoneNumbers = {};

		this.$get = ['$rootScope', '$q', 'RbsChange.AjaxAPI',
			function($rootScope, $q, AjaxAPI) {

				function getRegionNumberConfig() {
					var deferred = $q.defer();
					if (regionNumberConfig !== null) {
						if (angular.isArray(regionNumberConfig)) {
							deferred.resolve(regionNumberConfig);
						}
						else {
							return regionNumberConfig.promise;
						}
					}
					else {
						regionNumberConfig = deferred;
						AjaxAPI.getData('Rbs/Geo/Phone/NumberConfiguration/').then(
							function(result) {
								regionNumberConfig = result.data.items;
								deferred.resolve(regionNumberConfig);
							},
							function(result) {
								console.error('getRegionNumberConfig', result);
								regionNumberConfig = [];
								deferred.resolve(regionNumberConfig);
							}
						);
					}
					return deferred.promise;
				}

				function parseNumber(phoneNumber, regionCode) {
					var deferred = $q.defer();
					if (!angular.isString(phoneNumber) || (phoneNumber.length === 0)) {
						//Empty number
						deferred.resolve(null);
					}
					else if (phoneNumber.length < 5 || !regionCode) {
						deferred.reject('Too short number');
					}
					else if (!angular.isString(regionCode) || regionCode.length != 2) {
						deferred.reject('Invalid region code');
					}
					else {
						var key = phoneNumber + '.' + regionCode;
						if (parsedPhoneNumbers.hasOwnProperty(key)) {
							var parsedNumber = parsedPhoneNumbers[key];
							if (parsedNumber) {
								if (parsedNumber.promise) {
									return parsedNumber.promise;
								}
								deferred.resolve(parsedNumber);
							}
							else {
								deferred.reject('Invalid number');
							}
						}
						else {
							parsedPhoneNumbers[key] = deferred;
							AjaxAPI.getData('Rbs/Geo/Phone/ParsePhoneNumber',
								{ number: phoneNumber, regionCode: regionCode }).then(
								function(result) {
									var common = result.data.dataSets.common;
									parsedPhoneNumbers[key] = common;
									parsedPhoneNumbers[common.national + '.' + common.regionCode] = common;
									deferred.resolve(common);
								},
								function() {
									parsedPhoneNumbers[key] = false;
									deferred.reject('Invalid number');
								}
							);
						}
					}
					return deferred.promise;
				}

				function parseMobileNumber(phoneNumber, regionCode) {
					return parseNumber(phoneNumber, regionCode).then(function(parsedNumber) {
						if (!parsedNumber || parsedNumber.type == 'MOBILE' || parsedNumber.type == 'FIXED_LINE_OR_MOBILE') {
							return parsedNumber;
						}
						return $q.reject('Invalid mobile number');
					});
				}

				function parseFixedLineNumber(phoneNumber, regionCode) {
					return parseNumber(phoneNumber, regionCode).then(function(parsedNumber) {
						if (!parsedNumber || parsedNumber.type == 'FIXED_LINE' || parsedNumber.type == 'FIXED_LINE_OR_MOBILE') {
							return parsedNumber;
						}
						return $q.reject('Invalid fixed line number');
					});
				}

				// Public API
				return {
					getRegionNumberConfig: getRegionNumberConfig,
					parseNumber: parseNumber,
					parseMobileNumber: parseMobileNumber,
					parseFixedLineNumber: parseFixedLineNumber
				};
			}
		]
	}

	app.directive('rbsGeoInputPhoneNumber', ['RbsChange.AjaxAPI', '$q', 'RbsGeo.PhoneService', '$timeout', rbsGeoInputPhoneNumber]);
	function rbsGeoInputPhoneNumber(AjaxAPI, $q, PhoneService, $timeout) {

		var defaultCode = AjaxAPI.getLCID().substr(3);

		return {
			restrict: 'A',
			templateUrl: '/rbs-geo-input-phone-number.twig',
			require: 'ngModel',
			scope: {
				required: "=",
				mobileOnly: "@",
				fixedLineOnly: "@"
			},
			controller: ['$scope', function(scope) {
				if (scope.mobileOnly) {
					scope.numberType = 'MOBILE';
				}
				else if (scope.fixedLineOnly) {
					scope.numberType = 'FIXED_LINE';
				}
				else {
					scope.numberType = 'FIXED_LINE_OR_MOBILE';
				}

				scope.currentCfg = null;
				scope.regionNumberConfig = [];

				PhoneService.getRegionNumberConfig().then(
					function(regionNumberConfig) {
						scope.regionNumberConfig = regionNumberConfig;
						angular.forEach(regionNumberConfig, function(cfg) {
							if (scope.currentCfg === null || cfg[0] === defaultCode) {
								scope.currentCfg = cfg;
							}
						});
					}
				);
			}],
			link: function(scope, element, attributes, ngModel) {
				scope.parsedNumber = null;
				scope.name = attributes.name;

				scope.getCurrentPlaceHolder = function() {
					if (scope.currentCfg) {
						if (scope.mobileOnly) {
							return scope.currentCfg[4];
						}
						else if (scope.fixedLineOnly) {
							return scope.currentCfg[3];
						}
						else {
							return scope.currentCfg[3] || scope.currentCfg[4] || '';
						}
					}
					return '';
				};

				ngModel.$render = function() {
					scope.rawNumber = ngModel.$viewValue;
					if (scope.rawNumber) {

						PhoneService.parseNumber(scope.rawNumber, defaultCode).then(
							function(parsedNumber) {

								scope.parsedNumber = parsedNumber;
								if (parsedNumber) {
									scope.rawNumber = parsedNumber.national;
									angular.forEach(scope.regionNumberConfig, function(cfg) {
										if (cfg[0] === parsedNumber.regionCode) {
											scope.currentCfg = cfg;
										}
									});
								}
							},
							function(error) {
								scope.parsedNumber = null;
							}
						);
					}
				};

				scope.setCurrentCfg = function(cfg) {
					scope.currentCfg = cfg;
					ngModel.$setViewValue(scope.rawNumber);
					ngModel.$validate();
				};

				var delayedPromise = null;

				scope.validateRawNumber = function(delayed) {
					if (scope.rawNumber) {
						ngModel.$setTouched();
					}

					if (delayedPromise) {
						$timeout.cancel(delayedPromise);
						delayedPromise = null;
					}

					if (delayed) {
						delayedPromise = $timeout(function() {
							ngModel.$setViewValue(scope.rawNumber);
						}, 500)
					}
					else {
						ngModel.$setViewValue(scope.rawNumber);
					}
				};

				var errorCallback = function(error) {
					scope.parsedNumber = null;
					return $q.reject(error);
				};

				var successCallback = function(parsedNumber) {
					scope.parsedNumber = parsedNumber;
					if (parsedNumber) {
						scope.rawNumber = parsedNumber.national;
						ngModel.$setViewValue(parsedNumber.E164);
					}
					return parsedNumber;
				};

				ngModel.$asyncValidators.mobileNumber = function(modelValue, viewValue) {
					var regionCode = scope.currentCfg ? scope.currentCfg[0] : defaultCode;
					if (scope.mobileOnly) {
						return PhoneService.parseMobileNumber(viewValue, regionCode).then(successCallback, errorCallback);
					}
					else if (scope.fixedLineOnly) {
						return PhoneService.parseFixedLineNumber(viewValue, regionCode).then(successCallback, errorCallback);
					}
					else {
						return PhoneService.parseNumber(viewValue, regionCode).then(successCallback, errorCallback);
					}
				};
			}
		}
	}

	app.directive('rbsGeoPhoneFormatter', ['RbsChange.AjaxAPI', 'RbsGeo.PhoneService', rbsGeoPhoneFormatter]);
	function rbsGeoPhoneFormatter(AjaxAPI, PhoneService) {
		var defaultCode = AjaxAPI.getLCID().substr(3);
		return {
			restrict: 'A',
			templateUrl: '/rbs-geo-phone-formatter.twig',
			scope: {
				number: "=",
				regionCode: "="
			},
			link: function(scope, element, attributes) {
				scope.addLink = attributes.addLink === 'true';

				scope.$watch('number', function(number) {
					if (number) {
						PhoneService.parseNumber(number, scope.regionCode || defaultCode).then(
							function(parsedNumber) {
								if (parsedNumber) {
									scope.data = parsedNumber;
									parsedNumber.flagCountry = parsedNumber.regionCode.toLowerCase();
									parsedNumber.country = parsedNumber.regionCode;
									parsedNumber.class = (parsedNumber.type === 'MOBILE' || parsedNumber.type === 'FIXED_LINE_OR_MOBILE')
										? 'phone' : 'earphone';

									PhoneService.getRegionNumberConfig().then(
										function(regionNumberConfig) {
											angular.forEach(regionNumberConfig, function(cfg) {
												if (cfg[0] === scope.data.regionCode) {
													scope.data.country = cfg[2];
												}
											});
										}
									)
								}
								else {
									scope.data = { empty: true }
								}
							},
							function(error) {
								scope.data = { invalid: true, error: error }
							}
						)
					}
					else {
						scope.data = { empty: true }
					}
				})
			}
		}
	}
})();