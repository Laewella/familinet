<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Geo;

use Rbs\Geo\Address\AddressInterface;

/**
 * @name \Rbs\Geo\GeoManager
 */
class GeoManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'Rbs_Geo_GeoManager';
	const EVENT_COUNTRIES_BY_ZONE_CODE = 'getCountriesByZoneCode';
	const EVENT_FORMAT_ADDRESS = 'formatAddress';

	/**
	 * @var \Rbs\Geo\Address\AddressFilters
	 */
	protected $addressFilters;

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('getDefaultForNames', function ($event) { $this->onGetDefaultForNames($event); }, 5);

		$eventManager->attach(static::EVENT_COUNTRIES_BY_ZONE_CODE, function ($event) { $this->onGetCountriesByZoneCode($event); }, 5);

		$eventManager->attach(static::EVENT_FORMAT_ADDRESS, function ($event) { $this->onFormatAddress($event); }, 10);
		$eventManager->attach(static::EVENT_FORMAT_ADDRESS, function ($event) { $this->onFormatFieldsByLayout($event); }, 5);

		$eventManager->attach('getCountryByCode', function ($event) { $this->onGetCountryByCode($event); }, 5);
		$eventManager->attach('getCountryByIsoCode', function ($event) { $this->onGetCountryByIsoCode($event); }, 5);

		$eventManager->attach('getAddresses', function ($event) { $this->onGetAddresses($event); }, 5);
		$eventManager->attach('deleteAddress', function ($event) { $this->onDeleteAddress($event); }, 5);

		$eventManager->attach('validateAddress', function ($event) { $this->onValidateAddress($event); }, 5);
		$eventManager->attach('addAddress', function ($event) { $this->onDefaultAddAddress($event); }, 5);

		$eventManager->attach('updateAddress', function ($event) { $this->onUpdateAddress($event); }, 5);
		$eventManager->attach('setDefaultAddress', function ($event) { $this->onSetDefaultAddress($event); }, 5);
		$eventManager->attach('getDefaultAddress', function ($event) { $this->onGetDefaultAddress($event); }, 5);
		$eventManager->attach('getZoneByCode', function ($event) { $this->onGetZoneByCode($event); }, 5);
		$eventManager->attach('getAddressFieldsData', function ($event) { $this->onGetAddressFieldsData($event); }, 5);

		$eventManager->attach('getCoordinatesByAddress', function ($event) { $this->onGetCoordinatesByAddress($event); }, 5);
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Geo/Events/GeoManager');
	}

	/**
	 * @api
	 * @param string|string[]|null $zoneCode
	 * @return \Rbs\Geo\Documents\Country[]
	 */
	public function getCountriesByZoneCode($zoneCode)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['zoneCode' => $zoneCode]);

		$this->getEventManager()->trigger('getCountriesByZoneCode', $this, $args);
		if (isset($args['countries']) && is_array($args['countries']))
		{
			return $args['countries'];
		}
		return [];
	}

	/**
	 * Event Params: zoneCode
	 * @param \Change\Events\Event $event
	 */
	protected function onGetCountriesByZoneCode(\Change\Events\Event $event)
	{
		$zoneCode = $event->getParam('zoneCode');
		$documentManager = $event->getApplicationServices()->getDocumentManager();

		if ($zoneCode && is_array($zoneCode) && count($zoneCode) == 0)
		{
			$zoneCode = null;
		}

		// If a zone code is specified, look for a country having this code or a country with a zone on it having this code.
		if ($zoneCode)
		{
			$zoneCode = (array)$zoneCode;
			$queryZone = $documentManager->getNewQuery('Rbs_Geo_Zone');
			$queryZone->andPredicates($queryZone->in($queryZone->getColumn('code'), $zoneCode));
			$qZone = $queryZone->dbQueryBuilder()->query();
			$countriesIdZone = $qZone->getResults($qZone->getRowsConverter()->addIntCol('country'));

			$query = $documentManager->getNewQuery('Rbs_Geo_Country');
			$query->andPredicates($query->activated());
			if (count($countriesIdZone) > 0)
			{
				$query->andPredicates(
					$query->getPredicateBuilder()->logicOr(
						$query->in('code', $zoneCode),
						$query->in('id', $countriesIdZone)
					)
				);
			}
			else
			{
				$query->andPredicates($query->in('code', $zoneCode));
			}
			$countries = $query->getDocuments()->toArray();

			if (count($countries) > 0)
			{
				$event->setParam('countries', $countries);
				return;
			}
		}
		// If there is no zone code specified, look for all active countries.
		else
		{
			$query = $documentManager->getNewQuery('Rbs_Geo_Country');
			$pb = $query->getPredicateBuilder();
			$query->andPredicates($pb->activated());
			$event->setParam('countries', $query->getDocuments()->toArray());
		}
	}

	/**
	 * @api
	 * @param string $code
	 * @return \Rbs\Geo\Documents\Country|null
	 */
	public function getCountryByCode($code)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['code' => $code]);
		$em->trigger('getCountryByCode', $this, $args);
		$country = isset($args['country']) ? $args['country'] : null;
		return ($country instanceof \Rbs\Geo\Documents\Country) ? $country : null;
	}

	/**
	 * Event Params: code
	 * @param \Change\Events\Event $event
	 */
	protected function onGetCountryByCode(\Change\Events\Event $event)
	{
		$code = $event->getParam('code');
		if ($code && is_string($code) && !\Change\Stdlib\StringUtils::isEmpty($code) && $event->getParam('country') === null)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$query = $documentManager->getNewQuery('Rbs_Geo_Country');
			$country = $query->andPredicates($query->activated(), $query->eq('code', $code))->getFirstDocument();
			if ($country)
			{
				$event->setParam('country', $country);
			}
		}
	}

	/**
	 * @api
	 * @param string $isoCode
	 * @return \Rbs\Geo\Documents\Country|null
	 */
	public function getCountryByIsoCode($isoCode)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['isoCode' => $isoCode]);
		$em->trigger('getCountryByCode', $this, $args);
		$country = isset($args['country']) ? $args['country'] : null;
		return ($country instanceof \Rbs\Geo\Documents\Country) ? $country : null;
	}

	/**
	 * Event Params: isoCode
	 * @param \Change\Events\Event $event
	 */
	protected function onGetCountryByIsoCode(\Change\Events\Event $event)
	{
		$isoCode = $event->getParam('isoCode');
		if ($isoCode && is_string($isoCode) && !\Change\Stdlib\StringUtils::isEmpty($isoCode) && $event->getParam('country') === null)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$query = $documentManager->getNewQuery('Rbs_Geo_Country');
			$country = $query->andPredicates($query->activated(), $query->eq('isoCode', $isoCode))->getFirstDocument();
			if ($country)
			{
				$event->setParam('country', $country);
			}
		}
	}

	/**
	 * @api
	 * @param AddressInterface $address
	 * @return string[]
	 */
	public function getFormattedAddress($address)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['address' => $address]);
		$this->getEventManager()->trigger(static::EVENT_FORMAT_ADDRESS, $this, $args);
		if (isset($args['lines']) && is_array($args['lines']))
		{
			return $args['lines'];
		}
		return [];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onFormatAddress($event)
	{
		if ($event->getParam('lines') !== null)
		{
			return;
		}

		$address = $event->getParam('address');
		if (!($address instanceof AddressInterface))
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$fields = $address->getFields();
		if (!is_array($fields) || !count($fields))
		{
			return;
		}

		$addressFields = null;
		if ($address instanceof \Rbs\Geo\Documents\Address)
		{
			$addressFields = $address->getAddressFields();
		}
		elseif (isset($fields['__addressFieldsId']))
		{
			$addressFields = $documentManager->getDocumentInstance($fields['__addressFieldsId'], 'Rbs_Geo_AddressFields');
		}

		if ($addressFields instanceof \Rbs\Geo\Documents\AddressFields)
		{
			if (!isset($fields['country']) && isset($fields[AddressInterface::COUNTRY_CODE_FIELD_NAME]))
			{
				$countryCode = $fields[AddressInterface::COUNTRY_CODE_FIELD_NAME];
				$dqb = $documentManager->getNewQuery('Rbs_Geo_Country');
				$dqb->andPredicates($dqb->eq('code', $countryCode));
				$country = $dqb->getFirstDocument();
				if ($country instanceof \Rbs\Geo\Documents\Country)
				{
					$i18n = $applicationServices->getI18nManager();
					$fields['country'] = $i18n->trans($country->getI18nTitleKey());
				}
			}

			$collectionManager = $applicationServices->getCollectionManager();
			$fieldDefinitions = [];
			foreach ($addressFields->getFields() as $fieldDefinition)
			{
				$fieldCode = $fieldDefinition->getCode();
				$fieldDefinitions[$fieldCode] = $fieldDefinition;
				if (isset($fields[$fieldCode]) && $fieldDefinition->getCollectionCode())
				{
					$fields[$fieldCode . 'Value'] = $fields[$fieldCode];
					$collection = $collectionManager->getCollection($fieldDefinition->getCollectionCode());
					if ($collection)
					{
						$value = $collection->getItemByValue($fields[$fieldCode]);
						if ($value)
						{
							$fields[$fieldCode] = $value->getTitle();
						}
					}
				}
			}
			$event->setParam('fields', $fields);
			$event->setParam('fieldDefinitions', $fieldDefinitions);
			$event->setParam('layout', $addressFields->getFieldsLayout());
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onFormatFieldsByLayout($event)
	{
		if ($event->getParam('lines') !== null)
		{
			return;
		}
		$fields = $event->getParam('fields');
		$fieldDefinitions = $event->getParam('fieldDefinitions');
		$layout = $event->getParam('layout');
		if ($fields && $fieldDefinitions && $layout && is_array($fields) && is_array($fieldDefinitions) && is_array($layout))
		{
			$event->setParam('lines', $this->formatFieldsByLayout($fieldDefinitions, $fields, $layout));
		}
	}

	/**
	 * @param \Rbs\Geo\Documents\AddressField[] $fieldDefinitions
	 * @param array $fields
	 * @param array $layout
	 * @return array
	 */
	protected function formatFieldsByLayout(array $fieldDefinitions, array $fields, array $layout)
	{
		$lines = [];
		foreach ($layout as $lineLayout)
		{
			$line = [];
			foreach ($lineLayout as $fieldName)
			{
				$fieldValue = isset($fields[$fieldName]) ? $fields[$fieldName] : null;
				if ($fieldValue)
				{
					$line[] = $fieldValue;
				}
				elseif (isset($fieldDefinitions[$fieldName]) && $fieldDefinitions[$fieldName]->getRequired())
				{
					return null;
				}
			}
			if (count($line))
			{
				$lines[] = implode(' ', $line);
			}
		}
		return $lines;
	}

	/**
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param \Change\User\AuthenticationManager $authenticationManager
	 * @return integer
	 */
	protected function extractUserId($user, $authenticationManager)
	{
		if (is_numeric($user))
		{
			return (int)$user;
		}

		if ($user === null)
		{
			$user = $authenticationManager->getCurrentUser();
		}

		if ($user instanceof \Change\User\UserInterface)
		{
			return $user->authenticated() ? $user->getId() : 0;
		}

		if ($user instanceof \Rbs\User\Documents\User)
		{
			return $user->getId();
		}

		return 0;
	}

	/**
	 * Get user addresses to display in front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @return Address\AddressInterface[]
	 */
	public function getUserAddresses($user)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user]);
		$eventManager->trigger('getAddresses', $this, $args);
		if (isset($args['addresses']) && is_array($args['addresses']))
		{
			return $args['addresses'];
		}
		return [];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetAddresses($event)
	{
		if ($event->getParam('addresses') !== null)
		{
			return;
		}
		$userId = $this->extractUserId($event->getParam('user'), $event->getApplicationServices()->getAuthenticationManager());
		if (!$userId)
		{
			return;
		}
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Geo_Address');
		$query->andPredicates($query->eq('ownerId', $userId));
		$query->addOrder('name');
		$event->setParam('addresses', $query->getDocuments()->toArray());
	}

	/**
	 * User Address deletion from front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param mixed $address
	 * @return boolean
	 */
	public function deleteUserAddress($user, $address)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user, 'address' => $address]);
		$this->getEventManager()->trigger('deleteAddress', $this, $args);
		return (isset($args['done']) && $args['done'] === true);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onDeleteAddress($event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$address = $event->getParam('address');
		if (is_numeric($address))
		{
			$address = $documentManager->getDocumentInstance((int)$address);
		}
		if (!($address instanceof \Rbs\Geo\Documents\Address))
		{
			return;
		}

		$userId = $this->extractUserId($event->getParam('user'), $event->getApplicationServices()->getAuthenticationManager());
		if ($userId != $address->getOwnerId())
		{
			return;
		}

		// If the addressId represents an address document it is owned by the current user, delete it.
		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$address->delete();

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setParam('done', true);
	}

	/**
	 * get default for names
	 * ex: ['default', 'shipping', 'billing']
	 * @api
	 * @return string[]
	 */
	public function getDefaultForNames()
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['names' => []]);
		$this->getEventManager()->trigger('getDefaultForNames', $this, $args);
		return is_array($args['names']) ? $args['names'] : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetDefaultForNames($event)
	{
		$names = $event->getParam('names');
		if (is_array($names) && !in_array('default', $names))
		{
			$names[] = 'default';
			$event->setParam('names', $names);
		}
	}

	/**
	 * @param \Rbs\Geo\Address\AddressInterface $address
	 * @param array $options
	 * @return \Rbs\Geo\Address\NormalizeAddress|null
	 */
	public function normalizeAddress($address, $options)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['address' => $address, 'options' => $options, 'normalizeAddress' => null]);
		$eventManager->trigger('normalizeAddress', $this, $args);
		$normalizeAddress = $args['normalizeAddress'];
		return $normalizeAddress instanceof \Rbs\Geo\Address\NormalizeAddress ? $normalizeAddress : null;
	}

	/**
	 * Address creation from front office.
	 * @api
	 * @param array $addressData
	 * @return \Rbs\Geo\Address\AddressInterface|null
	 */
	public function validateAddress($addressData)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['addressData' => $addressData]);
		$eventManager->trigger('validateAddress', $this, $args);
		return (isset($args['address']) && $args['address'] instanceof \Rbs\Geo\Address\AddressInterface) ? $args['address'] : null;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onValidateAddress($event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$addressData = $event->getParam('addressData');
		$addressFieldsId = (int)(isset($addressData['common']['addressFieldsId']) ? $addressData['common']['addressFieldsId'] : 0);
		/** @var \Rbs\Geo\Documents\AddressFields $addressFields */
		$addressFields = $documentManager->getDocumentInstance($addressFieldsId, 'Rbs_Geo_AddressFields');
		if ($addressFields)
		{
			$address = new \Rbs\Geo\Address\BaseAddress($addressData);
			$address->setFieldValue('__lines', $this->getFormattedAddress($address));
			$event->setParam('address', $address);
		}
	}

	/**
	 * Address creation from front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param array $addressData
	 * @return \Rbs\Geo\Address\AddressInterface|null
	 */
	public function addUserAddress($user, $addressData)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user, 'addressData' => $addressData]);
		$eventManager->trigger('addAddress', $this, $args);
		return (isset($args['address']) && $args['address'] instanceof \Rbs\Geo\Address\AddressInterface) ? $args['address'] : null;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	public function onDefaultAddAddress($event)
	{
		if ($event->getParam('address') !== null)
		{
			return;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$userId = $this->extractUserId($event->getParam('user'), $event->getApplicationServices()->getAuthenticationManager());

		$addressData = $event->getParam('addressData');
		if (!$userId || !is_array($addressData)
			|| !isset($addressData['common']['addressFieldsId'], $addressData['fields']['countryCode'])
		)
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();
			$addressFieldsId = (int)$addressData['common']['addressFieldsId'];
			$name = isset($addressData['common']['name']) ? $addressData['common']['name'] : '-';

			/* @var $addressFields \Rbs\Geo\Documents\AddressFields */
			$addressFields = $documentManager->getDocumentInstance($addressFieldsId, 'Rbs_Geo_AddressFields');
			$fieldValues = $addressData['fields'];

			/* @var $address \Rbs\Geo\Documents\Address */
			$address = $documentManager->getNewDocumentInstanceByModelName('Rbs_Geo_Address');
			foreach ($fieldValues as $n => $v)
			{
				$address->setFieldValue($n, $v);
			}
			$address->setAddressFields($addressFields);
			$address->setOwnerId($userId);

			$user = $documentManager->getDocumentInstance($userId, 'Rbs_User_User');
			if ($user instanceof \Rbs\User\Documents\User)
			{
				$address->setOwnerUser(new \Rbs\User\Events\AuthenticatedUser($user));
			}

			$address->setName($name);
			$address->save();

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		if (isset($addressData['default']) && is_array($addressData['default']))
		{
			$defaultFor = [];
			foreach ($addressData['default'] as $default => $isDefault)
			{
				if ($isDefault)
				{
					$defaultFor[] = $default;
				}
			}
			$this->setDefaultUserAddress($userId, $address, $defaultFor);
		}

		$event->setParam('address', $address);
	}

	/**
	 * Address update from UA.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param array $addressDataUa
	 * @return \Rbs\Geo\Address\AddressInterface|null
	 */
	public function updateUserAddressForUa($user, $addressDataUa)
	{
		// Transforms an address in UA format into front office format
		$addressDataFront = $addressDataUa;
		$addressDataFront['common']['id'] = $addressDataUa['_meta']['id'];

		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user, 'addressData' => $addressDataFront]);
		$this->getEventManager()->trigger('updateAddress', $this, $args);
		return (isset($args['address']) && $args['address'] instanceof \Rbs\Geo\Address\AddressInterface) ? $args['address'] : null;
	}

	/**
	 * Address update from front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param array $addressData
	 * @return \Rbs\Geo\Address\AddressInterface|null
	 */
	public function updateUserAddress($user, $addressData)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user, 'addressData' => $addressData]);
		$this->getEventManager()->trigger('updateAddress', $this, $args);
		return (isset($args['address']) && $args['address'] instanceof \Rbs\Geo\Address\AddressInterface) ? $args['address'] : null;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onUpdateAddress($event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$userId = $this->extractUserId($event->getParam('user'), $event->getApplicationServices()->getAuthenticationManager());
		$addressData = $event->getParam('addressData');
		if (!$userId || !is_array($addressData) || !isset($addressData['common']['id']))
		{
			return;
		}
		$addressId = (int)$addressData['common']['id'];

		/* @var $address \Rbs\Geo\Documents\Address */
		$address = $documentManager->getDocumentInstance($addressId, 'Rbs_Geo_Address');
		if (!($address instanceof \Rbs\Geo\Documents\Address) || $address->getOwnerId() != $userId)
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();
			if (isset($addressData['common']['addressFieldsId']))
			{
				$addressFieldsId = (int)$addressData['common']['addressFieldsId'];
				if ($addressFieldsId)
				{
					$addressFields = $documentManager->getDocumentInstance($addressFieldsId, 'Rbs_Geo_AddressFields');
					if ($addressFields instanceof \Rbs\Geo\Documents\AddressFields)
					{
						$address->setAddressFields($addressFields);
					}
				}
			}
			if (isset($addressData['fields']) && is_array($addressData['fields']))
			{
				foreach ($addressData['fields'] as $n => $v)
				{
					$address->setFieldValue($n, $v);
				}
			}
			if (isset($addressData['common']['name']))
			{
				$addressName = (string)$addressData['common']['name'];
				$address->setName(\Change\Stdlib\StringUtils::isEmpty($addressName) ? '-' : $addressName);
			}

			$address->save();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		if (isset($addressData['default']) && is_array($addressData['default']))
		{
			$defaultFor = [];
			foreach ($addressData['default'] as $default => $isDefault)
			{
				if ($isDefault)
				{
					$defaultFor[] = $default;
				}
			}
			$this->setDefaultUserAddress($userId, $address, $defaultFor);
		}

		$event->setParam('address', $address);
	}

	/**
	 * Set default address from front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param mixed $address
	 * @param string|array $defaultFor
	 * @return boolean
	 */
	public function setDefaultUserAddress($user, $address, $defaultFor = [])
	{
		$eventManager = $this->getEventManager();
		if (is_string($defaultFor))
		{
			$defaultFor = [$defaultFor];
		}
		$args = $eventManager->prepareArgs(['user' => $user, 'address' => $address, 'defaultFor' => $defaultFor]);
		$this->getEventManager()->trigger('setDefaultAddress', $this, $args);
		return (isset($args['done']) && $args['done'] === true);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onSetDefaultAddress($event)
	{
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$defaultFor = $event->getParam('defaultFor');

		$address = $event->getParam('address');
		if (is_numeric($address))
		{
			$address = $documentManager->getDocumentInstance((int)$address);
		}

		if (!($address instanceof \Rbs\Geo\Documents\Address))
		{
			return;
		}

		$userId = $this->extractUserId($event->getParam('user'), $applicationServices->getAuthenticationManager());
		if (!$userId)
		{
			return;
		}
		$userDocument = $documentManager->getDocumentInstance($userId);
		if (!($userDocument instanceof \Rbs\User\Documents\User))
		{
			return;
		}
		$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($userDocument);

		// If the addressId represents an address document and there is an authenticated user, set the default address meta.
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			if (is_array($defaultFor) && in_array('default', $defaultFor))
			{
				$userDocument->setMeta('Rbs_Geo_DefaultAddressId', $address->getId());
			}

			$userProfile = $applicationServices->getProfileManager()->loadProfile($authenticatedUser, 'Rbs_User');
			if ($userProfile instanceof \Rbs\User\Profile\Profile)
			{
				$addressFieldsValue = $address->getFields();
				$update = false;
				foreach ($userProfile->getPropertyNames() as $propertyName)
				{
					$profileValue = $userProfile->getPropertyValue($propertyName);
					if (($profileValue === null || $profileValue === '') && isset($addressFieldsValue[$propertyName]))
					{
						$userProfile->setPropertyValue($propertyName, $addressFieldsValue[$propertyName]);
						$update = true;
					}
				}
				if ($update)
				{
					$applicationServices->getProfileManager()->saveProfile($authenticatedUser, $userProfile);
				}
			}

			$userDocument->saveMetas();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
		$event->setParam('done', true);
	}

	/**
	 * Get default addresses for front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param string|array $defaultFor
	 * @return \Rbs\Geo\Address\AddressInterface[]
	 */
	public function getDefaultUserAddresses($user, $defaultFor = [])
	{
		if (is_string($defaultFor))
		{
			$defaultFor = [$defaultFor];
		}
		$addresses = [];
		if (is_array($defaultFor) && count($defaultFor))
		{
			foreach ($defaultFor as $for)
			{
				$address = $this->getDefaultUserAddress($user, [$for]);
				$addresses[$for] = $address;
			}
		}
		return $addresses;
	}

	/**
	 * Get default address for front office.
	 * @api
	 * @param integer|\Change\User\UserInterface|\Rbs\User\Documents\User|null $user
	 * @param string|array $defaultFor
	 * @return \Rbs\Geo\Address\AddressInterface|null
	 */
	public function getDefaultUserAddress($user, $defaultFor = [])
	{
		if (is_string($defaultFor))
		{
			$defaultFor = [$defaultFor];
		}

		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user, 'defaultFor' => $defaultFor]);
		$this->getEventManager()->trigger('getDefaultAddress', $this, $args);
		if (isset($args['defaultAddress']) && $args['defaultAddress'] instanceof \Rbs\Geo\Address\AddressInterface)
		{
			return $args['defaultAddress'];
		}
		return null;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onGetDefaultAddress($event)
	{
		if ($event->getParam('defaultAddress') instanceof \Rbs\Geo\Address\AddressInterface)
		{
			return;
		}
		$defaultFor = $event->getParam('defaultFor');
		if (!is_array($defaultFor) || !in_array('default', $defaultFor))
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();

		$userId = $this->extractUserId($event->getParam('user'), $applicationServices->getAuthenticationManager());
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$user = $documentManager->getDocumentInstance($userId);
		if (!($user instanceof \Rbs\User\Documents\User))
		{
			return;
		}

		$address = $documentManager->getDocumentInstance($user->getMeta('Rbs_Geo_DefaultAddressId'));
		if ($address instanceof \Rbs\Geo\Documents\Address)
		{
			$event->setParam('defaultAddress', $address);
		}
	}

	/**
	 * @param string $codeZone
	 * @return \Rbs\Geo\Documents\Zone|null
	 */
	public function getZoneByCode($codeZone)
	{
		if (is_string($codeZone))
		{
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['codeZone' => $codeZone]);
			$this->getEventManager()->trigger('getZoneByCode', $this, $args);
			if (isset($args['zone']) && $args['zone'] instanceof \Rbs\Geo\Documents\Zone)
			{
				return $args['zone'];
			}
		}
		return null;
	}

	/**
	 * @var array
	 */
	protected $zonesByCode = [];

	/**
	 * Input param codeZone
	 * Output param zone
	 * @param \Change\Events\Event $event
	 */
	protected function onGetZoneByCode($event)
	{
		$codeZone = $event->getParam('codeZone');
		if (is_string($codeZone))
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			if (isset($this->zonesByCode[$codeZone]))
			{
				$event->setParam('zone', $documentManager->getDocumentInstance($this->zonesByCode[$codeZone]));
				return;
			}
			$query = $documentManager->getNewQuery('Rbs_Geo_Zone');
			$query->andPredicates($query->eq('code', $codeZone));
			$zone = $query->getFirstDocument();
			if ($zone)
			{
				$this->zonesByCode[$codeZone] = $zone->getId();
				$event->setParam('zone', $documentManager->getDocumentInstance($this->zonesByCode[$codeZone]));
			}
		}
	}

	/**
	 * @param array $options
	 * @return array
	 */
	public function getAddressFiltersDefinition($options = [])
	{
		if ($this->addressFilters === null)
		{
			$this->addressFilters = new \Rbs\Geo\Address\AddressFilters($this->getApplication());
		}
		return $this->addressFilters->getDefinitions($options);
	}

	/**
	 * @param \Rbs\Geo\Address\AddressInterface $address
	 * @param \Rbs\Geo\Documents\Zone|string|null $zone
	 * @param array $options
	 * @return boolean
	 */
	public function isValidAddressForZone(\Rbs\Geo\Address\AddressInterface $address, $zone, array $options = [])
	{
		if ($this->addressFilters === null)
		{
			$this->addressFilters = new \Rbs\Geo\Address\AddressFilters($this->getApplication());
		}
		$this->addressFilters->setError(null);
		if (is_string($zone))
		{
			$zone = $this->getZoneByCode($zone);
			if (!$zone)
			{
				return false;
			}
		}

		if ($zone instanceof \Rbs\Geo\Documents\Zone)
		{
			$options['zone'] = $zone;
			$valid = $this->addressFilters->isValid($address, $zone->getAddressFilterData(), $options);
			if (!$valid)
			{
				$error = $zone->getCurrentLocalization()->getFilterErrorMessage();
				if (\Change\Stdlib\StringUtils::isEmpty($error))
				{
					$error = 'm.rbs.geo.front.invalid_address_for_zone';
				}
				$this->addressFilters->setError($error);
			}
			return $valid;
		}
		elseif ($zone === null)
		{
			return true;
		}
		return false;
	}

	/**
	 * @return null|string
	 */
	public function getLastValidationError()
	{
		if ($this->addressFilters !== null)
		{
			return $this->addressFilters->getError();
		}
		return null;
	}

	/**
	 * Default context params:
	 *  - data:
	 *    - beginOfName
	 *    - countryCode
	 *    - options:
	 *       - modeId
	 * @param array $context
	 * @return array
	 */
	public function getCityAutoCompletion(array $context)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['context' => $context]);
		$eventManager->trigger('getCityAutoCompletion', $this, $args);
		if (isset($args['cities']) && is_array($args['cities']))
		{
			return $args['cities'];
		}
		return [];
	}

	/**
	 * Default context params:
	 *  - data:
	 *    - address
	 *    - countryCode
	 *    - options:
	 * @param array $context
	 * @return array
	 */
	public function getAddressCompletion(array $context)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['context' => $context]);
		$eventManager->trigger('getAddressCompletion', $this, $args);
		if (isset($args['addresses']) && is_array($args['addresses']))
		{
			return $args['addresses'];
		}
		return [];
	}

	/**
	 * Default context params:
	 *  - data:
	 *    - address:
	 *       - country
	 *       - zipCode
	 *       - city
	 *    - position:
	 *       - latitude
	 *       - longitude
	 *    - options:
	 *       - modeId
	 *    - matchingZone: string or array
	 * @param array $context
	 * @return array
	 */
	public function getPoints(array $context)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['context' => $context]);
		$eventManager->trigger('getPoints', $this, $args);
		if (isset($args['points']) && is_array($args['points']))
		{
			return $args['points'];
		}
		return [];
	}

	/**
	 * @param \Rbs\Geo\Documents\AddressFields|integer $addressFields
	 * @param array $context
	 * @return array
	 */
	public function getAddressFieldsData($addressFields, array $context)
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs(['addressFields' => $addressFields, 'context' => $context]);
		$em->trigger('getAddressFieldsData', $this, $eventArgs);
		if (isset($eventArgs['addressFieldsData']) && is_array($eventArgs['addressFieldsData']))
		{
			return $eventArgs['addressFieldsData'];
		}
		return [];
	}

	/**
	 * Input param addressFields, context
	 * Output param addressFieldsData
	 * @param \Change\Events\Event $event
	 */
	protected function onGetAddressFieldsData($event)
	{
		$addressFields = $event->getParam('addressFields');
		if (is_numeric($addressFields))
		{
			$addressFields = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($addressFields, 'Rbs_Geo_AddressFields');
		}

		if ($addressFields instanceof \Rbs\Geo\Documents\AddressFields)
		{
			$collectionManager = $event->getApplicationServices()->getCollectionManager();
			$dataSets = ['common' => ['id' => $addressFields->getId()], 'fields' => []];
			$idPrefix = uniqid($addressFields->getId() . '_', false) . '_';
			foreach ($addressFields->getFields() as $addressField)
			{
				$input = [
					'name' => $addressField->getCode(),
					'id' => $idPrefix . $addressField->getCode(),
					'title' => $addressField->getTitle(),
					'required' => $addressField->getRequired(),
					'match' => $addressField->getMatch(),
					'matchErrorMessage' => $addressField->getCurrentLocalization()->getMatchErrorMessage(),
					'defaultValue' => $addressField->getDefaultValue()
				];
				if ($addressField->getCollectionCode())
				{
					$collection = $collectionManager->getCollection($addressField->getCollectionCode());
					if ($collection)
					{
						$values = [];
						foreach ($collection->getItems() as $item)
						{
							$values[$item->getValue()] = ['value' => $item->getValue(), 'title' => $item->getTitle()];
						}
						$input['values'] = $values;
					}
				}
				$dataSets['fieldsIndex'][$input['name']] = count($dataSets['fields']);
				$dataSets['fields'][] = $input;
			}
			$dataSets['fieldsLayoutData'] = $addressFields->getFieldsLayoutData();
			$event->setParam('addressFieldsData', $dataSets);
		}
	}

	/**
	 * @param AddressInterface $address
	 * @return array
	 */
	public function getCoordinatesByAddress(\Rbs\Geo\Address\AddressInterface $address)
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs(['address' => $address]);
		$em->trigger('getCoordinatesByAddress', $this, $eventArgs);
		if (isset($eventArgs['coordinates']) && is_array($eventArgs['coordinates']))
		{
			return $eventArgs['coordinates'];
		}
		return [];
	}

	/**
	 * Input param address
	 * Output param coordinates, formattedAddress
	 * @param \Change\Events\Event $event
	 */
	protected function onGetCoordinatesByAddress($event)
	{
		if ($event->getParam('coordinates'))
		{
			return;
		}

		$address = $event->getParam('address');
		if ($address instanceof AddressInterface)
		{
			$validAddress = $this->validateAddress($address->toArray());
			if ($validAddress)
			{
				$address = $validAddress;
			}

			$google = $event->getApplication()->getConfiguration('Rbs/Geo/Google');
			if (is_array($google) && ($google['client'] ?? $google['APIKey'] ?? false))
			{
				$googleGeoCode = new \Rbs\Geo\Address\GoogleGeoCode($this);
				$googleGeoCode->setLogging($event->getApplication()->getLogging());
				if (isset($google['client']))
				{
					$googleGeoCode->setClient($google['client']);
				}
				else
				{
					$googleGeoCode->setKey($google['APIKey']);
				}
				$coordinates = $googleGeoCode->getCoordinates($address);
				if (is_array($coordinates))
				{
					$event->setParam('coordinates', $coordinates);
				}
			}
			else
			{
				$configuration = $event->getApplication()->getConfiguration('Change/Application');
				$url = $event->getApplication()->getConfiguration('Rbs/Geo/OSM/url');
				$userAgent = 'Proximis Omnichannel/' . $configuration['version'] . ' Project ' . $configuration['uuid'];
				$nominatimOSM = new \Rbs\Geo\Address\NominatimOSM($this, $userAgent, $url);
				$nominatimOSM->setLogging($event->getApplication()->getLogging());
				$i18nManager = $event->getApplicationServices()->getI18nManager();
				$nominatimOSM->setI18nManager($i18nManager);
				$coordinates = $nominatimOSM->getCoordinates($address);
				if (is_array($coordinates))
				{
					$event->setParam('coordinates', $coordinates);
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return array
	 */
	public function getNumberConfiguration(\Change\Events\Event $event)
	{
		$config = [];
		$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		foreach ($this->getSupportedRegions($event) as $regionCode => $name)
		{
			$sampleMobile = $phoneUtil->getExampleNumberForType($regionCode, \libphonenumber\PhoneNumberType::MOBILE);
			$sampleFixedLine = $phoneUtil->getExampleNumberForType($regionCode, \libphonenumber\PhoneNumberType::FIXED_LINE);

			$sampleFixedLineNational = $sampleMobileNational = $countryCode = null;
			if ($sampleMobile && $phoneUtil->isValidNumber($sampleMobile))
			{
				$countryCode = $sampleMobile->getCountryCode();
				$sampleMobileNational = $phoneUtil->format($sampleMobile, \libphonenumber\PhoneNumberFormat::NATIONAL);
			}

			if ($sampleFixedLine && $phoneUtil->isValidNumber($sampleFixedLine))
			{
				$countryCode = $sampleFixedLine->getCountryCode();
				$sampleFixedLineNational = $phoneUtil->format($sampleFixedLine, \libphonenumber\PhoneNumberFormat::NATIONAL);
			}

			if ($countryCode)
			{
				$config[$name] = [
					'regionCode' => $regionCode,
					'countryCode' => $countryCode,
					'name' => $name,
					'sampleFixedLine' => $sampleFixedLineNational,
					'sampleMobile' => $sampleMobileNational
				];
			}
		}
		return $config;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return array
	 */
	protected function getSupportedRegions(\Change\Events\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$q = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Geo_Country');
		$countries = $q->andPredicates($q->activated())->getDocuments()->preLoad()->toArray();
		$regions = [];
		/** @var \Rbs\Geo\Documents\Country $country */
		foreach ($countries as $country)
		{
			$name = $i18nManager->trans($country->getI18nTitleKey(), ['ucf']);
			if ($name)
			{
				$regions[$country->getCode()] = $name;
			}
		}
		return $regions;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param string $number
	 * @param null $regionCode
	 * @return array|null
	 */
	public function parsePhoneNumber($i18nManager, $number, $regionCode = null)
	{
		$geoCoder = \libphonenumber\geocoding\PhoneNumberOfflineGeocoder::getInstance();
		$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		$number = (string)$number;
		if (\libphonenumber\PhoneNumberUtil::isViablePhoneNumber($number))
		{
			$regionCode = (string)$regionCode;
			if (!$regionCode)
			{
				$regionCode = substr($i18nManager->getLCID(), 3);
			}
			$phone = $phoneUtil->parse($number, $regionCode);
			if ($phone)
			{
				$type = $phoneUtil->getNumberType($phone);
				switch ($type)
				{
					case \libphonenumber\PhoneNumberType::MOBILE:
						$typeName = 'MOBILE';
						break;
					case \libphonenumber\PhoneNumberType::FIXED_LINE:
						$typeName = 'FIXED_LINE';
						break;
					case \libphonenumber\PhoneNumberType::FIXED_LINE_OR_MOBILE:
						$typeName = 'FIXED_LINE_OR_MOBILE';
						break;
					case \libphonenumber\PhoneNumberType::PAGER:
						$typeName = 'PAGER';
						break;
					case \libphonenumber\PhoneNumberType::PREMIUM_RATE:
						$typeName = 'PREMIUM_RATE';
						break;
					case \libphonenumber\PhoneNumberType::TOLL_FREE:
						$typeName = 'TOLL_FREE';
						break;
					case \libphonenumber\PhoneNumberType::SHARED_COST:
						$typeName = 'SHARED_COST';
						break;
					case \libphonenumber\PhoneNumberType::VOIP:
						$typeName = 'VOIP';
						break;
					case \libphonenumber\PhoneNumberType::PERSONAL_NUMBER:
						$typeName = 'PERSONAL_NUMBER';
						break;
					case \libphonenumber\PhoneNumberType::UAN:
						$typeName = 'UAN';
						break;
					case \libphonenumber\PhoneNumberType::VOICEMAIL:
						$typeName = 'VOICEMAIL';
						break;
					default:
						$typeName = 'UNKNOWN';
						break;
				}
				$regionCode = $phoneUtil->getRegionCodeForNumber($phone);
				return [
					'E164' => $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::E164),
					'national' => $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::NATIONAL),
					'international' => $phoneUtil->format($phone, \libphonenumber\PhoneNumberFormat::INTERNATIONAL),
					'countryCode' => $phone->getCountryCode(),
					'regionCode' => $regionCode,
					'type' => $typeName,
					'flagCountry' => strtolower($regionCode),
					'country' => $geoCoder->getDescriptionForNumber($phone, $i18nManager->getLCID(), $regionCode)
				];
			}
		}
		return null;
	}
}