<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Setup;

/**
 * @name \Rbs\Simpleform\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$images = $configuration->getEntry('Change/Storage/Rbs_Simpleform', []);
		$images = array_merge([
			'class' => \Change\Storage\Engines\LocalStorage::class,
			'basePath' => 'App/Storage/Rbs_Simpleform/files',
			'baseURL' => '/index.php'
		], $images);
		$configuration->addPersistentEntry('Change/Storage/Rbs_Simpleform', $images);
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Simpleform', \Rbs\Simpleform\Setup\Patch\Listeners::class);
	}
}
