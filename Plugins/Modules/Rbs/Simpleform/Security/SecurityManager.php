<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Security;

/**
 * @name \Rbs\Simpleform\Security\SecurityManager
 */
class SecurityManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'Rbs_Simpleform_SecurityManager';
	const EVENT_INSTANTIATE_CAPTCHA = 'instantiateCaptcha';
	const EVENT_RENDER_CAPTCHA = 'renderCaptcha';

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(static::EVENT_INSTANTIATE_CAPTCHA, [$this, 'onDefaultInstantiateCaptcha'], 5);
		$eventManager->attach(static::EVENT_RENDER_CAPTCHA, [$this, 'onDefaultRenderCaptcha'], 5);
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Simpleform/Events/SecurityManager');
	}

	// Cross Site Request Forgery prevention.

	/**
	 * @api
	 * @see http://blog.ircmaxell.com/2013/02/preventing-csrf-attacks.html
	 * @return string
	 */
	public function getCSRFToken()
	{
		$token = \Zend\Math\Rand::getString(64);
		$session = $this->getApplication()->getSessionContainer('Change_Rbs_Simpleform');
		if ($session)
		{
			if (!isset($session['CSRFTokens']) || !is_array($session['CSRFTokens']))
			{
				$session['CSRFTokens'] = [];
			}
			$session['CSRFTokens'][$token] = true;
		}
		return $token;
	}

	/**
	 * @api
	 * @see http://blog.ircmaxell.com/2013/02/preventing-csrf-attacks.html
	 * @param string $token
	 * @return boolean
	 */
	public function checkCSRFToken($token)
	{
		if (!$token)
		{
			return false;
		}
		
		$session = $this->getApplication()->getSessionContainer('Change_Rbs_Simpleform');
		if ($session && isset($session['CSRFTokens'][$token]))
		{
			unset($session['CSRFTokens'][$token]);
			return true;
		}

		return false;
	}

	// CAPTCHA.

	/**
	 * @var \Zend\Captcha\AdapterInterface
	 */
	protected $captcha;

	/**
	 * @var string
	 */
	protected $captchaId;

	/**
	 * @param \Zend\Captcha\AdapterInterface $captcha
	 * @return $this
	 */
	public function setCaptcha($captcha)
	{
		$this->captcha = $captcha;
		return $this;
	}

	/**
	 * @param bool $generate
	 * @return \Zend\Captcha\AdapterInterface
	 */
	protected function getCaptcha($generate = false)
	{
		if ($this->captcha === null)
		{
			$em = $this->getEventManager();
			$args = $em->prepareArgs(['captcha' => false, 'generate' => $generate]);
			$em->trigger(static::EVENT_INSTANTIATE_CAPTCHA, $this, $args);
			$this->captcha = $args['captcha'];
		}
		return $this->captcha;
	}

	/**
	 * @api
	 * @return string
	 */
	public function getCaptchaId()
	{
		$this->getCaptcha(true);
		return $this->captchaId;
	}

	/**
	 * @api
	 * @param array $params
	 * @return string|null
	 */
	public function renderCaptcha(array $params = [])
	{
		$captcha = $this->getCaptcha(true);
		if (!$captcha)
		{
			return null;
		}

		$em = $this->getEventManager();
		$params['captcha'] = $captcha;
		$params['htmlResult'] = null;
		$args = $em->prepareArgs($params);
		$em->trigger(static::EVENT_RENDER_CAPTCHA, $this, $args);
		return $args['htmlResult'];
	}

	/**
	 * @api
	 * @param mixed $value
	 * @return boolean
	 */
	public function validateCaptcha($value)
	{
		$captcha = $this->getCaptcha();
		return !$captcha || $captcha->isValid($value);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultInstantiateCaptcha(\Change\Events\Event $event)
	{
		if (!($event->getParam('captcha') instanceof \Zend\Captcha\AdapterInterface))
		{
			/* @var $application \Change\Application */
			$application = $event->getApplication();
			$imgDir = $application->getWorkspace()->tmpPath('Captcha');
			\Change\Stdlib\FileUtils::mkdir($imgDir);

			$params = [
				'font' => $application->getWorkspace()->pluginsModulesPath('Rbs', 'Simpleform', 'Assets', 'Font', 'Gravity-Book.ttf'),
				'fontSize' => 20,
				'width' => 150,
				'height' => 70,
				'wordLen' => 5,
				'dotNoiseLevel' => 50,
				'lineNoiseLevel' => 3,
				'imgDir' => $imgDir,
				'imgUrl' => 'Action/Rbs/Simpleform/Image?i='
			];
			$captcha = new \Zend\Captcha\Image($params);

			if ($event->getParam('generate') && $this->captchaId === null)
			{
				$this->captchaId = $captcha->generate();
				$imgFileName = $captcha->getImgDir() . $captcha->getId() . $captcha->getSuffix();
				$content = file_get_contents($imgFileName);
				$key = str_replace('.', '_', 'captcha_' . $captcha->getId() . $captcha->getSuffix());
				$event->getApplicationServices()->getCacheManager()->setEntry('temporaryDownload', $key, $content, ['ttl' => 1200]);
				unlink($imgFileName);
			}

			$event->setParam('captcha', $captcha);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultRenderCaptcha(\Change\Events\Event $event)
	{
		$captcha = $event->getParam('captcha');
		if (!$event->getParam('htmlResult') && $captcha instanceof \Zend\Captcha\Image)
		{
			$html = '<img width="' . $captcha->getWidth() . '" height="' . $captcha->getHeight() . '"
				alt="' . $captcha->getImgAlt() . '"
				src="' . $captcha->getImgUrl() . rawurlencode($captcha->getId() . $captcha->getSuffix()) . '" />';
			$event->setParam('htmlResult', $html);
		}
	}
}