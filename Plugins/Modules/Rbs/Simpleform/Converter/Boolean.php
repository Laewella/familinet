<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\Boolean
 */
class Boolean extends \Rbs\Simpleform\Converter\Trim
{
	/**
	 * @param string $value
	 * @return boolean|\Rbs\Simpleform\Converter\Validation\Error
	 */
	protected function doParseFromUI($value)
	{
		if ($value === 'true')
		{
			return true;
		}
		elseif ($value === 'false')
		{
			return false;
		}
		$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_boolean', ['ucf']);
		return new Validation\Error([$message]);
	}

	/**
	 * @param boolean $value
	 * @return string
	 */
	public function formatValue($value)
	{
		return $this->getI18nManager()->trans('c.types.' . ($value === true ? 'yes' : 'no'), ['ucf']);
	}
}