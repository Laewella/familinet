<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\ConverterInterface
 */
interface ConverterInterface
{
	/**
	 * @param mixed $value
	 * @return mixed|\Rbs\Simpleform\Converter\Validation\Error JSON encodable value
	 */
	public function parseFromUI($value);

	/**
	 * @param mixed $value
	 * @return boolean
	 */
	public function isEmptyFromUI($value);

	/**
	 * @param mixed $value
	 * @return string
	 */
	public function formatValue($value);
}