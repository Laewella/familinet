<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Documents;

/**
 * @name \Rbs\Simpleform\Documents\Response
 */
class Response extends \Compilation\Rbs\Simpleform\Documents\Response
{
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, [$this, 'onDefaultCreated'], 5);
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return strval($this->getId());
	}

	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() === $this)
		{
			$restResult = $event->getParam('restResult');
			$restResult->setProperty('formId', $this->getFormId());
		}
		parent::onDefaultRouteParamsRestResult($event);
	}

	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() === $this)
		{
			$restResult = $event->getParam('restResult');
			if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
			{
				$fieldsValues = [];
				$data = $this->getFieldsInfos();
				if (is_array($data) && count($data))
				{
					/* @var $genericServices \Rbs\Generic\GenericServices */
					$genericServices = $event->getServices('genericServices');
					$fieldManager = $genericServices->getFieldManager();

					foreach ($data as $fv)
					{
						if (is_array($fv) && isset($fv['title']))
						{
							if (isset($fv['fieldType']['code'], $fv['fieldType']['parameters']))
							{
								$parameters = $fv['fieldType']['parameters'];
								$ft = $fieldManager->getFieldType($fv['fieldType']['code'], is_array($parameters) ? $parameters : []);
								if ($ft)
								{
									$fv['formattedValue'] = $ft->getConverter()->formatValue($fv['value']);
								}
							}
							$fieldsValues[] = $fv;
						}
					}
				}
				$restResult->setProperty('fieldsValues', $fieldsValues);
			}
		}
		parent::onDefaultUpdateRestResult($event);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultCreated(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}

		$arguments = ['responseId' => $this->getId()];
		$event->getApplicationServices()->getJobManager()->createNewJob('Rbs_Simpleform_Response_Added', $arguments, null, false);
	}

}
