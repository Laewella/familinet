(function() {
	"use strict";

	function rbsDocumentEditorRbsSimpleformFormEdit($timeout, REST) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onLoad = function() {
					if (angular.isObject(scope.document.export)) {
						scope.export = {};
						scope.export = angular.copy(scope.document.export);
						if (scope.export.jobId && !scope.export.fileURI) {
							scope.checkJob();
						}
					}
					else {
						scope.export = {};
					}
					scope.hasJobs = false;
				};
				scope.onReady = function() {
					if (!angular.isArray(scope.document.fields)) {
						scope.document.fields = [];
					}
				};
				scope.showExportButton = function() {
					return scope.document && scope.document.META$ && scope.document.META$.links &&
						scope.document.META$.links.export;

				};
				scope.launchExport = function() {
					REST.apiPost('resources/Rbs/Simpleform/' + scope.document.id + '/Export').then(
						function(result) {
							scope.export.jobId = result.jobId;
							scope.export.fileURI = null;
							scope.export.exportDate = null;
							scope.checkJob();
						},
						function(result) {
							console.error(result);
						}
					);
				};
				scope.checkJob = function() {
					scope.hasJobs = true;
					REST.job(scope.export.jobId).then(function(job) {
						scope.hasJobs = job && job.properties && job.properties.id == scope.export.jobId
							&& (job.properties.status == 'waiting' || job.properties.status == 'running');
						if (scope.hasJobs) {
							$timeout(scope.checkJob, 10000);
						}
						else {
							if (job.properties.status == 'success') {
								scope.export.jobId = job.properties.id;
								scope.export.fileURI = job.properties['arguments'].csvFile;
								scope.export.exportDate = job.properties.lastModificationDate;
							}
							else {
								scope.export = {};
							}
						}
					}, function() {
						scope.hasJobs = false;
					});
				};
			}
		};
	}

	angular.module('RbsChange').directive('rbsDocumentEditorRbsSimpleformFormEdit',
		['$timeout', 'RbsChange.REST', rbsDocumentEditorRbsSimpleformFormEdit]);

	function rbsSimpleformAsideFormResponse() {
		return {
			restrict: 'AE',
			templateUrl: 'Rbs/Simpleform/Documents/Form/aside-form-response.twig',
			link: function(scope) {
				scope.shouldBeShown = function() {
					return scope.document && scope.document.saveResponses;
				};
			}
		};
	}

	angular.module('RbsChange').directive('rbsSimpleformAsideFormResponse', rbsSimpleformAsideFormResponse);
})();