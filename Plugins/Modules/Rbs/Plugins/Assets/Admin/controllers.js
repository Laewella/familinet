(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * Controller for list.
	 *
	 * @param $scope
	 * @param Plugins
	 * @constructor
	 */
	function NewListController($scope, Plugins) {
		Plugins.getNew().then(
			function(data) { $scope.plugins = data; }
		);

		//sort
		$scope.predicate = 'vendor';
		$scope.reverse = false;
		$scope.isSortedOn = function(column) {
			return column == $scope.predicate;
		};
	}

	NewListController.$inject = ['$scope', 'RbsChange.Plugins'];
	app.controller('Rbs_Plugins_New_ListController', NewListController);

	/**
	 * Controller for list.
	 *
	 * @param $scope
	 * @param Plugins
	 * @constructor
	 */
	function RegisteredListController($scope, Plugins) {
		Plugins.getRegistered().then(
			function(data) { $scope.plugins = data; }
		);

		//sort
		$scope.predicate = 'vendor';
		$scope.reverse = false;
		$scope.isSortedOn = function(column) {
			return column == $scope.predicate;
		};
	}

	RegisteredListController.$inject = ['$scope', 'RbsChange.Plugins'];
	app.controller('Rbs_Plugins_Registered_ListController', RegisteredListController);

	/**
	 * Controller for list.
	 *
	 * @param $scope
	 * @param Plugins
	 * @constructor
	 */
	function InstalledListController($scope, Plugins) {
		Plugins.getInstalled().then(
			function(data) { $scope.plugins = data; }
		);

		//sort
		$scope.predicate = 'vendor';
		$scope.reverse = false;
		$scope.isSortedOn = function(column) {
			return column == $scope.predicate;
		};
	}

	InstalledListController.$inject =
		['$scope', 'RbsChange.Plugins'];
	app.controller('Rbs_Plugins_Installed_ListController', InstalledListController);
})();