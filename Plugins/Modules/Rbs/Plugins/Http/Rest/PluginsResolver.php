<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Plugins\Http\Rest;

/**
 * @name \Rbs\Plugins\Http\Rest\PluginsResolver
 */
class PluginsResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		return ['plugins.installedPlugins', 'plugins.registeredPlugins', 'plugins.newPlugins'];
	}

	/**
	 * Set Event params: resourcesActionName, documentId, LCID
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$nbParts = count($resourceParts);
		if ($nbParts == 0 && $method === \Change\Http\Rest\Request::METHOD_GET)
		{
			array_unshift($resourceParts, 'plugins');
			$event->setParam('namespace', implode('.', $resourceParts));
			$event->setParam('resolver', $this);
			$action = function ($event)
			{
				$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
				$action->execute($event);
			};
			$event->setAction($action);
			$event->setAuthorization(null);
			return;
		}
		elseif ($nbParts == 1)
		{
			$actionName = $resourceParts[0];
			if ($actionName === 'installedPlugins')
			{
				$action = new \Rbs\Plugins\Http\Rest\Actions\GetInstalledPlugins();
				$event->setAction(function ($event) use ($action) { $action->execute($event); });
				$authorisation = function () use ($event)
				{
					return $event->getPermissionsManager()->isAllowed('Administrator');
				};
				$event->setAuthorization($authorisation);
			}
			elseif ($actionName === 'registeredPlugins')
			{
				$action = new \Rbs\Plugins\Http\Rest\Actions\GetRegisteredPlugins();
				$event->setAction(function ($event) use ($action) { $action->execute($event); });
				$authorisation = function () use ($event)
				{
					return $event->getPermissionsManager()->isAllowed('Administrator');
				};
				$event->setAuthorization($authorisation);
			}
			elseif ($actionName === 'newPlugins')
			{
				$action = new \Rbs\Plugins\Http\Rest\Actions\GetNewPlugins();
				$event->setAction(function ($event) use ($action) { $action->execute($event); });
				$authorisation = function () use ($event)
				{
					return $event->getPermissionsManager()->isAllowed('Administrator');
				};
				$event->setAuthorization($authorisation);
			}
		}
	}
}