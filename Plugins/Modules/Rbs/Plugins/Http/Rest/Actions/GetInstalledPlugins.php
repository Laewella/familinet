<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Plugins\Http\Rest\Actions;

/**
 * @name \Rbs\Plugins\Http\Rest\Actions\GetInstalledPlugins
 */
class GetInstalledPlugins
{

	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$pm = $event->getApplicationServices()->getPluginManager();
		$array = [];
		$i = 0;
		foreach ($pm->getInstalledPlugins() as $plugin)
		{
			$array[] = $plugin->toArray();
			$array[$i]['registrationDate'] = $array[$i]['registrationDate']->format(\DateTime::ATOM);
			$i++;
		}

		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($array);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);

		$event->setResult($result);
	}
}