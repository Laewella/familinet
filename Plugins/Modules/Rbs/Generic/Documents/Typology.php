<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Documents;

/**
 * @name \Rbs\Generic\Documents\Typology
 */
class Typology extends \Compilation\Rbs\Generic\Documents\Typology
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('setLockedVisibilityContexts', [$this, 'onDefaultSetLockedVisibilityContexts'], 1);
	}

	public function preLoadAttributes()
	{
		$ids = [];
		foreach ($this->getGroups() as $group)
		{
			foreach ($group->getAttributesIds() as $id)
			{
				$ids[] = [$id, 'Rbs_Generic_Attribute'];
			}
		}
		if ($ids)
		{
			$this->getDocumentManager()->preLoad($ids);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		/** @var $document \Rbs\Generic\Documents\Typology */
		$document = $event->getDocument();
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$definitions = [];
			foreach ($document->getGroups() as $group)
			{
				foreach ($group->getAttributes() as $attribute)
				{
					$definitions[$attribute->getId()] = [
						'id' => $attribute->getId(),
						'name' => $attribute->getName() ?: ('attr_' . $attribute->getId()),
						'label' => $attribute->getLabel(),
						'type' => $attribute->getValueType(),
						'documentType' => $attribute->getDocumentType(),
						'usePicker' => $attribute->getUsePicker(),
						'required' => $attribute->getRequiredValue(),
						'defaultValue' => $attribute->getDefaultValue(),
						'localized' => $attribute->getLocalizedValue(),
						'collectionCode' => $attribute->getCollectionCode()
					];
				}
			}
			$restResult->setProperty('attributesDefinitions', $definitions);

			$visibilities = $document->getVisibilities();
			$em = $document->getEventManager();
			$args = $em->prepareArgs([
				'visibilities' => is_array($visibilities) ? $visibilities : [],
				'contexts' => is_array($restResult->getProperty('contexts')) ? $restResult->getProperty('contexts') : []
			]);
			$em->trigger('setLockedVisibilityContexts', $this, $args);
			$restResult->setProperty('contexts', $args['contexts']);
			$restResult->setProperty('visibilities', $args['visibilities']);
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$model = $event->getApplicationServices()->getModelManager()->getModelByName($document->getModelName());
			if ($model)
			{
				$i18n = $event->getApplicationServices()->getI18nManager();
				$restResult->setProperty('modelLabel', $i18n->trans($model->getLabelKey(), ['ucf']));
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultSetLockedVisibilityContexts(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if (!($document instanceof \Rbs\Generic\Documents\Typology))
		{
			return;
		}

		$contexts = $event->getParam('contexts');
		$visibilities = $event->getParam('visibilities');
		foreach ($contexts as $context)
		{
			$contextName = $context['name'];
			if (!isset($visibilities[$contextName]))
			{
				$visibilities[$contextName] = ['attributes' => []];
			}

			foreach ($document->getGroups() as $group)
			{
				foreach ($group->getAttributes() as $attribute)
				{
					if (!isset($visibilities[$contextName]['attributes'][(string)$attribute->getId()]))
					{
						$visibilities[$contextName]['attributes'][(string)$attribute->getId()] = false;
					}
				}
			}
			$attributes = array_filter($visibilities[$contextName]['attributes'],function($i){	return $i !== null;});
			if (!count($attributes))
			{
				$visibilities[$contextName]['attributes'] = null;
			}
			else
			{
				$visibilities[$contextName]['attributes'] = $attributes;
			}
		}
		$event->setParam('visibilities', $visibilities);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'modelName' && is_array($value))
		{
			$value = $value['name'];
		}
		return parent::processRestData($name, $value, $event);
	}
}
