<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\ProfileManager;

/**
 * @name \Rbs\Generic\Events\ProfileManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\User\ProfileManager::EVENT_LOAD, function ($event) { $this->onLoad($event); }, 5);
		$this->listeners[] = $events->attach(\Change\User\ProfileManager::EVENT_SAVE, function ($event) { $this->onSave($event); }, 5);
		$this->listeners[] = $events->attach(\Change\User\ProfileManager::EVENT_PROFILES, function ($event) { $this->onProfiles($event); }, 5);
		$this->listeners[] =
			$events->attach(\Change\User\ProfileManager::EVENT_PROFILE_FIELDS, function ($event) { $this->onGetProfileFields($event); }, 5);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onLoad(\Change\Events\Event $event)
	{
		$profileName = $event->getParam('profileName');
		if ($profileName === 'Change_User')
		{
			$profile = new \Change\User\UserProfile();
		}
		elseif ($profileName === 'Rbs_Admin')
		{
			$profile = new \Rbs\Admin\Profile\Profile();
		}
		elseif ($profileName === 'Rbs_User')
		{
			$profile = new \Rbs\User\Profile\Profile();
		}
		elseif ($profileName === 'Rbs_Website')
		{
			$profile = new \Rbs\Website\Profile\Profile();
		}
		elseif ($profileName === 'Rbs_Seo')
		{
			$profile = new \Rbs\Seo\Profile\Profile();
		}
		else
		{
			return;
		}

		$user = $event->getParam('user');
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices && $user instanceof \Change\User\UserInterface)
		{
			$docUser = $applicationServices->getDocumentManager()->getDocumentInstance($user->getId());
			if ($docUser instanceof \Rbs\User\Documents\User)
			{
				if ($profileName === 'Rbs_Seo')
				{
					$metaKey = 'profile_' . $profileName;
					$meta = $docUser->getMeta($metaKey);
					if (!is_array($meta))
					{
						$meta = [];
					}
					foreach ($profile->getPropertyNames() as $propertyName)
					{
						if (array_key_exists($propertyName, $meta))
						{
							$profile->setPropertyValue($propertyName, $meta[$propertyName]);
						}
					}
				}
				else
				{
					$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_User_Profile');
					$query->andPredicates($query->eq('user', $docUser));

					$documentProfile = $query->getFirstDocument();
					if ($documentProfile instanceof \Rbs\User\Documents\Profile)
					{
						if ($profileName === 'Change_User' && $documentProfile->getHasChangeUser())
						{
							$profile->setPropertyValue('LCID', $documentProfile->getDefaultLCID());
							$profile->setPropertyValue('TimeZone', $documentProfile->getDefaultTimeZone());
						}
						elseif ($profileName === 'Rbs_Admin' && $documentProfile->getHasRbsAdmin())
						{
							$profile->setPropertyValue('avatar', $documentProfile->getAdminAvatar());
							$profile->setPropertyValue('pagingSize', $documentProfile->getPagingSize());
							$profile->setPropertyValue('documentListViewMode', $documentProfile->getDocumentListViewMode());
							$profile->setPropertyValue('sendNotificationMailImmediately', $documentProfile->getSendNotificationMailImmediately());
							$profile->setPropertyValue('notificationMailInterval', $documentProfile->getNotificationMailInterval());
							$profile->setPropertyValue('notificationMailAt', $documentProfile->getNotificationMailAt());
							$profile->setPropertyValue('dateOfLastNotificationMailSent', $documentProfile->getDateOfLastNotificationMailSent());
						}
						elseif ($profileName === 'Rbs_User' && $documentProfile->getHasRbsUser())
						{
							$profile->setPropertyValue('firstName', $documentProfile->getFirstName());
							$profile->setPropertyValue('lastName', $documentProfile->getLastName());
							$profile->setPropertyValue('titleCode', $documentProfile->getTitleCode());
							$profile->setPropertyValue('phone', $documentProfile->getPhone());
							$profile->setPropertyValue('birthDate', $documentProfile->getBirthDate());
						}
						elseif ($profileName === 'Rbs_Website' && $documentProfile->getHasRbsWebsite())
						{
							$profile->setPropertyValue('pseudonym', $documentProfile->getWebPseudonym());
						}
					}
				}
			}
		}
		$event->setParam('profile', $profile);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onSave(\Change\Events\Event $event)
	{
		$profile = $event->getParam('profile');
		if (!($profile instanceof \Change\User\ProfileInterface))
		{
			return;
		}
		$profileName = $profile->getName();
		if (!in_array($profileName, ['Change_User', 'Rbs_Admin', 'Rbs_User', 'Rbs_Website', 'Rbs_Seo']))
		{
			return;
		}

		$user = $event->getParam('user');
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices && $user instanceof \Change\User\UserInterface)
		{
			$transactionManager = $applicationServices->getTransactionManager();
			try
			{
				$transactionManager->begin();
				$docUser = $applicationServices->getDocumentManager()->getDocumentInstance($user->getId());
				if ($docUser instanceof \Rbs\User\Documents\User)
				{
					if ($profileName === 'Rbs_Seo')
					{
						$metaKey = 'profile_' . $profileName;
						$meta = $docUser->getMeta($metaKey);
						if (!is_array($meta))
						{
							$meta = [];
						}
						foreach ($profile->getPropertyNames() as $propertyName)
						{
							$meta[$propertyName] = $profile->getPropertyValue($propertyName);
						}
						$docUser->setMeta($metaKey, $meta);
						$docUser->saveMetas();
					}
					else
					{
						$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_User_Profile');
						$query->andPredicates($query->eq('user', $docUser));

						/* @var $documentProfile \Rbs\User\Documents\Profile */
						$documentProfile = $query->getFirstDocument();
						if ($documentProfile === null)
						{
							$documentProfile = $applicationServices->getDocumentManager()
								->getNewDocumentInstanceByModelName('Rbs_User_Profile');
							$documentProfile->setUser($docUser);
						}

						if ($profileName === 'Change_User')
						{
							$documentProfile->setHasChangeUser(true);
							$documentProfile->setDefaultLCID($profile->getPropertyValue('LCID'));
							$documentProfile->setDefaultTimeZone($profile->getPropertyValue('TimeZone'));
						}
						elseif ($profileName === 'Rbs_Admin')
						{
							$documentProfile->setHasRbsAdmin(true);
							$documentProfile->setAdminAvatar($profile->getPropertyValue('avatar'));
							$documentProfile->setPagingSize($profile->getPropertyValue('pagingSize'));
							$documentProfile->setDocumentListViewMode($profile->getPropertyValue('documentListViewMode'));
							$documentProfile->setSendNotificationMailImmediately($profile->getPropertyValue('sendNotificationMailImmediately'));
							$documentProfile->setNotificationMailInterval($profile->getPropertyValue('notificationMailInterval'));
							$documentProfile->setNotificationMailAt($profile->getPropertyValue('notificationMailAt'));
							$documentProfile->setDateOfLastNotificationMailSent($profile->getPropertyValue('dateOfLastNotificationMailSent'));
						}
						elseif ($profileName === 'Rbs_User')
						{
							$documentProfile->setHasRbsUser(true);
							$documentProfile->setFirstName($profile->getPropertyValue('firstName'));
							$documentProfile->setLastName($profile->getPropertyValue('lastName'));
							$documentProfile->setTitleCode($profile->getPropertyValue('titleCode'));
							$documentProfile->setPhone($profile->getPropertyValue('phone'));
							$birthDate = $profile->getPropertyValue('birthDate');
							if (!$birthDate)
							{
								$documentProfile->setBirthDate(null);
							}
							else
							{
								$documentProfile->setBirthDate($birthDate);
							}
						}
						elseif ($profileName === 'Rbs_Website')
						{
							$documentProfile->setHasRbsWebsite(true);
							$documentProfile->setWebPseudonym($profile->getPropertyValue('pseudonym'));
						}

						$documentProfile->save();
					}
				}
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onProfiles(\Change\Events\Event $event)
	{
		$profiles = $event->getParam('profiles', []);
		$event->setParam('profiles', array_merge($profiles, ['Change_User', 'Rbs_User', 'Rbs_Admin', 'Rbs_Website', 'Rbs_Seo']));
	}

	/**
	 * @param \Change\Events\Event $event
	 * Event input param :
	 * - string profileFieldsName
	 * Event output param :
	 * - \Change\User\ProfileFieldInterface[] profileFields
	 */
	protected function onGetProfileFields(\Change\Events\Event $event)
	{
		if ($event->getParam('profileFields') !== null)
		{
			return;
		}
		$profileFields = [];

		switch ($event->getParam('profileFieldsName'))
		{
			case 'createAccount' :
			case 'userAccount' :
				$profileFields[] = new \Change\User\BaseProfileField('Rbs_User', 'titleCode');
				$profileFields[] = new \Change\User\BaseProfileField('Rbs_User', 'firstName');
				$profileFields[] = new \Change\User\BaseProfileField('Rbs_User', 'lastName');
				$profileFields[] = new \Change\User\BaseProfileField('Rbs_User', 'birthDate');
				$profileFields[] = new \Change\User\BaseProfileField('Rbs_User', 'phone');
				$profileFields[] = new \Change\User\BaseProfileField('Rbs_Website', 'pseudonym');
				break;
		}

		$event->setParam('profileFields', $profileFields);
	}
}