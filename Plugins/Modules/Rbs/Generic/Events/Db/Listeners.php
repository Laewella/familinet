<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Db;

/**
 * @name \Rbs\Generic\Events\Db\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			$predicateJSON = $event->getParam('predicateJSON');
			if (is_array($predicateJSON) && isset($predicateJSON['op']) && ucfirst($predicateJSON['op']) === 'HasTag')
			{
				$hasTag = new \Rbs\Tag\Db\Query\HasTag();
				$fragment = $hasTag->populate($predicateJSON, $event->getParam('JSONDecoder'), $event->getParam('predicateBuilder'));
				$event->setParam('SQLFragment', $fragment);
				$event->stopPropagation();
			}
		};
		$this->listeners[] = $events->attach(\Change\Db\DbProvider::EVENT_SQL_FRAGMENT, $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$fragment = $event->getParam('fragment');
			if ($fragment instanceof \Rbs\Tag\Db\Query\HasTag)
			{
				/** @noinspection PhpParamsInspection */
				$event->setParam('sql', $fragment->toSQLString($event->getTarget()));
				$event->stopPropagation();
			}
		};
		$this->listeners[] = $events->attach(\Change\Db\DbProvider::EVENT_SQL_FRAGMENT_STRING, $callback, 5);
	}
}