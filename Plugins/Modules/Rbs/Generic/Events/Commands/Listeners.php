<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Commands;

/**
 * @name \Rbs\Generic\Events\Commands\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Commands\Events\Event $event)
		{
			$commandConfigPath = __DIR__ . '/Assets/config.json';
			return json_decode(file_get_contents($commandConfigPath), 1);
		};
		$this->listeners[] = $events->attach('config', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Website\Commands\AddDefaultWebsite())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_website:add-default-website', $callback);

		$callback = function ($event)
		{
			(new \Rbs\User\Commands\AddUser())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_user:add-user', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Elasticsearch\Commands\Indices())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_elasticsearch:indices', $callback);

		$callback = function (\Change\Commands\Events\Event $event)
		{
			$event->getCommandResponse()->addWarningMessage('@deprecated since 1.9.0 use rbs_notification:install-notifications instead');
		};
		$this->listeners[] = $events->attach('rbs_mail:install-mails', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Notification\Commands\InstallNotifications())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_notification:install-notifications', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Notification\Commands\Configure())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_notification:configure', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Generic\Commands\InitializeWebsite())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_generic:initialize-website', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Website\Commands\RefreshPathRules())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_website:refresh-path-rules', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Generic\Commands\AttributeFilterIndex())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_generic:attribute-filter-index', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Generic\Commands\CheckStatus())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_generic:check-status', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Generic\Commands\ExportTypologies())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_generic:export-typologies', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Generic\Commands\ImportTypologies())->execute($event);
		};
		$this->listeners[] = $events->attach('rbs_generic:import-typologies', $callback);
	}
}