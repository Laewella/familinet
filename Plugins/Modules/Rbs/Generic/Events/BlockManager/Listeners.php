<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\BlockManager;

/**
 * @name \Rbs\Generic\Events\BlockManager\Listeners
 */
class Listeners implements \Zend\EventManager\ListenerAggregateInterface
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_Facets', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_InstantSearch', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_Result', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_ResultHeader', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_ShortSearch', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Geo_ManageAddresses', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Mail_HtmlFragment', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Mail_Richtext', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Mail_Text', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Mailinglist_ManageSubscriptions', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Mailinglist_ShortSubscription', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Mailinglist_Subscription', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Media_File', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Media_Image', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Media_Video', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Review_ReviewDetail', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Seo_FacetUrlDecorator', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Seo_HeadMetas', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Simpleform_Form', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Social_SocialButtons', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Social_SocialLinkAccount', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Theme_ThemeSelector', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Theme_ThemeSelectorMail', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_AccountShort', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_ChangeEmail', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_ChangeMobilePhone', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_ChangePassword', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_CreateAccount', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_EditAccount', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_Login', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_ManageAutoLoginToken', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_User_ResetPassword', true, $events);

		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Analytics', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Error', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Exception', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_HtmlFragment', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Interstitial', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Menu', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_ResponsiveSummary', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Richtext', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_SiteMap', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_SwitchLang', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Text', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_Thread', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_TrackersAskConsent', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_TrackersManage', true, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Website_XhtmlTemplate', true, $events);

		//@deprecated since 1.8.0 use Rbs_Catalog_ProductResult, Rbs_Catalog_SearchFacets, Rbs_Catalog_ProductListFacets
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_StoreResult', false, $events);
		new \Change\Presentation\Blocks\Standard\RegisterByBlockName('Rbs_Elasticsearch_StoreFacets', false, $events);
		$callback = function (\Change\Events\Event $event)
		{
			$catalogModule = $event->getApplicationServices()->getPluginManager()->getModule('Rbs', 'Catalog');
			$blockManager = $event->getTarget();
			if ($catalogModule && $blockManager instanceof \Change\Presentation\Blocks\BlockManager && $catalogModule->isAvailable())
			{
				$blockManager->registerBlock('Rbs_Elasticsearch_StoreResult', function () use ($blockManager, $event)
				{
					$class = new \Rbs\Elasticsearch\Blocks\StoreResultInformation('Rbs_Elasticsearch_StoreResult', $blockManager);
					$class->onInformation($event);
					return $class;
				});

				$blockManager->registerBlock('Rbs_Elasticsearch_StoreFacets', function () use ($blockManager, $event)
				{
					$class = new \Rbs\Elasticsearch\Blocks\StoreFacetsInformation('Rbs_Elasticsearch_StoreFacets', $blockManager);
					$class->onInformation($event);
					return $class;
				});
			}
		};
		$events->attach(\Change\Presentation\Blocks\BlockManager::EVENT_INFORMATION, $callback, 5);
		//@deprecated since 1.8.0 END
	}

	/**
	 * Detach all previously attached listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @return void
	 */
	public function detach(\Zend\EventManager\EventManagerInterface $events)
	{
	}
}
