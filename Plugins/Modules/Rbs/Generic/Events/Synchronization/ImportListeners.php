<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Synchronization;

/**
 * @name \Rbs\Generic\Events\Synchronization\ImportListeners
 * @ignore
 */
class ImportListeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$import = new \Rbs\Generic\Synchronization\Import();

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_RESOLVE,
			function (\Change\Synchronization\ImportEvent $event) use ($import)
			{
				$import->onResolve($event);
			}, 10);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_CREATE,
			function (\Change\Synchronization\ImportEvent $event) use ($import)
			{
				$import->onCreate($event);
			}, 10);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_POPULATE,
			function (\Change\Synchronization\ImportEvent $event) use ($import)
			{
				$import->onPopulateDocument($event);
				$import->onPopulateTypology($event);
			}, 10);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_SAVE,
			function (\Change\Synchronization\ImportEvent $event) use ($import)
			{
				$import->onSaveTypology($event);
				$import->onSaveTopic($event);
				$import->onUpdatePublicationStatus($event);
				$import->onSaveUserProfile($event);
				$import->onSaveAddress($event);
				$import->onSaveUrlRewriting($event);
			}, 1);
	}
}