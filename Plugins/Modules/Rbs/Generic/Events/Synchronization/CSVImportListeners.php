<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Synchronization;

/**
 * @name \Rbs\Commerce\Events\Synchronization\ImportListeners
 * @ignore
 */
class CSVImportListeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 * @param int $priority
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $eventManager, $priority = 1)
	{
		$csvEvents = new \Rbs\Generic\Synchronization\CSV\CSVEvents();

		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_GET_FILES,
				function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
				{
					$csvEvents->onPostGetFiles($event);
				}, 4);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_RESOLVE_TYPE,
			function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
			{
				$csvEvents->onResolveImportType($event);
			}, 11);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_ADD_TYPOLOGY,
			function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
			{
				$csvEvents->onAddDocumentTypology($event);
			}, 10);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_IMPORT,
				function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
				{
					$csvEvents->onImportValidatedLines($event);
				}, 10);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_PRE_IMPORT_FILE,
				function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
				{
					$csvEvents->onPreImport($event);
				}, 10);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_POST_IMPORT_FILE,
				function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
				{
					$csvEvents->onPostImport($event);
				}, 10);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_VALIDATE_LINE,
				function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
				{
					$csvEvents->onValidateLine($event);
				}, 10);
		$this->listeners[] = $eventManager->attach(\Change\Synchronization\CSV\CSVEngine::EVENT_GET_SORTED_IMPORT,
				function (\Change\Synchronization\CSV\CSVImportEvent $event) use ($csvEvents)
				{
					$csvEvents->onGetSortedImport($event);
				}, 9);
	}
}