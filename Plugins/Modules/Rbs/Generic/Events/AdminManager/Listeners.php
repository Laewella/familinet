<?php
/**
 * Copyright (C) 2014 Ready Business System, Gaël PORT
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\AdminManager;

/**
 * @name \Rbs\Generic\Events\AdminManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function ($event)
		{
			(new \Rbs\Timeline\Admin\GetRoutes())->execute($event);
			(new \Rbs\Seo\Admin\GetRoutes())->execute($event);
			(new \Rbs\Workflow\Admin\GetRoutes())->execute($event);
			(new \Rbs\Social\Admin\GetRoutes())->execute($event);
		};
		//Priority 1 (default value) to be sure to get the default routes
		$this->listeners[] = $events->attach('getRoutes', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Timeline\Admin\GetModelTwigAttributes())->execute($event);
			(new \Rbs\Seo\Admin\GetModelTwigAttributes())->execute($event);
			(new \Rbs\Tag\Admin\GetModelTwigAttributes())->execute($event);
			(new \Rbs\User\Admin\GetModelTwigAttributes())->execute($event);
			(new \Rbs\Website\Admin\GetModelTwigAttributes())->execute($event);
			(new \Rbs\Simpleform\Admin\GetModelTwigAttributes())->execute($event);
			(new \Rbs\Social\Admin\GetModelTwigAttributes())->execute($event);
		};
		//Priority 1 (default value) to be sure to get the default attributes
		$this->listeners[] = $events->attach('getModelTwigAttributes', $callback);

		$callback = function ($event)
		{
			(new \Rbs\Generic\Commands\InitializeWebsite())->getGenericSettingsStructures($event);
		};
		$this->listeners[] = $events->attach('getGenericSettingsStructures', $callback, 5);

		$callback = function ($event)
		{
			(new \Rbs\User\Admin\SearchDocuments())->execute($event);
			(new \Rbs\Geo\Admin\AdminManager())->onSearchDocument($event);
		};
		$this->listeners[] = $events->attach('searchDocuments', $callback, 10);
		$this->listeners[] = $events->attach('searchDocumentsQuery', $callback, 10);

		$callback = function ($event)
		{
			(new \Rbs\Geo\Admin\AdminManager())->onGetHomeAttributes($event);
		};
		$this->listeners[] = $events->attach('getHomeAttributes', $callback, 10);
	}
}