<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Documents;

/**
 * @name \Rbs\Generic\Events\Documents\Documents
 */
class Documents
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultSetLockedVisibilityContexts(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if (!($document instanceof \Rbs\Generic\Documents\Typology))
		{
			return;
		}

		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$label = $i18nManager->trans('m.rbs.generic.admin.attribute_context_list', ['ucf']);
		(new \Rbs\Generic\Events\Documents\LockedContextSetter())->set($event, 'list', true, $label);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}

		$dataComposer = $this->getDataComposer($event);
		if ($dataComposer)
		{
			$event->setParam('data', $dataComposer->toArray());
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @return \Change\Http\Ajax\V1\Traits\DataComposer|null
	 */
	protected function getDataComposer(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			/** @var \Change\Documents\AbstractDocument $document */
			return new \Rbs\Generic\Http\Ajax\V1\PublishedDataComposer($event);
		}
		elseif ($document instanceof \Change\Documents\AbstractDocument)
		{
			return new \Rbs\Generic\Http\Ajax\V1\DefaultDataComposer($event);
		}
		return null;
	}
}