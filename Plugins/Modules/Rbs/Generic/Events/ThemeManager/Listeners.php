<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\ThemeManager;

/**
 * @name \Rbs\Generic\Events\ThemeManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Theme\Events\ThemeResolver())->resolve($event);
		};
		$this->listeners[] = $events->attach(\Change\Presentation\Themes\ThemeManager::EVENT_LOADING, $callback, 1);

		$callback = function (\Change\Events\Event $event)
		{
			$resolver = new \Rbs\Theme\Events\MailTemplateResolver();
			return $resolver->resolve($event);
		};
		$this->listeners[] = $events->attach(\Change\Presentation\Themes\ThemeManager::EVENT_MAIL_TEMPLATE_LOADING, $callback, 1);

		$callback = function (\Change\Events\Event $event)
		{
			$themeManagerEvents = new \Rbs\Geo\Presentation\ThemeManagerEvents();
			$themeManagerEvents->onAddPageResources($event);
		};
		$this->listeners[] = $events->attach(\Change\Presentation\Themes\ThemeManager::EVENT_ADD_PAGE_RESOURCES, $callback);
	}
}