<?php
/**
 * Copyright (C) 2014 Ready Business System, Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events;

/**
 * @name \Rbs\Generic\Events\SharedListeners
 */
class SharedListeners
{
	/**
	 * @var \Rbs\Elasticsearch\Indices\DocumentIndexing
	 */
	protected $documentIndexing;

	/**
	 * @var \Rbs\Generic\Attributes\ToIndexDocuments
	 */
	protected $toIndexAttributes;

	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$events->attach('Application', 'setServices', function ($event)
		{
			if ($event instanceof \Change\Events\Event)
			{
				$genericServices = new \Rbs\Generic\GenericServices($event->getApplication(), $event->getApplicationServices());
				$event->getServices()->set('genericServices', $genericServices);
			}
			return true;
		}, 9998);

		$events->attach('TransactionManager', 'begin', function ($event)
		{
			if (!$this->documentIndexing)
			{
				$this->documentIndexing = new \Rbs\Elasticsearch\Indices\DocumentIndexing();
			}
			$this->documentIndexing->start($event);

			if (!$this->toIndexAttributes)
			{
				$this->toIndexAttributes = new \Rbs\Generic\Attributes\ToIndexDocuments();
			}
			$this->toIndexAttributes->start($event);
		});

		$callback = function ($event) use (&$documentIndexing)
		{
			if ($this->documentIndexing && $event instanceof \Change\Documents\Events\Event)
			{
				$this->documentIndexing->addDocument($event);
			}
		};
		foreach (['documents.created', 'documents.localized.created', 'documents.updated',
			'documents.deleted', 'documents.localized.deleted', 'documents.attributes.changed', 'documents.attributes.deleted'] as $name)
		{
			$events->attach('Documents', $name, $callback, 5);
		}

		$callback = function ($event)
		{
			if ($this->toIndexAttributes)
			{
				$this->toIndexAttributes->indexDocument($event);
			}
		};
		$events->attach('Documents', 'documents.attributes.changed', $callback, 5);
		$events->attach('Documents', 'documents.attributes.deleted', $callback, 5);

		$events->attach('TransactionManager', 'commit', function ($event)
		{
			if ($this->documentIndexing)
			{
				$this->documentIndexing->end($event);
			}

			if ($this->toIndexAttributes)
			{
				$this->toIndexAttributes->addJob($event);
			}
		}, 10);
	}
}