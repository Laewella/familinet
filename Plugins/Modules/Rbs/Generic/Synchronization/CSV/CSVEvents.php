<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Synchronization\CSV;

/**
 * @name \Rbs\Generic\Synchronization\CSV\CSVEvents
 */
class CSVEvents
{
	const TYPE_ATTRIBUTES = 'beneficiations';
	const TYPE_IMAGES = 'images';
	const TYPE_METAS = 'metas';
	const TYPE_SECTIONS = 'sections';
	const TYPE_URLS = 'urls';
	const TYPE_VISUALS = 'visuals';
	const TYPE_VISUALS_URLS = 'visual_urls';
	const TYPE_USERS = 'users';
	const TYPE_OPTIN = 'optins';
	const TYPE_USERS_ADDRESSES = 'user_addresses';
	const TYPE_COLLECTIONS = 'collections';

	const FILE_PATTERN = '_[a-zA-Z0-9-\-]{1,}.csv';
	const IMAGES_FILE_PATTERN = '[a-zA-Z0-9-\-_]{1,}.(jpg|jpeg|png)';
	const I18N_PART = '_i18n';
	const JSON_EXT = '.json';

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onGetSortedImport(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		if ($event->getParam('sortedImport') !== null)
		{
			$sortedImport = $event->getParam('sortedImport');
			array_unshift($sortedImport, self::TYPE_USERS, self::TYPE_SECTIONS, self::TYPE_VISUALS_URLS, self::TYPE_VISUALS, self::TYPE_COLLECTIONS,
				self::TYPE_USERS_ADDRESSES,
				self::TYPE_METAS, self::TYPE_URLS, self::TYPE_OPTIN);
			array_push($sortedImport, self::TYPE_ATTRIBUTES);
		}
		else
		{
			$sortedImport =
				[self::TYPE_USERS, self::TYPE_SECTIONS, self::TYPE_VISUALS_URLS, self::TYPE_VISUALS, self::TYPE_COLLECTIONS, self::TYPE_USERS_ADDRESSES,
					self::TYPE_METAS, self::TYPE_URLS, self::TYPE_ATTRIBUTES, self::TYPE_OPTIN
				];
		}
		$event->setParam('sortedImport', $sortedImport);
	}

	public function onResolveImportType(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		if ($event->getParam('validationFile') && $event->getParam('filePattern'))
		{
			return;
		}
		$type = $event->getParam('type');
		$type = strtolower($type);
		switch ($type)
		{
			case self::TYPE_ATTRIBUTES :
				$modelName = 'Rbs_Generic_Attribute';
				break;
			case self::TYPE_METAS :
				$modelName = 'Rbs_Seo_Meta';
				break;
			case self::TYPE_SECTIONS :
				$modelName = 'Rbs_Website_Topic';
				break;
			case self::TYPE_URLS :
				$modelName = 'Rbs_Seo_Url';
				break;
			case self::TYPE_VISUALS_URLS:
				$modelName = 'Rbs_Media_Image_URL';
				break;
			case self::TYPE_VISUALS:
				$modelName = 'Rbs_Media_Image';
				break;
			case self::TYPE_USERS:
				$modelName = 'Rbs_User_User';
				break;
			case self::TYPE_USERS_ADDRESSES:
				$modelName = 'Rbs_Geo_Address';
				break;
			case self::TYPE_COLLECTIONS:
				$modelName = 'Rbs_Collection_Collection';
				break;
			case self::TYPE_OPTIN:
				$modelName = 'Rbs_Mailinglist_Subscriber';
				break;
			default :
				$modelName = null;
				break;
		}
		if ($modelName)
		{
			$file = $type . self::JSON_EXT;
			$filePattern = $type . self::FILE_PATTERN;
			$workspace = $event->getApplication()->getWorkspace();
			$pluginManager = $event->getApplicationServices()->getPluginManager();
			$pluginPath = $pluginManager->getPlugin('module', 'Rbs', 'Generic')->getAbsolutePath();
			$file = $workspace->composePath($pluginPath, 'Synchronization', 'CSV', 'Assets', $file);
			$event->setParam('validationFile', $file);
			$event->setParam('filePattern', $filePattern);
			$event->setParam('modelName', $modelName);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onPreImport(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$modelName = $event->getParam('modelName');
		switch ($modelName)
		{
			case 'Rbs_Media_Image' :
				$this->importMediaFile($event);
				break;
			case 'Rbs_Seo_Meta':
				$dbProvider = $event->getApplicationServices()->getDbProvider();
				if (!($dbProvider instanceof \Change\Db\Mysql\DbProvider))
				{
					// Error / Exception
					return;
				}
				$this->deleteMetaTable($dbProvider->getDriver());
				break;
			default:
				break;
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onValidateLine(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$modelName = $event->getParam('modelName', '');
		$csvLine = $event->getParam('csvLine', []);
		if ($event->getParam('isValid') === true)
		{
			return;
		}
		$event->setParam('isValid', false);
		switch ($modelName)
		{
			case 'Rbs_Generic_Attribute':
			case 'Rbs_Website_Topic':
			case 'Rbs_Storelocator_Store':
			case 'Rbs_Media_Image':
			case 'Rbs_User_User':
			case 'Rbs_Geo_Address':
			case 'Rbs_Collection_Collection':
			case 'Rbs_Media_Image_URL':
			case 'Rbs_Mailinglist_Subscriber':
				$event->setParam('isValid', true);
				break;
			case 'Rbs_Seo_Meta':
				$dbProvider = $event->getApplicationServices()->getDbProvider();
				if (!($dbProvider instanceof \Change\Db\Mysql\DbProvider))
				{
					$event->getApplication()->getLogging()->error('DbProvider must be a \Change\Db\Mysql\DbProvider instance.');
					break;
				}
				$line = $this->formatAttributeLine($dbProvider->getDriver(), $csvLine);
				if ($line)
				{
					$event->setParam('isValid', true);
					$event->setParam('csvLine', $line);
				}
				break;
			case 'Rbs_Seo_Url':
				$line = $this->formatUrlLine($csvLine);
				if ($line)
				{
					$event->setParam('isValid', true);
					$event->setParam('csvLine', $line);
				}
				break;
			default:
				break;
		}
		if ($LCID = $this->getLineLang($csvLine))
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$LCIDs = $i18nManager->getSupportedLCIDs();
			if (!in_array($LCID, $LCIDs))
			{
				$event->setParam('isValid', false);
				$event->getCSVEngine()->pushErrors(2, 'LCID ' . $LCID
					. ' not found in project configuration. Please ensure that format is like fr_FR, en_US, ...');
			}
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onImportValidatedLines(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$modelName = $event->getParam('modelName');
		$validatedLines = $event->getParam('validatedLines');
		if (!$modelName || !$validatedLines)
		{
			return;
		}
		switch ($modelName)
		{
			case 'Rbs_Website_Topic':
				$this->createOrUpdateSection($event);
				break;
			case 'Rbs_Media_Image':
				$this->updateMedia($event);
				break;
			case 'Rbs_Media_Image_URL':
				$this->createMediaFromStream($event);
				break;
			case 'Rbs_Seo_Meta' :
				$dbProvider = $event->getApplicationServices()->getDbProvider();
				$logging = $event->getApplication()->getLogging();
				if (!($dbProvider instanceof \Change\Db\Mysql\DbProvider))
				{
					$logging->error('DbProvider must be a \Change\Db\Mysql\DbProvider instance.');
					break;
				}
				$this->insertRawMetas($dbProvider->getDriver(), $validatedLines);
				break;
			case 'Rbs_Generic_Attribute':
				$this->createOrUpdateAttribute($event);
				break;
			case 'Rbs_Seo_Url':
				$this->createUrlsRedirections($event);
				break;
			case 'Rbs_User_User':
				$this->createOrUpdateUser($event);
				break;
			case 'Rbs_Geo_Address':
				$this->createOrUpdateAddresses($event);
				break;
			case 'Rbs_Collection_Collection':
				$this->createOrUpdateCollections($event);
				break;
			case 'Rbs_Mailinglist_Subscriber':
				$this->createOrUpdateSubscribers($event);
				break;
			default:
				break;
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onPostImport(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		if ($event->getParam('modelName') === 'Rbs_Seo_Meta')
		{
			$applicationServices = $event->getApplicationServices();
			$this->addMetaCode($applicationServices->getTransactionManager(), $event->getCSVEngine(), $applicationServices->getDbProvider());
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onPostGetFiles(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$modelName = $event->getParam('modelName');
		if ($modelName === null)
		{
			return;
		}
		$i18nFilePattern = null;
		switch ($modelName)
		{
			case 'Rbs_Website_Topic' :
				$i18nFilePattern = self::TYPE_SECTIONS . self::I18N_PART . self::FILE_PATTERN;
				break;
			case 'Rbs_Media_Image' :
				$i18nFilePattern = self::TYPE_VISUALS . self::I18N_PART . self::FILE_PATTERN;
				break;
			default :
				break;
		}
		$files = [];
		if ($i18nFilePattern)
		{
			$storageName = $event->getParam('storageName', 'synchro');
			$storageManager = $event->getApplicationServices()->getStorageManager();
			if ($storageManager->getStorageByName($storageName))
			{
				$ftpPath = $storageManager->buildChangeURI($storageName, '/in' . $event->getTarget()->getAdditionalFilePart())
					->toString();
				$tmpInFiles = array_diff(scandir($ftpPath), ['.', '..']);
				foreach ($tmpInFiles as $file)
				{
					if (preg_match('/^' . $i18nFilePattern . '$/i', $file))
					{
						$files[] = $ftpPath . $file;
					}
				}
				$newFiles = array_merge($event->getParam('files', []), $files);
				$event->setParam('files', $newFiles);
			}
		}
		$i18nFiles = array_merge($event->getParam('i18nFiles', []), $files);
		$event->setParam('i18nFiles', $i18nFiles);
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function createOrUpdateSection(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName');
		$topics = $event->getParam('validatedLines');
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$topics)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or sections.');
			return;
		}
		$model = $modelManager->getModelByName($modelName);
		$documentManager = $applicationServices->getDocumentManager();
		try
		{
			$transactionManager->begin();
			$import = new \Rbs\Generic\Synchronization\Import();
			foreach ($topics as $topic)
			{
				$LCID = $this->getLineLang($topic);
				if (!$LCID)
				{
					continue;
				}
				$documentManager->pushLCID($LCID);
				$topic['publicationstatus'] = isset($topic['publicationstatus']) ? $topic['publicationstatus'] : null;
				$topic['label'] = isset($topic['label']) && $topic['label'] ? $topic['label'] : $topic['title'];
				$publicationStatus = $csvEngine->getPublicationStatus($topic['publicationstatus']);
				unset($topic['publicationstatus']);
				$code = (string)$topic['code'];
				$doc = $csvEngine->resolveDocument($code, $model);
				if (!$doc)
				{
					$doc = $documentManager->getNewDocumentInstanceByModel($model);
				}
				/** @var \Rbs\Website\Documents\Topic $doc */
				$doc = $csvEngine->populateDocumentData($topic, $doc);
				if (isset($topic['parent']) && $topic['parent'])
				{
					$section = $csvEngine->resolveDocument(trim($topic['parent']), 'Rbs_Website_Section');
					if ($section instanceof \Rbs\Website\Documents\Website)
					{
						$doc->setWebsite($section);
					}
					else if ($section instanceof \Rbs\Website\Documents\Topic)
					{
						$doc->setSection($section);
					}
				}
				$doc->save();
				if (!empty($topic['productlist']) && $topic['productlist'] === '1')
				{
					if ($event->getApplicationServices()->getPluginManager()->getModule('Rbs', 'Catalog')->isAvailable())
					{
						$sectionProductList = $this->getSectionProductList($documentManager, $doc);
						if (!$sectionProductList)
						{
							/** @var \Rbs\Catalog\Documents\SectionProductList $sectionProductList */
							$sectionProductList = $documentManager->getNewDocumentInstanceByModelName('Rbs_Catalog_SectionProductList');
							$sectionProductList->setLabel($doc->getLabel());
							$sectionProductList->setSynchronizedSection($doc);
							$sectionProductList->save();
						}
						if (!$doc->getIndexPage())
						{
							$indexPage = $csvEngine->resolveDocument('Contextual_ProductList_FunctionalPage', 'Rbs_Website_FunctionalPage');
							if ($indexPage && $indexPage instanceof \Rbs\Website\Documents\Page)
							{
								$doc->setIndexPage($indexPage);
								/** @var \Rbs\Website\Documents\SectionPageFunction $sectionPageFunction */
								$sectionPageFunction = $documentManager->getNewDocumentInstanceByModelName('Rbs_Website_SectionPageFunction');
								$sectionPageFunction->setFunctionCode('Rbs_Website_Section');
								$sectionPageFunction->setLabel($doc->getLabel());
								$sectionPageFunction->setPage($indexPage);
								$sectionPageFunction->setSection($doc);
								$doc->save();
								$sectionPageFunction->save();
							}
							else
							{
								$csvEngine->pushErrors(3, 'Missing synchronization code "Contextual_ProductList_FunctionalPage" on a functional page.');
							}
						}
					}
				}
				$applicationServices->getDocumentCodeManager()->addDocumentCode($doc, $code, 'Synchronization');
				$import->publish($doc, $LCID, $publicationStatus, $documentManager);
				if (isset($topic['typology']) && $topic['typology'])
				{
					$csvEngine->addTypology($doc, $topic['typology']);
				}
				$documentManager->popLCID();
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$documentManager->popLCID();
			$event->getApplication()->getLogging()->exception($e);
			$transactionManager->rollBack($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function createOrUpdateUser(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$users = $event->getParam('validatedLines', []);
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$users)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or users.');
			return;
		}
		$model = $modelManager->getModelByName($modelName);
		$documentManager = $applicationServices->getDocumentManager();
		$transactionManager = $applicationServices->getTransactionManager();
		$logging = $event->getApplication()->getLogging();
		try
		{
			$transactionManager->begin();
			foreach ($users as $user)
			{
				$user['email'] = strtolower($user['email']);
				$dqb = $documentManager->getNewQuery('Rbs_User_User');
				$dqb->andPredicates($dqb->like('email', $dqb->getFragmentBuilder()->string($user['email'])));
				/** @var \Rbs\User\Documents\User $doc */
				$doc = $dqb->getFirstDocument();
				if (!$doc)
				{
					$doc = $documentManager->getNewDocumentInstanceByModel($model, true);
				}
				if ($doc->isNew() || $user['force'] === '1')
				{
					if (!empty($user['password']))
					{
						$doc->setPasswordHash($user['password']);
						unset($user['password']);
					}
					elseif (!empty($user['clearpassword']))
					{
						$doc->setPassword($user['clearpassword']);
						unset($user['clearpassword']);
					}
				}
				else
				{
					unset($user['password']);
				}
				$user['hashmethod'] = empty($user['hashmethod']) ? 'bcrypt' : $user['hashmethod'];
				if (!empty($user['groups']))
				{
					$groupDocuments = [];
					$groups = explode('|', $user['groups']);
					foreach ($groups as $group)
					{
						if ($groupDocument = $csvEngine->resolveDocument($group, 'Rbs_User_Group'))
						{
							$groupDocuments[] = $groupDocument;
						}
					}
					if ($groupDocuments)
					{
						$doc->setGroups($groupDocuments);
					}
				}
				$doc = $event->getCSVEngine()->populateDocumentData($user, $doc);
				$doc->save();
				if (isset($user['typology']) && $user['typology'])
				{
					$csvEngine->addTypology($doc, $user['typology']);
				}

				if (array_key_exists('fidelity', $user))
				{
					$fidelity = $user['fidelity'] ?? null;
					$profileManager = $applicationServices->getProfileManager();
					$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($doc);
					$profile = $profileManager->loadProfile($authenticatedUser, 'Rbs_Commerce');
					if ($profile instanceof \Rbs\Commerce\Std\Profile)
					{
						$profile->setFidelityCardNumber($fidelity);
						$profileManager->saveProfile($authenticatedUser, $profile);
					}
				}
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$transactionManager->rollBack($e);
			$logging->exception($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 * @throws \Change\Transaction\RollbackException
	 */
	protected function createOrUpdateCollections(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$collections = $event->getParam('validatedLines', []);
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$collections)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or collections.');
			return;
		}
		$model = $modelManager->getModelByName($modelName);
		$documentManager = $applicationServices->getDocumentManager();
		$transactionManager = $applicationServices->getTransactionManager();
		$logging = $event->getApplication()->getLogging();
		try
		{
			$transactionManager->begin();
			foreach ($collections as $collection)
			{
				$LCID = $this->getLineLang($collection);
				if (!$LCID)
				{
					continue;
				}
				$documentManager->pushLCID($LCID);
				$doc = $csvEngine->resolveDocument($collection['code'], $model);
				if (!$doc)
				{
					$documentManager->popLCID();
					continue;
				}
				if ($doc instanceof \Rbs\Collection\Documents\ObjectCollection)
				{
					if ($doc->getItemsDefinition() === 'rbsCollectionObjectCollectionDefinitionColor'
						|| $doc->getItemsDefinition()
						=== 'rbsCollectionObjectCollectionDefinitionColorVisual'
					)
					{
						$add = false;
						$item = $doc->getItemByValue($collection['value']);
						if (!$item)
						{
							$item = $doc->newCollectionItem();
							$item->setValue($collection['value']);
							$add = true;
						}
						if ($item->getRefLCID() !== $LCID)
						{
							$item->getCurrentLocalization()->setTitle($collection['title']);
							$doc->save();
						}
						else
						{
							$item->getCurrentLocalization()->setTitle($collection['title']);
							$data = $item->getData() ?: [];
							if ($doc->getItemsDefinition() === 'rbsCollectionObjectCollectionDefinitionColorVisual')
							{
								if ($file = $csvEngine->resolveDocument($collection['visual']))
								{
									$data['visual'] = $file->getId();
								}
							}
							if ($collection['colorcode'] ?? false && $collection['colorcode'] !== '')
							{
								$data['colorCode'] = $collection['colorcode'];
							}
							$item->setData($data);
							if ($add)
							{
								$doc->getItems()->add($item);
							}
							$doc->save();
						}
					}
				}
				elseif ($doc instanceof \Rbs\Collection\Documents\Collection)
				{
					$item = $doc->getItemByValue($collection['value']);
					if (!$item)
					{
						$item = $doc->newCollectionItem();
						$item->setValue($collection['value']);
					}
					$item->getCurrentLocalization()->setTitle($collection['title']);
					$doc->getItems()->add($item);
					$doc->save();
				}
				$documentManager->popLCID();
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$transactionManager->rollBack($e);
			$logging->exception($e);
			$documentManager->popLCID();
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function importMediaFile(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$storageName = $event->getParam('storageName');
		$storageManager = $applicationServices->getStorageManager();
		$csvEngine = $event->getCSVEngine();
		if (!$storageManager->getStorageByName($storageName))
		{
			return;
		}

		$ftpPath = $storageManager->buildChangeURI($storageName, '/in' . $event->getTarget()->getAdditionalFilePart())->toString();
		$tmpInFiles = array_diff(scandir($ftpPath), ['.', '..']);
		$files = [];
		foreach ($tmpInFiles as $file)
		{
			if (preg_match('/^' . self::IMAGES_FILE_PATTERN . '$/i', $file))
			{
				$files[] = $ftpPath . $file;
			}
		}
		if (!$modelName || !$files)
		{
			return;
		}
		$model = $modelManager->getModelByName($modelName);
		$tm = $applicationServices->getTransactionManager();
		$authorizedExtensions = ['jpg', 'jpeg', 'png'];
		try
		{
			$documentManager = $applicationServices->getDocumentManager();
			$documentCodeManager = $applicationServices->getDocumentCodeManager();
			if ($applicationServices->getStorageManager()->getStorageByName('imagesSynchro'))
			{
				$imageStorage = $applicationServices->getStorageManager()->buildChangeURI('imagesSynchro', '')->toString();

				foreach (array_chunk($files, 10) as $chunk)
				{
					$tm->begin();
					foreach ($chunk as $file)
					{
						$pathInfo = pathinfo($file);
						$fileName = $pathInfo['filename'];
						$fileInfo = null;
						if (in_array(strtolower($pathInfo['extension']), $authorizedExtensions) && preg_match('/[a-zA-Z0-9\-_\.]{1,}/i', $fileName))
						{
							$image = [
								'importPath' => $file,
								'code' => $fileName,
								'basename' => (string)$pathInfo['basename']
							];
						}
						else
						{
							continue;
						}
						/** @var \Rbs\Media\Documents\Image $doc */
						$doc = $csvEngine->resolveDocument($image['code'], $model);
						$isNew = false;
						if (!$doc)
						{
							$doc = $documentManager->getNewDocumentInstanceByModel($model);
							$isNew = true;
						}

						$sha = sha1(file_get_contents($image['importPath']));
						$image['label'] = isset($image['label']) ?: $image['basename'];
						$newPath = $imageStorage . '/' . $sha . '_' . $image['basename'];
						if ($doc->getPath() !== $newPath)
						{
							copy($image['importPath'], $newPath);
							$image['path'] = $newPath;
							$doc = $csvEngine->populateDocumentData($image, $doc);
						}
						$doc->save();
						if ($isNew)
						{
							$doc->updateActivationStatus(true);
						}
						$documentCodeManager->addDocumentCode($doc, $image['code'], 'Synchronization');
						unlink($image['importPath']);
					}
					$tm->commit();
				}
			}
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
			$event->getApplication()->getLogging()->exception($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function createMediaFromStream(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$images = $event->getParam('validatedLines', []);
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$images)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or visuals.');
			return;
		}
		$model = $modelManager->getModelByName('Rbs_Media_Image');
		$tm = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();
		try
		{
			$tm->begin();
			if ($applicationServices->getStorageManager()->getStorageByName('imagesSynchro'))
			{
				$imageStorage = $applicationServices->getStorageManager()->buildChangeURI('imagesSynchro', '')->toString();
				$authorizedExtensions = ['jpg', 'jpeg', 'png'];
				foreach ($images as $image)
				{
					$pathInfo = pathinfo($image['url']);
					$fileName = $pathInfo['filename'];
					if (in_array(strtolower($pathInfo['extension']), $authorizedExtensions) && preg_match('/[a-zA-Z0-9\-_\.]{1,}/i', $fileName))
					{
						$image = [
							'importPath' => $image['url'],
							'code' => $fileName,
							'basename' => (string)$pathInfo['basename']
						];
						if (empty($image['code']))
						{
							$image['code'] = $fileName;
						}
					}
					else
					{
						continue;
					}

					// Only if local storage !
					$fileContent = @file_get_contents($image['importPath'], 'r');
					if ($fileContent)
					{
						/** @var \Rbs\Media\Documents\Image $doc */
						$doc = $csvEngine->resolveDocument($image['code'], $model);
						$isNew = false;
						if (!$doc)
						{
							$doc = $documentManager->getNewDocumentInstanceByModel($model);
							$isNew = true;
						}
						$sha = sha1($fileContent);
						$image['label'] = isset($image['label']) ?: $image['basename'];
						$newPath = $imageStorage . '/' . $sha . '_' . $image['basename'];
						copy($image['importPath'], $newPath);
						$image['path'] = $newPath;

						$doc = $csvEngine->populateDocumentData($image, $doc);
						$doc->save();
						if ($isNew)
						{
							$doc->updateActivationStatus(true);
						}
						$documentCodeManager->addDocumentCode($doc, $image['code'], 'Synchronization');
					}
				}
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
			$event->getApplication()->getLogging()->exception($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function updateMedia(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$images = $event->getParam('validatedLines', []);
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$images)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or medias.');
			return;
		}
		$model = $modelManager->getModelByName($modelName);
		$tm = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		try
		{
			$tm->begin();
			foreach ($images as $image)
			{
				$lcid = $this->getLineLang($image);
				$documentManager->pushLCID($lcid);
				if (!isset($image['alt']))
				{
					$image['alt'] = $image['title'];
				}
				$doc = $csvEngine->resolveDocument($image['code'], $model);
				if (!$doc)
				{
					continue;
				}
				$csvEngine->populateDocumentData($image, $doc);
				$doc->save();
				if (isset($image['typology']) && $image['typology'])
				{
					$csvEngine->addTypology($doc, $image['typology']);
				}
				$documentManager->popLCID();
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$documentManager->popLCID($e);
			$tm->rollBack($e);
			$event->getApplication()->getLogging()->exception($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function createOrUpdateAttribute(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$rawAttributes = $event->getParam('validatedLines', []);
		$csvEngine = $event->getCSVEngine();
		if (!$rawAttributes)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or attributes.');
		}
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		foreach ($rawAttributes as $lineIndex => $rawAttribute)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();
				$document = $csvEngine->resolveDocument($rawAttribute['code']);
				if ($document)
				{
					$typology = $documentManager->getTypologyByDocument($document);
					if ($typology)
					{
						/** @var \Rbs\Generic\Documents\Typology $typologyDocument */
						$typologyDocument = $documentManager->getDocumentInstance($typology->getId());
						if ($typologyDocument)
						{
							$LCID = $rawAttribute['lcid'];
							$typologyValues = [];
							$attributeValues = new \Change\Documents\Attributes\AttributeValues($LCID);
							$attributesByName = $this->extractNamedAttributes($typologyDocument);
							foreach ($attributesByName as $attributeName => $attribute)
							{
								$itemTypologyData = $this->formatRawAttributeLine($rawAttribute, $attribute);
								if (array_key_exists($attributeName, $itemTypologyData))
								{
									$attributeValues->set($attributeName,
										$this->normalizeAttributeValue($csvEngine, $attribute, $itemTypologyData[$attributeName]));
									$affectedAttributes[] = $rawAttribute;
								}
							}
							$typologyValues[] = $attributeValues;
							foreach ($typologyValues as $attributeValues)
							{
								if ($attributeValues instanceof \Change\Documents\Attributes\AttributeValues)
								{
									$documentManager->pushLCID($attributeValues->getLCID());
									$documentManager->saveAttributeValues($document, $typology, $attributeValues);
									$document->save();
									$documentManager->popLCID();
								}
							}
						}
					}
					else
					{
						$csvEngine->pushErrors(4, 'Typology for document ' . $rawAttribute['code'] . ' can\'t be found at line ' . $lineIndex);
					}
				}
				else
				{
					$csvEngine->pushErrors(4, 'Document with code ' . $rawAttribute['code'] . ' can\'t be found at line ' . $lineIndex);
				}
				$tm->commit();
			}
			catch (\Exception $e)
			{
				$documentManager->popLCID($e);
				$tm->rollBack($e);
				$event->getApplication()->getLogging()->exception($e);
				$event->setParam('hasError', true);
			}
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 * @throws \Change\Transaction\RollbackException
	 */
	protected function createUrlsRedirections(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$urlsRedirection = $event->getParam('validatedLines');
		$hashes = array_column($urlsRedirection, 'hash');
		$applicationServices = $event->getApplicationServices();
		$pathRuleManager = $applicationServices->getPathRuleManager();
		$dbProvider = $applicationServices->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('hash'));
		$qb->from('change_path_rule');
		$qb->where($fb->in('hash', $hashes));
		$query = $qb->query();
		$rc = $query->getRowsConverter();
		$existingHashes = $query->getResults($rc->addStrCol('hash'));
		$csvEngine = $event->getCSVEngine();
		foreach ($existingHashes as $existingHash)
		{
			if (isset($urlsRedirection[$existingHash]))
			{
				unset($urlsRedirection[$existingHash]);
			}
		}
		if (!count($urlsRedirection))
		{
			return;
		}
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			foreach ($urlsRedirection as $rule)
			{
				if (!in_array($rule['status'], ['200', '301', '302']))
				{
					$csvEngine->pushErrors(4, 'Status for rule must in a 301 or 302');
					continue;
				}
				$website = $csvEngine->resolveDocument($rule['website'], 'Rbs_Website_Website');
				if (!$website)
				{
					$csvEngine->pushErrors(4, 'Website with code ' . $rule['website'] . ' cannot be found.');
					continue;
				}
				$rule['website'] = $website->getId();
				if ($rule['type'] === 'Doc')
				{
					$document = $csvEngine->resolveDocument($rule['target']);
					if (!$document instanceof \Change\Documents\Interfaces\Publishable)
					{
						$csvEngine->pushErrors(4, 'Target is a Document with code ' . $rule['target'] . ' and cannot be found.');
						continue;
					}
					$rule['target'] = $document->getId();
					if (!empty($rule['section']))
					{
						$section = $csvEngine->resolveDocument($rule['section'], 'Rbs_Website_Section');
						$rule['section'] = $section ? $section->getId() : null;
					}
				}
				else
				{
					$qb = $dbProvider->getNewQueryBuilder();
					$fb = $qb->getFragmentBuilder();
					$qb->select($fb->column('document_id'));
					$qb->from('change_path_rule');
					$subqb = $dbProvider->getNewQueryBuilder();
					$subqb->select('document_id');
					$subqb->from('change_path_rule');
					$subqb->where($fb->eq('relative_path', $fb->string($rule['target_url'])));
					$qb->where($fb->eq('document_id', $fb->subQuery($subqb->query())));

					$query = $qb->query();
					$result = $query->getFirstResult($query->getRowsConverter()->addIntCol('document_id'));
					if (!$result)
					{
						continue;
					}
					$rule['target'] = $result;
					$rule['section'] = null;
				}
				$existingRules = $pathRuleManager->findPathRules($rule['website'], $rule['LCID'], $rule['target'], $rule['section']);
				$pathRule = $pathRuleManager->getNewRule($rule['website'], $rule['LCID'], $rule['relative_path'], $rule['target'], (int)$rule['status'],
					$rule['section']);
				$pathRuleManager->insertPathRule($pathRule);
				foreach ($existingRules as $existingRule)
				{
					if ($existingRule->getDocumentId() != $rule['target'])
					{
						continue;
					}
					if ($existingRule->getHttpStatus() === 200 && (int)$rule['status'] === 200)
					{
						$pathRuleManager->updateRuleStatus($existingRule->getRuleId(), 301);
					}
				}
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			$tm->rollBack($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function createOrUpdateSubscribers(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$subscribers = $event->getParam('validatedLines');
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$subscribers)
		{
			$csvEngine->pushErrors(4, '[OPTIN-001] Missing modelName or subscriber.');
			return;
		}
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$mailingListManager = $genericServices->getMailinglistManager();
		$model = $modelManager->getModelByName($modelName);
		$tm = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();

		try
		{
			$tm->begin();
			foreach ($subscribers as $subscriber)
			{
				$LCID = $this->getLineLang($subscriber);
				$dqb = $documentManager->getNewQuery('Rbs_User_User');
				$dqb->andPredicates($dqb->eq('email', $dqb->getFragmentBuilder()->string(strtolower($subscriber['email']))));
				/** @var \Rbs\User\Documents\User $doc */
				$user = $dqb->getFirstDocument();
				$subscriber['secret'] = sha1(uniqid($subscriber['email'], true));
				$lists = explode('|', $subscriber['optins']);
				$mailingLists = [];
				foreach ($lists as $list)
				{
					$mailingLists[] = $csvEngine->resolveDocument($list, 'Rbs_Mailinglist_MailingList', true);
				}

				/** @var \Rbs\Mailinglist\Documents\Subscriber $doc */
				$doc = $mailingListManager->getSubscriberByEmail($subscriber['email']);
				if (!$doc)
				{
					$doc = $documentManager->getNewDocumentInstanceByModel($model);
				}
				$doc = $csvEngine->populateDocumentData($subscriber, $doc);
				$doc->setUserLCID($LCID);
				$doc->setMailingLists($mailingLists);
				$doc->setUser($user);
				$doc->save();
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			$tm->rollBack($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	protected function createOrUpdateAddresses(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		$addresses = $event->getParam('validatedLines');
		$applicationServices = $event->getApplicationServices();
		$modelManager = $applicationServices->getModelManager();
		$modelName = $event->getParam('modelName', null);
		$csvEngine = $event->getCSVEngine();
		if (!$modelName || !$addresses)
		{
			$csvEngine->pushErrors(4, 'Missing modelName or addresses.');
			return;
		}
		$model = $modelManager->getModelByName($modelName);
		$tm = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		$constraintManager = $event->getApplicationServices()->getConstraintsManager();
		try
		{
			$tm->begin();
			foreach ($addresses as $address)
			{
				$addressModel = $csvEngine->resolveDocument($address['addresstype'], 'Rbs_Geo_AddressFields');
				if ($addressModel instanceof \Rbs\Geo\Documents\AddressFields)
				{
					$dqb = $documentManager->getNewQuery('Rbs_User_User');
					$dqb->andPredicates($dqb->eq('email', $address['email']));
					/** @var \Rbs\User\Documents\User $doc */
					$user = $dqb->getFirstDocument();
					if ($user)
					{
						/** @var \Rbs\Geo\Documents\Address $doc */
						$doc = $documentManager->getNewDocumentInstanceByModel($model);
						if ($doc)
						{
							$addressFields = $addressModel->getFields();
							$addressData = explode('|', $address['addressdata']);
							if (count($addressData) !== count($addressFields))
							{
								$csvEngine->pushErrors(3,
									'Number of address fields (' . count($addressFields) .
									') and address values (' . count($addressData) . ') in column "addressdata" doesn\'t  match for ' .
									'address name : ' . $address['name'] . ' and user ' . $address['email'] . '.');
								continue;
							}
							$doc->setAddressFields($addressModel);
							$doc->setName($address['name']);
							foreach ($addressFields as $index => $field)
							{
								$required = $field->getRequired();
								if ($required && empty($addressData[$index]))
								{
									$csvEngine->pushErrors(4, 'Missing required address field value for ' . $field->getTitle());
									continue 2;
								}
								$match = $field->getMatch();
								$collectionCode = $field->getCollectionCode();
								if ($collectionCode)
								{
									$collection = $applicationServices->getCollectionManager()->getCollection($collectionCode);
									$value = $collection->getItemByValue($addressData[$index]);
									if (!$value && $required)
									{
										$csvEngine->pushErrors(4, 'Address field value not found for ' . $field->getCode() . ' in collection values '
											. $collectionCode . ' for ' . $address['name']);
										continue 2;
									}
									if ($value)
									{
										$doc->setFieldValue($field->getCode(), $addressData[$index]);
									}
								}
								else if (isset($addressData[$index]) && $addressData[$index])
								{
									if ($match)
									{
										$c = $constraintManager->matches('/' . str_replace('/', '\/', $match) . '/');
										if ($c->isValid($addressData[$index]))
										{
											$doc->setFieldValue($field, $addressData[$index]);
										}
										else
										{
											$csvEngine->pushErrors(4, 'Value for ' . $field->getTitle() . ' does not match validation pattern.');
											continue 2;
										}
									}
									else
									{
										$doc->setFieldValue($field->getCode(), $addressData[$index]);
									}
								}
							}
							$doc->setOwnerId($user->getId());
							$doc->save();
						}
					}
					else
					{
						$csvEngine->pushErrors(4, 'No users found for ' . $address['email']);
					}
				}
				else
				{
					$csvEngine->pushErrors(4, 'Address model not found with code ' . $address['addresstype'] . ' for user ' . $address['email']);
				}
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			$tm->rollBack($e);
			$event->setParam('hasError', true);
		}
	}

	/**
	 * @param \PDO $pdo
	 * @param string[] $lines
	 */
	protected function insertRawMetas($pdo, array $lines)
	{
		$values = [];
		foreach ($lines as $line)
		{
			$values[] = implode(',', $line);
		}
		$pdo->exec('INSERT IGNORE INTO `rbs_generic_imp_metas` (`code`, `lcid`, `group`, `meta_type`, `meta_name`, `meta_value`) VALUES ('
			. implode('), (', $values) . ')');
	}

	/**
	 * @param \PDO $pdo
	 * @param string[] $attributeLine
	 * @return string[]
	 */
	protected function formatAttributeLine($pdo, $attributeLine)
	{
		return [
			$pdo->quote($attributeLine['code']),
			$pdo->quote($attributeLine['lcid']),
			$attributeLine['group'],
			$pdo->quote($attributeLine['metatype']),
			$pdo->quote($attributeLine['metaname']),
			$pdo->quote($attributeLine['metavalue'])
		];
	}

	/**
	 * @param string[] $urlLine
	 * @return string[]
	 */
	protected function formatUrlLine($urlLine)
	{
		return [
			'website' => trim($urlLine['website']),
			'LCID' => trim($urlLine['lcid']),
			'relative_path' => trim($urlLine['toredirect']),
			'status' => trim($urlLine['status']),
			'section' => isset($urlLine['section']) && $urlLine['section'] !== null ? $urlLine['section'] : null,
			'target' => !empty($urlLine['target']) ? trim($urlLine['target']) : null,
			'target_url' => !empty($urlLine['targeturl']) ? trim($urlLine['targeturl']) : null,
			'type' => isset($urlLine['target']) ? 'Doc' : 'String',
			'hash' => sha1(\Change\Stdlib\StringUtils::toLower($urlLine['toredirect']))
		];
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param $section
	 * @return \Rbs\Catalog\Documents\SectionProductList|null
	 */
	protected function getSectionProductList($documentManager, $section)
	{
		$query = $documentManager->getNewQuery('Rbs_Catalog_SectionProductList');
		$query->andPredicates(
			$query->eq('synchronizedSection', $section)
		);
		$query->addOrder('id', false);
		return $query->getFirstDocument();
	}

	/**
	 * @param \Rbs\Generic\Documents\Typology $typology
	 * @return \Rbs\Generic\Documents\Attribute[]
	 */
	protected function extractNamedAttributes($typology)
	{
		$attributes = [];
		foreach ($typology->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				$name = $attribute->getName();
				if ($name)
				{
					$attributes[$name] = $attribute;
				}
			}
		}
		return $attributes;
	}

	/**
	 * @param string[] $rawAttribute
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @return mixed
	 */
	protected function formatRawAttributeLine($rawAttribute, $attribute)
	{
		if ($attribute->getValueType() === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY)
		{
			$itemTypologyData = [$rawAttribute['attributename'] => explode('|', $rawAttribute['attributevalue'])];
		}
		else if ($attribute->getValueType() === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN)
		{
			$itemTypologyData = [$rawAttribute['attributename'] => $rawAttribute['attributevalue'] === '1'];
		}
		else if ($attribute->getValueType() === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER)
		{
			$itemTypologyData = [$rawAttribute['attributename'] => (int)$rawAttribute['attributevalue']];
		}
		else if ($attribute->getValueType() === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT)
		{
			$itemTypologyData = [$rawAttribute['attributename'] => (float)$rawAttribute['attributevalue']];
		}
		elseif ($attribute->getValueType() === \Change\Documents\Attributes\Interfaces\Attribute::TYPE_JSON)
		{
			if (is_string($rawAttribute['attributevalue']))
			{
				$array = json_decode($rawAttribute['attributevalue'], true);
				$itemTypologyData = [$rawAttribute['attributename'] => $array];
			}
			else
			{
				$itemTypologyData = [];
			}
		}
		else
		{
			$itemTypologyData = [$rawAttribute['attributename'] => $rawAttribute['attributevalue']];
		}
		return $itemTypologyData;
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVEngine $csvEngine
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param mixed $value
	 * @return mixed
	 */
	protected function normalizeAttributeValue(\Change\Synchronization\CSV\CSVEngine $csvEngine, $attribute, $value)
	{
		if ($value === null && $attribute->getRequiredValue())
		{
			/*throw new ImportException('Required Typology attribute "' . $name . '"', 15);*/
			$csvEngine->pushErrors(4, 'Missing required typology attribute value for ' . $attribute->getName());
		}

		switch ($attribute->getValueType())
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
				if ($value === true || $value === false)
				{
					return $value;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_RICHTEXT:
				if ($value === null)
				{
					return $value;
				}
				elseif (is_string($value) || (is_array($value) && isset($value['e'], $value['t'])))
				{
					return new \Change\Documents\RichtextProperty($value);
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
				if ($value === null || is_int($value) || is_float($value))
				{
					return $value;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				if (is_string($value))
				{
					$value = new \DateTime($value);
				}
				if ($value === null || $value instanceof \DateTime)
				{
					return $value;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
				if ($value === null || is_int($value))
				{
					return $value;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
				if ($value === null || is_string($value))
				{
					return $value;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
				if ($value === null)
				{
					return null;
				}
				if ($value)
				{
					$model = $attribute->getDocumentType();
					$doc = $csvEngine->resolveDocument($value, $model);
					if ($doc instanceof \Change\Documents\AbstractDocument)
					{
						return $doc->getId();
					}
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				if ($value === null || (is_array($value) && count($value) === 0))
				{
					return null;
				}
				if (is_array($value))
				{
					$docIds = [];
					foreach ($value as $doc)
					{
						if (!$doc)
						{
							break;
						}
						$model = $attribute->getDocumentType();
						$doc = $csvEngine->resolveDocument($doc, $model);
						if ($doc instanceof \Change\Documents\AbstractDocument)
						{
							$docIds[] = $doc->getId();
						}
					}
					if (count($docIds) === count($value))
					{
						return $docIds;
					}
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_JSON:
				return $value && is_array($value) ? $value : null;

			default:
				return null;
			/*throw new ImportException('Invalid Typology attribute "' . $name . '" type', 17);*/
		}
		return null;
		/*throw new ImportException('Invalid Typology attribute "' . $name . '" value', 16);*/
	}

	/**
	 * @param \PDO $pdo
	 */
	protected function deleteMetaTable($pdo)
	{
		$pdo->exec('DELETE FROM `rbs_generic_imp_metas`');
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @param \Change\Synchronization\CSV\CSVEngine $csvEngine
	 * @param \Change\Db\DbProvider $dbProvider
	 */
	protected function addMetaCode(\Change\Transaction\TransactionManager $transactionManager, \Change\Synchronization\CSV\CSVEngine $csvEngine,
		\Change\Db\DbProvider $dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('code'));
		$qb->distinct();
		$qb->from('rbs_generic_imp_metas');
		$query = $qb->query();
		$rc = $query->getRowsConverter();
		$results = $query->getResults($rc->addStrCol('code'));

		foreach ($results as $result)
		{
			$metas = $this->getMetasForCode($dbProvider, $result);
			$this->setDocumentSeoMeta($transactionManager, $csvEngine, $result, $metas);
		}
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param string $code
	 * @return array
	 */
	protected function getMetasForCode(\Change\Db\DbProvider $dbProvider, $code)
	{
		$metaQuery = $dbProvider->getNewQueryBuilder();
		$fb = $metaQuery->getFragmentBuilder();
		$metaQuery->select($fb->column('code'), $fb->column('lcid'), $fb->column('group'), $fb->column('meta_type'), $fb->column('meta_name'),
			$fb->column('meta_value'));
		$metaQuery->from('rbs_generic_imp_metas');
		$metaQuery->where($fb->eq($fb->column('code'), $fb->string($code)));
		$query = $metaQuery->query();
		return $query->getResults();
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @param \Change\Synchronization\CSV\CSVEngine $csvEngine
	 * @param string $code
	 * @param array $metaLines
	 * @throws \Change\Transaction\RollbackException
	 */
	public function setDocumentSeoMeta(\Change\Transaction\TransactionManager $transactionManager, \Change\Synchronization\CSV\CSVEngine $csvEngine, $code,
		$metaLines)
	{
		try
		{
			$transactionManager->begin();
			$document = $csvEngine->resolveDocument($code);
			if ($document instanceof \Change\Documents\Interfaces\Publishable)
			{
				$publicationData = $document->getPublicationData();
				foreach ($metaLines as $metaLine)
				{
					$toUpdateGroup = null;
					$LCID = $this->getLineLang($metaLine);
					if (isset($publicationData['metas'][$LCID]))
					{
						$group = $csvEngine->resolveDocument($metaLine['group'], 'Rbs_Seo_MetaGroup');
						$groupIds = $publicationData['metas'][$LCID]['groupIds'];
						if ($group)
						{
							$groupIds[] = $group->getId();
						}
						$publicationData['metas'][$LCID]['groupIds'] = array_unique($groupIds);
						$existingMetas = [];
						if (isset($publicationData['metas'][$LCID]['metas']))
						{
							$existingMetas = $publicationData['metas'][$LCID]['metas'];
							$added = false;
							foreach ($existingMetas as &$existingMeta)
							{
								if ($existingMeta['type'] === $metaLine['meta_type'] && $existingMeta['name'] === $metaLine['meta_name'])
								{
									$existingMeta['value'] = $metaLine['meta_value'];
									$added = true;
								}
							}
							if (!$added)
							{
								$existingMetas[] =
									['type' => $metaLine['meta_type'], 'name' => $metaLine['meta_name'], 'value' => $metaLine['meta_value']];
							}
						}
						else
						{
							$group = $csvEngine->resolveDocument($metaLine['group'], 'Rbs_Seo_MetaGroup');
							$publicationData['metas'][$LCID]['metas'][] =
								['type' => $metaLine['type'], 'name' => $metaLine['name'], 'value' => $metaLine['value']];
						}
						$publicationData['metas'][$LCID]['metas'] = $existingMetas;
						$publicationData['metas'][$LCID]['mode'] = 'manual';
					}
				}
				$document->setPublicationData($publicationData);
				$document->save();
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$transactionManager->rollBack($e);
		}
	}

	/**
	 * @param \Change\Synchronization\CSV\CSVImportEvent $event
	 */
	public function onAddDocumentTypology(\Change\Synchronization\CSV\CSVImportEvent $event)
	{
		/** @var string|null $typologyCode */
		$typologyCode = $event->getParam('typologyCode');
		$typologyDocument = null;
		if ($typologyCode)
		{
			$CSVEngine = $event->getCSVEngine();
			$typologyDocument = $CSVEngine->resolveDocument($typologyCode, 'Rbs_Generic_Typology');
			if (!$typologyDocument)
			{
				$documentManager = $event->getApplicationServices()->getDocumentManager();
				$query = $documentManager->getNewQuery('Rbs_Generic_Typology');
				$query->andPredicates($query->eq('name', $typologyCode));
				$typologyDocument = $query->getFirstDocument();
			}
			if ($typologyDocument && $typologyDocument instanceof \Rbs\Generic\Documents\Typology)
			{
				$typology = new \Rbs\Generic\Attributes\Typology($typologyDocument);
				$event->setParam('typology', $typology);
			}
		}
	}

	/**
	 * @param array $line
	 * @return string|null
	 */
	protected function getLineLang($line)
	{
		if (isset($line['reflcid']))
		{
			return $line['reflcid'];
		}
		else if (isset($line['lcid']))
		{
			return $line['lcid'];
		}
		return null;
	}
}