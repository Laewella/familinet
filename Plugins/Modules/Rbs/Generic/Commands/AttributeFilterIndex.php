<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Commands;

/**
 * @name \Rbs\Generic\Commands\AttributeFilterIndex
 */
class AttributeFilterIndex
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$transactionManager = $event->getApplicationServices()->getTransactionManager();

		$filterIndex = new \Rbs\Generic\Attributes\FilterIndex($dbProvider, $event->getApplication());

		$typologyIds = $this->getTypologyIds($dbProvider);
		$response->addInfoMessage(count($typologyIds) . ' typologies.');
		foreach ($typologyIds as $typologyId)
		{
			$response->addInfoMessage('Typology: ' . $typologyId);
			$limit = 100;
			$lastId = 0;
			do
			{
				$startId = $lastId;
				$ids = $this->getDocumentIds($dbProvider, $typologyId, $startId, $limit);
				try
				{
					$transactionManager->begin();

					foreach ($ids as $id)
					{
						try
						{
							$lastId = $id;
							$filterIndex->save($id);
						}
						catch (\Exception $e)
						{
							$response->addErrorMessage('  error on ' . $lastId . ': ' . $e->getMessage());
						}
					}

					$transactionManager->commit();
				}
				catch (\Exception $e)
				{
					throw $transactionManager->rollBack($e);
				}
				$response->addInfoMessage('  ids ' . $startId . ' to ' . $lastId . '...');
			}
			while(count($ids) == $limit);
		}

		$response->addInfoMessage('Done.');
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return integer[]
	 */
	protected function getTypologyIds($dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->column('typology_id'), 'typologyId'))
				->from($fb->getDocumentAttributesTable())
				->distinct();
		}
		$sq = $qb->query();
		return $sq->getResults($sq->getRowsConverter()->addIntCol('typologyId')->singleColumn('typologyId'));
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param integer $typologyId
	 * @param integer $lastId
	 * @param integer $limit
	 * @return array
	 */
	protected function getDocumentIds($dbProvider, $typologyId, $lastId, $limit)
	{
		$qb = $dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->column('document_id'))
				->from($fb->getDocumentAttributesTable())
				->where($fb->logicAnd(
					$fb->eq($fb->column('typology_id'), $fb->integerParameter('typologyId')),
					$fb->gt($fb->column('document_id'), $fb->integerParameter('lastId'))
				))
				->orderAsc($fb->column('document_id'));
		}
		$sq = $qb->query();
		$sq->bindParameter('typologyId', $typologyId);
		$sq->bindParameter('lastId', $lastId);
		$sq->setStartIndex(0);
		$sq->setMaxResults($limit);
		return $sq->getResults($sq->getRowsConverter()->addIntCol('document_id')->singleColumn('document_id'));
	}
}