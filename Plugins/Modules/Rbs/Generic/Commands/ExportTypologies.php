<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Rbs\Generic\Commands\ExportTypologies
 */
class ExportTypologies
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();

		// Filters.
		$all = $event->getParam('all');
		$typologyNames = $event->getParam('typologies');
		$modelNames = $event->getParam('models');
		if (!$all && !$typologyNames && !$modelNames)
		{
			$response->addCommentMessage('No typology specified.');
			return;
		}

		// File name.
		$workspace = $event->getApplication()->getWorkspace();
		$filePath = $workspace->composeAbsolutePath($event->getParam('fileName'));
		$directory = dirname($filePath);
		$file = basename($filePath);
		if (substr($file, -5) != '.json') {
			$filePath .= '.json';
		}
		if (!is_dir($directory)) {
			\Change\Stdlib\FileUtils::mkdir($directory);
		}

		// Context id.
		$contextId = $event->getParam('context', 'Rbs_Generic_Typologies');

		// Query documents.
		$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Generic_Typology');
		if ($typologyNames && $modelNames)
		{
			$query->orPredicates(
				$query->in('modelName', explode(',', $modelNames)),
				$query->in('name', explode(',', $typologyNames))
			);
		}
		elseif ($typologyNames)
		{
			$query->orPredicates(
				$query->in('name', explode(',', $typologyNames))
			);
		}
		elseif ($modelNames)
		{
			$query->orPredicates(
				$query->in('modelName', explode(',', $modelNames))
			);
		}
		/** @var \Rbs\Generic\Documents\Typology[] $typologies */
		$typologies = $query->getDocuments();

		/** @var \Rbs\Generic\Documents\Attribute[] $attributes */
		$attributes = [];
		$collectionCodes = [];
		foreach ($typologies as $typology)
		{
			foreach ($typology->getGroups() as $group)
			{
				foreach ($group->getAttributes() as $attribute)
				{
					$attributes[$attribute->getId()] = $attribute;
					if ($attribute->getCollectionCode())
					{
						$collectionCodes[] = $attribute->getCollectionCode();
					}
				}
			}
		}
		array_unique($collectionCodes);

		$collections = [];
		if ($collectionCodes)
		{
			$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Collection_Collection');
			$query->andPredicates($query->in('code', $collectionCodes));
			$collections = $query->getDocuments();
		}

		// Do export.
		$export = new \Rbs\Generic\Json\Export($applicationServices->getDocumentManager());
		$export->setDocumentCodeManager($applicationServices->getDocumentCodeManager());
		$export->setContextId($contextId);

		$export->setDocuments(array_values($attributes));
		$export->addDocuments($typologies);
		$export->addDocuments($collections);

		$buildDocumentCode = function(\Change\Documents\AbstractDocument $document, $contextId) {
			if ($document instanceof \Rbs\Collection\Documents\Collection)
			{
				return $document->getCode();
			}
			elseif ($document instanceof \Rbs\Generic\Documents\Typology)
			{
				$name = $document->getName() ?: $document->getId();
				return 'Rbs_Generic_Typology_' . $document->getModelName() . '_' . $name;
			}
			elseif ($document instanceof \Rbs\Generic\Documents\Attribute)
			{
				return 'Rbs_Generic_Attributes_' . ($document->getName() ?: $document->getId());
			}
			return $document->getId();
		};
		$export->getOptions()->set('buildDocumentCode', $buildDocumentCode);

		file_put_contents($filePath, json_encode($export->toArray(), JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
		$response->addInfoMessage('Typologies exported in file: ' . $filePath);
	}
}