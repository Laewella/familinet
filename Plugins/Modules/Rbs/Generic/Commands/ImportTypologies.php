<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Rbs\Generic\Commands\ImportTypologies
 */
class ImportTypologies
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		$response = $event->getCommandResponse();

		$workspace = $event->getApplication()->getWorkspace();
		$filePath = $workspace->composeAbsolutePath($event->getParam('fileName'));
		if (!is_readable($filePath))
		{
			$response->addErrorMessage('Unable to read: ' . $filePath);
			return;
		}

		$json = json_decode(file_get_contents($filePath), true);
		if (!is_array($json) || !isset($json['documents']) || !is_array($json['documents']))
		{
			$response->addErrorMessage('Invalid json file: ' . $filePath);
			return;
		}

		$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
		$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());
		$import->getOptions()->set('resolveInlineDocument', [$import, 'defaultResolveCollectionItem']);

		$dcm = $event->getApplicationServices()->getDocumentCodeManager();
		$callback = function ($document, $jsonDocument) use ($import, $dcm)
		{
			(new \Rbs\Generic\Attributes\ListenerCallbacks)->preSaveImport($document, $jsonDocument, $import, $dcm);
		};
		$import->getOptions()->set('preSave', $callback);

		$dm = $event->getApplicationServices()->getDocumentManager();
		$callback = function ($id, $contextId, $jsonDocument) use ($dm)
		{
			if (isset($jsonDocument['_model']) && $jsonDocument['_model'] == 'Rbs_Collection_Collection')
			{
				return (new \Rbs\Collection\Events\ImportCallbacks)->resolveCollection($jsonDocument, $dm);
			}
		};
		$import->getOptions()->set('resolveDocument', $callback);

		try
		{
			$applicationServices->getTransactionManager()->begin();
			$imported = $import->fromArray($json);
			$applicationServices->getTransactionManager()->commit();
			$response->addInfoMessage('Successfully imported ' . count($imported) . ' documents from file: ' . $filePath);
		}
		catch (\Exception $e)
		{
			$applicationServices->getTransactionManager()->rollBack($e);
			$response->addErrorMessage($e->getMessage());
		}
	}
}