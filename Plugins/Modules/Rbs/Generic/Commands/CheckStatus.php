<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Commands;

/**
 * @name \Rbs\Generic\Commands\CheckStatus
 */
class CheckStatus
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$serviceChecker = new \Rbs\Generic\ServicesStatus($event->getApplication(), $event->getApplicationServices());
		$serviceChecker->checkDb();
		$serviceChecker->checkRedis();
		$serviceChecker->checkElasticsearch();
		$errors = $serviceChecker->getErrors();
		$response = $event->getCommandResponse();
		if (count($errors))
		{
			foreach ($errors as $error)
			{
				$response->addErrorMessage($error);
			}
		}
		else
		{
			$response->addInfoMessage('All checked services are OK');
		}
	}
}