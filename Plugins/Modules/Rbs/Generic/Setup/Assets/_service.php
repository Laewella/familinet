<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
require_once(__DIR__ . '/Change/Application.php');

$application = new \Change\Application();
$application->useSession(false);
$application->start();

$eventManager = $application->getNewEventManager(['_service']);

$eventManager->attach('_service', function (\Change\Events\Event $event)
{
	$serviceChecker = new \Rbs\Generic\ServicesStatus($event->getApplication(), $event->getApplicationServices());
	$serviceChecker->checkStorage();
	$serviceChecker->checkDb();
	$serviceChecker->checkRedis();
	$serviceChecker->checkElasticsearch();
	$errors = $serviceChecker->getErrors();
	if (count($errors))
	{
		$logging = $event->getApplication()->getLogging();
		foreach ($errors as $error)
		{
			$logging->fatal($error);
		}

		header("HTTP/1.1 503 Service unavailable");
	}
});

$eventManager->trigger('_service');