<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Setup;

/**
 * @name \Rbs\Generic\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/ListenerAggregateClasses/Rbs_Generic', \Rbs\Generic\Events\SharedListeners::class);
		$configuration->addPersistentEntry('Change/Events/Documents/Rbs_Generic', \Rbs\Generic\Events\Documents\SharedListeners::class);
		$configuration->addPersistentEntry('Change/Events/CollectionManager/Rbs_Generic', \Rbs\Generic\Collection\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/DocumentManager/Rbs_Generic', \Rbs\Generic\Events\DocumentManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/Rest/Rbs_Generic', \Rbs\Generic\Events\Http\Rest\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/Web/Rbs_Generic', \Rbs\Generic\Events\Http\Web\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/Ajax/Rbs_Generic', \Rbs\Generic\Events\Http\Ajax\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Commands/Rbs_Generic', \Rbs\Generic\Events\Commands\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Db/Rbs_Generic', \Rbs\Generic\Events\Db\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/WorkflowManager/Rbs_Generic', \Rbs\Generic\Events\WorkflowManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/BlockManager/Rbs_Generic', \Rbs\Generic\Events\BlockManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/PageManager/Rbs_Generic', \Rbs\Generic\Events\PageManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ProfileManager/Rbs_Generic', \Rbs\Generic\Events\ProfileManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/AuthenticationManager/Rbs_Generic',
			\Rbs\Generic\Events\AuthenticationManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ThemeManager/Rbs_Generic', \Rbs\Generic\Events\ThemeManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/RichTextManager/Rbs_Generic', \Rbs\Generic\Events\RichTextManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/JobManager/Rbs_Generic', \Rbs\Generic\Events\JobManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/OAuthManager/Rbs_Generic', \Rbs\Generic\Events\OAuthManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ModelManager/Rbs_Generic', \Rbs\Generic\Events\ModelManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/PathRuleManager/Rbs_Generic', \Rbs\Generic\Events\PathRuleManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ImportEngine/Rbs_Generic', \Rbs\Generic\Events\Synchronization\ImportListeners::class);
		$configuration->addPersistentEntry('Change/Events/ExportEngine/Rbs_Generic', \Rbs\Generic\Events\Synchronization\ExportListeners::class);
		$configuration->addPersistentEntry('Change/Events/CSVEngine/Rbs_Generic', \Rbs\Generic\Events\Synchronization\CSVImportListeners::class);
		$configuration->addPersistentEntry('Change/Events/MailManager/Rbs_Generic', \Rbs\Generic\Events\Mail\Listeners::class);
		$configuration->addPersistentEntry('Rbs/Notification/Events/NotificationManager/Rbs_Generic',
			\Rbs\Generic\Events\NotificationManager\Listeners::class);
		$configuration->addPersistentEntry('Rbs/Admin/Events/AdminManager/Rbs_Generic', \Rbs\Generic\Events\AdminManager\Listeners::class);

		$configuration->addPersistentEntry('Rbs/Mail/defaultSender', 'no-reply@proximis.com');
		$configuration->addPersistentEntry('Rbs/Mail/defaultBaseUrl', null);

		$configuration->addPersistentEntry('Rbs/User/Events/UserManager/Rbs_Generic', \Rbs\Generic\Events\UserManager\Listeners::class);

		$configuration->addPersistentEntry('Rbs/Elasticsearch/Events/Manager/Rbs_Generic', \Rbs\Generic\Events\ElasticsearchManager\Listeners::class);

		$configuration->addPersistentEntry('Change/Application/version', $this->processVersion($application));

		$synchro = $configuration->getEntry('Change/Storage/synchro', []);
		$synchro = array_merge([
			'class' => \Change\Storage\Engines\LocalStorage::class,
			'basePath' => 'App/Storage/synchro',
		], $synchro);
		$configuration->addPersistentEntry('Change/Storage/synchro', $synchro);

		$webBaseDirectory = $configuration->getEntry('Change/Install/webBaseDirectory', '');
		$workspace = $application->getWorkspace();
		if ($webBaseDirectory)
		{
			$formattedPath = $workspace->composePath($webBaseDirectory, 'Imagestorage', 'imagesSynchro');
			$requirePath = implode(DIRECTORY_SEPARATOR, array_fill(0, count(explode(DIRECTORY_SEPARATOR, $webBaseDirectory)), '..'));
		}
		else
		{
			$formattedPath = $workspace->composePath('Imagestorage', 'imagesSynchro');
			$requirePath = $workspace->projectPath();
		}

		$synchro = $configuration->getEntry('Change/Storage/imagesSynchro', []);
		$synchro = array_merge([
				'class' => '\\Change\\Storage\\Engines\\LocalImageStorage',
				'basePath' => 'App/Storage/imagesSynchro',
				'formattedPath' => $formattedPath,
				'baseURL' => '/index.php'

		], $synchro);
		$configuration->addPersistentEntry('Change/Storage/imagesSynchro', $synchro);

		$webBasePath = $workspace->composeAbsolutePath($webBaseDirectory);
		if (is_dir($webBasePath))
		{
			$srcPath = __DIR__ . '/Assets/_service.php';
			$content = \Change\Stdlib\FileUtils::read($srcPath);
			$content = str_replace('__DIR__', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($webBasePath . DIRECTORY_SEPARATOR . basename($srcPath), $content);
		}
		else
		{
			throw new \RuntimeException('Invalid document root path: ' . $webBasePath .
				'. Check "Change/Install/webBaseDirectory" configuration entry.', 999999);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Application $application
	 * @return string
	 */
	protected function processVersion($application)
	{
		// Integrators can specify an approximate version of the core in composer.json file (i.e "~1.7.0")
		// Thus, composer.lock file is parsed first, for it contains the exact version of the core package
		$composerJsonPath = $application->getWorkspace()->projectPath('composer.lock');
		if (file_exists($composerJsonPath) && is_readable($composerJsonPath))
		{
			$json = json_decode(file_get_contents($composerJsonPath), true);
			if ($json)
			{
				$packages = $json['packages'];

				foreach ($packages as $package)
				{
					if ($package['name'] === 'rbschange/core')
					{
						return $package['version'];
					}
				}
			}
		}

		// If composer.lock is not accessible for some reason, parses composer.json
		$composerJsonPath = $application->getWorkspace()->projectPath('composer.json');
		if (file_exists($composerJsonPath) && is_readable($composerJsonPath))
		{
			$json = json_decode(file_get_contents($composerJsonPath), true);
			if ($json)
			{
				if (isset($json['require']['rbschange/core']))
				{
					return $json['require']['rbschange/core'];
				}
				elseif (isset($json['require']['proximis/omnichannel']))
				{
					return $json['require']['proximis/omnichannel'];
				}
				elseif (isset($json['require']['proximis/omnichannel-core']))
				{
					return $json['require']['proximis/omnichannel-core'];
				}
			}
		}

		// Default: dev version
		return '[' . \Change\Application::CHANGE_VERSION . '.dev]';
	}
}