<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Http\Ajax\V1;

/**
 * @name \Rbs\Generic\Http\Ajax\V1\DefaultDataComposer
 */
class PublishedDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Change\Documents\AbstractDocument
	 */
	protected $document;

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function __construct(\Change\Documents\Events\Event $event)
	{
		$this->document = $event->getDocument();

		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());
	}

	protected function generateDataSets()
	{
		if (!$this->document)
		{
			$this->dataSets = [];
			return;
		}

		$this->dataSets = [
			'common' => $this->getPublishedData($this->document)
		];

		$this->generateTypologyDataSet($this->document);
	}
}