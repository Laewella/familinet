<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Http\Ajax\V1;

/**
 * @name \Rbs\Generic\Http\Ajax\V1\Document
 */
class Document
{
	/**
	 * Default actionPath: Document/{documentId}
	 * Method: GET
	 * Event params:
	 *  - website, websiteUrlManager, section, page
	 *  - dataSetNames
	 *  - visualFormats
	 *  - URLFormats
	 * @param \Change\Http\Event $event
	 */
	public function getData(\Change\Http\Event $event)
	{
		$document = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($event->getParam('documentId'));
		if (!$document)
		{
			return;
		}

		/** @var \Change\Documents\AbstractDocument $document */
		if ($event->getParam('detailed') === null)
		{
			$event->setParam('detailed', true);
		}
		$event->setParam('level', 0);
		$event->setParam('referrer', null);

		$data = $document->getAJAXData($event->paramsToArray());
		if (!$data)
		{
			return;
		}

		$model = $document->getDocumentModel();
		$result = new \Change\Http\Ajax\V1\ItemResult(str_replace('_', '/', $model->getName()), $data);
		$event->setResult($result);
	}
}