<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Collection;

/**
 * @name \Rbs\Generic\Collection\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			if (!$event->getParam('collection'))
			{
				(new \Rbs\Collection\Events\CollectionResolver())->getCollection($event);
			}
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_COLLECTION, $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			if (!$event->getParam('collection'))
			{
				(new \Rbs\Geo\Collection\Collections())->getTerritorialUnitsByType($event);
			}
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_COLLECTION, $callback, 7);

		$callback = function (\Change\Events\Event $event)
		{
			if (!$event->getParam('collection'))
			{
				switch ($event->getParam('code'))
				{
					case 'Rbs_Collection_Collection_List':
						(new \Rbs\Collection\Events\CollectionResolver())->getCollectionList($event);
						(new \Rbs\Geo\Collection\Collections())->completeCollectionList($event);
						break;
					case 'Rbs_Collection_ObjectCollectionDefinitions':
						(new \Rbs\Collection\Collection\Collections())->addObjectCollectionDefinitions($event);
						break;

					case 'Rbs_Elasticsearch_CollectionIds':
						(new \Rbs\Elasticsearch\Collection\Collections())->addCollectionIds($event);
						break;
					case 'Rbs_Elasticsearch_Collection_AttributeIds':
						(new \Rbs\Elasticsearch\Collection\Collections())->addAttributeIds($event);
						break;
					case 'Rbs_Elasticsearch_Collection_AxisIds':
						(new \Rbs\Elasticsearch\Collection\Collections())->addAxisIds($event);
						break;
					case 'Rbs_Elasticsearch_FacetConfigurationType':
						(new \Rbs\Elasticsearch\Collection\Collections())->addFacetConfigurationType($event);
						break;
					case 'Rbs_Elasticsearch_FacetRenderingModes':
						(new \Rbs\Elasticsearch\Collection\Collections())->addFacetRenderingModes($event);
						break;
					case 'Rbs_Elasticsearch_IndicesName':
						(new \Rbs\Elasticsearch\Collection\Collections())->addIndicesName($event);
						break;
					case 'Rbs_Elasticsearch_IndexMapping':
						(new \Rbs\Elasticsearch\Collection\Collections())->addIndexMapping($event);
						break;
					case 'Rbs_Elasticsearch_Result_Functions':
						(new \Rbs\Elasticsearch\Collection\Collections())->addResultFunction($event);
						break;

					case 'Rbs_Generic_AttributeCollections':
						(new \Rbs\Generic\Collection\Collections())->addAttributeCollections($event);
						break;
					case 'Rbs_Generic_AttributeRenderingModes':
						(new \Rbs\Generic\Collection\Collections())->addAttributeRenderingModes($event);
						break;
					case 'Rbs_Generic_AttributesForModel':
						(new \Rbs\Generic\Collection\Collections())->addAttributesForModel($event);
						break;
					case 'Rbs_Generic_AttributeValueTypes':
						(new \Rbs\Generic\Collection\Collections())->addAttributeValueTypes($event);
						break;
					case 'Rbs_Generic_Collection_AddressFields':
						(new \Rbs\Generic\Collection\Collections())->addAddressFields($event);
						break;
					case 'Rbs_Generic_Collection_Languages':
						(new \Rbs\Generic\Collection\Collections())->addLanguages($event);
						break;
					case 'Rbs_Generic_Collection_PermissionPrivileges':
						(new \Rbs\Generic\Collection\Collections())->addPermissionPrivileges($event);
						break;
					case 'Rbs_Generic_Collection_PermissionRoles':
						(new \Rbs\Generic\Collection\Collections())->addPermissionRoles($event);
						break;
					case 'Rbs_Generic_Collection_SortDirections':
						(new \Rbs\Generic\Collection\Collections())->addSortDirections($event);
						break;
					case 'Rbs_Generic_Collection_TimeZones':
						(new \Rbs\Generic\Collection\Collections())->addTimeZones($event);
						break;
					case 'Rbs_Generic_Typologies':
						(new \Rbs\Generic\Collection\Collections())->addTypologies($event);
						break;

					case 'Rbs_Geo_All_Countries_Codes':
						(new \Rbs\Geo\Collection\Collections())->addAllCountriesCodes($event);
						break;
					case 'Rbs_Geo_All_Countries_IsoCodes':
						(new \Rbs\Geo\Collection\Collections())->addAllCountriesIsoCodes($event);
						break;
					case 'Rbs_Geo_Collection_Countries':
						(new \Rbs\Geo\Collection\Collections())->addCountries($event);
						break;
					case 'Rbs_Geo_AddressField_Names':
						(new \Rbs\Geo\Collection\Collections())->addAddressFieldNames($event);
						break;

					case 'Rbs_Media_BlockAlignments':
						(new \Rbs\Media\Collection\Collections())->addBlockAlignments($event);
						break;

					case 'Rbs_Seo_MetaValueTypes':
						(new \Rbs\Seo\Collection\Collections())->addMetaValueTypes($event);
						break;

					case 'Rbs_Simpleform_ConfirmationModes':
						(new \Rbs\Simpleform\Collection\Collections())->addConfirmationModes($event);
						break;
					case 'Rbs_Simpleform_FieldTypes':
						(new \Rbs\Simpleform\Collection\Collections())->addFieldTypes($event);
						break;
					case 'Rbs_Simpleform_AutoCapitalizeOptions':
						(new \Rbs\Simpleform\Collection\Collections())->addAutoCapitalizeOptions($event);
						break;

					case 'Rbs_Social_SocialCollection':
						(new \Rbs\Social\Collection\Collections())->addSocialCollection($event);
						break;

					case 'Rbs_Tag_Collection_TagModules':
						(new \Rbs\Tag\Collection\Collections())->addTagModules($event);
						break;

					case 'Rbs_Theme_WebsiteIds':
						(new \Rbs\Theme\Collection\Collections())->addWebsiteIds($event);
						break;

					case 'Rbs_User_CustomerFields':
						(new \Rbs\User\Collection\Collections())->addUserCustomerFields($event);
						break;
					case 'Rbs_User_OnLoginActions':
						(new \Rbs\User\Collection\Collections())->addOnLoginActions($event);
						break;

					case 'Rbs_Ua_UISupportedLCIDs':
						(new \Rbs\Ua\Collection\Collections())->addUISupportedLCIDs($event);
						break;

					case 'Rbs_Website_AvailablePageFunctions':
						(new \Rbs\Admin\Collection\Collections())->addAvailablePageFunctions($event);
						break;
					case 'Rbs_Website_InterstitialAudiences':
						(new \Rbs\Website\Collection\Collections())->addInterstitialAudiences($event);
						break;
					case 'Rbs_Website_InterstitialDisplayFrequencies':
						(new \Rbs\Website\Collection\Collections())->addInterstitialDisplayFrequencies($event);
						break;
					case 'Rbs_Website_InterstitialPopinSizes':
						(new \Rbs\Website\Collection\Collections())->addInterstitialPopinSizes($event);
						break;
					case 'Rbs_Website_UntranslatedPageActions':
						(new \Rbs\Website\Collection\Collections())->addUntranslatedPageActions($event);
						break;

					//@deprecated since 1.8.0 with no replacement
					case 'Rbs_Elasticsearch_Collection_Clients':
					case 'Rbs_Elasticsearch_IndexCategories':
					case 'Rbs_Elasticsearch_Collection_Indexes':
					case 'Rbs_Elasticsearch_ProductList_ResolutionModes':
						trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
						break;
				}
			}
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_COLLECTION, $callback, 10);

		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Collection\Events\CollectionResolver())->getCodes($event);
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_CODES, $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$codes = $event->getParam('codes', []);
			$codes = array_merge($codes, [
				'Rbs_Collection_Collection_List',
				'Rbs_Collection_ObjectCollectionDefinitions',

				'Rbs_Elasticsearch_CollectionIds',
				'Rbs_Elasticsearch_Collection_AttributeIds',
				'Rbs_Elasticsearch_Collection_AxisIds',
				'Rbs_Elasticsearch_FacetConfigurationType',
				'Rbs_Elasticsearch_FacetRenderingModes',
				'Rbs_Elasticsearch_IndicesName',
				'Rbs_Elasticsearch_Result_Functions',

				'Rbs_Generic_AttributeCollections',
				'Rbs_Generic_AttributeRenderingModes',
				'Rbs_Generic_AttributesForModel',
				'Rbs_Generic_AttributeValueTypes',
				'Rbs_Generic_Collection_AddressFields',
				'Rbs_Generic_Collection_Languages',
				'Rbs_Generic_Collection_PermissionRoles',
				'Rbs_Generic_Collection_PermissionPrivileges',
				'Rbs_Generic_Collection_SortDirections',
				'Rbs_Generic_Collection_TimeZones',
				'Rbs_Generic_Typologies',

				'Rbs_Geo_All_Countries_Codes',
				'Rbs_Geo_Collection_Countries',
				'Rbs_Geo_AddressField_Names',

				'Rbs_Seo_MetaValueTypes',

				'Rbs_Simpleform_ConfirmationModes',
				'Rbs_Simpleform_FieldTypes',
				'Rbs_Simpleform_AutoCapitalizeOptions',

				'Rbs_Social_SocialCollection',

				'Rbs_Tag_Collection_TagModules',

				'Rbs_Theme_WebsiteIds',

				'Rbs_Ua_UISupportedLCIDs',

				'Rbs_User_CustomerFields',
				'Rbs_User_OnLoginActions',

				'Rbs_Website_AvailablePageFunctions',
				'Rbs_Website_InterstitialAudiences',
				'Rbs_Website_InterstitialDisplayFrequencies',
				'Rbs_Website_InterstitialPopinSizes'
			]);
			$event->setParam('codes', $codes);
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_CODES, $callback, 1);
	}
}