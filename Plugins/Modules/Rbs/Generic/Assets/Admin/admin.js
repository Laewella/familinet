(function() {
	"use strict";

	var app = angular.module('RbsChange');

	function RbsGenericInitializeWebsiteCtrl(scope, $http, REST, i18n, NotificationCenter, ErrorFormatter) {
		scope.data = {
			websiteId: null,
			sidebarTemplateId: null,
			noSidebarTemplateId: null,
			LCID: null,
			userAccountTopicId: null
		};
		scope.onInitialization = false;

		scope.initializeWebsiteStructure = function() {
			scope.onInitialization = true;
			var params = {
				websiteId: scope.data.websiteId,
				sidebarTemplateId: scope.data.sidebarTemplateId,
				noSidebarTemplateId: scope.data.noSidebarTemplateId,
				LCID: scope.data.LCID,
				userAccountTopicId: scope.data.userAccountTopicId
			};
			$http.post(REST.getBaseUrl('commands/rbs_generic/initialize-website'), params).then(
				function() {
					scope.onInitialization = false;
					scope.alreadyInitialized = true;
					NotificationCenter.info(i18n.trans('m.rbs.generic.admin.initialize_website_success | ucf'));
				},
				function(result) {
					console.error(result);
					scope.onInitialization = false;
					NotificationCenter.error(i18n.trans('m.rbs.generic.admin.initialize_website_error | ucf'), ErrorFormatter.format(result.data));
				}
			);
		};

		function checkAlreadyInitialized(websiteId) {
			var params = {
				context: 'Rbs Generic Website Initialize ' + websiteId
			};
			$http.post(REST.getBaseUrl('Rbs/Generic/DocumentCodeContextExist'), params).then(
				function(result) {
					scope.alreadyInitialized = result.data.result;
					if (result.data.result) {
						NotificationCenter.warning(i18n.trans('m.rbs.generic.admin.already_initialized_website | ucf'));
					}
				},
				function(result) {
					console.error(result);
					NotificationCenter.error(i18n.trans('m.rbs.generic.admin.check_already_initialized_error | ucf'),
						ErrorFormatter.format(result.data));
				}
			);
		}

		function preselectTopics(websiteId) {
			var params = {
				codes: { userAccountTopicId: 'user_account_topic' },
				context: 'Website_' + websiteId
			};
			$http.post(REST.getBaseUrl('Rbs/Generic/GetDocumentsByCodes'), params).then(
				function(result) {
					if (result.data.userAccountTopicId) {
						scope.data.userAccountTopicId = result.data.userAccountTopicId;
					}
				},
				function(result) {
					console.error(result);
					NotificationCenter.error(i18n.trans('m.rbs.generic.admin.get_document_by_code_error | ucf'), ErrorFormatter.format(result.data));
				}
			);
		}

		scope.$watch('data.websiteId', function(websiteId) {
			NotificationCenter.clear();
			if (websiteId) {
				preselectTopics(websiteId);
				checkAlreadyInitialized(websiteId);
			}
			else {
				scope.alreadyInitialized = false;
			}
		});
	}

	RbsGenericInitializeWebsiteCtrl.$inject = ['$scope', '$http', 'RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter',
		'RbsChange.ErrorFormatter'];
	app.controller('RbsGenericInitializeWebsiteCtrl', RbsGenericInitializeWebsiteCtrl);

	// Jobs.

	function JobListController(status, sortDesc, scope, REST, i18n, NotificationCenter, ErrorFormatter) {
		scope.pagination = {
			currentPage: 1,
			totalItems: 0,
			itemsPerPage: null,
			currentItems: [],
			sort: 'start_date',
			desc: sortDesc,
			loading: false
		};

		function reloadJobs() {
			if (scope.pagination.busy) {
				return;
			}
			scope.pagination.loading = true;
			var params = {
				offset: (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage,
				limit: scope.pagination.itemsPerPage,
				sort: scope.pagination.sort,
				desc: scope.pagination.desc
			};
			REST.call(REST.getBaseUrl('jobs/' + status + '/'), params).then(
				function(data) {
					scope.pagination.totalItems = data.pagination.count;
					scope.pagination.currentItems = data.resources;
					var now = (new Date()).getTime();
					angular.forEach(scope.pagination.currentItems, function(value) {
						var date = (new Date(value.properties.startDate)).getTime();
						value.properties.__hours = Math.floor((now - date) / 1000 / 3600);
					});
					scope.pagination.loading = false;
				},
				function(error) {
					console.error(error);
					NotificationCenter.error(i18n.trans('m.rbs.generic.admin.load_jobs_error | ucf'),
						ErrorFormatter.format(error));
					scope.pagination.currentItems = [];
					scope.pagination.loading = false;
				}
			)
		}

		scope.reloadJobs = reloadJobs;

		scope.$watch('pagination.currentPage', reloadJobs);
		scope.$watch('pagination.itemsPerPage', reloadJobs);
		scope.$watch('pagination.sort', reloadJobs);
		scope.$watch('pagination.desc', reloadJobs);
	}

	function RbsGenericRunningJobs(scope, REST, i18n, NotificationCenter, ErrorFormatter) {
		JobListController('running', true, scope, REST, i18n, NotificationCenter, ErrorFormatter);
	}

	RbsGenericRunningJobs.$inject = ['$scope', 'RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter',
		'RbsChange.ErrorFormatter'];
	app.controller('RbsGenericRunningJobs', RbsGenericRunningJobs);

	function RbsGenericFailedJobs(scope, REST, i18n, NotificationCenter, ErrorFormatter) {
		JobListController('failed', true, scope, REST, i18n, NotificationCenter, ErrorFormatter);
	}

	RbsGenericFailedJobs.$inject = ['$scope', 'RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter',
		'RbsChange.ErrorFormatter'];
	app.controller('RbsGenericFailedJobs', RbsGenericFailedJobs);

	function RbsGenericWaitingJobs(scope, REST, i18n, NotificationCenter, ErrorFormatter) {
		JobListController('waiting', false, scope, REST, i18n, NotificationCenter, ErrorFormatter);
	}

	RbsGenericWaitingJobs.$inject = ['$scope', 'RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter',
		'RbsChange.ErrorFormatter'];
	app.controller('RbsGenericWaitingJobs', RbsGenericWaitingJobs);
})();