(function() {
	"use strict";

	function Editor(Models) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (scope.document.modelName) {
						scope.modelLabel = Models.getModelLabel(scope.document.modelName);
					}
				};

				scope.selectAll = function(group, contextName) {
					angular.forEach (group.attributes, function (attribute) {
						scope.document.visibilities[contextName].attributes[attribute.id + ''] = true;
					});
				};

				scope.deselectAll = function(group, contextName) {
					angular.forEach (group.attributes, function (attribute) {
						scope.document.visibilities[contextName].attributes[attribute.id + ''] = false;
					});
				};

				scope.checkDeleteContext = function (inlineDoc) {
					return !inlineDoc.locked;
				};
			}
		};
	}

	Editor.$inject = ['RbsChange.Models'];
	angular.module('RbsChange').directive('rbsDocumentEditorRbsGenericTypologyNew', Editor);
	angular.module('RbsChange').directive('rbsDocumentEditorRbsGenericTypologyEdit', Editor);
})();