<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic;

/**
 * @name \Rbs\Generic\GenericServices
 */
class GenericServices extends \Zend\ServiceManager\ServiceManager
{
	use \Change\Services\ServicesCapableTrait;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @return $this
	 */
	public function setApplicationServices(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->applicationServices = $applicationServices;
		return $this;
	}

	/**
	 * @return \Change\Services\ApplicationServices
	 */
	protected function getApplicationServices()
	{
		return $this->applicationServices;
	}

	/**
	 * @return array<alias => className>
	 */
	protected function loadInjectionClasses()
	{
		$classes = $this->getApplication()->getConfiguration('Rbs/Generic/Services');
		return is_array($classes) ? $classes : [];
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function __construct(\Change\Application $application, \Change\Services\ApplicationServices $applicationServices)
	{
		$this->setApplication($application);
		$this->setApplicationServices($applicationServices);

		parent::__construct(['shared_by_default' => true]);

		//SeoManager : Application, DocumentManager, TransactionManager, DbProvider
		$class = $this->getInjectedClassName('SeoManager', \Rbs\Seo\SeoManager::class);
		$this->setAlias('SeoManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Seo\SeoManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager())
				->setTransactionManager($this->applicationServices->getTransactionManager())
				->setDbProvider($this->applicationServices->getDbProvider());
			return $manager;
		});

		//AvatarManager : Application
		$class = $this->getInjectedClassName('AvatarManager', \Rbs\Media\Avatar\AvatarManager::class);
		$this->setAlias('AvatarManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Media\Avatar\AvatarManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//FieldManager : Application, ConstraintsManager
		$class = $this->getInjectedClassName('FieldManager', \Rbs\Simpleform\Field\FieldManager::class);
		$this->setAlias('FieldManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Simpleform\Field\FieldManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setConstraintsManager($this->applicationServices->getConstraintsManager());
			return $manager;
		});

		//SecurityManager : Application
		$class = $this->getInjectedClassName('SecurityManager', \Rbs\Simpleform\Security\SecurityManager::class);
		$this->setAlias('SecurityManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Simpleform\Security\SecurityManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//GeoManager : Application
		$class = $this->getInjectedClassName('GeoManager', \Rbs\Geo\GeoManager::class);
		$this->setAlias('GeoManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Geo\GeoManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//@deprecated since 1.8.0 use ElasticsearchManager
		//FacetManager : Application, DocumentManager
		$class = $this->getInjectedClassName('FacetManager', \Rbs\Elasticsearch\Facet\FacetManager::class);
		$this->setAlias('FacetManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Elasticsearch\Facet\FacetManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});



		//@deprecated since 1.8.0 use ElasticsearchManager
		//IndexManager : Application, DocumentManager
		$class = $this->getInjectedClassName('IndexManager', \Rbs\Elasticsearch\Index\IndexManager::class);
		$this->setAlias('IndexManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Elasticsearch\Index\IndexManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});

		//ElasticsearchManager : Application, DocumentManager
		$class = $this->getInjectedClassName('ElasticsearchManager', \Rbs\Elasticsearch\Manager::class);
		$this->setAlias('ElasticsearchManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Elasticsearch\Manager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});

		//@deprecated since 1.9.0 with no replacment
		//MailManager : Application, DocumentManager, JobManager
		$class = $this->getInjectedClassName('MailManager', \Rbs\Mail\MailManager::class);
		$this->setAlias('MailManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Mail\MailManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager())
				->setJobManager($this->applicationServices->getJobManager());
			return $manager;
		});

		//AdminManager : Application, i18nManager, ModelManager, PluginManager
		$class = $this->getInjectedClassName('AdminManager', \Rbs\Admin\AdminManager::class);
		$this->setAlias('AdminManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Admin\AdminManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setI18nManager($this->applicationServices->getI18nManager())
				->setModelManager($this->applicationServices->getModelManager())
				->setPluginManager($this->applicationServices->getPluginManager());
			return $manager;
		});


		//UserManager : Application
		$class = $this->getInjectedClassName('UserManager', \Rbs\User\UserManager::class);
		$this->setAlias('UserManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\User\UserManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		//ReviewManager : Application, DocumentManager
		$class = $this->getInjectedClassName('ReviewManager', \Rbs\Review\ReviewManager::class);
		$this->setAlias('ReviewManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Review\ReviewManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});

		//MailinglistManager : Application, DocumentManager, TransactionManager, I18nManager
		$class = $this->getInjectedClassName('MailinglistManager', \Rbs\Mailinglist\MailinglistManager::class);
		$this->setAlias('MailinglistManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Mailinglist\MailinglistManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager())
				->setI18nManager($this->applicationServices->getI18nManager())
				->setTransactionManager($this->applicationServices->getTransactionManager());
			return $manager;
		});

		//NotificationManager: Application, DocumentManager, JobManager, CacheManager, I18nManager
		// AuthenticationManager, MailManager, SmsManager, SviManager
		$class = $this->getInjectedClassName('NotificationManager', \Rbs\Generic\Notification\Manager::class);
		$this->setAlias('NotificationManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Generic\Notification\Manager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setCacheManager($this->applicationServices->getCacheManager())
				->setDocumentManager($this->applicationServices->getDocumentManager())
				->setI18nManager($this->applicationServices->getI18nManager())
				->setAuthenticationManager($this->applicationServices->getAuthenticationManager())
				->setJobManager($this->applicationServices->getJobManager())
				->setMailManager($this->applicationServices->getMailManager())
				->setSmsManager($this->applicationServices->getSmsManager())
				->setSviManager($this->applicationServices->getSviManager());
			return $manager;
		});

		//SocialManager : Application
		$class = $this->getInjectedClassName('SocialManager', \Rbs\Social\SocialManager::class);
		$this->setAlias('SocialManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Social\SocialManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});
	}

	/**
	 * @api
	 * @return \Rbs\Seo\SeoManager
	 */
	public function getSeoManager()
	{
		return $this->get('SeoManager');
	}

	/**
	 * @api
	 * @return \Rbs\Media\Avatar\AvatarManager
	 */
	public function getAvatarManager()
	{
		return $this->get('AvatarManager');
	}

	/**
	 * @api
	 * @return \Rbs\Simpleform\Field\FieldManager
	 */
	public function getFieldManager()
	{
		return $this->get('FieldManager');
	}

	/**
	 * @api
	 * @return \Rbs\Simpleform\Security\SecurityManager
	 */
	public function getSecurityManager()
	{
		return $this->get('SecurityManager');
	}

	/**
	 * @deprecated since 1.8.0 use getElasticsearchManager()
	 * @return \Rbs\Elasticsearch\Index\IndexManager
	 */
	public function getIndexManager()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this->get('IndexManager');
	}

	/**
	 * @api
	 * @return \Rbs\Elasticsearch\Manager
	 */
	public function getElasticsearchManager()
	{
		return $this->get('ElasticsearchManager');
	}

	/**
	 * @deprecated since 1.8.0 use getElasticsearchManager()
	 * @return \Rbs\Elasticsearch\Facet\FacetManager
	 */
	public function getFacetManager()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this->get('FacetManager');
	}

	/**
	 * @api
	 * @return \Rbs\Geo\GeoManager
	 */
	public function getGeoManager()
	{
		return $this->get('GeoManager');
	}

	/**
	 * @deprecated since 1.9.0 with no replacment
	 * @return \Rbs\Mail\MailManager
	 */
	public function getMailManager()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return $this->get('MailManager');
	}

	/**
	 * @api
	 * @return \Rbs\Admin\AdminManager
	 */
	public function getAdminManager()
	{
		return $this->get('AdminManager');
	}

	/**
	 * @api
	 * @return \Rbs\User\UserManager
	 */
	public function getUserManager()
	{
		return $this->get('UserManager');
	}

	/**
	 * @api
	 * @return \Rbs\Review\ReviewManager
	 */
	public function getReviewManager()
	{
		return $this->get('ReviewManager');
	}

	/**
	 * @api
	 * @return \Rbs\Mailinglist\MailinglistManager
	 */
	public function getMailinglistManager()
	{
		return $this->get('MailinglistManager');
	}

	/**
	 * @api
	 * @return \Rbs\Generic\Notification\Manager
	 */
	public function getNotificationManager()
	{
		return $this->get('NotificationManager');
	}

	/**
	 * @api
	 * @return \Rbs\Social\SocialManager
	 */
	public function getSocialManager()
	{
		return $this->get('SocialManager');
	}
}