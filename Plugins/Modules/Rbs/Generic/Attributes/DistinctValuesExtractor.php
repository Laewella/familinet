<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
* @name \Rbs\Generic\Attributes\DistinctValuesExtractor
*/
class DistinctValuesExtractor
{
	/**
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return array
	 */
	public function execute(\Rbs\Generic\Documents\Attribute $attribute, $dbProvider)
	{
		$distinctValues = [];
		if ($attribute->getValueType() == \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING)
		{
			$queryBuilder = $dbProvider->getNewQueryBuilder();
			$fb = $queryBuilder->getFragmentBuilder();
			$queryBuilder->select($fb->column('string_value'));
			$queryBuilder->distinct();
			$queryBuilder->from($fb->table('rbs_generic_dat_attributes_index'));
			$queryBuilder->where(
				$fb->logicAnd(
					$fb->eq($fb->column('attribute_id'), $fb->integerParameter('id')),
					$fb->isNotNull($fb->column('string_value'))
				)
			);
			$select = $queryBuilder->query();
			$select->bindParameter('id', $attribute->getId());
			$distinctValues = $select->getResults($select->getRowsConverter()->addTxtCol('string_value')->singleColumn('string_value'));
		}
		return $distinctValues;
	}
}