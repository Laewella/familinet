<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\ToIndexDocuments
 */
class ToIndexDocuments
{
	/**
	 * @var integer[]|null
	 */
	protected $toIndex;

	/**
	 * @param \Change\Events\Event $event
	 */
	public function start($event)
	{
		if ($event instanceof \Change\Events\Event && $event->getParam('primary'))
		{
			$this->toIndex = [];
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function indexDocument($event)
	{
		if ($this->toIndex !== null && $event instanceof \Change\Documents\Events\Event)
		{
			$documentId = $event->getDocument()->getId();
			if (!in_array($documentId, $this->toIndex))
			{
				$this->toIndex[] = $documentId;
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addJob($event)
	{
		if ($this->toIndex !== null && $event instanceof \Change\Events\Event && $event->getParam('primary'))
		{
			if (count($this->toIndex))
			{
				$jobManager = $event->getApplicationServices()->getJobManager();
				$jobManager->createNewJob('Rbs_Generic_Attributes_IndexForFilters', ['toIndex' => $this->toIndex], null, false);
			}
			$this->toIndex = null;
		}
	}
}