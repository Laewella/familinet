<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Notification;

/**
 * @name \Rbs\Generic\Notification\Manager
 */
class Manager extends \Rbs\Notification\Manager
{
	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		$identifiers = parent::getEventManagerIdentifier();
		$identifiers[] = 'GenericNotificationManager';
		return $identifiers;
	}

	/**
	 * @param \Rbs\Mailinglist\Documents\Subscriber $subscriber
	 * @param \Rbs\Website\Documents\Website|null $website
	 * @return \Change\Job\JobInterface|null
	 */
	public function mailingListManageRequest(\Rbs\Mailinglist\Documents\Subscriber $subscriber, \Rbs\Website\Documents\Website $website)
	{
		return $this->addJob('rbs_mailinglist_manage_request', ['subscriberId' => $subscriber->getId(), 'websiteId' => $website->getId()]);
	}

	/**
	 * @param string $email
	 * @param string $token
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string $LCID
	 * @return \Change\Job\JobInterface|null
	 */
	public function mailingListSubscriptionRequest($email, $token, \Rbs\Website\Documents\Website $website, $LCID)
	{
		return $this->addJob('rbs_mailinglist_subscription_request', ['email' => (string)$email,
			'token' => (string)$token, 'websiteId' => $website->getId(), 'LCID' => (string)$LCID]);
	}

	/**
	 * @param string $email
	 * @param string $token
	 * @param \Rbs\Website\Documents\Website|null $website
	 * @param string $link
	 * @param bool $confirmationPage
	 * @return \Change\Job\JobInterface|null
	 */
	public function userAccountRequest($email, $token, \Rbs\Website\Documents\Website $website = null, $link = null, $confirmationPage = false)
	{
		return $this->addJob('rbs_user_account_request', ['email' => (string)$email,
			'token' => (string)$token,
			'websiteId' => $website ? $website->getId() : 0,
			'link' => (string)$link, 'confirmationPage' => (bool)$confirmationPage]);
	}

	/**
	 * @param  \Rbs\User\Documents\User $user
	 * @param \Rbs\Website\Documents\Website $website
	 * @return \Change\Job\JobInterface|null
	 */
	public function userAccountValid(\Rbs\User\Documents\User $user, \Rbs\Website\Documents\Website $website = null)
	{
		return $this->addJob('rbs_user_account_valid',
			['userId' => $user->getId(), 'websiteId' => $website ? $website->getId() : 0]);
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @param string $email
	 * @param string $token
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string|null $confirmationUrl
	 * @return \Change\Job\JobInterface|null
	 */
	public function userChangeEmailRequest(\Rbs\User\Documents\User $user, $email, $token,
		\Rbs\Website\Documents\Website $website = null, $confirmationUrl)
	{
		return $this->addJob('rbs_user_change_email_request',
			['userId' => $user->getId(), (string)'email' => $email, (string)'token' => $token,
				'confirmationUrl' => $confirmationUrl,
				'websiteId' => $website ? $website->getId() : 0]);
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @param \Rbs\Website\Documents\Website $website
	 * @return \Change\Job\JobInterface|null
	 */
	public function userMailChanged(\Rbs\User\Documents\User $user, \Rbs\Website\Documents\Website $website = null)
	{
		return $this->addJob('rbs_user_mail_changed',
			['userId' => $user->getId(), 'website' => $website ? $website->getId() : 0]);
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @param string $token
	 * @param \Rbs\Website\Documents\Website|null $website
	 * @param string $link
	 * @return \Change\Job\JobInterface|null
	 */
	public function userResetPasswordRequest(\Rbs\User\Documents\User $user, $token, $website, $link)
	{
		return $this->addJob('rbs_user_reset_password_request',
			['userId' => $user->getId(), 'token' => (string)$token,
				'websiteId' => $website ? $website->getId() : 0, 'link' => (string)$link]);
	}

	/**
	 * @param $mobilePhone
	 * @param $token
	 * @return \Change\Job\JobInterface|null
	 */
	public function userValidMobilePhoneRequest($mobilePhone, $token)
	{
		return $this->addJob('rbs_user_valid_mobile_phone_request', ['mobilePhone' => $mobilePhone, 'token' => $token]);
	}

	/**
	 * @param string $email
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string $link
	 * @return \Change\Job\JobInterface|null
	 */
	public function suggestAccountCreation($email, \Rbs\Website\Documents\Website $website = null, $link = null)
	{
		return $this->addJob('rbs_user_suggest_account_creation',
			['email' => (string)$email, 'websiteId' => $website ? $website->getId() : 0, 'link' => (string)$link]);
	}

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const MAILING_LIST_MANAGE_REQUEST = 'rbs_mailinglist_manage_request';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const MAILING_LIST_SUBSCRIPTION_REQUEST = 'rbs_mailinglist_subscription_request';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const USER_ACCOUNT_REQUEST = 'rbs_user_account_request';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const USER_ACCOUNT_VALID = 'rbs_user_account_valid';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const USER_CHANGE_EMAIL_REQUEST = 'rbs_user_change_email_request';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const USER_MAIL_CHANGED = 'rbs_user_mail_changed';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const USER_RESET_PASSWORD_REQUEST = 'rbs_user_reset_password_request';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const USER_VALID_MOBILE_PHONE_REQUEST = 'rbs_user_valid_mobile_phone_request';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 */
	const SUGGEST_ACCOUNT_CREATION = 'rbs_user_suggest_account_creation';

	/**
	 * @deprecated since 1.9.0 with no replacement
	 * @return \DateTime|false
	 */
	public function getJobAt()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return false;
	}

	/**
	 * @deprecated since 1.9.0 with no replacement
	 * @param boolean|\DateTime $jobAt
	 * @return $this
	 */
	public function setJobAt($jobAt)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return $this;
	}

	/**
	 * @deprecated since 1.9.0 with no replacement
	 * @param string $code
	 * @param array $parameters
	 * @return array
	 */
	public function sendByJob($code, array $parameters)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return [];
	}
}