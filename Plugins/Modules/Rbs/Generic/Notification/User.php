<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Notification;

/**
 * @name \Rbs\Generic\Notification\User
 */
class User implements \Rbs\Notification\NotificationInterface
{
	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var \Rbs\Notification\Manager|null
	 */
	protected $manager;

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @param string $code
	 * @param array $data
	 */
	public function __construct($code, array $data)
	{
		$this->code = $code;
		$this->data = $this->normalizeConfiguration($data);
	}

	/**
	 * @return \Rbs\Notification\Manager|null
	 */
	public function getManager()
	{
		return $this->manager;
	}

	/**
	 * @param \Rbs\Notification\Manager $manager
	 * @return $this
	 */
	public function setManager(\Rbs\Notification\Manager $manager)
	{
		$this->manager = $manager;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @param array $parameters
	 * @return array
	 */
	public function send(array $parameters)
	{
		$config = $this->data['template'] ?? [];
		return $this->sendToCustomer($config, $parameters);
	}

	/**
	 * @param $config
	 * @param array $parameters
	 * @return array
	 */
	protected function sendToCustomer(array $config, array $parameters): array
	{
		$result = [];
		$manager = $this->getManager();
		$customerLCID = $parameters['__customerLCID'] ?? null;

		$customerEmail = $parameters['__customerMail'] ?? null;
		$customerMailId = $config['customer']['mail'] ?? 0;
		$sendMail = $customerMailId && $customerEmail;

		$customerSMS = $parameters['__customerSMS'] ?? null;
		$customerSmsId = $config['customer']['sms'] ?? 0;
		$sendSMS = ($customerSMS && $customerSmsId);
		if ($sendSMS || $sendMail)
		{
			$mail = null;
			$sms = null;
			$manager->setLCIDContext($customerLCID);

			if ($sendMail)
			{
				$mail = $manager->resolveMail($customerMailId, $parameters['websiteId'] ?? 0);
			}
			if ($sendSMS)
			{
				$sms = $manager->resolveSMS($customerSmsId);
			}

			if (!$mail && !$sms)
			{
				return $result;
			}

			$substitutions = $manager->buildSubstitutionValues($this, $parameters);
			if ($mail)
			{
				$result['__customerMail'] = $manager->sendMail($customerEmail, $mail, $substitutions);
			}
			if ($sms)
			{
				$result['__customerSMS'] = $manager->sendSMS($customerSMS, $sms, $substitutions);
			}
		}
		return $result;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	protected function normalizeConfiguration(array $data)
	{
		if ($data['static']['disableMail'] ?? false)
		{
			unset($data['template']['customer']['mail']);
		}
		if ($data['static']['disableSms'] ?? false)
		{
			unset($data['template']['customer']['sms']);
		}
		return $data;
	}
}