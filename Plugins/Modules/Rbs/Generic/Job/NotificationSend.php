<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Job;

/**
 * @deprecated since 1.9.0 with no replacment
 * @name \Rbs\Generic\Job\NotificationSend
 */
class NotificationSend
{
	/**
	 * @deprecated since 1.9.0 with no replacment
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);

		$job = $event->getJob();
		$code = $job->getArgument('__code');
		$LCID = $job->getArgument('LCID');
		if (!$code || !$LCID)
		{
			$event->failed('Invalid job parameters');
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$applicationServices->getI18nManager()->setLCID($LCID);

		$transactionManager = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();

		try
		{
			$documentManager->pushLCID($LCID);
			$transactionManager->begin();
			$parameters = $job->getArguments();
			if ($this->decodeParams($parameters, $documentManager))
			{
				$notificationCode = $code;
				switch ($code)
				{
					case 'rbs_mailinglist_manage_request' :
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						unset($parameters['requestData']);
						break;
					case 'rbs_mailinglist_subscription_request':
						$parameters['email'] = $parameters['requestData']['email'] ?? null;
						$parameters['token'] = $parameters['requestData']['token'] ?? null;
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						$parameters['LCID'] = $parameters['requestData']['LCID'] ?? null;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_account_request':
						$parameters['email'] = $parameters['requestData']['email'] ?? null;
						$parameters['token'] = $parameters['requestData']['token'] ?? null;
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						$parameters['link'] = $parameters['requestData']['link'] ?? null;
						$parameters['confirmationPage'] = $parameters['requestData']['confirmationPage'] ?? false;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_account_valid':
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_change_email_request':
						$parameters['email'] = $parameters['requestData']['email'] ?? null;
						$parameters['token'] = $parameters['requestData']['token'] ?? null;
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_mail_changed':
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_reset_password_request':
						$parameters['token'] = $parameters['requestData']['token'] ?? null;
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_valid_mobile_phone_request':
						$parameters['token'] = $parameters['requestData']['token'] ?? null;
						$parameters['mobilePhone'] = $parameters['requestData']['mobilePhone'] ?? null;
						unset($parameters['requestData']);
						break;
					case 'rbs_user_suggest_account_creation':
						$parameters['link'] = $parameters['requestData']['link'] ?? null;
						$parameters['websiteId'] = $parameters['requestData']['websiteId'] ?? 0;
						unset($parameters['requestData']);
						break;
				}

				if ($notificationCode)
				{
					$parameters['__notificationCode'] = $notificationCode;
					$newJob = $applicationServices->getJobManager()->createNewJob('Rbs_Notification_Send', $parameters);

					$event->setResultArgument('newJob', $newJob->getId());
				}
				$event->setResultArgument('sendResult', false);
				$event->failed('@deprecated since 1.9.0');
			}
			else
			{
				$event->failed('Invalid notification parameters');
			}

			$transactionManager->commit();
			$documentManager->popLCID();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			$documentManager->popLCID();
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param array $parameters
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array|null
	 */
	protected function decodeParams(&$parameters, \Change\Documents\DocumentManager $documentManager)
	{
		$ids = [];
		foreach ($parameters as &$value)
		{
			if (is_array($value))
			{
				$count = count($value);
				if ($count === 2 && isset($value['__id'], $value['__model']))
				{
					$doc = $documentManager->getDocumentInstance($value['__id'], $value['__model']);
					if ($doc instanceof \Rbs\Mailinglist\Documents\Subscriber)
					{
						$ids['subscriberId'] = $doc->getId();
					}
					elseif ($doc instanceof \Rbs\Website\Documents\Website)
					{
						$ids['websiteId'] = $doc->getId();
					}
					elseif ($doc instanceof \Rbs\User\Documents\User)
					{
						$ids['userId'] = $doc->getId();
					}
					$value = null;
				}
				elseif ($count === 1 && isset($value['__dateTime']))
				{
					$value = $value['__dateTime'];
				}
				elseif ($count)
				{
					$value = $this->decodeParams($value, $documentManager);
				}
			}
		}
		unset($value);
		$parameters = array_merge($ids, array_filter($parameters, function ($v) { return $v !== null; }));
		return $parameters;
	}
}