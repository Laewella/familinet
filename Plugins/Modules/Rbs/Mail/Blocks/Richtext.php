<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Blocks;

/**
 * @name \Rbs\Mail\Blocks\Richtext
 */
class Richtext extends \Rbs\Website\Blocks\Richtext
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->setLayoutParameters($event->getBlockLayout());
		$parameters->setNoCache();

		return $parameters;
	}

	/**
	 * @return string
	 */
	protected function getRichTextProfile()
	{
		return 'Mail';
	}
}