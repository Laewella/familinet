<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Blocks;

/**
 * @name \Rbs\Mail\Blocks\TextInformation
 */
class TextInformation extends \Change\Presentation\Blocks\Information
{
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.mail.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.website.admin.text', $ucf));
		$this->disableCache();
		$this->setMailSuitable(true);

		$this->addParameterInformationForDetailBlock('Rbs_Website_Text', $i18nManager);

		$templateInformation = $this->addTemplateInformation('Rbs_Mail', 'text.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.text_only', $ucf));

		$templateInformation = $this->addTemplateInformation('Rbs_Mail', 'textWithTitle.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.text_with_title', $ucf));
		$templateInformation->addParameterInformation('titleLevel', \Change\Documents\Property::TYPE_INTEGER, false, 1)
			->setLabel($i18nManager->trans('m.rbs.website.admin.text_title_level', $ucf))
			->setNormalizeCallback(function ($parametersValues) { return min(6, max(1, (int)($parametersValues['titleLevel'] ?? 1))); });
	}
}