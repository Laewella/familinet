<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Events;

/**
 * @deprecated since 1.9.0 with no replacment
 * @name \Rbs\Mail\Events\InstallMails
 */
class InstallMails
{
	/**
	 * @param \Change\Events\Event $event
	 * @param string $vendorName
	 * @param string $moduleName
	 * @throws \Exception
	 */
	public function execute(\Change\Events\Event $event, $vendorName, $moduleName)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
	}
}