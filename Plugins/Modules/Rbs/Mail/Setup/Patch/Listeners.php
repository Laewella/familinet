<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Setup\Patch;

/**
 * @name \Rbs\Mail\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		//Rbs_Mail_0001 v1.9 Refactor isVariant to variantOf
		$this->executePatch('Rbs_Mail_0001', 'Refactor isVariant to variantOf', [$this, 'patch0001']);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 */
	protected function patch0001(\Change\Plugins\Patch\Patch $patch)
	{

		$dbProvider = $this->applicationServices->getDbProvider();
		$transactionManager = $this->applicationServices->getTransactionManager();
		$mapping = $dbProvider->getSqlMapping();
		$tableDefinition = $dbProvider->getSchemaManager()->getTableDefinition($mapping->getDocumentTableName('Rbs_Mail_Mail'));
		if (!$tableDefinition)
		{
			throw new \RuntimeException('Module Rbs_Mail is not properly installed.', 99999);
		}
		if (!$tableDefinition->getField($mapping->getDocumentFieldName('isVariation')))
		{
			$this->sendInfo('No migration necessary.');
			$this->patchManager->installedPatch($patch);
			return;
		}

		$countVariantOfMigrate = 0;
		try
		{
			$transactionManager->begin();
			$qbSub = $dbProvider->getNewQueryBuilder();
			$fb = $qbSub->getFragmentBuilder();

			$qbSub->select($fb->getDocumentColumn('code'))->from($fb->getDocumentTable('Rbs_Mail_Mail'));
			$qbSub->where(
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('isVariation'), $fb->number(1)),
					$fb->eq($fb->getDocumentColumn('variantOf'), $fb->number(0))
				)
			);
			$selectSubQuery = $qbSub->query();
			$subResult = $selectSubQuery->getResults();

			if ($subResult)
			{
				$qb = $dbProvider->getNewQueryBuilder();
				$fb = $qbSub->getFragmentBuilder();
				$qb->select($fb->getDocumentColumn('id'), $fb->getDocumentColumn('code'))->from($fb->getDocumentTable('Rbs_Mail_Mail'));
				$qb->where(
					$fb->logicAnd(
						$fb->in($fb->getDocumentColumn('code'), $fb->subQuery($selectSubQuery)),
						$fb->eq($fb->getDocumentColumn('isVariation'), $fb->number(0)),
						$fb->eq($fb->getDocumentColumn('variantOf'), $fb->number(0))
					)
				);
				$selectQuery = $qb->query();
				$parentMailList = $selectQuery->getResults();

				$documentManager = $this->applicationServices->getDocumentManager();

				foreach ($parentMailList as $parentMail)
				{
					$qb = $dbProvider->getNewQueryBuilder();
					$fb = $qbSub->getFragmentBuilder();
					$qb->select($fb->getDocumentColumn('id'))->from($fb->getDocumentTable('Rbs_Mail_Mail'));
					$qb->where(
						$fb->logicAnd(
							$fb->in($fb->getDocumentColumn('code'), $parentMail['code']),
							$fb->eq($fb->getDocumentColumn('isVariation'), $fb->number(1))
						)
					);
					$mailQuery = $qb->query();

					foreach ($mailQuery->getResults() as $mailId)
					{
						/** @var \Rbs\Mail\Documents\Mail $documentMail */
						$documentMail = $documentManager->getDocumentInstance($mailId['document_id']);
						$documentParentMail = $documentManager->getDocumentInstance($parentMail['document_id']);
						if ($documentMail && $documentParentMail)
						{
							$documentMail->setVariantOf($documentParentMail);
							$documentMail->save();
							$countVariantOfMigrate++;
						}
					}
				}
				$this->sendInfo('VariantOf migrated: ' . $countVariantOfMigrate);
			}
			else
			{
				$this->sendInfo('No variantOf to migrate.');
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$patch->setException($e);
			$transactionManager->rollBack($e);
			return;
		}

		$patch->addInstallationData('RefactorVariantOf: ', $countVariantOfMigrate);
		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}
}