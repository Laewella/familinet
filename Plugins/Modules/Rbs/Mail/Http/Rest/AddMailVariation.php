<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Http\Rest;

/**
 * @name \Rbs\Mail\Http\Rest\AddMailVariation
 */
class AddMailVariation
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Http\Event $event)
	{
		$documentId = $event->getRequest()->getPost('documentId');
		if ($documentId)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$tm = $event->getApplicationServices()->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $document \Rbs\Mail\Documents\Mail */
				$document = $documentManager->getDocumentInstance($documentId);
				if ($document)
				{
					$refLCID = $document->getRefLCID();
					$documentManager->pushLCID($refLCID);
					/* @var $variation \Rbs\Mail\Documents\Mail */
					$variation = $documentManager->getNewDocumentInstanceByModelName('Rbs_Mail_Mail');
					$variation->setRefLCID($refLCID);
					$variation->setCode($document->getCode());
					$variation->setLabel($document->getLabel());
					$variation->setSubstitutions($document->getSubstitutions());
					$variation->setTemplate($document->getTemplate());
					$variation->setUseCache($document->getUseCache());
					$variation->setTTL($document->getTTL());
					$variation->setVariantOf($document);

					/** @noinspection DisconnectedForeachInstructionInspection */
					$variationLocalization = $variation->getCurrentLocalization();
					/** @noinspection DisconnectedForeachInstructionInspection */
					$documentLocalization = $document->getCurrentLocalization();
					$variationLocalization->setSubject($documentLocalization->getSubject());
					$variationLocalization->setSenderMail($documentLocalization->getSenderMail());
					$variationLocalization->setSenderName($documentLocalization->getSenderName());
					$variationLocalization->setEditableContent($documentLocalization->getEditableContent());
					$variationLocalization->setActive($documentLocalization->getActive());
					$variationLocalization->setStartActivation($documentLocalization->getStartActivation());
					$variationLocalization->setEndActivation($documentLocalization->getEndActivation());

					$variation->save();

					foreach ($document->getLCIDArray() as $LCID)
					{
						if ($LCID === $refLCID)
						{
							continue;
						}
						$documentManager->pushLCID($LCID);
						/** @noinspection DisconnectedForeachInstructionInspection */
						$variationLocalization = $variation->getCurrentLocalization();
						/** @noinspection DisconnectedForeachInstructionInspection */
						$documentLocalization = $document->getCurrentLocalization();
						$variationLocalization->setSubject($documentLocalization->getSubject());
						$variationLocalization->setSenderMail($documentLocalization->getSenderMail());
						$variationLocalization->setSenderName($documentLocalization->getSenderName());
						$variationLocalization->setEditableContent($documentLocalization->getEditableContent());
						$variationLocalization->setActive($documentLocalization->getActive());
						$variationLocalization->setStartActivation($documentLocalization->getStartActivation());
						$variationLocalization->setEndActivation($documentLocalization->getEndActivation());

						/** @noinspection DisconnectedForeachInstructionInspection */
						$variation->save();
						/** @noinspection DisconnectedForeachInstructionInspection */
						$documentManager->popLCID();
					}
					$tm->commit();
					$event->setParam('documentId', $variation->getId());
					$event->setParam('modelName', $variation->getDocumentModelName());
					$action = new \Change\Http\Rest\V1\Resources\GetDocument();
					$action->execute($event);
				}
				else
				{
					$result = new \Change\Http\Rest\V1\ErrorResult(999999, 'invalid document mail id');
					$event->setResult($result);
				}
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}
}