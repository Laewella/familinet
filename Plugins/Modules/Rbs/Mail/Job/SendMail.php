<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Job;

/**
 * @deprecated since 1.9.0 with no replacment
 * @name \Rbs\Mail\Job\SendMail
 */
class SendMail
{
	/**
	 * @param \Change\Job\Event $event
	 */
	public function execute(\Change\Job\Event $event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		$event->failed('@deprecated since 1.9.0 with no replacment');
	}
}