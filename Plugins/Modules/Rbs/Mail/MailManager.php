<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail;

/**
 * @deprecated since 1.9.0 with no replacment
 * @name \Rbs\Mail\MailManager
 */
class MailManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'MailManager';

	/**
	 * @deprecated since 1.9.0 use \Change\Stdlib\StringUtils::DEFAULT_SUBSTITUTION_REGEXP
	 */
	const VARIABLE_REGEXP = '/\{([a-z][A-Za-z0-9.]*)\}/';

	/**
	 * @var \Change\Job\JobManager
	 */
	protected $jobManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 * @return $this
	 */
	public function setJobManager($jobManager)
	{
		$this->jobManager = $jobManager;
		return $this;
	}

	/**
	 * @return \Change\Job\JobManager
	 */
	protected function getJobManager()
	{
		return $this->jobManager;
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Mail/Events/MailManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('render', function ($event) { $this->onDefaultRender($event); }, 5);
	}

	/**
	 * @deprecated since 1.9.0 with no replacment
	 * @param string $code
	 * @param \Change\Presentation\Interfaces\Website|null $website
	 * @param string|array $to
	 * @param array $substitutions
	 * @param array $attachments [url => mime]
	 * @return \Zend\Mail\Message|null
	 */
	public function getMailMessage($code, $website, $to, array $substitutions = [], array $attachments = [])
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return null;
	}

	/**
	 * @deprecated since 1.9.0 with no replacment
	 * @return string[]
	 */
	public function getCodes()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return [];
	}

	/**
	 * @deprecated since 1.9.0 use \Change\Mail\MailManager::buildHtmlContent()
	 * @param \Rbs\Mail\Documents\Mail $mail
	 * @param \Change\Presentation\Interfaces\Website|null $website
	 * @param string $LCID
	 * @param array $substitutions
	 * @return string
	 */
	public function render($mail, $website, $LCID, $substitutions)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'mail' => $mail,
			'website' => $website,
			'LCID' => $LCID,
			'substitutions' => $substitutions
		]);
		$eventManager->trigger('render', $this, $args);
		return $args['html'] ?? '';
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDefaultRender($event)
	{
		$applicationServices = $event->getApplicationServices();
		$html = $applicationServices->getMailManager()->buildHtmlContent($event->getParam('mail'), $event->getParam('website'),
			$event->getParam('LCID'), $event->getParam('substitutions'));
		$event->setParam('html', $html);
	}

	/**
	 * @deprecated since 1.9.0 with no replacement
	 * @param array|string $email
	 * @return boolean
	 */
	public function isValidEmailFormat($email)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		return false;
	}

	/**
	 * @api
	 * @param \Rbs\Theme\Documents\Template $template
	 * @param string[] $filters
	 * @param boolean $force
	 */
	public function installMails($template, $filters, $force = false)
	{
		$eventManager = $this->getEventManager();
		$eventManager->trigger('installMails', $this, ['mailTemplate' => $template, 'filters' => $filters, 'force' => $force]);
	}

	/**
	 * @deprecated since 1.9.0 use \Change\Stdlib\StringUtils::getSubstitutedString
	 * @param string $string
	 * @param array $substitutions
	 * @return string|null
	 */
	public function getSubstitutedString($string, $substitutions)
	{
		return \Change\Stdlib\StringUtils::getSubstitutedString($string, $substitutions);
	}
}