<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail;

/**
 * @name \Rbs\Mail\DefaultWebsite
 */
class DefaultWebsite implements \Change\Presentation\Interfaces\Website
{
	/**
	 * @var string
	 */
	protected $LCID;
	/**
	 * @var string
	 */
	protected $mailSender;

	/**
	 * @var NullUrlManager
	 */
	protected $urlManager;

	/**
	 * @var \Zend\Uri\Http
	 */
	protected $uri;

	/**
	 * @param string $LCID
	 * @param $uri
	 */
	public function __construct($uri, $LCID, $mailSender)
	{
		$this->LCID = $LCID;
		$this->uri = new \Zend\Uri\Http($uri);
		$this->uri->setPath('/')->normalize();
		$this->mailSender = $mailSender;
		$this->urlManager = new NullUrlManager($this->uri);
	}

	/**
	 * @return integer
	 */
	public function getId()
	{
		return 0;
	}

	/**
	 * @return string
	 */
	public function getLCID()
	{
		return $this->LCID;
	}

	/**
	 * @return string
	 */
	public function getHostName()
	{
		return $this->uri->getHost();
	}

	/**
	 * @return integer
	 */
	public function getPort()
	{
		return $this->uri->getPort();
	}

	/**
	 * @return string
	 */
	public function getScriptName()
	{
		return '';
	}

	/**
	 * Returned string do not start and end with '/' char
	 * @return string|null
	 */
	public function getRelativePath()
	{
		return null;
	}

	/**
	 * @return string
	 */
	public function getBaseurl()
	{
		return $this->uri->toString();
	}

	/**
	 * @param string $LCID
	 * @return \Change\Http\Web\UrlManager
	 */
	public function getUrlManager($LCID)
	{
		return $LCID ? $this->urlManager->setLCID($LCID) : $this->urlManager;
	}

	/**
	 * @return string|null
	 */
	public function getMailSender()
	{
		return $this->mailSender;
	}
}