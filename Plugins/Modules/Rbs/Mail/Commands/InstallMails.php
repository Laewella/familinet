<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Rbs\Mail\Commands\InstallMails
 * @deprecated since 1.9.0 use \Rbs\Mail\Notification\InstallNotifications instead
 */
class InstallMails
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		trigger_error(__CLASS__ . ' is deprecated', E_USER_WARNING);
		$response = $event->getCommandResponse();
		$response->addWarningMessage('use rbs_notification:install-notifications command instead.');
	}
}