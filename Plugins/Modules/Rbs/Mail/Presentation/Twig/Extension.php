<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Rbs\Mail\Presentation\Twig;

/**
 * @name \Rbs\Mail\Presentation\Twig\Extension
 */
class Extension implements \Twig_ExtensionInterface
{
	/**
	 * @var \Change\Presentation\Interfaces\Website
	 */
	protected $website;

	/**
	 * @var \Change\Http\UrlManager
	 */
	protected $urlManager;

	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * Constructor
	 *
	 * @param \Change\Presentation\Interfaces\Website $website
	 * @param \Change\Application $application |null
	 * @param \Change\Services\ApplicationServices|null $applicationServices
	 */
	public function __construct(\Change\Presentation\Interfaces\Website $website, \Change\Application $application = null,
		\Change\Services\ApplicationServices $applicationServices = null)
	{
		$this->website = $website;
		$this->urlManager = $website->getUrlManager(null);
		$this->application = $application;
		$this->applicationServices = $applicationServices;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'Rbs_Mail';
	}

	/**
	 * {@inheritdoc}
	 */
	public function initRuntime(\Twig_Environment $environment)
	{
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTokenParsers()
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getNodeVisitors()
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFilters()
	{
		return [
			new \Twig_SimpleFilter('richText', [$this, 'richText'], ['is_safe' => ['all']])
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTests()
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('canonicalURL', [$this, 'canonicalURL']),
			new \Twig_SimpleFunction('imageURL', [$this, 'imageURL']),
			new \Twig_SimpleFunction('resourceURL', [$this, 'resourceURL'])
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getOperators()
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getGlobals()
	{
		return [];
	}

	public function getWebsite()
	{
		return $this->website;
	}

	/**
	 * @param \Change\Documents\RichtextProperty|array $richText
	 * @return string
	 */
	public function richText($richText)
	{
		if ($richText instanceof \Change\Documents\RichtextProperty)
		{
			$context = [
				'website' => $this->urlManager->getWebsite(),
				'currentURI' => $this->urlManager->getSelf()
			];
			return $this->applicationServices->getRichTextManager()->render($richText, 'Mail', $context);
		}
		elseif (is_array($richText) && array_key_exists('h', $richText) && isset($richText['e'], $richText['t']))
		{
			$context = [
				'website' => $this->urlManager->getWebsite(),
				'currentURI' => $this->urlManager->getSelf()
			];
			$richText = new \Change\Documents\RichtextProperty($richText);
			return $this->applicationServices->getRichTextManager()->render($richText, 'Mail', $context);
		}
		return htmlspecialchars((string)$richText);
	}

	/**
	 * @param string $relativePath
	 * @return string
	 */
	public function resourceURL($relativePath)
	{
		$old = $this->urlManager->absoluteUrl(true);
		$url = $this->getWebsite()->getBaseurl() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . $relativePath;
		$this->urlManager->absoluteUrl($old);
		return $url;
	}

	/**
	 * @param \Rbs\Media\Documents\Image|integer|string $image
	 * @param integer|array $maxWidth The max width or an array containing max width and max height.
	 * @param integer $maxHeight
	 * @return string
	 */
	public function imageURL($image, $maxWidth = 0, $maxHeight = 0)
	{
		if ($this->application && $this->applicationServices)
		{
			if (is_array($maxWidth))
			{
				$size = array_values($maxWidth);
				$maxWidth = $size[0] ?? 0;
				$maxHeight = $size[1] ?? $maxHeight;
			}
			$maxHeight = $maxHeight ?: (int)$this->application->getConfiguration('Rbs/Media/FullPage/height');
			$maxWidth = $maxWidth ?: (int)$this->application->getConfiguration('Rbs/Media/FullPage/width');

			if (is_string($image))
			{
				if (strpos($image, 'change://') === 0)
				{
					$storageManager = $this->applicationServices->getStorageManager();
					$storageURI = new \Zend\Uri\Uri($image);
					$storageURI->setQuery(['max-width' => (int)$maxWidth, 'max-height' => (int)$maxHeight]);
					$url = $storageURI->normalize()->toString();
					return $storageManager->getPublicURL($url);
				}
				return $image;
			}

			$doc = $image;
			if (is_numeric($doc))
			{
				$doc = $this->applicationServices->getDocumentManager()->getDocumentInstance($doc);
			}

			if ($doc instanceof \Rbs\Media\Documents\Image && $doc->activated())
			{
				return $doc->getPublicURL((int)$maxWidth, (int)$maxHeight);
			}
		}
		return '';
	}

	/**
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param array $query
	 * @param string|null $LCID
	 * @return string|null
	 */
	public function canonicalURL($document, $query = [], $LCID = null)
	{
		if (is_numeric($document) || $document instanceof \Change\Documents\AbstractDocument)
		{
			return $this->urlManager->getCanonicalByDocument($document, $query, $LCID)->normalize()->toString();
		}
		return null;
	}
}
