<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Setup;

/**
 * @name \Rbs\User\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	const SOCIAL_COUNT_TABLE = 'rbs_social_count';
	const SOCIAL_LINK_TABLE = 'rbs_social_link';

	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			$this->tables[self::SOCIAL_COUNT_TABLE] = $td = $schemaManager->newTableDefinition(self::SOCIAL_COUNT_TABLE);
			$websiteId = $schemaManager->newIntegerFieldDefinition('website_id')->setNullable(false);
			$documentId = $schemaManager->newIntegerFieldDefinition('document_id')->setNullable(false);
			$td->addField($websiteId)->addField($documentId)
				->addField($schemaManager->newIntegerFieldDefinition('count')->setNullable(false)->setDefaultValue('0'))
				->addField($schemaManager->newDateFieldDefinition('last_date')->setNullable(false))
				->addField($schemaManager->newTextFieldDefinition('data')->setNullable(false))
				->addKey($this->newPrimaryKey()->setFields([$websiteId, $documentId]));

			$this->tables[self::SOCIAL_LINK_TABLE] = $td = $schemaManager->newTableDefinition(self::SOCIAL_LINK_TABLE);
			$userId = $schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false);
			$socialUserId = $schemaManager->newVarCharFieldDefinition('social_user_id')->setNullable(false);
			$providerName = $schemaManager->newVarCharFieldDefinition('provider')->setNullable(false);
			$td->addField($userId)
				->addField($socialUserId)
				->addField($providerName)
				->addField($schemaManager->newDateFieldDefinition('created_date')->setNullable(false))
				->addKey($this->newPrimaryKey()->setFields([$userId, $socialUserId]));
		}

		return $this->tables;
	}
}
