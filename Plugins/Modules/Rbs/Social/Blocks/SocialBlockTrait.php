<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Blocks;

/**
 * @name \Rbs\Social\Blocks\SocialBlockTrait
 */
trait SocialBlockTrait
{
	/**
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return bool
	 */
	protected function checkHandleSocialLink($event, $parameters)
	{
		$handle = $parameters->getParameter('handleSocialLink') && $event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL');
		$parameters->setParameterValue('handleSocialLink', $handle);
		return $handle;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array|null
	 */
	protected function getProvidersData($documentManager)
	{
		$query = $documentManager->getNewQuery('Rbs_Social_SocialConfiguration');
		$query->andPredicates($query->activated());
		$providers = [];
		foreach ($query->getDocuments() as $configuration)
		{
			/** @var \Rbs\Social\Documents\SocialConfiguration $configuration */
			$providers[] = [
				'id' => $configuration->getId(),
				'provider' => $configuration->getProvider()
			];
		}
		return $providers;
	}
}