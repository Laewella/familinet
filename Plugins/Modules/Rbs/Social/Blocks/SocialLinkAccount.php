<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Blocks;

/**
 * @name \Rbs\Social\Blocks\SocialLinkAccount
 */
class SocialLinkAccount extends \Change\Presentation\Blocks\Standard\Block
{
	use \Rbs\Social\Blocks\SocialBlockTrait;

	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('handleSocialLink', true);
		$parameters->setLayoutParameters($event->getBlockLayout());

		$this->checkHandleSocialLink($event, $parameters);

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 * @return string|null
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		if ($parameters->getParameter('handleSocialLink'))
		{
			$providers = $this->getProvidersData($event->getApplicationServices()->getDocumentManager());
			if ($providers)
			{
				$attributes['providers'] = $providers;
				return 'social-link-account.twig';
			}
		}
		return null;
	}
}