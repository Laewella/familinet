<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social;

/**
 * @name \Rbs\Social\SocialManager
 */
class SocialManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'SocialManager';
	const VARIABLE_REGEXP = '/\{([a-z][A-Za-z0-9.]*)\}/';

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Social/Events/SocialManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 * @throws \Zend\EventManager\Exception\InvalidArgumentException
	 * @throws \Change\Transaction\RollbackException
	 * @throws \RuntimeException
	 * @throws \LogicException
	 * @throws \Exception
	 * @throws \InvalidArgumentException
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('getSocialCount', function ($event) { $this->onDefaultGetSocialCount($event); }, 5);
		$eventManager->attach('addSocialCount', function ($event) { $this->onDefaultAddSocialCount($event); }, 5);
		$eventManager->attach('addSocialLink', function ($event) { $this->onDefaultAddSocialLink($event); }, 5);
		$eventManager->attach('deleteSocialLink', function ($event) { $this->onDefaultDeleteSocialLink($event); }, 5);
		$eventManager->attach('getSocialLinks', function ($event) { $this->onDefaultGetSocialLinks($event); }, 5);
		$eventManager->attach('getSocialLinkWithSocialId', function ($event) { $this->onDefaultGetSocialLinkWithSocialId($event); }, 5);
	}

	/**
	 * @param integer $websiteId
	 * @param integer $documentId
	 * @return array
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function getSocialCount($websiteId, $documentId)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'websiteId' => $websiteId,
			'documentId' => $documentId
		]);
		$eventManager->trigger('getSocialCount', $this, $args);
		return isset($args['data']) ? $args['data'] : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \InvalidArgumentException
	 * @throws \LogicException
	 * @throws \RuntimeException
	 */
	public function onDefaultGetSocialCount($event)
	{
		$websiteId = $event->getParam('websiteId');
		$documentId = $event->getParam('documentId');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$website = $documentManager->getDocumentInstance($websiteId);
		$document = $event->getParam($documentId);

		if ($document instanceof \Change\Documents\AbstractDocument && $website instanceof \Change\Presentation\Interfaces\Website)
		{
			$socialData = $this->getSocialData($websiteId, $documentId, $event->getApplicationServices()->getDbProvider());
			$socialData['data'] = json_decode($socialData['data'], true);
			$event->setParam('data', $socialData);
		}
	}

	/**
	 * @param integer $websiteId
	 * @param integer $documentId
	 * @param string $socialType
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function addSocialCount($websiteId, $documentId, $socialType)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'websiteId' => $websiteId,
			'documentId' => $documentId,
			'socialType' => $socialType
		]);
		$eventManager->trigger('addSocialCount', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	public function onDefaultAddSocialCount($event)
	{
		$websiteId = $event->getParam('websiteId');
		$documentId = $event->getParam('documentId');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$website = $documentManager->getDocumentInstance($websiteId);
		$document = $documentManager->getDocumentInstance($documentId);
		$socialType = $event->getParam('socialType');

		if ($website instanceof \Change\Presentation\Interfaces\Website && $document instanceof \Change\Documents\AbstractDocument
			&& is_string($socialType)
		)
		{
			$dbProvider = $event->getApplicationServices()->getDbProvider();
			$socialData = $this->getSocialData($websiteId, $documentId, $dbProvider);

			if ($socialData === null)
			{
				$this->insertSocialData($websiteId, $documentId, $this->getNewSocialCounts($socialType), $event);
			}
			else
			{
				$socialCounts = json_decode($socialData['data'], true);
				$newSocialCounts = $this->getNewSocialCounts($socialType, $socialCounts);
				$count = array_sum($newSocialCounts);
				$this->updateSocialData($websiteId, $documentId, $newSocialCounts, $count, $event);
			}
		}
	}

	/**
	 * @param integer $websiteId
	 * @param integer $documentId
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return array
	 * @throws \LogicException
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	protected function getSocialData($websiteId, $documentId, $dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('count'), $fb->column('last_date'), $fb->column('data'));
		$qb->from($fb->table('rbs_social_count'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('website_id'), $fb->integerParameter('websiteId')),
			$fb->eq($fb->column('document_id'), $fb->integerParameter('documentId'))
		));
		$sq = $qb->query();
		$sq->bindParameter('websiteId', $websiteId);
		$sq->bindParameter('documentId', $documentId);

		return $sq->getFirstResult($sq->getRowsConverter()->addIntCol('count')->addDtCol('last_date')->addTxtCol('data'));
	}

	/**
	 * @param integer $documentId
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array
	 * @throws \LogicException
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	public function getFormattedSocialData($documentId, $dbProvider, $documentManager)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('website_id'), $fb->column('count'), $fb->column('last_date'), $fb->column('data'));
		$qb->from($fb->table('rbs_social_count'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('document_id'), $fb->integerParameter('documentId'))
		));
		$sq = $qb->query();
		$sq->bindParameter('documentId', $documentId);
		$results = $sq->getResults($sq->getRowsConverter()
			->addIntCol('website_id')->addIntCol('count')->addDtCol('last_date')->addTxtCol('data')
		);

		return $this->formatSocialData($results, $documentManager);
	}

	/**
	 * @param array $socialData
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array
	 */
	protected function formatSocialData($socialData, $documentManager)
	{
		$formattedSocialData = [];

		foreach ($socialData as $social)
		{
			/** @var \Rbs\Website\Documents\Website $website */
			$website = $documentManager->getDocumentInstance($social['website_id'], 'Rbs_Website_Website');
			$totalCount = $social['count'];
			$data = json_decode($social['data'], true);
			$formattedData = [];
			foreach ($data as $socialType => $count)
			{
				$percent = ($count / $totalCount) * 100;
				$formattedData[] = ['name' => $this->getFullSocialType($socialType), 'count' => $count, 'percent' => $percent];
			}
			$formattedSocialData[] = [
				'website' => $website ? $website->getLabel() : '-',
				'count' => $totalCount,
				'lastDate' => $social['last_date'],
				'data' => $formattedData
			];
		}

		return $formattedSocialData;
	}

	/**
	 * @param integer $websiteId
	 * @param integer $documentId
	 * @param array $socialData
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function insertSocialData($websiteId, $documentId, $socialData, $event)
	{
		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();
			$dbProvider = $event->getApplicationServices()->getDbProvider();
			$qb = $dbProvider->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->table('rbs_social_count'));
			$qb->addColumns($fb->column('website_id'), $fb->column('document_id'), $fb->column('count'), $fb->column('last_date'),
				$fb->column('data'));
			$qb->addValues($fb->parameter('websiteId'), $fb->parameter('documentId'), $fb->parameter('count'), $fb->dateTimeParameter('lastDate'),
				$fb->parameter('data'));
			$iq = $qb->insertQuery();
			$iq->bindParameter('websiteId', $websiteId);
			$iq->bindParameter('documentId', $documentId);
			$iq->bindParameter('count', 1);
			$iq->bindParameter('data', json_encode($socialData));
			$iq->bindParameter('lastDate', new \DateTime());
			$iq->execute();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}

	/**
	 * @param integer $websiteId
	 * @param integer $documentId
	 * @param array $socialData
	 * @param integer $count
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function updateSocialData($websiteId, $documentId, $socialData, $count, $event)
	{
		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();
			$dbProvider = $event->getApplicationServices()->getDbProvider();
			$qb = $dbProvider->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->table('rbs_social_count'));
			$qb->assign($fb->column('count'), $fb->integerParameter('count'))
				->assign($fb->column('data'), $fb->parameter('data'))
				->assign($fb->column('last_date'), $fb->dateTimeParameter('lastDate'));
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('website_id'), $fb->integerParameter('websiteId')),
				$fb->eq($fb->column('document_id'), $fb->integerParameter('documentId'))
			));
			$uq = $qb->updateQuery();
			$uq->bindParameter('websiteId', $websiteId);
			$uq->bindParameter('documentId', $documentId);
			$uq->bindParameter('count', $count);
			$uq->bindParameter('data', json_encode($socialData));
			$uq->bindParameter('lastDate', new \DateTime());
			$uq->execute();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}

	/**
	 * @param string $socialType
	 * @param array $socialCounts
	 * @return array
	 */
	protected function getNewSocialCounts($socialType,
		$socialCounts = ['facebook' => 0, 'twitter' => 0, 'gplus' => 0, 'pinterest' => 0, 'instagram' => 0])
	{
		switch ($socialType)
		{
			case 'facebookLike':
				$socialCounts['facebook']++;
				break;
			case 'facebookUnlike':
				$socialCounts['facebook']--;
				break;
			case 'twitterTweet':
				$socialCounts['twitter']++;
				break;
			case 'gplusOn':
				$socialCounts['gplus']++;
				break;
			case 'gplusOff':
				$socialCounts['gplus']--;
				break;
			case 'pinterestPinIt':
				$socialCounts['pinterest']++;
				break;
		}

		return $socialCounts;
	}

	/**
	 * @param string $shortSocialType
	 * @return string
	 */
	protected function getFullSocialType($shortSocialType)
	{
		switch ($shortSocialType)
		{
			case 'facebook':
				return 'Facebook';
			case 'twitter':
				return 'Twitter';
			case 'gplus':
				return 'Google +';
			case 'pinterest':
				return 'Pinterest';
			case 'instagram':
				return 'Instagram';
			default:
				return 'error: unknown short social type: ' . $shortSocialType;
		}
	}

	/**
	 * @param integer $userId
	 * @param string $socialUserId
	 * @param string $provider
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 * @throws \RuntimeException
	 */
	public function addSocialLink($userId, $socialUserId, $provider)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'userId' => $userId,
			'socialUserId' => $socialUserId,
			'provider' => $provider
		]);
		$eventManager->trigger('addSocialLink', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 * @throws \LogicException
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	protected function onDefaultAddSocialLink(\Change\Events\Event $event)
	{
		$userId = $event->getParam('userId');
		$socialUserId = $event->getParam('socialUserId');
		$provider = $event->getParam('provider');
		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$link = $this->getSocialLink($userId, $socialUserId, $provider, $dbProvider);
		if ($link === null)
		{
			$tm = $event->getApplicationServices()->getTransactionManager();
			try
			{
				$tm->begin();
				$dbProvider = $event->getApplicationServices()->getDbProvider();
				$qb = $dbProvider->getNewStatementBuilder();
				$fb = $qb->getFragmentBuilder();
				$qb->insert($fb->table('rbs_social_link'));
				$qb->addColumns($fb->column('user_id'), $fb->column('social_user_id'), $fb->column('provider'), $fb->column('created_date'));
				$qb->addValues($fb->parameter('userId'), $fb->parameter('socialUserId'), $fb->parameter('provider'),
					$fb->dateTimeParameter('createdDate'));
				$iq = $qb->insertQuery();
				$iq->bindParameter('userId', $userId);
				$iq->bindParameter('socialUserId', $socialUserId);
				$iq->bindParameter('provider', $provider);
				$iq->bindParameter('createdDate', new \DateTime());
				$iq->execute();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}

	/**
	 * @param integer $userId
	 * @param string $provider
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function deleteSocialLink($userId, $provider)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'userId' => $userId,
			'provider' => $provider
		]);
		$eventManager->trigger('deleteSocialLink', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onDefaultDeleteSocialLink(\Change\Events\Event $event)
	{
		$userId = $event->getParam('userId');
		$provider = $event->getParam('provider');
		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();
			$dbProvider = $event->getApplicationServices()->getDbProvider();
			$qb = $dbProvider->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->table('rbs_social_link'));
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('user_id'), $fb->integerParameter('userId')),
				$fb->eq($fb->column('provider'), $fb->typedParameter('provider', \Change\Db\ScalarType::STRING))
			));
			$iq = $qb->deleteQuery();
			$iq->bindParameter('userId', $userId);
			$iq->bindParameter('provider', $provider);
			$iq->execute();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}

	/**
	 * @param integer $userId
	 * @param string $socialUserId
	 * @param string $provider
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return mixed
	 * @throws \LogicException
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	protected function getSocialLink($userId, $socialUserId, $provider, $dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('user_id'), $fb->column('social_user_id'), $fb->column('provider'));
		$qb->from($fb->table('rbs_social_link'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('user_id'), $fb->integerParameter('userId')),
			$fb->eq($fb->column('social_user_id'), $fb->typedParameter('socialUserId', \Change\Db\ScalarType::STRING)),
			$fb->eq($fb->column('provider'), $fb->typedParameter('provider', \Change\Db\ScalarType::STRING))
		));
		$sq = $qb->query();
		$sq->bindParameter('userId', $userId);
		$sq->bindParameter('socialUserId', $socialUserId);
		$sq->bindParameter('provider', $provider);

		return $sq->getFirstResult($sq->getRowsConverter()->addIntCol('user_id')->addTxtCol('social_user_id')->addTxtCol('provider'));
	}

	/**
	 * @param integer $userId
	 * @return array|mixed
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 * @throws \RuntimeException
	 */
	public function getSocialLinks($userId)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'userId' => $userId
		]);
		$eventManager->trigger('getSocialLinks', $this, $args);
		return isset($args['data']) ? $args['data'] : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \LogicException
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	protected function onDefaultGetSocialLinks(\Change\Events\Event $event)
	{
		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('user_id'), $fb->column('social_user_id'), $fb->column('provider'));
		$qb->from($fb->table('rbs_social_link'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('user_id'), $fb->integerParameter('userId'))
		));
		$sq = $qb->query();

		$sq->bindParameter('userId', $event->getParam('userId'));
		$result = $sq->getResults($sq->getRowsConverter()->addIntCol('user_id')->addTxtCol('social_user_id')->addTxtCol('provider'));
		$event->setParam('data', $result);
	}

	/**
	 * @param integer $socialUserId
	 * @param string $provider
	 * @return array|mixed
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 * @throws \RuntimeException
	 */
	public function getSocialLinkWithSocialId($socialUserId, $provider)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'socialUserId' => $socialUserId,
			'provider' => $provider
		]);
		$eventManager->trigger('getSocialLinkWithSocialId', $this, $args);
		return isset($args['data']) ? $args['data'] : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 * @throws \LogicException
	 */
	protected function onDefaultGetSocialLinkWithSocialId(\Change\Events\Event $event)
	{
		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('user_id'), $fb->column('social_user_id'), $fb->column('provider'));
		$qb->from($fb->table('rbs_social_link'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('social_user_id'), $fb->parameter('socialUserId')),
			$fb->eq($fb->column('provider'), $fb->parameter('provider'))
		));
		$sq = $qb->query();
		$sq->bindParameter('socialUserId', $event->getParam('socialUserId'));
		$sq->bindParameter('provider', $event->getParam('provider'));
		$result = $sq->getFirstResult($sq->getRowsConverter()->addIntCol('user_id')->addTxtCol('social_user_id')->addTxtCol('provider'));
		$event->setParam('data', $result);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onCreateAccountRequestFromCart(\Change\Events\Event $event)
	{
		$cart = $event->getParam('cart');
		if ($cart instanceof \Rbs\Commerce\Cart\Cart)
		{
			$options = $cart->getCustomer()->getOptions();
			if ($options->get('accountRequest') && $options->get('socialId'))
			{
				$data = [
					'socialId' => $options->get('socialId'),
					'provider' => $options->get('provider'),
					'password' => $options->get('password'),
					'profiles' => $options->get('profiles'),
					'userProfile' => $options->get('userSocialProfile')
				];

				$event->setParam('dataRequest', $data);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 * @throws \LogicException
	 */
	public function onDefaultConfirmAccountRequest(\Change\Events\Event $event)
	{
		$requestParameters = $event->getParam('requestParameters');
		if (!isset($requestParameters['socialId']) || !$requestParameters['socialId'])
		{
			return;
		}

		$documentUser = $event->getParam('user');
		if (!$documentUser instanceof \Rbs\User\Documents\User)
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();

		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			$dbProvider = $applicationServices->getDbProvider();

			$userId = $documentUser->getId();
			$metaData = ['userProfile' => $requestParameters['userProfile']];
			$documentUser->setMeta('Rbs_Social', $metaData);
			$documentUser->saveMetas();
			$socialUserId = $requestParameters['socialId'];
			$provider = $requestParameters['provider'];
			$link = $this->getSocialLink($userId, $socialUserId, $provider, $dbProvider);
			if ($link === null)
			{
				$qb = $dbProvider->getNewStatementBuilder();
				$fb = $qb->getFragmentBuilder();
				$qb->insert($fb->table('rbs_social_link'));
				$qb->addColumns($fb->column('user_id'), $fb->column('social_user_id'), $fb->column('provider'), $fb->column('created_date'));
				$qb->addValues($fb->parameter('userId'), $fb->parameter('socialUserId'), $fb->parameter('provider'),
					$fb->dateTimeParameter('createdDate'));
				$iq = $qb->insertQuery();
				$iq->bindParameter('userId', $userId);
				$iq->bindParameter('socialUserId', $socialUserId);
				$iq->bindParameter('provider', $provider);
				$iq->bindParameter('createdDate', new \DateTime());
				$iq->execute();
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}
}