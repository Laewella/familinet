<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Documents;

/**
 * @name \Rbs\Social\Documents\SocialConfiguration
 */
class SocialConfiguration extends \Compilation\Rbs\Social\Documents\SocialConfiguration
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 * @throws \RuntimeException
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getAJAXData', function ($event) { $this->onGetAJAXData($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		if ($this !== $event->getDocument())
		{
			return;
		}

		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$documentResult = $restResult;
			$socialApiURL = $event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL');
			$callbackUri =
				$socialApiURL . 'callback.php?auth_done=' . $this->getProvider() . '&website=' . md5($this->getWebsiteDomain() . '2016');
			$documentResult->setProperty('callbackuri', $callbackUri);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @throws \RuntimeException
	 */
	protected function onGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Social\SocialConfigurationDataComposer($event))->toArray());
	}
}
