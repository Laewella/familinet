<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social;

/**
 * @name \Rbs\Social\SocialConfigurationDataComposer
 */
class SocialConfigurationDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration
	 */
	protected $socialConfiguration;

	public function __construct(\Change\Events\Event $event)
	{
		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());

		if ($event instanceof \Change\Documents\Events\Event)
		{
			$socialConf = $event->getDocument();
		}
		else
		{
			$socialConf = $event->getParam('socialConfiguration');
		}

		if (is_numeric($socialConf))
		{
			$socialConf = $this->documentManager->getDocumentInstance($socialConf);
		}

		if ($socialConf instanceof \Rbs\Social\Documents\SocialConfiguration)
		{
			$this->socialConfiguration = $socialConf;
		}
	}

	protected function generateDataSets()
	{
		if (!$this->socialConfiguration)
		{
			$this->dataSets = [];
			return;
		}

		$this->dataSets = [
			'common' => [
				'id' => $this->socialConfiguration->getId(),
				'provider' => $this->socialConfiguration->getProvider(),
				'publicKey' => $this->socialConfiguration->getPublicKey(),
				'privateKey' => $this->socialConfiguration->getSecretKey(),
				'websiteDomain' => $this->socialConfiguration->getWebsiteDomain(),
				'scope' => $this->socialConfiguration->getProviderScope()
			],
		];

		$this->generateTypologyDataSet($this->socialConfiguration);
	}
}