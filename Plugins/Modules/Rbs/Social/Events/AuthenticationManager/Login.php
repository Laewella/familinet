<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Events\AuthenticationManager;

/**
 * @name \Rbs\Social\Events\AuthenticationManager\Login
 */
class Login
{
	/**
	 * @param \Change\Events\Event $event
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function onLogin(\Change\Events\Event $event)
	{
		if ($event->getParam('userId'))
		{
			return;
		}

		$realm = $event->getParam('realm');
		$password = $event->getParam('password');
		$options = $event->getParam('options');

		$login = $event->getParam('login');
		if ($login !== 'RBS_SOCIAL' || !$realm || !$password || !isset($options['httpEvent']))
		{
			return;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$socialLink = $genericServices->getSocialManager()->getSocialLinkWithSocialId($password, $realm);
		if ($socialLink)
		{
			$event->setParam('userId', $socialLink['user_id']);
		}
	}
}