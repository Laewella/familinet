/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	var app = angular.module('RbsChangeApp');

	function initSocialWindow() {
		var
			screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
			screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
			outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
			outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight),
			width = 500,
			height = 430,
			left = parseInt(screenX + ((outerWidth - width) / 2), 10),
			top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
			features = (
				'width=' + width +
				',height=' + height +
				',left=' + left +
				',top=' + top
			);

		return window.open('', 'SocialWindow', features);
	}

	app.controller('RbsSocialCreateAccountController', ['$scope', 'RbsChange.AjaxAPI', RbsSocialCreateAccountController]);
	function RbsSocialCreateAccountController(scope, AjaxAPI) {
		scope.url = "";
		scope.providerSelected = null;

		scope.socialCreateAccount = function(provider) {
			scope.providerSelected = provider;
			var SocialWindow = initSocialWindow();
			var data = {
				provider: provider,
				back: window.location.href,
				display: SocialWindow ? 'popup' : 'page'
			};

			AjaxAPI.postData('Rbs/Social/SocialConfiguration/CreateAccount', data).then(
				function(result) {
					if (SocialWindow) {
						SocialWindow.location = result.data.dataSets.providerUrlPopin;
					}
					else {
						window.location.href = result.data.dataSets.providerUrl;
					}
				},
				function(result) {
					console.log(result);
				}
			);
		}
	}

	app.controller('RbsSocialAuthController', ['$scope', 'RbsChange.AjaxAPI', RbsSocialAuthController]);
	function RbsSocialAuthController(scope, AjaxAPI) {
		scope.url = "";
		scope.providerSelected = null;

		scope.link = function(provider) {

			var SocialWindow = initSocialWindow();

			scope.providerSelected = provider;
			var data = {
				provider: provider,
				back: window.location.href,
				rememberMe: scope.data.rememberMe || false,
				ignoreProfileCart: false,
				display: SocialWindow ? 'popup' : 'page'
			};

			AjaxAPI.postData('Rbs/Social/SocialConfiguration/Authenticate', data).then(
				function(result) {
					if (SocialWindow) {
						SocialWindow.location = result.data.dataSets.providerUrlPopin;
					}
					else {
						window.location.href = result.data.dataSets.providerUrl;
					}
				},
				function(result) {

				}
			);
		}
	}

	app.controller('RbsSocialLinkAccountController', ['$scope', 'RbsChange.AjaxAPI', RbsSocialLinkAccountController]);
	function RbsSocialLinkAccountController(scope, AjaxAPI) {
		scope.url = "";
		scope.providerSelected = null;
		scope.linkedProviders = [];

		function loadLinkedProviders() {
			AjaxAPI.getData('Rbs/Social/SocialConfiguration/LinkedProviders', {}).then(
				function(result) {
					scope.linkedProviders = result.data.dataSets
				},
				function(result) {
					scope.linkedProviders = [];
					console.log('loadLinkedProviders error', result);
				}
			);
		}

		scope.link = function(provider) {
			scope.providerIdSelected = provider;
			var SocialWindow = initSocialWindow();
			AjaxAPI.postData('Rbs/Social/SocialConfiguration/LinkAccount', {
				provider: provider, back: window.location.href,
				display: SocialWindow ? 'popup' : 'page'
			}).then(
				function(result) {
					if (SocialWindow) {
						SocialWindow.location = result.data.dataSets.providerUrlPopin;
					}
					else {
						window.location.href = result.data.dataSets.providerUrl;
					}
				}
			);
		};

		scope.unlink = function($event, provider) {
			scope.providerIdSelected = provider;
			AjaxAPI.postData('Rbs/Social/SocialConfiguration/UnlinkAccount', { provider: provider }).then(
				function(result) {
					var index = scope.linkedProviders.indexOf(result.data.dataSets[0]);
					scope.linkedProviders.splice(index, 1);
				}
			);
		};

		loadLinkedProviders();
	}

	app.directive('rbsSocialAuthProcess', ['RbsChange.AjaxAPI', rbsSocialAuthProcess]);
	function rbsSocialAuthProcess(AjaxAPI) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-social-auth-process.twig',
			scope: true,
			link: function(scope) {

				scope.processData = scope.processEngine.getStepProcessData('identification');

				scope.processData.realm = scope.processEngine.parameters('realm');

				AjaxAPI.getData('Rbs/Social/SocialConfiguration').then(
					function(result) {
						scope.providers = [];
						if (result.data.dataSets) {
							scope.providers = result.data.dataSets;
						}
					}
				);

				scope.socialAuth = function(provider) {
					var SocialWindow = initSocialWindow();
					var data = {
						provider: provider,
						back: window.location.href,
						rememberMe: scope.processData.rememberMe || false,
						ignoreProfileCart: true,
						display: SocialWindow ? 'popup' : 'page',
						process: true,
						cartIdentifier: scope.processEngine.getObjectData().common.identifier || null
					};

					AjaxAPI.postData('Rbs/Social/SocialConfiguration/Authenticate', data).then(
						function(result) {
							if (SocialWindow) {
								SocialWindow.location = result.data.dataSets.providerUrlPopin;
							}
							else {
								window.location.href = result.data.dataSets.providerUrl;
							}
						}
					);
				}
			}
		}
	}

	app.controller('RbsSocialPaymentReturnCreateAccountController', ['$scope', 'RbsChange.AjaxAPI', RbsSocialPaymentReturnCreateAccountController]);
	function RbsSocialPaymentReturnCreateAccountController(scope, AjaxAPI) {
		scope.url = "";
		scope.providerSelected = null;

		scope.socialPaymentReturnCreateAccount = function(provider, transactionId, email) {
			scope.providerSelected = provider;
			var SocialWindow = initSocialWindow();
			var data = {
				provider: provider,
				back: window.location.href,
				transactionId: transactionId,
				fixedEmail: email,
				display: SocialWindow ? 'popup' : 'page'
			};

			AjaxAPI.postData('Rbs/Social/SocialConfiguration/CreateAccount', data).then(
				function(result) {
					if (SocialWindow) {
						SocialWindow.location = result.data.dataSets.providerUrlPopin;
					}
					else {
						window.location.href = result.data.dataSets.providerUrl;
					}
				},
				function(result) {
					console.error('socialPaymentReturnCreateAccount', result);
				}
			);
		}
	}

	app.controller('RbsSocialReservationCreateAccountController', ['$scope', 'RbsChange.AjaxAPI', RbsSocialReservationCreateAccountController]);
	function RbsSocialReservationCreateAccountController(scope, AjaxAPI) {
		scope.url = "";
		scope.providerSelected = null;

		scope.socialReservationCreateAccount = function(provider, cartIdentifier, email) {
			scope.providerSelected = provider;
			var SocialWindow = initSocialWindow();
			var data = {
				provider: provider,
				back: window.location.href,
				cartIdentifier: cartIdentifier,
				fixedEmail: email,
				display: SocialWindow ? 'popup' : 'page'
			};

			AjaxAPI.postData('Rbs/Social/SocialConfiguration/CreateAccount', data).then(
				function(result) {
					if (SocialWindow) {
						SocialWindow.location = result.data.dataSets.providerUrlPopin;
					}
					else {
						window.location.href = result.data.dataSets.providerUrl;
					}
				},
				function(result) {
					console.error('socialReservationCreateAccount', result);
				}
			);
		}
	}
})();