(function() {
	"use strict";

	angular.module('RbsChange').directive('rbsDocumentEditorRbsSocialSocialConfigurationNew', EditorNew);

	function EditorNew() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					scope.$watch('document.provider', function() {
						initProviderScope(scope.document);
					});
				};
			}
		};
	}

	angular.module('RbsChange').directive('rbsDocumentEditorRbsSocialSocialConfigurationEdit', EditorEdit);

	function EditorEdit() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.reInit = function() {
					initProviderScope(scope.document);
				}
			}
		};
	}

	function initProviderScope(document) {
		if (document.provider && angular.equals(document.provider.toString(), 'facebook')) {
			document.providerScope = {
				"user_friends": false,
				"user_about_me": false,
				"user_actions.books": false,
				"user_actions.fitness": false,
				"user_actions.music": false,
				"user_actions.news": false,
				"user_actions.video": false,
				"user_birthday": false,
				"user_education_history": false,
				"user_events": false,
				"user_games_activity": false,
				"user_hometown": false,
				"user_likes": false,
				"user_location": false,
				"user_managed_groups": false,
				"user_photos": false,
				"user_posts": false,
				"user_relationships": false,
				"user_relationship_details": false,
				"user_religion_politics": false,
				"user_tagged_places": false,
				"user_videos": false,
				"user_website": false,
				"user_work_history": false,
				"read_custom_friendlists": false,
				"read_insights": false,
				"read_audience_network_insights": false,
				"read_page_mailboxes": false,
				"manage_pages": false,
				"publish_pages": false,
				"publish_actions": false,
				"rsvp_event": false,
				"pages_show_list": false,
				"pages_manage_cta": false,
				"pages_manage_instant_articles": false,
				"ads_read": false,
				"ads_management": false,
				"pages_messaging": false,
				"pages_messaging_phone_number": false
			}
		}
		else {
			document.providerScope = {};
		}
	}
})();