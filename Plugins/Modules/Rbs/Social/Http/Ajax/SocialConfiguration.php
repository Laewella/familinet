<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Http\Ajax;

/**
 * @name \Rbs\Social\Http\Ajax\SocialConfiguration
 */
class SocialConfiguration
{
	const AUTHENTICATION = 'authentication';
	const AUTHENTICATION_PROCESS = 'authentication_process';
	const CREATE_ACCOUNT = 'creation';
	const LINK_ACCOUNT = 'linkaccount';
	const LOGIN = 'login';
	const GET_USER_INFOS = 'get_user_infos';

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getSocialConfiguration(\Change\Http\Event $event)
	{
		/** @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration */
		$socialConfiguration = $event->getApplicationServices()->getDocumentManager()
			->getDocumentInstance($event->getParam('provider'), 'Rbs_Social_SocialConfiguration');
		if (!$socialConfiguration)
		{
			return;
		}

		$socialConfigurationData = $socialConfiguration->getAJAXData($event->paramsToArray());
		if ($socialConfigurationData);
		{
			$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/Social/SocialConfiguration', $socialConfigurationData);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function getSocialConfigurationList(\Change\Http\Event $event)
	{
		if (!$event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL'))
		{
			$event->setResult(new \Change\Http\Ajax\V1\ItemResult());
			return;
		}
		/** @var \Change\Documents\DocumentCollection $socialConfigurationCollection */
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Social_SocialConfiguration');
		$query->andPredicates($query->activated());

		/** @var \Rbs\Social\Documents\SocialConfiguration[] $socialConfigurationCollection */
		$socialConfigurationCollection = $query->getDocuments();

		$socialConfigurationList = [];

		foreach ($socialConfigurationCollection as $socialConfiguration)
		{
			$socialConfigurationList[] = [
				'id' => $socialConfiguration->getId(),
				'provider' => $socialConfiguration->getProvider()
			];
		}

		$result = new \Change\Http\Ajax\V1\ItemResult('providers', $socialConfigurationList);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function linkedProviders(\Change\Http\Event $event)
	{
		$providers = [];

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$socialManager = $genericServices->getSocialManager();
		$authenticationManager = $event->getApplicationServices()->getAuthenticationManager();
		$proximisUserId = $authenticationManager->getCurrentUser()->getId();
		$linkedAccounts = $socialManager->getSocialLinks($proximisUserId);

		foreach ($linkedAccounts as $link)
		{
			$providers[] = $link['provider'];
		}

		$result = new \Change\Http\Ajax\V1\ItemResult('providers', $providers);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Zend\Session\Exception\InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function linkAccount(\Change\Http\Event $event)
	{
		$data = $event->getParam('data');
		$provider = $data['provider'];
		$back = $data['back'];
		$session = $event->getApplication()->getSessionContainer('Authentication');
		if ($session)
		{
			$session['back'] = $back;
			$session['processType'] = SocialConfiguration::LINK_ACCOUNT;
		}

		/** @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration */
		$socialConfiguration = $event->getApplicationServices()->getDocumentManager()
			->getDocumentInstance($provider, 'Rbs_Social_SocialConfiguration');
		if (!$socialConfiguration)
		{
			return;
		}

		$context = $event->paramsToArray();
		$socialConfigurationData = $socialConfiguration->getAJAXData($context);
		if (!$socialConfigurationData)
		{
			return;
		}
		$scope = [];

		foreach ($socialConfigurationData['common']['scope'] as $key => $val)
		{
			if ($val)
			{
				$scope[] = $key;
			}
		}

		/** @var \Rbs\Website\Documents\Website $website */
		$website = $event->getParam('website');
		$actionUrl = $website->getUrlManager($website->getCurrentLCID())
			->getActionURL('Rbs_Social', 'AuthSocialAccount',
				['provider' => '{provider}', 'userId' => '{userId}', 'accessToken' => '{accessToken}', 'display' => $data['display'] ?? 'page']);
		$loginUrl = $event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL') . SocialConfiguration::LOGIN;

		$params = [
			'provider' => $socialConfigurationData['common']['provider'],
			'public_key' => $socialConfigurationData['common']['publicKey'],
			'private_key' => $socialConfigurationData['common']['privateKey'],
			'domain_website' => $socialConfigurationData['common']['websiteDomain'],
			'scope' => implode(',', $scope),
			'back_action' => $actionUrl
		];

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Proximis Social Plugin');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'X-HTTP-Method-Override: GET']);

		$response = json_decode(curl_exec($ch));

		curl_close($ch);

		$result = new \Change\Http\Ajax\V1\ItemResult('providerUrl',
			['providerUrl' => urldecode($response->providerUrl),
				'providerUrlPopin' => urldecode($response->providerUrlPopin)]
		);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function unlinkAccount(\Change\Http\Event $event)
	{
		$data = $event->getParam('data');
		$providerId = $data['provider'];

		/** @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration */
		$socialConfiguration = $event->getApplicationServices()->getDocumentManager()
			->getDocumentInstance($providerId, 'Rbs_Social_SocialConfiguration');

		if (!$socialConfiguration)
		{
			return;
		}

		$provider = $socialConfiguration->getProvider();
		$authenticationManager = $event->getAuthenticationManager();
		$proximisUserId = $authenticationManager->getCurrentUser()->getId();

		$genericServices = $event->getServices('genericServices');
		if ($genericServices instanceof \Rbs\Generic\GenericServices)
		{
			$socialManager = $genericServices->getSocialManager();
			$socialManager->deleteSocialLink($proximisUserId, $provider);
		}
		$result = new \Change\Http\Ajax\V1\ItemResult('provider', [$provider]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Zend\Session\Exception\InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function authenticate(\Change\Http\Event $event)
	{
		$data = $event->getParam('data');

		if (!isset($data['provider']))
		{
			return;
		}

		$provider = $data['provider'];
		$back = isset($data['back']) ? $data['back'] : '';
		$rememberMe = isset($data['rememberMe']) ? $data['rememberMe'] : false;
		$ignoreProfileCart = isset($data['ignoreProfileCart']) ? $data['ignoreProfileCart'] : false;

		$session = $event->getApplication()->getSessionContainer('Authentication');
		if ($session)
		{
			$session['processType'] = isset($data['process']) ? SocialConfiguration::AUTHENTICATION_PROCESS : SocialConfiguration::AUTHENTICATION;
			$session['back'] = $back;
			$session['rememberMe'] = $rememberMe;
			$session['ignoreProfileCart'] = $ignoreProfileCart;
			$session['cartIdentifier'] = isset($data['cartIdentifier']) ? $data['cartIdentifier'] : null;
		}

		/** @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration */
		$socialConfiguration = $event->getApplicationServices()->getDocumentManager()
			->getDocumentInstance($provider, 'Rbs_Social_SocialConfiguration');
		if (!$socialConfiguration)
		{
			return;
		}

		$context = $event->paramsToArray();
		$socialConfigurationData = $socialConfiguration->getAJAXData($context);
		if (!$socialConfigurationData)
		{
			return;
		}

		$scope = [];
		foreach ($socialConfigurationData['common']['scope'] as $key => $val)
		{
			if ($val)
			{
				$scope[] = $key;
			}
		}

		/** @var \Rbs\Website\Documents\Website $website */
		$website = $event->getParam('website');
		$actionUrl = $website->getUrlManager($website->getCurrentLCID())
			->getActionURL('Rbs_Social', 'AuthSocialAccount',
				['provider' => '{provider}', 'userId' => '{userId}', 'accessToken' => '{accessToken}', 'display' => $data['display'] ?? 'page']);
		$loginUrl = $event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL') . SocialConfiguration::LOGIN;

		$params = [
			'provider' => $socialConfigurationData['common']['provider'],
			'public_key' => $socialConfigurationData['common']['publicKey'],
			'private_key' => $socialConfigurationData['common']['privateKey'],
			'domain_website' => $socialConfigurationData['common']['websiteDomain'],
			'scope' => implode(',', $scope),
			'back_action' => $actionUrl
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Proximis Social Plugin');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'X-HTTP-Method-Override: GET']);
		$response = json_decode(curl_exec($ch));

		curl_close($ch);

		$result = new \Change\Http\Ajax\V1\ItemResult('providerUrl',
			['providerUrl' => urldecode($response->providerUrl),
				'providerUrlPopin' => urldecode($response->providerUrlPopin)]
		);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Zend\Session\Exception\InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function createAccount(\Change\Http\Event $event)
	{
		$data = $event->getParam('data');

		if (!isset($data['provider']))
		{
			return;
		}

		$provider = $data['provider'];
		$back = isset($data['back']) ? $data['back'] : '';

		$session = $event->getApplication()->getSessionContainer('Authentication');
		if ($session)
		{
			$session['processType'] = SocialConfiguration::CREATE_ACCOUNT;
			$session['back'] = $back;
			$session['transactionId'] = isset($data['transactionId']) ? $data['transactionId'] : '';
			$session['cartIdentifier'] = isset($data['cartIdentifier']) ? $data['cartIdentifier'] : '';
			$session['fixedEmail'] = isset($data['fixedEmail']) ? $data['fixedEmail'] : '';
		}

		/** @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration */
		$socialConfiguration = $event->getApplicationServices()->getDocumentManager()
			->getDocumentInstance($provider, 'Rbs_Social_SocialConfiguration');
		if (!$socialConfiguration)
		{
			return;
		}

		$context = $event->paramsToArray();
		$socialConfigurationData = $socialConfiguration->getAJAXData($context);
		if (!$socialConfigurationData)
		{
			return;
		}

		$scope = [];
		foreach ($socialConfigurationData['common']['scope'] as $key => $val)
		{
			if ($val)
			{
				$scope[] = $key;
			}
		}

		/** @var \Rbs\Website\Documents\Website $website */
		$website = $event->getParam('website');
		$actionUrl = $website->getUrlManager($website->getCurrentLCID())
			->getActionURL('Rbs_Social', 'AuthSocialAccount',
				['provider' => '{provider}', 'userId' => '{userId}', 'accessToken' => '{accessToken}', 'display' => $data['display'] ?? 'page']);

		$loginUrl = $event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL') . SocialConfiguration::LOGIN;

		$params = [
			'provider' => $socialConfigurationData['common']['provider'],
			'public_key' => $socialConfigurationData['common']['publicKey'],
			'private_key' => $socialConfigurationData['common']['privateKey'],
			'domain_website' => $socialConfigurationData['common']['websiteDomain'],
			'scope' => implode(',', $scope),
			'back_action' => $actionUrl
		];

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Proximis Social Plugin');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'X-HTTP-Method-Override: GET']);

		$response = json_decode(curl_exec($ch));

		curl_close($ch);

		$result = new \Change\Http\Ajax\V1\ItemResult('providerUrl',
			['providerUrl' => urldecode($response->providerUrl), 'providerUrlPopin' => urldecode($response->providerUrlPopin)]
		);

		$event->setResult($result);
	}
}