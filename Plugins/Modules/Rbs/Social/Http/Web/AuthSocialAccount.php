<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Http\Web;

/**
 * @name \Rbs\Social\Http\Web\AuthSocialAccount
 */
class AuthSocialAccount extends \Change\Http\Web\Actions\AbstractAjaxAction
{
	/**
	 * @param \Change\Http\Web\Event $event
	 * @throws \Zend\Session\Exception\InvalidArgumentException
	 * @throws \Zend\Uri\Exception\InvalidArgumentException
	 * @throws \Zend\Uri\Exception\InvalidUriException
	 * @throws \RuntimeException
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 * @throws \LogicException
	 */
	public function execute(\Change\Http\Web\Event $event)
	{
		if ($event->getRequest()->getMethod() === 'GET')
		{
			$session = $event->getApplication()->getSessionContainer('Authentication');
			if ($session)
			{
				$processType = $session['processType'];
				switch ($processType)
				{
					case \Rbs\Social\Http\Ajax\SocialConfiguration::CREATE_ACCOUNT:
						$this->createAccount($event, $session);
						break;
					case \Rbs\Social\Http\Ajax\SocialConfiguration::LINK_ACCOUNT:
						$this->linkAccount($event, $session);
						break;
					case \Rbs\Social\Http\Ajax\SocialConfiguration::AUTHENTICATION:
						$this->authenticate($event, $session);
						break;
					case \Rbs\Social\Http\Ajax\SocialConfiguration::AUTHENTICATION_PROCESS:
						$this->authenticateProcess($event, $session);
						break;
				}
			}
		}
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param \Zend\Session\Container $session
	 * @throws \Change\Transaction\RollbackException
	 * @throws \LogicException
	 * @throws \Zend\Uri\Exception\InvalidArgumentException
	 * @throws \Zend\Uri\Exception\InvalidUriException
	 */
	public function createAccount(\Change\Http\Web\Event $event, \Zend\Session\Container $session)
	{
		$data = $event->getRequest()->getQuery()->toArray();
		$back = $session['back'];
		$socialUserId = $data['userId'];
		$website = $event->getParam('website');

		$provider = $data['provider'];

		/* @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');

		if (!isset($data['userId']) || empty($data['userId']))
		{
			$this->setErrorResult($event, 'CreateAccountRequestError', $genericServices->getUserManager()->getLastError(),
				\Zend\Http\Response::STATUS_CODE_409);
			$this->finalize($event, $back);
			return;
		}

		$userInfo = $this->getSocialUserData($event);
		if (isset($session['fixedEmail']) && $session['fixedEmail'])
		{
			$userInfo['email'] = $session['fixedEmail'];
		}

		$redirect = new \Zend\Uri\Http($back);
		$query = $redirect->getQueryAsArray();

		if (!isset($userInfo['email']) || empty($userInfo['email']))
		{
			$query['Rbs_Social_requestEmail'] = true;
			$redirect->setQuery($query);
			$back = $redirect->normalize()->toString();
			$this->setErrorResult($event, 'CreateAccountRequestError', $genericServices->getUserManager()->getLastError(),
				\Zend\Http\Response::STATUS_CODE_409);
			$this->finalize($event, $back);
			return;
		}

		$email = $userInfo['email'];

		$profiles['Rbs_User']['firstName'] = $userInfo['first_name'] ?? null;
		$profiles['Rbs_User']['lastName'] = $userInfo['last_name'] ?? null;
		$profiles['Rbs_User']['birthDate'] = $userInfo['birthday'] ?? null;

		$data = [
			'email' => $email,
			'socialId' => $socialUserId,
			'provider' => $provider,
			'profiles' => $profiles,
			'password' => time(),
			'websiteId' => $website->getId(),
			'LCID' => $website->getCurrentLCID(),
			'userProfile' => $userInfo,
		];

		if (isset($session['transactionId']) && $session['transactionId'])
		{
			$data['transactionId'] = $session['transactionId'];
		}
		if (isset($session['cartIdentifier']) && $session['cartIdentifier'])
		{
			$data['cartIdentifier'] = $session['cartIdentifier'];
		}

		$result = $genericServices->getUserManager()->createAccountRequest($email, $data);
		if ($result)
		{
			$query['Rbs_Social_requestAccountCreated'] = true;
			$query['email'] = $email;
			$redirect->setQuery($query);
			$back = $redirect->normalize()->toString();

			$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/AccountRequest', ['common' => ['email' => $email]]);
			$event->setResult($result);
		}
		else
		{
			$query['Rbs_Social_accountAlreadyExists'] = true;
			$query['email'] = $email;
			$redirect->setQuery($query);
			$back = $redirect->normalize()->toString();
			$this->setErrorResult($event, 'CreateAccountRequestError', $genericServices->getUserManager()->getLastError(),
				\Zend\Http\Response::STATUS_CODE_409);
		}

		$this->finalize($event, $back);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param \Zend\Session\Container $session
	 * @throws \RuntimeException
	 * @throws \Zend\EventManager\Exception\InvalidCallbackException
	 */
	public function linkAccount(\Change\Http\Web\Event $event, \Zend\Session\Container $session)
	{
		$data = $event->getRequest()->getQuery()->toArray();
		$back = $session['back'];
		$provider = $data['provider'];
		/* @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		if (!isset($data['userId']) || empty($data['userId']))
		{
			$this->setErrorResult($event, 'LinkAccountRequestError', $genericServices->getUserManager()->getLastError(),
				\Zend\Http\Response::STATUS_CODE_409);
		}
		else
		{
			$socialUserId = $data['userId'];
			$authenticationManager = $event->getApplicationServices()->getAuthenticationManager();
			$proximisUserId = $authenticationManager->getCurrentUser()->getId();
			//save linked account
			$genericServices = $event->getServices('genericServices');
			if ($genericServices instanceof \Rbs\Generic\GenericServices)
			{
				$socialManager = $genericServices->getSocialManager();
				$socialManager->addSocialLink($proximisUserId, $socialUserId, $provider);
			}
		}

		$this->finalize($event, $back);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param \Zend\Session\Container $session
	 * @throws \Zend\Uri\Exception\InvalidUriException
	 * @throws \Zend\Uri\Exception\InvalidArgumentException
	 */
	public function authenticate(\Change\Http\Web\Event $event, \Zend\Session\Container $session)
	{
		$data = $event->getRequest()->getQuery()->toArray();
		$provider = $data['provider'];
		$socialUserId = $data['userId'];
		$back = $session['back'];
		$rememberMe = $session['rememberMe'];
		$ignoreProfileCart = $session['ignoreProfileCart'];
		$paramArray = ['rememberMe' => $rememberMe ? '1' : '0', 'ignoreProfileCart' => $ignoreProfileCart ? '1' : '0', 'display' => $data['display']];
		$event->getRequest()->setQuery(new \Zend\Stdlib\Parameters($paramArray));
		$authenticationManager = $event->getApplicationServices()->getAuthenticationManager();
		$user = $authenticationManager->login('RBS_SOCIAL', $socialUserId, $provider, ['httpEvent' => $event]);
		if (!$user || !$user->authenticated())
		{
			$redirect = new \Zend\Uri\Http($back);
			$query = $redirect->getQueryAsArray();
			$query['Rbs_Social_notLinkedAccount'] = $provider;
			$redirect->setQuery($query);
			$back = $redirect->normalize()->toString();
		}
		else
		{
			$authenticationManager->setConfirmed(true);
			$accessorId = $user->getId();
			$website = $event->getParam('website');
			(new \Rbs\User\Http\Ajax\Authentication())->saveToSession($website, $accessorId, true, $event->getApplication());
		}
		$this->finalize($event, $back);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param \Zend\Session\Container $session
	 * @throws \Zend\Uri\Exception\InvalidUriException
	 * @throws \Zend\Uri\Exception\InvalidArgumentException
	 */
	public function authenticateProcess(\Change\Http\Web\Event $event, \Zend\Session\Container $session)
	{
		$data = $event->getRequest()->getQuery()->toArray();
		$provider = $data['provider'];
		$socialUserId = $data['userId'];
		$back = $session['back'];
		$rememberMe = $session['rememberMe'];
		$ignoreProfileCart = $session['ignoreProfileCart'];
		$paramArray = array_merge($data,
			['rememberMe' => $rememberMe ? '1' : '0', 'ignoreProfileCart' => $ignoreProfileCart ? '1' : '0', 'display' => $data['display']]);
		$event->getRequest()->setQuery(new \Zend\Stdlib\Parameters($paramArray));
		$authenticationManager = $event->getApplicationServices()->getAuthenticationManager();
		$user = $authenticationManager->login('RBS_SOCIAL', $socialUserId, $provider, ['httpEvent' => $event]);
		if (!$user || !$user->authenticated())
		{
			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');

			if (!isset($data['userId']) || empty($data['userId']))
			{
				$this->setErrorResult($event, 'CreateAccountRequestError', $genericServices->getUserManager()->getLastError(),
					\Zend\Http\Response::STATUS_CODE_409);
				$this->finalize($event, $back);
				return;
			}

			if (isset($session['cartIdentifier']))
			{
				/* @var $commerceServices \Rbs\Commerce\CommerceServices */
				$commerceServices = $event->getServices('commerceServices');
				$cart = $commerceServices->getCartManager()->getCartByIdentifier($session['cartIdentifier']);
				if ($cart instanceof \Rbs\Commerce\Cart\Cart)
				{
					$tm = $event->getApplicationServices()->getTransactionManager();

					try
					{
						$tm->begin();

						$userInfo = $this->getSocialUserData($event);
						$redirect = new \Zend\Uri\Http($back);
						$query = $redirect->getQueryAsArray();

						if (!isset($userInfo['email']) || empty($userInfo['email']))
						{
							$query['Rbs_Social_requestEmail'] = true;
							$redirect->setQuery($query);
							$back = $redirect->normalize()->toString();
							$this->setErrorResult($event, 'CreateAccountRequestError', $genericServices->getUserManager()->getLastError(),
								\Zend\Http\Response::STATUS_CODE_409);
							$this->finalize($event, $back);
							return;
						}

						$email = $userInfo['email'];

						$cart->getCustomer()->setEmail($email);

						$profiles['Rbs_User']['firstName'] = isset($userInfo['first_name']) ? $userInfo['first_name'] : null;
						$profiles['Rbs_User']['lastName'] = isset($userInfo['last_name']) ? $userInfo['last_name'] : null;
						$profiles['Rbs_User']['birthDate'] = isset($userInfo['birthday']) ? $userInfo['birthday'] : null;

						$cart->getCustomer()->getOptions()->set('profiles', $profiles);

						$cart->getCustomer()->getOptions()->set('socialId', $socialUserId);
						$cart->getCustomer()->getOptions()->set('provider', $provider);
						$cart->getCustomer()->getOptions()->set('userSocialProfile', $userInfo);

						$cart->getCustomer()->getOptions()->set('password', time());
						$cart->getCustomer()->getOptions()->set('accountRequest', true);

						$commerceServices->getCartManager()->saveCart($cart);

						$tm->commit();
					}
					catch (\Exception $e)
					{
						$event->getApplicationServices()->getLogging()->exception($e);
						$tm->rollBack($e);
					}

					echo $cart->getCustomer()->getEmail();
				}
			}

			$redirect = new \Zend\Uri\Http($back);
			$query = $redirect->getQueryAsArray();
			$query['Rbs_Social_notLinkedAccount'] = $provider;
			$redirect->setQuery($query);
			$back = $redirect->normalize()->toString();
		}
		else
		{
			$authenticationManager->setConfirmed(true);
			$accessorId = $user->getId();
			$website = $event->getParam('website');
			(new \Rbs\User\Http\Ajax\Authentication())->saveToSession($website, $accessorId, true, $event->getApplication());
		}
		$this->finalize($event, $back);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string $code
	 * @param string $message
	 * @param integer $httpStatus
	 * @return \Change\Http\Ajax\V1\ErrorResult
	 */
	public function setErrorResult(\Change\Http\Event $event, $code, $message, $httpStatus = \Zend\Http\Response::STATUS_CODE_400)
	{
		$errorResult = new \Change\Http\Ajax\V1\ErrorResult($code, $message, $httpStatus);
		$event->setResult($errorResult);
		return $errorResult;
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param string $back
	 */
	private function finalize($event, $back)
	{
		$data = $event->getRequest()->getQuery()->toArray();

		if ($data['display'] == 'popup')
		{
			$result = new \Change\Http\Web\Result\RawResult(\Zend\Http\Response::STATUS_CODE_200);
			$result->getHeaders()->addHeaderLine('Content-type', 'text/html');

			$result->setContent("<script type=\"text/javascript\">window.opener.location='$back';window.close();</script>");
			$event->setResult($result);
		}
		else
		{
			$event->setParam('redirectLocation', $back);
			$event->setResult($this->getNewAjaxResult());
		}
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @return array
	 */
	private function getSocialUserData($event)
	{
		$data = $event->getRequest()->getQuery()->toArray();
		print_r($data);
		$accessToken = $data['accessToken'];

		$provider = $data['provider'];

		$userInfosUrl =
			$event->getApplication()->getConfiguration()->getEntry('Rbs/Social/ApiURL') . \Rbs\Social\Http\Ajax\SocialConfiguration::GET_USER_INFOS;

		$website = $event->getParam('website');
		$actionUrl = $website->getUrlManager($website->getCurrentLCID())
			->getActionURL('Rbs_Social', 'AuthSocialAccount',
				['provider' => '{provider}', 'userId' => '{userId}', 'accessToken' => '{accessToken}']);

		$applicationServices = $event->getApplicationServices();
		$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Social_SocialConfiguration');
		$query->andPredicates($query->eq('provider', $provider));
		/** @var \Rbs\Social\Documents\SocialConfiguration $socialConfiguration */
		$socialConfiguration = $query->getFirstDocument();
		if (!$socialConfiguration)
		{
			return [];
		}

		$socialConfigurationData = $socialConfiguration->getAJAXData($event->paramsToArray());
		if (!$socialConfigurationData)
		{
			return [];
		}

		$scope = [];
		foreach ($socialConfigurationData['common']['scope'] as $key => $val)
		{
			if ($val)
			{
				$scope[] = $key;
			}
		}
		$params = [
			'access_token' => $accessToken,
			'provider' => $provider,
			'public_key' => $socialConfigurationData['common']['publicKey'],
			'private_key' => $socialConfigurationData['common']['privateKey'],
			'domain_website' => $socialConfigurationData['common']['websiteDomain'],
			'scope' => implode(',', $scope),
			'back_action' => $actionUrl
		];

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $userInfosUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_USERAGENT, 'OAuth/2 Simple PHP Client v0.1.1; HybridAuth http://hybridauth.sourceforge.net/');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'X-HTTP-Method-Override: GET']);
		$userInfosRaw = curl_exec($ch);

		curl_close($ch);

		return is_string($userInfosRaw) ? json_decode($userInfosRaw, true) : [];
	}
}