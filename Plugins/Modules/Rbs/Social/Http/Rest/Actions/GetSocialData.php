<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Http\Rest\Actions;

/**
 * @name \Rbs\Social\Http\Rest\Actions\GetSocialData
 */
class GetSocialData
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$documentId = $event->getRequest()->getQuery('documentId');
		if ($documentId)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$document = $documentManager->getDocumentInstance($documentId);
			if ($document instanceof \Change\Documents\AbstractDocument && $document instanceof \Change\Documents\Interfaces\Publishable)
			{
				$socialManager = new \Rbs\Social\SocialManager();
				$socialManager->setDocumentManager($event->getApplicationServices()->getDocumentManager());

				$result->setArray($socialManager->getFormattedSocialData($documentId, $event->getApplicationServices()->getDbProvider(),
					$documentManager));
				$event->setResult($result);
			}
			else
			{
				$result->setArray(['error' => 'invalid document']);
				$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_500);
				$event->setResult($result);
			}
		}
		else
		{
			$result->setArray(['error' => 'invalid document id']);
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_500);
			$event->setResult($result);
		}
	}
}