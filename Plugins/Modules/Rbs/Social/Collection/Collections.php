<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Social\Collection;

use Change\I18n\I18nString;

/**
 * @name \Rbs\Social\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addSocialCollection(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'facebook' => new I18nString($i18n, 'm.rbs.social.admin.social_collection_facebook', ['ucf']),
				'google' => new I18nString($i18n, 'm.rbs.social.admin.social_collection_google', ['ucf']),
				'twitter' => new I18nString($i18n, 'm.rbs.social.admin.social_collection_twitter', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Social_SocialCollection', $collection);
			$event->setParam('collection', $collection);
		}
	}
}