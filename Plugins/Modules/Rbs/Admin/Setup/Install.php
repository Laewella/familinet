<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Setup;

use Change\Http\OAuth\Consumer;
use Change\Plugins\PluginManager;

/**
 * @name \Rbs\Admin\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param \Change\Plugins\Plugin $plugin
	 * @return callable[]
	 */
	public function attach($events, $plugin)
	{
		$callable = parent::attach($events, $plugin);
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_APPLICATION, [$this, 'onExecuteApplication']);
		return $callable;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onExecuteApplication(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$resources = new \Rbs\Admin\Http\Resources($event->getApplication());
			$assetManager = $resources->getNewAssetManager();

			foreach ($pluginManager->getModules() as $plugin)
			{
				$resources->registerPlugin($assetManager, $plugin);
			}
			$resources->write($assetManager);
		}
		else
		{
			$resources = new \Rbs\Admin\Http\Resources($event->getApplication());
			$assetManager = $resources->getNewAssetManager();
			$plugins = $event->getParam('plugins');
			/** @var \Change\Plugins\Plugin $plugin */
			foreach ($plugins as $plugin)
			{
				if ($plugin->isModule())
				{
					$resources->registerPlugin($assetManager, $plugin);
				}
			}
			$resources->write($assetManager);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/Documents/Rbs_Admin', \Rbs\Admin\Events\Documents\SharedListeners::class);
		$webBaseDirectory = $configuration->getEntry('Change/Install/webBaseDirectory');

		$workspace = $application->getWorkspace();
		if ($webBaseDirectory && !$workspace->isAbsolutePath($webBaseDirectory))
		{
			$requirePath = implode(DIRECTORY_SEPARATOR, array_fill(0, count(explode(DIRECTORY_SEPARATOR, $webBaseDirectory)), '..'));
		}
		else
		{
			$requirePath = $workspace->projectPath();
		}

		$webBasePath = $workspace->composeAbsolutePath($webBaseDirectory);
		if (is_dir($webBasePath))
		{
			$srcPath = __DIR__ . '/Assets/admin.php';
			$content = \Change\Stdlib\FileUtils::read($srcPath);
			$content = str_replace('__DIR__', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($webBasePath . DIRECTORY_SEPARATOR . basename($srcPath), $content);
		}
		else
		{
			throw new \RuntimeException('Invalid document root path: ' . $webBasePath .
				'. Check "Change/Install/webBaseDirectory" configuration entry.', 999999);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$OAuth = $applicationServices->getOAuthManager();
		$consumer = $OAuth->getConsumerByApplication('Rbs_Admin');
		if ($consumer)
		{
			return;
		}

		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();

			$consumer = new Consumer($OAuth->generateConsumerKey(), $OAuth->generateConsumerSecret());
			$isb = $applicationServices->getDbProvider()->getNewStatementBuilder('Install::executeApplication');
			$fb = $isb->getFragmentBuilder();
			$isb->insert($fb->table($isb->getSqlMapping()->getOAuthApplicationTable()), $fb->column('application'),
				$fb->column('consumer_key'), $fb->column('consumer_secret'), $fb->column('timestamp_max_offset'),
				$fb->column('token_access_validity'), $fb->column('token_request_validity'), $fb->column('active'));
			$isb->addValues($fb->parameter('application'), $fb->parameter('consumer_key'), $fb->parameter('consumer_secret'),
				$fb->integerParameter('timestamp_max_offset'), $fb->parameter('token_access_validity'),
				$fb->parameter('token_request_validity'), $fb->booleanParameter('active'));
			$iq = $isb->insertQuery();
			$iq->bindParameter('application', 'Rbs_Admin');
			$iq->bindParameter('consumer_key', $consumer->getKey());
			$iq->bindParameter('consumer_secret', $consumer->getSecret());
			$iq->bindParameter('timestamp_max_offset', 60);
			$iq->bindParameter('token_access_validity', 'P10Y');
			$iq->bindParameter('token_request_validity', 'P1D');
			$iq->bindParameter('active', true);
			$iq->execute();

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}
}