<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Actions;

/**
 * @name \Rbs\Admin\Http\Actions\GetHome
 */
class GetHome
{
	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$manager = $genericServices->getAdminManager();
		$OAuth = $event->getApplicationServices()->getOAuthManager();
		$consumer = $OAuth->getConsumerByApplication('Rbs_Admin');
		$application = $event->getApplication();
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$LCID = $i18nManager->getLCID();
		$result = new \Rbs\Admin\Http\Result\Home();
		$templateFileName = implode(DIRECTORY_SEPARATOR, [__DIR__, 'Assets', 'home.twig']);

		$devMode = $application->inDevelopmentMode();
		if ($devMode)
		{
			$event->setParam('ACTION_NAME', __METHOD__);
		}

		$applicationShortName = $i18nManager->trans('m.rbs.ua.common.ui_main_admin_panel', ['ucf']);
		$applicationPackageName = $i18nManager->trans('m.rbs.ua.common.application_package_name', ['ucf']);
		$attributes = [
			'applicationName' => $applicationShortName . ' - ' . $applicationPackageName,
			'applicationShortName' => $applicationShortName,
			'applicationPackageName' => $applicationPackageName,
			'devMode' => $devMode,
			'angularDevMode' => $devMode && $application->getConfiguration('Change/Presentation/AngularDevMode'),
			'baseURL' => $event->getUrlManager()->getByPathInfo('/')->normalize()->toString(),
			'restURL' => $event->getUrlManager()->getByPathInfo('../rest.php/')->normalize()->toString(),
			'LCID' => $LCID,
			'lang' => substr($LCID, 0, 2),
			'lowerHyphenLCID' => strtolower(str_replace('_', '-', $LCID)),
			'OAuth' => array_merge($consumer->toArray(), ['realm' => 'Rbs_Admin']),
			'mainMenu' => $manager->getMainMenu(),
			'supportedLCIDs' => $i18nManager->getSupportedLCIDs(),
			'blockDefinitions' => $this->getBlockDefinitions($event->getApplicationServices()->getBlockManager())
		];
		$attributes['__change'] = $attributes;

		$resources = new \Rbs\Admin\Http\Resources($application);
		$assetManager = $resources->getNewAssetManager();
		$resources->registerPlugins($assetManager, $applicationServices->getPluginManager());

		$attributes['scripts'] = $resources->getScriptsUrl($assetManager);
		$attributes['styles'] = $resources->getStylesUrl($assetManager);

		$eventManager = $manager->getEventManager();
		$args = $eventManager->prepareArgs(['attributes' => $attributes]);

		$manager->getEventManager()->trigger('getHomeAttributes', $manager, $args);

		$html = $manager->renderTemplateFile($templateFileName, $args['attributes']);
		$renderer = function () use ($html)
		{
			return $html;
		};
		$result->setRenderer($renderer);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Presentation\Blocks\BlockManager $blockManager
	 * @return array
	 */
	protected function getBlockDefinitions($blockManager)
	{
		$array = [];
		$names = $blockManager->getBlockNames();
		foreach ($names as $name)
		{
			$information = $blockManager->getBlockInformation($name);
			if ($information)
			{
				list($v, $m, $b) = explode('_', $name);
				$data = [
					'name' => $information->getName(),
					'label' => $information->getLabel(),
					'template' => 'Block/' . $v . '/' . $m . '/' . $b . '/parameters.twig',
					'mailSuitable' => $information->isMailSuitable(),
					'defaultTTL' => $information->getDefaultTTL()
				];

				// Default template information.
				$templateInformation = $information->getDefaultTemplateInformation();
				if ($templateInformation && count($templateInformation->getParametersInformation()) > 0)
				{
					$data['defaultTemplate']['hasParameter'] = true;
				}

				$array[$information->getSection()][] = $data;
			}
		}
		ksort($array);
		return $array;
	}
}