<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http;

/**
 * @name \Rbs\Admin\Http\Resources
 */
class Resources
{

	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var boolean
	 */
	protected $devMode = false;

	/**
	 * @var string
	 */
	protected $webBaseDirectory;

	/**
	 * @var string
	 */
	protected $assetsVersion;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Application $application)
	{
		$this->application = $application;
		$configuration = $this->application->getConfiguration();
		$this->devMode = $configuration->inDevelopmentMode();
		$config = $configuration->getEntry('Change/Install');
		$this->webBaseDirectory = $config['webBaseDirectory'] ?? '';
		$this->assetsVersion = $config['assetsVersion'] ?? '';

	}

	/**
	 * @return boolean
	 */
	public function getDevMode()
	{
		return $this->devMode;
	}

	/**
	 * @param boolean $devMode
	 * @return $this
	 */
	public function setDevMode($devMode)
	{
		$this->devMode = $devMode;
		return $this;
	}

	/**
	 * @return \Change\Application
	 */
	public function getApplication()
	{
		return $this->application;
	}

	/**
	 * @param \Change\Application $application
	 * @return $this
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getWebBaseDirectory()
	{
		return $this->webBaseDirectory;
	}

	/**
	 * @param string $webBaseDirectory
	 * @return $this
	 */
	public function setWebBaseDirectory($webBaseDirectory)
	{
		$this->webBaseDirectory = $webBaseDirectory;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAssetsVersion()
	{
		return $this->assetsVersion;
	}

	/**
	 * @param string $assetsVersion
	 * @return $this
	 */
	public function setAssetsVersion($assetsVersion)
	{
		$this->assetsVersion = $assetsVersion;
		return $this;
	}

	/**
	 * @return \Assetic\AssetManager
	 */
	public function getNewAssetManager()
	{
		return new \Assetic\AssetManager();
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return string
	 */
	public function buildAssetBaseDirectory()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this->getAssetBaseDirectory();
	}

	/**
	 * @return string
	 */
	protected function getAssetBaseDirectory()
	{
		return $this->application->getWorkspace()->composeAbsolutePath($this->webBaseDirectory, 'Assets', $this->assetsVersion);
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function registerPlugins(\Assetic\AssetManager $assetManager, \Change\Plugins\PluginManager $pluginManager)
	{
		foreach ($pluginManager->getModules() as $modulePlugin)
		{
			if ($modulePlugin->getActivated())
			{
				$this->registerPlugin($assetManager, $modulePlugin);
			}
		}
		return $this;
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\Plugin $plugin
	 * @return $this
	 */
	public function registerPlugin(\Assetic\AssetManager $assetManager, \Change\Plugins\Plugin $plugin)
	{
		$name = $plugin->getName();
		switch ($name)
		{
			case 'Rbs_Admin':
				$this->registerAdminAssets($assetManager, $plugin);
				break;
			case 'Rbs_Ua':
				$this->registerUaAssets($assetManager, $plugin);
				$this->registerStandardPluginAssets($assetManager, $plugin);
				break;
			default:
				$this->registerStandardPluginAssets($assetManager, $plugin);
				break;
		}

		return $this;
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\Plugin $plugin
	 */
	protected function registerAdminAssets(\Assetic\AssetManager $assetManager, \Change\Plugins\Plugin $plugin)
	{
		$pluginPath = $plugin->getAssetsPath();
		$jsAssets = new \Assetic\Asset\AssetCollection();
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginPath . '/js/rbschange.js'));
		$jsAssets->add(new \Assetic\Asset\GlobAsset($pluginPath . '/js/*/*.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginPath . '/clipboard/controllers.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginPath . '/tasks/controllers.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginPath . '/notifications/controllers.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginPath . '/js/routes.js'));
		if (!$this->devMode)
		{
			$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
		}
		$jsAssets->setTargetPath('Rbs/Admin/js/Rbs_Admin.js');
		$assetManager->set('JS_Rbs_Admin', $jsAssets);

		$cssAsset = new \Assetic\Asset\AssetCollection();
		$cssAsset->add(new \Assetic\Asset\GlobAsset($pluginPath . '/css/*.css'));
		$cssAsset->add(new \Assetic\Asset\FileAsset($pluginPath . '/dashboard/dashboard.css'));
		$cssAsset->setTargetPath('Rbs/Admin/css/Rbs_Admin.css');
		$assetManager->set('CSS_Rbs_Admin', $cssAsset);
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\Plugin $plugin
	 */
	protected function registerUaAssets(\Assetic\AssetManager $assetManager, \Change\Plugins\Plugin $plugin)
	{
		$assetsPath = $plugin->getAssetsPath() . '/Proximis';
		$globs = [$assetsPath . '/*.css', $assetsPath . '/*/*.css'];
		$cssAsset = new \Assetic\Asset\GlobAsset($globs);
		$cssAsset->setTargetPath('Rbs/Admin/css/Rbs_Ua.css');
		$assetManager->set('CSS_Rbs_Ua', $cssAsset);

		$genericAssetsPath = $plugin->getAssetsPath() . '/ProximisModal';
		if (is_dir($genericAssetsPath))
		{
			$globs = [$genericAssetsPath . '/*.js', $genericAssetsPath . '/*/*.js'];
			$jsAssets = new \Assetic\Asset\GlobAsset($globs);
			if (!$this->devMode)
			{
				$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
			}
			$jsAssets->setTargetPath('Rbs/Admin/js/Rbs_Ua_proximisModal.js');
			$assetManager->set('JS_Rbs_Ua_proximisModal', $jsAssets);
		}

		$genericAssetsPath = $plugin->getAssetsPath() . '/ProximisIntl';
		if (is_dir($genericAssetsPath))
		{
			$globs = [$genericAssetsPath . '/*.js', $genericAssetsPath . '/*/*.js'];
			$jsAssets = new \Assetic\Asset\GlobAsset($globs);
			if (!$this->devMode)
			{
				$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
			}
			$jsAssets->setTargetPath('Rbs/Admin/js/Rbs_Ua_proximisIntl.js');
			$assetManager->set('JS_Rbs_Ua_proximisIntl', $jsAssets);
		}
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\Plugin $plugin
	 */
	protected function registerStandardPluginAssets(\Assetic\AssetManager $assetManager, \Change\Plugins\Plugin $plugin)
	{
		$adminAssetsPath = $plugin->getAssetsPath() . '/Admin';
		if (is_dir($adminAssetsPath))
		{
			$name = $plugin->getName();
			$globs = [$adminAssetsPath . '/*.js', $adminAssetsPath . '/Documents/*/*.js'];
			$jsAssets = new \Assetic\Asset\GlobAsset($globs);
			if ($jsAssets->all())
			{
				if (!$this->devMode)
				{
					$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
				}
				$jsAssets->setTargetPath('Rbs/Admin/js/' . $name . '.js');
				$assetManager->set('JS_' . $name, $jsAssets);
			}

			$globs = [$adminAssetsPath . '/*.css'];
			$cssAsset = new \Assetic\Asset\GlobAsset($globs);
			if ($cssAsset->all())
			{
				$cssAsset->setTargetPath('Rbs/Admin/css/' . $name . '.css');
				$assetManager->set('CSS_' . $name, $cssAsset);
			}
		}
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @return string[]
	 */
	public function getScriptsUrl(\Assetic\AssetManager $assetManager)
	{
		$baseUrl = '/Assets/'. $this->assetsVersion . '/';
		$scripts = [];
		$names = $assetManager->getNames();
		if (count(array_intersect(['JS_Rbs_Admin'], $names)) != 1)
		{
			return $scripts;
		}
		$scripts[] = $baseUrl . $assetManager->get('JS_Rbs_Ua_proximisModal')->getTargetPath();
		$scripts[] = $baseUrl . $assetManager->get('JS_Rbs_Ua_proximisIntl')->getTargetPath();
		$scripts[] = $baseUrl . $assetManager->get('JS_Rbs_Admin')->getTargetPath();
		foreach (array_diff($names, ['JS_Rbs_Admin', 'JS_Rbs_Ua_proximisModal', 'JS_Rbs_Ua_proximisIntl']) as $name)
		{
			if (strpos($name, 'JS_') === 0)
			{
				$scripts[] = $baseUrl . $assetManager->get($name)->getTargetPath();
			}
		}
		return $scripts;
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @return string[]
	 */
	public function getStylesUrl(\Assetic\AssetManager $assetManager)
	{
		$baseUrl = '/Assets/' . $this->assetsVersion . '/';
		$styles = [];
		$names = $assetManager->getNames();
		if (count(array_intersect(['CSS_Rbs_Admin', 'CSS_Rbs_Ua'], $names)) != 2)
		{
			return $styles;
		}

		$styles[] = $baseUrl . $assetManager->get('CSS_Rbs_Ua')->getTargetPath();
		$styles[] = $baseUrl . $assetManager->get('CSS_Rbs_Admin')->getTargetPath();

		foreach (array_diff($names, ['CSS_Rbs_Admin', 'CSS_Rbs_Ua']) as $name)
		{
			if (strpos($name, 'CSS_') === 0)
			{
				$styles[] = $baseUrl . $assetManager->get($name)->getTargetPath();
			}
		}
		return $styles;
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function write(\Assetic\AssetManager $assetsManager)
	{
		$writer = new \Assetic\AssetWriter($this->getAssetBaseDirectory());
		$writer->writeManagerAssets($assetsManager);
	}
}