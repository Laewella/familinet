<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Rest\Actions;

/**
 * @name \Rbs\Admin\Http\Rest\Actions\ChangeVersion
 */
class ChangeVersion
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setHeaderLastModified(\Change\Stdlib\FileUtils::getModificationDate(__FILE__));
		$version = $event->getApplication()->getConfiguration('Change/Application/version');
		$result->setArray(['version' => $version]);
		$event->setResult($result);
	}
}