<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin;

/**
 * @name \Rbs\Admin\WysiwygHtmlParser
 */
class WysiwygHtmlParser implements \Change\Presentation\RichText\ParserInterface
{
	/**
	 * @var \Rbs\Website\Documents\Website|null
	 */
	protected $website;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function __construct(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->applicationServices = $applicationServices;
	}

	/**
	 * @param string $rawText
	 * @param array $context
	 * @return string
	 */
	public function parse($rawText, $context)
	{
		if (isset($context['website']))
		{
			$this->website = $context['website'];
		}

		$replacements = ['<ul>' => '<ul class="bullet">'];
		$rawText = strtr($rawText, $replacements);

		$rawText = $this->replaceHref($rawText);
		$rawText = $this->replaceSrc($rawText);

		return $rawText;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return string
	 */
	protected function getUrl($document)
	{
		$website = $this->website;
		if ($document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$website = $document->getSection() ? $document->getSection()->getWebsite() : null;
		}
		elseif ($document instanceof \Change\Presentation\Interfaces\Section)
		{
			$website = $document->getWebsite();
		}

		if ($website)
		{
			$urlManager = $website->getUrlManager($website->getLCID());
			return $urlManager->getCanonicalByDocument($document)->normalize()->toString();
		}

		return 'javascript:;';
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return string
	 */
	protected function getDownloadUrl($document)
	{
		$website = $this->website;
		if ($website)
		{
			$urlManager = $website->getUrlManager($website->getLCID());
			return $urlManager->getActionURL('Rbs_Media', 'Download', ['documentId' => $document->getId()]);
		}

		return 'javascript:;';
	}

	/**
	 * @param integer $documentId
	 * @param integer $width
	 * @param integer $height
	 * @return string|null
	 */
	protected function getImageUrl($documentId, $width, $height)
	{
		if ($this->applicationServices)
		{
			$image = $this->applicationServices->getDocumentManager()->getDocumentInstance($documentId);
			if ($image instanceof \Rbs\Media\Documents\Image)
			{
				return $image->getPublicURL($width, $height);
			}
		}
		return null;
	}

	/**
	 * @param string $rawText
	 * @return string
	 */
	protected function replaceHref($rawText)
	{
		if (!preg_match_all('/<a\s[^>]+>.*<\/a>/Usi', $rawText, $matches))
		{
			return $rawText;
		}

		$links = array_unique($matches[0]);
		foreach ($links as $link)
		{
			$match = [];
			if (!preg_match('/<a[^>]*(data-document-id="(\d+)")[^>]*>/Ui', $link, $match))
			{
				continue;
			}

			$document = $this->applicationServices->getDocumentManager()->getDocumentInstance((int)$match[2]);
			if ($document instanceof \Rbs\Media\Documents\File)
			{
				$href = $this->getDownloadUrl($document);
				$size = $this->applicationServices->getI18nManager()->transFileSize($document->getContentLength());
				$extension = strtoupper($document->getExtension());
				$collection = $this->applicationServices->getCollectionManager()->getCollection('Rbs_Media_FileExtensions');
				if ($collection)
				{
					$item = $collection->getItemByValue(strtolower($extension));
					if ($item)
					{
						$extension = '<abbr title="' . $item->getTitle() . '">' . $extension . '</abbr>';
					}
				}
				$suffix = ' [' . $extension . ' — ' . $size . ']';
			}
			elseif ($document instanceof \Change\Documents\AbstractDocument && $document->getDocumentModel()->isPublishable())
			{
				$href = $this->getUrl($document);
				$suffix = '';
			}
			else
			{
				$href = 'javascript:;';
				$suffix = '';
			}
			$href = \Change\Stdlib\StringUtils::attrEscape($href);

			$replaceLink = $link;

			if (preg_match('/href="([^"]*)"/', $link, $hrefMatch))
			{
				$replaceLink = str_replace($hrefMatch[0], '', $replaceLink);
			}

			$replaceLink = str_replace($match[1], $match[1] . ' href="' . $href . '"', $replaceLink) . $suffix;
			$rawText = str_replace($link, $replaceLink, $rawText);
		}
		return $rawText;
	}

	/**
	 * @param string $rawText
	 * @return string
	 */
	protected function replaceSrc($rawText)
	{
		if (preg_match_all('/<img\s[^>]+>/i', $rawText, $matches))
		{
			$links = array_unique($matches[0]);
			foreach ($links as $link)
			{
				if (preg_match('/data-document-id="(\d+)"/', $link, $match))
				{
					$width = 0;
					if (preg_match('/width="(\d+)"/', $link, $dataMatch))
					{
						$width = (int)$dataMatch[1];
					}

					$height = 0;
					if (preg_match('/height="(\d+)"/', $link, $dataMatch))
					{
						$height = (int)$dataMatch[1];
					}

					$replaceLink = $link;
					$documentId = (int)$match[1];
					$href = $this->getImageUrl($documentId, $width, $height);

					if (preg_match('/src="([^"]+)"/', $link, $dataMatch))
					{
						$replaceLink = str_replace($dataMatch[0], '', $replaceLink);
					}

					$replaceLink = str_replace($match[0], $match[0] . ' src="' . \Change\Stdlib\StringUtils::attrEscape($href) . '"',
						$replaceLink);
					$rawText = str_replace($link, $replaceLink, $rawText);
				}
			}
		}
		return $rawText;
	}
}