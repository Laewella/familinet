(function(jQuery, __change) {
	'use strict';

	// Initialize Intl.
	__change.RBS_Ua_Intl.initialize(__change.LCID);

	/**
	 * @ngdoc module
	 * @name RbsChange
	 *
	 * @description
	 * The RbsChange module contains all components for Change admin panel.
	 */

	// Convenient hack to reverse jQuery collections.
	jQuery.fn.reverse = [].reverse;

	// Declares the main module and its dependencies.
	var app = angular.module('RbsChange', ['ngRoute', 'ngResource', 'ngSanitize', 'ngTouch', 'ngCookies',
		'ngAnimate', 'ngMessages', 'OAuthModule', 'ui.bootstrap', 'colorpicker.module', 'proximisModal', 'proximisIntl']);

	//-------------------------------------------------------------------------
	//
	// Constants.
	//
	//-------------------------------------------------------------------------

	app.constant('RbsChange.Device', {
		isMultiTouch: function() {
			return ('ontouchstart' in document.documentElement);
		}
	});

	/**
	 * Events used by Change, where you can attach your own handlers.
	 */
	app.constant('RbsChange.Events', {
		// Raised to provoke a trigger.
		'Logout': 'Change:Logout',

		// Raised when an Editor has finished loading its document.
		// Single argument is the edited document.
		'EditorLoaded': 'Change:Editor.Loaded',

		// Raised when an Editor is ready.
		// Single argument is the edited document.
		'EditorReady': 'Change:Editor.Ready',

		// Raised when an Editor is about to save a Document.
		// Single argument is a hash object with:
		// - document: the edited document that is about to be saved
		// - promises: array of promises that should be resolved before the save process is called.
		'EditorPreSave': 'Change:Editor.RegisterPreSavePromises',

		// Raised when an Editor has just saved a Document.
		// Single argument is a hash object with:
		// - document: the edited document that has been saved
		// - promises: array of promises that should be resolved before the edit process is terminated.
		'EditorPostSave': 'Change:Editor.RegisterPostSavePromises',

		// The following events are less useful for you...
		'EditorDocumentUpdated': 'Change:Editor.DocumentUpdated',
		'EditorCorrectionChanged': 'Change:CorrectionChanged',
		'EditorCorrectionRemoved': 'Change:CorrectionRemoved',
		'EditorUpdateDocumentProperties': 'Change:UpdateDocumentProperties',

		// Raised from the <rbs-document-list/> directive when a filter parameter is present in the URL.
		// Listeners should fill in the 'predicates' recieved in the args.
		'DocumentListApplyFilter': 'Change:DocumentList.ApplyFilter',

		// Raised from the <rbs-document-list/> directive when a converter has been requested on a column.
		// {
		//    "converter" : "...",
		//    "params"    : "...",
		//    "promises"  : [],
		//    "values"    : {}
		// }
		// Listeners should fill in the "promises" array and the "values" hash object.
		'DocumentListConverterGetValues': 'Change:DocumentList.ConverterGetValues',

		'DocumentListPreview': 'Change:DocumentList.Preview'
	});

	//-------------------------------------------------------------------------
	//
	// Configuration.
	//
	//-------------------------------------------------------------------------

	app.config(['$compileProvider', '$locationProvider', '$interpolateProvider', 'localStorageServiceProvider', 'OAuthServiceProvider',
		function($compileProvider, $locationProvider, $interpolateProvider, localStorageServiceProvider, OAuthServiceProvider) {
			$locationProvider.html5Mode(true);
			$interpolateProvider.startSymbol('(=').endSymbol('=)');

			localStorageServiceProvider.setPrefix('_Change_');

			var oauthUrl = __change.restURL + 'OAuth/';
			OAuthServiceProvider.setBaseUrl(oauthUrl);
			OAuthServiceProvider.setRealm(__change.OAuth.realm);

			// Sign all the requests on our REST services...
			OAuthServiceProvider.setSignedUrlPatternInclude('/rest.php/');
			// ... but do NOT sign OAuth requests.
			OAuthServiceProvider.setSignedUrlPatternExclude(oauthUrl);

			if (!__change.angularDevMode) {
				$compileProvider.debugInfoEnabled(false);
				if (__change.devMode) {
					console.warn(
						'Development mode is disabled in AngularJS.' +
						' To enable it, set Change/Presentation/AngularDevMode to true in the project configuration.' +
						'\nMore info here: https://code.angularjs.org/' + angular.version.full + '/docs/guide/production#disabling-debug-data'
					);
				}
			}
	}]);

	//-------------------------------------------------------------------------
	//
	// Directives.
	//
	//-------------------------------------------------------------------------

	app.directive('rbsChangeVersion', ['RbsChange.REST', function(REST) {
		return {
			'restrict': 'A',
			link: function(scope, elm) {
				var versionData = '(AngularJS ' + angular.version.full + ' &mdash; jQuery ' + jQuery.fn.jquery + ')';
				REST.call(REST.getBaseUrl('admin/changeVersion')).then(
					function(result) {
						if (result.version != null) {
							versionData = 'Version ' + result.version + ' ' + versionData;
						}
						elm.html(versionData);
					}
				);
			}
		};
	}]);

	/**
	 * Directive that automatically gives the focus to an element when it is created/displayed.
	 */
	app.directive('rbsAutoFocus', function() {
		var timer = null;

		return function(scope, elm) {
			if (timer) {
				clearTimeout(timer);
			}

			timer = setTimeout(function() {
				elm.focus();
				timer = null;
			});
		};
	});

	// This directive cannot be prefixed by rbs because she is applied on html tag time.
	app.directive('time', ['$timeout', '$filter', function($timeout, $filter) {
		var DEFAULT_INTERVAL = 60;

		return {
			'restrict': 'E',
			'scope': {
				'datetime': '@',
				'display': '@'
			},

			'link': function(scope, element, attrs) {

				if (!element.is('[datetime]')) {
					throw new Error("Attribute 'datetime' is required on <time/> elements.");
				}

				var dateTime,
					stop,
					content = element.html();

				attrs.$observe('datetime', function(value) {
					if (value) {
						dateTime = moment(value);
						if (stop) {
							$timeout.cancel(stop);
						}
						update();
					}
				});

				attrs.$observe('display', function() {
					if (stop) {
						$timeout.cancel(stop);
					}
					update();
				});

				function update() {
					var html, title;

					if (!dateTime) {
						return;
					}

					switch (attrs.display) {
						case 'relative':
							html = dateTime.fromNow();
							title = $filter('rbsDateTime')(dateTime.toDate());
							break;
						case 'both':
							html = $filter('rbsDateTime')(dateTime.toDate()) + ' (' + dateTime.fromNow() + ')';
							break;
						default :
							title = dateTime.fromNow();
							html = $filter('rbsDateTime')(dateTime.toDate());
					}

					if (content) {
						element.html(content.replace(/\{time\}/, html));
					}
					else {
						element.html(html);
					}

					if (title) {
						element.attr('title', title);
					}

					// Re-launch timer for next update
					stop = $timeout(update, DEFAULT_INTERVAL * 1000);
				}

				scope.$on('$destroy', function() {
					$timeout.cancel(stop);
				});
			}
		};
	}]);

	app.directive('rbsAdvancedMode', ['RbsChange.i18n', function(i18n) {
		return {
			'restrict': 'AE',
			'transclude': true,
			'template': '<div class="advanced-mode"><div class="separator"></div><div class="inner"><h4>' +
			i18n.trans('m.rbs.admin.admin.advanced_mode') + '</h4><div data-ng-transclude=""></div></div></div>',
			'replace': true
		};
	}]);

	//-------------------------------------------------------------------------
	//
	// Controllers.
	//
	//-------------------------------------------------------------------------

	/**
	 * RootController
	 *
	 * This Controller is bound to the <body/> tag and is, thus, the "root Controller".
	 * Mostly, it deals with user authentication and settings.
	 */
	app.controller('Change.RootController',
		['$scope', '$rootScope', 'RbsChange.User', '$location', 'RbsChange.Events', function(scope, $rootScope, User, $location, Events) {
			scope.hideLoadingPanel = false;
			if ($location.path() !== '/authenticate') {
				if (User.init()) {
					scope.hideLoadingPanel = true;
				}
			}

			$rootScope.$on('OAuth:AuthenticationSuccess', function() {
				User.load().then(function() {
					$location.url($location.search()['route']);
					scope.hideLoadingPanel = true;
				});
			});

			$rootScope.logout = function() {
				$rootScope.$emit(Events.Logout);
			};
		}]);

	//-------------------------------------------------------------------------
	//
	// Animations.
	//
	//-------------------------------------------------------------------------

	function rbsVerticalIfAnimation() {
		return {
			enter: function(element, done) {
				jQuery(element).css({
					overflow: 'hidden',
					height: 0
				});
				jQuery(element).animate({
					height: element.find('.vertical-if-animation-content').outerHeight(true)
				}, 500, function() {
					element.css('height', 'auto');
					done();
				});
			},

			leave: function(element, done) {
				jQuery(element).css({
					height: element.find('.vertical-if-animation-content').outerHeight(true)
				});
				jQuery(element).animate({
					overflow: 'hidden',
					height: 0
				}, 500, done);
			}
		};
	}
	app.animation('.vertical-if-animation', rbsVerticalIfAnimation);

	function rbsVerticalShowHideAnimation() {
		return {
			beforeAddClass: function(element, className, done) {
				if (className == 'ng-hide') {
					jQuery(element).animate({
						overflow: 'hidden',
						height: 0
					}, done);
				}
				else {
					done();
				}
			},

			removeClass: function(element, className, done) {
				if (className == 'ng-hide') {
					element.css({
						height: 0,
						overflow: 'hidden'
					});
					jQuery(element).animate({
							height: element.find('.vertical-show-hide-animation-content').height()
						},
						function() {
							element.css({
								height: 'auto',
								overflow: 'visible'
							});
							done();
						}
					);
				}
				else {
					done();
				}
			}
		};
	}
	app.animation('.vertical-show-hide-animation', rbsVerticalShowHideAnimation);

	if (moment) {
		moment.locale(__change.LCID);
	}
})(window.jQuery, window.__change);