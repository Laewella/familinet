(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsEmptyLabel
	 * @name rbsEmptyLabel
	 * @function
	 *
	 * @description
	 * For empty strings, returns a hyphen.
	 *
	 * @param {string} string The input string.
	 */
	app.filter('rbsEmptyLabel', function() {
		return function(input, value, cssClass) {
			value = value || '-';
			if (input === 0) {
				return '0';
			}
			if (input === null || ('' + input).trim().length === 0) {
				if (cssClass) {
					return '<span class="' + cssClass + '">' + value + '</span>';
				}
				return value;
			}
			return input;
		};

	});

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsModelLabel
	 * @name rbsModelLabel
	 * @function
	 *
	 * @description
	 * Returns the model's label.
	 *
	 * @param {string} string The model's label.
	 */
	app.filter('rbsModelLabel', ['RbsChange.Models', function(Models) {
		return function(input) {
			return input ? Models.getModelLabel(input) : '';
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsCapitalize
	 * @name rbsCapitalize
	 * @function
	 *
	 * @description
	 * Returns the input string with its first letter uppercase and the remaining lowercase.
	 *
	 * @param {string} string The input string.
	 */
	app.filter('rbsCapitalize', function() {
		return function(input) {
			return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
		};
	});

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsFileSize
	 * @name rbsFileSize
	 * @function
	 *
	 * @description
	 * Returns a formatted and human readable file size from an input value in bytes.
	 *
	 * @param {number} size The size in bytes.
	 */
	app.filter('rbsFileSize', ['RbsChange.i18n', function(i18n) {
		var units = [
			[i18n.trans('c.filesize.bytes')],
			[i18n.trans('c.filesize.kilobytes_abbr'), i18n.trans('c.filesize.kilobytes | ucf')],
			[i18n.trans('c.filesize.megabytes_abbr'), i18n.trans('c.filesize.megabytes | ucf')],
			[i18n.trans('c.filesize.gigabytes_abbr'), i18n.trans('c.filesize.gigabytes | ucf')],
			[i18n.trans('c.filesize.terabytes_abbr'), i18n.trans('c.filesize.terabytes | ucf')]
		];

		return function(bytes) {
			var value = bytes, u = 0;
			while (value >= 1024 && u < units.length) {
				u++;
				value /= 1024.0;
			}
			if (u === 0) {
				return value + ' ' + units[u][0];
			}
			return Math.round(value) + ' <abbr title="' + units[u][1] + '">' + units[u][0] + '</abbr>';
		};

	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsEllipsis
	 * @name rbsEllipsis
	 * @function
	 *
	 * @description
	 * Truncates the input string to `length` characters and adds an ellipsis at the end.
	 *
	 * @param {string} string The input string.
	 * @param {number} length The number of characters of string to display before the ellipsis.
	 */
	app.filter('rbsEllipsis', function() {
		return function(input, length, where) {
			where = (where || 'end').toLowerCase();
			if (!angular.isString(input)) {
				return input;
			}
			if (input.length <= length) {
				return input;
			}
			if (where === 'center') {
				return input.substring(0, Math.floor(length / 2) - 3) + '...' +
					input.substring(input.length - Math.floor(length / 2));
			}
			return input.substring(0, length - 3) + '...';
		};
	});

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsDiff
	 * @name rbsDiff
	 * @function
	 *
	 * @description
	 * Formats a visual diff between the input string and the first parameter.
	 *
	 * Uses {@link https://code.google.com/p/google-diff-match-patch/ lib/diff_match_patch]
	 *
	 * @param {string} left The input string to be compared to the first parameter.
	 * @param {string=} right The string to be compared to the input string.
	 */
	app.filter('rbsDiff', ['RbsChange.i18n', function(i18n) {
		return function(input, match) {
			var output, diffObj, diffs;

			if (angular.isObject(input)) {
				input = JSON.stringify(input);
			}
			if (angular.isString(input)) {
				input = input.replace(/</g, '&lt;').replace(/>/g, '&gt;');
			}

			if (angular.isObject(match)) {
				match = JSON.stringify(match);
			}
			if (angular.isString(match)) {
				match = match.replace(/</g, '&lt;').replace(/>/g, '&gt;');
			}

			output = '<span class="diff">';
			if (!angular.isString(input) || !angular.isString(match)) {
				match = '' + match;
				if (match) {
					output += '<del title="' + i18n.trans('m.rbs.admin.admin.deleted | ucf') + '">' + match + '</del>';
				}
				input = '' + input;
				if (input) {
					output += '<ins title="' + i18n.trans('m.rbs.admin.admin.added | ucf') + '">' + input + '</ins>';
				}
			}
			else {
				//noinspection JSPotentiallyInvalidConstructorUsage
				diffObj = new diff_match_patch();
				diffs = diffObj.diff_main(match || '', input || '');
				diffObj.diff_cleanupSemantic(diffs);

				angular.forEach(diffs, function(diff) {
					if (diff[0] === -1) {
						output += '<del title="' + i18n.trans('m.rbs.admin.admin.deleted | ucf') + '">' + diff[1] + '</del>';
					}
					else if (diff[0] === 1) {
						output += '<ins title="' + i18n.trans('m.rbs.admin.admin.added | ucf') + '">' + diff[1] + '</ins>';
					}
					else {
						output += diff[1];
					}
				});
			}

			return output + '</span>';
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsStatusLabel
	 * @name rbsStatusLabel
	 * @function
	 *
	 * @description
	 * Replace a document publication status by its label.
	 *
	 * @param {string} status The publication status.
	 */
	app.filter('rbsStatusLabel', ['RbsChange.i18n', function(i18n) {
		return function(input) {
			if (!input) {
				return '';
			}
			return i18n.trans('m.rbs.admin.admin.status_' + angular.lowercase(input) + '|ucf');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsMaxNumber
	 * @name rbsMaxNumber
	 * @function
	 *
	 * @description
	 * For large numbers, displays the `max` value appended with a +
	 *
	 * @param {number} number The input number.
	 * @param {number=} max The maximum value to display, defaults to 99.
	 */
	app.filter('rbsMaxNumber', ['$filter', function($filter) {
		return function(input, max) {
			max = max || 99;
			if (input > max) {
				return $filter('number')(max) + '+';
			}
			return $filter('number')(input);
		};
	}]);

	// Date formats.

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsDateTime
	 * @name rbsDateTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with date and time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('rbsDateTime', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'medium');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsDate
	 * @name rbsDate
	 * @function
	 *
	 * @description
	 * Formats a Date object with date, without time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('rbsDate', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'mediumDate');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsTime
	 * @name rbsTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with time, without the date.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('rbsTime', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'mediumTime');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsBoolean
	 * @name rbsBoolean
	 * @function
	 *
	 * @description
	 * Formats a Boolean value with localized <em>yes</em> or <em>no</em>.
	 *
	 * @param {boolean} value The boolean value to format.
	 */
	app.filter('rbsBoolean', ['RbsChange.i18n', function(i18n) {
		return function(input) {
			return i18n.trans(input ? 'm.rbs.admin.admin.yes' : 'm.rbs.admin.admin.no');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsOrderBy
	 * @name rbsOrderBy
	 * @function
	 *
	 * @description
	 * A fixed version of the standard orderBy filter, doing nothing if no sortPredicate.
	 *
	 * @param {Array} input The array to sort.
	 * @param {Function|string|Array=} sortPredicate A predicate to be used by the comparator to determine the order of elements.
	 *
	 *    Can be one of:
	 *
	 *    - `Function`: Getter function. The result of this function will be sorted using the `<`, `=`, `>` operator.
	 *    - `string`: An Angular expression. The result of this expression is used to compare elements
	 *      (for example `name` to sort by a property called `name` or `name.substr(0, 3)` to sort by
	 *      3 first characters of a property called `name`). The result of a constant expression
	 *      is interpreted as a property name to be used in comparisons (for example `"special name"`
	 *      to sort object by the value of their `special name` property). An expression can be
	 *      optionally prefixed with `+` or `-` to control ascending or descending sort order
	 *      (for example, `+name` or `-name`). If no property is provided, (e.g. `'+'`) then the array
	 *      element itself is used to compare where sorting.
	 *    - `Array`: An array of function or string predicates. The first predicate in the array
	 *      is used for sorting, but when two items are equivalent, the next predicate is used.
	 *
	 *    If the predicate is missing or empty then it defaults to `'+'`.
	 *
	 * @param {boolean=} reverseOrder Reverse the order of the array.
	 * @returns {Array} Sorted copy of the source array.
	 */
	app.filter('rbsOrderBy', ['$filter', function($filter) {
		return function(input, sortPredicate, reverseOrder) {
			if (!sortPredicate) {
				return input;
			}
			return $filter('orderBy')(input, sortPredicate, reverseOrder);
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsDocumentListSummary
	 * @name rbsDocumentListSummary
	 * @function
	 *
	 * @description
	 * Renders an HTML string that summarizes the given list of documents.
	 *
	 * @param {Array} documents Array of Document objects
	 */
	app.filter('rbsDocumentListSummary', ['RbsChange.i18n', function(i18n) {
		return function(docs) {
			function getLabel(obj) {
				if (angular.isObject(obj)) {
					if (obj.hasOwnProperty('label')) {
						return obj.label;
					}
					else if (obj.hasOwnProperty('title')) {
						return obj.title;
					}
					else if (obj.hasOwnProperty('name')) {
						return obj.name;
					}
				}
				return '' + obj;
			}

			var out = '',
				msg,
				i;

			if (angular.isArray(docs)) {
				if (docs.length > 3) {
					out = i18n.trans('m.rbs.admin.admin.filter_document_list_summary_more_three', {
						'count': docs.length,
						'element1': getLabel(docs[0]),
						'element2': getLabel(docs[1]),
						'element3': getLabel(docs[2])
					});
				}
				else if (docs.length > 1) {
					msg = [];
					for (i = 0; i < docs.length - 1; i++) {
						msg.push(getLabel(docs[i]));
					}
					out = i18n.trans('m.rbs.admin.admin.filter_document_list_summary_less_three', {
						'count': docs.length,
						'elementsAsHtml': msg.join('</strong>, <strong class="element">'),
						'lastElement': getLabel(docs[docs.length - 1])
					});
				}
				else {
					out = '<strong class="element">' + getLabel(docs[0]) + '</strong>';
				}
			}
			return out;
		};
	}]);
})();