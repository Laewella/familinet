/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc service
	 * @id RbsChange.service:Blocks
	 * @name Blocks
	 *
	 * @description
	 * A service to handle blocks definitions.
	 */
	app.factory('RbsChange.Blocks', [function() {
		var allBlocks = {};
		var standardBlocks = [];
		var mailSuitableBlocks = [];

		angular.forEach(__change.blockDefinitions, function(pluginBlocks, pluginLabel) {
			angular.forEach(pluginBlocks, function(block) {
				block.plugin = pluginLabel;
				if (block.mailSuitable) {
					mailSuitableBlocks.push(block);
				}
				else {
					standardBlocks.push(block);
				}
				allBlocks[block.name] = block;
			});
		});

		return {
			/**
			 * @ngdoc method
			 * @methodOf Blocks
			 * @name getList
			 *
			 * @description
			 * Get the blocks list.
			 *
			 * @param {boolean=} mailSuitable Mail suitable restriction.
			 * @returns {Array}
			 */
			getList: function(mailSuitable) {
				return mailSuitable ? mailSuitableBlocks : standardBlocks;
			},

			/**
			 * @ngdoc method
			 * @methodOf Blocks
			 * @name getBlock
			 *
			 * @description
			 * Get the infos for the given block.
			 *
			 * @param {string} name The block name.
			 * @returns {Object|null}
			 */
			getBlock: function(name) {
				return allBlocks.hasOwnProperty(name) ? allBlocks[name] : null;
			},

			/**
			 * @ngdoc method
			 * @methodOf Blocks
			 * @name getBlockLabel
			 *
			 * @description
			 * Get the label of the given block.
			 *
			 * @param {string} name The block name.
			 * @returns {string|null}
			 */
			getBlockLabel: function(name) {
				return allBlocks.hasOwnProperty(name) ? allBlocks[name].label : null;
			}
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:rbsFilterBlocks
	 * @name rbsFilterBlocks
	 *
	 * @description
	 * Filter a block list.
	 *
	 * @param {Array} blocks The block list.
	 * @param {string} keyword The filter value.
	 */
	app.filter('rbsFilterBlocks', rbsFilterBlock);

	function rbsFilterBlock() {
		return function(blocks, keyword) {
			var result = [];
			if (keyword == '' || keyword == undefined) {
				result = blocks;
			}
			else {
				angular.forEach(blocks, function(block) {
					var q = keyword.toLowerCase();
					var t = block.name.toLowerCase();
					var l = block.label.toLowerCase();
					if (t.indexOf(q) != -1 || l.indexOf(q) != -1) {
						result.push(block);
					}
				});
			}
			return result;
		}
	}
})(window.__change);