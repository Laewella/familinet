/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.factory('RbsChange.DocumentCache', ['$cacheFactory', function($cacheFactory) {
		return $cacheFactory('RbsChange.DocumentCache', { capacity: 50 });
	}]);

	/**
	 * @ngdoc service
	 * @id RbsChange.service:REST
	 * @name REST
	 *
	 * @description
	 * Provides methods to deal with REST services:
	 *
	 * - load and save Documents
	 * - load Collections
	 * - call other REST services
	 */
	app.provider('RbsChange.REST', function RbsChangeRESTProvider() {
		var forEach = angular.forEach,
			REST_BASE_URL,
			HTTP_STATUS_CREATED = 201,
			temporaryId;

		this.setBaseUrl = function(url) {
			REST_BASE_URL = url;
		};

		this.$get = ['$http', '$location', '$q', '$timeout', '$rootScope', 'RbsChange.Utils', 'RbsChange.ArrayUtils', 'RbsChange.UrlManager',
			'localStorageService', 'RbsChange.DocumentCache', 'RbsChange.Events', 'RbsChange.NotificationCenter', 'RbsChange.i18n',

			function($http, $location, $q, $timeout, $rootScope, Utils, ArrayUtils, UrlManager, localStorageService, DocumentCache, Events, NotificationCenter, i18n) {
				var absoluteUrl,
					language = 'fr_FR',
					lastCreatedDocument = null,
					REST;

				temporaryId = localStorageService.get("temporaryId");
				if (temporaryId === null) {
					temporaryId = 0;
				}
				else {
					temporaryId = parseInt(temporaryId, 10);
				}

				if (!REST_BASE_URL) {
					absoluteUrl = $location.absUrl();
					absoluteUrl = absoluteUrl.replace(/admin\.php.*/, 'rest.php/');
					REST_BASE_URL = absoluteUrl;
				}

				/**
				 * @ngdoc object
				 * @id RbsChange.objects:ChangeDocument
				 * @name ChangeDocument
				 *
				 * @description
				 * An object representing a Change document.
				 *
				 * @property {string} model The model name of the document.
				 * @property {number} id The id of the document.
				 */
				function ChangeDocument() {
					this.META$ = {
						'links': {},
						'actions': {},
						'locales': [],
						'correction': null,
						'treeNode': null,
						'tags': null
					};
				}

				ChangeDocument.prototype.meta = function(string) {
					var splat = string.split(/\./),
						obj, i;
					obj = this.META$;
					for (i = 0; i < splat.length && obj; i++) {
						obj = obj[splat[i]];
					}
					return obj;
				};

				/**
				 * @ngdoc method
				 * @methodOf ChangeDocument
				 * @name is
				 *
				 * @description
				 * Checks if the document is an instance of a given model name.
				 *
				 * @param {string} modelName The model name to check.
				 *
				 * @returns {boolean}
				 */
				ChangeDocument.prototype.is = function(modelName) {
					return Utils.isModel(this, modelName);
				};

				/**
				 * @ngdoc method
				 * @methodOf ChangeDocument
				 * @name isNew
				 *
				 * @description
				 * Checks if the document is new.
				 *
				 * @returns {boolean}
				 */
				ChangeDocument.prototype.isNew = function() {
					return Utils.isNew(this);
				};

				ChangeDocument.prototype.url = function(name, params) {
					return UrlManager.getUrl(this, params || {}, name || 'edit');
				};

				ChangeDocument.prototype.refUrl = function(name) {
					return UrlManager.getUrl(this, { LCID: this.refLCID }, name || 'edit');
				};

				ChangeDocument.prototype.translateUrl = function(LCID) {
					return UrlManager.getTranslateUrl(this, LCID);
				};

				ChangeDocument.prototype.hasUrl = function(name) {
					return this.url(name) !== 'javascript:;';
				};

				ChangeDocument.prototype.nodeChildrenCount = function() {
					return this.META$.treeNode ? this.META$.treeNode.childrenCount : 0;
				};

				ChangeDocument.prototype.nodeHasChildren = function() {
					return this.nodeChildrenCount() > 0;
				};

				/**
				 * @deprecated since 1.9.0 without replacement.
				 */
				ChangeDocument.prototype.nodeIsEmpty = function() {
					console.warn('ChangeDocument.nodeIsEmpty is deprecated since 1.9.0 without replacement.');
					return this.nodeChildrenCount() === 0;
				};

				/**
				 * @deprecated since 1.9.0 without replacement.
				 */
				ChangeDocument.prototype.isRefLang = function() {
					console.warn('ChangeDocument.isRefLang is deprecated since 1.9.0 without replacement.');
					return this.refLCID === this.LCID;
				};

				/**
				 * @ngdoc method
				 * @methodOf ChangeDocument
				 * @name isLocalized
				 *
				 * @description
				 * Checks if the document is localized.
				 *
				 * @returns {boolean}
				 */
				ChangeDocument.prototype.isLocalized = function() {
					return angular.isDefined(this.refLCID);
				};

				/**
				 * @ngdoc method
				 * @methodOf ChangeDocument
				 * @name isTranslatedIn
				 *
				 * @description
				 * Checks if the document is localized.
				 *
				 * @param {string} LCID The language code.
				 *
				 * @returns {boolean}
				 */
				ChangeDocument.prototype.isTranslatedIn = function(LCID) {
					if (!this.META$.locales) {
						return false;
					}
					var i, translated = false;
					for (i = 0; i < this.META$.locales.length && !translated; i++) {
						translated = (this.META$.locales[i].id === LCID);
					}
					return translated;
				};

				/**
				 * @ngdoc method
				 * @methodOf ChangeDocument
				 * @name hasCorrection
				 *
				 * @description
				 * Checks if the document has currently a correction.
				 *
				 * @returns {boolean}
				 */
				ChangeDocument.prototype.hasCorrection = function() {
					return Utils.hasCorrection(this);
				};

				ChangeDocument.prototype.isActionAvailable = function(actionName) {
					return angular.isObject(this.META$.actions) && this.META$.actions.hasOwnProperty(actionName);
				};

				ChangeDocument.prototype.getActionUrl = function(actionName) {
					if (angular.isObject(this.META$.actions) && this.META$.actions.hasOwnProperty(actionName)) {
						return this.META$.actions[actionName].href;
					}
					return null;
				};

				ChangeDocument.prototype.getLink = function(rel) {
					if (!rel) {
						throw new Error("Argument 'rel' should not be empty.");
					}
					if (angular.isObject(this.META$.links) && this.META$.links.hasOwnProperty(rel)) {
						return this.META$.links[rel].href;
					}
					return null;
				};

				ChangeDocument.prototype.getTagsUrl = function() {
					return this.META$.links['self'] ? this.META$.links['self'].href + '/tags/' : null;
				};

				ChangeDocument.prototype.loadTags = function() {
					var q = $q.defer(), doc = this;

					if (!this.META$.tags) {
						this.META$.tags = [];

						if (doc.getTagsUrl() !== null) {
							$http.get(doc.getTagsUrl(), getHttpConfig(transformResponseCollectionFn)).then(
								function(result) {
									doc.META$.tags.length = 0;
									angular.forEach(result.data.resources, function(r) {
										doc.META$.tags.push(r);
									});
									q.resolve(result.data.resources);
								},
								function (result) {
									q.reject(result);
								}
							);
						}
					}
					else {
						q.resolve(doc.META$.tags);
					}

					return q.promise;
				};

				ChangeDocument.prototype.getTags = function() {
					this.loadTags();
					return this.META$.tags;
				};

				/**
				 * Builds a 'Resource' object with meta information, such as locales and links.
				 * Each Resource has a 'META$' property that holds these information.
				 */
				function buildChangeDocument(data, baseDocument) {

					var chgDoc = baseDocument || new ChangeDocument(),
						properties;

					// TODO FB 2013-03-21: I think this can be optimized :)

					// Response format differs between the 'Collection', 'Document' and 'Tree' resources.

					// Search for the properties of the resource:
					if (angular.isDefined(data.properties)) {
						if (angular.isDefined(data.properties.nodeOrder) && angular.isDefined(data.properties.document)) {
							properties = data.properties.document;
							chgDoc.META$.treeNode = angular.copy(data.properties);
							chgDoc.META$.url = properties.link.href;
							delete chgDoc.META$.treeNode.document;
							forEach(data.links, function(link) {
								if (link.rel === 'self') {
									chgDoc.META$.treeNode.url = link.href;
								}
							});
						}
						else {
							properties = data.properties;
						}
					}
					else if (angular.isDefined(data.model)) {
						properties = data;
					}
					else if (angular.isDefined(data.nodeOrder) && angular.isDefined(data.document)) {
						properties = data.document;
						data.actions = data.document.actions;
						delete properties.actions;

						chgDoc.META$.treeNode = angular.copy(data);
						delete chgDoc.META$.treeNode.document;
						chgDoc.META$.treeNode.url = data.link.href;
					}

					// Parse the 'links' section:
					forEach(data.links, function(link) {
						chgDoc.META$.links[link.rel] = link;
						if (link.rel === 'self' && !chgDoc.META$.url) {
							chgDoc.META$.url = link.href;
						}
						else if (link.rel === 'node') {
							chgDoc.META$.treeNode = angular.extend(
								chgDoc.META$.treeNode || {},
								{
									'url': link.href
								}
							);
						}
						else if (link.rel === 'parent') {
							chgDoc.META$.treeNode = angular.extend(
								chgDoc.META$.treeNode || {},
								{
									'parentUrl': link.href
								}
							);
						}
						else if (link.rel === 'children') {
							chgDoc.META$.treeNode = angular.extend(
								chgDoc.META$.treeNode || {},
								{
									'childrenUrl': link.href
								}
							);
						}
					});

					if (angular.isObject(data.link)) {
						chgDoc.META$.links['self'] = data.link;
					}

					// Parse the 'actions' section:
					forEach(data.actions, function(action) {
						chgDoc.META$.actions[action.rel] = action;
						if (action.rel === 'correction') {
							chgDoc.META$.correction = {};
						}
					});

					// Parse the 'i18n' sections:
					if (data.i18n) {
						chgDoc.META$.links.i18n = data.i18n;
						forEach(data.i18n, function(url, lcid) {
							chgDoc.META$.locales.push({
								'id': lcid,
								'label': lcid,
								'isReference': data.properties.refLCID === lcid
							});
						});
					}

					// Transform sub-documents into ChangeDocument instances.
					var transformSubDocument = function(properties) {
						angular.forEach(properties, function(value, name) {
							if (Utils.isDocument(value)) {
								properties[name] = buildChangeDocument(value);
							}
							else if (Utils.isInlineDocument(value)) {
								transformSubDocument(value);
							}
							else if (angular.isArray(value)) {
								angular.forEach(value, function(v, i) {
									if (Utils.isDocument(value[i])) {
										value[i] = buildChangeDocument(value[i]);
									}
									else if (Utils.isInlineDocument(value[i])) {
										transformSubDocument(value[i]);
									}
								});
							}
						});
					};
					transformSubDocument(properties);

					angular.extend(chgDoc, properties);
					return chgDoc;
				}

				function transformResponseCollectionFn(response) {
					var data = null;
					try {
						data = JSON.parse(response);
						if (angular.isDefined(data.resources)) {
							forEach(data.resources, function(rsc, key) {
								data.resources[key] = buildChangeDocument(rsc);
							});
						}
					}
					catch (e) {
						data = {
							"error": true,
							"code": "InvalidResponse",
							"message": "Got error when parsing response: " + response
						};
					}
					return data;
				}

				function transformResponseResourceFn(response) {
					var data = null;
					try {
						data = JSON.parse(response);
						if (angular.isDefined(data.properties)) {
							data = buildChangeDocument(data);
						}
					}
					catch (e) {
						data = {
							"error": true,
							"code": "InvalidResponse",
							"message": "Got error when parsing response: " + response
						};
					}
					return data;
				}

				/**
				 * Returns the HTTP Config that should be used for every REST call.
				 * Special headers, such as Accept-Language, and authentication stuff go here :)
				 *
				 * @returns {Object}
				 *   - **headers** - `{Object}`
				 *     - **Accept-Language** - `{string}`
				 */
				function getHttpConfig(transformResponseFn) {
					var config = {
						'headers': {
							'Accept-Language': angular.lowercase(language).replace('_', '-')
						}
					};

					if (angular.isFunction(transformResponseFn)) {
						config.transformResponse = transformResponseFn;
					}

					return config;
				}

				/**
				 * Returns the HTTP Config that should be used for every REST call.
				 * Special headers, such as Accept-Language, and authentication stuff go here :)
				 *
				 * @returns {Object}
				 *   - **headers** - `{Object}`
				 *     - **Accept-Language** - `{string}`
				 */
				function getHttpConfigWithCache(transformResponseFn) {
					var config = getHttpConfig(transformResponseFn);
					config.cache = true;
					return config;
				}

				/**
				 * Resolves the given `q` with the given `data`.
				 * This function ensures that the Q is resolved within the Angular life-cycle, without the need to attach
				 * the promise to a Scope.
				 *
				 * @param q
				 * @param data
				 */
				function resolveQ(q, data) {
					if (data === null || (data.code && data.message && !data.id)) {
						q.reject(data);
					}
					else {
						q.resolve(data);
					}
				}

				/**
				 * Rejects the given `q` with the given `reason`.
				 * This function ensures that the Q is rejected within the Angular life-cycle, without the need to attach
				 * the promise to a Scope.
				 *
				 * @param q
				 * @param reason
				 */
				function rejectQ(q, reason) {
					if (angular.isObject(reason)) {
						if (reason.status == 401) {
							if (!reason.code || reason.code == 'EXCEPTION-72000' || reason.code == 'EXCEPTION-72001' ||
								reason.code == 'EXCEPTION-72004') {
								$rootScope.$emit(Events.Logout);
							}
							else if (reason.code == 'EXCEPTION-72005') {
								var seconds = reason.data.delay, messageBody;
								if (seconds > 0) {
									messageBody = i18n.trans('m.rbs.generic.admin.error_invalid_positive_delay|ucf').replace('$SECONDS$', seconds)
								}
								else {
									messageBody = i18n.trans('m.rbs.generic.admin.error_invalid_negative_delay|ucf').replace('$SECONDS$', -seconds)
								}
								NotificationCenter.error(
									i18n.trans('m.rbs.generic.admin.error_invalid_timestamp_title|ucf'), messageBody, reason.code
								);
							}
						}
					}
					q.reject(reason);
				}

				function digest() {
					// In some (rare) cases, Angular does not fire the AJAX request above. This is really
					// strange, and I have to say that I don't know why.
					// Calling a digest cycle on the $rootScope solves the problem...
					if (!$rootScope.$$phase) {
						$rootScope.$apply();
					}
				}

				function _ToSlash(string) {
					return string.replace(/_/g, '/');
				}

				$rootScope.$on('$routeChangeStart', function() {
					DocumentCache.removeAll();
				});

				// Public API of the REST service.

				REST = {
					getHttpConfig: function(transformer) {
						return getHttpConfig(transformer);
					},

					transformObjectToChangeDocument: function(object) {
						return angular.extend(new ChangeDocument(), object);
					},

					collectionTransformer: function() {
						return transformResponseCollectionFn;
					},

					resourceTransformer: function() {
						return transformResponseResourceFn;
					},

					isLastCreated: function(doc) {
						return lastCreatedDocument && doc &&
							(doc.id === lastCreatedDocument.id && doc.model === lastCreatedDocument.model);
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name getBaseUrl
					 *
					 * @description
					 * Returns the full URL of an action based on its `relativePath`, suitable to use with `$http`.
					 *
					 * @param {string} relativePath Relative path.
					 * @returns {string} Full path to use with `$http`.
					 */
					getBaseUrl: function(relativePath) {
						var relativeParts = relativePath.split('/');
						for (var i = 0; i < relativeParts.length; i++) {
							relativeParts[i] = encodeURIComponent(relativeParts[i]);
						}
						return REST_BASE_URL + relativeParts.join('/');
					},

					/**
					 * @param lang
					 */
					setLanguage: function(lang) {
						language = lang;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name getAvailableLanguages
					 *
					 * @description
					 * Returns the list of all available languages.
					 *
					 * @returns {Promise} Promise resolved when the list of languages is loaded.
					 */
					getAvailableLanguages: function() {
						return this.action(
							'collectionItems',
							{ 'code': 'Rbs_Generic_Collection_Languages' },
							true // cache
						);
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name getResourceUrl
					 *
					 * @description
					 * Returns the URL of the resource identified by its `model`, `id` and optional `lcid`.
					 *
					 * @param {string|number|ChangeDocument} model The Document Model name or the Document's ID or the ChangeDocument.
					 * @param {number=} id The Document's ID.
					 * @param {string=} lcid The Document's locale ID.
					 *
					 * @return {string} The resource's URL.
					 */
					getResourceUrl: function(model, id, lcid) {
						var url;

						if (/^[0-9]+$/.test(model)) {
							url = REST_BASE_URL + 'resources/' + model;
						}
						else {
							if (Utils.isDocument(model)) {
								id = model.id;
								lcid = model.LCID;
								model = model.model;
							}

							// Resulting URL will end with a slash.
							url = this.getCollectionUrl(model, null);

							if (id) {
								url += id;
								if (lcid) {
									url += '/' + lcid;
								}
							}
						}

						return url;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name getJobUrl
					 *
					 * @description
					 * Returns the URL of the Job identified by `id`.
					 *
					 * @param {number} id The Job's ID.
					 *
					 * @return {string} The resource's URL.
					 */
					getJobUrl: function(id) {
						return REST_BASE_URL + 'jobs/' + id;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name getCollectionUrl
					 *
					 * @description
					 * Returns the URL of the collection for the given `model` and optional `params`.
					 *
					 * @param {string} model Model name.
					 * @param {Object} params Parameters (limit, offset, sort, ...)
					 *
					 * @return {string} The collection's URL.
					 */
					getCollectionUrl: function(model, params) {
						model = Utils.modelInfo(model);
						return Utils.makeUrl(
							REST_BASE_URL + 'resources/' + model.vendor + '/' + model.module + '/' + model.document + '/',
							params
						);
					},

					/**
					 * Returns unique ID for newly created resources.
					 * These IDs are negative integers.
					 */
					getTemporaryId: function() {
						temporaryId--;
						localStorageService.set('temporaryId', temporaryId);
						return temporaryId;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name newResource
					 *
					 * @description
					 * Creates a new, unsaved resource of the given `model` in the given locale (`lcid`).
					 *
					 * @param {string} model The Document Model name.
					 * @param {string=} lcid Locale ID (5 chars).
					 *
					 * @return {ChangeDocument} The new unsaved Document.
					 */
					newResource: function(model, lcid) {
						var props = {
							'id': REST.getTemporaryId(),
							'model': model,
							'publicationStatus': 'DRAFT'
						};
						if (Utils.isValidLCID(lcid)) {
							props.refLCID = lcid;
							props.LCID = lcid;
						}

						return buildChangeDocument({
							'properties': props
						});
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name resource
					 *
					 * @description
					 * Loads the Resource identified by its `model`, `id` and optional `lcid`.
					 *
					 * @param {string|number|ChangeDocument} model Document Model name or the Resource's ID or Document object.
					 * @param {number=} id Resource's ID.
					 * @param {string=} lcid (Optional) Locale ID.
					 *
					 * @return {Promise} Promise that will be resolved with the Document when the Resource is loaded.
					 */
					resource: function(model, id, lcid) {
						var q = $q.defer(), self = this, httpConfig = getHttpConfig(transformResponseResourceFn);
						httpConfig.cache = DocumentCache;

						$http.get(this.getResourceUrl(model, id, lcid), httpConfig).then(
							function(result) {
								if (Utils.hasCorrection(result.data)) {
									self.loadCorrection(result.data).then(
										function(doc) {
											doc.META$.loaded = true;
											resolveQ(q, doc);
										},
										function(result) {
											rejectQ(q, result.data);
										}
									);
								}
								else {
									result.data.META$.loaded = true;
									resolveQ(q, result.data);
								}
							},
							function(result) {
								if (result.status === 303 && Utils.isDocument(result.data)) {
									resolveQ(q, result.data);
								}
								else {
									if (result.data) {
										result.data.httpStatus = result.status;
									}
									rejectQ(q, result.data);
								}
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name job
					 *
					 * @description
					 * Loads the Job identified by its `id`.
					 *
					 * @param {number=} id Job's ID.
					 *
					 * @return {Promise} Promise that will be resolved with the Job when the Resource is loaded.
					 */
					job: function(id) {
						var q = $q.defer();
						var httpConfig = getHttpConfig(function(response) {
							var data = null;
							try {
								data = JSON.parse(response);
							}
							catch (e) {
								data = {
									error: true,
									code: 'InvalidResponse',
									message: 'Got error when parsing response: ' + response
								};
							}
							return data;
						});

						$http.get(this.getJobUrl(id), httpConfig).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								if (result.data) {
									result.data.httpStatus = result.status;
								}
								rejectQ(q, result.data);
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name resources
					 *
					 * @description
					 * Loads the Resources identified by the given `ids`.
					 *
					 * @param {Array} ids Array of Document IDs.
					 *
					 * @return {Promise} Promise that will be resolved with the Documents when the Resources are loaded.
					 */
					resources: function(ids) {
						var q = $q.defer(),
							url = Utils.makeUrl(this.getBaseUrl('admin/documentList'), { 'ids': ids });

						$http.get(url, getHttpConfig(transformResponseCollectionFn)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								rejectQ(q, result.data);
							}
						);

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name getResources
					 *
					 * @description
					 * Returns an Array with empty Document objects, and loads the Resources identified by the given `ids`.
					 * When the Resources are loaded, the returned Array is populated with the loaded Documents.
					 *
					 * @param {Array} ids Array of Document IDs.
					 *
					 * @return {Array} Array of empty Document objects, populated when the Documents are loaded.
					 */
					getResources: function(ids) {
						var docs = [], i;
						for (i = 0; i < ids.length; i++) {
							docs.push({ id: ids[i], model: '', label: '[' + ids[i] + ']' });
						}
						this.resources(ids).then(
							function(collection) {
								var i;
								for (i = 0; i < collection.resources.length; i++) {
									angular.extend(docs[i], collection.resources[i]);
								}
								if (collection.resources.length < docs.length) {
									docs.splice(collection.resources.length);
								}
							},
							function(result) { console.error(result); }
						);
						digest();

						return docs;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name searchDocuments
					 *
					 * @description
					 * Look for documents of a given model and matching a search string.
					 *
					 * @param {string} modelName Document Model name.
					 * @param {string} searchString String to search.
					 * @param {number} limit The maximum result count.
					 * @param {Array=} extraColumns A list of extra columns.
					 *
					 * @return {Promise} Promise that will be resolved with the Documents when the documents are loaded.
					 */
					searchDocuments: function(modelName, searchString, limit, extraColumns) {
						var q = $q.defer();
						var params = {
							modelName: modelName,
							searchString: searchString,
							limit: limit
						};
						if (angular.isArray(extraColumns) && extraColumns.length) {
							params.extraColumns = extraColumns;
						}

						var url = Utils.makeUrl(this.getBaseUrl('admin/searchDocuments'), params);
						$http.get(url, getHttpConfig(transformResponseCollectionFn)).then(
							function(result) {
								var docs = [];
								for (var i = 0; i < result.data.resources.length; i++) {
									docs.push(buildChangeDocument(result.data.resources[i]));
								}
								resolveQ(q, docs);
							},
							function(result) {
								rejectQ(q, result.data);
							}
						);

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name ensureLoaded
					 *
					 * @description
					 * Ensures that the given Document has been fully loaded.
					 *
					 * @param {string|ChangeDocument} model Document Model name, or Document object.
					 * @param {number=} id Resource's ID.
					 * @param {string=} lcid (Optional) Locale ID.
					 *
					 * @return {Promise} Promise that will be resolved with the Document when the Resource is loaded.
					 */
					ensureLoaded: function(model, id, lcid) {
						if (this.isFullyLoaded(model)) {
							var q = $q.defer();
							resolveQ(q, model);
							return q.promise;
						}
						return this.resource(model, id, lcid);
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name isFullyLoaded
					 *
					 * @description
					 * Tells whether the given `doc` has already been fully loaded or not.
					 *
					 * @param {ChangeDocument} doc Document object.
					 * @returns {boolean} True if `doc` has been fully loaded.
					 */
					isFullyLoaded: function(doc) {
						return Utils.isDocument(doc) && doc.META$.loaded === true;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name collection
					 *
					 * @description
					 * Loads a collection via a 'GET' REST call.
					 *
					 * @param {string} model Model name or URL of a RESTful service that returns a Collection.
					 * @param {Object} params Parameters (limit, offset, sort, ...)
					 *
					 * @returns {Promise} Promise that will be resolved when the collection is loaded.
					 * The Promise is resolved with the whole response as argument.
					 */
					collection: function(model, params) {
						var q = $q.defer(), url;
						if (angular.isObject(params) && angular.isObject(params.filter)) {
							if (Utils.isModelName(model)) {
								url = this.getCollectionUrl(model, {});
							}
							else {
								if (model.charAt(0) === '/') {
									url = Utils.makeUrl(REST_BASE_URL + model.substr(1), {});
								}
								else {
									url = Utils.makeUrl(model, {});
								}
							}
							$http.post(url + 'filtered/', params, getHttpConfig(transformResponseCollectionFn)).then(
								function(result) {
									resolveQ(q, result.data);
								},
								function(result) {
									rejectQ(q, result.data);
								}
							);
						}
						else {
							if (Utils.isModelName(model)) {
								url = this.getCollectionUrl(model, params);
							}
							else {
								if (model.charAt(0) === '/') {
									url = Utils.makeUrl(REST_BASE_URL + model.substr(1), params);
								}
								else {
									url = Utils.makeUrl(model, params);
								}
							}
							$http.get(url, getHttpConfig(transformResponseCollectionFn)).then(
								function(result) {
									resolveQ(q, result.data);
								},
								function(result) {
									rejectQ(q, result.data);
								}
							);
						}
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name loadCorrection
					 *
					 * @description
					 * Loads the Correction for the given `resource` (Document).
					 *
					 * @param {ChangeDocument} resource The Document.
					 *
					 * @returns {Promise} Promise that will be resolved when the Correction has been applied on the given `resource`.
					 */
					loadCorrection: function(resource) {
						var q = $q.defer();
						if (Utils.hasCorrection(resource)) {
							$http.get(resource.META$.actions['correction'].href, getHttpConfig()).then(
								function(result) {
									Utils.applyCorrection(resource, result.data);
									resolveQ(q, resource);
								},
								function(result) {
									rejectQ(q, result.data);
								}
							);
						}
						else {
							rejectQ(q, 'No correction available on the given Document');
						}
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name save
					 *
					 * @description
					 * Saves the given `resource` via a 'POST' (creation) or 'PUT' (update) REST call.
					 *
					 * @param {ChangeDocument} resource The Document to be saved.
					 * @param {ChangeDocument=} currentTreeNode Current Document that should be used as the parent in the tree.
					 * @param {Array=} propertiesList Array of properties to save.
					 *
					 * @return {Promise} Promise that will be resolved when the Document is successfully saved.
					 * Promise is resolved with the saved Document as argument.
					 */
					save: function(resource, currentTreeNode, propertiesList) {
						var mainQ = $q.defer(),
							url,
							method,
							REST = this;

						// mainQ is the Promise that will be resolved when all the "actions" (correction, tree, ...)
						// have been called successfully.

						// Make a copy of the resource object and remove unwanted properties (META$).
						resource = angular.copy(resource);

						if (Utils.isNew(resource)) {
							// If resource is new (see isNew()), we must POST on the Collection's URL.
							method = 'post';
							// Remove temporary ID
							delete resource.id;
							url = this.getCollectionUrl(resource.model, {});
						}
						else {
							DocumentCache.removeAll();
							// If resource is NOT new (already been saved), we must PUT on the Resource's URL.
							method = 'put';
							url = this.getResourceUrl(resource);
							// Save only the properties listed here + the properties of the Correction (if any).
							if (angular.isArray(propertiesList)) {
								if (Utils.hasCorrection(resource)) {
									angular.forEach(resource.META$.correction.propertiesNames, function(propName) {
										if (propertiesList.indexOf(propName) === -1) {
											propertiesList.push(propName);
										}
									});
								}
								var toSave = {};
								angular.forEach(propertiesList, function(prop) {
									if (resource.hasOwnProperty(prop)) {
										toSave[prop] = resource[prop];
									}
								});
								resource = toSave;
							}
						}

						delete resource.META$;

						// For child-documents, only send the ID.
						angular.forEach(resource, function(value, name) {
							if (Utils.isDocument(value)) {
								resource[name] = value.id;
							}
						});

						// REST call:
						$http[method](url, resource, getHttpConfig(transformResponseResourceFn)).then(
							// Save SUCCESS:
							// 1) a ChangeDocument instance is created via the response interceptor,
							// 2) load its Correction (if any),
							// 3) insert resource in tree (if needed).
							function(result) {
								var doc = result.data;

								// 1) "doc" is a ChangeDocument instance.
								function maybeInsertResourceInTree(resource, qToResolve) {
									if (result.status === HTTP_STATUS_CREATED) {
										lastCreatedDocument = resource;
									}

									if (!Utils.isTreeNode(resource) &&
										(result.status === HTTP_STATUS_CREATED || resource.treeName === null) && currentTreeNode) {
										// Load model's information to check if the document should be inserted in a tree.
										REST.modelInfo(resource).then(
											// modelInfo success
											function(modelInfo) {
												if (!modelInfo.metas || !modelInfo.metas.treeName) {
													resolveQ(qToResolve, resource);
												}
												else {
													$http.post(currentTreeNode.META$.treeNode.url + '/', { "id": doc.id }, getHttpConfig()).then(
														function() {
															doc = buildChangeDocument(doc, resource);
															if (result.status === HTTP_STATUS_CREATED) {
																lastCreatedDocument = doc;
															}
															resolveQ(qToResolve, doc);
														},
														function(result) {
															result.data.httpStatus = result.status;
															rejectQ(qToResolve, result.data);
														}
													);
												}
											},

											// modelInfo error
											function(data) {
												rejectQ(qToResolve, data);
											}
										);
									}
									else {
										resolveQ(qToResolve, resource);
									}
								}

								// 2) load its Correction (if any)
								// After being saved, a Document may have a Correction attached to it, especially
								// if it was PUBLISHED on the website.
								if (Utils.hasCorrection(doc)) {
									REST.loadCorrection(doc).then(
										function(doc) {
											maybeInsertResourceInTree(doc, mainQ);
										},
										function(result) { console.error(result); }
									);
								}
								else {
									// 3) insert resource in tree (if needed)
									maybeInsertResourceInTree(doc, mainQ);
								}

							},
							// Save ERROR: reject main promise (mainQ).
							function errorCallback(result) {
								result.data.httpStatus = result.status;
								rejectQ(mainQ, result.data);
							}
						);

						return mainQ.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name delete
					 *
					 * @description
					 * Deletes the given `resource` via a <em>DELETE</em> REST call.
					 *
					 * @param {ChangeDocument} resource The Document to be deleted.
					 *
					 * @return {Promise} Promise that will be resolved when the Document is successfully deleted.
					 * Promise is resolved with the deleted Resource as argument.
					 */
					delete: function(resource) {
						var q = $q.defer();

						$http['delete'](this.getResourceUrl(resource.model, resource.id), getHttpConfig()).then(
							function(result) {
								// When deleting a resource, the response's body is empty with a 204.
								// So don't expect anything in the Promise's argument...
								resolveQ(q, result.data);
							},
							function(result) {
								result.data.httpStatus = result.status;
								rejectQ(q, result.data);
							}
						);

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name preview
					 *
					 * @description
					 * Preview the given `resource` in the given context and with the specified property values.
					 *
					 * @param {ChangeDocument} resource The Document to be saved.
					 * @param {Object} context Current Document that should be used as the parent in the tree.
					 * @param {boolean} applyModifications If set to true, apply the current modifications.
					 *
					 * @return {Promise} Promise that will be resolved when the Document is successfully previewed.
					 * Promise is resolved with the previewed Document as argument.
					 */
					preview: function(resource, context, applyModifications) {
						var url = this.getResourceUrl(resource) + '/preview';

						var propertyValues = {};
						if (applyModifications) {
							angular.forEach(resource, function(value, name) {
								if (name !== 'META$' && name != 'model' && name != 'LCID' && name != 'refLCID' && name != 'id') {
									propertyValues[name] = value;
								}
							});

							// For child-documents, only send the ID.
							angular.forEach(propertyValues, function(value, name) {
								if (Utils.isDocument(value)) {
									propertyValues[name] = value.id;
								}
							});
						}
						context['properties'] = propertyValues;

						var q = $q.defer();

						$http.post(url, context, getHttpConfig()).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								result.data.httpStatus = result.status;
								rejectQ(q, result.data);
							}
						);
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name executeTaskByCodeOnDocument
					 *
					 * @description
					 * Execute a Task identified by its code on the given Document, with optional parameters.
					 *
					 * @param {string} taskCode The Task's code.
					 * @param {ChangeDocument} doc The Document.
					 * @param {Object=} params Parameters.
					 *
					 * @returns {Promise} Promise resolved when the Task has been completed successfully.
					 */
					executeTaskByCodeOnDocument: function(taskCode, doc, params) {
						var q = $q.defer(),
							rest = this;

						if (!Utils.isDocument(doc)) {
							throw new Error("Parameter 'resource' should be a valid Document.");
						}

						if (!doc.META$.actions || !doc.META$.actions.hasOwnProperty(taskCode)) {
							q.reject("Action '" + taskCode + "' is not available for Document '" + doc.id + "'.");
						}
						else {
							DocumentCache.removeAll();
							// Load the Task Document
							// TODO Optimize: can we call 'execute' directly with '/execute' at the end of the Task URL?
							$http.get(doc.META$.actions[taskCode].href, getHttpConfig(transformResponseResourceFn)).then(
								function(result) {
									// Execute Task.
									rest.executeTask(result.data, params).then(
										// Success
										function() {
											// Task has been executed and we don't need it here anymore.
											rest.resource(doc).then(
												function(updatedDoc) {
													resolveQ(q, updatedDoc);
												},
												function(result) {
													console.error(result);
													rejectQ(q, result.data);
												}
											);
										},
										// Error
										function(result) {
											rejectQ(q, result.data);
										}
									);
								},
								function(result) {
									rejectQ(q, result.data);
								}
							);
						}

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name executeTask
					 *
					 * @description
					 * Execute a Task with optional parameters.
					 *
					 * @param {ChangeDocument} task The Task Document.
					 * @param {Object=} params Parameters.
					 *
					 * @returns {Promise} Promise resolved when the Task has been completed successfully.
					 */
					executeTask: function(task, params) {
						var q = $q.defer();

						function doExecute(taskObj) {
							if (taskObj.META$.actions['execute']) {
								$http.post(taskObj.META$.actions['execute'].href, params, getHttpConfig(transformResponseResourceFn)).then(
									function(result) {
										angular.extend(task, result.data);
										resolveQ(q, result.data);
									},
									function(result) {
										rejectQ(q, result.data);
									}
								);
							}
							else {
								rejectQ(q, 'Could not execute task ' + taskObj.id);
							}
						}

						if (task.META$.actions['execute']) {
							doExecute(task);
						}
						else {
							q = $q.defer();
							this.resource(task).then(
								// Success
								doExecute,
								// Error
								function(result) {
									rejectQ(q, result);
								}
							);
						}

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name treeUrl
					 *
					 * @description
					 * Returns the URL of a tree from its `treeName`.
					 *
					 * @param {string} treeName The Tree name.
					 *
					 * @returns {string} REST URL to load the resources of tree `treeName`.
					 */
					treeUrl: function(treeName) {
						return REST_BASE_URL + 'resourcestree/' + _ToSlash(treeName) + '/';
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name treeChildren
					 *
					 * @description
					 * Loads the tree children of the given `resource`.
					 *
					 * @param {ChangeDocument|string} resource Parent Document in a tree.
					 * @param {Object=} params Parameters.
					 *
					 * @returns {Promise} Promise resolved with the children when they are loaded.
					 */
					treeChildren: function(resource, params) {
						var q = $q.defer(),
							url;

						if (angular.isString(resource)) {
							url = this.treeUrl(resource);
						}
						else if (angular.isObject(resource) && resource.META$ && resource.META$.treeNode &&
							resource.META$.treeNode.url) {
							url = resource.META$.treeNode.url + '/';
						}

						if (url) {
							url = Utils.makeUrl(url, params);
							$http.get(url, getHttpConfig(transformResponseCollectionFn)).then(
								function(result) {
									resolveQ(q, result.data);
								},
								function(result) {
									rejectQ(q, result.data);
								}
							);
						}
						else {
							resolveQ(q, null);
						}

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name treeNode
					 *
					 * @description
					 * Loads the TreeNode object of the given `resource`.
					 *
					 * @param {ChangeDocument} resource The Document.
					 *
					 * @returns {Promise} Promise resolved when the TreeNode is loaded.
					 */
					treeNode: function(resource) {
						var q = $q.defer(),
							url;

						if (angular.isString(resource)) {
							url = resource;
						}
						else if (angular.isObject(resource) && resource.META$ && resource.META$.treeNode &&
							resource.META$.treeNode.url) {
							url = resource.META$.treeNode.url;// + '/';
						}
						else {
							throw new Error("'resource' parameter should be a TreeNode URL or a ChangeDocument.");
						}

						$http.get(url, getHttpConfig(transformResponseResourceFn)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								rejectQ(q, result.data);
							}
						);

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name modelInfo
					 *
					 * @description
					 * Returns information about a Document Model from its `modelName`.
					 *
					 * @param {string} modelName The Document Model full name.
					 *
					 * @returns {Promise} Promise resolved with the Model's information.
					 */
					modelInfo: function(modelName) {
						var q = $q.defer();

						if (Utils.isDocument(modelName)) {
							modelName = modelName.model;
						}

						// TODO Use UI language
						$http.get(REST_BASE_URL + 'models/' + _ToSlash(modelName), getHttpConfigWithCache()).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								rejectQ(q, result.data);
							}
						);

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name query
					 *
					 * @description
					 * Sends a query to search for documents. Query is sent via a POST call.
					 *
					 * Query objects can be built with the {@link RbsChange.service:Query Query service}.
					 *
					 * @param {Object} queryObject The Query object.
					 * @param {Object=} params Parameters.
					 *
					 * @returns {Promise} Promise resolved with a collection of documents that match the filters.
					 */
					query: function(queryObject, params) {
						var q = $q.defer();

						$http.post(Utils.makeUrl(REST_BASE_URL + 'query/', params), queryObject, getHttpConfig(transformResponseCollectionFn)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								result.data.httpStatus = result.status;
								rejectQ(q, result.data);
							}
						);

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name action
					 *
					 * @description
					 * Calls the action `actionName` with the given `params` (HTTP GET).
					 *
					 * Use {@link RbsChange.service:REST#postAction `postAction()`} to send the action with an HTTP POST.
					 *
					 * @param {string} actionName The action's name.
					 * @param {Object=} params Parameters.
					 * @param {boolean=} cache If true, the result is cached by AngularJS.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					action: function(actionName, params, cache) {
						var q = $q.defer(),
							url;

						url = Utils.makeUrl(REST_BASE_URL + 'actions/' + actionName + '/', params);

						$http.get(url, cache === true ? getHttpConfigWithCache() : getHttpConfig()).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								result.data.httpStatus = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name postAction
					 *
					 * @description
					 * Calls the action `actionName` with the given `params` (HTTP POST).
					 *
					 * Same as {@link RbsChange.service:REST#action `action()`}, but with an HTTP POST.
					 *
					 * @param {string} actionName The action's name.
					 * @param {*} content The request content.
					 * @param {Object=} params Parameters.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					postAction: function(actionName, content, params) {
						var q = $q.defer(),
							url;

						url = Utils.makeUrl(REST_BASE_URL + 'actions/' + actionName + '/', params);

						$http.post(url, content, getHttpConfig()).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								result.data.httpStatus = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();

						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name call
					 *
					 * @description
					 * Calls a REST service with its full URL.
					 *
					 * @param {string} url Full URL
					 * @param {Object=} urlParams Parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					call: function(url, urlParams, transformer) {
						var q = $q.defer();

						$http.get(Utils.makeUrl(url, urlParams), getHttpConfig(transformer)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								if (!result.data) {
									result.data = {};
								}
								result.data.status = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name apiGet
					 *
					 * @description
					 * Calls a REST service with its full URL.
					 *
					 * @param {string} relativePath Relative path.
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					apiGet: function(relativePath, params, transformer) {
						var url = this.getBaseUrl(relativePath);
						if (angular.isObject(params)) {
							url = Utils.makeUrl(url, params);
						}

						var q = $q.defer();
						$http.get(url, getHttpConfig(transformer)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								if (!result.data) {
									result.data = {};
								}
								result.data.status = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name apiPost
					 *
					 * @description
					 * POST on a REST service with its full URL.
					 *
					 * @param {string} relativePath Relative path.
					 * @param {Object=} data Request content.
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					apiPost: function(relativePath, data, params, transformer) {
						var url = this.getBaseUrl(relativePath);
						if (angular.isObject(params)) {
							url = Utils.makeUrl(url, params);
						}

						var q = $q.defer();
						$http.post(url, data, getHttpConfig(transformer)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								if (!result.data) {
									result.data = {};
								}
								result.data.status = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name apiPut
					 *
					 * @description
					 * PUT on a REST service with its full URL.
					 *
					 * @param {string} relativePath Relative path.
					 * @param {Object=} data Request content.
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					apiPut: function(relativePath, data, params, transformer) {
						var url = this.getBaseUrl(relativePath);
						if (angular.isObject(params)) {
							url = Utils.makeUrl(url, params);
						}

						var q = $q.defer();
						$http.put(url, data, getHttpConfig(transformer)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								if (!result.data) {
									result.data = {};
								}
								result.data.status = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @ngdoc method
					 * @methodOf REST
					 * @name apiDelete
					 *
					 * @description
					 * DELETE on a REST service with its full URL.
					 *
					 * @param {string} relativePath Relative path.
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {Promise} Promise resolved with the action's result.
					 */
					apiDelete: function(relativePath, params, transformer) {
						var url = this.getBaseUrl(relativePath);
						if (angular.isObject(params)) {
							url = Utils.makeUrl(url, params);
						}

						var q = $q.defer();
						$http.delete(url, getHttpConfig(transformer)).then(
							function(result) {
								resolveQ(q, result.data);
							},
							function(result) {
								if (!result.data) {
									result.data = {};
								}
								result.data.status = result.status;
								rejectQ(q, result.data);
							}
						);

						digest();
						return q.promise;
					},

					/**
					 * @deprecated since 1.9.0 without replacement.
					 */
					treeAncestors: function() {
						console.warn('RsChange.REST.treeAncestors is deprecated since 1.9.0 without replacement.');

					},

					/**
					 * @deprecated since 1.9.0 without replacement.
					 */
					getLastCreated: function() {
						console.warn('RsChange.REST.getLastCreated is deprecated since 1.9.0 without replacement.');
						return lastCreatedDocument;
					},

					/**
					 * @deprecated since 1.9.0, use RbsChange.Block service.
					 */
					blocks: function() {
						console.warn('RsChange.REST.blocks is deprecated since 1.9.0, use RbsChange.Block service.');
					},

					/**
					 * @deprecated since 1.9.0 without replacement.
					 */
					blockInfo: function(blockName) {
						console.warn('RsChange.REST.blockInfo is deprecated since 1.9.0 without replacement.');
					},

					//
					// Storage
					//

					/**
					 * @ngdoc service
					 * @id RbsChange.service:REST.storage
					 * @name REST.storage
					 *
					 * @description
					 * Sub-service of {@link RbsChange.service:REST REST} that provides methods to deal with file uploads.
					 *
					 * Inject {@link RbsChange.service:REST REST service} to use it:
					 * <code>REST.storage.method(...)</code>
					 */
					storage: {
						/**
						 * @ngdoc method
						 * @methodOf REST.storage
						 * @name upload
						 *
						 * @description
						 * Uploads a file on the server.
						 *
						 * @param {DOMElement} fileElm The input[file] element.
						 * @param {string} storageName Name of the storage configuration on the server.
						 *
						 * @returns {Promise} Promise resolved when the file has been uploaded.
						 */
						upload: function(fileElm, storageName) {
							var q = $q.defer(),
								formData = new FormData();

							fileElm = fileElm.get(0);
							if (!fileElm.files) {
								window.alert('Browser not compatible');
								rejectQ(q, 'Browser not compatible');
								digest();
							}

							// Using the HTML5's File API:
							// https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/FormData
							if (fileElm.files.length > 0) {
								if (!storageName) {
									storageName = 'tmp';
								}
								formData.append('file', fileElm.files[0]);

								window.jQuery.ajax({
									url: REST_BASE_URL + 'storage/' + storageName + '/',
									type: 'POST',
									method: 'POST',
									data: formData,
									processData: false,  // tell jQuery not to process the data,
									contentType: false,  // tell jQuery not to change the ContentType,

									success: function(data) {
										resolveQ(q, data);
										digest();
									},

									error: function(jqXHR, textStatus, errorThrown) {
										var error;
										try {
											error = JSON.parse(jqXHR.responseText);
										}
										catch (e) {
											error = {
												code: errorThrown || 'UPLOAD-ERROR',
												message: 'Could not upload file: ' + jqXHR.responseText
											};
											if (jqXHR.responseText) {
												error.message = 'Could not upload file: ' + jqXHR.responseText;
											}
											else {
												error.message = 'Could not upload file.';
											}
										}
										rejectQ(q, error);
										digest();
									}
								});
							}
							else {
								window.alert('Please select a file');
								rejectQ(q, 'No file');
								digest();
							}

							return q.promise;
						},

						/**
						 * @ngdoc method
						 * @methodOf REST.storage
						 * @name info
						 *
						 * @description
						 * Returns information about the given storage element.
						 *
						 * @param {string} storagePath Storage Path.
						 *
						 * @returns {Promise} Promise resolved with storage element's information.
						 */
						info: function(storagePath) {
							var q = $q.defer();

							if (Utils.startsWith(storagePath, "change://")) {
								$http.get(REST_BASE_URL + 'storage/' + storagePath.substr(9), getHttpConfig()).then(
									function(result) {
										result.data.fileName = storagePath.substr(9);
										resolveQ(q, result.data);
									},
									function(result) {
										rejectQ(q, result.data);
									}
								);
							}
							else {
								rejectQ(q, "'storagePath' should begin with 'change://'.");
							}
							return q.promise;
						}
					}
				};
				return REST;
			}
		];
	});
})();