(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentFilterPanel',
		['$http', 'localStorageService', 'RbsChange.REST', 'RbsChange.Utils',
			'RbsChange.NotificationCenter', 'RbsChange.i18n', 'RbsChange.Navigation', '$rootScope',
			function($http, localStorageService, REST, Utils, NotificationCenter, i18n, Navigation, $rootScope) {
				var searchIndex = 0;
				return {
					restrict: 'E',
					templateUrl: 'Rbs/Admin/js/directives/document-filter-panel.twig',
					require: 'rbsDocumentFilterPanel',
					controller: ['$scope', '$attrs', function(scope, attrs) {
						var model = attrs.model;
						scope.model = model;
						if (model && !scope.modelInfo) {
							REST.modelInfo(model).then(
								function(modelInfo) {
									scope.modelInfo = modelInfo;
									scope.filtersDefinition = modelInfo.metas.filtersDefinition;
								},
								function(result) { console.error(result); }
							);
						}

						if (!angular.isObject(scope.filter) || !scope.filter.name) {
							scope.filter = { name: 'group', parameters: { operator: 'AND' }, filters: [], search: 0 };
						}

						function getStoredFilter() {
							var local = localStorageService.get('filter_' + model);
							if (local) {
								return angular.fromJson(local);
							}
							return null;
						}

						function removeStoredFilter() {
							localStorageService.remove('filter_' + model);
						}

						function saveStoredFilter(filter) {
							if (!filter || !filter.filters || filter.filters.length === 0) {
								removeStoredFilter();
							}
							else {
								localStorageService.set('filter_' + model, filter);
							}
						}

						function initializeFilter() {
							var currentContext = Navigation.getCurrentContext();
							if (currentContext && model) {
								var data = currentContext.savedData('filter_' + model);
								if (angular.isObject(data) && data.filter) {
									scope.currentContext = currentContext;
									scope.filter = angular.copy(data.filter);
									return;
								}
							}

							var loadedFilter = getStoredFilter();
							if (loadedFilter && loadedFilter.name == 'group') {
								scope.filter = loadedFilter;
								return;
							}

							if (scope.defaultFilter && scope.defaultFilter.name == 'group') {
								scope.filter = angular.copy(scope.defaultFilter);
							}
						}

						initializeFilter();

						scope.showFilter = (attrs.openByDefault == 'true' || (scope.filter && scope.filter.filters && scope.filter.filters.length));

						scope.savedFilters = [];

						function loadFilters() {
							var url = Utils.makeUrl(REST.getBaseUrl('actions/filters/'), { model: model });
							var p = $http.get(url);
							p.then(
								function(result) {
									scope.savedFilters = result.data;
								},
								function(result) { console.error(result); }
							);
							return p;
						}

						loadFilters();

						this.resetFilter = function() {
							if (scope.defaultFilter && scope.defaultFilter.name == 'group') {
								scope.filter = angular.copy(scope.defaultFilter);
							}
							else {
								scope.filter = {
									name: 'group', parameters: { operator: 'AND' }, filters: [], search: searchIndex
								};
							}
							removeStoredFilter();
							$rootScope.$broadcast('filterUpdated', scope.filter);
						};

						this.applyFilter = function() {
							saveStoredFilter(scope.filter);
							$rootScope.$broadcast('filterUpdated', scope.filter);
						};

						this.loadFilters = function() {
							return loadFilters();
						}
					}],

					link: function(scope, element, attrs, panelController) {
						var model = attrs.model;
						if (!model) {
							scope.model = null;
							scope.modelInfo = null;
							element.hide();
							return;
						}

						scope.$on('Navigation.saveContext', function(event, args) {
							if (scope.modelInfo && scope.modelInfo.metas) {
								var label = scope.modelInfo.metas.label;
								args.context.label(label);
								var data = { filter: scope.filter };
								args.context.savedData('filter_' + scope.modelInfo.metas.name, data);
							}
						});

						scope.closeFilterPanel = function() {
							scope.showFilter = false;
						};

						scope.openFilterPanel = function() {
							scope.showFilter = true;
						};

						scope.toggleFilterPanel = function() {
							if (scope.showFilter) {
								scope.closeFilterPanel();
							}
							else {
								scope.openFilterPanel();
							}
						};

						scope.resetFilter = function() {
							panelController.resetFilter();
						};

						scope.applyFilter = function() {
							panelController.applyFilter();
						};

						scope.$watchCollection('filter.filters', function(filters) {
							if (!scope.showFilter && angular.isArray(filters) && filters.length) {
								scope.showFilter = true;
							}
						});

						scope.showFilterList = false;
						scope.existingFilterInUse = null;

						scope.useExistingFilter = function(f) {
							if (f && f.content) {
								scope.filter = angular.copy(f.content);
								scope.existingFilterInUse = f;
								scope.applyFilter();
							}
							else {
								scope.existingFilterInUse = null;
							}
						};

						scope.createFilter = function() {
							var url = REST.getBaseUrl('actions/filters/'),
								label = window.prompt(i18n.trans('m.rbs.admin.admin.enter_filter_title'));

							if (label) {
								scope.creatingFilter = true;
								$http.post(url, { 'model_name': model, 'content': angular.fromJson(scope.filter), 'label': label }).then(
									function(result) {
										panelController.loadFilters().then(
											function() {
												scope.creatingFilter = false;
												scope.useExistingFilter(result.data);
											},
											function(result) { console.error(result); }
										);
									},
									function(result) {
										scope.creatingFilter = false;
										var title = i18n.trans('m.rbs.admin.admin.filter_create_error');
										NotificationCenter.error(title, result.data.message, 'rbs_filter_create_error');
									}
								);
							}
						};

						scope.updateExistingFilter = function() {
							var url = REST.getBaseUrl('actions/filters/');
							scope.updatingFilter = true;

							scope.existingFilterInUse.content = angular.fromJson(scope.filter);

							$http.put(url, scope.existingFilterInUse).then(
								function() {
									panelController.loadFilters().then(
										function() {
											scope.updatingFilter = false;
										},
										function(result) { console.error(result); }
									);
								},
								function(result) {
									scope.updatingFilter = false;
									var title = i18n.trans('m.rbs.admin.admin.filter_update_error');
									NotificationCenter.error(title, result.data.message, 'rbs_filter_update_error');
								}
							);
						};

						scope.removeExistingFilter = function() {
							if (window.confirm(i18n.trans('m.rbs.admin.admin.confirm_delete_filter'))) {
								scope.deletingFilter = true;
								REST.apiDelete('actions/filters/', { filter_id: scope.existingFilterInUse['filter_id'] })
									.then(
										function() {
											scope.useExistingFilter(null);
											panelController.loadFilters().then(
												function() {
													scope.deletingFilter = false;
												},
												function(result) { console.error(result); }
											);
										},
										function(data) {
											scope.deletingFilter = false;
											NotificationCenter.error(i18n.trans('m.rbs.admin.admin.filter_delete_error'),
												data.message, 'rbs_filter_delete_error');
										}
									);
							}
						};
					}
				};
			}
		]);

	app.directive('rbsDocumentFilterContainer', function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-container.twig',
			require: 'rbsFiltersContainer',
			scope: {
				model: '=',
				definitions: '='
			},

			link: function(scope, element, attr, rbsFiltersContainerCtrl) {
				scope.getRootFilter = function() {
					return rbsFiltersContainerCtrl.getRootFilter();
				};

				scope.isAnd = function() {
					var filter = rbsFiltersContainerCtrl.getRootFilter();
					return (filter.parameters && filter.parameters.operator) == 'AND';
				};

				scope.swapOperator = function() {
					var rc = function(pOperator, filters) {
						for (var i = 0; i < filters.length; i++) {
							if (filters[i].hasOwnProperty('filters') && filters[i].name === 'group') {
								filters[i].parameters.operator = ((pOperator == 'AND') ? 'OR' : 'AND');
								rc(filters[i].parameters.operator, filters[i].filters);
							}
						}
					};
					var filter = scope.getRootFilter();
					filter.parameters.operator = ((filter.parameters.operator == 'OR') ? 'AND' : 'OR');
					rc(filter.parameters.operator, filter.filters);
				};

				scope.addFilter = function() {
					if (angular.isObject(scope.definitionToAdd)) {
						var childName = scope.definitionToAdd.name;
						var filter = scope.getRootFilter();
						filter.filters.push({
							name: childName,
							parameters: angular.copy(scope.definitionToAdd.parameters)
						});
						scope.definitionToAdd = null;
						rbsFiltersContainerCtrl.redrawFilters(scope, element, filter.filters);
					}
				};

				scope.$watch('definitions', function(definitions) {
					if (definitions && definitions.length) {
						rbsFiltersContainerCtrl.setDefinitions(definitions);
					}
				});

				scope.$watchCollection('getFilter().filters', function(filters) {
					rbsFiltersContainerCtrl.redrawFilters(scope, element, filters);
				});
			}
		};
	});

	app.directive('rbsDocumentFilterPropertyBoolean', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-property-boolean.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'eq';
					}

					if (!scope.filter.parameters.hasOwnProperty('value')) {
						scope.filter.parameters.value = false;
					}

					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					return true;
				};

				containerController.applyConfigured(element, true, true);
			}
		};
	});

	app.directive('rbsDocumentFilterPropertyDatetime', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-property-datetime.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'lte';
					}

					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						var p = scope.filter.parameters;
						return !!(p.operator && (p.operator == 'isNull' || p.operator == 'isNotNull' || p.value));
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});

				scope.$watch('filter.parameters.operator', function(op) {
					if (op === 'isNull' || op === 'isNotNull') {
						scope.filter.parameters.value = null;
					}
				});
			}
		};
	});

	app.directive('rbsDocumentFilterPropertyDocument', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-property-document.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'eq';
					}
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						var p = scope.filter.parameters;
						return !!(p.operator && (p.operator == 'isNull' || p.operator == 'isNotNull' || p.value));
					}
					return false;
				};

				scope.showValueSelector = function() {
					if (scope.filter) {
						var p = scope.filter.parameters;
						return !!(p.operator && p.operator != 'isNull' && p.operator != 'isNotNull');
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});

				scope.$watch('filter.parameters.operator', function(op) {
					if (op === 'isNull' || op === 'isNotNull') {
						scope.filter.parameters.value = null;
					}
				})
			}
		};
	});

	app.directive('rbsDocumentFilterPropertyNumber', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-property-number.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'eq';
					}
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						var p = scope.filter.parameters;
						return !!(p.operator && (p.operator == 'isNull' || p.operator == 'isNotNull' || angular.isNumber(p.value)));
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});

				scope.$watch('filter.parameters.operator', function(op) {
					if (op === 'isNull' || op === 'isNotNull') {
						scope.filter.parameters.value = null;
					}
				})
			}
		};
	});

	app.directive('rbsDocumentFilterPropertyString', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-property-string.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'eq';
					}
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						var p = scope.filter.parameters;
						return !!(p.operator && (p.operator == 'isNull' || p.operator == 'isNotNull' || p.value));
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});

				scope.$watch('filter.parameters.operator', function(op) {
					if (op === 'isNull' || op === 'isNotNull') {
						scope.filter.parameters.value = null;
					}
				})
			}
		};
	});

	app.directive('rbsDocumentFilterModel', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-model.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					return !!(scope.filter && scope.filter.parameters.value);
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});
			}
		};
	});

	app.directive('rbsDocumentFilterTypology', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/document-filter-typology.twig',
			scope: {
				filter: '=', contextKey: '@'
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					return !!(scope.filter && scope.filter.parameters.value);
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});
			}
		};
	});
})();