/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsTip
	 * @name rbsTip
	 * @restrict A
	 *
	 * @description
	 * Displays a tip for the user in a <code>&lt;div class="alert-info"&gt;&lt;/div&gt;</code>.
	 * The user can close the tip and ask to never show again this tip (this information is stored in the
	 * localStorage of the browser).
	 *
	 * @param {string} rbsTip Unique identifier of the tip. It is recommended to use something similar to
	 * `vendorPluginName...` to avoid conflicts.
	 *
	 * @example
	 * ```html
	 *     <div rbs-tip="rbsDocumentListGeneralWithPreview">
	 *         ... tip contents ...
	 *     </div>
	 * ```
	 */
	app.directive('rbsTip', ['localStorageService', function(localStorageService) {
		var ls = localStorageService.get('dismissedTips') || {};

		function dismiss(tipId) {
			ls[tipId] = true;
			localStorageService.set('dismissedTips', ls);
		}

		function isDismissed(tipId) {
			return ls[tipId] === true;
		}

		return {
			restrict: 'A',
			replace: true,
			transclude: true,
			scope: true,
			templateUrl: 'Rbs/Admin/js/directives/tip.twig',

			compile: function(tElement, tAttrs) {
				if (isDismissed(tAttrs.rbsTip)) {
					tElement.remove();
				}

				// Linking function
				return function(scope, element, attrs) {
					scope.dismissForEver = function() {
						$(element).alert('close');
						dismiss(attrs.rbsTip);
					};
				};
			}
		};
	}]);
})();