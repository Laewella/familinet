(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsColorPicker
	 * @name rbsColorPicker
	 * @restrict A
	 *
	 * @description
	 * A color picker.
	 *
	 * @param {string} ngModel Assignable angular expression to data-bind to.
	 * @param {string=} size The size of the input-group representing the picker: 'sm', 'md' or 'lg' ('md' by default).
	 */
	app.directive('rbsColorPicker', function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/color-picker.twig',
			scope: {
				colorCode: '=ngModel'
			},
			link: function(scope, elm, attrs) {
				scope.size = attrs.size || 'md';
				scope.openPicker = function() {
					elm.find('.color-picker-input').focus().click();
				}
			}
		};
	});
})();