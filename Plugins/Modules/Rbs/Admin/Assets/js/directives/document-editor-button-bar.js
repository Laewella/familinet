(function($) {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorButtonBar', ['$rootScope', 'RbsChange.Dialog', 'RbsChange.Utils',
		'RbsChange.Actions', 'RbsChange.Events', 'RbsChange.i18n', 'RbsChange.REST', 'RbsChange.NotificationCenter',
		function($rootScope, Dialog, Utils, Actions, Events, i18n, REST, NotificationCenter) {
			return {
				restrict: 'E',
				transclude: true,
				templateUrl: 'Rbs/Admin/js/directives/document-editor-button-bar.twig',
				require: '^rbsDocumentEditorBase',

				link: function(scope, element, attrs) {
					scope.disableDelete = true;
					scope.advancedDiffs = true;

					function updateDisableDelete() {
						scope.disableDelete = !Utils.isDocument(scope.document) || scope.document.isNew() ||
							!scope.document.isActionAvailable('delete') || attrs.disableDelete === 'true';
					}

					attrs.$observe('disableDelete', updateDisableDelete);
					scope.$on(Events.EditorReady, updateDisableDelete);

					scope.confirmReset = function($event) {
						Dialog.confirmEmbed(
							element.find('.confirmation-area'),
							i18n.trans('m.rbs.admin.admin.confirm_restore | ucf'),
							i18n.trans('m.rbs.admin.admin.confirm_restore_message | ucf'),
							scope,
							{
								'pointedElement': $($event.target),
								'primaryButtonText': i18n.trans('m.rbs.admin.admin.restore_data_button | ucf'),
								'cssClass': 'warning'
							}
						).then(
							function() {
								scope.reset();
							},
							function(result) { console.error(result); }
						);
					};

					scope.confirmDelete = function($event) {
						var text = i18n.trans('m.rbs.admin.admin.confirm_delete_message | ucf');
						if (attrs.warningOnDeleteMessage) {
							text += '<div class="text-danger"><span class="icon icon-warning-sign"></span> <strong>' + attrs.warningOnDeleteMessage +
								'</strong></div>';
						}
						Dialog.confirmEmbed(
							element.find('.confirmation-area'),
							i18n.trans('m.rbs.admin.admin.confirm_delete | ucf'),
							text,
							scope,
							{
								'pointedElement': $($event.target),
								'primaryButtonText': i18n.trans('m.rbs.admin.admin.delete_data_button | ucf'),
								'primaryButtonClass': 'btn-danger',
								'cssClass': 'danger'
							}
						).then(
							function() {
								REST.delete(scope.document).then(
									function() {
										scope.goBack();
									},
									function(data) {
										if (data && data.message) {
											NotificationCenter.error(data.message, null, data.code);
										}
										else {
											console.error(data);
										}
									}
								);
							},
							function(result) { console.error(result); }
						);
					};

					scope.$on('Change:EditorPreSubmit', function(event, doc, promises) {
						if (Utils.hasCorrection(scope.document)) {
							promises.push(Dialog.confirmEmbed(
								element.find('.confirmation-area'),
								i18n.trans('m.rbs.admin.admin.confirm_update_correction | ucf'),
								i18n.trans('m.rbs.admin.admin.confirm_update_correction_message | ucf'),
								scope,
								{
									'pointedElement': $(element).find('[data-role=save]').first(),
									'cssClass': 'warning'
								}
							));
						}
					});
				}
			};
		}]);
})(window.jQuery);