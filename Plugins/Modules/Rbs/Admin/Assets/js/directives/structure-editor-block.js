(function() {
	'use strict';

	var app = angular.module('RbsChange');

	// Pre-load templates to avoid directive link delay.
	app.run(['$http', '$templateCache', function($http, $templateCache) {
		var templates = [
			'Rbs/Admin/js/directives/structure-editor-row-template.twig',
			'Rbs/Admin/js/directives/structure-editor-cell-template.twig',
			'Rbs/Admin/js/directives/structure-editor-block-chooser.twig',
			'Rbs/Admin/js/directives/structure-editor-block-template.twig'
		];
		angular.forEach(templates, function(template) {
			$http.get(template).then(
				function(result) {
					$templateCache.put(template, result.data);
				},
				function(result) { console.error(result); }
			);
		});
	}]);

	/**
	 * data-rbs-row
	 */
	app.directive('rbsRow', [function() {
		return {
			restrict: 'A',
			require: '^rbsStructureEditor',
			scope: {}, // isolated scope is required
			templateUrl: 'Rbs/Admin/js/directives/structure-editor-row-template.twig',

			link: {
				pre: function(scope, elm, attrs, ctrl) {
					ctrl.registerBlockScope(elm, scope);

					scope.item = ctrl.getItemById(elm.data('id'));

					elm.addClass('row');

					// On click on a column, select the row.
					elm.click(function(event) {
						var targetElement = angular.element(event.target);
						if (targetElement === elm || targetElement.parentNode === elm) {
							ctrl.selectBlock(elm);
						}
					});

					scope.selectBlock = function() {
						ctrl.selectBlock(elm);
					};

					scope.openSettings = function() {
						ctrl.openSettingsModal(scope.item);
					};

					elm.dblclick(function(event) {
						var targetElement = angular.element(event.target);
						if (targetElement === elm || targetElement.parentNode === elm) {
							scope.openSettings();
						}
					});

					ctrl.initChildren(scope, elm, scope.item, ctrl.isReadOnly());
				}
			}
		};
	}]);

	/**
	 * data-rbs-cell
	 */
	app.directive('rbsCell', [function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/structure-editor-cell-template.twig',
			scope: {}, // isolated scope is required
			require: '^rbsStructureEditor',

			link: {
				pre: function(scope, elm, attrs, ctrl) {
					ctrl.registerBlockScope(elm, scope);

					scope.getColumnIndex = function() {
						return elm.index();
					};

					scope.item = ctrl.getItemById(elm.data('id'));
					scope.rowItem = ctrl.getItemById(elm.closest('[data-rbs-row]').data('id'));

					function refreshSizeClass() {
						for (var i = 1; i <= scope.rowItem.grid; i++) {
							elm.removeClass('col-md-' + i);
						}
						elm.addClass('col-md-' + scope.item.size);
					}

					function refreshOffsetClass() {
						for (var i = 1; i <= scope.rowItem.grid; i++) {
							elm.removeClass('col-md-offset-' + i);
						}
						if (scope.item.offset) {
							elm.addClass('col-md-offset-' + scope.item.offset);
						}
					}

					scope.$watch('item.size', refreshSizeClass);
					scope.$watch('item.offset', refreshOffsetClass);
					scope.$watch('rowItem.grid', function() {
						refreshSizeClass();
						refreshOffsetClass();
					});

					// On click on a column, select the row.
					elm.click(function(event) {
						var targetElement = angular.element(event.target);
						if (targetElement === elm || targetElement.parentNode === elm) {
							ctrl.selectBlock(elm, { 'highlight-column': scope.getColumnIndex() });
						}
					});

					scope.selectBlock = function() {
						ctrl.selectBlock(elm, { 'highlight-column': scope.getColumnIndex() });
					};

					scope.openSettings = function() {
						ctrl.openSettingsModal(scope.item);
					};

					elm.dblclick(function(event) {
						var targetElement = angular.element(event.target);
						if (targetElement === elm || targetElement.parentNode === elm) {
							scope.openSettings();
						}
					});

					ctrl.initChildren(scope, elm, scope.item, ctrl.isReadOnly());
				}
			}
		};
	}]);

	/**
	 * data-rbs-block-chooser
	 */
	app.directive('rbsBlockChooser', [function() {
		return {
			restrict: 'A',
			scope: true,
			require: '^rbsStructureEditor',
			replace: true,
			templateUrl: 'Rbs/Admin/js/directives/structure-editor-block-chooser.twig',

			link: {
				pre: function(scope, element, attrs, ctrl) {
					ctrl.registerBlockScope(element, scope);

					scope.selectBlock = function() {
						ctrl.selectBlock(element, scope);
					};
				}
			}
		};
	}]);

	/**
	 * data-rbs-block-template
	 */
	app.directive('rbsBlockTemplate', ['proximisModalStack', 'RbsChange.structureEditorService', 'RbsChange.REST',
		function(proximisModalStack, structureEditorService, REST) {
			return {
				restrict: 'A',
				scope: { readonly: '@' }, // isolated scope is required
				require: '^rbsStructureEditor',
				replace: true,
				templateUrl: 'Rbs/Admin/js/directives/structure-editor-block-template.twig',

				link: {
					pre: function(scope, element, attrs, ctrl) {
						ctrl.registerBlockScope(element, scope);

						scope.item = ctrl.getItemById(attrs.id);
						scope.isRichText = structureEditorService.isRichTextBlock(scope.item.name);

						if (scope.isRichText) {
							scope.contextKey = 'block_' + attrs.id + '_richtext';

							if (!scope.item.parameters) {
								scope.item.parameters = {
									content: { e: 'Markdown', 'h': null, 't': null }
								};
							}
							else if (!scope.item.parameters.hasOwnProperty('content')) {
								scope.item.parameters.content = { e: 'Markdown', 'h': null, 't': null };
							}
							else if (typeof scope.item.parameters.content === 'string') {
								scope.item.parameters.content = { e: 'Markdown', 'h': null, 't': scope.item.parameters.content }
							}
						}

						var preview = element.find('.preview-container');

						scope.refreshPreview = function() {
							preview.empty();
							if (scope.isRichText && scope.item.parameters.content.t) {
								var params = {
									profile: ctrl.mailSuitable ? 'Mail' : 'Website',
									editor: scope.item.parameters.content.e
								};
								REST.postAction('renderRichText', scope.item.parameters.content.t, params).then(
									function(data) {
										preview.html(data.html);
										preview.find('*').click(function(event) { event.preventDefault(); });
									},
									function(result) { console.error(result); }
								);
							}
						};

						scope.refreshPreview();

						scope.selectBlock = function() { ctrl.selectBlock(element); };
						scope.openSettings = function() { ctrl.openSettingsModal(scope.item); };

						scope.openTech = function() {
							proximisModalStack.open({
								templateUrl: 'Rbs/Admin/js/directives/structure-editor-modal-tech.twig',
								size: 'lg',
								controller: 'RbsStructureEditorTechController',
								resolve: {
									modalData: function() { return { item: scope.item }; }
								}
							});
						};

						ctrl.registerBlockMethods(attrs.id, {
							refreshPreview: function() { scope.refreshPreview(); }
						});
					}
				}
			};
		}
	]);

	/**
	 * RbsStructureEditorTechController
	 */
	app.controller('RbsStructureEditorTechController', ['proximisModalStack', '$scope', '$uibModalInstance', 'modalData',
		RbsStructureEditorTechController]);

	function RbsStructureEditorTechController(proximisModalStack, scope, $uibModalInstance, modalData) {
		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};

		$uibModalInstance.result.then(closeFunction, closeFunction);

		scope.close = function() {
			$uibModalInstance.close();
		};

		scope.modalData = modalData;
	}
})();