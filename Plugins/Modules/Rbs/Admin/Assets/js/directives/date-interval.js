(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDateInterval
	 * @name rbsDateInterval
	 * @restrict A
	 *
	 * @description
	 * The date interval selector.
	 *
	 * @param {number=} showYears The max for the years selector. If omitted, there won't be any selector for years.
	 * @param {number=} minYears The min for the years selector. If omitted, value will be 0. Must be used with showYears.
	 * @param {number=} showMonths The max for the months selector. If omitted, there won't be any selector for months.
	 * @param {number=} minMonths The min for the months selector. If omitted, value will be 0. Must be used with showMonths.
	 * @param {number=} showDays The max for the days selector. If omitted, there won't be any selector for days.
	 * @param {number=} minDays The min for the days selector. If omitted, value will be 0. Must be used with showDays.
	 * @param {number=} showHours The max for the hours selector. If omitted, there won't be any selector for hours.
	 * @param {number=} minHours The min for the hours selector. If omitted, value will be 0. Must be used with showHours.
	 * @param {number=} showMinutes The max for the minutes selector. If omitted, there won't be any selector for minutes.
	 * @param {number=} minMinutes The min for the minutes selector. If omitted, value will be 0. Must be used with showMinutes.
	 */
	app.directive('rbsDateInterval', function() {
		return {
			'restrict': 'A',
			templateUrl: 'Rbs/Admin/js/directives/date-interval.twig',
			require: 'ngModel',
			scope: {
				showYears: '@',
				minYears: '@',
				showMonths: '@',
				minMonths: '@',
				showDays: '@',
				minDays: '@',
				showHours: '@',
				minHours: '@',
				showMinutes: '@',
				minMinutes: '@'
			},

			link: function(scope, elm, attrs, ngModel) {
				scope.minYears = scope.minYears || 0;
				scope.minMonths = scope.minMonths || 0;
				scope.minDays = scope.minDays || 0;
				scope.minHours = scope.minHours || 0;
				scope.minMinutes = scope.minMinutes || 0;

				scope.interval = { Y: 0, M: 0, D: 0, TH: 0, TM: 0 };

				scope.userChange = function() {
					var d = [];
					if (scope.interval.Y) {
						d.push(scope.interval.Y + 'Y');
					}
					if (scope.interval.M) {
						d.push(scope.interval.M + 'M');
					}
					if (scope.interval.D) {
						d.push(scope.interval.D + 'D');
					}
					if (scope.interval.TH || scope.interval.TM) {
						d.push('T');
						if (scope.interval.TH) {
							d.push(scope.interval.TH + 'H');
						}
						if (scope.interval.TM) {
							d.push(scope.interval.TM + 'M');
						}
					}
					var viewValue = d.length ? 'P' + d.join('') : null;
					ngModel.$setViewValue(viewValue);
				};

				ngModel.$render = function() {
					if (angular.isString(ngModel.$viewValue) && ngModel.$viewValue.indexOf('P') === 0) {
						var a = ngModel.$viewValue.substr(1).split('T');
						var m = a[0].match(/(\d+)Y/);
						if (m && m.length == 2) {
							scope.interval.Y = parseInt(m[1], 10);
						}
						m = a[0].match(/(\d+)M/);
						if (m && m.length == 2) {
							scope.interval.M = parseInt(m[1], 10);
						}
						m = a[0].match(/(\d+)D/);
						if (m && m.length == 2) {
							scope.interval.D = parseInt(m[1], 10);
						}
						if (a.length == 2) {
							m = a[1].match(/(\d+)H/);
							if (m && m.length == 2) {
								scope.interval.TH = parseInt(m[1], 10);
							}

							m = a[1].match(/(\d+)M/);
							if (m && m.length == 2) {
								scope.interval.TM = parseInt(m[1], 10);
							}
						}
					}
				}
			}
		};
	});
})();