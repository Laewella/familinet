/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsMaxHeight', ['$timeout', function($timeout) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/max-height.twig',
			transclude: true,
			scope: {},
			link: function(scope, elm, attrs) {
				scope.containerNode = elm.find('.max-height-container');
				scope.contentNode = elm.find('.max-height-content').get(0);
				scope.deployed = false;
				scope.showButtons = false;
				scope.maxHeight = parseInt(attrs['rbsMaxHeight'], 10);
				if (isNaN(scope.maxHeight) || scope.maxHeight < 0) {
					scope.maxHeight = 0;
				}

				scope.toggle = function() {
					scope.deployed = !scope.deployed;
					refreshStyles();
				};

				var height;

				$timeout(checkHeight, 100);
				function checkHeight() {
					var newHeight = scope.contentNode.offsetHeight;
					if (height !== newHeight) {
						height = newHeight;

						var newShowButtons;
						if (!scope.maxHeight) {
							newShowButtons = false;
						}
						else {
							newShowButtons = (height > scope.maxHeight + 20);
						}

						if (scope.showButtons !== newShowButtons) {
							scope.showButtons = newShowButtons;
							refreshStyles();
						}
					}
					$timeout(checkHeight, 100);
				}

				function refreshStyles() {
					if (!scope.showButtons || scope.deployed) {
						scope.containerNode.css({ overflow: 'visible', 'max-height': '' });
					}
					else {
						scope.containerNode.css({ overflow: 'hidden', 'max-height': scope.maxHeight + 'px' });
					}
				}
			}
		}
	}]);
})();