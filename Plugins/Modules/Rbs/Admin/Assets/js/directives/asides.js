(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsActiveRoute
	 * @name rbsActiveRoute
	 * @restrict A
	 * @element a
	 *
	 * @description
	 * Toggles the 'active' CSS class depending on the current URL.
	 */
	app.directive('rbsActiveRoute', ['$rootScope', '$location', function($rootScope, $location) {
		return {
			restrict: 'A',

			link: function(scope, iElement, iAttrs) {
				var activeEl,
					href;

				function setHref(h) {
					href = h;
					if (href.substr(0, 5) !== 'http:' && href.substr(0, 6) !== 'https:' && href.charAt(0) !== '/') {
						href = '/' + href;
					}
				}

				function isSameURL() {
					return href === $location.absUrl() || href === $location.path();
				}

				function updateStyle() {
					activeEl[isSameURL() ? 'addClass' : 'removeClass']('active');
				}

				// Get parent element on which the 'active' class should be set.
				activeEl = iAttrs.rbsActiveRoute ? iElement.closest(iAttrs.rbsActiveRoute) : iElement;
				href = setHref(iAttrs.href);

				// React to every route change and add/remove 'active' on parent element.
				$rootScope.$on('$routeChangeSuccess', updateStyle);
				$rootScope.$on('$routeUpdate', updateStyle);
				updateStyle();

				iAttrs.$observe('href', function(h) {
					setHref(h);
					updateStyle();
				});
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAsideColumn
	 * @name rbsAsideColumn
	 * @restrict A
	 *
	 * @description
	 * Shortcut to set the default CSS class name for a standard left column.
	 */
	app.directive('rbsAsideColumn', function() {
		return {
			restrict: 'A',
			link: function(scope, iElement) {
				iElement.addClass('col-md-3');
			}
		};
	});

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsMainColumn
	 * @name rbsMainColumn
	 * @restrict A
	 *
	 * @description
	 * Shortcut to set the default CSS class name for a standard main view with a left column.
	 */
	app.directive('rbsMainColumn', function() {
		return {
			restrict: 'A',
			link: function(scope, iElement) {
				iElement.addClass('col-md-9');
			}
		};
	});

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsFullWidth
	 * @name rbsFullWidth
	 * @restrict A
	 *
	 * @description
	 * Shortcut to set the default CSS class name for a full width view.
	 */
	app.directive('rbsFullWidth', function() {
		return {
			restrict: 'A',
			link: function(scope, iElement) {
				iElement.addClass('col-md-12');
			}
		};
	});

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDefaultAsidesForList
	 * @name rbsDefaultAsidesForList
	 * @restrict AE
	 *
	 * @description
	 * Default Asides for a list view, including the menu.
	 */
	app.directive('rbsDefaultAsidesForList', ['$rootScope', function($rootScope) {
		return {
			restrict: 'AE',
			template: '<div data-ng-include="menuUrl"></div>',
			scope: true,

			link: function(scope, iElement, iAttrs) {
				var plugin = iAttrs['plugin'] || $rootScope.rbsCurrentPluginName;
				scope.menuUrl = plugin.replace(/_/, '/') + '/menu.twig';
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAsideEditorMenu
	 * @name rbsAsideEditorMenu
	 * @restrict AE
	 *
	 * @description
	 * This Directive displays the menu with the sections of an Editor.
	 * It updates on the 'Change:UpdateEditorMenu' event.
	 */
	app.directive('rbsAsideEditorMenu', ['$rootScope', function($rootScope) {
		return {
			restrict: 'AE',
			templateUrl: 'Rbs/Admin/js/directives/aside-editor-sections.twig',
			require: '?^rbsDocumentEditorBase',

			link: function(scope, iElement, iAttrs, ctrl) {
				$rootScope.$on('Change:UpdateEditorMenu', function(event, menuEntries) {
					scope.entries = menuEntries;
				});

				if (ctrl) {
					scope.entries = ctrl.getMenuEntries();
				}
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAside
	 * @name rbsAside
	 * @restrict AE
	 *
	 * @description
	 * Displays an Aside whose template is located at the URL provided in the 'template' attribute.
	 *
	 * @param {string=} template The aside template URL.
	 */
	app.directive('rbsAside', function() {
		return {
			restrict: 'AE',
			replace: true,
			template: '<div data-ng-include="asideUrl"></div>',
			scope: true,

			link: function(scope, iElement, iAttrs) {
				scope.asideUrl = iAttrs['template'];
			}
		};
	});

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAsideTranslation
	 * @name rbsAsideTranslation
	 * @restrict AE
	 *
	 * @description
	 * Displays the languages list to translate the given document.
	 *
	 * @param {ChangeDocument} document The document.
	 */
	app.directive('rbsAsideTranslation',
		['RbsChange.Utils', 'RbsChange.REST', 'RbsChange.i18n', '$q', function(Utils, REST, i18n, $q) {
			return {
				restrict: 'AE',
				templateUrl: 'Rbs/Admin/js/directives/aside-translations.twig',
				scope: {
					document: '='
				},

				link: function(scope) {
					var docPromise = $q.defer();

					scope.$watch('document', function(doc) {
						if (Utils.isDocument(doc) && doc.refLCID) {
							docPromise.resolve(doc);
						}
					});

					$q.all([docPromise.promise, REST.getAvailableLanguages()]).then(
						function(results) {
							var doc = results[0],
								langs = results[1],
								contents = [];

							scope.currentLCID = doc.LCID;
							scope.missingTranslations = false;

							angular.forEach(langs.items, function(item, lcid) {
								if (lcid === doc.refLCID) {
									contents.push({
										'url': doc.refUrl(),
										'text': item.label + ' (<abbr title="' +
										i18n.trans('m.rbs.admin.admin.reference_language | ucf') + '">' +
										i18n.trans('m.rbs.admin.admin.ref_lang_abbr') + '</abbr>)',
										'icon': 'icon-book'
									});
								}
								else {
									var translated = doc.isTranslatedIn(lcid);
									contents.push({
										'url': doc.translateUrl(lcid),
										'text': item.label,
										'cssClass': translated ? 'translated' : 'untranslated',
										'icon': translated ? 'icon-ok' : 'icon-warning-sign'
									});
									if (!translated) {
										scope.missingTranslations = true;
									}
								}
							});

							if (contents.length > 1) {
								scope.rbsI18nItems = contents;
							}
						},
						function(result) { console.error(result); }
					);
				}
			};
		}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAsideSeo
	 * @name rbsAsideSeo
	 * @restrict AE
	 *
	 * @description
	 * Displays the link to edit the SEO information of the given document.
	 *
	 * @param {ChangeDocument} document The document.
	 */
	app.directive('rbsAsideSeo', ['RbsChange.Utils', 'RbsChange.REST', '$location', function(Utils, REST, $location) {
		return {
			restrict: 'AE',
			templateUrl: 'Rbs/Seo/aside.twig',
			scope: {
				document: '='
			},

			link: function(scope) {
				scope.showContent = false;
				scope.$watch('document', function(doc) {
					if (Utils.isDocument(doc)) {
						var seoLink = doc.getLink('seo');
						if (seoLink) {
							REST.call(seoLink, null, REST.resourceTransformer()).then(
								function(seoDocument) {
									scope.seoDocument = seoDocument;
									scope.showContent = true;
								},
								function(result) { console.error(result); }
							);
						}
						else {
							scope.showContent = true;
						}
					}
				});

				scope.seoCreate = function() {
					scope.seoCreating = true;
					REST.call(scope.document.getActionUrl('addSeo'), null, REST.resourceTransformer()).then(
						function(seoDocument) {
							scope.seoCreating = false;
							scope.seoDocument = seoDocument;
							$location.path(seoDocument.url());
						},
						function(result) { console.error(result); }
					);
				};
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAsideCode
	 * @name rbsAsideCode
	 * @restrict AE
	 *
	 * @description
	 * Displays a for to edit the code in the given context.
	 *
	 * @param {string} context The code context.
	 * @param {ChangeDocument} document The document.
	 */
	app.directive('rbsAsideCode', ['RbsChange.Utils', 'RbsChange.REST', function(Utils, REST) {
		return {
			restrict: 'AE',
			templateUrl: 'Rbs/Admin/js/directives/aside-code.twig',
			scope: {
				document: '=',
				contextLabel: '@'
			},

			link: function(scope, element, attrs) {
				scope.data = {
					codes: [],
					originalCodes: [],
					context: attrs['context'] || attrs['contextId'],
					loaded: false,
					initialized: false,
					icon: attrs.icon || 'icon-magic'
				};

				scope.$watch('document', function(doc) {
					if (Utils.isDocument(doc)) {
						scope.data.loaded = false;
						REST.apiGet('resourcescode/' + scope.data.context + '/codes/', { id: doc.id }).then(
							function(data) {
								scope.data.loaded = true;
								scope.data.initialized = true;
								if (data.contextId) {
									scope.data.context = data.context;
								}
								var newCodes = angular.isArray(data.codes) ? data.codes : [];
								scope.data.codes = newCodes;
								scope.data.originalCodes = angular.copy(newCodes);
							},
							function(result) { console.error(result); }
						);
					}
				});

				scope.addCode = function() {
					scope.data.codes.push('');
				};

				scope.removeCode = function(index) {
					scope.data.codes.splice(index, 1);
				};

				scope.saveCodes = function() {
					scope.data.loaded = false;
					var postData = { id: scope.document.id, codes: scope.data.codes };
					REST.apiPost('resourcescode/' + scope.data.context + '/codes/', postData).then(
						function(data) {
							scope.data.loaded = true;
							var newCodes = angular.isArray(data.codes) ? data.codes : [];
							scope.data.codes = newCodes;
							scope.data.originalCodes = angular.copy(newCodes);
						},
						function(result) { console.error(result); }
					);
				};

				scope.isModified = function() {
					var length = scope.data.codes.length;
					if (length !== scope.data.originalCodes.length) {
						return true;
					}
					for (var i = 0; i < length; i++) {
						if (scope.data.codes[i] !== scope.data.originalCodes[i]) {
							return true;
						}
					}
					return false;
				};

				scope.resetCodes = function() {
					scope.data.codes = angular.copy(scope.data.originalCodes);
				}
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsAsidePluginMenu
	 * @name rbsAsidePluginMenu
	 * @restrict AE
	 *
	 * @deprecated since 1.9.0, use rbsDefaultAsidesForList instead.
	 *
	 * @description
	 * **Deprecated since 1.9.0, use rbsDefaultAsidesForList instead.**
	 *
	 * Displays the menu of the given plugin.
	 * If `plugin` attribute is not set, this Directive will look for the current plugin based on the current route.
	 *
	 * @param {string=} plugin The plugin name in the form "Vendor_Plugin".
	 */
	app.directive('rbsAsidePluginMenu', ['$rootScope', function($rootScope) {
		return {
			restrict: 'AE',
			template: '<div data-ng-include="menuUrl"></div>',
			scope: true,

			link: function(scope, iElement, iAttrs) {
				console.warn('rbsAsidePluginMenu is deprecated since 1.9.0, use rbsDefaultAsidesForList instead.');
				var plugin = iAttrs['plugin'] || $rootScope.rbsCurrentPluginName;
				scope.menuUrl = plugin.replace(/_/, '/') + '/menu.twig';
			}
		};
	}]);
})();
