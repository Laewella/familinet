/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	angular.module('RbsChange').directive('rbsStorageDownloader', ['RbsChange.REST', '$timeout', function(REST, $timeout) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/storage-downloader.twig',
			scope: {
				changeUri: '=',
				fileName: '='
			},

			link: function(scope, element) {
				scope.downloadUrl = null;
				scope.checkingURL = false;

				scope.onDownload = function() {
					scope.downloadUrl = null;
					scope.checkingURL = true;
					if (angular.isString(scope.changeUri) && scope.changeUri.length > 10) {
						var url = scope.changeUri.replace('change://', REST.getBaseUrl('storage/'));
						REST.call(url).then(
							function(data) {
								if (data && data.temporaryDownload) {
									scope.downloadUrl = data.temporaryDownload;
									element.append("<iframe src='" + data.temporaryDownload + "' style='display: none;' ></iframe>");
									$timeout(function() {
										scope.checkingURL = false;
									}, 1000);
								}
							},
							function(error) {
								scope.checkingURL = false;
								console.error(error);
							}
						)
					}
				}
			}
		};
	}]);
})();