(function () {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsBulletCorrection
	 * @name rbsBulletCorrection
	 * @restrict AE
	 *
	 * @description
	 * Show an icon if there is a correction on the document.
	 *
	 * @param {string} ngModel The ChangeDocument.
	 */
	app.directive('rbsBulletCorrection', [function ()
	{
		return {
			restrict : 'AE',
			templateUrl : 'Rbs/Admin/js/directives/bullet-correction.twig',
			require: '?ngModel',
			replace: true,
			scope : {
				document : '=ngModel'
			}
		};
	}]);
})();