(function() {
	'use strict';

	var app = angular.module('RbsChange');

	// Pre-load templates to avoid directive link delay.
	app.run(['$http', '$templateCache', function($http, $templateCache) {
		var templates = [
			'Rbs/Admin/js/directives/structure-editor-aside.twig',
			'Rbs/Admin/js/directives/structure-editor-aside-row.twig'
		];
		angular.forEach(templates, function(template) {
			$http.get(template).then(
				function(result) {
					$templateCache.put(template, result.data);
				},
				function(result) { console.error(result); }
			);
		});
	}]);

	/**
	 * data-rbs-structure-editor-aside
	 */
	app.directive('rbsStructureEditorAside', function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/structure-editor-aside.twig',
			scope: false,
			link: function() {}
		};
	});

	/**
	 * data-rbs-structure-editor-aside-block
	 */
	app.directive('rbsStructureEditorAsideBlock', ['$timeout', 'RbsChange.structureEditorService', 'RbsChange.Blocks', 'RbsChange.Dialog',
		'RbsChange.i18n', function($timeout, structureEditorService, Blocks, Dialog, i18n) {
			return {
				restrict: 'A',
				transclude: true,
				scope: true,
				templateUrl: 'Rbs/Admin/js/directives/structure-editor-aside-block.twig',

				link: function(scope) {
					var ctrl = scope.editorCtrl;
					scope.selectedBlock = ctrl.getSelectedBlock();
					scope.isChooser = scope.selectedBlock.is('[data-rbs-block-chooser]');

					scope.canInsertSideways = function() {
						return !scope.selectedBlock.is('[data-rbs-row]') && !ctrl.isInColumnLayout(scope.selectedBlock);
					};

					scope.isInColumnLayout = function() {
						return ctrl.isInColumnLayout(scope.selectedBlock);
					};

					scope.selectParentCell = function() {
						ctrl.selectParentCell(scope.selectedBlock);
					};

					scope.newBlockBefore = function() {
						ctrl.newBlockBefore();
					};

					scope.newBlockAfter = function() {
						ctrl.newBlockAfter();
					};

					scope.newBlockTop = function() {
						ctrl.newBlockTop();
					};

					scope.newBlockBottom = function() {
						ctrl.newBlockBottom();
					};

					scope.newBlockLeft = function() {
						ctrl.newBlockSideways('left');
					};

					scope.newBlockRight = function() {
						ctrl.newBlockSideways('right');
					};

					scope.removeBlock = function() {
						if (scope.isChooser) {
							ctrl.removeBlock(scope.selectedBlock);
							ctrl.notifyChange('remove', 'block', scope.selectedBlock);
						}
						else {
							Dialog.confirmLocal(
								scope.selectedBlock,
								i18n.trans('m.rbs.admin.admin.structure_editor_remove_block | ucf'),
								'<strong>' + i18n.trans('m.rbs.admin.admin.structure_editor_remove_block_confirm | ucf') + '</strong>',
								{ placement: 'top' }
							).then(
								function() {
									ctrl.removeBlock(scope.selectedBlock);
									ctrl.notifyChange('remove', 'block', scope.selectedBlock);
								},
								function(result) { console.error(result); }
							);
						}
					};

					if (scope.isChooser) {
						scope.chooser = {
							selected: '',
							keyword: '',
							blocks: Blocks.getList(ctrl.mailSuitable)
						};

						scope.$watch('chooser.selected', function() {
							if (scope.chooser.selected) {
								var blockDefinition = Blocks.getBlock(scope.chooser.selected);
								if (blockDefinition) {
									var block = replaceItem({
										type: 'block',
										name: blockDefinition.name,
										label: blockDefinition.label
									});
									ctrl.notifyChange('create', blockDefinition.label, block);
									$timeout(function() { ctrl.selectBlock(block); });
								}
							}
						});
					}

					function replaceItem(item) {
						var block = ctrl.getSelectedBlock();
						var createdEl = ctrl.createBlock(block.parent(), item, block.index());
						ctrl.removeBlock(block);
						return createdEl;
					}
				}
			};
		}
	]);

	/**
	 * data-rbs-structure-editor-aside-row
	 */
	app.directive('rbsStructureEditorAsideRow', ['RbsChange.structureEditorService', '$timeout', 'RbsChange.Dialog', 'RbsChange.i18n',
		'RbsChange.ArrayUtils',
		function(structureEditorService, $timeout, Dialog, i18n, ArrayUtils) {
			return {
				restrict: 'A',
				scope: true,
				templateUrl: 'Rbs/Admin/js/directives/structure-editor-aside-row.twig',

				link: function(scope, elm, attrs) {
					var ctrl = scope.editorCtrl;
					var rowElm;
					var selected = ctrl.getSelectedBlock();
					if (selected.is('[data-rbs-cell]')) {
						rowElm = selected.parent().parent();
					}
					else {
						rowElm = selected;
					}

					scope.highlightedColIndex = attrs.highlightColumn || -1;

					var gridSize = attrs.gridSize || structureEditorService.getDefaultGridSize();

					var childrenElm = rowElm.children('.children');
					if (childrenElm.children().length === 0) {
						ctrl.selectBlock(ctrl.createBlock(childrenElm, { 'type': 'cell', 'size': structureEditorService.getDefaultGridSize() }));
					}

					scope.gridSize = gridSize;
					scope.totalColumns = 0;

					scope.highlightColumn = function(index) {
						if (angular.isDefined(index)) {
							if (index === scope.highlightedColIndex) {
								scope.highlightedColIndex = -1;
							}
							else {
								scope.highlightedColIndex = Math.min(index, childrenElm.children().length - 1);
							}

							if (scope.highlightedColIndex !== -1) {
								ctrl.selectBlock(jQuery(childrenElm.children()[index]), { 'highlight-column': index });
							}
							else {
								ctrl.selectBlock(rowElm);
							}
						}
					};

					scope.setEqualColumns = function() {
						angular.forEach(scope.columns, function(column) {
							column.offset = 0;
							column.span = scope.equalSize;
						});
						ctrl.notifyChange('resize', 'allColumns', rowElm, { size: scope.equalSize });
					};

					scope.addBlockInColumn = function(index, $event) {
						if (index === scope.highlightedColIndex) {
							$event.stopPropagation();
						}

						var colIndex = index, newBlock, cell = jQuery(childrenElm.children()[index]);
						newBlock = ctrl.createBlock(cell.children('.children'));
						scope.columns[index].childCount++;
						index = scope.highlightedColIndex;
						scope.highlightedColIndex = -1;
						scope.highlightColumn(index);
						ctrl.notifyChange('create', 'block', newBlock, { column: colIndex });
					};

					// Column offset.

					scope.canIncreaseOffset = function(index) {
						return scope.totalColumns < gridSize || (scope.columns[index + 1] && scope.columns[index + 1].offset >= 1);
					};

					scope.increaseOffset = function(index, $event) {
						if (index === scope.highlightedColIndex) {
							$event.stopPropagation();
						}

						var offset = 0,
							isLastCol = (index === scope.columns.length - 1);

						if (isLastCol && scope.totalColumns < gridSize) {
							offset = $event.altKey ? gridSize - scope.totalColumns : 1;
						}
						else {
							if (scope.columns[index + 1] && scope.columns[index + 1].offset >= 1) {
								offset = $event.altKey ? scope.columns[index + 1].offset : 1;
								scope.columns[index + 1].offset -= offset;
							}
							else {
								if (scope.totalColumns < gridSize) {
									offset = $event.altKey ? gridSize - scope.totalColumns : 1;
								}
							}
						}

						scope.columns[index].offset += offset;
						ctrl.notifyChange('increaseOffset', 'column', rowElm, { offset: offset });
					};

					scope.decreaseOffset = function(index, $event) {
						if (index === scope.highlightedColIndex) {
							$event.stopPropagation();
						}

						var offset = $event.altKey ? scope.columns[index].offset : 1;
						scope.columns[index].offset -= offset;
						if (scope.columns[index + 1]) {
							scope.columns[index + 1].offset += offset;
						}
						ctrl.notifyChange('decreaseOffset', 'column', rowElm, { offset: offset });
					};

					// Column size

					scope.canExpandColumn = function(index) {
						return scope.totalColumns < gridSize
							|| (scope.columns[index + 1] && scope.columns[index + 1].offset >= 1)
							|| (scope.columns[index].offset >= 1);
					};

					scope.expandColumn = function(index, $event) {
						if (index === scope.highlightedColIndex) {
							$event.stopPropagation();
						}

						var column = scope.columns[index];
						var nextColumn = scope.columns[index + 1];
						var expandSize = 0,
							offsetToRemove = 0,
							nextOffsetToRemove = 0,
							prevSpan = column.span;

						// If the Shift key is pressed,
						// find all the offsets that can be removed in favour of the column width.
						if ($event.altKey) {
							offsetToRemove = column.offset || 0;
							if (nextColumn) {
								nextOffsetToRemove = nextColumn.offset || 0;
							}
							if (scope.totalColumns < gridSize) {
								expandSize = gridSize - scope.totalColumns;
							}
						}
						else {
							// Use empty space if any.
							if (scope.totalColumns < gridSize) {
								expandSize = 1;
							}
							else {
								if (column.offset >= 1) {
									// Can we reduce the offset of the column?
									offsetToRemove = 1;
								}
								else {
									if (nextColumn.offset >= 1) {
										// Can we reduce the offset of the next column?
										nextOffsetToRemove = 1;
									}
								}
							}
						}

						if (offsetToRemove) {
							column.offset -= offsetToRemove;
							expandSize += offsetToRemove;
						}
						if (nextOffsetToRemove) {
							nextColumn.offset -= nextOffsetToRemove;
							expandSize += nextOffsetToRemove;
						}

						column.span += expandSize;
						ctrl.getItemById(column.id).size = column.span;

						ctrl.notifyChange('resize', 'column', rowElm, { from: prevSpan, to: column.span });
					};

					scope.reduceColumn = function(index, $event) {
						if (index === scope.highlightedColIndex) {
							$event.stopPropagation();
						}

						var column = scope.columns[index];
						column.span--;
						if (scope.columns[index + 1]) {
							scope.columns[index + 1].offset++;
						}
						ctrl.getItemById(column.id).size = column.span;

						ctrl.notifyChange('resize', 'column', rowElm, { from: column.span + 1, to: column.span });
					};

					// Insert column

					scope.canInsertColumn = function(index) {
						return scope.canIncreaseOffset(index);
					};

					scope.insertColumn = function(index) {
						index++;

						var size = 0;
						if (scope.columns[index] && scope.columns[index].offset >= 1) {
							size = scope.columns[index].offset;
							ctrl.getItemById(scope.columns[index].id).offset = 0;
						}
						else if (scope.totalColumns < gridSize) {
							size = gridSize - scope.totalColumns;
						}

						if (size) {
							ctrl.createBlock(childrenElm, { size: size, offset: 0, type: 'cell' }, index);

							$timeout(function() {
								scope.columns = getColumnsInfo();
								scope.highlightColumn(index);
								ctrl.notifyChange('create', 'column', rowElm, { location: index });
							});
						}
						else {
							throw new Error('Could not insert column because its size would be 0.');
						}
					};

					// Delete column

					scope.deleteColumn = function(index, $event) {
						$event.stopPropagation();

						Dialog.confirmLocal(
							childrenElm.children().get(index),
							i18n.trans('m.rbs.admin.admin.structure_editor_remove_column | ucf'),
							i18n.trans('m.rbs.admin.admin.structure_editor_remove_column_confirm | ucf',
								{ CHILDCOUNT: scope.columns[index].childCount }),
							{ placement: 'top' }
						).then(
							function() {
								if (index === scope.highlightedColIndex) {
									scope.highlightedColIndex = -1;
								}
								ctrl.removeItem(scope.columns[index]);
								scope.columns.splice(index, 1);
								ctrl.notifyChange('remove', 'column', rowElm, { location: index });
								if (scope.columns.length === 0) {
									scope.insertColumn(0);
								}
							},
							function(result) { console.error(result); }
						);
					};

					// Move column.

					scope.moveColumnLeft = function(index, $event) {
						$event.stopPropagation();

						if (index > 0) {
							var children = childrenElm.children();
							jQuery(children[index - 1]).before(children[index]);
							ArrayUtils.move(scope.columns, index, index - 1);
							ctrl.notifyChange('move', 'column', rowElm, { direction: 'left' });
							$timeout(function() { scope.highlightColumn(index - 1); });
						}
					};

					scope.moveColumnRight = function(index, $event) {
						$event.stopPropagation();

						if (index < scope.columns.length) {
							var children = childrenElm.children();
							jQuery(children[index]).before(children[index + 1]);
							ArrayUtils.move(scope.columns, index, index + 1);
							ctrl.notifyChange('move', 'column', rowElm, { direction: 'right' });
							$timeout(function() { scope.highlightColumn(index + 1); });
						}
					};

					// ---

					scope.$watch('columns', function() {
						scope.totalColumns = 0;
						if (scope.columns) {
							scope.equalSize = (gridSize % scope.columns.length === 0) ? (gridSize / scope.columns.length) : 0;
							angular.forEach(scope.columns, function(col) {
								scope.totalColumns += col.span + col.offset;
							});

							var children = childrenElm.children();
							if (children.length !== scope.columns.length) {
								throw new Error('Bad columns count: given ' + scope.columns.length + ' columns but ' +
									children.length + ' columns exist in the row.');
							}

							for (var i = 0; i < scope.columns.length; i++) {
								var column = scope.columns[i];
								var item = ctrl.getItemById(column.id);
								item.size = column.span;
								if (column.offset) {
									item.offset = column.offset;
								}
								else {
									delete item.offset;
								}
							}

							scope.highlightColumn();
						}
					}, true);

					// New blocks.
					scope.newBlockBefore = function() {
						ctrl.newBlockBefore();
					};

					scope.newBlockAfter = function() {
						ctrl.newBlockAfter();
					};

					scope.removeBlock = function() {
						var block = ctrl.getSelectedBlock();
						Dialog.confirmLocal(
							block,
							i18n.trans('m.rbs.admin.admin.structure_editor_remove_block_group | ucf'),
							'<strong>' + i18n.trans('m.rbs.admin.admin.structure_editor_remove_block_group_confirm | ucf') + '</strong>',
							{ placement: 'top' }
						).then(
							function() {
								ctrl.removeBlock(block);
								ctrl.notifyChange('remove', 'block', block);
							},
							function(result) { console.error(result); }
						);
					};

					$timeout(function() {
						scope.columns = getColumnsInfo();
					});

					function getColumnsInfo() {
						var cols = [];
						childrenElm.children().each(function(index, cellElm) {
							cellElm = jQuery(cellElm);
							var id = cellElm.data('id');
							var item = ctrl.getItemById(id);
							if (!item.size) {
								throw new Error("Bad column layout: column '" + index + "' should have a should have a size.");
							}
							cols.push({
								id: id,
								span: item.size,
								offset: item.offset || 0,
								childCount: cellElm.children('children').children().length
							});
						});
						return cols;
					}
				}
			};
		}
	]);
})();