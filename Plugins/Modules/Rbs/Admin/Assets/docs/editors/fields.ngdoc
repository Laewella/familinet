@ngdoc overview
@name Available fields
@sortOrder 200
@description

# Available fields

The following Directives help you in building editors with the correct layout.

Required attributes:

- `property` : name of the property of the Document that should be bound to the control.
- `label` : label that should be associated to the control.

Technically, since the edited Document is available as `document` in the editor's Scope,
a <code>ng-model</code> attribute will be added to the generated <code>&lt;input/&gt;</code> with a value like this:
<code>ng-model="document.&lt;property&gt;"</code>

Optionnal attributes:

- `required`

Of course, you can use common Angular Directives, such as `ng-hide`, `ng-show`, ...


## Text fields ##

### Simple text ###
```html
<rbs-field-text property="title" label="Title"></rbs-field-text>
```
See {@link RbsChange.directive:rbsFieldText `rbs-field-text`} Directive.

### E-mail ###
```html
<rbs-field-email property="emailAddress" label="E-mail address"></rbs-field-email>
```
See {@link RbsChange.directive:rbsFieldEmail `rbs-field-email`} Directive.

### URL ###
```html
<rbs-field-url property="websiteUrl" label="Website URL"></rbs-field-url>
```
See {@link RbsChange.directive:rbsFieldUrl `rbs-field-url`} Directive.

### Rich text ###
```html
<rbs-field-rich-text property="description" label="Description"></rbs-field-rich-text>
```
See {@link RbsChange.directive:rbsFieldRichText `rbs-field-rich-text`} Directive.


## Number fields ##

### Integer ###
```html
<rbs-field-integer property="quantity" label="Quantity"></rbs-field-integer>
```
See {@link RbsChange.directive:rbsFieldInteger `rbs-field-integer`} Directive.

### Float (decimal) ###
```html
<rbs-field-float property="weight" label="Weight"></rbs-field-float>
```
See {@link RbsChange.directive:rbsFieldFloat `rbs-field-float`} Directive.

### Price ###
```html
<rbs-field-price property="totalPrice" label="Total price"></rbs-field-price>
```
See {@link RbsChange.directive:rbsFieldPrice `rbs-field-price`} Directive.


## Boolean ##
```html
<rbs-field-boolean property="active" label="Active"></rbs-field-boolean>
```
See {@link RbsChange.directive:rbsFieldBoolean `rbs-field-boolean`} Directive.


## Document selectors ##

### Single Document picker ###
```html
<rbs-field-picker
    property="primaryImage"
    label="Primary image"
    accepted-model="Rbs_Media_Image"
    selector-title="Choose an image"
    allow-creation="true"
    allow-edition="true">
</rbs-field-picker>
```
See {@link RbsChange.directive:rbsFieldPicker `rbs-field-picker`} Directive.

### Multiple Documents picker ###
```html
<rbs-field-picker-multiple
    property="otherImages"
    label="Optionnal images"
    accepted-model="Rbs_Media_Image"
    selector-title="Choose optionnal images"
    allow-creation="true">
</rbs-field-picker-multiple>
```
See {@link RbsChange.directive:rbsFieldPickerMultiple `rbs-field-picker-multiple`} Directive.

### Dropdown listbox (select) ###
```html
<rbs-field-document-select
    property="website"
    label="Website"
    accepted-model="Rbs_Website_Website">
</rbs-field-document-select>
```
See {@link RbsChange.directive:rbsFieldDocumentSelect `rbs-field-document-select`} Directive.