@ngdoc overview
@name Document preview
@sortOrder 400
@description
# Document preview

You can activate the *Preview* feature by defining a <code>&lt;rbs-preview&gt;</code> tag.
The contents of this tag is the Angular template used for every Document, referenced by `doc`.

When the preview is requested, the Document is first loaded so that all its properties are available in the template.
The fully loaded Document is available as `doc.document`: you may use this one in the preview template.

```html
<rbs-document-list
    model="Rbs_Media_Image"
    filter-collection="filter">
    <column name="path" thumbnail="XS"></column>
    <column name="label" primary="true"></column>
    <preview>
        <img rbs-storage-image="(= doc.document.path =)" thumbnail="M" class="pull-left img-polaroid margin-right"/>
        <h4>(= doc.document.label =)</h4>
    </preview>
</rbs-document-list>
```
