<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Job;

/**
 * @name \Rbs\User\Job\ProfileCleanup
 */
class ProfileCleanup
{

	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function cleanUp(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$documentId = $job->getArgument('id');
		$model = $job->getArgument('model');
		if ($model !== 'Rbs_User_User' || !is_numeric($documentId))
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$qb = $documentManager->getNewQuery('Rbs_User_Profile');
			$qb->andPredicates($qb->eq('user', $documentId));
			$profiles = $qb->getDocuments()->toArray();
			foreach ($profiles as $profile)
			{
				$profile->delete();
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			throw $transactionManager->rollBack($e);
		}
	}
}