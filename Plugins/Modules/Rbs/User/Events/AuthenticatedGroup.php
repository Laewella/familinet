<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Events;

use Change\User\GroupInterface;
use Rbs\User\Documents\Group;

/**
* @name \Rbs\User\Events\AuthenticatedGroup
*/
class AuthenticatedGroup implements GroupInterface, \JsonSerializable
{
	/**
	 * @var Group
	 */
	protected $group;

	/**
	 * @param Group $group
	 */
	public function __construct(Group $group)
	{
		$this->group = $group;
	}

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->group->getId();
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->group->getRealm();
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return ['id' => $this->getId()];
	}
}