<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Documents;

/**
 * @name \Rbs\User\Documents\User
 */
class User extends \Compilation\Rbs\User\Documents\User
{

	const EVENT_HASH_PASSWORD = 'hashPassword';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); }, 5);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->onUpdated($event); }, 10);
		$eventManager->attach(self::EVENT_HASH_PASSWORD, function ($event) { $this->onHashPasswordUsingHashMethod($event); }, 5);
	}

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		$login = $this->getLogin();
		if (!\Change\Stdlib\StringUtils::isEmpty($login))
		{
			return $login;
		}
		return $this->getEmail();
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		return $this;
	}

	/**
	 * return string|null
	 */
	protected function getSaltString()
	{
		$cfg = $this->getConfiguration();
		return $cfg->getEntry('Rbs/User/salt');
	}

	/**
	 * @param string $password
	 * @return string
	 * @throws \RuntimeException
	 */
	protected function hashPassword($password)
	{
		$hashMethod = $this->getHashMethod();
		$callable = [$this, 'hashPassword' . ucfirst(strtolower($hashMethod))];
		if (is_callable($callable))
		{
			return $callable($password);
		}
		$eventManager = $this->getEventManager();
		$argv = $eventManager->prepareArgs(['password' => $password, 'hashMethod' => $hashMethod, 'hashPassword' => null]);
		$eventManager->trigger(self::EVENT_HASH_PASSWORD, $this, $argv);
		$hashPassword = $argv['hashPassword'] ?? null;
		if ($hashPassword && $hashPassword !== $password && is_string($hashPassword))
		{
			return $hashPassword;
		}
		throw new \RuntimeException('Unable to hash password');
	}

	/**
	 * @param string $password
	 * @return string
	 */
	protected function hashPasswordBcrypt($password)
	{
		$options = [];
		$cfg = $this->getConfiguration();
		$cost = $cfg->getEntry('Rbs/User/bcrypt/cost');
		if ($cost)
		{
			$options['cost'] = $cost;
		}
		$saltString = $this->getSaltString();
		if (!\Change\Stdlib\StringUtils::isEmpty($saltString) && \Change\Stdlib\StringUtils::length($saltString) > 21)
		{
			$options['salt'] = $saltString;
		}
		return password_hash($password, PASSWORD_BCRYPT, $options);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onHashPasswordUsingHashMethod(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('hashPassword') !== null)
		{
			return;
		}
		$password = $event->getParam('password');
		$hashMethod = $event->getParam('hashMethod');
		if (in_array($hashMethod, hash_algos()))
		{
			$saltString = $this->getSaltString();
			if (!\Change\Stdlib\StringUtils::isEmpty($saltString))
			{
				$event->setParam('hashPassword', hash($hashMethod, $saltString . '-' . $password));
				return;
			}
			else
			{
				$event->setParam('hashPassword', hash($hashMethod, $password));
				return;
			}
		}
		throw new \RuntimeException("hash $hashMethod does not exist");
	}

	/**
	 * @param string $password
	 * @return boolean
	 */
	public function checkPassword($password)
	{
		$hashMethod = $this->getHashMethod();
		$callable = [$this, 'checkPassword' . ucfirst(strtolower($hashMethod))];
		if (is_callable($callable))
		{
			return $callable($password, $this->getPasswordHash());
		}
		return $this->getPasswordHash() === $this->hashPassword($password);
	}

	/**
	 * @param string $password
	 * @param string $hash
	 * @return string
	 */
	protected function checkPasswordBcrypt($password, $hash)
	{
		return password_verify($password, $hash);
	}

	/**
	 * @var string
	 */
	protected $password;

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 * @return $this
	 */
	public function setPassword($password)
	{
		if (!\Change\Stdlib\StringUtils::isEmpty($password))
		{
			$this->password = $password;
			$this->setPasswordHash($this->hashPassword($this->password));
		}
		return $this;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$um = $restResult->getUrlManager();
			$restResult->addLink(new \Change\Http\Rest\V1\Link($um, $restResult->getBaseUrl() . '/Profiles/', 'profiles'));
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\User\UserDataComposer($event))->toArray());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onUpdated(\Change\Documents\Events\Event $event)
	{
		$modifiedPropertyNames = $event->getParam('modifiedPropertyNames');
		if (is_array($modifiedPropertyNames) && in_array('passwordHash', $modifiedPropertyNames))
		{
			$event->getApplicationServices()->getAuthenticationManager()->invalidateTokens($this->getId());
		}
	}
}