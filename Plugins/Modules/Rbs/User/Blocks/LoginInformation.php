<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Blocks;

use Change\Documents\Property;
use Change\Presentation\Blocks\Information;

/**
 * @name \Rbs\User\Blocks\LoginInformation
 */
class LoginInformation extends Information
{
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.user.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.user.admin.login', $ucf));
		$this->addParameterInformation('realm', Property::TYPE_STRING, true, 'web')
			->setLabel($i18nManager->trans('m.rbs.user.admin.login_realm', $ucf));
		$this->addParameterInformation('showTitle', \Change\Documents\Property::TYPE_BOOLEAN, false, true)
			->setLabel($i18nManager->trans('m.rbs.generic.admin.block_show_title', $ucf));
		$this->addParameterInformation('onLogin', Property::TYPE_STRING, false, 'doNothing')
			->setLabel($i18nManager->trans('m.rbs.user.admin.on_login_action', $ucf))
			->setCollectionCode('Rbs_User_OnLoginActions');
		$this->addParameterInformation('redirectionTargetId', Property::TYPE_DOCUMENTID, false)
			->setLabel($i18nManager->trans('m.rbs.user.admin.redirection_target_id', $ucf))
			->setAllowedModelsNames(['Rbs_Website_StaticPage', 'Rbs_Website_Section']);
	}
}
