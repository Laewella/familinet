<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Blocks;

/**
 * @name \Rbs\User\Blocks\Login
 */
class Login extends \Change\Presentation\Blocks\Standard\Block
{
	use \Rbs\Social\Blocks\SocialBlockTrait;

	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * Optional Event method: getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('realm', 'web');
		$parameters->addParameterMeta('onLogin', 'doNothing');
		$parameters->addParameterMeta('redirectionTargetId');
		$parameters->addParameterMeta('showTitle', true);
		$parameters->addParameterMeta('handleSocialLink', true);
		$parameters->setLayoutParameters($event->getBlockLayout());

		$user = $event->getAuthenticationManager()->getCurrentUser();
		if ($user->authenticated())
		{
			$parameters->setParameterValue('accessorId', $user->getId());
			$parameters->setParameterValue('accessorName', $user->getName());
		}

		if ($this->checkHandleSocialLink($event, $parameters))
		{
			$query = $event->getUrlManager()->getSelf()->getQueryAsArray();

			if ($query['Rbs_Social_notLinkedAccount'] ?? null)
			{
				$parameters->setParameterValue('notLinkedAccount', true);
				$parameters->setParameterValue('notLinkedProvider', $query['Rbs_Social_notLinkedAccount']);
			}
		}

		// @deprecated since 1.8.0: compatibility fallback.
		if (!$parameters->getParameterValue('onLogin') && $parameters->getParameter('reloadOnSuccess'))
		{
			trigger_error('Parameter reloadOnSuccess is deprecated in Rbs_User_Login block.', E_USER_WARNING);
			$parameters->setParameterValue('onLogin', 'reload');
		}
		// END @deprecated

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		if ($parameters->getParameter('onLogin') === 'redirectToTarget')
		{
			$redirectionTargetId = $parameters->getParameter('redirectionTargetId');
			$target = $documentManager->getDocumentInstance($redirectionTargetId);
			if (!$target)
			{
				$event->getApplication()->getLogging()->warn(__METHOD__, 'Invalid redirection target id:', $redirectionTargetId);
				$parameters->setParameterValue('onLogin', 'reload');
			}
			else
			{
				$parameters->setParameterValue('redirectionUrl', $event->getUrlManager()->getCanonicalByDocument($target)->normalize()->toString());
			}
		}

		if ($parameters->getParameter('handleSocialLink'))
		{
			$attributes['providers'] = $this->getProvidersData($documentManager);
		}

		return 'login.twig';
	}
}