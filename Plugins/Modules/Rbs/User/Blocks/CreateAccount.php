<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Blocks;

/**
 * @name \Rbs\User\Blocks\CreateAccount
 */
class CreateAccount extends \Change\Presentation\Blocks\Standard\Block
{
	use \Rbs\Social\Blocks\SocialBlockTrait;

	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * Optional Event method: getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('confirmationPage', 0);
		$parameters->addParameterMeta('allowedTitles');
		$parameters->addParameterMeta('handleSocialLink', true);
		$parameters->setLayoutParameters($event->getBlockLayout());

		$parameters->setParameterValue('authenticated', $event->getAuthenticationManager()->getCurrentUser()->authenticated());

		$httpRequest = $event->getHttpRequest();
		if ($httpRequest)
		{
			$email = (string)$httpRequest->getQuery('email');
			$token = (string)$httpRequest->getQuery('token');
			if ($email && $token)
			{
				$parameters->setParameterValue('token', $token);
				$parameters->setParameterValue('email', $email);
				$parameters->setNoCache(); // In this case the cache is useless.
			}
		}

		if ($this->checkHandleSocialLink($event, $parameters))
		{
			$query = $event->getUrlManager()->getSelf()->getQueryAsArray();
			if ($query['Rbs_Social_requestAccountCreated'])
			{
				$parameters->setParameterValue('socialRequestAccountCreated', true);
				if (isset($email))
				{
					$parameters->setParameterValue('socialEmail', $email);
				}
			}
			elseif ($query['Rbs_Social_requestEmail'])
			{
				$parameters->setParameterValue('requestEmail', true);
			}
			elseif ($query['Rbs_Social_accountAlreadyExists'])
			{
				$parameters->setParameterValue('accountAlreadyExists', true);
				if (isset($email))
				{
					$parameters->setParameterValue('socialEmail', $email);
				}
			}
		}

		$collectionManager = $event->getApplicationServices()->getCollectionManager();
		$allowedTitles = [];
		$collection = $collectionManager->getCollection('Rbs_User_Collection_Title');
		if ($collection)
		{
			foreach ($collection->getItems() as $tmp)
			{
				$allowedTitles[] = ['title' => $tmp->getTitle(), 'value' => $tmp->getValue()];
			}
		}
		$parameters->setParameterValue('allowedTitles', $allowedTitles);

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		// @deprecated since 1.8.0
		if ($parameters->getParameter('showComplementaryFields'))
		{
			trigger_error('Parameter showComplementaryFields is deprecated', E_USER_WARNING);
		}
		// END @deprecated

		if ($parameters->getParameter('handleSocialLink'))
		{
			$attributes['providers'] = $this->getProvidersData($event->getApplicationServices()->getDocumentManager());
		}

		return 'create-account.twig';
	}
}