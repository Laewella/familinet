(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * Controller for applications.
	 *
	 * @param $scope
	 * @param $routeParams
	 * @param REST
	 * @param i18n
	 * @param $http
	 * @param ArrayUtils
	 * @constructor
	 */
	function ApplicationsController($scope, $routeParams, REST, i18n, $http, ArrayUtils) {
		REST.resource($routeParams.id).then(function(user) {
			$scope.document = user;
		});

		$scope.reloadTokens = function() {
			REST.apiGet('user/userTokens/', { userId: $routeParams.id }).then(function(data) {
				$scope.tokens = data;
			});
		};

		$scope.reloadTokens();

		$scope.revokeToken = function(token) {
			if (confirm(i18n.trans('m.rbs.user.admin.confirm_revoke_token | ucf', { 'token': token.token }))) {
				var url = REST.getBaseUrl('user/revokeToken/');
				$http.post(url, { 'token': token.token }).then(
					function() {
						ArrayUtils.removeValue($scope.tokens, token);
					}
				);
			}
		};

		$scope.displayToken = function(token) {
			$scope.tokenToDisplay = token.token;
		};

		//sort
		$scope.predicate = 'application';
		$scope.reverse = false;
		$scope.isSortedOn = function(column) {
			return column == $scope.predicate;
		};
	}

	ApplicationsController.$inject =
		['$scope', '$routeParams', 'RbsChange.REST', 'RbsChange.i18n', '$http', 'RbsChange.ArrayUtils'];
	app.controller('Rbs_User_User_ApplicationsController', ApplicationsController);

	/**
	 * Controller for permission.
	 */
	function PermissionController($scope, $routeParams, REST, i18n, $http, ArrayUtils, $q) {
		REST.resource($routeParams.id).then(function(user) {
			$scope.document = user;
		});

		$scope.reloadPermissions = function() {
			REST.apiGet('user/permissionRules/', { accessorId: $routeParams.id }).then(function(data) {
				$scope.permissionRules = data;
			});
		};

		$scope.reloadPermissions();

		$scope.addPermissionRules = function() {
			$scope.newPermissionRules.roles = $scope.extractRolesValues();
			if ($scope.newPermissionRules.roles && $scope.newPermissionRules.privileges && $scope.newPermissionRules.resources) {
				var url = REST.getBaseUrl('user/addPermissionRules/');
				$http.post(url, { 'permissionRules': $scope.newPermissionRules }).then(
					function() {
						$scope.reloadPermissions();
					}
				);
			}
		};

		$scope.removePermissionRule = function(permissionRuleToRemove) {
			if (permissionRuleToRemove.rule_id) {
				if (confirm(i18n.trans('m.rbs.user.admin.confirm_remove_permission_rule | ucf', permissionRuleToRemove))) {
					var url = REST.getBaseUrl('user/removePermissionRule/');
					$http.post(url, { 'rule_id': permissionRuleToRemove.rule_id }).then(
						function() {
							ArrayUtils.removeValue($scope.permissionRules, permissionRuleToRemove);
						}
					);
				}
			}
		};

		$scope.removeAllPermissionRules = function() {
			if (confirm(i18n.trans('m.rbs.user.admin.confirm_remove_all_permission_rules | ucf',
					{ 'user': $scope.document.label }))) {
				var url = REST.getBaseUrl('user/removePermissionRule/');
				var promises = [];
				angular.forEach($scope.permissionRules, function(permissionRule) {
					promises.push($http.post(url, { 'rule_id': permissionRule.rule_id }));
				});
				$q.all(promises).then(function() {
					$scope.reloadPermissions();
				});
			}
		};

		$scope.extractRolesValues = function() {
			var roles = [];
			angular.forEach($scope.roles, function(bool, role) {
				if (bool !== false) {
					roles.push(role);
				}
			});
			return roles;
		};

		$scope.newPermissionRules = { 'accessor_id': $routeParams.id, 'roles': ['*'], 'privileges': ['*'], 'resources': ['0'] };

		$scope.roles = {};
		//get the permission roles collection
		$scope.permissionRoles = {};
		REST.action('collectionItems', { code: 'Rbs_Generic_Collection_PermissionRoles' }).then(function(data) {
			$scope.permissionRoles = data.items;
			delete($scope.permissionRoles['*']);
		});
		$scope.showRoles = false;

		//get the permission privileges collection
		$scope.permissionPrivileges = {};
		REST.action('collectionItems', { code: 'Rbs_Generic_Collection_PermissionPrivileges' }).then(function(data) {
			$scope.permissionPrivileges = data.items;
			delete($scope.permissionPrivileges['*']);
		});
		$scope.showPrivileges = false;

		//sort
		$scope.predicate = 'role';
		$scope.reverse = false;
		$scope.isSortedOn = function(column) {
			return column == $scope.predicate;
		};
	}

	PermissionController.$inject = ['$scope', '$routeParams', 'RbsChange.REST', 'RbsChange.i18n', '$http', 'RbsChange.ArrayUtils', '$q'];
	app.controller('Rbs_User_User_PermissionController', PermissionController);

	app.filter('filterRole', function() {
		return function(permissionRoles, queryFilter) {
			var result = {};
			if (queryFilter == '' || queryFilter == undefined) {
				result = permissionRoles;
			}
			else {
				angular.forEach(permissionRoles, function(role, key) {
					var q = queryFilter.toLowerCase();
					var t = role.title.toLowerCase();
					var l = role.label.toLowerCase();
					var k = key.toLowerCase();
					if (t.indexOf(q) != -1 || l.indexOf(q) != -1) {
						result[key] = role;
					}
					else if (k.indexOf(q) != -1) {
						result[key] = role;
					}
				});
			}
			return result;
		}
	});

	app.controller('Rbs_User_User_PublicProfileController', ['$scope', '$routeParams', 'RbsChange.REST', 'RbsChange.i18n', '$http',
		PublicProfileController]);

	function PublicProfileController($scope, $routeParams, REST, i18n, $http) {

		REST.resource($routeParams.id).then(function(user) {
			$scope.document = user;
			//Groups
			$scope.query = {
				'model': 'Rbs_User_Group',
				'join': [
					{
						'model': 'Rbs_User_User',
						'name': 'juser',
						'property': 'groups'
					}
				],
				'where': {
					'and': [
						{
							'op': 'eq',
							'lexp': {
								'property': 'id',
								'join': 'juser'
							},
							'rexp': {
								'value': user.id
							}
						}
					]
				}
			};
			//Profiles
			var url = user.META$.links.profiles.href;
			$http.get(url).then(
				function(result) {
					$scope.profile = result.data.Rbs_Admin;
				}
			);
		});
	}


	app.controller('Rbs_User_User_PopoverPreviewController', ['$scope', 'RbsChange.REST', '$http', PopoverPreviewController]);
	function PopoverPreviewController($scope, REST, $http) {
		$scope.$watch('message', function(message) {
			REST.resource(message.authorId).then(function(user) {
				$scope.document = user;
				var url = user.META$.links.profiles.href;
				$http.get(url).then(
					function(result) {
					$scope.profile = result.data.Rbs_Admin;
				}
				);
			});
		});
	}
})();