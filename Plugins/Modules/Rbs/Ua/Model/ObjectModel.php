<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\ObjectModel
 */
class ObjectModel implements ModelInterface
{
	use \Rbs\Ua\Model\DefinitionTrait;

	const API_PROPERTIES = '_API_PROPERTIES';

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var \Rbs\Ua\Model\ObjectModel|null
	 */
	protected $extends;

	/**
	 * @var array
	 */
	protected $properties = [];

	/**
	 * @var \Rbs\Ua\Model\FallbackContextInterface|null
	 */
	protected $fallbackContext;

	/**
	 * @var string|null
	 */
	protected $description;

	/**
	 * @param string $type
	 * @param array $definition
	 */
	public function __construct($type, array $definition)
	{
		$this->type = $type;
		$this->setDefinition($definition);
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return $this
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * @return null|\Rbs\Ua\Model\ObjectModel
	 */
	public function getExtends()
	{
		return $this->extends;
	}

	/**
	 * @return array
	 */
	public function getProperties()
	{
		return $this->properties;
	}

	/**
	 * @param string $name
	 * @return array|null
	 */
	public function getProperty($name)
	{
		return $this->properties[$name] ??  null;
	}

	/**
	 * @param string $name
	 * @param array $propertyData
	 * @return $this
	 */
	public function setProperty($name, array $propertyData)
	{
		$this->properties[$name] = $propertyData;
		return $this;
	}

	/**
	 * @param string $name
	 * @return \Rbs\Ua\Model\ModelInterface|null
	 */
	public function getPropertyModel($name)
	{
		return $this->models->getModel($this->properties[$name]['type'] ?? null);
	}

	/**
	 * @return null|\Rbs\Ua\Model\FallbackContextInterface
	 */
	public function getFallbackContext()
	{
		return $this->fallbackContext;
	}

	/**
	 * @param null|\Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return $this
	 */
	public function setFallbackContext(\Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		$this->fallbackContext = $fallbackContext;
		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return array|null
	 * @throws \LogicException
	 */
	public function normalize($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		if (!$this->isCompiled())
		{
			throw new \LogicException('type ' . $this->getType() . ' not compiled');
		}

		if ($value === null)
		{
			return $value;
		}
		if ($fallbackContext)
		{
			$value = $this->normalizeObject($value, $fallbackContext);
			if ($value === null)
			{
				return null;
			}
		}
		return $this->populateResult($value, []) ?: null;
	}

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return mixed
	 */
	protected function normalizeObject($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext)
	{
		if ($this->extends)
		{
			$value = $this->extends->normalizeObject($value, $fallbackContext);
		}
		$this->setFallbackContext($fallbackContext);
		return $fallbackContext->normalizeObject($value, $this);
	}

	/**
	 * @param $value
	 * @param array $result
	 * @return array
	 */
	protected function populateResult($value, array $result)
	{
		$properties = $this->properties;
		$rawProperties = [];
		if (is_array($value) || $value instanceof \Traversable)
		{
			foreach ($value as $key => $ov)
			{
				$rawProperties[$key] = $ov;
			}
		}
		elseif (is_object($value))
		{
			$rawProperties = get_object_vars($value);
			if (isset($rawProperties[self::API_PROPERTIES]))
			{
				$apiProperties = $rawProperties[self::API_PROPERTIES];
				unset($rawProperties[self::API_PROPERTIES]);
				if (is_array($apiProperties))
				{
					$rawProperties = array_merge($rawProperties, $apiProperties);
				}
			}
		}

		$fallbackContext = $this->getFallbackContext();
		foreach ($properties as $key => $property)
		{
			if (!array_key_exists($key, $result))
			{
				$v = $this->getObjectProperty($value, $rawProperties, $key, $property, $fallbackContext) ?? $property['defaultValue'] ?? null;
				if ($v !== null)
				{
					try
					{
						$v = $this->models->normalize($v, $property['type'], $fallbackContext);
					}
					catch (\RuntimeException $e)
					{
						throw new \RuntimeException($this->type . '::' . $key . '(' . $e->getMessage() . ')', 0, $e);
					}
				}

				if ($v === null && $property['required'])
				{
					throw new \RuntimeException('Invalid required property ' . $this->type . '::' . $key
						. ' on value type : ' . (is_object($value) ? get_class($value) : 'array'));
				}
				$result[$key] = $v;
			}
		}

		if ($this->extends)
		{
			$result = $this->extends->populateResult($value, $result);
		}
		return $result;
	}

	/**
	 * @param mixed $value
	 * @param array $rawProperties
	 * @param string $key
	 * @param array $property
	 * @param \Rbs\Ua\Model\FallbackContextInterface|null $fallbackContext
	 * @return mixed|null
	 */
	protected function getObjectProperty($value, array $rawProperties, $key, array $property, $fallbackContext = null)
	{
		if (array_key_exists($key, $rawProperties))
		{
			return $rawProperties[$key];
		}

		if (is_object($value))
		{
			$getter = [$value, 'get' . ucfirst($key)];
			if (is_callable($getter))
			{
				return $getter();
			}

			if ($value instanceof \Change\Documents\Interfaces\Localizable)
			{
				$i18nGetter = [$value->getCurrentLocalization(), 'get' . ucfirst($key)];
				if (is_callable($i18nGetter))
				{
					return $i18nGetter();
				}
			}
		}

		if ($fallbackContext)
		{
			$v = $fallbackContext->resolveProperty($value, $this, $key, $resolved);
			if ($resolved)
			{
				return $v;
			}
		}

		if (!array_key_exists('defaultValue', $property) && $property['required'])
		{
			throw new \RuntimeException('Unable to resolve ' . $this->type . '::' . $key
				. ' on value type : ' . (is_object($value) ? get_class($value) : 'array'));
		}
		return null;
	}

	/**
	 * @var \Rbs\Ua\Model\Models
	 */
	protected $models;

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 * @return $this
	 * @throws \LogicException
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		if (!$this->isCompiled())
		{
			$this->models = $models;
			$definition = $this->getDefinition();
			$extendsType = $definition['extends'] ?? null;
			$this->description = $definition['description'] ?? null;
			if ($extendsType)
			{
				$extends = $models->getModel($extendsType);
				if (!$extends instanceof ObjectModel)
				{
					throw new \LogicException('Invalid extends ' . $extendsType . ' on ' . $this->type);
				}
				$this->extends = $extends;
			}

			$properties = $definition['properties'] ?? false;
			if ($properties && is_array($properties))
			{
				foreach ($definition['properties'] as $name => $property)
				{
					$propertyType = $property['type'] ?? null;
					if ($propertyType)
					{
						$propertyModel = $models->getModel($propertyType);
						if ($propertyModel instanceof ModelInterface)
						{
							$property['required'] = (bool)($property['required'] ?? false);
							$this->properties[$name] = $property;
							continue;
						}
						else
						{
							throw new \LogicException('Invalid type ' . $propertyType . ' on ' . $this->type . '::' . $name);
						}
					}
					else
					{
						throw new \LogicException('Undefined type on ' . $this->type . '::' . $name);
					}
				}
			}
			elseif (!$this->extends)
			{
				throw new \LogicException('Undefined properties on ' . $this->type);
			}
			$this->markCompiled();
		}
		return $this;
	}

	/**
	 * @param string $type
	 * @return boolean
	 */
	public function isInstanceOf($type)
	{
		if ($this->type === $type)
		{
			return true;
		}
		if ($this->extends)
		{
			return $this->extends->isInstanceOf($type);
		}
		return false;
	}

	/**
	 * @api
	 * @param mixed|array $object
	 * @param array|callable $apiProperties
	 */
	public static function addAPIProperties(&$object, array $apiProperties)
	{
		if ($apiProperties)
		{
			if (is_object($object))
			{
				$current = $object->{self::API_PROPERTIES} ?? null;
				$object->{self::API_PROPERTIES} = ($current && is_array($current)) ? array_merge($current, $apiProperties) : $apiProperties;
			}
			elseif (is_array($object))
			{
				foreach ($apiProperties as $k => $v)
				{
					$object[$k] = $v;
				}
			}
		}
	}
}