<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\ArrayModel
 */
class ArrayModel implements ModelInterface
{
	use \Rbs\Ua\Model\DefinitionTrait;
	
	const SUFFIX = '[]';

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var string
	 */
	protected $itemType;

	/**
	 * @param string $type
	 */
	public function __construct($type)
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getItemType()
	{
		return $this->itemType;
	}

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return array|null
	 * @throws \LogicException
	 */
	public function normalize($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		if (!$this->isCompiled())
		{
			throw new \LogicException('type ' . $this->getType() . ' not compiled');
		}
		
		if (is_array($value) || $value instanceof \Traversable)
		{
			$result = [];
			foreach ($value as $idx => $ov)
			{
				try
				{
					$v = $this->models->normalize($ov, $this->getItemType(), $fallbackContext);
					if ($v !== null)
					{
						$result[] = $v;
					}
				}
				catch (\RuntimeException $e)
				{
					throw new \RuntimeException($this->itemType . '[' . $idx . '](' . $e->getMessage() . ')', 0, $e);
				}
			}
			return $result;
		}
		return null;
	}

	/**
	 * @var \Rbs\Ua\Model\Models
	 */
	private $models;
	
	/**
	 * @param \Rbs\Ua\Model\Models $models
	 * @return $this
	 * @throws \LogicException
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		if (!$this->isCompiled())
		{
			$this->models = $models;

			if (substr($this->type, -2) !== self::SUFFIX)
			{
				throw new \LogicException('Invalid Array suffix on ' . $this->getType());
			}
			$this->itemType = substr($this->type, 0, -2);

			$itemType = $models->getModel($this->itemType);
			if (!$itemType instanceof ModelInterface)
			{
				throw new \LogicException('Invalid itemType on ' . $this->getType());
			}
			$itemType->compile($models);
			$this->markCompiled();
		}
		return $this;
	}
}