<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\DefinitionTrait
 */
trait DefinitionTrait
{
	/**
	 * @var array
	 */
	protected $definition = [];

	/**
	 * @var boolean
	 */
	protected $compiled = false;

	/**
	 * @return bool
	 */
	public function isCompiled() 
	{
		return $this->compiled;
	}

	/**
	 * @return $this
	 */
	public function markCompiled()
	{
		$this->compiled = true;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getDefinition()
	{
		return $this->definition;
	}

	/**
	 * @param array $definition
	 * @return $this
	 */
	public function setDefinition(array $definition = [])
	{
		$this->definition = $definition;
		$this->compiled = false;
		return $this;
	}
}