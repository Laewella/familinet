<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\ScalarModel
 */
class ScalarModel implements ModelInterface
{
	const Boolean = 'Boolean';
	const Integer = 'Integer';
	const Float = 'Float';
	const DateTime = 'DateTime';
	const String = 'String';

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @param string $type
	 */
	public function __construct($type)
	{
		switch ($type)
		{
			case self::Boolean:
			case self::Integer:
			case self::Float:
			case self::DateTime:
			case self::String:
				$this->type = $type;
				break;
			default:
				$this->type = self::String;
		}
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return mixed
	 */
	public function normalize($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		switch ($this->type)
		{
			case self::Boolean:
				if (is_string($value))
				{
					return ($value === 'true' || $value === '1');
				}
				return $value !== null ? (bool)$value : null;
			case self::Integer:
				return $value === null ? null : (int)$value;

			case self::Float:
				return $value === null ? null : (float)$value;

			case self::DateTime:
				if (is_numeric($value))
				{
					$value = (new \DateTime())->setTimestamp((int)$value);
				}
				elseif (is_string($value))
				{
					$value = (new \DateTime($value));
				}
				return $value instanceof \DateTime ? $value->format(\DateTime::ATOM) : null;

			default:
				return $value === null ? null : (string)$value;
		}
	}

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 * @return $this
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		return $this;
	}
}