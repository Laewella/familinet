<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\Models
 */
class Models
{
	/**
	 * @var \Rbs\Ua\Model\ModelInterface[]
	 */
	protected $models = [];

	/**
	 * @return \Rbs\Ua\Model\ModelInterface[]
	 */
	public function getModels()
	{
		return $this->models;
	}

	public function __construct()
	{
		$this->addModel(new ScalarModel(ScalarModel::Boolean));
		$this->addModel(new ScalarModel(ScalarModel::Integer));
		$this->addModel(new ScalarModel(ScalarModel::Float));
		$this->addModel(new ScalarModel(ScalarModel::DateTime));
		$this->addModel(new ScalarModel(ScalarModel::String));
		$this->addModel(new VariantObjectModel());
	}

	/**
	 * @api
	 * @param \Rbs\Ua\Model\ModelInterface $model
	 * @return $this
	 */
	public function addModel(\Rbs\Ua\Model\ModelInterface $model)
	{
		if (isset($this->models[$model->getType()]))
		{
			throw new \LogicException('Duplicate declaration for model name ' . $model->getType());
		}
		$this->models[$model->getType()] = $model;
		return $this;
	}

	/**
	 * @api
	 * @param string $type
	 * @return \Rbs\Ua\Model\ModelInterface|null
	 */
	public function getModel($type)
	{
		if (!$type)
		{
			return null;
		}

		if (isset($this->models[$type]))
		{
			return $this->models[$type];
		}
		elseif (substr($type, -2) === ArrayModel::SUFFIX)
		{
			$model = new ArrayModel($type);
			$model->compile($this);
			$this->addModel($model);
			return $model;
		}
		elseif (substr($type, -2) === HashTableModel::SUFFIX)
		{
			$model = new HashTableModel($type);
			$model->compile($this);
			$this->addModel($model);
			return $model;
		}
		return null;
	}

	/**
	 * @api
	 * @param array $modelsDefinition
	 */
	public function setDefinitions(array $modelsDefinition)
	{
		foreach ($modelsDefinition as $type => $modelDefinition)
		{
			$this->setDefinition($type, $modelDefinition);
		}
	}

	/**
	 * @api
	 * @param string $type
	 * @param array $modelDefinition
	 */
	public function setDefinition($type, $modelDefinition)
	{
		if (is_array($modelDefinition) && is_string($type))
		{
			if (isset($modelDefinition['enum']))
			{
				$this->addModel(new EnumModel($type, $modelDefinition));
			}
			elseif (isset($modelDefinition['properties']) && is_array($modelDefinition['properties']))
			{
				$this->addModel(new ObjectModel($type, $modelDefinition));
			}
			else
			{
				throw new \LogicException('Unknown type ' . $type);
			}
		}
	}

	/**
	 * @param mixed $value
	 * @param string $type
	 * @param \Rbs\Ua\Model\FallbackContextInterface|null $fallbackContext
	 * @return mixed
	 */
	public function normalize($value, $type, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		$model = $this->getModel($type);
		if (!$model)
		{
			return null;
		}
		return $model->normalize($value, $fallbackContext);
	}


	/**
	 * @return $this
	 * @throws \LogicException
	 */
	public function compile()
	{
		foreach ($this->models as $model)
		{
			$model->compile($this);
		}
		return $this;
	}
}