/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRest');

	/**
	 * @ngdoc provider
	 * @id proximisRest.provider:proximisRestApiDefinitionProvider
	 * @name proximisRestApiDefinitionProvider
	 *
	 * @description
	 * Use `proximisRestApiDefinitionProvider` to configure REST API definition for `proximisRestApiDefinition`.
	 */
	app.provider('proximisRestApiDefinition', proximisRestApiDefinitionProvider);

	function proximisRestApiDefinitionProvider() {
		var services = {};

		/**
		 * @ngdoc method
		 * @methodOf proximisRestApiDefinitionProvider
		 * @name applyRestApi
		 *
		 * @description
		 * Register web services from REST API definition.
		 *
		 * @param {Object} definition The REST API definition.
		 */
		this.applyDefinition = function(definition) {
			angular.forEach(definition.api, function(service, name) {
				services[name] = service;
			});
		};

		/**
		 * @ngdoc service
		 * @id proximisRest.service:proximisRestApiDefinition
		 * @name proximisRestApiDefinition
		 *
		 * @description
		 * Provides methods to get information on REST API.
		 */
		this.$get = function() {
			var defaultParameters = {};

			function getService(name) {
				if (services.hasOwnProperty(name)) {
					return services[name];
				}
				return null;
			}

			function getOperation(service, method) {
				if (service && method && service.hasOwnProperty(method)) {
					return service[method];
				}
				return null;
			}

			function getData(name, method, params) {
				var service = getService(name);
				if (!service) {
					console.warn('proximisRestApiDefinition', 'No web service found for', name, 'in', services);
					return false;
				}

				var operation = getOperation(service, method);
				if (!operation) {
					console.warn('proximisRestApiDefinition', 'No operation found for', name, method);
					return false;
				}

				var path = service.path;

				params = angular.extend({}, defaultParameters, params);
				var bodyParams = {};
				var queryStringParams = {};

				angular.forEach(operation.parameters, function(parameter) {
					var paramName = parameter.name;
					switch (parameter['src']) {
						case 'path':
							if (!params.hasOwnProperty(paramName)) {
								throw new Error('Missing required parameter ' + paramName + '!');
							}
							path = path.replace('{' + paramName + '}', encodeURIComponent(params[paramName]));
							break;

						case 'query':
							if (params.hasOwnProperty(paramName)) {
								queryStringParams[paramName] = params[paramName];
							}
							else if (parameter.required) {
								throw new Error('Missing required parameter ' + paramName + '!');
							}
							break;

						case 'body':
							if (params.hasOwnProperty(paramName)) {
								bodyParams[paramName] = params[paramName];
							}
							else if (parameter.required) {
								throw new Error('Missing required parameter ' + paramName + '!');
							}
							break;
					}
				});

				return {
					path: path,
					method: method,
					data: bodyParams,
					params: queryStringParams
				}
			}

			// Public API.
			return {
				/**
				 * @ngdoc method
				 * @methodOf proximisRestApiDefinition
				 * @name getData
				 *
				 * @description
				 * Returns data to call the named web service for the given `method`.
				 *
				 * @param {string} name The service name.
				 * @param {string} method The HTTP method (`GET`, `POST`, `PUT` or `DELETE`).
				 * @param {Object=} params Parameters to append in the URL.
				 *
				 * @returns {Object|boolean} An object with `path`, `method`, `data` and `params` properties.
				 * Or `false` if there is no valid service found.
				 */
				getData: getData,

				/**
				 * @ngdoc method
				 * @methodOf proximisRestApiDefinition
				 * @name setDefaultParameter
				 *
				 * @description
				 * Sets the value of a default parameter.
				 *
				 * @param {string} name The parameter name.
				 * @param {*} value The parameter value.
				 */
				setDefaultParameter: function(name, value) {
					if (angular.isString(name) && name.length) {
						if (angular.isUndefined(value)) {
							if (defaultParameters.hasOwnProperty(name)) {
								delete defaultParameters[name];
							}
						}
						else {
							defaultParameters[name] = value;
						}
					}
					else {
						throw 'The argument "name" should be a non-empty string.';
					}
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRestApiDefinition
				 * @name getDefaultParameter
				 *
				 * @description
				 * Get the value of a default parameter.
				 *
				 * @param {string} name The parameter value.
				 */
				getDefaultParameter: function(name) {
					if (angular.isString(name) && name.length) {
						if (defaultParameters.hasOwnProperty(name)) {
							return defaultParameters[name];
						}
						return null;
					}
					else {
						throw 'The argument "name" should be a non-empty string.';
					}
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRestApiDefinition
				 * @name clearDefaultParameters
				 *
				 * @description
				 * Clears the default parameters.
				 */
				clearDefaultParameters: function() {
					defaultParameters = {};
				}
			};
		};
	}
})();
