/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRest');

	/**
	 * @ngdoc provider
	 * @id proximisRest.provider:proximisRestApiProvider
	 * @name proximisRestApiProvider
	 *
	 * @description
	 * Use `proximisRestApiProvider` to configure base URL for `proximisRestApi`.
	 */
	app.provider('proximisRestApi', [proximisRestApiProvider]);

	function proximisRestApiProvider() {
		var REST_BASE_URL;
		var language = 'fr_FR';

		/**
		 * @ngdoc method
		 * @methodOf proximisRestApiProvider
		 * @name setBaseUrl
		 *
		 * @description
		 * Sets the base URL for the proximisRestApi service.
		 *
		 * @param {string} url The base URL.
		 */
		this.setBaseUrl = function(url) {
			REST_BASE_URL = url;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRestApiProvider
		 * @name setLanguage
		 *
		 * @description
		 * Sets the default LCID for API
		 *
		 * @param {string} LCID The default LCID.
		 */
		this.setLanguage = function(LCID) {
			language = LCID;
		};

		this.$get = ['$http', '$rootScope', 'ProximisRestEvents',
			function($http, $rootScope, ProximisRestEvents) {
				/**
				 * Returns the HTTP Config that should be used for every REST call.
				 * Special headers, such as Accept-Language, and authentication stuff go here :)
				 *
				 * @returns {Object}
				 *   - **headers** - `{Object}`
				 *     - **Accept-Language** - `{string}`
				 */
				function getHttpConfig(transformResponseFn) {
					var config = {
						headers: {
							'Accept-Language': angular.lowercase(language).replace('_', '-')
						}
					};

					if (angular.isFunction(transformResponseFn)) {
						config.transformResponse = transformResponseFn;
					}

					return config;
				}

				/**
				 * @param {string} method
				 * @param {string} url
				 * @param {Object} result
				 */
				function onError(method, url, result) {
					console.error('proximisRestApi ' + method + ' result', result, url, ' from: ', window.location.href);
					if (result.status === 401) {
						$rootScope.$emit(ProximisRestEvents.Unauthorized, result.data);
					}
				}

				/**
				 * Returns the full URL of an action based on its `relativePath`, suitable to use with `$http`.
				 *
				 * @param {string} relativePath Relative path.
				 * @returns {string} Full path to use with `$http`.
				 */
				function getBaseUrl(relativePath) {
					var relativeParts = relativePath.split('/');
					for (var i = 0; i < relativeParts.length; i++) {
						relativeParts[i] = encodeURIComponent(relativeParts[i]);
					}
					return REST_BASE_URL + relativeParts.join('/');
				}

				/**
				 * Complete the given `url` by adding `queryParams`.
				 *
				 * @param {string} url The URL to use. This URL is supposed to not have params yet.
				 * @param {Object} queryParams Hash object representing the parameters to append to the URL.
				 * @returns {string} The updated URL.
				 */
				function makeUrl(url, queryParams) {

					function serialize(query, obj, prefix) {
						if (angular.isUndefined(obj) || obj === null) {
							return;
						}

						if (angular.isDate(obj)) {
							query.push(prefix + '=' + encodeURIComponent(moment(value).format()));
						}
						else if (angular.isArray(obj)) {
							for (var i = 0; i < obj.length; i++) {
								serialize(query, obj[i], prefix + '[' + i + ']');
							}
						}
						else if (angular.isObject(obj)) {
							query.push(prefix + '=' + encodeURIComponent(angular.toJson(obj)));
						}
						else {
							query.push(prefix + '=' + encodeURIComponent(obj));
						}
					}

					var query = [];
					if (angular.isObject(queryParams)) {
						angular.forEach(queryParams, function(value, key) {
							serialize(query, value, key);
						});
					}

					if (query.length) {
						url += '?' + query.join('&');
					}
					return url;
				}

				/**
				 * @ngdoc service
				 * @id proximisRest.service:proximisRestApi
				 * @name proximisRestApi
				 *
				 * @description
				 * Provides methods to deal with REST services.
				 */
				return {
					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name get
					 *
					 * @description
					 * Calls a REST service with its full URL.
					 *
					 * @param {string|Object} relativePath The relative path or an object containing:
					 *      * `path` (string, required)
					 *      * `params` (Object, optional)
					 *      * `transformer` (Function, optional)
					 * @config {string} path
					 * @config {Object=} params
					 * @config {Function=} transformer
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {HttpPromise} Promise resolved with the action's result.
					 */
					get: function(relativePath, params, transformer) {
						if (angular.isObject(relativePath)) {
							if (!angular.isObject(params) && angular.isObject(relativePath['params'])) {
								params = relativePath['params'];
							}
							if (!angular.isFunction(transformer) && angular.isFunction(relativePath['transformer'])) {
								transformer = relativePath['transformer'];
							}
							relativePath = relativePath['path'];
						}

						var url = makeUrl(getBaseUrl(relativePath), params);

						var httpPromise = $http.get(url, getHttpConfig(transformer));
						httpPromise.then(null, function(result) { onError('GET', url, result); });
						return httpPromise;
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name post
					 *
					 * @description
					 * POST on a REST service with its full URL.
					 *
					 * @param {string|Object} relativePath The relative path or an object containing:
					 *      * `path` (string, required)
					 *      * `data` (Object, optional)
					 *      * `params` (Object, optional)
					 *      * `transformer` (Function, optional)
					 * @config {string} path
					 * @config {Object=} data
					 * @config {Object=} params
					 * @config {Function=} transformer
					 * @param {Object=} data Request content.
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {HttpPromise} Promise resolved with the action's result.
					 */
					post: function(relativePath, data, params, transformer) {
						if (angular.isObject(relativePath)) {
							if (!angular.isObject(data) && angular.isObject(relativePath['data'])) {
								data = relativePath['data'];
							}
							if (!angular.isObject(params) && angular.isObject(relativePath['params'])) {
								params = relativePath['params'];
							}
							if (!angular.isFunction(transformer) && angular.isFunction(relativePath['transformer'])) {
								transformer = relativePath['transformer'];
							}
							relativePath = relativePath['path'];
						}

						var url = makeUrl(getBaseUrl(relativePath), params);

						var httpPromise = $http.post(url, data, getHttpConfig(transformer));
						httpPromise.then(null, function(result) { onError('POST', url, result); });
						return httpPromise;
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name put
					 *
					 * @description
					 * PUT on a REST service with its full URL.
					 *
					 * @param {string|Object} relativePath The relative path or an object containing:
					 *      * `path` (string, required)
					 *      * `data` (Object, optional)
					 *      * `params` (Object, optional)
					 *      * `transformer` (Function, optional)
					 * @config {string} path
					 * @config {Object=} data
					 * @config {Object=} params
					 * @config {Function=} transformer
					 * @param {Object=} data Request content.
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {HttpPromise} Promise resolved with the action's result.
					 */
					put: function(relativePath, data, params, transformer) {
						if (angular.isObject(relativePath)) {
							if (!angular.isObject(data) && angular.isObject(relativePath['data'])) {
								data = relativePath['data'];
							}
							if (!angular.isObject(params) && angular.isObject(relativePath['params'])) {
								params = relativePath['params'];
							}
							if (!angular.isFunction(transformer) && angular.isFunction(relativePath['transformer'])) {
								transformer = relativePath['transformer'];
							}
							relativePath = relativePath['path'];
						}

						var url = makeUrl(getBaseUrl(relativePath), params);

						var httpPromise = $http.put(url, data, getHttpConfig(transformer));
						httpPromise.then(null, function(result) { onError('PUT', url, result); });
						return httpPromise;
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name delete
					 *
					 * @description
					 * DELETE on a REST service with its full URL.
					 *
					 * @param {string|Object} relativePath The relative path or an object containing:
					 *      * `path` (string, required)
					 *      * `params` (Object, optional)
					 *      * `transformer` (Function, optional)
					 * @config {string} path
					 * @config {Object=} params
					 * @config {Function=} transformer
					 * @param {Object=} params URL parameters.
					 * @param {Function=} transformer Response Transformer to use.
					 *
					 * @returns {HttpPromise} Promise resolved with the action's result.
					 */
					delete: function(relativePath, params, transformer) {
						if (angular.isObject(relativePath)) {
							if (!angular.isObject(params) && angular.isObject(relativePath['params'])) {
								params = relativePath['params'];
							}
							if (!angular.isFunction(transformer) && angular.isFunction(relativePath['transformer'])) {
								transformer = relativePath['transformer'];
							}
							relativePath = relativePath['path'];
						}

						var url = makeUrl(getBaseUrl(relativePath), params);

						var httpPromise = $http.delete(url, getHttpConfig(transformer));
						httpPromise.then(null, function(result) { onError('DELETE', url, result); });
						return httpPromise;
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name sendData
					 *
					 * @description
					 * GET, POST, PUT, DELETE on a REST service with its full URL.
					 *
					 * @param {Object} definitionData An object containing:
					 *      * `path` (string, required)
					 *      * `method` (string, required)
					 *      * `data` (Object, optional)
					 *      * `params` (Object, optional): query parameters
					 *      * `transformer` (Function, optional)
					 * @config {string} path
					 * @config {string} method
					 * @config {Object=} data
					 * @config {Object=} params
					 * @config {Function=} transformer
					 * @returns {HttpPromise} Promise resolved with the action's result.
					 */
					sendData: function(definitionData) {
						if (!angular.isObject(definitionData)) {
							throw new Error('Illegal argument definitionData');
						}
						var relativePath = definitionData.path;
						if (definitionData.method && relativePath) {
							switch (definitionData.method) {
								case 'GET' :
									return this.get(relativePath, definitionData.params, definitionData.transformer);
								case 'POST' :
									return this.post(relativePath, definitionData.data, definitionData.params, definitionData.transformer);
								case 'PUT' :
									return this.put(relativePath, definitionData.data, definitionData.params, definitionData.transformer);
								case 'DELETE' :
									return this.delete(relativePath, definitionData.params, definitionData.transformer);
							}
						}
						throw new Error('Illegal end point : (' + method + ') ' + relativePath);
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name upload
					 *
					 * @description
					 * Upload on a REST service with its full URL.
					 *
					 * @param {Object} file The File Object to upload:
					 * @param {string|Object} relativePath The relative path or an object containing:
					 *      * `path` (string, required)
					 *      * `params` (Object, optional)
					 * @config {string} path
					 * @config {Object=} params
					 * @param {Object=} params URL parameters.
					 *
					 * @returns {HttpPromise} Promise resolved with the action's result.
					 */
					upload: function(file, relativePath, params) {
						if (angular.isObject(relativePath)) {
							if (!angular.isObject(params) && angular.isObject(relativePath['params'])) {
								params = relativePath['params'];
							}
							relativePath = relativePath['path'];
						}

						var url = makeUrl(getBaseUrl(relativePath), params);
						var fd = new FormData();
						fd.append('file', file);

						var httpPromise = $http.post(url, fd, {
							transformRequest: angular.identity,
							headers: { 'Content-Type': undefined }
						});

						httpPromise.then(null, function(result) { onError('UPLOAD', url, result); });
						return httpPromise;
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name makeUrl
					 *
					 * @description
					 * Generate the full URL of a REST service.
					 *
					 * @param {string|Object} relativePath The relative path or an object containing:
					 *      * `path` (string, required)
					 *      * `params` (Object, optional)
					 * @config {string} path
					 * @config {Object=} params
					 * @param {Object=} params URL parameters.
					 */
					makeUrl: function(relativePath, params) {
						if (angular.isObject(relativePath)) {
							if (!angular.isObject(params) && angular.isObject(relativePath['params'])) {
								params = relativePath['params'];
							}
							relativePath = relativePath['path'];
						}

						return makeUrl(getBaseUrl(relativePath), params);
					},

					/**
					 * @ngdoc method
					 * @methodOf proximisRestApi
					 * @name setLanguage
					 *
					 * @description
					 * Set the LCID for the rest API calls.
					 *
					 * @param {string} LCID The LCID, for example: `fr_FR`.
					 */
					setLanguage: function(LCID) {
						if (angular.isString(LCID)) {
							language = LCID;
						}
						else {
							throw new Error('LCID parameter must be a string');
						}
					}
				};
			}
		];
	}
})();

