/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('RbsUaAppList', ['$scope', '$window', 'RbsChange.REST', 'RbsChange.NotificationCenter', 'RbsChange.i18n', 'RbsChange.ErrorFormatter',
		function($scope, $window, REST, NotificationCenter, i18n, ErrorFormatter) {

			REST.call(REST.getBaseUrl('ua/apps')).then(
				function(data) {
					$scope.apps = data;
				},
				function(error) {
					NotificationCenter.error(i18n.trans('m.rbs.ua.admin.apps_error | ucf'),
						ErrorFormatter.format(error));
				}
			);

			$scope.copy = function($event, app, key) {

				var body = angular.element($window.document.body);
				var textarea = angular.element('<textarea/>');
				textarea.css({
					position: 'fixed',
					opacity: '0'
				});
				textarea.val(app[key]);
				body.append(textarea);
				textarea[0].select();
				if (!document.execCommand('copy')) {
					app.limit = 64;
					var td = angular.element($event.currentTarget).parent().parent();
					var range;
					if (document.selection) {
						range = body[0].createTextRange();
						range.moveToElementText(td[0]);
						range.select();
					}
					else if (window.getSelection) {
						range = document.createRange();
						range.selectNode(td[0]);
						var sel = window.getSelection();
						sel.removeAllRanges();
						sel.addRange(range);
					}
				}
				textarea.remove();
			}

		}]);

})();