/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxDateTimeIntervalSelector
	 * @name pxDateTimeSelector
	 *
	 * @description
	 * A directive to select a date and time interval.
	 *
	 * @param {Date} startDate Start date of the interval.
	 * @param {Date} endDate End date of the interval.
	 */
	app.directive('pxDateTimeIntervalSelector', [function() {
		return {
			restrict: 'A',
			scope: {
				fieldName: '@',
				startDate: '=',
				endDate: '='
			},
			templateUrl: 'Rbs/Ua/Proximis/directives/date-time-interval-selector.twig'
		};
	}]);
})();