/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxLabel
	 * @name pxLabel
	 *
	 * @description
	 * Basic Directive to wrap fields.
	 *
	 * @param {string} pxLabel The label tag content.
	 * @param {string=} mode The rendering mode ('horizontal'|'vertical', 'horizontal' by default).
	 */
	app.directive('pxLabel', ['ProximisEvents', function(ProximisEvents) {
		return {
			restrict: 'A',
			scope: true,
			templateUrl: 'Rbs/Ua/Proximis/directives/label.twig',
			transclude: true,

			link: function(scope, element, attributes, ctrl, transclude) {
				scope.renderingMode = attributes.mode || 'horizontal';
				var labelNode = element.children('label');
				labelNode.html(attributes['pxLabel']);

				element.addClass('form-group property');
				transclude(scope.$parent, function(transEl) {
					var propertyContent = element.find('.property-content');
					propertyContent.on(ProximisEvents.FieldIdCreated, function(event, extraParameter) {
						labelNode.attr('for', extraParameter.id);
						event.stopPropagation();
					});
					propertyContent.append(transEl);
				});
			}
		}
	}]);
})();