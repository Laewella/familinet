/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis:pxDocumentSelector
	 * @name pxDocumentSelector
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {string|Array|Object} acceptedModels A model name, a model array or an object describing model filters.
	 *
	 * List of available filters:
	 *  * `publishable` (boolean)
	 *  * `activable` (boolean)
	 *  * `editable` (boolean)
	 *  * `instanceOf` (string)
	 *
	 * **Examples:**
	 *  * `select-model="'Rbs_Website_StaticPage'"`
	 *  * `select-model="['Rbs_Website_StaticPage', 'Rbs_Website_Menu']"`
	 *  * `select-model="{ publishable: true }"`
	 *
	 * @param {string} searchRestService The webservice to search documents.
	 * @param {string} loadRestService The webservice to load documents.
	 * @param {Object=} restServiceParams Additional webservice parameters.
	 *
	 * The default REST services use an optional `urlContext` parameter to configure published document URLs generation.
	 * If it is provided, this parameter should be an object with the following properties:
	 *  * `websiteId` (integer, required)
	 *  * `sectionId` (integer, optional)
	 *  * `LCID` (string, required)
	 *
	 * @param {string=} selectorTitle The placeholder of the selector.
	 * @param {boolean=} readonly Set to true to make the selector readonly.
	 * @param {string=} fieldName If set, a data-px-field="{value}" directive will be added to the main field.
	 *
	 * @description
	 * Displays a Document selector to let the user select a single document.
	 */
	app.directive('pxDocumentSelector', ['proximisDocumentModels', 'proximisRestApi', 'proximisRestApiDefinition',
		function(proximisDocumentModels, proximisRestApi, proximisRestApiDefinition) {
			return {
				restrict: 'A',
				templateUrl: 'Rbs/Ua/Proximis/directives/document-selector.twig',
				require: 'ngModel',
				scope: true,
				controller: 'ProximisItemSelectorController',

				link: function(scope, element, attrs, ngModel) {
					documentSelectorFunction(scope, element, attrs, ngModel, false, proximisDocumentModels, proximisRestApi,
						proximisRestApiDefinition);
				}
			};
		}
	]);

	/**
	 * @ngdoc directive
	 * @id proximis:pxDocumentsSelector
	 * @name pxDocumentsSelector
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {string|Array|Object} acceptedModels A model name, a model array or an object describing model filters.
	 *
	 * List of available filters:
	 *  * `publishable` (boolean)
	 *  * `activable` (boolean)
	 *  * `editable` (boolean)
	 *  * `instanceOf` (string)
	 *
	 * **Examples:**
	 *  * `select-model="'Rbs_Website_StaticPage'"`
	 *  * `select-model="['Rbs_Website_StaticPage', 'Rbs_Website_Menu']"`
	 *  * `select-model="{ publishable: true }"`
	 *
	 * @param {string} searchRestService The webservice to search documents.
	 * @param {string} loadRestService The webservice to load documents.
	 * @param {Object=} restServiceParams Additional webservice parameters.
	 *
	 * The default REST service uses an optional `urlContext` parameter to configure published document URLs generation.
	 * If it is provided, this parameter should be an object with the following properties:
	 *  * `websiteId` (integer, required)
	 *  * `sectionId` (integer, optional)
	 *  * `LCID` (string, required)
	 *
	 * @param {string=} selectorTitle The placeholder of the selector.
	 * @param {boolean=} readonly Set to true to selector readonly.
	 * @param {string=} fieldName If set, a data-px-field="{value}" directive will be added to the main field.
	 * 
	 * @description
	 * Displays a Document selector to let the user select multiple documents.
	 */
	app.directive('pxDocumentsSelector', ['proximisDocumentModels', 'proximisRestApi', 'proximisRestApiDefinition',
		function(proximisDocumentModels, proximisRestApi, proximisRestApiDefinition) {
			return {
				restrict: 'A',
				templateUrl: 'Rbs/Ua/Proximis/directives/document-selector.twig',
				require: 'ngModel',
				scope: true,
				controller: 'ProximisItemSelectorController',

				link: function(scope, element, attrs, ngModel) {
					documentSelectorFunction(scope, element, attrs, ngModel, true, proximisDocumentModels, proximisRestApi,
						proximisRestApiDefinition);
				}
			};
		}
	]);

	function documentSelectorFunction(scope, element, attrs, ngModel, multiple, proximisDocumentModels, proximisRestApi, proximisRestApiDefinition) {
		scope.multiple = multiple;
		scope.readonly = !!attrs.readonly;
		scope.disableReordering = !multiple || scope.readonly;
		scope.selectorTitle = attrs.selectorTitle;
		scope.selectorElement = element;
		scope.fieldName = attrs.fieldName || null;

		var searchRestService = attrs['searchRestService'];
		var loadRestService = attrs['loadRestService'];
		if (!searchRestService || !loadRestService) {
			console.error('Attributes data-search-rest-service and data-load-rest-service are required!')
		}

		scope.models = { filtered: [], selected: null };

		attrs.$observe('acceptedModels', function() {
			var value = scope.$eval(attrs['acceptedModels']);
			if (angular.isString(value)) {
				value = [value];
			}

			var filter = value;
			if (angular.isArray(filter)) {
				scope.models.filters = { name: filter };
			}
			else if (angular.isObject(filter)) {
				scope.models.filters = filter;
			}
			else {
				scope.models.filters = { abstract: false, editable: true };
			}
			scope.models.filtered = proximisDocumentModels.getByFilter(scope.models.filters);
		});

		scope.$watchCollection('models.filtered', function(modelsFiltered) {
			if (angular.isArray(modelsFiltered)) {
				if (scope.models.selected) {
					selectModelName(scope.models.selected.name);
				}
				//noinspection JSUnresolvedVariable
				if (!angular.isObject(scope.models.selected) && modelsFiltered.length) {
					selectModelName(modelsFiltered[0].name);
				}
			}
		});

		function selectModelName(modelName) {
			if (!modelName) {
				return;
			}

			if (angular.isObject(scope.models.filtered)) {
				for (var i = 0; i < scope.models.filtered.length; i++) {
					if (scope.models.filtered[i].name === modelName) {
						scope.models.selected = scope.models.filtered[i];
						break;
					}
				}
			}
		}

		// viewValue => modelValue
		ngModel.$parsers.unshift(function(viewValue) {
			if (viewValue === undefined) {
				return viewValue;
			}

			var modelValue;
			if (multiple) {
				modelValue = [];
				angular.forEach(viewValue, function(item) {
					if (angular.isObject(item) && angular.isObject(item._meta) && item._meta.id) {
						modelValue.push(item._meta.id);
					}
					else {
						console.error('[pxDocumentsSelector] Invalid document', item);
					}
				});
			}
			else {
				modelValue = 0;
				if (angular.isObject(viewValue)) {
					if (angular.isObject(viewValue._meta) && viewValue._meta.id) {
						modelValue = viewValue._meta.id;
					}
					else {
						console.error('[pxDocumentSelector] Invalid document', viewValue);
					}
				}
			}
			return modelValue;
		});

		// modelValue => viewValue
		ngModel.$formatters.unshift(function(modelValue) {
			if (modelValue === undefined) {
				return modelValue;
			}
			var viewValue = multiple ? [] : null;
			var oldList = scope.items.list;
			var itemList = [];

			if (multiple) {
				if (angular.isArray(modelValue)) {
					var ids = [], item;
					angular.forEach(modelValue, function(id) {
						if (angular.isNumber(id) && id > 0) {
							item = scope.tools.getItemById(oldList, id);
							if (item) {
								itemList.push(item);
							}
							else {
								ids.push(id);
							}
						}
						else {
							console.error('[pxDocumentsSelector] Invalid number value:', id);
						}
					});
					if (ids.length) {
						var params = completeParams({ documentIds: ids });
						var promise = proximisRestApi.get(proximisRestApiDefinition.getData(loadRestService, 'GET', params));
						promise.then(
							function(result) {
								angular.forEach(result.data.items, function(item) {
									itemList.push(item);
								});
							},
							function(error) {
								console.error('[pxDocumentsSelector]', error);
							}
						);
					}
					viewValue = scope.tools.arrayCopy(itemList);
				}
			}
			else {
				if (angular.isNumber(modelValue) && modelValue > 0) {
					params = completeParams({ documentIds: [modelValue] });
					promise = proximisRestApi.get(proximisRestApiDefinition.getData(loadRestService, 'GET', params));
					promise.then(
						function(result) {
							viewValue = result.data.items[0];
							itemList.push(viewValue);
						},
						function(error) {
							console.error('[pxDocumentSelector]', error);
						}
					);
				}
			}

			scope.items.list = itemList;
			return viewValue;
		});

		// Watch from changes coming from the list which is bound to `scope.items.list`.
		scope.$watchCollection('items.list', function() {
			if (scope.items.list.length === 0 && ngModel.$viewValue === undefined) {
				return;
			}
			if (multiple) {
				ngModel.$setViewValue(scope.tools.arrayCopy(scope.items.list));
			}
			else {
				ngModel.$setViewValue(scope.items.list.length ? scope.items.list[0] : null);
			}
		});

		scope.canAutoComplete = function() {
			return scope.models.selected && scope.autoComplete.value.trim().length > 0;
		};

		scope.getAutoCompleteDefinition = function() {
			var value = angular.lowercase(scope.autoComplete.value.trim());
			var params = { model: scope.models.selected.name, search: value, offset: scope.autoComplete.offset, limit: scope.autoComplete.limit };
			return proximisRestApiDefinition.getData(searchRestService, 'GET', completeParams(params));
		};

		scope.$watch('models.selected.name', function() {
			if (angular.isObject(scope.autoComplete) && angular.isFunction(scope.autoComplete.refresh)) {
				scope.autoComplete.refresh();
			}
		});

		function completeParams(params) {
			if (attrs['restServiceParams']) {
				angular.forEach(scope.$eval(attrs['restServiceParams']), function(value, key) {
					params[key] = value;
				});
			}
			return params;
		}
	}
})();