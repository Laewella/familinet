(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxPagination
	 * @name pxPagination
	 *
	 * @description
	 * A directive showing a pagination.
	 *
	 * The given pagination object will be updated on user interaction so, parent directives/controllers may use a `$watch`
	 * to handle `currentPage` and `itemsPerPage` changes.
	 *
	 * Make sure to set `loading` property of the pagination object to `true` during the data loading.
	 *
	 * @param {Object} pxPagination The pagination object:
	 *  <ul>
	 *      <li>`itemsPerPage` - integer, optional, default `20` - will be updated by the directive</li>
	 *      <li>`currentPage` - integer, optional, default `1` - will be updated by the directive</li>
	 *      <li>`totalItems` - integer, optional, default `0`</li>
	 *      <li>`loading` - boolean, optional, default `false`</li>
	 *  </ul>
	 */
	app.directive('pxPagination', ['proximisPagination', function(proximisPagination) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/pagination.twig',
			scope: {
				pagination: '=pxPagination'
			},
			link: function(scope) {
				scope.predefinedPageSizes = proximisPagination.predefinedPageSizes;
				scope.data = {};
				scope.smallWidth = window.screen.width <= 991;

				proximisPagination.initPagination(scope.pagination);

				scope.$watch('pagination.totalItems', function () {
					scope.data.pageCount = Math.ceil(scope.pagination.totalItems / scope.pagination.itemsPerPage);
				});

				scope.updatePageSize = function(newPerPage) {
					if (!newPerPage) {
						return;
					}
					var oldPerPage = scope.pagination.itemsPerPage;
					scope.pagination.currentPage = Math.floor(((scope.pagination.currentPage - 1) * oldPerPage) / newPerPage) + 1;
					scope.pagination.itemsPerPage = newPerPage;
					scope.data.pageCount = Math.ceil(scope.pagination.totalItems / scope.pagination.itemsPerPage);
				};
			}
		};
	}]);
})();