/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(jQuery) {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis:pxItemSelectorTokenList
	 * @name pxItemSelectorTokenList
	 * @restrict A
	 *
	 * @description
	 * Used in document and path rule selectors to handle the selected items.
	 */
	app.directive('pxItemSelectorTokenList', ['$timeout', 'proximisCoreArray', function($timeout, proximisCoreArray) {
		return {
			restrict: 'A',
			link: function(scope, element) {
				var tokenList = element,
					dragging, isHandle, startIndex, stopIndex,
					placeholder = jQuery('<li class="sortable-placeholder"></li>');

				scope.tokenList = {
					removeItem: function(index) {
						proximisCoreArray.remove(scope.items.list, index, index);
					}
				};

				// Enable drag and drop to reorder the items.
				if (!scope.disableReordering) {
					tokenList.on({
							'mousedown': function() {
								isHandle = true;
							},
							'mouseup': function() {
								isHandle = false;
							}
						},
						'i.icon-reorder'
					);

					tokenList.on({
							'dragstart': function(e) {
								if (!isHandle) {
									return false;
								}
								isHandle = false;
								//noinspection JSUnresolvedVariable
								var dt = e.originalEvent.dataTransfer;
								dt.effectAllowed = 'move';
								dragging = jQuery(this);
								dragging.addClass('sortable-dragging');
								startIndex = dragging.index();
								dt.setData('Text', dragging.text());
							},

							'dragend': function() {
								dragging.removeClass('sortable-dragging').show();
								placeholder.detach();
								dragging = null;
							}
						},
						'li[data-id]'
					);

					tokenList.on({
							'dragenter': function(e) {
								e.preventDefault();
								//noinspection JSUnresolvedVariable
								e.originalEvent.dataTransfer.dropEffect = 'move';
							},

							'dragover': function(e) {
								e.preventDefault();
								//noinspection JSUnresolvedVariable
								e.originalEvent.dataTransfer.dropEffect = 'move';

								if (dragging) {
									if (!jQuery(this).is(placeholder)) {
										dragging.hide();
										placeholder.height(dragging.height());
										jQuery(this)[placeholder.index() < jQuery(this).index() ? 'after' : 'before'](placeholder);
										if (placeholder.index() > startIndex) {
											placeholder.html(placeholder.index());
										}
										else {
											placeholder.html(placeholder.index() + 1);
										}
									}
								}
							},

							'drop': function(e) {
								e.stopPropagation();
								e.preventDefault();
								placeholder.after(dragging);

								stopIndex = placeholder.index();
								if (startIndex !== stopIndex) {
									proximisCoreArray.move(scope.items.list, startIndex, stopIndex);
									scope.$digest();
								}
							}
						},
						'li[data-id], li.sortable-placeholder'
					);
				}
			}
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id proximis:pxItemSelectorAutoCompleteList
	 * @name pxItemSelectorAutoCompleteList
	 * @restrict A
	 *
	 * @description
	 * Used in document and path rule selectors to handle the auto-completion.
	 */
	app.directive('pxItemSelectorAutoCompleteList', ['$timeout', 'proximisRestApi', function($timeout, proximisRestApi) {
		return {
			restrict: 'A',
			link: function(scope, element) {
				scope.autoComplete = {
					show: false,
					refreshing: false,
					value: '',
					items: [],
					offset: 0,
					limit: 10,
					totalCount: 0,
					loadingNextPage: false,
					open: openAutoCompleteList,
					close: closeAutoCompleteList,
					refresh: refreshAutoCompleteList,
					loadPreviousPage: loadAutoCompleteListPreviousPage,
					loadNextPage: loadAutoCompleteListNextPage,
					add: autoCompleteAdd,
					notInSelection: notInSelection
				};

				function autoCompleteAdd(item) {
					if (!scope.multiple) {
						closeAutoCompleteList();
						scope.items.list.length = 0;
					}
					scope.items.list.push(item);
				}

				function openAutoCompleteList() {
					if (scope.autoComplete.show) {
						return;
					}

					refreshListPosition();
					scope.autoComplete.show = true;
				}

				function refreshListPosition() {
					var selector = scope.selectorElement;
					var input = selector.find('input[type=text]');
					var css = {
						'left': 0,
						'top': (selector.outerHeight() + selector.offset().top + 3 - selector.offset().top) + 'px',
						'width': selector.outerWidth(),
						'transition': 'top 1s ease'
					};
					element.css(css);
				}

				function closeAutoCompleteList() {
					if (!scope.autoComplete.show) {
						return;
					}
					scope.autoComplete.value = '';
					scope.autoComplete.show = false;
				}

				function refreshAutoCompleteList() {
					scope.autoComplete.error = null;
					if (!scope.autoComplete.value.trim().length) {
						scope.autoComplete.items = [];
						scope.autoComplete.totalCount = 0;
						closeAutoCompleteList();
					}
					else if (!scope.autoComplete.refreshing) {
						openAutoCompleteList();
						scope.autoComplete.refreshing = true;
						$timeout(doRefreshAutoCompleteList);
					}
				}

				function doRefreshAutoCompleteList() {
					if (!scope.canAutoComplete()) {
						scope.autoComplete.items = [];
						scope.autoComplete.totalCount = 0;
						scope.autoComplete.refreshing = false;
						return;
					}

					scope.autoComplete.offset = 0;
					var promise = proximisRestApi.get(scope.getAutoCompleteDefinition());
					promise.then(
						function(result) {
							scope.autoComplete.items = result.data.items;
							scope.autoComplete.totalCount = result.data.pagination.count;
							scope.autoComplete.refreshing = false;
						},
						function(error) {
							console.error('[pxItemSelectorAutoCompleteList]', error);
							scope.autoComplete.error = error;
							scope.autoComplete.refreshing = false;
						}
					);
				}

				function loadAutoCompleteListPreviousPage() {
					if (scope.canAutoComplete() && scope.autoComplete.offset > 0) {
						scope.autoComplete.offset -= scope.autoComplete.limit;
						doLoadPage()
					}
				}

				function loadAutoCompleteListNextPage() {
					if (scope.canAutoComplete() && (scope.autoComplete.offset + scope.autoComplete.items.length) < scope.autoComplete.totalCount) {
						scope.autoComplete.offset += scope.autoComplete.limit;
						doLoadPage();
					}
				}

				function doLoadPage() {
					scope.autoComplete.error = null;
					scope.autoComplete.loadingPage = true;
					var promise = proximisRestApi.get(scope.getAutoCompleteDefinition());
					promise.then(
						function(result) {
							scope.autoComplete.items = result.data.items;
							scope.autoComplete.loadingPage = false;
						},
						function(error) {
							console.error('[doLoadPage]', error);
							scope.autoComplete.error = error;
							scope.autoComplete.loadingPage = false;
						}
					);
				}

				function notInSelection(item) {
					return scope.tools.getItemById(scope.items.list, item._meta.id) == null;
				}

				// Watch from changes coming from the list which is bound to `scope.items.list`.
				scope.$watchCollection('items.list', function() {
					if (scope.autoComplete.show) {
						$timeout(refreshListPosition, 100);
					}
				});
			}
		}
	}]);
})(jQuery);