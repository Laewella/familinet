/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	var filtersData = {
		count: 0,
		staticFilters: {}
	};

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxFilters
	 * @name pxFilters
	 *
	 * @description
	 * A directive showing a filter bar for a model.
	 *
	 * @param {Object} pxFilters The filters object.
	 * @param {Object} staticFilterMethods The methods for static filters.
	 *     If an object is passed, a selectFilter(name, parameters) method will be automatically added to enable filter selection.
	 * @param {string} model The model to filter.
	 * @param {Object=} options An object accessible in filter template (not used by `pxFilters` itself).
	 */
	app.directive('pxFilters', ['$timeout', 'proximisCoreI18n', pxFilters]);

	function pxFilters($timeout, proximisCoreI18n) {
		function initStaticFilters(filtersId, scope) {
			scope.staticFilters = {
				sorted: [],
				byName: {},
				current: null
			};

			if (angular.isArray(filtersData.staticFilters[filtersId])) {
				angular.forEach(filtersData.staticFilters[filtersId], function(staticFilter) {
					scope.staticFilters.sorted.push(staticFilter);
					scope.staticFilters.byName[staticFilter.name] = staticFilter;
				});
				delete filtersData.staticFilters[filtersId];
			}

			var staticFilter = scope.staticFilters.byName.none;
			if (staticFilter) {
				if (!staticFilter.label) {
					staticFilter.label = proximisCoreI18n.trans('m.rbs.ua.common.filter_all|ucf');
				}
			}
			else {
				staticFilter = {
					name: 'none',
					label: proximisCoreI18n.trans('m.rbs.ua.common.filter_all|ucf'),
					active: false
				};

				scope.staticFilters.sorted.unshift(staticFilter);
				scope.staticFilters.byName.none = staticFilter;
			}
		}

		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/filters.twig',
			transclude: 'element',
			replace: true,
			scope: {
				filter: '=pxFilters',
				staticFilterMethods: '=',
				model: '@',
				options: '='
			},
			link: function(scope, element) {
				var filtersId = element.find('[data-px-filters]').attr('data-filters-id');
				if (!filtersId) {
					filtersId = filtersData.count++;
					element.find('[data-px-filters]').attr('data-filters-id', filtersId);
				}

				if (!scope.staticFilterMethods.none) {
					scope.staticFilterMethods.none = {
						isValid: function() {
							return true;
						},
						compile: function() {
							return {};
						}
					}
				}

				initStaticFilters(filtersId, scope);

				scope.selectStaticFilter = function(filterName, parameters) {
					if (scope.staticFilters.current && scope.staticFilters.current.name === filterName && angular.isObject(parameters)) {
						angular.forEach(parameters, function(value, name) {
							scope.staticFilters.current.parameters[name] = value;
						});
						return;
					}

					scope.staticFilters.current = null;
					angular.forEach(scope.staticFilters.sorted, function(staticFilter) {
						if (staticFilter.name === filterName) {
							scope.staticFilters.current = staticFilter;
							setCurrentFilter(staticFilter);
						}
						else {
							staticFilter.active = false;
						}
					});
					if (!scope.staticFilters.current) {
						setCurrentFilter(scope.staticFilters.byName.none);
						console.error('Unknown filter:', filterName);
					}
					else if (angular.isObject(parameters)) {
						angular.forEach(parameters, function (value, name) {
							scope.staticFilters.current.parameters[name] = value;
						});
					}
				};

				if (!scope.staticFilterMethods.selectFilter) {
					scope.staticFilterMethods.selectFilter = scope.selectStaticFilter
				}

				scope.isFilterValid = function(filter) {
					var filterMethods = scope.staticFilterMethods[filter.name];
					if (filterMethods && angular.isFunction(filterMethods.isValid)) {
						return filterMethods.isValid(filter);
					}
					return true;
				};

				if (scope.filter && scope.filter.name) {
					scope.selectStaticFilter(scope.filter.name);
					if (scope.filter.parameters && scope.staticFilters.current.name === scope.filter.name) {
						scope.staticFilters.current.parameters = scope.filter.parameters;
					}
				}
				else {
					scope.selectStaticFilter('none');
				}

				function currentFilterUpdated() {
					var filter = scope.staticFilters.current;

					var filterMethods = scope.staticFilterMethods[filter.name];
					if (filterMethods && angular.isFunction(filterMethods.compile)) {
						filter.compiled = filterMethods.compile(filter);
					}

					if (filter && scope.isFilterValid(filter)) {
						scope.filter = filter;
					}
				}

				function setCurrentFilter(staticFilter) {
					staticFilter.active = true;
					if (staticFilter.content) {
						$timeout(function() {
							var button = element.find('#' + staticFilter.id);
							var left = button.position().left + (button.width() / 2) - 10;
							element.find('.px-filters-arrow').css({ left: left });
						});
					}
					else {
						currentFilterUpdated();
					}
				}

				scope.$watch('staticFilters.current.parameters', currentFilterUpdated, true);
			}
		}
	}

	app.directive('pxFiltersItemParameters', ['$compile', function($compile) {
		return {
			restrict: 'A',
			template: '<div></div>',
			link: function(scope, elm) {
				scope.$watch('filter.content', function(content) {
					if (content) {
						elm.children().replaceWith($compile('<div>' + content + '</div>')(scope));
					}
					else {
						elm.html('<div></div>');
					}
				});
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id proximisOms.directive:pxStaticFilter
	 * @name pxStaticFilter
	 *
	 * @param {string} pxStaticFilter The filter technical name.
	 * @param {string} label The human readable action's label.
	 * @param {string} icon The action's icon, in form `icon-xxx` ({@link http://fortawesome.github.io/Font-Awesome/3.2.1/icons/ available icons}).
	 * @param {string=} class Optional class(es) to add to the action button.
	 * @param {string=} mode Optional mode: `single`, `multiple`, `both` (`both` by default).
	 * @param {string=} invalidFilterMessage Custom invalid filter message if defined. If not, default message is displayed.
	 *
	 * @description
	 * Action definition in a {@link proximis.directive:pxUaEntityList `pxUaEntityList`}.
	 */
	app.directive('pxStaticFilter', ['proximisCoreI18n', function(proximisCoreI18n) {
		return {
			restrict: 'A',
			require: '^pxFilters',
			compile: function(element, attrs) {
				var filtersId = element.parent().attr('data-filters-id');
				if (!filtersId) {
					filtersId = filtersData.count++;
					element.parent().attr('data-filters-id', filtersId);
				}

				if (!attrs['pxStaticFilter']) {
					throw new Error('The directive px-static-filter requires a technical name.');
				}
				if (!attrs.label && attrs['pxStaticFilter'] !== 'none') {
					throw new Error('The directive px-static-filter requires a "label" parameter.');
				}

				var staticFilter = {
					id: 'proximis_filters_' + filtersId + '_static_filter_' + attrs['pxStaticFilter'],
					name: attrs['pxStaticFilter'],
					label: attrs.label,
					content: element.html().trim(),
					parameters: { invalidFilterMessage: attrs.invalidFilterMessage ? attrs.invalidFilterMessage : proximisCoreI18n.trans(
							'm.rbs.ua.common.select_all_parameters_to_activate_filter | ucf') }
				};

				if (!filtersData.staticFilters.hasOwnProperty(filtersId)) {
					filtersData.staticFilters[filtersId] = [];
				}
				filtersData.staticFilters[filtersId].push(staticFilter);
			}
		};
	}]);
})();