(function() {
	'use strict';
	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxPhoneFormatter
	 * @name pxPhoneFormatter
	 *
	 * @description
	 * Displays a formatted phone number.
	 *
	 * @param {Object} number The phone number data to display, containing at least:
	 *  * `E164` (string): the phone number in the format compatible with an `href="tel:..."` link
	 *  * `national` (string): the phone number in the national format
	 *  * `international` (string): the phone number in the international format
	 *  * `type` (string): the phone type
	 *  * `countryCode` (string): the country code
	 *  * `flagCountry` (string): the country flag code
	 * @param {string} regionCode The region code.
	 * @param {boolean} addLink Set to true add an `href="tel:..."` link.
	 */
	app.directive('pxPhoneFormatter', [pxPhoneFormatter]);
	function pxPhoneFormatter() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/phone-formatter.twig',
			scope: {
				number: '=',
				regionCode: "="
			},
			link: function(scope, element, attributes) {
				scope.addLink = attributes.addLink === 'true';

				scope.$watch('number',
					/**
					 * @param {Object} number
					 * @config {string} type
					 */
					function(number) {
						if (number) {
							scope.data = number;
							scope.data.class = number.type === 'MOBILE' ? 'phone' : 'earphone';
						}
					}
				);
			}
		}
	}
})();
