/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxStorageUploadFile
	 * @name pxStorageUploadFile
	 *
	 * @description
	 * A directive to set upload file in scope
	 *
	 * @param {string} pxStorageUploadFile The scope variable.
	 */
	app.directive('pxStorageUploadFile', ['$parse', pxStorageUploadFile]);

	function pxStorageUploadFile($parse) {
		return {
			restrict: 'A',
			scope: false,
			link: function (scope, element, attrs) {
				var model = $parse(attrs.pxStorageUploadFile);
				var modelSetter = model.assign;
				element.bind('change', function () {
					scope.$apply(function () {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		}
	}
})();