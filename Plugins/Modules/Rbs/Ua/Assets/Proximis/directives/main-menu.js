/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxMainMenu
	 * @name pxMainMenu
	 * @restrict A
	 *
	 * @description
	 * Renders the main menu.
	 */
	app.directive('pxMainMenu', ['$location', 'ProximisGlobal', 'proximisRoute', pxMainMenu]);

	function pxMainMenu($location, ProximisGlobal, proximisRoute) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/main-menu.twig',

			link: function(scope) {
				scope.mainMenu = ProximisGlobal.menu;

				scope.onMainMenuEntryClick = function(entry) {
					var url = null;
					if (entry.url) {
						url = entry.url;
					}
					else if (entry.name && (entry.module || entry.model)) {
						url = proximisRoute.get(entry.module || entry.model, entry.name);
					}

					$location.url(url);
				};
			}
		};
	}
})();