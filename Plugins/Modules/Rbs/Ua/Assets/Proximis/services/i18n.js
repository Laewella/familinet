/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisI18n
	 * @name proximisI18n
	 *
	 * @deprecated since 1.8.0, use proximisCoreI18n instead.
	 * @description
	 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreI18n `proximisCoreI18n`} instead.**
	 *
	 * UI localization service.
	 */
	app.service('proximisI18n', ['proximisCoreI18n', proximisI18n]);

	function proximisI18n(proximisCoreI18n) {
		/**
		 * @ngdoc method
		 * @methodOf proximisI18n
		 * @name trans
		 *
		 * @deprecated since 1.8.0, use proximisCoreI18n.trans() instead.
		 * @description
		 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreI18n `proximisCoreI18n.trans()`} instead.**
		 *
		 * Translates a localization string.
		 *
		 * @param {string} string The localization string.
		 * @param {Object=} params Replacements parameters.
		 */
		this.trans = function(string, params) {
			console.warn('proximisI18n.trans() is deprecated since 1.8.0, use proximisCoreI18n.trans() instead.');
			return proximisCoreI18n.trans(string, params);
		};
	}
})();