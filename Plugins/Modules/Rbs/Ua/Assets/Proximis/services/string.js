/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisString
	 * @name proximisString
	 *
	 * @description
	 * Provides utility methods on strings.
	 */
	app.service('proximisString', proximisString);

	function proximisString() {
		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name isValidLCID
		 *
		 * @description
		 * Checks whether the given locale ID is valid or not.
		 *
		 * @param {string} lcid Locale ID.
		 *
		 * @returns {boolean} true if the given `lcid` is valid.
		 */
		this.isValidLCID = function(lcid) {
			return angular.isString(lcid) && (/^[a-z]{2}(_[a-zA-Z]{2})?$/).test(lcid);
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name isModelName
		 *
		 * @description
		 * Indicates whether the given `string` represents a valid Document Model name or not.
		 *
		 * @param {string} string Document Model name.
		 *
		 * @returns {boolean} true if the given `string` is a valid Document Model name.
		 */
		this.isModelName = function(string) {
			return angular.isString(string) && (/^\w+_\w+_\w+$/).test(string);
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name isModuleName
		 *
		 * @description
		 * Indicates whether the given `string` represents a plugin (or module) name or not.
		 *
		 * @returns {boolean} true if the given `string` is a valid plugin name.
		 */
		this.isModuleName = function(string) {
			return angular.isString(string) && (/^\w+_\w+$/).test(string);
		};

		// String manipulation methods.

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name startsWith
		 *
		 * @description
		 * Indicates whether the String `haystack` starts with the String `needle`.
		 * The comparison is case-sensitive. For a case-insensitive comparison,
		 * use {@link proximis.service:proximisString#startsWithIgnoreCase `proximisString.startsWithIgnoreCase()`}.
		 *
		 * @param {string} haystack The String to search in.
		 * @param {string} needle The String to search for.
		 *
		 * @returns {boolean} true if `haystack` starts with `needle`.
		 */
		this.startsWith = function(haystack, needle) {
			return haystack.slice(0, needle.length) === needle;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name startsWithIgnoreCase
		 *
		 * @description
		 * Indicates whether the String `haystack` starts with the String `needle`.
		 * The comparison is case-INsensitive. For a case-sensitive comparison,
		 * use {@link proximis.service:proximisString#startsWith `proximisString.startsWith()`}.
		 *
		 * @param {string} haystack The String to search in.
		 * @param {string} needle The String to search for.
		 *
		 * @returns {boolean} true if `haystack` starts with `needle`.
		 */
		this.startsWithIgnoreCase = function(haystack, needle) {
			return this.startsWith(angular.lowercase(haystack), angular.lowercase(needle));
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name endsWith
		 *
		 * @description
		 * Indicates whether the String `haystack` ends with the String `needle`.
		 * The comparison is case-sensitive. For a case-INsensitive comparison,
		 * use {@link proximis.service:proximisString#endsWithIgnoreCase `proximisString.endsWithIgnoreCase()`}.
		 *
		 * @param {string} haystack The String to search in.
		 * @param {string} needle The String to search for.
		 *
		 * @returns {boolean} true if `haystack` ends with `needle`.
		 */
		this.endsWith = function(haystack, needle) {
			return haystack.slice(-needle.length) === needle;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name endsWithIgnoreCase
		 *
		 * @description
		 * Indicates whether the String `haystack` ends with the String `needle`.
		 * The comparison is case-INsensitive. For a case-sensitive comparison,
		 * use {@link proximis.service:proximisString:Utils#endsWith `proximisString.endsWith()`}.
		 *
		 * @param {string} haystack The String to search in.
		 * @param {string} needle The String to search for.
		 *
		 * @returns {boolean} true if `haystack` ends with `needle`.
		 */
		this.endsWithIgnoreCase = function(haystack, needle) {
			return this.endsWith(angular.lowercase(haystack), angular.lowercase(needle));
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name equalsIgnoreCase
		 *
		 * @description
		 * Indicates whether the String `s1` equals the String `s2`. The comparison is case-INsensitive.
		 *
		 * @param {string} s1 First string.
		 * @param {string} s2 Second string.
		 *
		 * @returns {boolean} true if `s1` equals `s2`.
		 */
		this.equalsIgnoreCase = function(s1, s2) {
			return angular.lowercase(s1) === angular.lowercase(s2);
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name containsIgnoreCase
		 *
		 * @description
		 * Indicates whether the String `haystack` contains the String `needle`. The comparison is case-INsensitive.
		 *
		 * @param {string} haystack The String to search in.
		 * @param {string} needle The String to search for.
		 *
		 * @returns {boolean} true if `needle` is found in `haystack`.
		 */
		this.containsIgnoreCase = function(haystack, needle) {
			return angular.lowercase(haystack).indexOf(angular.lowercase(needle)) !== -1;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisString
		 * @name hashCode
		 *
		 * @description
		 * Create hash from a string.
		 *
		 * @param {string} string The String to hash.
		 *
		 * @returns {number} The hash.
		 */
		this.hashCode = function(string) {
			var hash = 0;
			if (string.length === 0) return hash;
			for (var i = 0; i < string.length; i++) {
				var char = string.charCodeAt(i);
				hash = ((hash << 5) - hash) + char;
				hash = hash & hash;
			}
			return hash;
		}
	}
})();