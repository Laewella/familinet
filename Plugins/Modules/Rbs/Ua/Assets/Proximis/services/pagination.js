(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisPagination
	 * @name proximisPagination
	 *
	 * @description
	 * A service for pagination.
	 * In most cases you prefer using the {@link proximis.directive:pxPagination `pxPagination`} directive.
	 */
	app.service('proximisPagination', ['proximisSettings', function(proximisSettings) {
		/**
		 * @ngdoc property
		 * @propertyOf proximisPagination
		 * @name proximisPagination
		 *
		 * @description
		 * Removes the message.
		 */
		this.predefinedPageSizes = [10, 20, 30, 50, 75, 100];

		/**
		 * @ngdoc method
		 * @methodOf proximisPagination
		 * @name initPagination
		 *
		 * @description
		 * Initialize missing properties in a pagination object.
		 *
		 * @param {Object} pagination The pagination to initialize.
		 */
		this.initPagination = function (pagination) {
			if (!pagination.totalItems) {
				pagination.totalItems = 0;
			}
			if (!pagination.itemsPerPage) {
				pagination.itemsPerPage = proximisSettings.get('pagingSize', 20);
			}
			if (!pagination.currentPage) {
				pagination.currentPage = 1;
			}
		};
	}]);
})();