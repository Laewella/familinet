/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc provider
	 * @id proximis.provider:proximisUserProvider
	 * @name proximisUserProvider
	 *
	 * @description
	 * Use `proximisUserProvider` to configure base URL for `proximisUser`.
	 */
	app.provider('proximisUser', [proximisUserProvider]);

	function proximisUserProvider() {
		var profileNames;

		/**
		 * @ngdoc method
		 * @methodOf proximisUserProvider
		 * @name setProfileNames
		 *
		 * @description
		 * Sets the list of profiles for the current application.
		 *
		 * @param {Array} names The profile names with sorted from the less to the most specific.
		 */
		this.setProfileNames = function(names) {
			if (angular.isArray(names)) {
				profileNames = names;
			}
			else {
				throw new Error('proximisUserProvider.setProfileNames', 'The parameter "names" should be an array of strings.')
			}
		};

		/**
		 * @ngdoc service
		 * @id proximis.service:proximisUser
		 * @name proximisUser
		 *
		 * @description
		 * Provides methods to deal with the current user.
		 */
		this.$get = ['$window', 'proximisRestApi', 'proximisRestApiDefinition', 'ProximisRestEvents', 'proximisRestOAuth',
			'proximisMessages', 'proximisCoreI18n', 'proximisErrorFormatter', '$location', '$rootScope', '$cookies', '$q',
			'ProximisGlobal',
			function($window, proximisRestApi, proximisRestApiDefinition, ProximisRestEvents, proximisRestOAuth, proximisMessages, proximisCoreI18n, proximisErrorFormatter, $location, $rootScope, $cookies, $q, ProximisGlobal) {
				var user = { settings: {} };
				$rootScope.user = user;
				var readyQ = $q.defer();

				var profiles = {};

				function startAuthentication() {
					var callback = $location.url();
					if (callback.substr(0, 7) === '/OAuth/') {
						callback = '/'
					}
					proximisRestOAuth.startAuthentication(callback);
				}

				function refreshSettings() {
					angular.forEach(profileNames, function(profileName) {
						angular.forEach(profiles[profileName], function(value, key) {
							user.settings[key] = value;
							if (profileName == 'Change_User' && key == 'LCID') {
								proximisRestApi.setLanguage(value);
								var date = new Date();
								date.setFullYear(date.getFullYear() + 1);
								$cookies.put('LCID', value, { expires: date });

								var currentLCID = ProximisGlobal.LCID;
								if (currentLCID && currentLCID !== value) {
									$window.location.reload();
								}
							}
						});
					});
					$rootScope.$broadcast('user.settings.changed', user.settings);
				}

				var proximisUser = {};

				/**
				 * @ngdoc method
				 * @methodOf proximisUser
				 * @name load
				 *
				 * @description
				 * Load the data for the current user.
				 *
				 * @returns {HttpPromise}
				 */
				proximisUser.load = function() {
					var promise = proximisRestApi.get(
						proximisRestApiDefinition.getData('common.currentUser', 'GET', { profileNames: profileNames })
					);
					promise.then(
						function(result) {
							profiles = result.data.item.profiles;
							delete result.data.item.profiles;
							angular.extend(user, result.data.item);
							refreshSettings();
							readyQ.resolve(user);
						},
						function(result) {
							if (angular.isObject(result.data) && angular.isObject(result.data.status) && result.data.status.errorCode) {
								var errorCode = result.data.status.errorCode;
								if (errorCode == 'EXCEPTION-72000' || errorCode == 'EXCEPTION-72001' || errorCode == 'EXCEPTION-72004') {
									proximisUser.logout();
									$window.location.reload();
								}
							}
						}
					);
					return promise;
				};

				/**
				 * @ngdoc method
				 * @methodOf proximisUser
				 * @name ready
				 *
				 * @description
				 * Get a promise that resolves when the user is loaded.
				 *
				 * @returns {Promise}
				 */
				proximisUser.ready = function() {
					return readyQ.promise;
				};

				/**
				 * @ngdoc method
				 * @methodOf proximisUser
				 * @name save
				 *
				 * @description
				 * Save the current user profile.
				 *
				 * @param {Object} updatedUser The updated user.
				 * @returns {HttpPromise}
				 */
				proximisUser.saveProfile = function(updatedUser) {
					angular.forEach(updatedUser.settings, function(value, key) {
						var found = false;
						angular.forEach(profileNames, function(profileName) {
							if (found) {
								return;
							}
							var profile = profiles[profileName];
							if (profile && profile.hasOwnProperty(key)) {
								profile[key] = value;
								found = true;
							}
						});
					});

					var params = { profiles: profiles };
					var promise = proximisRestApi.put(proximisRestApiDefinition.getData('common.currentUser.updateProfiles', 'PUT', params));
					promise.then(
						function(result) {
							proximisMessages.success(proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf'), result.data.status.message, true,
								10000);
							refreshSettings();
						},
						function(result) {
							proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'), result.data.status.message, true);
						}
					);
					return promise;
				};

				/**
				 * @ngdoc method
				 * @methodOf proximisUser
				 * @name get
				 *
				 * @description
				 * Gets an object that represents the current user.
				 *
				 * @returns {Object}
				 */
				proximisUser.get = function() {
					return user;
				};

				/**
				 * @ngdoc method
				 * @methodOf proximisUser
				 * @name logout
				 *
				 * @description
				 * Performs a logout.
				 */
				proximisUser.logout = function() {
					// Remove all properties of our 'user' object, keeping the same reference.
					if (user) {
						var p, props = [];
						for (p in user) {
							if (user.hasOwnProperty(p)) {
								props.push(p);
							}
						}
						for (p = 0; p < props.length; p++) {
							delete user[props[p]];
						}
					}

					proximisRestOAuth.logout();
				};

				/**
				 * @ngdoc method
				 * @methodOf proximisUser
				 * @name init
				 *
				 * @description
				 * Initialize `$rootScope.user` and load the data for the current user.
				 *
				 * @returns {boolean} `true` if the current user in authenticated, else `false`.
				 */
				proximisUser.init = function() {
					$rootScope.user = this.get();
					if (proximisRestOAuth.hasOAuthData()) {
						this.load();
						return true;
					}
					else {
						startAuthentication();
						return false;
					}
				};

				$rootScope.$on(ProximisRestEvents.Logout, function() {
					proximisUser.logout();
					$window.location.assign(ProximisGlobal.baseURL);
				});

				$rootScope.$on(ProximisRestEvents.Unauthorized, function(event, data) {
					if (!angular.isObject(data) || !angular.isObject(data.status) || !data.status.errorCode) {
						return;
					}

					var errorCode = data.status.errorCode;
					if (errorCode == 'EXCEPTION-72000' || errorCode == 'EXCEPTION-72001' || errorCode == 'EXCEPTION-72004') {
						if (proximisRestOAuth.hasOAuthData()) {
							proximisUser.logout();
							$window.location.reload(true);
						}
					}
				});
				return proximisUser;
			}
		];
	}
})();