/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisSettings
	 * @name proximisSettings
	 *
	 * @description
	 * Provides methods to deal with settings.
	 */
	app.service('proximisSettings', ['proximisUser', 'ProximisGlobal', proximisSettings]);

	function proximisSettings(proximisUser, ProximisGlobal) {
		var user = proximisUser.get();
		return {
			/**
			 * @ngdoc method
			 * @methodOf proximisSettings
			 * @name set
			 *
			 * @description
			 * Sets the value of a setting key.
			 *
			 * @param {string} key The setting key.
			 * @param {*} value The setting value.
			 * @param {bool} save Set to `true` to persist the new value in user's profile.
			 *
			 * @returns {HttpPromise|null} If `save` is true, returns a promise that resolves when the profile is saved.
			 */
			set: function(key, value, save) {
				var isModified = user.settings[key] !== value;
				user.settings[key] = value;
				if (isModified && save !== false) {
					var newSettings = {};
					newSettings.key = value;
					return proximisUser.saveProfile(newSettings);
				}
				return null;
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisSettings
			 * @name get
			 *
			 * @description
			 * Gets the value of a setting key.
			 *
			 * @param {string} key The setting key.
			 * @param {*=} defaultValue A default value.
			 *
			 * @returns {*} The setting value.
			 */
			get: function(key, defaultValue) {
				return user.settings[key] || defaultValue;
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisSettings
			 * @name ready
			 *
			 * @description
			 * Get a promise that resolves when the user is loaded.
			 *
			 * @returns {Promise}
			 */
			ready: function() {
				return proximisUser.ready();
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisSettings
			 * @name defaultLCID
			 *
			 * @description
			 * Get the default LCID.
			 *
			 * @returns {string}
			 */
			defaultLCID: function() {
				return ProximisGlobal.supportedLCIDs[0];
			}
		};
	}
})();