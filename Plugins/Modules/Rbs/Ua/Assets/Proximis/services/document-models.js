/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisDocumentModels
	 * @name proximisDocumentModels
	 *
	 * @description
	 * A service to retrieve existing document models.
	 */
	app.service('proximisDocumentModels', ['$filter', 'proximisCoreArray', 'proximisRestApi', 'proximisRestApiDefinition',
		proximisDocumentModels]);

	function proximisDocumentModels($filter, proximisCoreArray, proximisRestApi, proximisRestApiDefinition) {
		var allModels = [], loaded = false, promise = null;

		function getAll() {
			return getByFilter();
		}

		function getByFilter(filter) {
			var models = [];
			if (allModels.length) {
				applyFilter(models, filter)
			}
			else if (!loaded) {
				if (!promise) {
					promise = proximisRestApi.get(proximisRestApiDefinition.getData('Rbs_Ua_DocumentModel.list', 'GET'));
				}
				promise.then(
					function(result) {
						loaded = true;
						allModels = $filter('orderBy')(result.data.items, ['pluginLabel', 'label']);
						applyFilter(models, filter);
					},
					function(error) {
						loaded = true;
						console.error('[proximisDocumentModels]', error);
					}
				);
			}
			return models;
		}

		function applyFilter(models, filter) {
			if (!angular.isObject(filter)) {
				angular.forEach(allModels, function(testModel) {
					models.push(testModel);
				});
				return;
			}

			angular.forEach(allModels, function(testModel) {
				var valid = true;
				angular.forEach(filter, function(value, attr) {
					if (testModel.hasOwnProperty(attr)) {
						if (angular.isArray(value)) {
							if (angular.isArray(testModel[attr])) {
								if (proximisCoreArray.intersect(testModel[attr], value).length === 0) {
									valid = false;
								}
							}
							else if (!proximisCoreArray.inArray(testModel[attr], value)) {
								valid = false;
							}
						}
						else if (angular.isArray(testModel[attr])) {
							if (!proximisCoreArray.inArray(value, testModel[attr])) {
								valid = false;
							}
						}
						else if (testModel[attr] !== value) {
							valid = false;
						}
					}
					else {
						valid = false;
					}
				});
				if (valid) {
					models.push(testModel);
				}
			});
		}

		// Public API.
		return {
			/**
			 * @ngdoc method
			 * @methodOf proximisDocumentModels
			 * @name getAll
			 *
			 * @description Get all document models.
			 */
			getAll: getAll,

			/**
			 * @ngdoc method
			 * @methodOf proximisDocumentModels
			 * @name getByFilter
			 *
			 * @description Get a filtered set of models.
			 *
			 * @param {string|array|Object=} filter A model name, a model array or an object describing model filters.
			 *
			 * List of available filters:
			 *  * `publishable` (boolean)
			 *  * `activable` (boolean)
			 *  * `editable` (boolean)
			 *  * `instanceOf` (string)
			 *
			 * **Examples:**
			 *  * `'Rbs_Website_StaticPage'`
			 *  * `['Rbs_Website_StaticPage', 'Rbs_Website_Menu']`
			 *  * `{ publishable: true }`
			 *  * `{ publishable: true, instanceOf: 'Rbs_Website_Page' }`
			 */
			getByFilter: getByFilter
		};
	}
})();