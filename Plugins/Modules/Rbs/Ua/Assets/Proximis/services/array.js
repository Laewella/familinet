/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisArray
	 * @name proximisArray
	 *
	 * @deprecated since 1.8.0, use proximisCoreArray instead.
	 * @description
	 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray`} instead.**
	 */
	app.service('proximisArray', ['proximisCoreArray', proximisArray]);

	function proximisArray(proximisCoreArray) {
		return {
			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name remove
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.remove() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.remove()`} instead.**
			 *
			 * Removes elements in an Array.
			 *
			 * @param {Array} arr Array from which elements should be removed.
			 * @param {number} from Index of first element to be removed.
			 * @param {number=} to Index of last element to be removed.
			 */
			remove: function(arr, from, to) {
				console.warn('proximisArray.remove() is deprecated since 1.8.0, use proximisCoreArray.remove() instead.');
				return proximisCoreArray.remove(arr, from, to);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name removeValue
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.removeValue() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.removeValue()`} instead.**
			 *
			 * Removes a value in an Array. This method uses `angular.equals()` to search for the
			 * value to remove.
			 *
			 * @param {Array} arr Array from which the value should be removed.
			 * @param {*} value The value to be removed.
			 */
			removeValue: function(arr, value) {
				console.warn('proximisArray.removeValue() is deprecated since 1.8.0, use proximisCoreArray.removeValue() instead.');
				return proximisCoreArray.removeValue(arr, value);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name removeValues
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.removeValues() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.removeValues()`} instead.**
			 *
			 * Removes values in an Array from another Array.
			 *
			 * @param {Array} arr Array from which elements should be removed.
			 * @param {Array} elementsToRemove Array of elements to be removed from `arr`.
			 */
			removeValues: function(arr, elementsToRemove) {
				console.warn('proximisArray.removeValues() is deprecated since 1.8.0, use proximisCoreArray.removeValues() instead.');
				return proximisCoreArray.removeValues(arr, elementsToRemove);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name move
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.move() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.move()`} instead.**
			 *
			 * Moves an element into an Array.
			 *
			 * See http://jsperf.com/array-prototype-move
			 *
			 * @param {Array} arr Array into which an element should be moved.
			 * @param {number} pos1 Initial position of the element to be moved.
			 * @param {number} pos2 New position of the element to be moved.
			 * @return {boolean}
			 */
			move: function(arr, pos1, pos2) {
				console.warn('proximisArray.move() is deprecated since 1.8.0, use proximisCoreArray.move() instead.');
				return proximisCoreArray.move(arr, pos1, pos2);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name clear
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.clear() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.clear()`} instead.**
			 *
			 * Clears the given Array.
			 *
			 * @param {Array} arr Array to clear.
			 */
			clear: function(arr) {
				console.warn('proximisArray.clear() is deprecated since 1.8.0, use proximisCoreArray.clear() instead.');
				return proximisCoreArray.clear(arr);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name inArray
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.inArray() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.inArray()`} instead.**
			 *
			 * Checks if a value is an Array.
			 *
			 * @param {*} value The value.
			 * @param {Array} arr The Array.
			 * @returns {boolean}
			 */
			inArray: function(value, arr) {
				console.warn('proximisArray.inArray() is deprecated since 1.8.0, use proximisCoreArray.inArray() instead.');
				return proximisCoreArray.inArray(value, arr);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name intersect
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.intersect() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.intersect()`} instead.**
			 *
			 * Returns an Array containing the elements of `arr1` that are also present in `arr2`.
			 *
			 * @param {Array} arr1 The first Array.
			 * @param {Array} arr2 The second Array.
			 * @returns {Array}
			 */
			intersect: function(arr1, arr2) {
				console.warn('proximisArray.intersect() is deprecated since 1.8.0, use proximisCoreArray.intersect() instead.');
				return proximisCoreArray.intersect(arr1, arr2);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisArray
			 * @name append
			 *
			 * @deprecated since 1.8.0, use proximisCoreArray.append() instead.
			 * @description
			 * **Deprecated since 1.8.0, use {@link proximisCore.service:proximisCoreArray `proximisCoreArray.append()`} instead.**
			 *
			 * Appends elements in an Array at the end of another Array.
			 *
			 * @param {Array} dst The destination Array.
			 * @param {Array} src Array of elements to append to `dst`.
			 * @returns {Array}
			 */
			append: function(dst, src) {
				console.warn('proximisArray.append() is deprecated since 1.8.0, use proximisCoreArray.append() instead.');
				return proximisCoreArray.append(dst, src);
			}
		};
	}
})();