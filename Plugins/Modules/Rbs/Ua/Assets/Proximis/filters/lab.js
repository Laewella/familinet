/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxLab
	 * @name pxLab
	 *
	 * @deprecated since 1.8.0, use pxCoreLab instead.
	 * @description
	 * **Deprecated since 1.8.0, use {@link proximisCore.filter:pxCoreLab `pxCoreLab`} instead.**
	 *
	 * Appends `:` (double dots) at the end of the input string.
	 *
	 * @param {string} string The input string.
	 */
	app.filter('pxLab', ['$filter', function($filter) {
		return function(input) {
			console.warn('pxLab is deprecated since 1.8.0, use pxCoreLab instead.');
			return $filter('pxCoreLab')(input);
		};
	}]);
})();