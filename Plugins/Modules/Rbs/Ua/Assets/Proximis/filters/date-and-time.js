/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxDateTime
	 * @name pxDateTime
	 * @function
	 *
	 * Formats a Date object with date and time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('pxDateTime', ['$filter', 'proximisSettings', function($filter, proximisSettings) {
		return function(input) {
			var timeZone;
			if (input && (timeZone = proximisSettings.get('TimeZone'))) {
				return $filter('date')(input, 'medium', moment(input).tz(timeZone).format('Z'));
			}
			return $filter('date')(input, 'medium');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxDate
	 * @name pxDate
	 * @function
	 *
	 * Formats a Date object with date, without time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('pxDate', ['$filter', 'proximisSettings', function($filter, proximisSettings) {
		return function(input) {
			var timeZone;
			if (input && (timeZone = proximisSettings.get('TimeZone'))) {
				return $filter('date')(input, 'mediumDate', moment(input).tz(timeZone).format('Z'));
			}
			return $filter('date')(input, 'mediumDate');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxTime
	 * @name pxTime
	 * @function
	 *
	 * Formats a Date object with time, without the date.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('pxTime', ['$filter', 'proximisSettings', function($filter, proximisSettings) {
		return function(input) {
			var timeZone;
			if (input && (timeZone = proximisSettings.get('TimeZone'))) {
				return $filter('date')(input, 'mediumTime', moment(input).tz(timeZone).format('Z'));
			}
			return $filter('date')(input, 'mediumDate');
		};
	}]);
})();