/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxBoolean
	 * @name pxBoolean
	 *
	 * @deprecated since 1.8.0, use pxBoolean instead.
	 * @description
	 * **Deprecated since 1.8.0, use {@link proximisCore.filter:pxBoolean `pxBoolean`} instead.**
	 *
	 * Formats a Boolean value with localized <em>yes</em> or <em>no</em>.
	 *
	 * @param {boolean} value The boolean value to format.
	 */
	app.filter('pxBoolean', ['$filter', function($filter) {
		return function(input) {
			console.warn('pxBoolean is deprecated since 1.8.0, use pxCoreBoolean instead.');
			return $filter('pxCoreBoolean')(input);
		};
	}]);
})();