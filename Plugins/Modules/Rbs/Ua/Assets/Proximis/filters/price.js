/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxPrice
	 * @name pxPrice
	 * @function
	 *
	 * @deprecated since 1.8.0, use pxCorePrice instead.
	 * @description
	 * **Deprecated since 1.8.0, use {@link proximisCore.filter:pxCorePrice `pxCorePrice`} instead.**
	 *
	 * Format price by currency.
	 *
	 * @param {number} input A value to format.
	 * @param {Object|string} currency An object containing 'code' (ex: `{code: 'EUR'}` ) or the string for the code (ex: `'EUR'`).
	 * @param {number=} fractionSize The fraction size (deducted from the locale if omitted).
	 *
	 * @example
	 * ```html
	 *   <span>(= 10.55 | pxPrice:'EUR' =)</span>
	 *   <span>(= 10.55 | pxPrice:{code: 'EUR'} =)</span>
	 * ```
	 */
	app.filter('pxPrice', ['$filter', function($filter) {
		return function(input, currency, fractionSize) {
			console.warn('pxPrice is deprecated since 1.8.0, use pxCorePrice instead.');
			return $filter('pxCorePrice')(input, currency, fractionSize);
		};
	}]);
})();