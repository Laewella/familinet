/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxBarcodeSrc
	 * @name pxBarcodeSrc
	 * @deprecated since 1.8.0 replace pxBarcodeSrc filter by pxBarcode directive
	 * @description
	 * return ''
	 *
	 * @param {Object} options The barcode rendering options.
	 *    The available options are:
	 *    * `format` (string, default `code128`): one of the barcode formats provided by ({@link http://framework.zend.com/manual/current/en/modules/zend.barcode.objects.html Zend Framework 2}
	 *    * `factor` (integer, default `1`): a size multiplier.
	 *    * `barHeight` (integer, default `50`): the bars height.
	 *    * `drawText` (boolean, default `true`): set to false to not print the encoded text.
	 *
	 *    And if `drawText` is set to `true`, the additional following ones:
	 *    * `stretchText` (boolean, default `false`): set to true to stretch to the entire barcode width.
	 *    * `font` (string): a path to a font used to write the text.
	 *    * `fontSize` (string): the font size.
	 */
	app.filter('pxBarcodeSrc', ['proximisRestApi', 'proximisRestApiDefinition', pxBarcodeSrc]);

	function pxBarcodeSrc() {
		return function() {
			console.warn('@deprecated since 1.8.0 replace pxBarcodeSrc filter by pxBarcode directive');
			return '';
		};
	}
})();