/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	app.controller('ProximisAuthorizeController', ['$window', '$rootScope', '$routeParams', '$scope', '$element',
		'ProximisRestGlobal', 'ProximisRestEvents', 'proximisRestApi', 'proximisRestOAuth', 'proximisMessages',
		function($window, $rootScope, $routeParams, scope, element, ProximisRestGlobal, ProximisRestEvents, proximisRestApi, proximisRestOAuth, proximisMessages) {

			scope.submit = function() {
				var params = {
					oauth_token: $routeParams.oauth_token,
					realm: ProximisRestGlobal.OAuth.realm,
					login: scope.login,
					password: scope.password,
					device: scope.device
				};
				proximisRestApi.post('OAuth/Authorize', params).then(
					function(result) {
						var data = result.data;
						/** @var {string} data.error */
						if (data.error) {
							proximisMessages.error(data.error, null, true);
						}
						else {
							proximisRestOAuth.getAccessToken(data.oauth_token, data.oauth_verifier, data.oauth_callback);
						}
					},
					function(result) {
						console.error(result);
					}
				);
			};

			var userAgent = $window.navigator.userAgent;

			var system =
				userAgent.match(/windows/i) ? 'Windows' :
					userAgent.match(/kindle/i) ? 'Kindle' :
						userAgent.match(/android/i) ? 'Android' :
							userAgent.match(/ipad/i) ? 'iPad' :
								userAgent.match(/iphone/i) ? 'iPhone' :
									userAgent.match(/ipod/i) ? 'iPod' :
										userAgent.match(/mac/i) ? 'OS X' :
											userAgent.match(/(linux|x11)/i) ? 'Linux' :
												'unknown system';

			var webBrowser =
				userAgent.match(/firefox/i) && !userAgent.match(/seamonkey/i) ? 'Firefox' :
					userAgent.match(/seamonkey/i) ? 'Seamonkey' :
						userAgent.match(/chrome/i) && !userAgent.match(/chromium/i) ? 'Chrome' :
							userAgent.match(/chromium/i) ? 'Chromium' :
								userAgent.match(/safari/i) ? 'Safari' :
									userAgent.match(/msie/i) || userAgent.match(/rv/i) ? 'Internet Explorer' :
										'unknown web browser';

			scope.device = webBrowser + ' ' + element.attr('data-on-device') + ' ' + system;

			$rootScope.$broadcast(ProximisRestEvents.AuthorizeLoaded);
		}
	]);
})();
