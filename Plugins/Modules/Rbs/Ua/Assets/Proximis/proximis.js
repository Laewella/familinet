/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	'use strict';

	// Initialize Intl.
	__change.RBS_Ua_Intl.initialize(__change.LCID);

	/**
	 * @ngdoc module
	 * @name proximis
	 *
	 * @description
	 * The Proximis module contains all components shared between the interfaces based on Rbs_Ua.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `Proximis` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximis` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `px`
	 */
	var app = angular.module('proximis', ['LocalStorageModule', 'ngRoute', 'proximisRest', 'proximisModal', 'proximisCore', 'proximisAnalytics']);

	// Constants.

	/**
	 * @ngdoc constant
	 * @id proximis.constant:ProximisEvents
	 * @name ProximisEvents
	 *
	 * @description
	 * Contains the name of the main standard events thrown or listened by the Proximis module.
	 *
	 * @property {string} FieldIdCreated
	 *     This event is emitted by the {@link proximis.directive:pxField `pxField`} directive and caught by {@link proximis.directive:pxLabel `pxLabel`}.
	 *
	 *     Event parameters:
	 *      * `id` (string) : the field id.
	 *
	 */
	app.constant('ProximisEvents', {
		FieldIdCreated: 'Proximis:FieldIdCreated'
	});

	/**
	 * @ngdoc constant
	 * @id proximis.constant:ProximisGlobal
	 * @name ProximisGlobal
	 *
	 * @description
	 * A wrapper for the `__change` global variable.
	 *
	 * @property {string} baseURL The base URL for interface files.
	 * @property {string} restURL The base URL for REST services.
	 * @property {string} applicationShortName The current application short name.
	 * @property {string} applicationPackageName The package name.
	 * @property {string} applicationName The current application full name (concatenation of applicationShortName and applicationPackageName.
	 * @property {boolean} devMode True if the development mode is enabled.
	 * @property {boolean} angularDevMode True if the angular development mode should be enabled.
	 * @property {Array} menu The main menu definition.
	 * @property {string} LCID The current UI LCID.
	 * @property {Array} supportedLCIDs The supported LCIDs.
	 * @property {Object} routes The routes definition. Do not access it directly, use the {@link proximis.service:proximisRoute `proximisRoute`} service and the {@link proximis.filter:pxRoute `pxRoute`} filter.
	 * @property {Object} application The application instance configuration, containing:
	 *      * `version` (string|null): the instance version.
	 *      * `uuid` (string|null): project identifier for the instance.
	 *      * `env` (string|null): the environment of the instance.
	 * @config {string|null} version
	 * @config {string|null} uuid
	 * @config {string|null} env
	 */
	app.constant('ProximisGlobal', __change);

	/**
	 * @ngdoc constant
	 * @id proximis.constant:ProximisImageFormats
	 * @name ProximisImageFormats
	 *
	 * @description
	 * A list of image formats. These formats are used in {@link proximis.directive:pxStorageImage `pxStorageImage`}.
	 *
	 * @property {string} xs Format 57x32.
	 * @property {string} s Format 100x56.
	 * @property {string} m Format 177x100.
	 * @property {string} l Format 267x150.
	 * @property {string} xl Format 356x200.
	 */
	app.constant('ProximisImageFormats', {
		xs: '57x32',
		s: '100x56',
		m: '177x100',
		l: '267x150',
		xl: '356x200'
	});

	if (moment) {
		moment.locale(__change.LCID);
	}
})(window.__change);