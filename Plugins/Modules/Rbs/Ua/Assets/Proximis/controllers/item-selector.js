/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc controller
	 * @id proximis:ProximisItemSelectorController
	 * @name ProximisItemSelectorController
	 *
	 * @description
	 * Shared controller for document and path rule selectors.
	 */
	app.controller('ProximisItemSelectorController', ['$scope', function(scope) {
		scope.tools = {
			arrayCopy: function(array) {
				var copy = [];
				angular.forEach(array, function(item) {copy.push(item);});
				return copy;
			},

			getItemById: function(array, id) {
				for (var i = 0; i < array.length; i++) {
					if (array[i]._meta.id == id) {
						return array[i];
					}
				}
				return null;
			}
		};

		scope.items = {
			list: [],
			clear: function() {
				scope.items.list = [];
			},
			isEmpty: function() {
				return scope.items.list.length === 0;
			}
		};
	}]);
})();