/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(jQuery) {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisModal
	 *
	 * @description
	 * The ProximisModal module contains some components for modal handling.
	 *
	 * This module depends on {@link http://angular-ui.github.io/bootstrap/ UI-bootstrap} library.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisModal` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisModal` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxModal`
	 */
	var app = angular.module('proximisModal', ['ui.bootstrap']);

	/**
	 * @ngdoc service
	 * @id proximisModal:proximisModalStack
	 * @name proximisModalStack
	 *
	 * @description
	 * This service is used to manage a modal stack where opening a new modal hides the others.
	 * It provides methods to close the last modal and show back the previous or close the full stack.
	 * This is useful for cascading modals, where the child extends the scope of the parent.
	 */
	app.service('proximisModalStack', ['$uibModal', '$timeout', function($uibModal, $timeout) {
		var className = 'modal-hidden-stack';
		var opened = [];

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name open
		 *
		 * @description
		 * Open a new modal and hide any other modal in the stack.
		 *
		 * @param {Object} options A list of options for the modal.
		 *    See {@link http://angular-ui.github.io/bootstrap/#/modal UI-bootstrap modal} for the list of available options.
		 */
		function open(options) {
			hideStack();
			var classNames = className + ' modal-stack-idx-' + (opened.length + 1);
			if (options.windowClass) {
				options.windowClass += ' ' + classNames;
			}
			else {
				options.windowClass = classNames;
			}

			var modal = $uibModal.open(options);

			if (!options.hasOwnProperty('controller')) {
				var closeAllFunction = function() {
					closeAll();
				};
				modal.result.then(closeAllFunction, closeAllFunction);
			}
			opened.push(modal);
			return modal;
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name close
		 *
		 * @description
		 * Close the last modal (and `parentModalsToClose` parent ones) and show the previous one.
		 *
		 * @param {number=} [parentModalsToClose=0] The number of parent modals that should be closed.
		 */
		function close(parentModalsToClose) {
			if (opened.length) {
				var modal = opened[opened.length - 1];
				var deferredDismiss = function() {
					$timeout(function() {
						modal.dismiss('cancel');
					});
				};
				modal.opened.then(deferredDismiss, deferredDismiss);
			}
			if (parentModalsToClose > 0) {
				opened.pop();
				close(parentModalsToClose - 1);
			}
			else {
				showPrevious();
			}
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name closeAll
		 *
		 * @description
		 * Close each modals in the stack.
		 */
		function closeAll() {
			for (var i = 0; i < opened.length; i++) {
				opened[i].dismiss();
			}
			opened = [];
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name showPrevious
		 *
		 * @description
		 * Show the previous opened modal. Call it only when the current one is closed.
		 */
		function showPrevious() {
			opened.pop();
			if (opened.length) {
				jQuery('.modal-stack-idx-' + opened.length).show();
			}
		}

		function hideStack() {
			jQuery('.' + className).hide();
		}

		this.open = open;
		this.close = close;
		this.closeAll = closeAll;
		this.showPrevious = showPrevious;
	}]);
})(window.jQuery);