/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	'use strict';

	var currentLCID = null;
	var locale = null;

	__change.RBS_Ua_Intl = {
		initialized: false,
		initialize: function(LCID) {
			if (this.initialized) {
				if (currentLCID !== LCID) {
					console.error('[ProximisIntl] Intl is already initialized with ' + currentLCID + ', different from ' + LCID);
				}
				return;
			}

			currentLCID = LCID;
			locale = LCID.replace('_', '-');

			if (!window.Intl) {
				var assetBasePath = (__change.navigationContext && __change.navigationContext.assetBasePath) ? __change.navigationContext.assetBasePath : '/Assets/';
				document.write('<scr' + 'ipt type="text/javascript" src="'+ assetBasePath+'Rbs/Ua/lib/Intl.js-1.2.4/dist/Intl.min.js"></scr' + 'ipt>');
				document.write(
					'<scr' + 'ipt type="text/javascript" src="' + assetBasePath + 'Rbs/Ua/lib/Intl.js-1.2.4/locale-data/jsonp/' + locale + '.js"></scr' + 'ipt>'
				);
			}

			this.initialized = true;
		}
	};

	/**
	 * @ngdoc module
	 * @name proximisIntl
	 *
	 * @description
	 * The ProximisIntl module contains some components for internationalization.
	 *
	 * It is based on the Intl API __with a fallback for browsers that not support it__.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisIntl` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisIntl` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxIntl`
	 */
	var app = angular.module('proximisIntl', []);

	/**
	 * @ngdoc service
	 * @id proximisIntl:proximisIntlFormatter
	 * @name proximisIntlFormatter
	 *
	 * @description
	 * This service is a wrapper for Intl API.
	 */
	app.service('proximisIntlFormatter', function() {
		if (!__change.RBS_Ua_Intl.initialized) {
			console.error('[ProximisIntl] Intl is not initialized!');
		}

		return {
			/**
			 * @ngdoc method
			 * @methodOf proximisIntlFormatter
			 * @name getDateTimeFormatter
			 *
			 * @description
			 * A wrapper for `new Intl.DateTimeFormat`.
			 * See {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat Intl.DateTimeFormat} for details.
			 *
			 * @param {Object=} options An object with some or all of the following properties: timeZone, hour12, etc.
			 *     See {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat Intl.DateTimeFormat} for full list.
			 */
			getDateTimeFormatter: function(options) {
				return new window.Intl.DateTimeFormat(locale, options);
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisIntlFormatter
			 * @name getNumberFormatter
			 *
			 * @description
			 * A wrapper for `new Intl.NumberFormat()`.
			 * See {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat Intl.NumberFormat} for details.
			 *
			 * @param {Object=} options An object with some or all of the following properties: style, currency, etc.
			 *     See {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat Intl.NumberFormat} for full list.
			 */
			getNumberFormatter: function(options) {
				return new window.Intl.NumberFormat(locale, options);
			}
		};
	});
})(window.__change);