/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	var app = angular.module('proximisAnalytics');
	/**
	 * @ngdoc service
	 * @id proximisAnalytics.service:proximisAnalyticsService
	 * @name proximisAnalyticsService
	 *
	 * @description
	 * Analytics sender service.
	 */
	app.service('proximisAnalyticsService', ['ProximisAnalyticsGlobal', proximisAnalyticsService]);

	function proximisAnalyticsService(ProximisAnalyticsGlobal) {
		return {
			init: function() {
				if (!ProximisAnalyticsGlobal.analytics || !ProximisAnalyticsGlobal.analytics.trackerId) {
					return;
				}
				// Function from Google Analytics
				(function(i, s, o, g, r, a, m) {
					i['GoogleAnalyticsObject'] = r;
					i[r] = i[r] || function() { (i[r].q = i[r].q || []).push(arguments) };
					i[r].l = 1 * new Date();
					a = s.createElement(o);
					m = s.getElementsByTagName(o)[0];
					a.async = 1;
					a.src = g;
					m.parentNode.insertBefore(a, m)
				})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
				// END Function from Google Analytics

				window.ga('create', ProximisAnalyticsGlobal.analytics.trackerId, {
					cookieExpires: 31536000 // 365 days (<13 months) in seconds
				});
			},
			trackView: function(path) {
				if (!ProximisAnalyticsGlobal.analytics || !ProximisAnalyticsGlobal.analytics.trackerId) {
					return;
				}

				var fieldsObject = {};
				if(path) {
					if (path.charAt(0) === '/') {
						fieldsObject.location = window.location.origin + path;
					}
					else {
						fieldsObject.location = path;
					}
				}

				if (ProximisAnalyticsGlobal.role) {
					fieldsObject.dimension1 = ProximisAnalyticsGlobal.role;
				}
				if (ProximisAnalyticsGlobal.application) {
					fieldsObject.dimension2 = ProximisAnalyticsGlobal.application.version;
					fieldsObject.dimension3 = ProximisAnalyticsGlobal.application.uuid;
					fieldsObject.dimension4 = ProximisAnalyticsGlobal.application.env;
				}
				window.ga('send', 'pageview', fieldsObject);
			}
		};
	}
})();