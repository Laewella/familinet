/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisAnalytics
	 *
	 * @description
	 * The ProximisAnalytics module contains every tool to be able to collect analytics data.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisAnalytics` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisAnalytics` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxAnalytics`
	 */
	var app = angular.module('proximisAnalytics', []);

	/**
	 * @ngdoc constant
	 * @id proximisAnalytics.constant:ProximisAnalyticsGlobal
	 * @name ProximisAnalyticsGlobal
	 *
	 * @description
	 * A wrapper for the `__change` global variable.
	 *
	 * @property {Object|null} analytics
	 *      The analytics configuration.
	 *
	 *      Analytics parameters:
	 *      * `trackerId` (string|null) Used to initialize the analytics tracking context.
	 * @config {string|null} trackerId
	 *
	 * @property {Object} application
	 *      The application instance configuration.
	 *
	 *      Instance parameters:
	 *      * `version` (string|null): the instance version.
	 *      * `uuid` (string|null): project identifier for the instance.
	 *      * `env` (string|null): the environment of the instance.
	 * @config {string|null} version
	 * @config {string|null} uuid
	 * @config {string|null} env
	 *
	 * @property {string|null} role The role of the user.
	 */
	app.constant('ProximisAnalyticsGlobal', __change);

	// By default, tracks the view
	// - at first loading of the page
	// - when URL has been changed (query string is considered as irrelevant in comparison operation)
	app.run(['$location', '$rootScope', 'proximisAnalyticsService',
		function(location, rootScope, proximisAnalyticsService) {
			proximisAnalyticsService.init();
			proximisAnalyticsService.trackView();
			rootScope.$on('$locationChangeSuccess',
				/**
				 * @param {Object} event
				 * @param {string} newURL
				 * @param {string} oldURL
				 */
				function(event, newURL, oldURL) {
					newURL = newURL.split('?')[0];
					oldURL = oldURL.split('?')[0];
					if (newURL !== oldURL) {
						proximisAnalyticsService.trackView(newURL);
					}
				}
			);
		}
	]);
})(window.__change);