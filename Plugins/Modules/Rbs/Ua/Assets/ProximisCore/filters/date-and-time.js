/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisCore');

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCoreDateTime
	 * @name pxCoreDateTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with date and time.
	 *
	 * @param {Date} date The date to format.
	 * @param {string=} timezone ex: '+0430'
	 */
	app.filter('pxCoreDateTime', ['$filter', function($filter) {
		return function(input, timezone) {
			return $filter('date')(input, 'medium', timezone);
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCoreDate
	 * @name pxCoreDate
	 * @function
	 *
	 * @description
	 * Formats a Date object with date, without time.
	 *
	 * @param {Date} date The date to format.
	 * @param {string=} timezone ex: '+0430'
	 */
	app.filter('pxCoreDate', ['$filter', function($filter) {
		return function(input, timezone) {
			return $filter('date')(input, 'mediumDate', timezone);
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCoreTime
	 * @name pxCoreTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with time, without the date.
	 *
	 * @param {Date} date The date to format.
	 * @param {string=} timezone ex: '+0430'
	 */
	app.filter('pxCoreTime', ['$filter', function($filter) {
		return function(input, timezone) {
			return $filter('date')(input, 'mediumTime', timezone);
		};
	}]);
})();