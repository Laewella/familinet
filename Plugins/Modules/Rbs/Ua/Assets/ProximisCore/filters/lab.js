/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisCore');

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCoreLab
	 * @name pxCoreLab
	 *
	 * @description
	 * Appends `:` (double dots) at the end of the input string.
	 *
	 * @param {string} string The input string.
	 */
	app.filter('pxCoreLab', ['ProximisCoreGlobal', function(ProximisCoreGlobal) {
		return function labFilterFn(input) {
			return input + (ProximisCoreGlobal.LCID.substr(0, 2).toLowerCase() === 'fr' ? ' ' : '') + ':';
		};
	}]);
})();