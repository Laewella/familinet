/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisCore');

	/**
	 * @ngdoc service
	 * @id proximisCore.service:proximisCoreI18n
	 * @name proximisCoreI18n
	 *
	 * @description
	 * UI localization service.
	 */
	app.service('proximisCoreI18n', ['$filter', 'ProximisCoreGlobal', proximisCoreI18n]);

	function proximisCoreI18n($filter, ProximisCoreGlobal) {
		var formatters = {
			ucf: function(input) {
				if (input && input.length > 0) {
					return angular.uppercase(input.substr(0, 1)) + input.substr(1);
				}
				return input;
			},
			etc: function(input) {
				return input + '…';
			},
			lab: function(input) {
				return $filter('pxCoreLab')(input);
			}
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisCoreI18n
		 * @name trans
		 *
		 * @description
		 * Translates a localization string.
		 *
		 * @param {string} string The localization string.
		 * @param {Object=} params Replacements parameters.
		 */
		this.trans = function(string, params) {
			var p, path, key, filters = null;

			p = string.indexOf('|');
			if (p !== -1) {
				filters = string.substr(p + 1).trim().split('|');
				string = string.substring(0, p).trim();
			}
			string = string.toLowerCase();

			p = string.lastIndexOf('.');
			path = string.substring(0, p);
			key = string.substr(p + 1);

			// Search for the key in the global object.
			if (ProximisCoreGlobal.i18n[path] && ProximisCoreGlobal.i18n[path][key]) {
				string = ProximisCoreGlobal.i18n[path][key];
				// Replace parameters (if any).
				angular.forEach(params, function(value, key) {
					string = string.replace(new RegExp('\\$' + key + '\\$', 'gi'), value);
				});
			}

			if (filters) {
				angular.forEach(filters, function(filterName) {
					string = formatters[filterName.trim()](string);
				});
			}

			return string;
		};
	}
})();