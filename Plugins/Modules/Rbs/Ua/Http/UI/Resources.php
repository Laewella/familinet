<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\Resources
 */
class Resources
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var \Rbs\Ua\ApplicationInfo
	 */
	protected $applicationInfo;

	/**
	 * @var string
	 */
	protected $cachePath;

	/**
	 * @var \Twig_ExtensionInterface[]
	 */
	protected $extensions;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var boolean
	 */
	protected $devMode = false;

	/**
	 * @var string
	 */
	protected $webBaseDirectory;

	/**
	 * @var string
	 */
	protected $assetsVersion;

	/**
	 * @return \Change\Application
	 */
	protected function getApplication()
	{
		return $this->application;
	}

	/**
	 * @param \Change\Application $application
	 * @return $this
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		$configuration = $application->getConfiguration();
		$this->devMode = $configuration->inDevelopmentMode();
		$config = $configuration->getEntry('Change/Install');
		$this->webBaseDirectory = $config['webBaseDirectory'] ?? '';
		$this->assetsVersion = $config['assetsVersion'] ?? '';
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager($pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @return \Assetic\AssetManager
	 */
	public function getNewAssetManager()
	{
		return new \Assetic\AssetManager();
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function registerCommonAssets($assetsManager)
	{
		$devMode = $this->application->inDevelopmentMode();
		$pluginPath = $this->pluginManager->getPlugin('module', 'Rbs', 'Ua')->getAssetsPath();

		// JS.
		$globs = [];

		$assetsPath = $pluginPath . '/ProximisIntl';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$assetsPath = $pluginPath . '/ProximisRest';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$assetsPath = $pluginPath . '/ProximisCore';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$assetsPath = $pluginPath . '/ProximisModal';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$assetsPath = $pluginPath . '/ProximisAnalytics';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$assetsPath = $pluginPath . '/Proximis';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$assetsPath = $pluginPath . '/ProximisPolyfill';
		$globs[] = $assetsPath . '/*.js';
		$globs[] = $assetsPath . '/*/*.js';

		$jsAssets = new \Assetic\Asset\GlobAsset($globs);
		if (!$devMode)
		{
			$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
		}
		$jsAssets->setTargetPath('Rbs/Ua/js/proximis.js');
		$assetsManager->set('BODY_JS_proximis', $jsAssets);

		// CSS.
		$assetsPath = $pluginPath . '/Proximis';
		$globs = [$assetsPath . '/*.css', $assetsPath . '/*/*.css'];
		$cssAsset = new \Assetic\Asset\GlobAsset($globs);
		$cssAsset->setTargetPath('Rbs/Ua/css/proximis.css');
		$assetsManager->set('HEAD_CSS_proximis', $cssAsset);

		// Images.
		$srcPath = $pluginPath . '/img';
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath, \FilesystemIterator::SKIP_DOTS),
			\RecursiveIteratorIterator::SELF_FIRST);
		$targetBasePath = 'Rbs/Ua/img';
		foreach ($iterator as $fileInfo)
		{
			/* @var $fileInfo \SplFileInfo */
			if ($fileInfo->isFile())
			{
				$pathname = $fileInfo->getPathname();
				$targetPath = str_replace($srcPath, $targetBasePath, $pathname);
				$asset = new \Assetic\Asset\FileAsset($pathname);
				$asset->setTargetPath($targetPath);
				$assetsManager->set(preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
			}
		}
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function registerAssets($assetsManager)
	{
		$devMode = $this->application->inDevelopmentMode();
		$moduleName = $this->getModule();
		$vendorName = $this->getVendor();
		$plugin = $this->pluginManager->getPlugin('module', $vendorName, $moduleName);

		$jsAssets = new \Assetic\Asset\AssetCollection();
		$this->registerModuleAssets($jsAssets, $plugin, 'js');
		$this->registerModuleSpecificJs($jsAssets);

		if (!$devMode)
		{
			$jsAssets->ensureFilter(new \Assetic\Filter\JSMinFilter());
		}
		$jsAssets->setTargetPath('Rbs/Ua/js/' . $vendorName . '_' . $moduleName . '.js');
		$assetsManager->set('BODY_JS_' . $vendorName . '_' . $moduleName, $jsAssets);

		$cssAssets = new \Assetic\Asset\AssetCollection();
		$this->registerModuleAssets($cssAssets, $plugin, 'css');
		$this->registerModuleSpecificCss($cssAssets);

		$cssAssets->setTargetPath('Rbs/Ua/css/' . $vendorName . '_' . $moduleName . '.css');
		$assetsManager->set('HEAD_CSS_' . $vendorName . '_' . $moduleName, $cssAssets);

		$srcPath = $plugin->getAssetsPath() . '/img';
		if (is_dir($srcPath))
		{
			$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath, \FilesystemIterator::SKIP_DOTS),
				\RecursiveIteratorIterator::SELF_FIRST);
			$targetBasePath = 'Rbs/' . $moduleName . '/img';
			foreach ($iterator as $fileInfo)
			{
				/* @var $fileInfo \SplFileInfo */
				if ($fileInfo->isFile())
				{
					$targetPath = str_replace($srcPath, $targetBasePath, $fileInfo->getPathname());
					$asset = new \Assetic\Asset\FileAsset($fileInfo->getPathname());
					$asset->setTargetPath($targetPath);
					$assetsManager->set(preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
				}
			}
		}
	}

	/**
	 * @param \Assetic\Asset\AssetCollection $assets
	 * @param \Change\Plugins\Plugin $plugin
	 * @param string $fileExtension
	 */
	protected function registerModuleAssets($assets, $plugin, $fileExtension)
	{
		$pluginPath = $plugin->getAssetsPath();
		$assets->add(new \Assetic\Asset\FileAsset($pluginPath . '/Ua/proximis' . $plugin->getShortName() . '.' . $fileExtension));
		$assets->add(new \Assetic\Asset\GlobAsset($pluginPath . '/Ua/*/*.' . $fileExtension));
		$assets->add(new \Assetic\Asset\GlobAsset($pluginPath . '/Ua/views/*/*.' . $fileExtension));
		$assets->add(new \Assetic\Asset\GlobAsset($pluginPath . '/Ua/views/*/*/*.' . $fileExtension));
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function registerCommonLibAssets($assetsManager)
	{
		$plugin = $this->pluginManager->getPlugin('module', 'Rbs', 'Ua');
		$srcPath = $plugin->getAssetsPath() . '/lib';
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath, \FilesystemIterator::SKIP_DOTS),
			\RecursiveIteratorIterator::SELF_FIRST);
		$targetBasePath = 'Rbs/Ua/lib';
		foreach ($iterator as $fileInfo)
		{
			/* @var $fileInfo \SplFileInfo */
			if ($fileInfo->isFile())
			{
				$targetPath = str_replace($srcPath, $targetBasePath, $fileInfo->getPathname());
				$asset = new \Assetic\Asset\FileAsset($fileInfo->getPathname());
				$asset->setTargetPath($targetPath);
				$assetsManager->set(preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
			}
		}
	}

	/**
	 * @api
	 * @return array
	 */
	public function getRoutes()
	{
		$routes = [];
		$module = $this->getPluginManager()->getModule('Rbs', 'Ua');
		$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'routes.common.json');
		$routes = $this->appendRoutes($filePath, $routes);

		if ($this->getVendor() !== 'Rbs' || $this->getModule() !== 'Ua')
		{
			$module = $this->getPluginManager()->getModule($this->getVendor(), $this->getModule());
			$fileName = 'routes.' . $this->getApplicationInfo()->getName() . '.json';
			$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'Ua', $fileName);
			$routes = $this->appendRoutes($filePath, $routes);
		}

		return $routes;
	}

	/**
	 * @api
	 * @return array
	 */
	public function getRestApi()
	{
		$restApi = [];
		$module = $this->getPluginManager()->getModule('Rbs', 'Ua');
		$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'rest.global.json');
		$restApi = $this->appendRestApi($filePath, $restApi);

		$fileNames = $this->getRestApiFileNames();
		if ($fileNames)
		{
			$module = $this->getPluginManager()->getModule($this->getVendor(), $this->getModule());
			foreach ($fileNames as $fileName)
			{
				$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'Ua', $fileName);
				$restApi = $this->appendRestApi($filePath, $restApi);
			}
		}
		return $this->normalizeApi($restApi);
	}

	/**
	 * @param array $restApi
	 * @return array
	 */
	protected function normalizeApi(array $restApi)
	{
		$api = [];
		if (!isset($restApi['api']))
		{
			return $api;
		}
		foreach ($restApi['api'] as $definition)
		{
			if (!isset($definition['name'], $definition['path']))
			{
				continue;
			}

			$entry = ['path' => $definition['path']];
			foreach ($definition['operations'] as $operation)
			{
				$method = $operation['method'] ??  'GET';
				$parameters = $operation['parameters'] ?? [];
				foreach ($parameters as &$parameter)
				{
					unset($parameter['description']);
				}
				unset($parameter);

				$fields = [];
				foreach ($operation['responses'] ?? [] as $response)
				{
					foreach ($response['properties'] ?? [] as $propertyName => $property)
					{
						if ($property['fields'] ?? false)
						{
							$fields[] = $propertyName;
						}
					}
				}

				if ($fields)
				{
					$parameters[] = ['name' => 'fields', 'type' => 'String[]', 'src' => 'query', 'enum' => $fields];
				}
				$entry[$method] = ['parameters' => $parameters];
			}
			$api[$definition['name']] = $entry;
		}

		return ['api' => $api];
	}

	/**
	 * @return string
	 */
	public function getAssetsBasePath()
	{
		return '/Assets/' . ($this->assetsVersion ? $this->assetsVersion . '/' : '');
	}

	/**
	 * @deprecated since 1.8.0 with no replacement
	 * @return null|string
	 */
	public function getDefaultWebBaseDirectory()
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		return $this->getAssetBaseDirectory();
	}

	/**
	 * @return string
	 */
	protected function getAssetBaseDirectory()
	{
		return $this->application->getWorkspace()->composeAbsolutePath($this->webBaseDirectory, 'Assets', $this->assetsVersion);
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function write(\Assetic\AssetManager $assetsManager)
	{
		$writer = new \Assetic\AssetWriter($this->getAssetBaseDirectory());
		$writer->writeManagerAssets($assetsManager);
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager($modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	protected function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @api
	 * @param \Twig_ExtensionInterface $extension
	 * @return $this
	 */
	public function addExtension(\Twig_ExtensionInterface $extension)
	{
		$this->getExtensions();
		$this->extensions[] = $extension;
		return $this;
	}

	/**
	 * @return \Twig_ExtensionInterface[]
	 */
	public function getExtensions()
	{
		if ($this->extensions === null)
		{
			$extension = new \Rbs\Ua\Presentation\Twig\Extension($this->getApplication(), $this->getI18nManager(), $this->getModelManager());
			$this->extensions = [$extension];

			$extension = new \Change\Presentation\Templates\Twig\Extension($this->getI18nManager(), $this->getApplication()->getConfiguration());
			$this->extensions[] = $extension;
		}
		return $this->extensions;
	}

	/**
	 * @return string
	 */
	protected function getCachePath()
	{
		if ($this->cachePath === null)
		{
			$this->cachePath = $this->getApplication()->getWorkspace()
				->cachePath('Ua', 'Templates', 'Compiled');
			\Change\Stdlib\FileUtils::mkdir($this->cachePath);
		}
		return $this->cachePath;
	}

	/**
	 * @param string $moduleName
	 * @param string $pathName
	 * @param array $attributes
	 * @return string
	 */
	public function renderModuleTemplateFile($moduleName, $pathName, array $attributes)
	{
		$overridePath = $this->getApplication()->getWorkspace()->appPath('Overrides', 'Ua');
		$loader = new \Rbs\Ua\Presentation\Twig\Loader($overridePath, $this->getPluginManager());
		$twig = new \Twig_Environment($loader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->getExtensions() as $extension)
		{
			$twig->addExtension($extension);
		}
		return $twig->render('@' . $moduleName . '/' . $pathName, $attributes);
	}

	/**
	 * @api
	 * @return array
	 */
	public function getI18nKeys()
	{
		$i18nManager = $this->getI18nManager();
		$LCID = $i18nManager->getLCID();
		$pluginManager = $this->getPluginManager();
		$packages = [];

		// Core.
		foreach (['filesize', 'types'] as $subPackage)
		{
			$packageName = implode('.', ['c', $subPackage]);
			$keys = $i18nManager->getTranslationsForPackage($packageName, $LCID);
			if (is_array($keys))
			{
				$package = [];
				foreach ($keys as $key => $value)
				{
					$package[$key] = $value;
				}
				$packages[$packageName] = $package;
			}
		}

		$module = $pluginManager->getModule('Rbs', 'Ua');
		$packages = $this->appendI18nPackage($module, 'common', $packages);

		if ($this->getVendor() !== 'Rbs' || $this->getModule() !== 'Ua')
		{
			$module = $pluginManager->getModule($this->getVendor(), $this->getModule());
			$packages = $this->appendI18nPackage($module, 'ua', $packages);
		}

		return $packages;
	}

	/**
	 * @return \Rbs\Ua\ApplicationInfo
	 */
	public function getApplicationInfo()
	{
		return $this->applicationInfo;
	}

	/**
	 * @param \Rbs\Ua\ApplicationInfo $applicationInfo
	 * @return Resources
	 */
	public function setApplicationInfo($applicationInfo)
	{
		$this->applicationInfo = $applicationInfo;
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getVendor()
	{
		return 'Rbs';
	}

	/**
	 * @return string
	 */
	protected function getModule()
	{
		return 'Ua';
	}

	protected function getRestApiFileNames()
	{
		return [];
	}

	/**
	 * @return array
	 */
	public function getMainMenu()
	{
		$pm = $this->getPluginManager();
		$sections = [];
		$entries = [];
		$plugin = $pm->getModule($this->getVendor(), $this->getModule());
		$mainMenuPath = $plugin->getAssetsPath() . '/Ua/main-menu.' . $this->applicationInfo->getName() . '.json';
		if (is_readable($mainMenuPath))
		{
			$menuJson = json_decode(file_get_contents($mainMenuPath), true);
			if (is_array($menuJson))
			{
				$sections = $this->parseSections($menuJson);
				$entries = $this->parseEntries($menuJson);
			}
		}

		if (!$sections)
		{
			return [];
		}

		uasort($sections, function ($a, $b)
		{
			$ida = $a['index'] ?? PHP_INT_MAX;
			$idb = $b['index'] ?? PHP_INT_MAX;
			return $ida >= $idb;
		});

		reset($sections);
		$defaultSection = key($sections);

		foreach ($entries as $entry)
		{
			$section = isset($entry['section']) && isset($sections[$entry['section']]) ? $entry['section'] : $defaultSection;
			$sections[$section]['entries'][] = $entry;
		}

		$result = [];
		foreach ($sections as $section)
		{
			if (isset($section['entries']))
			{
				$result[] = $section;
			}
		}
		return $result;
	}

	/**
	 * @param array $menuJson
	 * @return array
	 */
	protected function parseSections($menuJson)
	{
		$result = [];
		if (isset($menuJson['sections']) && is_array($menuJson['sections']))
		{
			$i18nManager = $this->getI18nManager();
			foreach ($menuJson['sections'] as $item)
			{
				if (isset($item['code']))
				{
					if (isset($item['label']) && is_string($item['label']))
					{
						$item['label'] = $i18nManager->trans($item['label'], ['ucf']);
					}
					$result[$item['code']] = $item;
				}
			}
		}
		return $result;
	}

	/**
	 * @param array $menuJson
	 * @return array
	 */
	protected function parseEntries($menuJson)
	{
		$result = [];
		if (isset($menuJson['entries']) && is_array($menuJson['entries']))
		{
			$i18nManager = $this->getI18nManager();
			foreach ($menuJson['entries'] as $item)
			{
				$label = $item['label'] ?? null;
				if ($label && is_string($label))
				{
					$label = $i18nManager->trans($label, ['ucf']);
					$item['label'] = $label;
				}

				$type = $item['type'] ?? null;
				if ($type === 'separator' || ($type === 'text' && $label))
				{
					$result[] = $item;
				}
				elseif (isset($item['url']) || (isset($item['name']) && (isset($item['module']) || isset($item['model']))))
				{
					$item['type'] = 'link';
					$result[] = $item;
				}
			}
		}
		return $result;
	}

	/**
	 * @param \Change\Plugins\Plugin $module
	 * @param string $subPackage
	 * @param array $packages
	 * @return array
	 */
	protected function appendI18nPackage($module, $subPackage, $packages)
	{
		$i18nManager = $this->getI18nManager();
		$LCID = $i18nManager->getLCID();
		$packageName = implode('.',
			['m', strtolower($module->getVendor()), strtolower($module->getShortName()), $subPackage]);
		$keys = $i18nManager->getTranslationsForPackage($packageName, $LCID);
		if (is_array($keys))
		{
			$package = [];
			foreach ($keys as $key => $value)
			{
				$package[$key] = $value;
			}
			$packages[$packageName] = $package;
			return $packages;
		}
		return $packages;
	}

	/**
	 * @param string $filePath
	 * @param array $routes
	 * @return array
	 */
	protected function appendRoutes($filePath, $routes)
	{
		if (is_readable($filePath))
		{
			$moduleRoutes = json_decode(file_get_contents($filePath), true);
			if (is_array($moduleRoutes))
			{
				$routes = array_merge($routes, $moduleRoutes);
			}
			else
			{
				$this->getApplication()->getLogging()->error('invalid json file: ' . $filePath);
			}
		}
		return $routes;
	}

	/**
	 * @param string $filePath
	 * @param array $restApi
	 * @return array
	 */
	protected function appendRestApi($filePath, $restApi)
	{
		if (is_readable($filePath))
		{
			$moduleRestApi = json_decode(file_get_contents($filePath), true);
			if (is_array($moduleRestApi))
			{
				foreach ($moduleRestApi as $key => $entry)
				{
					if ($key === 'includes')
					{
						if ($entry && is_array($entry))
						{
							$basePath = dirname($filePath) . DIRECTORY_SEPARATOR;
							foreach ($entry as $include)
							{
								$restApi = $this->appendRestApi($basePath . $include, $restApi);
							}
						}
						continue;
					}
					if (is_string($entry))
					{
						$restApi[$key] = $entry;
					}
					elseif (is_array($entry))
					{
						$restApi[$key] = isset($restApi[$key]) ? array_merge($restApi[$key], $entry) : $entry;
					}
				}
			}
			else
			{
				$this->getApplication()->getLogging()->error('invalid json file: ' . $filePath);
			}
		}
		return $restApi;
	}

	/**
	 * @param \Assetic\Asset\AssetCollection $jsAssets
	 */
	protected function registerModuleSpecificJs(\Assetic\Asset\AssetCollection $jsAssets)
	{
		// nothing by default
	}

	/**
	 * @param \Assetic\Asset\AssetCollection $cssAssets
	 */
	protected function registerModuleSpecificCss(\Assetic\Asset\AssetCollection $cssAssets)
	{
		// nothing by default
	}
}