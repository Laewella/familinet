<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\Controller
 */
class Controller extends \Change\Http\Controller
{
	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Http', 'Http.UA', 'Http.UA.UI'];
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Http\Event::EVENT_REQUEST, function ($event)
		{
			$this->onRequest($event);
		}, 5);

		$eventManager->attach(\Change\Http\Event::EVENT_RESPONSE, function ($event)
		{
			$this->onResponse($event);
		}, 5);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function onRequest(\Change\Http\Event $event)
	{
		$request = $event->getRequest();
		$applicationServices = $event->getApplicationServices();
		$applicationServices->getPermissionsManager()->allow(true);
		$request->populateLCIDByHeader($applicationServices->getI18nManager());
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function onResponse($event)
	{
		if ($event->getResponse() !== null)
		{
			return;
		}

		$result = $event->getResult();
		if ($result instanceof \Change\Http\Result)
		{
			$response = $this->createResponse();
			$response->getHeaders()->addHeaders($result->getHeaders());
			$response->setStatusCode($result->getHttpStatusCode() ?: 500);
			$callable = [$result, 'toHtml'];
			if (is_callable($callable))
			{
				$response->setContent(call_user_func($callable));
			}
			else
			{
				$response->setContent((string)$result);
			}
			$event->setResponse($response);
		}
	}
}