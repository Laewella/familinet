<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\HomeBase
 */
abstract class HomeBase
{
	/**
	 * @return string
	 */
	protected abstract function getVendor();

	/**
	 * @return string
	 */
	protected abstract function getModule();

	/**
	 * @param string $applicationName
	 * @return \Rbs\Ua\Http\UI\Resources
	 */
	protected abstract function getResources($applicationName);

	/**
	 * @param \Rbs\Ua\Http\UI\Resources $resources
	 * @param array $attributes
	 * @return array
	 */
	protected function appendAttributes(\Rbs\Ua\Http\UI\Resources $resources, $attributes)
	{
		return $attributes;
	}

	/**
	 * @param \Rbs\Ua\Http\UI\Resources $resources
	 * @param array $attributes
	 * @return array
	 */
	protected function appendTemplatesAttributes(\Rbs\Ua\Http\UI\Resources $resources, $attributes)
	{
		$cachedTemplates = $attributes['cachedTemplates'] ?? [];
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(__DIR__ . '/../../Assets/Proximis',
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));

		$baseName = 'Rbs/Ua/Proximis/';
		$baseTemplateName = 'Proximis/';
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && $current->getExtension() === 'twig')
			{
				$subPathname = $current->getSubPathname();
				if (strpos($subPathname, 'directives') === 0 || strpos($subPathname, 'views') === 0)
				{
					$cachedTemplates[$baseName . $subPathname] =
						$resources->renderModuleTemplateFile('Rbs_Ua', $baseTemplateName . $subPathname, []);
				}
			}
			$it->next();
		}
		$attributes['cachedTemplates'] = $cachedTemplates;
		return $attributes;
	}

	/**
	 * @param \Rbs\Ua\Http\UI\Resources $resources
	 * @param string $baseDir
	 * @param string $vendor
	 * @param string $shortModuleName
	 * @return array
	 */
	protected function getCachedModuleTemplates(\Rbs\Ua\Http\UI\Resources $resources, $baseDir, $vendor, $shortModuleName)
	{
		$cachedTemplates = [];
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($baseDir,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));

		$baseName = $vendor . '/' . $shortModuleName . '/Ua/';
		$moduleName = $vendor . '_' . $shortModuleName;
		$baseTemplateName = 'Ua/';
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && $current->getExtension() === 'twig')
			{
				$subPathname = $current->getSubPathname();
				if (strpos($subPathname, 'directives') === 0 || strpos($subPathname, 'views') === 0)
				{
					$cachedTemplates[$baseName . $subPathname] =
						$resources->renderModuleTemplateFile($moduleName, $baseTemplateName . $subPathname, []);
				}
			}
			$it->next();
		}
		return $cachedTemplates;
	}

	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$application = $event->getApplication();
		$applicationServices = $event->getApplicationServices();
		$vendorName = $this->getVendor();
		$moduleName = $this->getModule();

		$OAuthManager = $applicationServices->getOAuthManager();

		$i18nManager = $applicationServices->getI18nManager();
		$resources = $this->getResources($event->getParam('applicationName'));
		$resources->setApplication($application)
			->setI18nManager($i18nManager)
			->setModelManager($applicationServices->getModelManager())
			->setPluginManager($applicationServices->getPluginManager());

		$applicationInfo = $resources->getApplicationInfo();

		$consumer = $OAuthManager->getConsumerByApplication($applicationInfo->getRealm());
		if (!$consumer)
		{
			$event->getController()->error($event);
			return;
		}

		$devMode = $application->inDevelopmentMode();
		if ($devMode)
		{
			$event->setParam('ACTION_NAME', get_class($this) . '::execute');
		}

		$LCID = $i18nManager->getLCID();
		$urlManager = $event->getUrlManager();
		$role = $applicationInfo->getRole();
		$applicationShortName =
			$i18nManager->trans('m.' . $vendorName . '.' . $moduleName . '.ua.' . $applicationInfo->getName() . '_title', ['ucf']);
		$applicationPackageName = $i18nManager->trans('m.rbs.ua.common.application_package_name', ['ucf']);
		$attributes = [
			'applicationName' => $applicationShortName . ' - ' . $applicationPackageName,
			'applicationShortName' => $applicationShortName,
			'applicationPackageName' => $applicationPackageName,
			'role' => $role,
			'snakeRole' => \Change\Stdlib\StringUtils::snakeCase($role, '-'),
			'devMode' => $devMode,
			'angularDevMode' => $devMode && $application->getConfiguration('Change/Presentation/AngularDevMode'),
			'baseURL' => $urlManager->getByPathInfo(null)->normalize()->toString(),
			'restURL' => $urlManager->getByPathInfo('../../uaRest.php/' . $role . '/')->normalize()->toString(),
			'applicationsMenuURL' => $urlManager->getByPathInfo('../')->normalize()->toString(),
			'LCID' => $LCID,
			'lang' => substr($LCID, 0, 2),
			'lowerHyphenLCID' => strtolower(str_replace('_', '-', $LCID)),
			'OAuth' => array_merge($consumer->toArray(), ['realm' => $applicationInfo->getRealm()]),
			'menu' => $resources->getMainMenu(),
			'routes' => $resources->getRoutes(),
			'restApi' => $resources->getRestApi(),
			'i18n' => $resources->getI18nKeys(),
			'supportedLCIDs' => $i18nManager->getSupportedLCIDs(),
			'vendorName' => $vendorName,
			'moduleName' => $moduleName,
			'application' => [
				'version' => $application->getConfiguration('Change/Application/version'),
				'uuid' => $application->getConfiguration('Change/Application/uuid'),
				'env' => $application->getConfiguration('Change/Application/env')
			],
			'navigationContext' => [
				'assetBasePath' => $resources->getAssetsBasePath()
			]
		];

		if ($application->getConfiguration('Rbs/Performance/Analytics/UA/enabled') && $attributes['application']['uuid']
			&& $attributes['application']['env']
		)
		{
			$attributes['analytics'] = ['trackerId' => $application->getConfiguration('Rbs/Performance/Analytics/UA/trackerId')];
		}

		$attributes = $this->appendAttributes($resources, $attributes);
		$attributes['__change'] = $attributes;

		$templateAttributes = $this->appendTemplatesAttributes($resources, $attributes);

		$html = $resources->renderModuleTemplateFile($vendorName . '_' . $moduleName, 'Ua/home.twig', $templateAttributes);
		$result = new \Rbs\Ua\Http\UI\HtmlResult($html);
		$event->setResult($result);
	}

}