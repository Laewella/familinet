<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API;

/**
 * @name \Rbs\Ua\Http\API\Controller
 */
class Controller extends \Change\Http\Controller
{
	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Http', 'Http.UA', 'Http.UA.API'];
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		parent::attachEvents($eventManager);

		(new \Change\Http\Rest\OAuth\ListenerAggregate())->attach($eventManager);

		$eventManager->attach(\Change\Http\Event::EVENT_AUTHENTICATE, function($event)
		{
			(new \Rbs\Ua\Http\API\BasicAuthentication)->onAuthenticate($event);
		}, 5);

		$eventManager->attach(\Change\Http\Event::EVENT_REQUEST, function ($event) {
			$this->onRequest($event);
		}, 5);

		$eventManager->attach(\Change\Http\Event::EVENT_EXCEPTION, function ($event) {
			$this->onException($event);
		}, 5);

		$eventManager->attach(\Change\Http\Event::EVENT_RESPONSE, function ($event) {
			$this->onResponse($event);
		}, 5);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function onRequest(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$applicationServices->getPermissionsManager()->allow(false);
		$request = $event->getRequest();
		$i18nManager = $applicationServices->getI18nManager();
		$request->populateLCIDByHeader($i18nManager);
		$method = $request->getMethod();
		if ($method === 'GET')
		{
			if ($applicationServices->getCacheManager()->isValidNamespace('prefetch'))
			{
				$applicationServices->getDocumentManager()->usePersistentCache(true);
			}
		}
		elseif ($method === 'PUT' || $method === 'POST')
		{
			try
			{
				$h = $request->getHeaders('Content-Type');
			}
			catch (\Exception $e)
			{
				// Header not found.
				return;
			}

			if ($h && ($h instanceof \Zend\Http\Header\ContentType) && (strpos($h->getFieldValue(), 'application/json') === 0))
			{
				$string = file_get_contents('php://input');
				$data = json_decode($string, true);
				if (is_array($data) && JSON_ERROR_NONE === json_last_error())
				{
					if (\Zend\Stdlib\ArrayUtils::isList($data))
					{
						$request->setPost(new \Zend\Stdlib\Parameters(['data' => $data]));
					}
					else
					{
						$request->setPost(new \Zend\Stdlib\Parameters($data));
					}
				}
			}
		}
	}

	/**
	 * @api
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function createResponse()
	{
		$response = parent::createResponse();
		$response->getHeaders()->addHeaderLine('Content-Type: application/json');
		return $response;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	protected function getDefaultResponse($event)
	{
		$response = $this->createResponse();
		$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_500);
		$content = ['code' => 'ERROR-GENERIC', 'message' => 'Generic error'];
		$response->setContent(json_encode($content));
		return $response;
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function onException($event)
	{
		$exception = $event->getParam('Exception');
		$data = [];
		$array = ['status' => ['error' => true, 'errorCode' => 'EXCEPTION', 'message' =>  '']];
		$httpStatus = \Zend\Http\Response::STATUS_CODE_500;

		if ($exception instanceof \Change\Http\HttpException)
		{
			$httpStatus = $exception->getHttpStatus();
			$data = $exception->getData() ?: $data;
		}

		if ($exception instanceof \Exception)
		{
			$array['status']['errorCode'] = 'EXCEPTION-' . $exception->getCode();
			$array['status']['message'] = $exception->getMessage();
			$data = $event->getParam('ExceptionData', $data);
			if ($event->getApplication()->inDevelopmentMode())
			{
				$array['traceAsString'] = explode(PHP_EOL, $exception->getTraceAsString());
			}
		}
		$array['data'] = $data ?: null;
		$event->setResult(new ArrayResult($array, $httpStatus));
		$event->setResponse(null);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function onResponse($event)
	{
		if ($event->getResponse() !== null)
		{
			return;
		}

		$result = $event->getResult();
		if ($result instanceof \Change\Http\Result)
		{
			$response = $this->createResponse();
			if ($result->getHttpStatusCode() === \Zend\Http\Response::STATUS_CODE_200 && !$result->getHeaderLastModified())
			{
				$result->setHeaderNoCache();
			}
			$response->getHeaders()->addHeaders($result->getHeaders());
			$response->getHeaders()->addHeaderLine('Change-Memory-Usage: ' . number_format(memory_get_usage()));
			$response->setStatusCode($result->getHttpStatusCode());
			$event->setResponse($response);

			if ($this->resultNotModified($event->getRequest(), $result))
			{
				$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_304);
			}

			$callable = [$result, 'toArray'];
			if (is_callable($callable))
			{
				$data = call_user_func($callable);
				$response->setContent(json_encode($data));
			}
			elseif ($result->getHttpStatusCode() === \Zend\Http\Response::STATUS_CODE_404)
			{
				$array = [
					'status' => ['error' => true, 'errorCode' => 'PATH-NOT-FOUND', 'message' => 'Unable to resolve path'],
					'data' => ['path' => $event->getRequest()->getPath()]
				];
				if ($event->getAction())
				{
					$array['status']['code'] = 'ACTION-WITHOUT-RESULT';
					$array['status']['message'] = 'No result defined for action';
				}
				$response->setContent(json_encode($array));
			}
		}
	}
}