<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\ChangeVersion
 */
class ChangeVersion
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$version = $event->getApplication()->getConfiguration('Change/Application/version');
		$result = $this->buildArrayResult($event, ['version' => $version]);
		$result->setHeaderLastModified(\Change\Stdlib\FileUtils::getModificationDate(__FILE__));
		$result->setHeaderNoCache();
		$event->setResult($result);
	}
}