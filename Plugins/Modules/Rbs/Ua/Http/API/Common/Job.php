<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Job
 */
class Job
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function checkJob($event)
	{
		$applicationService = $event->getApplicationServices();
		$i18nManager = $applicationService->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$jobId = $inputParameters->get('jobId');

		if (!$jobId)
		{
			$message = $i18nManager->trans('m.rbs.ua.ua.error_job_id_missing', ['ucf']);
			$event->setResult($this->buildErrorResult($event, $message, 'JOB_ID_MISSING', null, 409));
			return;
		}

		$jobManager = $event->getApplicationServices()->getJobManager();
		$job = $jobManager->getJob($jobId);

		if (!$job)
		{
			$message = $i18nManager->trans('m.rbs.ua.ua.error_job_not_found', ['ucf']);
			$event->setResult($this->buildErrorResult($event, $message, 'JOB_NOT_FOUND', null, 409));
			return;
		}

		$status = $job->getStatus();
		$arguments = null;
		if ($status === 'success')
		{
			$arguments = $job->getArguments();
		}

		$event->setResult($this->buildArrayResult($event, ['item' => ['id' => $job->getId(), 'status' => $status, 'jobData' => $arguments]]));
	}
}