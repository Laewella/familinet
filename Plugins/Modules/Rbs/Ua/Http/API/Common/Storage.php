<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Storage
 */
class Storage
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getStorage($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$storageName = $inputParameters['storageName'];
		$relativePath = $inputParameters['relativePath'];
		if ($storageName && $relativePath)
		{
			$applicationServices = $event->getApplicationServices();
			$path = 'change://' . $storageName . '/' . $relativePath;
			$storageManager = $applicationServices->getStorageManager();
			$itemInfo = $storageManager->getItemInfo($path);
			if ($itemInfo && $itemInfo->isFile())
			{
				$item = [
					'storageURI' => $path,
					'size' => $itemInfo->getSize(),
					'temporaryDownload' => $this->getTemporaryDownloadUrl($path, $event->getUrlManager(),
						$applicationServices->getCacheManager()),
					'mTime' => \DateTime::createFromFormat('U', $itemInfo->getMTime()),
					'mimeType' => $itemInfo->getMimeType()
				];
				$result = $this->buildArrayResult($event, ['item' => $item]);
				$event->setResult($result);
			}
		}
	}

	/**
	 * @param string $storageURI
	 * @param \Change\Http\UrlManager $urlManager
	 * @param \Change\Cache\CacheManager $cacheManager
	 * @return string|boolean
	 */
	protected function getTemporaryDownloadUrl($storageURI, $urlManager, $cacheManager)
	{
		if ($storageURI && $cacheManager && $urlManager)
		{
			$options = ['ttl' => 120];
			$key = sha1($storageURI . (new \DateTime())->getTimestamp());
			$cacheManager->setEntry('temporaryDownload', $key, $storageURI, $options);
			return $urlManager->getByPathInfo('global/temporaryDownload/' . $key)->normalize()->toString();
		}
		return false;
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getTemporaryDownload($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();
		$cacheManager = $applicationServices->getCacheManager();
		$options = ['ttl' => 120];
		$key = $inputParameters['key'];
		if ($cacheManager->hasEntry('temporaryDownload', $key, $options))
		{
			$storageURI = $cacheManager->getEntry('temporaryDownload', $key);
			$storageManager = $event->getApplicationServices()->getStorageManager();
			$itemInfo = $storageManager->getItemInfo($storageURI);
			if ($inputParameters['forceDownload'])
			{
				$event->getController()->getEventManager()
					->attach(\Change\Http\Event::EVENT_RESPONSE, function ($event) { $this->onDownloadContent($event); }, 10);
			}
			else
			{
				$event->getController()->getEventManager()
					->attach(\Change\Http\Event::EVENT_RESPONSE, function ($event) { $this->onGetFileContent($event); }, 10);
			}

			// This web-service returns a content that is not JSON, this is what justifies the hand-built \Rbs\Ua\Http\API\ArrayResult
			// Do not alter this line unless you know what you are doing
			$result = new \Rbs\Ua\Http\API\ArrayResult(['storageURI' => $storageURI, 'size' => $itemInfo->getSize(),
				'contentType' => $itemInfo->getMimeType()]);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function onDownloadContent($event)
	{
		/* @var $result \Rbs\Ua\Http\API\ArrayResult */
		$result = $event->getResult();
		$ra = $result->toArray();
		$path = $ra['storageURI'];
		$response = new \Change\Http\StreamResponse();
		if (!$event->getController()->resultNotModified($event->getRequest(), $result))
		{
			$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_200);
			$response->getHeaders()->clearHeaders();
			$result->getHeaders()->addHeaderLine('Content-Type', 'application/force-download; name="' . basename($path) . '"');
			$result->getHeaders()->addHeaderLine('Content-Transfer-Encoding', 'binary');
			$result->getHeaders()->addHeaderLine('Content-Length', $ra['size']);
			$result->getHeaders()->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($path) . '"');
			$offset = 48 * 60 * 60;
			$result->getHeaders()->addHeaderLine('Expires', gmdate('D, d M Y H:i:s', time() + $offset) . ' GMT');
			$result->getHeaders()->addHeaderLine('Cache-Control', 'no-cache, must-revalidate');
			$result->getHeaders()->addHeaderLine('Pragma', 'no-cache');
			$response->getHeaders()->addHeaders($result->getHeaders());
			$response->setUri($path);
		}
		else
		{
			$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_304);
		}
		$event->setResponse($response);
		$event->stopPropagation();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function onGetFileContent($event)
	{
		/** @var $result \Rbs\Ua\Http\API\ArrayResult */
		$result = $event->getResult();
		$ra = $result->toArray();
		$path = $ra['storageURI'];
		$octetStreamContentType = 'application/octet-stream';
		$contentType = $ra['contentType'] ?: $octetStreamContentType;

		$response = new \Change\Http\StreamResponse();
		if (!$event->getController()->resultNotModified($event->getRequest(), $result))
		{
			$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_200);
			$response->getHeaders()->clearHeaders();
			$result->getHeaders()->addHeaderLine('Content-Type', $contentType);
			$result->getHeaders()->addHeaderLine('Content-Length', $ra['size']);
			$result->getHeaders()->addHeaderLine('Cache-Control', 'no-cache, must-revalidate');
			$result->getHeaders()->addHeaderLine('Pragma', 'no-cache');
			$response->getHeaders()->addHeaders($result->getHeaders());
			$response->setUri($path);

			if ($contentType == $octetStreamContentType)
			{
				$result->getHeaders()->addHeaderLine('Content-Transfer-Encoding', 'binary');
			}
		}
		else
		{
			$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_304);
		}

		$event->setResponse($response);
		$event->stopPropagation();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function setStorage($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$storageName = $inputParameters['storageName'];
		$relativePath = ltrim((string)$inputParameters['relativePath'], '/');

		$file = $event->getRequest()->getFiles('file');
		if ($storageName && $relativePath && $file && isset($file['tmp_name']))
		{
			$applicationServices = $event->getApplicationServices();
			$storageManager = $applicationServices->getStorageManager();
			$path = $storageManager->buildChangeURI($storageName, '/' . $relativePath)->normalize()->toString();
			if (file_exists($path))
			{
				unlink($path);
			}
			elseif (explode('/', $relativePath) > 1)
			{
				\Change\Stdlib\FileUtils::mkdir(dirname($path));
			}
			move_uploaded_file($file['tmp_name'], $path);

			$itemInfo = $storageManager->getItemInfo($path);
			if ($itemInfo && $itemInfo->isFile())
			{
				$item = [
					'storageURI' => $path,
					'size' => $itemInfo->getSize(),
					'temporaryDownload' => $this->getTemporaryDownloadUrl($path, $event->getUrlManager(),
						$applicationServices->getCacheManager()),
					'mTime' => \DateTime::createFromFormat('U', $itemInfo->getMTime()),
					'mimeType' => $itemInfo->getMimeType()
				];
				$result = $this->buildArrayResult($event, ['item' => $item]);
				$event->setResult($result);
			}
		}
	}

	/**
	 * @deprecated since 1.8.0 use specialized function
	 * @param \Change\Http\Event $event
	 */
	public function getUnsafeStorage($event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		$this->getStorage($event);
	}

	/**
	 * @deprecated since 1.8.0 use specialized function
	 * @param \Change\Http\Event $event
	 */
	public function setUnsafeStorage($event)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		$this->setStorage($event);
	}
}