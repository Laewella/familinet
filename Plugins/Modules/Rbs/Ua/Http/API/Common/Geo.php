<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Geo
 */
class Geo
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getNumberConfiguration($event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$geoManager = $genericServices->getGeoManager();
		$config = $geoManager->getNumberConfiguration($event);
		ksort($config);

		$result = $this->buildArrayResult($event, ['items' => array_values($config)]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * inputParameters :
	 *   number
	 *   regionCode
	 */
	public function parseNumber($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$geoManager = $genericServices->getGeoManager();
		$inputNumber = $inputParameters->get('number');
		$inputRegionCode = $inputParameters->get('regionCode');
		$parsedNumber = $geoManager->parsePhoneNumber($i18nManager, $inputNumber, $inputRegionCode);

		if (!$parsedNumber)
		{
			$errorData = ['number' => $inputNumber, 'regionCode' => $inputRegionCode];
			throw new \Change\Http\HttpException('Invalid phone number', 100, null,
				\Zend\Http\Response::STATUS_CODE_400, $errorData);
		}

		$event->setResult($this->buildArrayResult($event, ['item' => $parsedNumber]));
	}

	/**
	 * Event params:
	 *  inputParameters:
	 *   addressId The address ID
	 *   customerId The customer ID
	 * @param \Change\Http\Event $event
	 */
	public function getAddressDetail(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();
		$collectionManager = $applicationServices->getCollectionManager();

		$userId = $inputParameters->get('customerId');
		$documentId = $inputParameters->get('addressId');

		$document = $applicationServices->getDocumentManager()->getDocumentInstance($documentId, 'Rbs_Geo_Address');
		if (!($document instanceof \Rbs\Geo\Documents\Address) || $document->getOwnerId() !== $userId)
		{
			return;
		}

		$addressFields = $document->getAddressFields();
		if ($addressFields)
		{
			$this->setAddressFieldsAPIProperties($addressFields, $collectionManager);
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($document, [
			'addressModel' => $addressFields,
			'fields' => $document->getFields()
		]);

		$event->setResult($this->buildArrayResult($event, ['item' => $document], new \Rbs\Oms\Http\API\Models($event)));
	}

	/**
	 * Event params:
	 *  inputParameters:
	 *   data The address data
	 *   addressId The address ID
	 *   customerId The customer ID
	 * @param \Change\Http\Event $event
	 */
	public function putAddressDetail(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$i18nManager = $applicationServices->getI18nManager();

		$addressData = $inputParameters->get('data');
		$addressId = $inputParameters->get('addressId');
		$userId = $inputParameters->get('customerId');

		/** @var \Rbs\Geo\Documents\Address|null $address */
		$address = $documentManager->getDocumentInstance($addressId, 'Rbs_Geo_Address');
		if (!($address instanceof \Rbs\Geo\Documents\Address))
		{
			return;
		}

		if ($addressData['_meta']['id'] === $addressId && $address->getOwnerId() === $userId)
		{
			/** @var \Rbs\User\Documents\User|null $user */
			$user = $documentManager->getDocumentInstance($userId, 'Rbs_User_User');

			/** @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$geoManager = $genericServices->getGeoManager();
			$updatedAddress = $geoManager->updateUserAddressForUa($user, $addressData);
			if ($updatedAddress)
			{
				$message = $i18nManager->trans('m.rbs.ua.common.address_successfully_updated', ['ucf']);
				$event->setResult($this->buildSuccessResult($event, $message));
				return;
			}
		}

		$message = $i18nManager->trans('m.rbs.ua.common.error_while_updating_address', ['ucf']);
		$event->setResult($this->buildErrorResult($event, $message));
	}

	/**
	 * Event params:
	 *  inputParameters:
	 *   data The address data
	 *   customerId The customer ID
	 * @param \Change\Http\Event $event
	 */
	public function postNewAddress(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();

		$addressData = $inputParameters->get('data');
		$userId = $inputParameters->get('customerId');
		if ($userId)
		{
			/** @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$createdAddress = $genericServices->getGeoManager()->addUserAddress($userId, $addressData);
			if ($createdAddress instanceof \Rbs\Geo\Documents\Address)
			{
				$message = $i18nManager->trans('m.rbs.ua.common.address_successfully_created', ['ucf']);
				$event->setResult($this->buildArrayResult($event, ['status' => ['message' => $message], 'addressId' => $createdAddress->getId()]));
				return;
			}
		}

		$message = $i18nManager->trans('m.rbs.ua.common.error_while_creating_address', ['ucf']);
		$event->setResult($this->buildErrorResult($event, $message, 'ERROR', null, \Zend\Http\Response::STATUS_CODE_409));
	}

	/**
	 * Event params:
	 *  inputParameters:
	 *   addressId The address ID
	 *   customerId The customer ID
	 * @param \Change\Http\Event $event
	 */
	public function deleteAddress(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();

		/** @var \Rbs\Geo\Documents\Address|null $address */
		$address = $applicationServices->getDocumentManager()->getDocumentInstance($inputParameters->get('addressId'), 'Rbs_Geo_Address');
		if (!($address instanceof \Rbs\Geo\Documents\Address))
		{
			return;
		}

		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			$address->delete();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
		}

		$i18nManager = $applicationServices->getI18nManager();
		$event->setResult($this->buildSuccessResult($event, $i18nManager->trans('m.rbs.ua.common.address_successfully_deleted', ['ucf'])));
	}

	/**
	 * Event params:
	 *  inputParameters:
	 *   zoneCode
	 * @param \Change\Http\Event $event
	 */
	public function getAddressFieldsCountries(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$addressFieldsCountries = [];
		$zoneCode = $inputParameters->get('zoneCode');

		$genericServices = $event->getServices('genericServices');
		if ($genericServices instanceof \Rbs\Generic\GenericServices)
		{
			$i18n = $event->getApplicationServices()->getI18nManager();
			foreach ($genericServices->getGeoManager()->getCountriesByZoneCode($zoneCode) as $country)
			{
				// Exclude countries with no address model linked to it.
				if (!$country->getAddressFields())
				{
					continue;
				}

				\Rbs\Ua\Model\ObjectModel::addAPIProperties($country, ['title' => $i18n->trans($country->getI18nTitleKey())]);
				$addressFieldsCountries[] = $country;
			}
		}
		$result = $this->buildArrayResult($event, ['items' => $addressFieldsCountries], new \Rbs\Ua\Http\API\Models($event));
		$event->setResult($result);
	}

	/**
	 * Event params:
	 *  inputParameters:
	 *   addressModelId The address model (AddressFields) ID
	 * @param \Change\Http\Event $event
	 */
	public function getAddressFieldsDetail(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		/** @var $addressFields \Rbs\Geo\Documents\AddressFields */
		$applicationServices = $event->getApplicationServices();
		$addressFields = $applicationServices->getDocumentManager()
			->getDocumentInstance($inputParameters->get('addressModelId'), 'Rbs_Geo_AddressFields');
		$collectionManager = $applicationServices->getCollectionManager();

		if ($addressFields)
		{
			$this->setAddressFieldsAPIProperties($addressFields, $collectionManager);
			$result = $this->buildArrayResult($event, ['item' => $addressFields], new \Rbs\Ua\Http\API\Models($event));
			$event->setResult($result);
		}
	}

	/**
	 * @param \Rbs\Geo\Documents\AddressFields $addressFields
	 * @param \Change\Collection\CollectionManager $collectionManager
	 */
	protected function setAddressFieldsAPIProperties($addressFields, $collectionManager)
	{
		$index = 1;
		foreach ($addressFields->getFields() as $addressField)
		{
			$values = [];
			$collectionCode = $addressField->getCollectionCode();
			if ($collectionCode)
			{
				$collection = $collectionManager->getCollection($collectionCode);
				if ($collection)
				{
					foreach ($collection->getItems() as $item)
					{
						$values[$item->getValue()] = ['value' => $item->getValue(), 'title' => $item->getTitle()];
					}
				}
			}
			\Rbs\Ua\Model\ObjectModel::addAPIProperties($addressField, [
				'index' => $addressField->getCode() === 'countryCode' ? 0 : $index,
				'matchErrorMessage' => $addressField->getCurrentLocalization()->getMatchErrorMessage(),
				'collectionCode' => $collectionCode,
				'availableValues' => $values
			]);
			$index++;
		}
	}
}