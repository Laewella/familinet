<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Barcode
 */
class Barcode
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getBarcodes($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$config = $inputParameters->get('config');
		$texts = $inputParameters->get('texts');

		$barcodesSrc = $this->generateBarcodes($config, $texts);

		$event->setResult($this->buildArrayResult($event, ['items' => $barcodesSrc]));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getBarcodesToPrint($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$config = $inputParameters->get('config');
		$texts = $inputParameters->get('texts');

		$barcodesSrc = $this->generateBarcodes($config, $texts);

		$resources = new \Rbs\Oms\Http\UI\Resources($event->getParam('applicationName'));
		$resources->setApplication($event->getApplication())
			->setI18nManager($event->getApplicationServices()->getI18nManager())
			->setPluginManager($event->getApplicationServices()->getPluginManager())
			->setModelManager($event->getApplicationServices()->getModelManager());
		$html = $resources->renderModuleTemplateFile('Rbs_Oms', 'Ua/print/barcode-list.twig',
			['barcodesSrc' => $barcodesSrc]);

		$result = $this->buildArrayResult($event, ['item' => ['html' => $html]]);
		$event->setResult($result);
	}

	/**
	 * @param $config
	 * @param $texts
	 * @return array
	 */
	protected function generateBarcodes($config, $texts):array
	{
		$format = $config['format'] ?? 'code128';
		$options = [];

		$factor = $config['factor'] ?? 1;
		if ($factor >= 1)
		{
			$options['factor'] = $factor;
		}

		$barHeight = $config['barHeight'] ?? 50;
		if ($barHeight >= 1)
		{
			$options['barHeight'] = $barHeight;
		}

		if (!($config['drawText'] ?? true))
		{
			$options['drawText'] = false;
		}
		else
		{
			$options['stretchText'] = $config['stretchText'] ?? false;
			$font = $config['font'] ?? null;
			if ($font && is_readable($font))
			{
				$options['font'] = $font;
			}

			$fontSize = $config['fontSize'] ?? 0;
			if ($fontSize)
			{
				$options['fontSize'] = $fontSize;
			}
		}

		$barcodesSrc = [];
		foreach ($texts as $text)
		{
			$options['text'] = $text;
			$barcode = \Zend\Barcode\Barcode::factory($format, 'image', $options, []);
			if ($barcode instanceof \Zend\Barcode\Renderer\Image)
			{
				ob_start();
				$barcode->render();
				$contents = ob_get_contents();
				ob_end_clean();
				$barcodesSrc[$text] = 'data:image/png;base64,' . base64_encode($contents);
			}
		}

		return $barcodesSrc;
	}
}