<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Report
 */
class Report
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function log(\Change\Http\Event $event)
	{
		$inputParameters = $event->getParam('inputParameters');
		$payloads = (array)$inputParameters->get('payloads');
		$logging = $event->getApplication()->getLogging()->getLoggerByName('phperror');
		$instance = $event->getApplication()->getConfiguration('Change/fluentd/instance') ??
			$event->getApplication()->getConfiguration('Change/Logging/writers/fluentd/instance');
		foreach ($payloads as $item)
		{
			if ($instance)
			{
				$item['serviceContext']['service'] .= ' ' . $instance;
			}
			$extra = [
				'app' => $item,
				'file' => $item['context']['reportLocation']['filePath'],
				'line' => $item['context']['reportLocation']['lineNumber'],
				'errno' => 1
			];
			$logging->err($item['message'], $extra);
		}
		$event->setResult($this->buildArrayResult($event, ['status' => ['message' => 'OK']]));
	}
}