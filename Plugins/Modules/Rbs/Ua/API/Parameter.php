<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\Parameter
 */
class Parameter
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var string
	 */
	protected $src;

	/**
	 * @var string
	 */
	protected $pattern;

	/**
	 * @var mixed
	 */
	protected $defaultValue;

	/**
	 * @var boolean
	 */
	protected $required = false;

	/**
	 * @var array
	 */
	protected $enum = [];

	/**
	 * Parameter constructor.
	 * @param $name
	 * @param $type
	 * @param $src
	 */
	public function __construct($name, $type, $src)
	{
		$this->name = $name;
		$this->type = $type;
		$this->src = $src;
		$this->setRequired($src === 'path');
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getSrc()
	{
		return $this->src;
	}

	/**
	 * @return string|null
	 */
	public function getPattern()
	{
		return $this->pattern;
	}

	/**
	 * @param string $pattern
	 * @return $this
	 */
	public function setPattern($pattern)
	{
		$this->pattern = $pattern;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDefaultValue()
	{
		return $this->defaultValue;
	}

	/**
	 * @param mixed $defaultValue
	 * @return $this
	 */
	public function setDefaultValue($defaultValue)
	{
		$this->defaultValue = $defaultValue;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getRequired()
	{
		return $this->required;
	}

	/**
	 * @param boolean $required
	 * @return $this
	 */
	public function setRequired($required)
	{
		$this->required = $required;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getEnum()
	{
		return $this->enum;
	}

	/**
	 * @param array $enum
	 * @return $this
	 */
	public function setEnum(array $enum = [])
	{
		$this->enum = $enum;
		return $this;
	}
}