<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\Definitions
 */
class Definitions
{
	/**
	 * @var \Rbs\Ua\API\Definition[]
	 */
	protected $definitions = [];

	/**
	 * @var \Rbs\Ua\Model\Models
	 */
	protected $models;

	public function __construct()
	{
		$this->models = new \Rbs\Ua\Model\Models();
	}

	/**
	 * @param string $filePath
	 */
	public function initFromFile($filePath)
	{
		if (is_readable($filePath))
		{
			$data = json_decode(file_get_contents($filePath), true);
			if (!is_array($data))
			{
				return;
			}
			$this->parseData($filePath, $data);
		}
	}

	/**
	 * @param string $filePath
	 * @param array $data
	 */
	protected function parseData($filePath, $data)
	{
		$includes = $data['includes'] ?? null;
		if ($includes && is_array($includes))
		{
			$basePath = dirname($filePath) . DIRECTORY_SEPARATOR;
			foreach ($includes as $include)
			{
				$this->initFromFile($basePath . $include);
			}
		}

		$models = $data['models'] ?? null;
		if ($models && is_array($models))
		{
			$this->models->setDefinitions($models);
		}
		$api = $data['api'] ?? null;
		if ($api && is_array($api))
		{
			$this->initFromArray($api);
		}
	}

	/**
	 * @param array $api
	 */
	public function initFromArray(array $api)
	{
		foreach ($api as $def)
		{
			if (is_array($def) && isset($def['name'], $def['path'], $def['operations']) && is_string($def['path']) && is_array($def['operations']))
			{
				$operations = [];
				foreach ($def['operations'] as $opeData)
				{
					if (isset($this->definitions[$def['path']]))
					{
						throw new \LogicException('Duplicate declaration for path ' . $def['path']);
					}

					if (is_array($opeData) && isset($opeData['method'], $opeData['action']))
					{
						if (isset($operations[$opeData['method']]))
						{
							throw new \LogicException('Duplicate method ' . $opeData['method']. ' for path ' . $def['path']);
						}

						$parameters = [];
						if (isset($opeData['parameters']) && is_array($opeData['parameters']))
						{
							$parameters = $this->buildParameters($opeData, ['path' => $def['path'], 'method' => $opeData['method']]);
						}
						$responses = $this->buildResponses($opeData['responses'] ?? [], ['path' => $def['path'], 'method' => $opeData['method']]);
						$operation = new Operation($opeData['method'], $opeData['action'], $parameters, $responses);
						if (array_key_exists('authorization', $opeData))
						{
							$operation->setAuthorization($opeData['authorization']);
						}

						$operations[$operation->getMethod()] = $operation;

						// @deprecated since 1.8.0 with no replacement
						$responseType = $opeData['responseType'] ?? null;
						if ($responseType)
						{
							trigger_error(__METHOD__ . ' responseType is deprecated on ' . $def['path'], E_USER_WARNING);
							/** @noinspection PhpDeprecationInspection */
							$operation->setResponseType($responseType);
						}
						// END @deprecated
					}
				}
				$definition = new Definition($def['name'], $def['path'], $operations);
				$this->definitions[$definition->getPath()] = $definition;
			}
		}
	}

	/**
	 * @param array $responsesData
	 * @param array $operation
	 * @return \Rbs\Ua\API\Response[]
	 */
	protected function buildResponses(array $responsesData, array $operation)
	{
		$responseStatuses = [];
		$responses = [];
		foreach ($responsesData as $responseData)
		{
			$httpStatus = $responseData['httpStatus'] ?? 200;
			if (in_array($httpStatus, $responseStatuses))
			{
				throw new \LogicException('Duplicate response status ' . $httpStatus . ' for path ' . $operation['path'] . ' and method '
					. $operation['method']);
			}
			$responseStatuses[] = $httpStatus;

			$response = new \Rbs\Ua\API\Response($httpStatus);
			if (isset($responseData['predefinedType']))
			{
				switch ($responseData['predefinedType'])
				{
					case 'Success':
						$response->setModel($this->getSuccessModel());
						$responses[] = $response;
						break;
					case 'Error':
						$response->setModel($this->getErrorModel());
						$responses[] = $response;
						break;
					default:
						break;
				}
				continue;
			}

			$propertiesData = $responseData['properties'] ?? [];
			$properties = [];
			$fields = [];
			foreach ($propertiesData as $name => $propertyData)
			{
				if (isset($properties[$name]))
				{
					throw new \LogicException('Duplicate property ' . $name . ' for path ' . $operation['path'] . ', method '
						. $operation['method'] . ' and status ' . $httpStatus);
				}

				$isField = $propertyData['fields'] ?? false;
				if ($isField)
				{
					unset($propertyData['fields'], $propertyData['required']);
					$fields[$name] = $propertyData;
				}
				else
				{
					$propertyData['required'] = true;
					$properties[$name] = $propertyData;
				}
			}

			if ($properties)
			{
				$response->setModel(new \Rbs\Ua\Model\ObjectModel('ResponseProperties', ['properties' => $properties]));
				if ($fields)
				{
					$response->setFields(new \Rbs\Ua\Model\ObjectModel('ResponseFields', ['properties' => $fields]));
				}
				$responses[] = $response;
			}
		}
		return $responses;
	}

	/**
	 * @return \Rbs\Ua\Model\ObjectModel
	 */
	protected function getSuccessModel()
	{
		return new \Rbs\Ua\Model\ObjectModel('ResponseProperties', ['properties' => [
			'status' => ['type' => 'Rbs.Ua.Status', 'required' => true]
		]]);
	}

	/**
	 * @return \Rbs\Ua\Model\ObjectModel
	 */
	protected function getErrorModel()
	{
		return new \Rbs\Ua\Model\ObjectModel('ResponseProperties', ['properties' => [
			'status' => ['type' => 'Rbs.Ua.ErrorStatus', 'required' => true],
			'data' => ['type' => 'Object'],
			'traceAsString' => ['type' => 'String[]']
		]]);
	}

	/**
	 * @param array $opeData
	 * @param array $operation
	 * @return array
	 */
	protected function buildParameters(array $opeData, array $operation)
	{
		$parameters = [];
		foreach ($opeData['parameters'] as $paramData)
		{
			if (is_array($paramData) && isset($paramData['name'], $paramData['type'], $paramData['src']))
			{
				if (isset($parameters[$paramData['name']]))
				{
					throw new \LogicException('Duplicate parameter ' . $paramData['name'] . ' for path ' . $operation['path'] . ' and method '
						. $operation['method']);
				}

				$parameter = new Parameter($paramData['name'], $paramData['type'], $paramData['src']);
				$parameter->setPattern($paramData['pattern'] ?? null);
				$parameter->setDefaultValue($paramData['defaultValue'] ?? null);
				$parameter->setRequired($parameter->getRequired() || ($paramData['required'] ?? false));
				if (($enum = $paramData['enum'] ?? false) && is_array($enum))
				{
					$parameter->setEnum($enum);
				}
				$parameters[$parameter->getName()] = $parameter;
			}
		}
		return $parameters;
	}

	/**
	 * @return Definition[]
	 */
	public function getDefinitions()
	{
		return $this->definitions;
	}

	/**
	 * @return $this
	 */
	public function resetDefinitions()
	{
		$this->definitions = [];
		return $this;
	}

	/**
	 * @return \Rbs\Ua\Model\Models
	 */
	public function getModels()
	{
		return $this->models;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	public function resolveApi($event)
	{
		$this->models->compile();
		$logging = $event->getApplication()->getLogging();
		$path = substr($event->getParam('pathInfo'), 1);
		$request = $event->getRequest();
		$method = strtoupper($request->getMethod());
		foreach ($this->definitions as $definition)
		{
			foreach ($definition->getOperations() as $operation)
			{
				if ($method === $operation->getMethod())
				{
					$pathParameters = [];
					if ($operation->checkPath($path, $definition->getPath(), $pathParameters))
					{
						$operation->compile($this->models);

						$event->setAction($this->buildCallable($operation->getAction(), $logging));

						$authorization = $operation->getAuthorization();
						if ($authorization !== false)
						{
							$event->setAuthorization($this->buildCallable($authorization, $logging));
						}
						$inputParameters = $this->buildInputParameters($operation, $pathParameters, $request);
						$event->setParam('inputParameters', $inputParameters);
						$event->setParam('inputFields', $this->buildInputFields($operation, $request));
						$event->setParam('models', $this->models);
						$event->setParam('operation', $operation);

						// @deprecated since 1.8.0 with no replacement
						$event->setParam('responseType', $operation->getResponseType());
						// END @deprecated
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * @param $strCallable
	 * @param \Change\Logging\Logging $logging
	 * @return \Callable|null
	 */
	public function buildCallable($strCallable, \Change\Logging\Logging $logging)
	{
		if (!is_string($strCallable))
		{
			return null;
		}

		$callParts = explode('::', $strCallable);
		if (count($callParts) === 1)
		{
			$className = $callParts[0];
			$methodName = null;
		}
		elseif (count($callParts) === 2)
		{
			list($className, $methodName) = $callParts;
		}
		else
		{
			$logging->error(__METHOD__, 'Invalid callable', $strCallable);
			return null;
		}

		if (!class_exists($className))
		{
			$logging->error(__METHOD__, 'The class', $className, 'does not exists');
			return null;
		}
		$class = new $className();

		$callable = $methodName ? [$class, $methodName] : $class;
		if (!is_callable($callable))
		{
			$logging->error(__METHOD__, $className, $methodName, 'is not callable');
			return null;
		}

		return $callable;
	}

	/**
	 * @param \Rbs\Ua\API\Operation $operation
	 * @param \Change\Http\Request $request
	 * @return string[]
	 */
	public function buildInputFields($operation, $request)
	{
		$inputFields = [];
		$fields = $request->getQuery('fields');
		if ($fields)
		{
			if (is_string($fields))
			{
				$fields = explode(',', $fields);
			}
			if (is_array($fields))
			{
				$fields = array_count_values($fields);
				foreach ($operation->getFieldsNames() as $name)
				{
					if (isset($fields[$name]))
					{
						$inputFields[] = $name;
					}
				}
			}
		}
		return $inputFields;
	}

	/**
	 * @param \Rbs\Ua\API\Operation $operation
	 * @param array $pathParameters
	 * @param \Change\Http\Request $request
	 * @return \Zend\Stdlib\Parameters
	 */
	public function buildInputParameters($operation, $pathParameters, $request)
	{
		$inputParameters = new \Zend\Stdlib\Parameters();
		$models = $this->getModels();
		foreach ($operation->getParameters() as $parameter)
		{
			$fallbackContext = null;
			$value = null;
			switch ($parameter->getSrc())
			{
				case 'path':
					$value = $pathParameters[$parameter->getName()];
					break;
				case 'query':
					$fallbackContext = new \Rbs\Ua\API\QueryObjectFallback();
					$model = $models->getModel($parameter->getType());
					$value = $request->getQuery($parameter->getName());
					if ($value && is_string($value))
					{
						if ($model instanceof \Rbs\Ua\Model\ArrayModel)
						{
							$value = explode(',', $value);
						}
						elseif ($model instanceof \Rbs\Ua\Model\HashTableModel)
						{
							$value = json_decode($value);
						}
					}
					break;
				case 'body':
					$value = $request->getPost($parameter->getName());
					break;
			}
			$value = $models->normalize($value, $parameter->getType(), $fallbackContext);

			if ($value === null && $parameter->getDefaultValue() !== null)
			{
				$value = $models->normalize($parameter->getDefaultValue(), $parameter->getType(), $fallbackContext);
			}

			if ($value !== null)
			{
				if (($enum = $parameter->getEnum()) && !in_array($value, $enum))
				{
					throw new \RuntimeException('Illegal Value "' . $value. '" for '. $parameter->getType() . ' parameter '
						. $parameter->getName(), 99999);
				}
				$inputParameters->set($parameter->getName(), $value);
			}
			elseif ($parameter->getRequired())
			{
				throw new \RuntimeException('Missing required ' . $parameter->getType() . ' parameter '
					. $parameter->getName(), 99999);
			}
		}
		return $inputParameters;
	}
}