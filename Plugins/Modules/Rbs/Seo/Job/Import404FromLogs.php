<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Job;

/**
 * @name \Rbs\Seo\Job\Import404FromLogs
 */
class Import404FromLogs
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$configuration = $event->getApplication()->getConfiguration();
		$credentialsPath = $configuration->getEntry('Rbs/Seo/404LogImport/credentialsPath');
		if ($credentialsPath)
		{
			$credentialsPath = $event->getApplication()->getWorkspace()->composeAbsolutePath($credentialsPath);
			if (!is_readable($credentialsPath))
			{
				$credentialsPath = null;
			}
		}
		$projectId = $configuration->getEntry('Rbs/Seo/404LogImport/projectId');
		$forwardedRuleName = $configuration->getEntry('Rbs/Seo/404LogImport/forwardedRuleName');
		if (!$credentialsPath || !$projectId || !$forwardedRuleName)
		{
			$event->failed('Invalid configurations: credentialsPath, projectId and forwardedRuleName are required!');
			return;
		}

		$scopes = [
			'https://www.googleapis.com/auth/logging.read'
		];

		$client = new \Google_Client();
		$client->setAuthConfigFile($credentialsPath);
		$client->setScopes($scopes);

		$job = $event->getJob();
		$lastImportDate = $job->getArgument('lastImportDate');
		if ($lastImportDate)
		{
			$lastImportDate = new \DateTime($lastImportDate);
		}
		else
		{
			$lastImportDate = new \DateTime();
			$lastImportDate->sub(new \DateInterval('P1D'));
		}
		$currentImportDate = new \DateTime();

		$filter = 'resource.labels.forwarding_rule_name:"' . $forwardedRuleName . '"';
		$filter .= ' AND httpRequest.status = 404';
		$filter .= ' AND timestamp > "' . $lastImportDate->format(\DateTime::RFC3339) . '"';
		$filter .= ' AND timestamp <= "' . $currentImportDate->format(\DateTime::RFC3339) . '"';

		$logging = new \Google_Service_Logging($client);
		$request = new \Google_Service_Logging_ListLogEntriesRequest();
		$request->setProjectIds([$projectId]);
		$request->setFilter($filter);
		$request->setPageSize(100);
		$request->setOrderBy('timestamp desc');

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		$websiteResolver = new \Rbs\Website\Events\WebsiteResolver();
		$urlPattern = $configuration->getEntry('Rbs/Seo/404LogImport/urlPattern');
		$urls = [];
		$remainingAttempts = 5;
		$pageToken = null;
		do
		{
			try
			{
				$response = $logging->entries->listEntries($request, ['fields' => 'entries,nextPageToken']);
			}
			catch (\Exception $e)
			{
				if (!$remainingAttempts)
				{
					throw $e;
				}
				$remainingAttempts--;
				continue;
			}

			foreach ($response->getEntries() as $entry)
			{
				/** @var \Google_Service_Logging_LogEntry $entry */
				/** @var \Google_Service_Logging_HttpRequest $httpRequest */
				$httpRequest = $entry->getHttpRequest();
				$url = $httpRequest->getRequestUrl();
				if ($urlPattern && !preg_match($urlPattern, $url))
				{
					$event->getApplication()->getLogging()->info(__METHOD__, 'Ignore URL (do not match pattern):', $url);
					continue;
				}

				if (isset($urls[$url]))
				{
					$urls[$url]['hits']++;
				}
				else
				{
					$data = $websiteResolver->resolveURL($event, $url);
					if (!$data)
					{
						$event->getApplication()->getLogging()->info(__METHOD__, 'Ignore URL (no website):', $url);
						continue;
					}
					elseif (!$data['relativePath'])
					{
						$event->getApplication()->getLogging()->info(__METHOD__, 'Ignore URL (no relative path):', $url);
						continue;
					}
					$data['hits'] = 1;
					$data['lastHitDate'] = \DateTime::createFromFormat('Y-m-d\TH:i:s+', $entry->getTimestamp());
					$urls[$url] = $data;
				}
			}
			$pageToken = $response->getNextPageToken();
			$request->setPageToken($pageToken);
		}
		while ($pageToken);

		$transactionManager = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$transactionManager->begin();

			foreach ($urls as $data)
			{
				$event->getApplication()->getLogging()->info(__METHOD__, 'Log URL:', $data['websiteId'], $data['LCID'],
					$data['relativePath'], $data['hits']);
				$seoManager->hit404Url($data['websiteId'], $data['LCID'], $data['relativePath'], $data['lastHitDate'], $data['hits']);
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}

		$reportedAt = new \DateTime();
		$reportedAt->add(new \DateInterval('PT1H'));
		$event->setResultArgument('lastImportDate', $currentImportDate->format(\DateTime::ATOM));
		$event->reported($reportedAt);
	}

	/**
	 * @param \Change\Job\Event $event
	 */
	public function onThrow($event)
	{
		$reportedAt = new \DateTime();
		$reportedAt->add(new \DateInterval('PT1H'));
		$event->reported($reportedAt);

		$job = $event->getJob();
		$event->getApplication()->getLogging()->getLoggerByName('job')->warn('Reschedule on error: ' . $job->getName() . ' ' . $job->getId() . ' at ' .
			$reportedAt->format(\DateTime::ATOM));
	}
}