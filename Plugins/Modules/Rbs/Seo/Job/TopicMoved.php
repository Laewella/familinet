<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Job;

/**
* @name \Rbs\Seo\Job\TopicMoved
*/
class TopicMoved
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$topicId = $event->getJob()->getArgument('topicId');
		$newParentSectionId = $event->getJob()->getArgument('newParentSectionId');
		if (!$topicId || !$newParentSectionId)
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$topic = $documentManager->getDocumentInstance($topicId);
		$newParentSection = $documentManager->getDocumentInstance($newParentSectionId);
		if ($topic instanceof \Rbs\Website\Documents\Topic && $newParentSection instanceof \Rbs\Website\Documents\Section)
		{
			$topicNode = $applicationServices->getTreeManager()->getNodeByDocument($topic);
			if ($topicNode && $topicNode->getParentId() == $newParentSectionId)
			{
				$modelsConfiguration = $documentManager->getNewQuery('Rbs_Seo_ModelConfiguration')->getDocuments();
				$generatePathRulesModels = [];
				foreach ($modelsConfiguration as $modelConfiguration)
				{
					if ($modelConfiguration instanceof \Rbs\Seo\Documents\ModelConfiguration)
					{
						foreach ($modelConfiguration->getLCIDArray() as $LCID)
						{
							$documentManager->pushLCID($LCID);

							/** @noinspection DisconnectedForeachInstructionInspection */
							$local = $modelConfiguration->getCurrentLocalization();
							if ($local && $local->getPathTemplate()
								&& strpos($local->getPathTemplate(), '.ancestorsTitles}') !== false)
							{
								$generatePathRulesModels[$modelConfiguration->getModelName()] = true;
							}

							/** @noinspection DisconnectedForeachInstructionInspection */
							$documentManager->popLCID();
						}
					}
				}

				if (count($generatePathRulesModels))
				{
					$transactionManager = $event->getApplicationServices()->getTransactionManager();

					try
					{
						$transactionManager->begin();
						$jobManager = $event->getApplicationServices()->getJobManager();
						foreach ($generatePathRulesModels as $modelName => $rules)
						{
							$jobManager->createNewJob('Rbs_Website_GeneratePathRulesByModel', ['modelName' => $modelName], null, false);
						}
						$transactionManager->commit();
					}
					catch (\Exception $e)
					{
						throw $transactionManager->rollBack($e);
					}
				}
			}
		}
	}
}