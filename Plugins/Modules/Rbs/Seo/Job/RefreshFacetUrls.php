<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Job;

/**
 * @name \Rbs\Seo\Job\RefreshFacetUrls
 */
class RefreshFacetUrls
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$group = $documentManager->getDocumentInstance($job->getArgument('facetUrlGroupId'));
		if (!($group instanceof \Rbs\Seo\Documents\FacetUrlGroup))
		{
			$event->failed('Invalid facetUrlGroupId argument: ' . $job->getArgument('facetUrlGroupId'));
			return;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			// Delete old path rules.
			foreach ($group->getOldPathRuleIds() as $oldPathRuleId)
			{
				$seoManager->deletePathRuleById($oldPathRuleId, $pathRuleManager);
			}
			$group->setOldPathRuleIds([]);

			// Delete old decorators.
			foreach ($group->getOldDecoratorIds() as $oldDecoratorId)
			{
				$decorator = $documentManager->getDocumentInstance($oldDecoratorId);
				if ($decorator)
				{
					$decorator->delete();
				}
			}
			$group->setOldDecoratorIds([]);

			// Create new path rules.
			$target = $group->getTargetIdInstance();
			if ($target instanceof \Rbs\Website\Documents\Section)
			{
				$website = $target->getWebsite();
			}
			elseif ($target instanceof \Rbs\Website\Documents\StaticPage)
			{
				$website = $target->getSection()->getWebsite();
			}
			if (!isset($website))
			{
				$event->failed('Invalid target for group: ' . $group->getId());
				return;
			}

			$websiteId = $website->getId();
			$LCID = $group->getTargetLCID();
			$targetId = $target->getId();

			$relativePaths = [];
			$facetDefinitions = [];
			$workspace = $event->getApplication()->getWorkspace();
			$ruleBuilder = new \Rbs\Seo\FacetUrl\PathRuleBuilder($documentManager, $pathRuleManager, $seoManager, $workspace);
			foreach ($group->getFacetUrlBatches() as $batch)
			{
				$urlPattern = $batch->getUrlPattern();
				if (!$urlPattern)
				{
					continue;
				}

				foreach ($batch->getFacetIds() as $facetId)
				{
					if (!isset($facetDefinitions[$facetId]))
					{
						$facet = $documentManager->getDocumentInstance($facetId);
						$facetDefinitions[$facetId] = ($facet instanceof \Rbs\Elasticsearch\Documents\Facet) ? $facet->getFacetDefinition() : false;
					}
				}

				foreach ($batch->getUrlDefinitions() as $urlDefinition)
				{
					// Generate decorator if missing.
					$decoratorId = $urlDefinition->getDecoratorId();
					if (!$decoratorId)
					{
						/** @var \Rbs\Seo\Documents\FacetUrlDecorator $decorator */
						$decorator = $documentManager->getNewDocumentInstanceByModelName('Rbs_Seo_FacetUrlDecorator');
						$decorator->setTargetId($targetId);
						$decorator->setTargetLCID($LCID);
						$decorator->setFacetValues($urlDefinition->getValues());
						$decorator->save();
						$decoratorId = $decorator->getId();
						$urlDefinition->setDecoratorId($decoratorId);
					}

					// Create the path rule.
					$rule = $ruleBuilder->build($urlDefinition, $websiteId, $LCID, $targetId, $urlPattern, $facetDefinitions);
					$relativePath = $rule->getRelativePath();

					// If the path rule is already generated, check if the relative path should be updated.
					$oldRuleId = $urlDefinition->getPathRuleId();
					if ($oldRuleId)
					{
						$oldRule = $pathRuleManager->getPathRuleById($oldRuleId);
						// Nothing to do (except updating the decorator id) if:
						// - the relative path is not modified
						// - the existing rule is user edited
						if ($oldRule && ($oldRule->getUserEdited() || $relativePath === $oldRule->getRelativePath()))
						{
							if ($oldRule->getUserEdited())
							{
								$event->getApplication()->getLogging()->info(__METHOD__, 'User edited rule ignored:', $oldRule->getRuleId());
							}

							if ($oldRule->getDecoratorId() !== $decoratorId)
							{
								$pathRuleManager->updateRuleDecoratorId($oldRule->getRuleId(), $decoratorId);
							}
							continue;
						}

						// Else the relative path is modified: delete the old rule.
						if ($oldRule)
						{
							$seoManager->deletePathRuleById($oldRule->getRuleId(), $pathRuleManager, $oldRule->getHash() !== $rule->getHash(), false);
						}
					}

					// Insert the new pathRule.
					if (!$pathRuleManager->getPathRule($websiteId, $LCID, $relativePath))
					{
						try
						{
							$pathRuleManager->insertPathRule($rule);
							$urlDefinition->setPathRuleId($rule->getRuleId());
							$urlDefinition->setConflict(false);
							$relativePaths[] = $relativePath;
						}
						catch (\Exception $e)
						{
							$event->getApplication()->getLogging()->exception($e);
							$urlDefinition->setPathRuleId(0);
							$urlDefinition->setConflict(true);
						}
					}
					else
					{
						$urlDefinition->setPathRuleId(0);
						$urlDefinition->setConflict(true);
					}
				}
			}
			$group->save();

			if ($group->getMeta('Job_RefreshFacetUrls') === $job->getId())
			{
				$group->setMeta('Job_RefreshFacetUrls', null);
				$group->saveMetas();
			}

			// Cleanup 404 rules.
			if ($relativePaths)
			{
				$seoManager->delete404RulesByRelativePaths($relativePaths);
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}