<?php
/**
 * Copyright (C) 2014 Ready Business System, Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo;

/**
 * @name \Rbs\Seo\SeoManager
 */
class SeoManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'SeoManager';

	const VARIABLE_REGEXP = '/\{([a-z][A-Za-z0-9.]*\.[a-z][A-Za-z0-9.]*)\}/';

	/**
	 * @var \Change\Transaction\TransactionManager
	 */
	protected $transactionManager;

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider($dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @return \Change\Db\DbProvider
	 */
	public function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @return $this
	 */
	public function setTransactionManager($transactionManager)
	{
		$this->transactionManager = $transactionManager;
		return $this;
	}

	/**
	 * @return \Change\Transaction\TransactionManager
	 */
	protected function getTransactionManager()
	{
		return $this->transactionManager;
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Seo/Events/SeoManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('getMetaVariables', function ($event) { $this->onDefaultGetMetaVariables($event); }, 5);
		$eventManager->attach('getPathVariables', function ($event) { (new \Rbs\Seo\Std\PathTemplateComposer())->onGetPathVariables($event); }, 5);
		$eventManager->attach('getMetaSubstitutions', function ($event) { $this->onDefaultGetMetaSubstitutions($event); }, 5);
		$eventManager->attach('getMetas', function ($event) { $this->onDefaultGetMetas($event); }, 5);

		$eventManager->attach('redirect404RulesToDocument', function ($event) { $this->onGet404RulesToRedirect($event); }, 10);
		$eventManager->attach('redirect404RulesToDocument', function ($event) { $this->onRedirect404RulesToDocument($event); }, 5);
		$eventManager->attach('redirect404RulesToDocument', function ($event) { $this->onCleanupRedirected404Rules($event); }, 1);

		$eventManager->attach('redirect404RulesToPathRule', function ($event) { $this->onGet404RulesToRedirect($event); }, 10);
		$eventManager->attach('redirect404RulesToPathRule', function ($event) { $this->onRedirect404RulesToPathRule($event); }, 5);
		$eventManager->attach('redirect404RulesToPathRule', function ($event) { $this->onCleanupRedirected404Rules($event); }, 1);

		$eventManager->attach('delete404RulesByRelativePaths', function ($event) { $this->onDelete404RulesByRelativePaths($event); }, 1);
	}

	/**
	 * @param \Change\Presentation\Interfaces\Page $page
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	public function getMetas($page, $document)
	{
		$decorator = null;
		if ($document instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
		{
			$decorator = $document;
			$document = $decorator->getTargetIdInstance();
		}

		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'page' => $page,
			'document' => $document,
			'decorator' => $decorator
		]);
		$eventManager->trigger('getMetas', $this, $args);
		return $args['metas'] ?? [];
	}

	/**
	 * @param \Change\Documents\AbstractDocument|\Change\Documents\Interfaces\Publishable $document
	 * @param \Change\Presentation\Interfaces\Page $page
	 * @param array $variables
	 * @param \Rbs\Seo\Documents\FacetUrlDecorator|null $decorator
	 * @return array
	 */
	public function getMetaSubstitutions($document, $page, $variables, $decorator = null)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'variables' => $variables,
			'page' => $page,
			'document' => $document,
			'decorator' => $decorator
		]);
		$eventManager->trigger('getMetaSubstitutions', $this, $args);
		return $args['substitutions'] ?? [];
	}

	/**
	 * @param string[] $functions
	 * @param \Rbs\Seo\Documents\FacetUrlDecorator|null $decorator
	 * @return array
	 */
	public function getMetaVariables($functions, $decorator = null)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'functions' => $functions,
			'decorator' => $decorator
		]);
		$eventManager->trigger('getMetaVariables', $this, $args);
		return $args['variables'] ?? [];
	}

	/**
	 * @param string $modelName
	 * @return array
	 */
	public function getPathVariables($modelName)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['modelName' => $modelName]);
		$eventManager->trigger('getPathVariables', $this, $args);
		return $args['variables'] ?? [];
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetMetaVariables($event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices instanceof \Change\Services\ApplicationServices)
		{
			$variables = $event->getParam('variables') ?: [];

			$i18nManager = $applicationServices->getI18nManager();
			$decorator = $event->getParam('decorator');
			if ($decorator instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
			{
				$facetCount = count($decorator->getFacetValues());
				for ($i = 1; $i <= $facetCount; $i++)
				{
					$variables['facet.' . $i . '.title'] =
						$i18nManager->trans('m.rbs.seo.admin.meta_variable_nth_facet_title', ['ucf'], ['NUMBER' => $i]);
					$variables['facet.' . $i . '.value'] =
						$i18nManager->trans('m.rbs.seo.admin.meta_variable_nth_facet_value', ['ucf'], ['NUMBER' => $i]);
				}
			}

			$event->setParam('variables', array_merge($variables, [
				'document.title' => $i18nManager->trans('m.rbs.seo.admin.meta_variable_document_title', ['ucf']),
				'page.title' => $i18nManager->trans('m.rbs.seo.admin.meta_variable_page_title', ['ucf']),
				'page.website.title' => $i18nManager->trans('m.rbs.seo.admin.meta_variable_website_title', ['ucf']),
				'page.section.title' => $i18nManager->trans('m.rbs.seo.admin.meta_variable_section_title', ['ucf'])
			]));
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetMetaSubstitutions($event)
	{
		$page = $event->getParam('page');
		$document = $event->getParam('document');
		if ($page instanceof \Rbs\Website\Documents\Page && $document instanceof \Change\Documents\Interfaces\Publishable)
		{
			$variables = $event->getParam('variables');
			$substitutions = $event->getParam('substitutions') ?: [];
			$section = $page->getSection();
			foreach ($variables as $variable)
			{
				switch ($variable)
				{
					case 'document.title':
						$substitutions['document.title'] = $document->getDocumentModel()->getPropertyValue($document, 'title');
						break;
					case 'page.title':
						$substitutions['page.title'] = $page->getTitle();
						break;
					case 'page.website.title':
						$website = $section ? $section->getWebsite() : null;
						if ($website instanceof \Rbs\Website\Documents\Website)
						{
							$substitutions['page.website.title'] = $website->getTitle();
						}
						break;
					case 'page.section.title':
						if ($section)
						{
							$substitutions['page.section.title'] = $section->getTitle();
						}
						break;
				}
			}

			$decorator = $event->getParam('decorator');
			if ($decorator instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
			{
				/* @var $genericServices \Rbs\Generic\GenericServices */
				$genericServices = $event->getServices('genericServices');
				$seoManager = $genericServices->getSeoManager();
				$documentManager = $event->getApplicationServices()->getDocumentManager();
				foreach ($decorator->getFacetValues() as $index => $facetValue)
				{
					if (in_array('facet.' . ($index + 1) . '.value', $variables))
					{
						$substitutions['facet.' . ($index + 1) . '.value'] = $seoManager->getFacetValueTitle($facetValue) ?: $facetValue->getValue();
					}
					if (in_array('facet.' . ($index + 1) . '.title', $variables))
					{
						$facet = $documentManager->getDocumentInstance($facetValue->getFacetId());
						if ($facet instanceof \Rbs\Elasticsearch\Documents\Facet)
						{
							$substitutions['facet.' . ($index + 1) . '.title'] = $facet->getCurrentLocalization()->getTitle();
						}
					}
				}
			}

			$event->setParam('substitutions', $substitutions);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDefaultGetMetas(\Change\Events\Event $event)
	{
		$page = $event->getParam('page');
		$document = $event->getParam('document');
		$decorator = $event->getParam('decorator');

		/* @var $seoManager \Rbs\Seo\SeoManager */
		$seoManager = $event->getTarget();
		if ($page instanceof \Change\Presentation\Interfaces\Page && $document instanceof \Change\Documents\Interfaces\Publishable)
		{
			/* @var $document \Change\Documents\Interfaces\Publishable|\Change\Documents\AbstractDocument */
			$metas = ['title' => null, 'metas' => []];

			$regExp = static::VARIABLE_REGEXP;
			$modelNames = array_merge($document->getDocumentModel()->getAncestorsNames(), [$document->getDocumentModelName()]);
			$availableVariables = $seoManager->getMetaVariables($modelNames, $decorator);

			$publicationMetas = $decorator ? $decorator->getPublicationMetas() : $document->getPublicationMetas();
			if (!$publicationMetas || $publicationMetas->getMode() === 'auto')
			{
				$modelConfiguration = $this->getModelConfiguration($document);
				if ($modelConfiguration)
				{
					$publicationMetas = $modelConfiguration->getDefaultPublicationMetas();
				}
			}
			elseif ($publicationMetas->getMode() === 'none')
			{
				$publicationMetas = null;
			}

			if ($publicationMetas)
			{
				$foundVariables = $this->getAllVariablesFromPublicationMetas($regExp, $publicationMetas);
				$variables = array_filter($foundVariables, function ($foundVariable) use ($availableVariables)
				{
					return array_key_exists($foundVariable, $availableVariables);
				});
				$substitutions = $this->getMetaSubstitutions($document, $page, $variables, $decorator);

				$metaTitle = $publicationMetas->getTitle();
				if ($metaTitle)
				{
					$metas['title'] = $this->getSubstitutedString($metaTitle, $substitutions, $regExp);
				}

				foreach ($publicationMetas->getMetas() as $meta)
				{
					$metas['metas'][] = [
						'type' => $meta->getType(),
						'name' => $meta->getName(),
						'value' => $this->getSubstitutedString($meta->getValue(), $substitutions, $regExp)
					];
				}
			}

			if (!$metas['title'])
			{
				$metas['title'] = $document->getDocumentModel()->getPropertyValue($document, 'title');
			}

			$event->setParam('metas', $metas);
		}
	}

	/**
	 * @param \Change\Documents\Interfaces\Publishable $document
	 * @return \Rbs\Seo\Documents\ModelConfiguration|null
	 */
	public function getModelConfiguration($document)
	{
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			$modelConfiguration = $this->documentManager->getDocumentInstance($document->getModelConfigurationId());
			if ($modelConfiguration instanceof \Rbs\Seo\Documents\ModelConfiguration)
			{
				return $modelConfiguration;
			}
		}
		return null;
	}

	/**
	 * @param string $meta
	 * @param array $substitutions
	 * @param string $regExp
	 * @return string|null
	 */
	protected function getSubstitutedString($meta, $substitutions, $regExp)
	{
		if ($meta && count($substitutions))
		{
			$meta = preg_replace_callback($regExp, function ($matches) use ($substitutions)
			{
				if (array_key_exists($matches[1], $substitutions))
				{
					return $substitutions[$matches[1]];
				}
				return '';
			}, $meta);
		}
		return $meta ?: null;
	}

	/**
	 * @param string $regExp
	 * @param \Rbs\Seo\Std\PublicationMetas $publicationMetas
	 * @return array
	 */
	protected function getAllVariablesFromPublicationMetas($regExp, $publicationMetas)
	{
		$matches = [];
		preg_match_all($regExp, $publicationMetas->getTitle(), $matches);
		$variables = $matches[1];
		foreach ($publicationMetas->getMetas() as $meta)
		{
			preg_match_all($regExp, $meta->getValue(), $matches);
			$variables = array_merge($variables, $matches[1]);
		}
		return array_unique($variables);
	}

	// 404 rules.

	/**
	 * @param integer[] $ruleIds
	 */
	public function hide404Rules(array $ruleIds)
	{
		if ($ruleIds)
		{
			$qb = $this->getDbProvider()->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->table('rbs_seo_dat_404'));
			$qb->assign($fb->column('hidden'), $fb->number(1));
			$qb->where($fb->in($fb->column('rule_id'), array_map(function ($id) use ($fb) { return $fb->number($id); }, $ruleIds)));
			$uq = $qb->updateQuery();
			$uq->execute();
		}
	}

	/**
	 * @param integer[] $ruleIds
	 */
	public function show404Rules(array $ruleIds)
	{
		if ($ruleIds)
		{
			$qb = $this->getDbProvider()->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->table('rbs_seo_dat_404'));
			$qb->assign($fb->column('hidden'), $fb->number(0));
			$qb->where($fb->in($fb->column('rule_id'), array_map(function ($id) use ($fb) { return $fb->number($id); }, $ruleIds)));
			$uq = $qb->updateQuery();
			$uq->execute();
		}
	}

	/**
	 * @param integer $websiteId
	 * @param string $LCID
	 * @param string $relativePath
	 * @param \DateTime $lastHitDate
	 * @param integer $count
	 */
	public function hit404Url($websiteId, $LCID, $relativePath, \DateTime $lastHitDate, $count = 1)
	{
		$hash = (new \Change\Http\Web\PathRule())->setRelativePath($relativePath)->getHash();
		$ruleId = $this->doGetRuleId($websiteId, $LCID, $hash);
		if ($ruleId)
		{
			$this->doHit404Url($ruleId, $lastHitDate, $count);
		}
		else
		{
			$this->doInsert404Url($websiteId, $LCID, $relativePath, $hash, $lastHitDate, $count);
		}
	}

	/**
	 * @param integer $ruleId
	 * @param integer $count
	 * @param \DateTime $lastHitDate
	 */
	protected function doHit404Url($ruleId, \DateTime $lastHitDate, $count)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->table('rbs_seo_dat_404'));
			$qb->assign($fb->column('hits'), $fb->addition($fb->column('hits'), $fb->integerParameter('hits')));
			$qb->assign($fb->column('last_hit_date'), $fb->dateTimeParameter('last_hit_date'));
			$qb->where($fb->eq($fb->column('rule_id'), $fb->parameter('rule_id')));
		}

		$uq = $qb->updateQuery();
		$uq->bindParameter('rule_id', $ruleId);
		$uq->bindParameter('hits', (int)$count);
		$uq->bindParameter('last_hit_date', $lastHitDate);
		$uq->execute();
	}

	/**
	 * @param integer $websiteId
	 * @param string $LCID
	 * @param string $relativePath
	 * @param string $hash
	 * @param \DateTime $lastHitDate
	 * @param integer $count
	 */
	protected function doInsert404Url($websiteId, $LCID, $relativePath, $hash, \DateTime $lastHitDate, $count)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->table('rbs_seo_dat_404'), $fb->column('website_id'), $fb->column('lcid'), $fb->column('hash'),
				$fb->column('relative_path'), $fb->column('insertion_date'), $fb->column('hits'), $fb->column('last_hit_date'))
				->addValues($fb->integerParameter('website_id'), $fb->parameter('lcid'), $fb->parameter('hash'), $fb->parameter('relative_path'),
					$fb->dateTimeParameter('insertion_date'), $fb->integerParameter('hits'), $fb->dateTimeParameter('last_hit_date'));
		}

		$iq = $qb->insertQuery();
		$iq->bindParameter('website_id', (int)$websiteId);
		$iq->bindParameter('lcid', $LCID);
		$iq->bindParameter('relative_path', $relativePath);
		$iq->bindParameter('hash', $hash);
		$iq->bindParameter('insertion_date', new \DateTime());
		$iq->bindParameter('hits', (int)$count);
		$iq->bindParameter('last_hit_date', $lastHitDate);
		$iq->execute();
	}

	/**
	 * @param array|\Change\Http\Web\PathRule $rule
	 */
	public function saveDeletedPathRule($rule)
	{
		if ($rule instanceof \Change\Http\Web\PathRule)
		{
			$rule = [
				'rule_id' => $rule->getRuleId(),
				'website_id' => $rule->getWebsiteId(),
				'lcid' => $rule->getLCID(),
				'hash' => $rule->getHash(),
				'relative_path' => $rule->getRelativePath(),
				'document_id' => $rule->getDocumentId(),
				'document_alias_id' => $rule->getDocumentAliasId(),
				'section_id' => $rule->getSectionId(),
				'http_status' => $rule->getHttpStatus(),
				'query' => $rule->getQuery(),
				'user_edited' => $rule->getUserEdited(),
				'decorator_id' => $rule->getDecoratorId()
			];
		}
		$ruleId = $this->doGetRuleId($rule['website_id'], $rule['lcid'], $rule['hash']);

		if ($ruleId)
		{
			$this->doUpdateDeletedPathRule($ruleId, $rule);
		}
		else
		{
			$this->doInsertDeletedPathRule($rule);
		}
	}

	/**
	 * @param int $ruleId
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @param bool $add404
	 * @param bool $deleteRedirects
	 */
	public function deletePathRuleById(int $ruleId, \Change\Http\Web\PathRuleManager $pathRuleManager, $add404 = true, $deleteRedirects = true)
	{
		$ruleId = (int)$ruleId;
		if ($ruleId)
		{
			$qb = $this->dbProvider->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->column('rule_id'), $fb->column('relative_path'), $fb->column('hash'), $fb->column('website_id'), $fb->column('lcid'),
				$fb->column('section_id'), $fb->column('document_id'), $fb->column('document_alias_id'), $fb->column('http_status'),
				$fb->column('query'), $fb->column('user_edited'));
			$qb->from($qb->getSqlMapping()->getPathRuleTable());
			$qb->where(
				$fb->eq($fb->column('rule_id'), $fb->integerParameter('rule_id'))
			);
			$sq = $qb->query();
			$sq->bindParameter('rule_id', $ruleId);

			$rule = $sq->getFirstResult($sq->getRowsConverter()
				->addIntCol('rule_id', 'website_id', 'section_id', 'document_id', 'document_alias_id', 'http_status')
				->addTxtCol('relative_path', 'hash', 'lcid', 'query')
				->addBoolCol('user_edited'));

			if ($rule)
			{
				// Delete the rule.
				$pathRuleManager->updateRuleStatus($ruleId, 404); // Delete the path rule.
				if ($add404)
				{
					$this->saveDeletedPathRule($rule); // Insert/update the 404 rule.
				}

				// Delete its redirects.
				if ($deleteRedirects)
				{
					$redirects = $pathRuleManager->findRedirectedRulesForQuery($rule['website_id'], $rule['lcid'], $rule['document_id'],
						$rule['section_id'], $rule['query']);
					foreach ($redirects as $redirect)
					{
						$pathRuleManager->updateRuleStatus($redirect->getRuleId(), 404); // Delete the path rule.
						if ($add404)
						{
							$this->saveDeletedPathRule($redirect); // Insert/update the 404 rule.
						}
					}
				}
			}
		}
	}

	/**
	 * @param array $rule
	 */
	protected function doInsertDeletedPathRule(array $rule)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->table('rbs_seo_dat_404'), $fb->column('website_id'), $fb->column('lcid'), $fb->column('hash'),
				$fb->column('relative_path'), $fb->column('document_id'), $fb->column('rule_data'), $fb->column('insertion_date'))
				->addValues($fb->integerParameter('website_id'), $fb->parameter('lcid'), $fb->parameter('hash'), $fb->parameter('relative_path'),
					$fb->integerParameter('document_id'), $fb->lobParameter('rule_data'), $fb->dateTimeParameter('insertion_date'));
		}

		$iq = $qb->insertQuery();
		$iq->bindParameter('website_id', (int)$rule['website_id']);
		$iq->bindParameter('lcid', $rule['lcid']);
		$iq->bindParameter('hash', $rule['hash']);
		$iq->bindParameter('relative_path', $rule['relative_path']);
		$iq->bindParameter('document_id', (int)$rule['document_alias_id'] ?: $rule['document_id']);
		$iq->bindParameter('rule_data', json_encode($rule));
		$iq->bindParameter('insertion_date', new \DateTime());
		$iq->execute();
	}

	/**
	 * @param integer $ruleId
	 * @param array $rule
	 */
	protected function doUpdateDeletedPathRule($ruleId, array $rule)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->table('rbs_seo_dat_404'));
			$qb->assign($fb->column('website_id'), $fb->integerParameter('website_id'));
			$qb->assign($fb->column('lcid'), $fb->parameter('lcid'));
			$qb->assign($fb->column('hash'), $fb->parameter('hash'));
			$qb->assign($fb->column('relative_path'), $fb->parameter('relative_path'));
			$qb->assign($fb->column('document_id'), $fb->integerParameter('document_id'));
			$qb->assign($fb->column('rule_data'), $fb->lobParameter('rule_data'));
			$qb->where($fb->eq($fb->column('rule_id'), $fb->parameter('rule_id')));
		}

		$uq = $qb->updateQuery();
		$uq->bindParameter('rule_id', $ruleId);
		$uq->bindParameter('website_id', (int)$rule['website_id']);
		$uq->bindParameter('lcid', $rule['lcid']);
		$uq->bindParameter('hash', $rule['hash']);
		$uq->bindParameter('relative_path', $rule['relative_path']);
		$uq->bindParameter('document_id', (int)$rule['document_alias_id'] ?: $rule['document_id']);
		$uq->bindParameter('rule_data', json_encode($rule));
		$uq->execute();
	}

	/**
	 * @param integer $websiteId
	 * @param string $LCID
	 * @param string $hash
	 * @return integer|null
	 */
	protected function doGetRuleId($websiteId, $LCID, $hash)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->column('rule_id'));
			$qb->from($fb->table('rbs_seo_dat_404'));
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->column('website_id'), $fb->integerParameter('website_id')),
					$fb->eq($fb->column('lcid'), $fb->parameter('lcid')),
					$fb->eq($fb->column('hash'), $fb->parameter('hash'))
				)
			);
		}

		$sq = $qb->query();
		$sq->bindParameter('website_id', (int)$websiteId);
		$sq->bindParameter('lcid', $LCID);
		$sq->bindParameter('hash', $hash);
		return $sq->getFirstResult($sq->getRowsConverter()->addIntCol('rule_id'));
	}

	/**
	 * @param integer[] $ruleIds
	 * @param integer $documentId
	 * @param string $LCID
	 * @param integer $websiteId
	 * @param integer $sectionId
	 */
	public function redirect404RulesToDocument($ruleIds, $documentId, $LCID, $websiteId, $sectionId = 0)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'ruleIds' => $ruleIds,
			'documentId' => $documentId,
			'LCID' => $LCID,
			'websiteId' => $websiteId,
			'sectionId' => $sectionId
		]);
		$eventManager->trigger('redirect404RulesToDocument', $this, $args);
	}

	/**
	 * @param integer[] $ruleIds
	 * @param \Change\Http\Web\PathRule $pathRule
	 */
	public function redirect404RulesToPathRule($ruleIds, \Change\Http\Web\PathRule $pathRule)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'ruleIds' => $ruleIds,
			'pathRule' => $pathRule
		]);
		$eventManager->trigger('redirect404RulesToPathRule', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGet404RulesToRedirect(\Change\Events\Event $event)
	{
		if ($event->getParam('rules') !== null)
		{
			return;
		}

		$ruleIds = $event->getParam('ruleIds');
		if (!$ruleIds)
		{
			return;
		}

		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select(
			$fb->alias($fb->column('relative_path'), 'relative_path'),
			$fb->alias($fb->column('hash'), 'hash')
		);
		$qb->from($fb->table('rbs_seo_dat_404'));
		$qb->where($fb->in($fb->column('rule_id'), array_map(function ($id) use ($fb) { return $fb->number($id); }, $ruleIds)));
		$rules = $qb->query()->getResults();

		$event->setParam('rules', $rules);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onRedirect404RulesToDocument(\Change\Events\Event $event)
	{
		if ($event->getParam('redirectionDone') !== null)
		{
			return;
		}

		$rules = $event->getParam('rules');
		$LCID = $event->getParam('LCID');
		$websiteId = $event->getParam('websiteId');
		$sectionId = $event->getParam('sectionId');
		$documentId = $event->getParam('documentId');
		if (!$rules || !$LCID || !$websiteId || !$documentId)
		{
			return;
		}

		$document = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($documentId);
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			throw new \RuntimeException('documentId should target a publishable document!', 999999);
		}

		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
		foreach ($rules as $rule)
		{
			if (!$pathRuleManager->getPathRule($websiteId, $LCID, $rule['relative_path']))
			{
				$pathRule = $pathRuleManager->getNewRule($websiteId, $LCID, $rule['relative_path'], $documentId, 301, $sectionId, null, true);
				$pathRuleManager->insertPathRule($pathRule);
			}
		}

		$event->setParam('redirectionDone', true);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onRedirect404RulesToPathRule(\Change\Events\Event $event)
	{
		if ($event->getParam('redirectionDone') !== null)
		{
			return;
		}

		$rules = $event->getParam('rules');
		$pathRule = $event->getParam('pathRule');
		if (!$rules || !($pathRule instanceof \Change\Http\Web\PathRule))
		{
			return;
		}

		$LCID = $pathRule->getLCID();
		$websiteId = $pathRule->getWebsiteId();
		$documentId = $pathRule->getDocumentId();
		$sectionId = $pathRule->getSectionId();
		$query = $pathRule->getQuery();

		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
		foreach ($rules as $rule)
		{
			if (!($toto = $pathRuleManager->getPathRule($websiteId, $LCID, $rule['relative_path'])))
			{
				$newPathRule = $pathRuleManager->getNewRule($websiteId, $LCID, $rule['relative_path'], $documentId, 301, $sectionId, $query, true);
				$pathRuleManager->insertPathRule($newPathRule);
			}
		}

		$event->setParam('redirectionDone', true);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onCleanupRedirected404Rules(\Change\Events\Event $event)
	{
		if ($event->getParam('cleanupDone') !== null)
		{
			return;
		}

		$ruleIds = $event->getParam('ruleIds');
		if (!$ruleIds)
		{
			return;
		}

		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table('rbs_seo_dat_404'));
		$qb->where($fb->in($fb->column('rule_id'), array_map(function ($id) use ($fb) { return $fb->number($id); }, $ruleIds)));
		$dq = $qb->deleteQuery();
		$dq->execute();

		$event->setParam('cleanupDone', true);
	}

	/**
	 * @param string[] $relativePaths
	 */
	public function delete404RulesByRelativePaths($relativePaths)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs([
			'relativePaths' => $relativePaths
		]);
		$eventManager->trigger('delete404RulesByRelativePaths', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDelete404RulesByRelativePaths(\Change\Events\Event $event)
	{
		if ($event->getParam('done') !== null)
		{
			return;
		}

		$relativePaths = $event->getParam('relativePaths');
		if (!$relativePaths)
		{
			return;
		}

		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table('rbs_seo_dat_404'));
		$qb->where($fb->in($fb->column('relative_path'), array_map(function ($path) use ($fb) { return $fb->string($path); }, $relativePaths)));
		$dq = $qb->deleteQuery();
		$dq->execute();

		$event->setParam('done', true);
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Value $facetValue
	 * @return string|null
	 */
	public function getFacetValueTitle($facetValue)
	{
		/** @var \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facetDefinitions */
		$facet = $this->documentManager->getDocumentInstance($facetValue->getFacetId());
		$definition = ($facet instanceof \Rbs\Elasticsearch\Documents\Facet) ? $facet->getFacetDefinition() : null;
		if ($definition)
		{
			$value = $facetValue->getValue();
			$valueLabel = null;
			$buckets = [['key' => $facetValue->getValue(), 'doc_count' => 1]];
			$aggregationValues = $definition->formatAggregation($definition->buildAggregations($buckets), []);
			foreach ($aggregationValues->getValues() as $aggregationValue)
			{
				if ($aggregationValue->getKey() == $value)
				{
					return $aggregationValue->getTitle();
					break;
				}
			}
			return $valueLabel;
		}
		return null;
	}
}