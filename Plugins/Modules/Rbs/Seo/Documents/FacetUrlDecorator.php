<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Documents;

/**
 * @name \Rbs\Seo\Documents\FacetUrlDecorator
 */
class FacetUrlDecorator extends \Compilation\Rbs\Seo\Documents\FacetUrlDecorator
{
	/**
	 * @var integer|null
	 */
	protected $modelConfigurationId = false;

	/**
	 * @var \Change\Presentation\Interfaces\PublicationMetas
	 */
	protected $publicationMetas = false;

	/**
	 * @var \Rbs\Seo\FacetUrl\Value[]
	 */
	protected $facetValues = false;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getLabel', function (\Change\Documents\Events\Event $event) { $this->onGetLabel($event); }, 5);
		$eventManager->attach('getAJAXData', function (\Change\Documents\Events\Event $event) { $this->onGetAJAXData($event); }, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Seo\Documents\PublishableListeners())->onGetPublicationMetas($event);
		};
		$eventManager->attach('getPublicationMetas', $callback, 5);
	}

	protected function onCreate()
	{
		$this->setWrappedFields();
		$this->fixPublicationData();
	}

	protected function onUpdate()
	{
		$this->setWrappedFields();
		$this->fixPublicationData();
	}

	/**
	 * @api
	 * @return string[]
	 */
	public function getModifiedPropertyNames()
	{
		$this->setWrappedFields();
		return parent::getModifiedPropertyNames();
	}

	protected function setWrappedFields()
	{
		if (is_array($this->facetValues))
		{
			$data = [];
			foreach ($this->facetValues as $value)
			{
				$data[] = $value->toArray();
			}
			$this->setFacetValuesData($data);
			$this->facetValues = false;
		}

		if ($this->publicationMetas !== false)
		{
			$publicationData = $this->getPublicationData();
			if (!is_array($publicationData))
			{
				$publicationData = [];
			}

			if ($this->publicationMetas instanceof \Rbs\Seo\Std\PublicationMetas)
			{
				$publicationData['metas'] = $this->publicationMetas->toArray();
			}
			else
			{
				$publicationData['metas'] = [];
			}
			$this->setPublicationData($publicationData);
			$this->publicationMetas = false;
		}
	}

	protected function fixPublicationData()
	{
		$publicationData = $this->getPublicationData();
		if (!is_array($publicationData))
		{
			$publicationData = [];
		}

		// Model configuration id.
		if (!isset($publicationData['modelConfigurationId']))
		{
			$target = $this->getTargetIdInstance();
			if ($target)
			{
				$dqb = $this->documentManager->getNewQuery('Rbs_Seo_ModelConfiguration');
				$dqb->andPredicates($dqb->eq('modelName', $target->getDocumentModelName()));
				/** @var \Rbs\Seo\Documents\ModelConfiguration $modelConfiguration */
				$modelConfiguration = $dqb->getFirstDocument();
				$publicationData['modelConfigurationId'] = $modelConfiguration->getId();
			}
		}

		// Metas.
		if (!isset($publicationData['metas']['mode']))
		{
			$publicationData['metas']['mode'] = 'auto';
		}

		$this->setPublicationData($publicationData);
	}

	/**
	 * @return integer
	 */
	public function getModelConfigurationId()
	{
		if ($this->modelConfigurationId === false)
		{
			$publicationData = $this->getPublicationData() ?: [];
			$this->modelConfigurationId = isset($publicationData['modelConfigurationId']) ? (int)$publicationData['modelConfigurationId'] : 0;
		}
		return $this->modelConfigurationId;
	}

	/**
	 * @return \Change\Presentation\Interfaces\PublicationMetas|null
	 */
	public function getPublicationMetas()
	{
		if ($this->publicationMetas === false)
		{
			$publicationData = $this->getPublicationData() ?: [];
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs([
				'metasData' => $publicationData['metas'] ?? [],
				'metas' => null
			]);
			$eventManager->trigger('getPublicationMetas', $this, $args);
			$this->publicationMetas = $args['metas'] instanceof \Change\Presentation\Interfaces\PublicationMetas ? $args['metas'] : null;
		}
		return $this->publicationMetas;
	}

	/**
	 * @param \Change\Presentation\Interfaces\PublicationMetas|null|false $publicationMetas
	 * @return $this
	 */
	public function setPublicationMetas($publicationMetas)
	{
		if ($publicationMetas === false)
		{
			unset($this->publicationMetas);
		}
		else
		{
			$this->publicationMetas = $publicationMetas;
		}
		return $this;
	}

	/**
	 * @return string
	 * @throws \RuntimeException
	 */
	public function getLabel()
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs([]);
		$documentEvent = new \Change\Documents\Events\Event('getLabel', $this, $eventArgs);
		$em->triggerEvent($documentEvent);
		return $eventArgs['label'] ?? '';
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onGetLabel(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('label') !== null)
		{
			return;
		}

		$target = $this->getTargetIdInstance();
		$label = $target instanceof \Change\Documents\Interfaces\Editable ? $target->getLabel() : '[' . $this->getTargetId() . ']';
		if (!is_array($this->getFacetValues()))
		{
			throw new \RuntimeException('Not array!!!' . var_export($this->getFacetValues(), true));
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		foreach ($this->getFacetValues() as $facetValue)
		{
			$label .= ' ' . ($seoManager->getFacetValueTitle($facetValue) ?: $facetValue->getValue());
		}
		$event->setParam('label', $label);
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Do nothing.
		return $this;
	}

	/**
	 * @return \Rbs\Seo\FacetUrl\Value[]
	 */
	public function getFacetValues()
	{
		if ($this->facetValues === false)
		{
			$this->facetValues = [];
			$data = $this->getFacetValuesData();
			if (is_array($data))
			{
				foreach ($data as $value)
				{
					$this->facetValues[] = new \Rbs\Seo\FacetUrl\Value($value);
				}
			}
		}
		return $this->facetValues;
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Value[] $values
	 */
	public function setFacetValues(array $values)
	{
		$this->facetValues = $values;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Seo\Std\FacetUrlDecoratorDataComposer($event))->toArray());
	}
}
