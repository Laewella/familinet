<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Documents;

/**
 * @name \Rbs\Seo\Documents\ModelConfiguration
 */
class ModelConfiguration extends \Compilation\Rbs\Seo\Documents\ModelConfiguration
{
	/**
	 * @var \Rbs\Seo\Std\PublicationMetas[]
	 */
	protected $defaultPublicationMetas = [];

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, [$this, 'onUpdated'], 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @throws \Exception
	 */
	public function onUpdated(\Change\Documents\Events\Event $event)
	{
		if ($this != $event->getDocument())
		{
			return;
		}

		$modifiedPropertyNames = $event->getParam('modifiedPropertyNames');
		if (!is_array($modifiedPropertyNames) || !count($modifiedPropertyNames))
		{
			return;
		}

		if (in_array('pathTemplate', $modifiedPropertyNames) || in_array('modelName', $modifiedPropertyNames))
		{
			$jm = $event->getApplicationServices()->getJobManager();
			$jm->createNewJob('Rbs_Website_GeneratePathRulesByModel', ['modelName' => $this->getModelName()]);
		}
	}

	/**
	 * @return \Rbs\Seo\Std\PublicationMetas|null
	 */
	public function getDefaultPublicationMetas()
	{
		$LCID = $this->documentManager->getLCID();
		if (!array_key_exists($LCID, $this->defaultPublicationMetas))
		{
			$metaData = $this->getDefaultMetasData();
			$this->defaultPublicationMetas[$LCID] = isset($metaData[$LCID]) ? new \Rbs\Seo\Std\PublicationMetas($metaData[$LCID]) : [];
		}
		return $this->defaultPublicationMetas[$LCID];
	}

	/**
	 * @param \Rbs\Seo\Std\PublicationMetas|null|false $value
	 * @return $this
	 */
	public function setDefaultPublicationMetas($value)
	{
		$LCID = $this->documentManager->getLCID();
		if ($value === false)
		{
			unset($this->defaultPublicationMetas[$LCID]);
		}
		else
		{
			$this->defaultPublicationMetas[$LCID] = $value;
		}
		return $this;
	}
}
