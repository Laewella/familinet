<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Documents;

/**
 * @name \Rbs\Seo\Documents\FacetUrlGroup
 */
class FacetUrlGroup extends \Compilation\Rbs\Seo\Documents\FacetUrlGroup
{
	/**
	 * @var \Zend\Stdlib\Parameters|null
	 */
	protected $content;

	/**
	 * @var \Rbs\Seo\FacetUrl\Batch[]|null
	 */
	protected $facetUrlBatches;

	/**
	 * @var int|null
	 */
	protected $facetUrlConflictCount;

	/**
	 * @var int[]|null
	 */
	protected $oldPathRuleIds;

	/**
	 * @var int[]|null
	 */
	protected $oldDecoratorIds;

	/**
	 * @var boolean
	 */
	protected $forceRefreshFacetUrl = false;

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	protected function getContent()
	{
		if ($this->content === null)
		{
			$v = $this->getContentData();
			$this->content = new \Zend\Stdlib\Parameters(is_array($v) ? $v : null);
		}
		return $this->content;
	}

	/**
	 * @return boolean
	 */
	public function getForceRefreshFacetUrl()
	{
		return $this->forceRefreshFacetUrl;
	}

	/**
	 * @param boolean $forceRefreshFacetUrl
	 * @return $this
	 */
	public function setForceRefreshFacetUrl($forceRefreshFacetUrl)
	{
		$this->forceRefreshFacetUrl = $forceRefreshFacetUrl;
		return $this;
	}

	/**
	 * @return \Rbs\Seo\FacetUrl\Batch[]
	 */
	public function getFacetUrlBatches()
	{
		if ($this->facetUrlBatches === null)
		{
			$this->doSetFacetUrlBatches($this->getContent()->get('facetUrlBatches') ?: []);
		}
		return $this->facetUrlBatches;
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Batch[] $facetUrlBatches
	 * @return $this
	 */
	protected function doSetFacetUrlBatches(array $facetUrlBatches)
	{
		$this->facetUrlBatches = [];
		foreach ($facetUrlBatches as $batch)
		{
			if ($batch instanceof \Rbs\Seo\FacetUrl\Batch)
			{
				$this->facetUrlBatches[] = $batch;
			}
			elseif (is_array($batch))
			{
				$this->facetUrlBatches[] = new \Rbs\Seo\FacetUrl\Batch($batch);
			}
		}
		return $this;
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Batch[] $facetUrlBatches
	 * @return $this
	 */
	public function setFacetUrlBatches(array $facetUrlBatches)
	{
		// Get old batches.
		/** @var \Rbs\Seo\FacetUrl\Batch[] $oldBatches */
		$oldBatches = [];
		foreach ($this->getFacetUrlBatches() as $oldBatch)
		{
			/** @var \Rbs\Seo\FacetUrl\Definition[] $oldDefinitions */
			$oldDefinitions = [];
			foreach ($oldBatch->getUrlDefinitions() as $oldDefinition)
			{
				$oldDefinitions[$oldDefinition->getKey()] = $oldDefinition;
				$decoratorId = $oldDefinition->getDecoratorId();
				if ($decoratorId)
				{
					$this->addOldDecoratorId($decoratorId);
				}

				$pathRuleId = $oldDefinition->getPathRuleId();
				if ($pathRuleId)
				{
					$this->addOldPathRuleId($pathRuleId);
				}
			}
			$oldBatches[$oldBatch->getKey()] = $oldDefinitions;
		}

		// Set new batches.
		$this->doSetFacetUrlBatches($facetUrlBatches);

		// Preserve old decoratorId and pathRuleId.
		foreach ($this->facetUrlBatches as $newBatch)
		{
			$batchKey = $newBatch->getKey();
			if (isset($oldBatches[$batchKey]))
			{
				$oldDefinitions = $oldBatches[$batchKey];
				foreach ($newBatch->getUrlDefinitions() as $newDefinition)
				{
					$definitionKey = $newDefinition->getKey();
					if (isset($oldDefinitions[$definitionKey]))
					{
						$oldDefinition = $oldDefinitions[$definitionKey];

						$decoratorId = $newDefinition->getDecoratorId() ?: $oldDefinition->getDecoratorId();
						if ($decoratorId)
						{
							$newDefinition->setDecoratorId($decoratorId);
							$this->removeOldDecoratorId($decoratorId);
						}

						$pathRuleId = $newDefinition->getPathRuleId() ?: $oldDefinition->getPathRuleId();
						if ($pathRuleId)
						{
							$newDefinition->setPathRuleId($pathRuleId);
							$this->removeOldPathRuleId($pathRuleId);
						}
					}
				}
			}
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getFacetUrlConflictCount() : int
	{
		if ($this->facetUrlConflictCount === null)
		{
			$this->facetUrlConflictCount = (int)$this->getContent()->get('facetUrlConflictCount');
		}
		return $this->facetUrlConflictCount;
	}

	/**
	 * @return int[]
	 */
	public function getOldPathRuleIds()
	{
		if ($this->oldPathRuleIds === null)
		{
			$this->setOldPathRuleIds($this->getContent()->get('oldPathRuleIds') ?: []);
		}
		return $this->oldPathRuleIds;
	}

	/**
	 * @param int[] $oldPathRuleIds
	 * @return $this
	 */
	public function setOldPathRuleIds(array $oldPathRuleIds)
	{
		$this->oldPathRuleIds = [];
		foreach ($oldPathRuleIds as $id)
		{
			if (is_numeric($id))
			{
				$this->oldPathRuleIds[] = (int)$id;
			}
		}
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function addOldPathRuleId(int $id)
	{
		$this->getOldPathRuleIds();
		if (!in_array($id, $this->oldPathRuleIds))
		{
			$this->oldPathRuleIds[] = $id;
		}
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function removeOldPathRuleId(int $id)
	{
		$this->getOldPathRuleIds();
		$index = array_search($id, $this->oldPathRuleIds);
		if ($index !== false)
		{
			unset($this->oldPathRuleIds[$index]);
		}
		$this->oldPathRuleIds = array_values($this->oldPathRuleIds);
		return $this;
	}

	/**
	 * @return int[]
	 */
	public function getOldDecoratorIds()
	{
		if ($this->oldDecoratorIds === null)
		{
			$this->setOldDecoratorIds($this->getContent()->get('oldDecoratorIds') ?: []);
		}
		return $this->oldDecoratorIds;
	}

	/**
	 * @param int[] $oldDecoratorIds
	 * @return $this
	 */
	public function setOldDecoratorIds(array $oldDecoratorIds)
	{
		$this->oldDecoratorIds = [];
		foreach ($oldDecoratorIds as $id)
		{
			if (is_numeric($id))
			{
				$this->oldDecoratorIds[] = (int)$id;
			}
		}
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function addOldDecoratorId(int $id)
	{
		$this->getOldDecoratorIds();
		if (!in_array($id, $this->oldDecoratorIds))
		{
			$this->oldDecoratorIds[] = $id;
		}
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function removeOldDecoratorId(int $id)
	{
		$this->getOldDecoratorIds();
		$index = array_search($id, $this->oldDecoratorIds);
		if ($index !== false)
		{
			unset($this->oldDecoratorIds[$index]);
		}
		$this->oldDecoratorIds = array_values($this->oldDecoratorIds);
		return $this;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function () { $this->onDefaultInsert(); }, 15);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function () { $this->onDefaultSave(); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->onDefaultCreated($event); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function () { $this->onDefaultSave(); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->onDefaultUpdated($event); }, 10);
	}

	protected function onDefaultInsert()
	{
		$query = $this->getDocumentManager()->getNewQuery('Rbs_Seo_FacetUrlGroup');
		$query->andPredicates($query->eq('targetId', $this->getTargetId()), $query->eq('targetLCID', $this->getTargetLCID()));
		$existingGroup = $query->getFirstDocument();
		if ($existingGroup)
		{
			throw new \RuntimeException('A facet URL group with the same target and LCID already exists!', 999999);
		}
		$this->refreshWebsiteId();
	}

	protected function onDefaultSave()
	{
		$this->refreshWebsiteId();
		$this->setWrappedFields();
	}

	protected function setWrappedFields()
	{
		if (is_array($this->facetUrlBatches))
		{
			$conflicts = 0;
			$data = [];
			foreach ($this->facetUrlBatches as $batch)
			{
				$data[] = $batch->toArray();
				foreach ($batch->getUrlDefinitions() as $definition)
				{
					if ($definition->getConflict())
					{
						$conflicts++;
					}
				}
			}
			$this->getContent()->set('facetUrlBatches', $data);
			$this->getContent()->set('facetUrlConflictCount', $conflicts);
			$this->facetUrlBatches = null;
		}

		if (is_array($this->oldPathRuleIds))
		{
			$this->getContent()->set('oldPathRuleIds', $this->oldPathRuleIds);
			$this->oldPathRuleIds = null;
		}

		if (is_array($this->oldDecoratorIds))
		{
			$this->getContent()->set('oldDecoratorIds', $this->oldDecoratorIds);
			$this->oldDecoratorIds = null;
		}

		if ($this->content !== null)
		{
			$this->setContentData($this->content->toArray());
			$this->content = null;
		}
	}

	protected function refreshWebsiteId()
	{
		$target = $this->getTargetIdInstance();
		$website = null;
		if ($target instanceof \Rbs\Website\Documents\StaticPage)
		{
			$website = $target->getSection()->getWebsite();
		}
		elseif ($target instanceof \Rbs\Website\Documents\Topic)
		{
			$website = $target->getWebsite();
		}
		elseif ($target instanceof \Rbs\Website\Documents\Website)
		{
			$website = $target;
		}

		if ($website instanceof \Rbs\Website\Documents\Website)
		{
			$this->setWebsiteId($website->getId());
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultCreated(\Change\Documents\Events\Event $event)
	{
		if (!$this->getMeta('Job_RefreshFacetUrls'))
		{
			$this->setForceRefreshFacetUrl(false);
			$jobManager = $event->getApplicationServices()->getJobManager();
			$this->refreshFacetUrls($jobManager);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultUpdated(\Change\Documents\Events\Event $event)
	{
		if ($this->getForceRefreshFacetUrl())
		{
			$this->setForceRefreshFacetUrl(false);
			$jobManager = $event->getApplicationServices()->getJobManager();
			$this->refreshFacetUrls($jobManager);
		}
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 */
	protected function refreshFacetUrls($jobManager)
	{
		$jobId = $this->getMeta('Job_RefreshFacetUrls');
		if ($jobId)
		{
			$job = $jobManager->getJob($jobId);
			if ($job && in_array($job->getStatus(), [\Change\Job\JobInterface::STATUS_WAITING, \Change\Job\JobInterface::STATUS_RUNNING]))
			{
				return;
			}
		}

		$job = $jobManager->createNewJob('Rbs_Seo_RefreshFacetUrls', ['facetUrlGroupId' => $this->getId()]);
		$this->setMeta('Job_RefreshFacetUrls', $job->getId());
		$this->saveMetas();
	}

	/**
	 * @return int
	 */
	public function getUrlCount() : int
	{
		$count = 0;
		foreach ($this->getFacetUrlBatches() as $batch)
		{
			$count += count($batch->getUrlDefinitions());
		}
		return $count;
	}

	/**
	 * @return int
	 */
	public function getTargetWebsiteId()
	{
		$target = $this->getTargetIdInstance();
		$website = null;
		if ($target instanceof \Rbs\Website\Documents\Section)
		{
			$website = $target->getWebsite();
		}
		elseif ($target instanceof \Rbs\Website\Documents\StaticPage)
		{
			$website = $target->getSection()->getWebsite();
		}
		return $website ? $website->getId() : 0;
	}
}
