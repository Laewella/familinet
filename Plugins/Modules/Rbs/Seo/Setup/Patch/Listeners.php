<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Setup\Patch;

/**
 * @name \Rbs\Seo\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		//Rbs_Seo_0001 v1.6 Update seo manager realm.
		//Rbs_Seo_0002 v1.6 Migrate data in ModelConfiguration documents.
		//Rbs_Seo_0003 v1.6 Initialize publication data in documents.
		//Rbs_Seo_0004 v1.6 Migrate data from old DocumentSeo to the new publicationData property in publishable documents.
		//Rbs_Seo_0005 v1.6 Purge Seo documents.

		//Rbs_Seo_0006 v1.8 Delete corrections on model configurations.
		//Rbs_Seo_0007 v1.8 Delete "Job_RefreshFacetUrls" meta on facet URL groups when the job is ended.
		//Rbs_Seo_0008 v1.8 Generate facet URL decorators for each existing facet URL.
		//Rbs_Seo_0009 v1.8 Add websiteId in FacetUrlGroup.

		//Rbs_Seo_0010 v2.1 Remove old model "Rbs_Event_Category" from the document table "Rbs_Seo_ModelConfiguration".
		$this->executePatch('Rbs_Seo_0010', 'Remove old model "Rbs_Event_Category" from the document table "Rbs_Seo_ModelConfiguration".', [$this, 'patch0010']);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Exception
	 */
	protected function patch0010(\Change\Plugins\Patch\Patch $patch)
	{
		$transactionManager = $this->applicationServices->getTransactionManager();
		$deleted=0;

		try
		{
			$transactionManager->begin();
			$dqb = $this->applicationServices->getDocumentManager()->getNewQuery('Rbs_Seo_ModelConfiguration');
			$dqb->andPredicates($dqb->eq('modelName', 'Rbs_Event_Category'));
			$documents = $dqb->getDocuments();

			foreach ($documents as $document)
			{
				$document->delete();
				$deleted++;
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$this->sendError($e->getMessage());
			$patch->setException($e);
			throw $transactionManager->rollBack($e);
		}

		$this->sendInfo('documents deleted ' . $deleted);
		$patch->addInstallationData('documentsDeleted', $deleted);

		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}
}