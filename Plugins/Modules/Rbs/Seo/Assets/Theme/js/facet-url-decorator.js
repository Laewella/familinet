(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	/**
	 * @name data-rbs-seo-facet-url-decorator
	 */
	app.directive('rbsSeoFacetUrlDecorator', ['$rootScope', '$filter', 'RbsChange.AjaxAPI', function($rootScope, $filter, AjaxAPI) {
		return {
			restrict: 'A',
			scope: false,
			link: function(scope) {
				var visibility = scope.blockParameters.visibility || 'list';
				var attributesMode = scope.blockParameters.attributesMode || 'flat';
				scope.imageFormat = scope.blockParameters.defaultImageFormat || 'attribute';

				if (scope.blockData.decoratorData) {
					scope.decoratorId = scope.blockData.decoratorData.common.id;
					scope.attributes = $filter('rbsVisibleAttributes')(scope.blockData.decoratorData, visibility, attributesMode);
				}
				else {
					scope.decoratorId = 0;
					scope.attributes = [];
				}

				var attributesCache = {};
				attributesCache['attr' + scope.decoratorId] = scope.attributes;

				$rootScope.$on('RbsSeoUpdateDecorator', function(event, args) {
					var newDecoratorId = args.decoratorId || 0;
					scope.decoratorId = newDecoratorId;
					scope.attributes = [];
					if (newDecoratorId) {
						var key = 'attr' + newDecoratorId;
						if (attributesCache.hasOwnProperty(key)) {
							scope.attributes = attributesCache[key];
						}
						else {
							AjaxAPI.getData('Rbs/Seo/FacetUrlDecorator/' + newDecoratorId, null, scope.blockData.context).then(
								function (result) {
									scope.attributes = $filter('rbsVisibleAttributes')(result.data.dataSets, visibility, attributesMode);
									attributesCache[key] = scope.attributes;
								}
							);
						}
					}
				});
			}
		};
	}]);
})();