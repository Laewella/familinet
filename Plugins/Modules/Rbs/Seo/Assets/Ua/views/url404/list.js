/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeo404ListController', ['$scope', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisModalStack',
		ProximisSeo404ListController]);

	function ProximisSeo404ListController(scope, proximisRestApi, proximisRestApiDefinition, proximisModalStack) {
		scope.filterOptions = {};

		// Actions.
		scope.actionMethods = {
			hide: function(items, selection, forceRefresh) {
				var ruleIds = [];
				angular.forEach(items, function(item) {
					ruleIds.push(item._meta.id);
				});
				selection.clear();
				var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Url404.hide', 'PUT', { ruleIds: ruleIds }));
				if (forceRefresh) {
					scope.actionMethods.refresh(promise);
				}
				return promise;
			},
			show: function(items, selection, forceRefresh) {
				var ruleIds = [];
				angular.forEach(items, function(item) {
					ruleIds.push(item._meta.id);
				});
				selection.clear();
				var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Url404.show', 'PUT', { ruleIds: ruleIds }));
				if (forceRefresh) {
					scope.actionMethods.refresh(promise);
				}
				return promise;
			},
			open: function(item) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/url404/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeo404DetailController',
					resolve: {
						parentData: function() { return { rule: item }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return true;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.none.isValid(filter)) {
						return null;
					}
					return prepareFilter(filter, null);
				}
			},
			website: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.website;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.website.isValid(filter)) {
						return null;
					}
					return prepareFilter(filter, [{
						name: 'website',
						parameters: {
							propertyName: 'website_id', operator: 'eq', value: filter.parameters.website
						}
					}]);
				}
			},
			lcid: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.lcid;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.lcid.isValid(filter)) {
						return null;
					}
					return prepareFilter(filter, [{
						name: 'lcid',
						parameters: {
							propertyName: 'lcid', operator: 'eq', value: filter.parameters.lcid
						}
					}]);
				}
			},
			relativePath: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.relativePath;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.relativePath.isValid(filter)) {
						return null;
					}
					return prepareFilter(filter, [{
						name: 'relativePath',
						parameters: {
							propertyName: 'relative_path', operator: 'contains', value: filter.parameters.relativePath
						}
					}]);
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});

		function prepareFilter(filter, conditions) {
			if (!conditions && filter.parameters['includeHidden']) {
				return null;
			}

			var staticFilter = {
				name: 'group',
				parameters: { operator: 'AND' },
				filters: []
			};

			if (conditions) {
				staticFilter.filters = conditions;
			}

			if (!filter.parameters['includeHidden']) {
				staticFilter.filters.push({
					name: 'includeHidden',
					parameters: {
						propertyName: 'hidden', operator: 'eq', value: false
					}
				});
			}
			return staticFilter;
		}
	}

	/**
	 * parentData:
	 *  - rule
	 */
	app.controller('ProximisSeo404DetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeo404DetailController]);

	function ProximisSeo404DetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack,
		proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			rule: parentData.rule,
			related: [],
			loadingData: true,
			newTarget: {
				docId: 0,
				ruleId: 0
			},
			selection: {
				all: false,
				indexes: [],
				rules: [],
				clear: clear
			}
		};

		scope.modalData.rule = parentData.rule;
		var params = { ruleId: parentData.rule._meta.id };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Url404.related', 'GET', params)).then(
			function(result) {
				scope.modalData.related = result.data.items;
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load rules related to ' + params.ruleId + '".', result);
			}
		);

		function clear() {
			if (scope.modalData.selection.all) {
				scope.modalData.selection.all = false;
			}
			else {
				setFullSelection(false);
			}
		}

		function setFullSelection(makeSelected) {
			angular.forEach(scope.modalData.selection.indexes, function(selected, index) {
				scope.modalData.selection.indexes[index] = makeSelected;
			});
		}

		function onIndexesUpdated() {
			scope.modalData.selection.rules = [];
			var selected = [];
			angular.forEach(scope.modalData.related, function(doc, index) {
				var isSelected = index < scope.modalData.selection.indexes.length && scope.modalData.selection.indexes[index];
				if (isSelected) {
					scope.modalData.selection.rules.push(doc);
				}
				selected.push(isSelected);
			});
			scope.modalData.selection.indexes = selected;
		}

		function onRelatedUpdated() {
			var oldRules = angular.copy(scope.modalData.selection.rules);
			scope.modalData.selection.rules = [];
			var selected = [];
			angular.forEach(scope.modalData.related, function(rule) {
				var isSelected = false;
				angular.forEach(oldRules, function(oldRule) {
					if (!isSelected) {
						isSelected = rulesEquals(oldRule, rule);
					}
				});
				selected.push(isSelected);
			});
			scope.modalData.selection.indexes = selected;
		}

		function rulesEquals(rule1, rule2) {
			if (!angular.isObject(rule1._meta) || !angular.isObject(rule2._meta)) {
				return false;
			}
			var equals = true;
			angular.forEach(rule1._meta, function(value, key) {
				if (rule2._meta[key] != value) {
					equals = false;
				}
			});
			return equals;
		}

		scope.$watch('modalData.selection.all', setFullSelection);
		scope.$watchCollection('modalData.related', onRelatedUpdated);
		scope.$watch('modalData.selection.indexes', onIndexesUpdated, true);

		scope.redirectToDocument = function() {
			var params = {
				targetId: scope.modalData.newTarget.docId,
				LCID: scope.modalData.rule.common.LCID,
				websiteId: scope.modalData.rule.common.websiteId
			};
			return redirectRules(params, 'Rbs_Seo_Url404.redirectToDocument');
		};

		scope.redirectToPathRule = function() {
			var params = {
				targetId: scope.modalData.newTarget.ruleId
			};
			return redirectRules(params, 'Rbs_Seo_Url404.redirectToPathRule');
		};

		function redirectRules(params, serviceName) {
			scope.modalData.savingData = true;

			params.ruleIds = [parentData.rule._meta.id];
			angular.forEach(scope.modalData.selection.rules, function(item) {
				params.ruleIds.push(item._meta.id);
			});

			var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData(serviceName, 'PUT', params));
			promise.then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to redirect rules.', result);
				}
			);
			return promise;
		}

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();