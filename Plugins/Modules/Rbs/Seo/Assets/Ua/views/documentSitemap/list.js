/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoDocumentSitemapListController', ['$scope', 'proximisModalStack', 'proximisDocumentModels',
		ProximisSeoDocumentSitemapListController]);

	function ProximisSeoDocumentSitemapListController(scope, proximisModalStack, proximisDocumentModels) {
		scope.globalData = {
			websites: [],
			models: proximisDocumentModels.getByFilter({ publishable: true }),
			restParams: {
				model: null
			}
		};
		scope.filterOptions = {
			restParams: scope.globalData.restParams
		};

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/documentSitemap/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoDocumentSitemapDetailController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id, websites: scope.globalData.websites }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return !!scope.globalData.restParams.model;
				}
			},
			label: {
				isValid: function(filter) {
					return scope.globalData.restParams.model && filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);

			scope.$watch('globalData.restParams.model', function(model) {
				if (model && scope.actionMethods.refresh) {
					scope.actionMethods.refresh();
				}
			});
		});
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - websites {id, LCID, label}[]
	 */
	app.controller('ProximisSeoDocumentSitemapDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent',
		'proximisModalStack', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n',
		ProximisSeoDocumentSitemapDetailController]);

	function ProximisSeoDocumentSitemapDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack,
		proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			detail: null,
			websiteExclusions: []
		};

		var params = { documentId: parentData.documentId };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentSitemap.detail', 'GET', params)).then(
			function(result) {
				scope.modalData.detail = result.data.item;
				if (scope.modalData.detail && parentData.websites.length) {
					angular.forEach(parentData.websites, function(website) {
						website.excluded = false;
						angular.forEach(scope.modalData.detail.data.inactiveWebsiteIds, function(websiteId) {
							if (website.value == websiteId) {
								website.excluded = true;
							}
						});
						scope.modalData.websiteExclusions.push(website);
					});
				}
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			scope.modalData.detail.data.inactiveWebsiteIds = [];
			angular.forEach(scope.modalData.websiteExclusions, function(website) {
				if (website.excluded) {
					scope.modalData.detail.data.inactiveWebsiteIds.push(website.value);
				}
			});
			var params = { documentId: scope.modalData.detail._meta.id, data: scope.modalData.detail.data };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentSitemap.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();