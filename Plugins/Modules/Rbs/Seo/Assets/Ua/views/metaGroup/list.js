/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoMetaGroupListController', ['$scope', 'proximisCoreI18n', 'proximisRestApi', 'proximisRestApiDefinition',
		'proximisModalStack', 'proximisMessages', ProximisSeoMetaGroupListController]);

	function ProximisSeoMetaGroupListController(scope, proximisCoreI18n, proximisRestApi, proximisRestApiDefinition, proximisModalStack,
		proximisMessages) {
		scope.openAddMetaGroupModal = function() {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/metaGroup/detail-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoAddMetaGroupController',
				resolve: {
					refreshParent: function() { return scope.actionMethods.refresh; }
				}
			});
		};

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/metaGroup/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoMetaGroupDetailController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			},
			delete: function(document) {
				if (confirm(proximisCoreI18n.trans('m.rbs.seo.ua.confirm_deleting_meta_group', ['ucf']))) {
					var params = { metaGroupId: document._meta.id };
					var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaGroup.detail', 'DELETE', params));
					scope.actionMethods.refresh(promise);
				}
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			label: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});
	}

	app.controller('ProximisSeoAddMetaGroupController', ['$uibModalInstance', '$scope', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoAddMetaGroupController]);

	function ProximisSeoAddMetaGroupController(modalInstance, scope, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: false,
			savingData: false,
			errorMessage: null,
			detail: {
				common: {
					label: null,
					content: []
				}
			}
		};

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { data: scope.modalData.detail.common };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaGroup.new', 'POST', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable create new meta group.', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - documentId
	 */
	app.controller('ProximisSeoMetaGroupDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoMetaGroupDetailController]);

	function ProximisSeoMetaGroupDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack,
		proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			detail: null
		};

		var params = { metaGroupId: parentData.documentId };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaGroup.detail', 'GET', params)).then(
			function(result) {
				scope.modalData.detail = result.data.item;
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { metaGroupId: scope.modalData.detail._meta.id, data: scope.modalData.detail.common };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaGroup.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();