/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoModelSitemapListController', ['$scope', 'proximisModalStack', ProximisSeoModelSitemapListController]);

	function ProximisSeoModelSitemapListController(scope, proximisModalStack) {
		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/modelSitemap/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoModelSitemapDetailController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			label: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});
	}

	/**
	 * parentData:
	 *  - documentId
	 */
	app.controller('ProximisSeoModelSitemapDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoModelSitemapDetailController]);

	function ProximisSeoModelSitemapDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack,
		proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			detail: null
		};

		var params = { modelSitemapId: parentData.documentId };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelSitemap.detail', 'GET', params)).then(
			function(result) {
				scope.modalData.detail = result.data.item;
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { modelSitemapId: parentData.documentId, data: scope.modalData.detail.data };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelSitemap.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();