/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoFacetUrlGroupListController', ['$scope', 'proximisModalStack', 'proximisRestApiDefinition', 'proximisRestApi',
		'proximisMessages', 'proximisCoreI18n', ProximisSeoFacetUrlGroupListController]);

	function ProximisSeoFacetUrlGroupListController(scope, proximisModalStack, proximisRestApiDefinition, proximisRestApi, proximisMessages, proximisCoreI18n) {
		scope.globalData = {
			websites: [],
			website: null,
			restParams: {
				websiteId: null,
				LCID: null
			}
		};

		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Website.list', 'GET', {})).then(
			function(result) {
				scope.globalData.websites = result.data.items;
				if (result.data.length === 1) {
					scope.globalData.website = result.data.items[0];
				}
			},
			function(result) {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'), result.data.status.message, true);
				console.error('Unable to to load websites!', result.data.status.message);
			}
		);

		scope.$watch('globalData.website', function() {
			scope.globalData.restParams.websiteId = scope.globalData.website ? scope.globalData.website.id : null;
			scope.globalData.restParams.LCID = scope.globalData.website ? scope.globalData.website.LCID : null;
		});

		scope.openAddRuleModal = function() {
			if (!scope.globalData.website) {
				return;
			}

			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/add-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoAddFacetUrlGroupController',
				resolve: {
					parentData: function() { return { website: angular.copy(scope.globalData.website) }; },
					refreshParent: function() { return scope.actionMethods.refresh; }
				}
			});
		};

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				if (!scope.globalData.website) {
					return;
				}

				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoFacetUrlGroupDetailController',
					resolve: {
						parentData: function() {
							return {
								documentId: document._meta.id, websiteId: scope.globalData.website.id, LCID: scope.globalData.website.LCID
							};
						},
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			},
			openList: function(document) {
				if (!scope.globalData.website) {
					return;
				}

				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/list-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoFacetUrlListController',
					resolve: {
						parentData: function() {
							return {
								documentId: document._meta.id,
								websiteId: scope.globalData.website.id,
								LCID: scope.globalData.website.LCID
							};
						},
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			},
			delete: function(document) {
				if (confirm(proximisCoreI18n.trans('m.rbs.seo.ua.confirm_deleting_urls', ['ucf']))) {
					var params = {
						websiteId: scope.globalData.restParams.websiteId, LCID: scope.globalData.restParams.LCID, documentId: document._meta.id
					};
					var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.detail', 'DELETE', params));
					scope.actionMethods.refresh(promise);
				}
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return scope.globalData.website;
				}
			},
			targetId: {
				isValid: function(filter) {
					return scope.globalData.website && filter.parameters && filter.parameters.targetId;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.targetId.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'targetId',
							parameters: {
								propertyName: 'targetId', operator: 'eq', value: filter.parameters.targetId
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});
	}

	/**
	 * parentData:
	 *  - website {id, LCID, label}
	 */
	app.controller('ProximisSeoAddFacetUrlGroupController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent',
		'proximisModalStack', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n',
		ProximisSeoAddFacetUrlGroupController]);

	function ProximisSeoAddFacetUrlGroupController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			newGroup: {
				targetId: null,
				websiteId: parentData.website.id,
				LCID: parentData.website.LCID
			},
			website: parentData.website,
			savingData: false,
			errorMessage: null
		};

		scope.data = {};

		scope.saveNewFacetUrlGroup = function() {
			scope.modalData.savingData = true;

			var batches = [];
			angular.forEach(scope.data.batches, function(batch) {
				var batchData = {
					facetIds: [],
					urlPattern: batch.urlPattern,
					urlDefinitions: []
				};

				angular.forEach(batch.facets, function(facet) {
					batchData.facetIds.push(facet.id);
				});

				function convertDefinition(definitions, definitionData) {
					angular.forEach(definitions, function(subDefinitions, key) {
						var subDefinitionData = angular.copy(definitionData);
						subDefinitionData.push({
							facetId: batchData.facetIds[definitionData.length],
							value: key.substr(2)
						});
						if (subDefinitions === true) {
							batchData.urlDefinitions.push({ values: subDefinitionData });
						}
						else if (angular.isObject(subDefinitions)) {
							convertDefinition(subDefinitions, subDefinitionData);
						}
					});
				}

				convertDefinition(batch.urlDefinitions, []);

				batches.push(batchData);
			});

			scope.modalData.newGroup.facetUrlBatches = batches;

			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.new', 'POST', scope.modalData.newGroup)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.savingData = false;
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save new facet URL group.', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - websiteId
	 *  - LCID
	 */
	app.controller('ProximisSeoFacetUrlGroupDetailController', ['$timeout', '$uibModalInstance', '$scope', 'parentData', 'refreshParent',
		'proximisModalStack', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n',
		ProximisSeoFacetUrlGroupDetailController]);

	function ProximisSeoFacetUrlGroupDetailController($timeout, modalInstance, scope, parentData, refreshParent, proximisModalStack,
		proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			document: null,
			websiteLabel: null,
			savingData: false,
			errorMessage: null
		};

		var reloadTimeout;
		scope.$on('$destroy', function() {
			if (reloadTimeout) {
				$timeout.cancel(reloadTimeout);
			}
		});

		scope.data = {
			batches: [],
			document: null,
			documentLoaded: false
		};

		var params = {
			websiteId: parentData.websiteId,
			LCID: parentData.LCID,
			documentId: parentData.documentId
		};

		function loadData() {
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.detail', 'GET', params)).then(
				function(result) {
					scope.data.document = result.data.item;
					scope.data.documentLoaded = true;
					if (scope.data.document.generating) {
						reloadTimeout = $timeout(function() { loadData(); }, 5000);
					}
					else {
						initData(scope.data.document);
					}
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
				}
			);
		}

		loadData();

		function initData(document) {
			angular.forEach(document.facetUrlBatches, function(batchData) {
				if (!batchData.facetIds || !batchData.facetIds.length) {
					return;
				}

				var batch = {
					key: null,
					facets: [],
					facetIds: batchData.facetIds,
					urlDefinitions: {},
					path: [],
					urlPattern: batchData.urlPattern,
					currentURLCount: 0,
					possibleURLCount: 1,
					collapsed: false
				};

				angular.forEach(batchData.urlDefinitions, function(definitionData) {
					convertDefinition(definitionData.values, batch.urlDefinitions);
				});

				function convertDefinition(definitionData, definitions) {
					if (definitionData.length < 1) {
						return;
					}
					var key = 'v-' + definitionData[0].value;
					if (definitionData.length == 1) {
						definitions[key] = true;
					}
					else {
						if (!definitions[key]) {
							definitions[key] = {};
						}
						convertDefinition(definitionData.slice(1), definitions[key]);
					}
				}

				scope.data.batches.push(batch);
			});
		}

		scope.updateFacetUrlGroup = function() {
			scope.modalData.savingData = true;

			var batches = [];
			angular.forEach(scope.data.batches, function(batch) {
				var batchData = {
					facetIds: [],
					urlPattern: batch.urlPattern,
					urlDefinitions: []
				};

				angular.forEach(batch.facets, function(facet) {
					batchData.facetIds.push(facet.id);
				});

				function convertDefinition(definitions, definitionData) {
					angular.forEach(definitions, function(subDefinitions, key) {
						var subDefinitionData = angular.copy(definitionData);
						subDefinitionData.push({
							facetId: batchData.facetIds[definitionData.length],
							value: key.substr(2)
						});
						if (subDefinitions === true) {
							batchData.urlDefinitions.push({ values: subDefinitionData });
						}
						else if (angular.isObject(subDefinitions)) {
							convertDefinition(subDefinitions, subDefinitionData);
						}
					});
				}

				convertDefinition(batch.urlDefinitions, []);

				batches.push(batchData);
			});
			scope.data.document.facetUrlBatches = batches;
			var params = {
				websiteId: scope.data.document.websiteId,
				LCID: scope.data.document.targetLCID,
				documentId: scope.data.document._meta.id,
				data: scope.data.document
			};

			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.savingData = false;
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to update facet URL group.', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - websiteId
	 *  - LCID
	 */
	app.controller('ProximisSeoFacetUrlListController', ['$timeout', '$uibModalInstance', '$scope', 'parentData', 'refreshParent',
		'proximisModalStack', 'proximisRestApi', 'proximisRestApiDefinition', ProximisSeoFacetUrlListController]);

	function ProximisSeoFacetUrlListController($timeout, modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi, proximisRestApiDefinition) {
		scope.modalData = {
			document: null,
			urls: null,
			websiteLabel: null,
			savingData: false,
			errorMessage: null,
			documentLoaded: false
		};

		var reloadTimeout;
		scope.$on('$destroy', function() {
			if (reloadTimeout) {
				$timeout.cancel(reloadTimeout);
			}
		});

		var params = {
			websiteId: parentData.websiteId,
			LCID: parentData.LCID,
			documentId: parentData.documentId,
			fields: ['url']
		};

		function loadData() {
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.detail', 'GET', params)).then(
				function(result) {
					scope.modalData.document = result.data.item;
					scope.modalData.urls = result.data.url;
					scope.modalData.documentLoaded = true;
					if (scope.modalData.document.generating) {
						reloadTimeout = $timeout(function() { loadData(); }, 5000);
					}
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
				}
			);
		}

		loadData();

		scope.editUrl = function(urlIndex, url) {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/url-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoFacetUrlController',
				resolve: {
					parentData: function() {
						return {
							urlGroup: scope.modalData.document,
							urlIndex: urlIndex,
							url: url
						};
					},
					refreshParent: function() {
						return function() {
							loadData();
							refreshParent();
						}
					}
				}
			});
		};

		scope.editMeta = function(urlIndex, url) {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/documentMetas/detail-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoDocumentMetasDetailController',
				resolve: {
					parentData: function() {
						return {
							documentId: url.decoratorId,
							LCID: url.pathRule.common.LCID
						};
					},
					refreshParent: function() {
						return function() {
							loadData();
							refreshParent();
						}
					}
				}
			});
		};

		scope.editAttributes = function(urlIndex, url) {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/attributes-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoFacetUrlAttributesController',
				resolve: {
					parentData: function() {
						return {
							documentId: url.decoratorId,
							urlGroup: scope.modalData.document,
							url: url
						};
					},
					refreshParent: function() {
						return function() {
							loadData();
							refreshParent();
						}
					}
				}
			});
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - urlGroup
	 *  - urlIndex
	 *  - url
	 */
	app.controller('ProximisSeoFacetUrlController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', 'proximisCoreArray', ProximisSeoFacetUrlController]);

	function ProximisSeoFacetUrlController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n, proximisCoreArray) {
		scope.modalData = {
			document: null,
			websiteLabel: null,
			savingData: false,
			errorMessage: null,
			originalPaths: [],
			unchanged: true,
			newRuleLastId: -2,
			urlGroup: angular.copy(parentData.urlGroup),
			url: angular.copy(parentData.url)
		};

		scope.saveProgress = {
			success: false,
			error: false,
			running: false
		};
		scope.newRedirect = {};
		scope.httpStatuses = [301, 302];
		var types = ['urls', 'redirects'];

		function initPathRule() {
			scope.modalData.originalLocation = {
				urls: [{
					id: parentData.url.pathRule._meta.id,
					relativePath: parentData.url.pathRule.common.relativePath,
					userEdited: parentData.url.pathRule.common.userEdited,
					httpStatus: parentData.url.pathRule.common.httpStatus,
					query: parentData.url.pathRule.common.query,
					conflict: parentData.url.conflict
				}],
				redirects: parentData.url.redirects
			};
			scope.location = angular.copy(scope.modalData.originalLocation);
			scope.modalData.unchanged = true;

			scope.displayConfig = {
				'urls': [{ 'edit': false }],
				'redirects': []
			};

			scope.modalData.originalPaths.push(parentData.url.pathRule.relativePath);
			for (var i = 0; i < scope.location.redirects.length; i++) {
				scope.displayConfig['redirects'].push({ 'edit': false });
				scope.modalData.originalPaths.push(scope.location.redirects[i].relativePath);
			}

			scope.clearNewRedirect();

			if (scope.saveProgress.running) {
				scope.saveProgress.running = scope.saveProgress.error = false;
				scope.saveProgress.success = true;
			}
		}

		function addRedirect(relativePath, httpStatus, id) {
			scope.location.redirects.push({
				id: id ? id : scope.modalData.newRuleLastId--,
				relativePath: relativePath,
				httpStatus: httpStatus ? httpStatus : 301,
				query: parentData.url.pathRule.common.query,
				userEdited: true
			});
			scope.displayConfig.redirects.push({ 'edit': false });
		}

		function findRuleByPath(relativePath) {
			for (var i = 0; i < types.length; i++) {
				for (var j = 0; j < scope.location[types[i]].length; j++) {
					if (scope.location[types[i]][j].relativePath === relativePath) {
						return { type: types[i], index: i, rule: scope.location[types[i]][j] };
					}
				}
			}
		}

		function checkRelativePath(relativePath, localRuleCheck) {
			var localRule = findRuleByPath(scope.newRedirect.relativePath);
			if (localRule) {
				return $q(function(resolve) { resolve({ used: localRuleCheck ? localRuleCheck(localRule) : true, localRule: localRule }); });
			}

			if (proximisCoreArray.inArray(relativePath, scope.modalData.originalPaths)) {
				return $q(function(resolve) { resolve({ used: false }); });
			}

			var params = {
				websiteId: parentData.urlGroup.websiteId,
				LCID: parentData.urlGroup.targetLCID,
				relativePath: relativePath
			};
			return proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_PathRule.check', 'GET', params));
		}

		function refreshUnchanged() {
			for (var i = 0; i < types.length; i++) {
				if (scope.location[types[i]].length !== scope.modalData.originalLocation[types[i]].length) {
					scope.modalData.unchanged = false;
					return;
				}

				for (var j = 0; j < scope.location[types[i]].length; j++) {
					var rule = scope.location[types[i]][j];
					var oRule = scope.modalData.originalLocation[types[i]][j];
					if (rule.id !== oRule.id || rule.relativePath !== oRule.relativePath || rule.httpStatus !== oRule.httpStatus) {
						scope.modalData.unchanged = false;
						return;
					}
				}
			}
			scope.modalData.unchanged = true;
		}

		scope.getHref = function(url) {
			return parentData.urlGroup.baseUrl + url.relativePath;
		};

		scope.save = function() {
			scope.modalData.savingData = true;

			var pathRule = parentData.url.pathRule;
			pathRule._meta.id = scope.location.urls[0].id;
			pathRule.common.relativePath = scope.location.urls[0].relativePath;
			pathRule.common.userEdited = scope.location.urls[0].userEdited;
			pathRule.common.httpStatus = scope.location.urls[0].httpStatus;
			pathRule.common.query = scope.location.urls[0].query;

			var url = {
				values: parentData.url.values,
				conflict: false,
				pathRule: pathRule,
				redirects: scope.location.redirects
			};
			var params = {
				websiteId: parentData.url.pathRule.common.websiteId,
				LCID: parentData.url.pathRule.common.LCID,
				documentId: parentData.urlGroup.targetId,
				facetUrl: url
			};
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.urlList', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					scope.cancel();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.startEdit = function(type, index) {
			scope.location[type][index].newRelativePath = scope.location[type][index].relativePath;
			scope.location[type][index].newHttpStatus = scope.location[type][index].httpStatus;
			scope.displayConfig[type][index].edit = true;
		};

		scope.endEdit = function(type, index) {
			scope.modalData.errorMessage = null;
			delete scope.location[type][index].newRelativePath;
			delete scope.location[type][index].newHttpStatus;
			scope.displayConfig[type][index].edit = false;
		};

		scope.isModified = function(type, index) {
			var rule = scope.location[type][index];
			return (rule.relativePath != rule.newRelativePath || rule.httpStatus != rule.newHttpStatus);
		};

		// URLs handling.
		scope.updateUrl = function(index) {
			scope.modalData.errorMessage = null;
			if (scope.isModified('urls', index)) {
				var localRuleCheck = function(localRule) { return localRule.type === 'redirects'; };
				checkRelativePath(scope.location.urls[index].newRelativePath, localRuleCheck).then(
					function(result) {
						if (!result.data.used) {
							if (result.data.localRule) {
								result.data.localRule.rule.relativePath = null;
								result.data.localRule.rule.httpStatus = 404;
							}
							var url = scope.location.urls[index];
							if (url.conflict) {
								url.conflict = false;
							}
							else {
								addRedirect(url.relativePath, 301, url.id);
								url.id = scope.modalData.newRuleLastId--;
							}
							url.relativePath = url.newRelativePath;
							scope.endEdit('urls', index);
							refreshUnchanged();
						}
						else {
							scope.modalData.errorMessage = proximisCoreI18n.trans('m.rbs.seo.ua.url_already_used|ucf');
						}
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
					}
				);
			}
		};

		// Redirects handling.
		scope.updateRedirect = function(index) {
			scope.modalData.errorMessage = null;
			if (scope.isModified('redirects', index)) {
				var localRuleCheck = function(localRule) { return localRule.rule.httpStatus !== 404; };
				checkRelativePath(scope.location.redirects[index].relativePath, localRuleCheck).then(
					function(result) {
						if (!result.data.used) {
							if (result.data.localRule) {
								result.data.localRule.rule.relativePath = null;
							}
							var redirect = scope.location.redirects[index];
							addRedirect(redirect.newRelativePath, redirect.newHttpStatus, null, redirect.query);
							scope.deleteRedirect(index);
							refreshUnchanged();
						}
						else {
							scope.modalData.errorMessage = proximisCoreI18n.trans('m.rbs.seo.ua.url_already_used|ucf');
						}
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
					}
				);
			}
		};

		scope.deleteRedirect = function(index) {
			scope.modalData.unchanged = scope.saveProgress.success = false;
			if (scope.location.redirects[index].id > 0) {
				scope.location.redirects[index].httpStatus = 404;
			}
			else {
				scope.location.redirects.splice(index, 1);
				scope.displayConfig.redirects.splice(index, 1);
			}

			refreshUnchanged();
		};

		scope.makeCurrentUrl = function(index) {
			var redirect = scope.location.redirects[index];

			var length = scope.location.urls.length;
			for (var i = 0; i < length; i++) {
				var url = scope.location.urls[i];
				if (url.query == redirect.query && url.sectionId == redirect.sectionId) {
					var newPath = redirect.relativePath;
					redirect.relativePath = url.relativePath;
					url.relativePath = newPath;
					var id = redirect.id;
					redirect.id = url.id;
					url.id = id;
					break;
				}
			}

			refreshUnchanged();
		};

		scope.addNewRedirect = function() {
			scope.modalData.errorMessage = null;
			var localRuleCheck = function(localRule) { return localRule.rule.httpStatus !== 404; };
			checkRelativePath(scope.newRedirect.relativePath, localRuleCheck).then(
				function(result) {
					if (!result.data.used) {
						if (result.data.localRule) {
							result.data.localRule.rule.httpStatus = scope.newRedirect.httpStatus;
						}
						else {
							addRedirect(scope.newRedirect.relativePath, scope.newRedirect.httpStatus);
						}
						scope.clearNewRedirect();
						refreshUnchanged();
					}
					else {
						scope.modalData.errorMessage = proximisCoreI18n.trans('m.rbs.seo.ua.url_already_used|ucf');
					}
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
				}
			);
		};

		scope.clearNewRedirect = function() {
			scope.modalData.errorMessage = null;
			scope.newRedirect.relativePath = '';
			scope.newRedirect.httpStatus = null;
		};

		initPathRule();

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	app.directive('pxSeoFacetUrlGroupTargetValidator', ['$q', 'proximisRestApi', 'proximisRestApiDefinition',
		function($q, proximisRestApi, proximisRestApiDefinition) {
			return {
				require: 'ngModel',
				link: function(scope, elm, attrs, ngModel) {
					ngModel.$asyncValidators.targetId = function(modelValue) {
						if (ngModel.$isEmpty(modelValue)) {
							return $q.when();
						}

						var def = $q.defer();
						var params = {
							websiteId: scope.modalData.newGroup.websiteId,
							LCID: scope.modalData.newGroup.LCID,
							documentId: modelValue
						};
						proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlGroup.detail', 'GET', params)).then(
							function() {
								def.reject();
							},
							function() {
								def.resolve();
							}
						);

						return def.promise;
					};
				}
			};
		}
	]);

	/**
	 * parentData:
	 *  - documentId
	 *  - urlGroup
	 *  - url
	 */
	app.controller('ProximisSeoFacetUrlAttributesController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', 'proximisCoreArray',
		ProximisSeoFacetUrlAttributesController]);

	function ProximisSeoFacetUrlAttributesController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			document: null,
			typology: null,
			savingData: false,
			loadingTypology: false,
			errorMessage: null,
			urlGroup: angular.copy(parentData.urlGroup),
			url: angular.copy(parentData.url)
		};

		scope.saveProgress = {
			success: false,
			error: false,
			running: false
		};

		function loadData() {
			var params = { facetUrlDecoratorId: parentData.documentId };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlDecorator.detail', 'GET', params)).then(
				function(result) {
					scope.modalData.document = result.data.item;
					scope.modalData.documentLoaded = true;
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
				}
			);
		}

		loadData();

		scope.$watch('modalData.document.typologyId', function(newValue, oldValue) {
			if (newValue == oldValue) {
				return;
			}

			if (newValue) {
				scope.modalData.loadingTypology = true;
				var params = { typologyId: newValue, documentModelName: scope.modalData.document._meta.model};
				proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Ua_Typology.attributes', 'GET', params)).then(
					function(result) {
						scope.typology = result.data.item;
						scope.modalData.loadingTypology = false;
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
						scope.typology = null;
						scope.modalData.loadingTypology = false;
					}
				);
			}
			else if (oldValue) {
				scope.typology = null;
			}
		});

		scope.save = function() {
			var params = {
				facetUrlDecoratorId: parentData.documentId,
				document: scope.modalData.document
			};
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_FacetUrlDecorator.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					scope.cancel();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();