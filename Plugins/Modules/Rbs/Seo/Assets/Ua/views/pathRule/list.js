/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoPathRuleListController', ['$scope', '$routeParams', 'proximisCoreI18n', 'proximisRestApi',
		'proximisRestApiDefinition', 'proximisMessages', 'proximisModalStack', ProximisSeoPathRuleListController]);

	function ProximisSeoPathRuleListController(scope, $routeParams, proximisCoreI18n, proximisRestApi, proximisRestApiDefinition, proximisMessages,
		proximisModalStack) {
		scope.globalData = {
			websites: []
		};

		if ($routeParams.http) {
			scope.filter = { 'name': 'httpStatus', parameters: { httpStatus: $routeParams.http } };
		}

		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Website.list', 'GET', {})).then(
			function(result) {
				scope.globalData.websites = result.data.items;
				if (result.data.length === 1) {
					scope.globalData.website = result.data.items[0];
				}
			},
			function(result) {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'), result.data.status.message, true);
				console.error('Unable to to load websites!', result.data.status.message);
			}
		);

		scope.openAddRuleModal = function() {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/pathRule/add-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoAddPathRuleController',
				resolve: {
					parentData: function() { return scope.globalData; },
					refreshParent: function() { return scope.actionMethods.refresh; }
				}
			});
		};

		// Actions.
		scope.actionMethods = {
			delete: function(rule) {
				if (confirm(proximisCoreI18n.trans('m.rbs.seo.ua.confirm_deleting_rule', ['ucf']))) {
					var params = { pathRuleId: rule._meta.id };
					var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_PathRule.detail', 'DELETE', params));
					scope.actionMethods.refresh(promise);
				}
			},
			openUrl: function(rule) {
				if (rule.common.query) {
					proximisModalStack.open({
						templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/list-modal-content.twig',
						size: 'lg',
						controller: 'ProximisSeoFacetUrlListController',
						resolve: {
							parentData: function() {
								return {
									documentId: rule.common.documentId,
									LCID: rule.common.LCID,
									websiteId: rule.common.websiteId
								};
							},
							refreshParent: function() { return scope.actionMethods.refresh; }
						}
					});
				}
				else {
					proximisModalStack.open({
						templateUrl: 'Rbs/Seo/Ua/views/documentPathRules/detail-modal-content.twig',
						size: 'lg',
						controller: 'ProximisSeoDocumentPathRulesDetailController',
						resolve: {
							parentData: function() {
								return {
									documentId: rule.common.documentId
								};
							},
							refreshParent: function() { return scope.actionMethods.refresh; }
						}
					});
				}
			},
			openDecoratorMetas: function(rule) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/documentMetas/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoDocumentMetasDetailController',
					resolve: {
						parentData: function() {
							return {
								documentId: rule.common.decoratorId ? rule.common.decoratorId : rule.common.documentId,
								LCID: rule.common.LCID
							};
						},
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			website: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.website;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.website.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [
							{
								name: 'website',
								parameters: {
									propertyName: 'website_id', operator: 'eq', value: filter.parameters.website.id
								}
							},
							{
								name: 'lcid',
								parameters: {
									propertyName: 'lcid', operator: 'eq', value: filter.parameters.website.LCID
								}
							}
						]
					};
				}
			},
			relativePath: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.relativePath;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.relativePath.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'relativePath',
							parameters: {
								propertyName: 'relative_path', operator: 'contains', value: filter.parameters.relativePath
							}
						}]
					};
				}
			},
			httpStatus: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.httpStatus;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.httpStatus.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'httpStatus',
							parameters: {
								propertyName: 'http_status', operator: 'eq', value: filter.parameters.httpStatus
							}
						}]
					};
				}
			},
			target: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.targetId;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.target.isValid(filter)) {
						return null;
					}
					return {
						name: 'target',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'documentId',
							parameters: {
								propertyName: 'document_id', operator: 'eq', value: filter.parameters.targetId
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});
	}

	/**
	 * parentData:
	 *  - website: {id, LCID, label} (bidirectional)
	 *  - httpStatus (optional, bidirectional)
	 *  - websites: {id, LCID, label}[]
	 */
	app.controller('ProximisSeoAddPathRuleController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent',
		'proximisModalStack', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoAddPathRuleController]);

	function ProximisSeoAddPathRuleController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			newRule: {
				relativePath: null,
				targetDocId: null,
				targetRuleId: null,
				website: parentData.website,
				httpStatus: parentData.httpStatus || 301
			},
			websites: parentData.websites,
			savingData: false,
			errorMessage: null
		};

		scope.saveNewRuleToDocument = function() {
			scope.modalData.newRule.targetType = 'document';
			scope.modalData.newRule.targetId = scope.modalData.newRule.targetDocId;
			saveNewRule();
		};

		scope.saveNewRuleToRule = function() {
			scope.modalData.newRule.targetType = 'rule';
			scope.modalData.newRule.targetId = scope.modalData.newRule.targetRuleId;
			saveNewRule();
		};

		function saveNewRule() {
			scope.modalData.savingData = true;
			scope.modalData.newRule.websiteId = scope.modalData.newRule.website.id;
			scope.modalData.newRule.LCID = scope.modalData.newRule.website.LCID;
			parentData.website = scope.modalData.newRule.website;
			parentData.httpStatus = scope.modalData.newRule.httpStatus;
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_PathRule.new', 'POST', scope.modalData.newRule)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.savingData = false;
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save new redirection.', result);
				}
			);
		}

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();