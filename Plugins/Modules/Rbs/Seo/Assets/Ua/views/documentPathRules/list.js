/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoDocumentPathRulesListController', ['$scope', 'proximisModalStack', 'proximisDocumentModels',
		ProximisSeoDocumentPathRulesListController]);

	function ProximisSeoDocumentPathRulesListController(scope, proximisModalStack, proximisDocumentModels) {
		scope.globalData = {
			languages: [],
			models: proximisDocumentModels.getByFilter({ publishable: true }),
			restParams: {
				model: null
			}
		};

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/documentPathRules/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoDocumentPathRulesDetailController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return scope.globalData.restParams.model;
				}
			},
			label: {
				isValid: function(filter) {
					return scope.globalData.restParams.model && filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);

			scope.$watch('globalData.restParams.model', function(model) {
				if (model && scope.actionMethods.refresh) {
					scope.actionMethods.refresh();
				}
			});
		});
	}

	/**
	 * parentData:
	 *  - documentId
	 */
	app.controller('ProximisSeoDocumentPathRulesDetailController', ['$uibModalInstance', '$scope', 'parentData', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', ProximisSeoDocumentPathRulesDetailController]);

	function ProximisSeoDocumentPathRulesDetailController(modalInstance, scope, parentData, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			pathRules: null,
			variables: [],
			addGroupIds: []
		};

		var params = { documentId: parentData.documentId };

		var loadData = function() {
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentPathRules.detail', 'GET', params)).then(
				function(result) {
					scope.modalData.pathRules = result.data.item;
					scope.modalData.loadingData = false;
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to load data related to ' + params.id + '".', result);
				}
			);
		};

		loadData();

		scope.getHref = function(location, url) {
			return location.baseUrl + (url ? url.relativePath : '');
		};

		scope.openLocation = function(locationGroupIndex, locationGroup, locationIndex, location) {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/documentPathRules/location-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoDocumentPathRulesLocationController',
				resolve: {
					parentData: function() {
						return {
							documentId: parentData.documentId,
							pathRules: scope.modalData.pathRules,
							locationGroupIndex: locationGroupIndex, locationGroup: locationGroup,
							locationIndex: locationIndex, location: location
						};
					},
					refreshParent: function() { return loadData; }
				}
			});
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - pathRules
	 *  - locationGroupIndex
	 *  - locationIndex
	 */
	app.controller('ProximisSeoDocumentPathRulesLocationController', ['$q', '$uibModalInstance', '$scope', 'parentData', 'refreshParent',
		'proximisModalStack', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', 'proximisCoreArray',
		ProximisSeoDocumentPathRulesLocationController]);

	function ProximisSeoDocumentPathRulesLocationController($q, modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n, proximisCoreArray) {
		scope.modalData = {
			loadingData: false,
			savingData: false,
			errorMessage: null,
			pathRules: parentData.pathRules,
			locationGroup: parentData.locationGroup,
			originalLocation: parentData.location,
			originalPaths: [],
			unchanged: true,
			newRuleLastId: -1
		};
		scope.saveProgress = {
			success: false,
			error: false,
			running: false
		};
		scope.newRedirect = {};
		scope.httpStatuses = [301, 302];
		var types = ['urls', 'redirects', 'facetUrls'];

		function initPathRule() {
			scope.location = angular.copy(scope.modalData.originalLocation);
			scope.modalData.unchanged = true;

			scope.displayConfig = {
				'urls': [],
				'facetUrls': [],
				'redirects': []
			};

			for (var i = 0; i < types.length; i++) {
				for (var j = 0; j < scope.location[types[i]].length; j++) {
					scope.displayConfig[types[i]].push({ 'edit': false });
					scope.modalData.originalPaths.push(scope.location[types[i]][j].relativePath);
				}
			}

			scope.clearNewRedirect();

			if (scope.saveProgress.running) {
				scope.saveProgress.running = scope.saveProgress.error = false;
				scope.saveProgress.success = true;
			}
		}

		function removeRedirectByRelativePath(relativePath) {
			var length = scope.location.redirects.length;
			for (var i = 0; i < length; i++) {
				if (scope.location.redirects[i].relativePath == relativePath) {
					scope.deleteRedirect(i);
					return;
				}
			}
		}

		function addRedirect(relativePath, httpStatus, id, query) {
			scope.location.redirects.push({
				id: id ? id : scope.modalData.newRuleLastId--,
				relativePath: relativePath,
				httpStatus: httpStatus ? httpStatus : 301,
				query: query,
				userEdited: true
			});
			scope.displayConfig.redirects.push({ 'edit': false });
		}

		function findRuleByPath(relativePath) {
			for (var i = 0; i < types.length; i++) {
				for (var j = 0; j < scope.location[types[i]].length; j++) {
					if (scope.location[types[i]][j].relativePath === relativePath) {
						return { type: types[i], index: i, rule: scope.location[types[i]][j] };
					}
				}
			}
		}

		function checkRelativePath(relativePath, localRuleCheck) {
			var localRule = findRuleByPath(scope.newRedirect.relativePath);
			if (localRule) {
				return $q(function(resolve) { resolve({ used: localRuleCheck ? localRuleCheck(localRule) : true, localRule: localRule }); });
			}

			if (proximisCoreArray.inArray(relativePath, scope.modalData.originalPaths)) {
				return $q(function(resolve) { resolve({ used: false }); });
			}

			var params = {
				websiteId: scope.modalData.locationGroup.websiteId,
				LCID: scope.modalData.locationGroup.LCID,
				relativePath: relativePath
			};
			return proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_PathRule.check', 'GET', params));
		}

		function refreshUnchanged() {
			for (var i = 0; i < types.length; i++) {
				if (scope.location[types[i]].length !== scope.modalData.originalLocation[types[i]].length) {
					scope.modalData.unchanged = false;
					return;
				}

				for (var j = 0; j < scope.location[types[i]].length; j++) {
					var rule = scope.location[types[i]][j];
					var oRule = scope.modalData.originalLocation[types[i]][j];
					if (rule.id !== oRule.id || rule.relativePath !== oRule.relativePath || rule.httpStatus !== oRule.httpStatus) {
						scope.modalData.unchanged = false;
						return;
					}
				}
			}
			scope.modalData.unchanged = true;
		}

		scope.getHref = function(location, url) {
			return location.baseUrl + (url ? url.relativePath : '');
		};

		scope.save = function() {
			scope.modalData.savingData = true;
			var pathRules = angular.copy(scope.modalData.pathRules);
			pathRules['locationGroups'][parentData.locationGroupIndex].locations[parentData.locationIndex] = scope.location;
			var params = {
				documentId: scope.modalData.pathRules._meta.id,
				documentPathRules: pathRules
			};
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentPathRules.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					scope.cancel();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.startEdit = function(type, index) {
			scope.location[type][index].newRelativePath = scope.location[type][index].relativePath;
			scope.location[type][index].newHttpStatus = scope.location[type][index].httpStatus;
			scope.displayConfig[type][index].edit = true;
		};

		scope.endEdit = function(type, index) {
			scope.modalData.errorMessage = null;
			delete scope.location[type][index].newRelativePath;
			delete scope.location[type][index].newHttpStatus;
			scope.displayConfig[type][index].edit = false;
		};

		scope.isModified = function(type, index) {
			var rule = scope.location[type][index];
			return (rule.relativePath != rule.newRelativePath || rule.httpStatus != rule.newHttpStatus);
		};

		// URLs handling.
		scope.updateUrl = function(index) {
			scope.modalData.errorMessage = null;
			if (scope.isModified('urls', index)) {
				var localRuleCheck = function(localRule) { return localRule.type === 'redirects'; };
				checkRelativePath(scope.location.urls[index].newRelativePath, localRuleCheck).then(
					function(result) {
						if (!result.data.used) {
							if (result.data.localRule) {
								result.data.localRule.rule.relativePath = null;
								result.data.localRule.rule.httpStatus = 404;
							}
							var url = scope.location.urls[index];
							addRedirect(url.relativePath, 301, url.id);
							url.id = scope.modalData.newRuleLastId--;
							url.relativePath = url.newRelativePath;
							scope.endEdit('urls', index);
							refreshUnchanged();
						}
						else {
							scope.modalData.errorMessage = proximisCoreI18n.trans('m.rbs.seo.ua.url_already_used|ucf');
						}
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
					}
				);
			}
		};

		scope.restoreDefaultUrl = function(index) {
			var location = scope.location;
			var defaultPath = location['defaultRelativePath'];

			// Remove potential redirect with the same relative path.
			removeRedirectByRelativePath(defaultPath);

			// Add the redirect to the old URL.
			addRedirect(location.urls[index].relativePath, 301, location.urls[index].id);

			location.urls[index].id = scope.modalData.newRuleLastId--;
			location.urls[index].relativePath = defaultPath;

			refreshUnchanged();
		};

		// Redirects handling.
		scope.updateRedirect = function(index) {
			scope.modalData.errorMessage = null;
			if (scope.isModified('redirects', index)) {
				var localRuleCheck = function(localRule) { return localRule.rule.httpStatus !== 404; };
				checkRelativePath(scope.location.redirects[index].relativePath, localRuleCheck).then(
					function(result) {
						if (!result.data.used) {
							if (result.data.localRule) {
								result.data.localRule.rule.relativePath = null;
							}
							var redirect = scope.location.redirects[index];
							addRedirect(redirect.newRelativePath, redirect.newHttpStatus, null, redirect.query);
							scope.deleteRedirect(index);
							refreshUnchanged();
						}
						else {
							scope.modalData.errorMessage = proximisCoreI18n.trans('m.rbs.seo.ua.url_already_used|ucf');
						}
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
					}
				);
			}
		};

		scope.deleteRedirect = function(index) {
			scope.modalData.unchanged = scope.saveProgress.success = false;
			if (scope.location.redirects[index].id > 0) {
				scope.location.redirects[index].httpStatus = 404;
			}
			else {
				scope.location.redirects.splice(index, 1);
				scope.displayConfig.redirects.splice(index, 1);
			}

			refreshUnchanged();
		};

		scope.makeCurrentUrl = function(index) {
			var redirect = scope.location.redirects[index];
			var length = scope.location.urls.length;
			for (var i = 0; i < length; i++) {
				var url = scope.location.urls[i];
				if (url.query == redirect.query && url.sectionId == redirect.sectionId) {
					var newPath = redirect.relativePath;
					redirect.relativePath = url.relativePath;
					url.relativePath = newPath;
					var id = redirect.id;
					redirect.id = url.id;
					url.id = id;
					break;
				}
			}

			refreshUnchanged();
		};

		scope.addNewRedirect = function() {
			scope.modalData.errorMessage = null;
			var localRuleCheck = function(localRule) { return localRule.rule.httpStatus !== 404; };
			checkRelativePath(scope.newRedirect.relativePath, localRuleCheck).then(
				function(result) {
					if (!result.data.used) {
						if (result.data.localRule) {
							result.data.localRule.rule.httpStatus = scope.newRedirect.httpStatus;
						}
						else {
							addRedirect(scope.newRedirect.relativePath, scope.newRedirect.httpStatus);
						}
						scope.clearNewRedirect();
						refreshUnchanged();
					}
					else {
						scope.modalData.errorMessage = proximisCoreI18n.trans('m.rbs.seo.ua.url_already_used|ucf');
					}
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
				}
			);
		};

		scope.clearNewRedirect = function() {
			scope.modalData.errorMessage = null;
			scope.newRedirect.relativePath = '';
			scope.newRedirect.httpStatus = null;
		};

		initPathRule();

		scope.openFacet = function() {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/facetUrlGroup/list-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoFacetUrlListController',
				resolve: {
					parentData: function() {
						return {
							documentId: parentData.documentId,
							websiteId: scope.modalData.locationGroup.websiteId,
							LCID: scope.modalData.locationGroup.LCID
						};
					},
					refreshParent: function() { return refreshParent; }
				}
			});
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();