/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	/**
	 * @ngdoc directive
	 * @id proximisSeo:pxSeoUrlConfigurator
	 * @name pxSeoUrlConfigurator
	 * @restrict A
	 *
	 * @description
	 * Configure URLs for facets.
	 */
	app.directive('pxSeoUrlConfigurator', ['proximisRestApi', 'proximisRestApiDefinition', function(proximisRestApi, proximisRestApiDefinition) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Seo/Ua/directives/url-configurator.twig',
			controller: 'ProximisItemSelectorController',

			link: function(scope) {
				var originalData = angular.copy(scope.data);
				var dataExtend = {
					facets: [],
					indexCategories: {},
					batches: [],
					newBatch: [],
					isNewBatchValid: false,
					facetLoaded: false
				};
				angular.extend(scope.data, dataExtend, originalData);

				if (!scope.data.facetLoaded) {
					proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Facet.list', 'GET')).then(
						function(result) {
							for (var i = 0; i < result.data.items.length; i++) {
								var facet = result.data.items[i];
								var facetData = {
									id: facet._meta.id,
									index: scope.data.facets.length,
									label: facet.label,
									indexCategory: facet.indexCategory,
									indexCategoryLabel: facet.indexCategoryLabel,
									values: [],
									valuesLoaded: false
								};

								scope.data.facets.push(facetData);

								if (!scope.data.indexCategories[facet.indexCategory]) {
									scope.data.indexCategories[facet.indexCategory] = {
										label: facet.indexCategoryLabel,
										facets: [facetData]
									}
								}
								else {
									scope.data.indexCategories[facet.indexCategory].facets.push(facetData);
								}
							}

							angular.forEach(scope.data.batches, function(batch) {
								batch.possibleURLCount = 1;
								for (var j = 0; j < batch.facetIds.length; j++) {
									var facet = findFacet(batch.facetIds[j]);
									batch.facets.push(facet);
									loadFacetValues(facet, batch);
								}

								batch.key = '';
								for (j = 0; j < scope.data.facets.length; j++) {
									batch.key += containsFacet(batch.facetIds, scope.data.facets[j].id) ? '1' : '0';
								}

								scope.updateCurrentURLCount(batch);
							});

							scope.data.facetLoaded = true;
						},
						function(result) {
							console.error('Unable to load facets data.', result);
						}
					);
				}

				scope.addBatch = function() {
					// Collapse other batches.
					for (var i = 0; i < scope.data.batches.length; i++) {
						scope.data.batches[i].collapsed = true;
					}

					var batch = {
						key: generateBatchKey(scope.data.newBatch),
						facets: [],
						urlDefinitions: {},
						path: [],
						urlPattern: '{root}/',
						currentURLCount: 0,
						possibleURLCount: 1,
						collapsed: false
					};
					angular.forEach(scope.data.newBatch, function(value, index) {
						if (value) {
							var facet = scope.data.facets[index];
							batch.facets.push(facet);
							loadFacetValues(facet, batch);
							batch.urlPattern += '{' + batch.facets.length + '}/';
						}
					});
					scope.data.batches.push(batch);
					resetNewBatch();
				};

				scope.removeBatch = function(index) {
					scope.data.batches.splice(index, 1);
				};

				scope.$watch('data.facets', resetNewBatch);

				scope.$watch('data.newBatch', validateNewBatch, true);

				scope.updateCurrentURLCount = function(batch) {
					batch.currentURLCount = doCountUrlDefinitionsValue(batch, batch.urlDefinitions, 0);
				};

				function doCountUrlDefinitionsValue(batch, urlDefinitions, level) {
					var count = 0;
					if (level < batch.facets.length - 1) {
						angular.forEach(urlDefinitions, function(item) {
							count += doCountUrlDefinitionsValue(batch, item, level + 1);
						});
					}
					else {
						angular.forEach(urlDefinitions, function(value) {
							if (value) {
								count++;
							}
						});
					}
					return count;
				}

				function resetNewBatch() {
					scope.data.newBatch = [];
					for (var i = 0; i < scope.data.facets.length; i++) {
						scope.data.newBatch[i] = false;
					}
				}

				function validateNewBatch() {
					scope.data.isNewBatchValid = false;
					for (var i = 0; i < scope.data.newBatch.length; i++) {
						if (scope.data.newBatch[i]) {
							scope.data.isNewBatchValid = true;
							break;
						}
					}
					var batchKey = generateBatchKey(scope.data.newBatch);
					for (i = 0; i < scope.data.batches.length; i++) {
						if (scope.data.batches[i].key === batchKey) {
							scope.data.isNewBatchValid = false;
							break;
						}
					}
				}

				function generateBatchKey(newBatch) {
					var key = '';
					for (var i = 0; i < scope.data.facets.length; i++) {
						key += newBatch[i] ? '1' : '0';
					}
					return key;
				}

				function findFacet(facetId) {
					for (var i = 0; i < scope.data.facets.length; i++) {
						if (scope.data.facets[i].id === facetId) {
							return scope.data.facets[i];
						}
					}
				}

				function containsFacet(facetIds, facetId) {
					for (var i = 0; i < facetIds.length; i++) {
						if (facetIds[i] === facetId) {
							return true;
						}
					}
					return false;
				}

				function loadFacetValues(facet, batch) {
					if (!facet.valuesLoaded) {
						facet.valuesLoaded = true;
						proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_Facet.values', 'GET', { facetId: facet.id })).then(
							function(result) {
								facet.values = [];
								angular.forEach(result.data.items, function(label, code) {
									facet.values.push({
										label: label,
										code: code,
										key: 'v-' + code
									});
								});
								batch.possibleURLCount *= facet.values.length;
							},
							function(result) {
								console.error('Unable to load values for facet ' + facet.id + '.', result);
							}
						);
					}
					else {
						batch.possibleURLCount *= facet.values.length;
					}
				}
			}
		};
	}]);

	app.directive('pxSeoUrlConfiguratorBatch', ['$timeout', 'proximisCoreArray', function($timeout, proximisCoreArray) {
		return {
			restrict: 'A',
			scope: true,
			controller: 'ProximisItemSelectorController',

			link: function(scope, elm) {
				var batch = scope.batch;

				scope.navigate = function(facetIndex, valueKey) {
					if (batch.path.length < facetIndex) {
						return;
					}

					// This will remove all the values after 'facetIndex' in 'path' Array.
					batch.path.length = facetIndex;
					batch.path[facetIndex] = valueKey;
					updateIndicators();
				};

				scope.selectValue = function(facetIndex, valueKey, $event) {
					$event.stopPropagation();
					doSelectFacetValue(resolveValueKeys(facetIndex, valueKey), batch.urlDefinitions, 0);
					scope.navigate(facetIndex, valueKey);
				};

				scope.deselectValue = function(facetIndex, valueKey, $event) {
					$event.stopPropagation();
					doDeselectFacetValue(resolveValueKeys(facetIndex, valueKey), batch.urlDefinitions, 0);
					if (batch.path[facetIndex] && (batch.path[facetIndex] == valueKey)) {
						batch.path.length = facetIndex;
						updateIndicators();
					}
				};

				scope.selectAllValues = function() {
					doSelectAllValues(batch.urlDefinitions, 0);
				};

				scope.deselectAllValues = function() {
					batch.path = [];
					batch.urlDefinitions = {};
					updateIndicators();
				};

				scope.moveColumnLeft = function(facetIndex) {
					if (facetIndex < 1) {
						return;
					}
					moveColumn(facetIndex, -1);
				};

				scope.moveColumnRight = function(facetIndex) {
					if (facetIndex > batch.facets.length - 2) {
						return;
					}
					moveColumn(facetIndex, 1);
				};

				function moveColumn(facetIndex, offset) {
					var oldDefinitions = angular.copy(batch.urlDefinitions);
					var oldFacets = batch.facets;

					batch.path = [];
					batch.urlDefinitions = {};
					batch.facets = [];
					updateIndicators();

					$timeout(function() {
						proximisCoreArray.move(oldFacets, facetIndex, facetIndex + offset);
						batch.facets = oldFacets;
						convertDefinitions(oldDefinitions, [], facetIndex, facetIndex + offset);
					});
				}

				function convertDefinitions(oldDefinitions, parentKeys, oldIndex, newIndex) {
					angular.forEach(oldDefinitions, function(value, key) {
						var keys = angular.copy(parentKeys);
						keys.push(key);
						if (value === true) {
							proximisCoreArray.move(keys, oldIndex, newIndex);
							doSelectFacetValue(keys, batch.urlDefinitions, 0);
						}
						else {
							convertDefinitions(value, keys, oldIndex, newIndex);
						}
					});
				}

				scope.$watch('batch.urlDefinitions', function() { scope.updateCurrentURLCount(batch); }, true);

				function resolveValueKeys(facetIndex, valueKey) {
					var valueKeys = [];
					angular.forEach(batch.path, function(item, itemIndex) {
						if (itemIndex < facetIndex) {
							valueKeys.push(item);
						}
					});
					valueKeys.push(valueKey);
					return valueKeys;
				}

				function doSelectAllValues(result, index) {
					var facet = batch.facets[index];
					angular.forEach(facet.values, function(value) {
						if (index == scope.batch.facets.length - 1) {
							result[value.key] = true;
						}
						else {
							result[value.key] = {};
							doSelectAllValues(result[value.key], index + 1);
						}
					});
				}

				function doSelectFacetValue(valueKeys, result, index) {
					if (!valueKeys[index]) {
						return;
					}
					if (index < scope.batch.facets.length - 1) {
						if (!result[valueKeys[index]]) {
							result[valueKeys[index]] = {};
						}
						doSelectFacetValue(valueKeys, result[valueKeys[index]], index + 1);
					}
					else {
						result[valueKeys[index]] = true;
					}
				}

				function doDeselectFacetValue(valueKeys, result, index) {
					if (index < (valueKeys.length - 1)) {
						doDeselectFacetValue(valueKeys, result[valueKeys[index]], index + 1);
					}
					else {
						delete result[valueKeys[index]];
					}
				}

				function resolveValueIndex(facetIndex, valueKey) {
					var facet = batch.facets[facetIndex];
					for (var i = 0; i < facet.values.length; i++) {
						if (facet.values[i].key == valueKey) {
							return i;
						}
					}
					return -1;
				}

				function updateIndicators() {
					var i, lv, rv, lCol, rCol, lTop, rTop, top, height;

					elm.find('.configurator-column .configurator-column-body .indicator').hide();
					for (i = 1; i < batch.path.length; i++) {
						lCol = elm.find('.configurator-column:nth-child(' + i + ') .configurator-column-body');
						rCol = elm.find('.configurator-column:nth-child(' + (i + 1) + ') .configurator-column-body');

						lv = lCol.find('.configurator-column-cell:nth-child(' + (resolveValueIndex(i - 1, batch.path[i - 1]) + 1) + ')');
						rv = rCol.find('.configurator-column-cell:nth-child(' + (resolveValueIndex(i, batch.path[i]) + 1) + ')');

						lTop = lv.position().top;
						rTop = rv.position().top;

						if (lTop < rTop) {
							top = lTop;
							height = rTop - lTop + rv.outerHeight();
						}
						else {
							top = rTop;
							height = lTop - rTop + lv.outerHeight();
						}

						elm.find('.configurator-column:nth-child(' + (i + 1) + ') .configurator-column-body .indicator')
							.css({ height: height + 'px', top: top + 'px' })
							.show();
					}
				}
			}
		};
	}]);

	app.directive('pxSeoUrlConfiguratorItem', function() {
		return {
			restrict: 'A',
			scope: false,
			link: function(scope) {
				scope.$watch('batch.path', refreshUsed, true);
				scope.$watch('batch.urlDefinitions', refreshUsed, true);

				function refreshUsed() {
					if (scope.batch.path.length < scope.facetIndex) {
						scope.used = 'U';
						return;
					}
					var urlDefinitions = scope.batch.urlDefinitions;
					for (var i = 0; i < scope.facetIndex; i++) {
						if (!urlDefinitions[scope.batch.path[i]]) {
							scope.used = 'N';
							return;
						}
						urlDefinitions = urlDefinitions[scope.batch.path[i]];
					}
					scope.used = urlDefinitions[scope.value.key] ? 'Y' : 'N';
				}
			}
		};
	});
})();