/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	/**
	 * @ngdoc directive
	 * @id proximisSeo:pxSeoPathRuleSelector
	 * @name pxSeoPathRuleSelector
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {string=} selectorTitle The placeholder of the selector.
	 * @param {boolean=} readonly Set to true to make the selector readonly.
	 *
	 * @description
	 * Displays a Document selector to let the user select a single document.
	 */
	app.directive('pxSeoPathRuleSelector', ['proximisRestApi', 'proximisRestApiDefinition', function(proximisRestApi, proximisRestApiDefinition) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Seo/Ua/directives/path-rule-selector.twig',
			require: 'ngModel',
			scope: true,
			controller: 'ProximisItemSelectorController',

			link: function(scope, element, attrs, ngModel) {
				pathRuleSelectorFunction(scope, element, attrs, ngModel, false, proximisRestApi, proximisRestApiDefinition);
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id proximisSeo:pxSeoPathRulesSelector
	 * @name pxSeoPathRulesSelector
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {string=} selectorTitle The placeholder of the selector.
	 * @param {boolean=} readonly Set to true to selector readonly.
	 *
	 * @description
	 * Displays a Document selector to let the user select multiple documents.
	 */
	app.directive('pxSeoPathRulesSelector', ['proximisRestApi', 'proximisRestApiDefinition', function(proximisRestApi, proximisRestApiDefinition) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Seo/Ua/directives/path-rule-selector.twig',
			require: 'ngModel',
			scope: true,
			controller: 'ProximisItemSelectorController',

			link: function(scope, element, attrs, ngModel) {
				pathRuleSelectorFunction(scope, element, attrs, ngModel, true, proximisRestApi, proximisRestApiDefinition);
			}
		};
	}]);

	function pathRuleSelectorFunction(scope, element, attrs, ngModel, multiple, proximisRestApi, proximisRestApiDefinition) {
		scope.multiple = multiple;
		scope.readonly = attrs.readonly ? true : false;
		scope.disableReordering = !multiple || scope.readonly;
		scope.selectorTitle = attrs.selectorTitle;
		scope.selectorElement = element;

		// viewValue => modelValue
		ngModel.$parsers.unshift(function(viewValue) {
			if (viewValue === undefined) {
				return viewValue;
			}

			var modelValue;
			if (multiple) {
				modelValue = [];
				angular.forEach(viewValue, function(item) {
					if (angular.isObject(item) && angular.isObject(item._meta) && item._meta.id) {
						modelValue.push(item._meta.id);
					}
					else {
						console.error('[pxSeoPathRulesSelector] Invalid rule', item);
					}
				});
			}
			else {
				modelValue = 0;
				if (angular.isObject(viewValue)) {
					if (angular.isObject(viewValue._meta) && viewValue._meta.id) {
						modelValue = viewValue._meta.id;
					}
					else {
						console.error('[pxSeoPathRuleSelector] Invalid rule', viewValue);
					}
				}
			}
			return modelValue;
		});

		// modelValue => viewValue
		ngModel.$formatters.unshift(function(modelValue) {
			if (modelValue === undefined) {
				return modelValue;
			}
			var viewValue = multiple ? [] : null;
			var oldList = scope.items.list;
			var itemList = [];

			if (multiple) {
				if (angular.isArray(modelValue)) {
					var ids = [], item;
					angular.forEach(modelValue, function(id) {
						if (angular.isNumber(id) && id > 0) {
							item = scope.tools.getItemById(oldList, id);
							if (item) {
								itemList.push(item);
							}
							else {
								ids.push(id);
							}
						}
						else {
							console.error('[pxSeoPathRulesSelector] Invalid number value:', id);
						}
					});
					if (ids.length) {
						var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_PathRule.load', 'GET', { ids: ids }));
						promise.then(
							function(result) {
								angular.forEach(result.data, function(item) {
									itemList.push(item);
								});
							},
							function(error) {
								console.error('[pxSeoPathRulesSelector]', error);
							}
						);
					}
					viewValue = scope.tools.arrayCopy(itemList);
				}
			}
			else {
				if (angular.isNumber(modelValue) && modelValue > 0) {
					promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_PathRule.load', 'GET', { ids: [modelValue] }));
					promise.then(
						function(result) {
							viewValue = result.data[0];
							itemList.push(viewValue);
						},
						function(error) {
							console.error('[pxSeoPathRuleSelector]', error);
						}
					);
				}
			}

			scope.items.list = itemList;
			return viewValue;
		});

		// Watch from changes coming from the list which is bound to `scope.items.list`.
		scope.$watchCollection('items.list', function() {
			if (scope.items.list.length === 0 && ngModel.$viewValue === undefined) {
				return;
			}
			if (multiple) {
				ngModel.$setViewValue(scope.tools.arrayCopy(scope.items.list));
			}
			else {
				ngModel.$setViewValue(scope.items.list.length ? scope.items.list[0] : null);
			}
		});

		scope.canAutoComplete = function() {
			return scope.autoComplete.value.trim().length > 0;
		};

		scope.getAutoCompleteDefinition = function() {
			var value = angular.lowercase(scope.autoComplete.value.trim());
			var params = { search: value, offset: scope.autoComplete.offset, limit: scope.autoComplete.limit };
			return proximisRestApiDefinition.getData('Rbs_Seo_PathRule.search', 'GET', params);
		};
	}
})();