/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	/**
	 * @ngdoc directive
	 * @id proximisSeo:pxSeoExtendedTextField
	 * @name pxSeoExtendedTextField
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {Array=} variables Each item in the array should be an object with the following properties:
	 *  <ul>
	 *      <li>`label` - string - a human readable label</li>
	 *      <li>`code` - string, optional - the variable's code for dynamic usage</li>
	 *      <li>`value` - string, optional - the variable's value for static usage</li>
	 *  </ul>
	 *
	 * @description
	 * A text field with a menu to insert predefined suggestions.
	 */
	app.directive('pxSeoExtendedTextField', ['proximisRestApi', 'proximisRestApiDefinition', function(proximisRestApi, proximisRestApiDefinition) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Seo/Ua/directives/extended-text-field.twig',
			require: 'ngModel',
			scope: {
				variables: '='
			},

			link: function(scope, element, attrs, ngModel) {
				ngModel.$render = function() {
					scope.value = ngModel.$viewValue;
				};

				scope.$watch('value', function(newValue, oldValue) {
					if (newValue !== oldValue) {
						ngModel.$setViewValue(newValue);
					}
				});

				scope.appendSuggestion = function(suggestion) {
					function getValue() {
						return (!angular.isString(scope.value) || !scope.value) ? '' : (scope.value + ' ');
					}

					if (suggestion.value) {
						scope.value = getValue() + suggestion.value;
					}
					else if (suggestion.code) {
						scope.value = getValue() + '{' + suggestion.code + '}';
					}
				}
			}
		};
	}]);
})();