<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\FacetUrl;

/**
 * @name \Rbs\Seo\FacetUrl\Value
 */
class Value
{
	/**
	 * @var int
	 */
	protected $facetId = 0;

	/**
	 * @var string|null
	 */
	protected $value;

	/**
	 * @param array|\Rbs\Seo\FacetUrl\Value|null $data
	 */
	public function __construct($data = null)
	{
		if (is_array($data))
		{
			$this->fromArray($data);
		}
		else if ($data instanceof \Rbs\Seo\FacetUrl\Value)
		{
			$this->fromArray($data->toArray());
		}
	}

	/**
	 * @return int
	 */
	public function getFacetId() : int
	{
		return $this->facetId;
	}

	/**
	 * @param int $facetId
	 * @return $this
	 */
	public function setFacetId(int $facetId)
	{
		$this->facetId = $facetId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param string|null $value
	 * @return $this
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}

	/**
	 * @param array $array
	 * @return $this
	 */
	protected function fromArray(array $array)
	{
		foreach ($array as $name => $value)
		{
			switch ($name)
			{
				case 'value':
					$this->setValue($value);
					break;
				case 'facetId':
					$this->setFacetId($value);
					break;
			}
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray() : array
	{
		return [
			'value' => $this->value,
			'facetId' => $this->facetId
		];
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>, which is a value of any type other than a resource.
	 */
	public function jsonSerialize()
	{
		return $this->toArray();
	}
}