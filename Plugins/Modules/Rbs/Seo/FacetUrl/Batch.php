<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\FacetUrl;

/**
 * @name \Rbs\Seo\FacetUrl\Batch
 */
class Batch implements \JsonSerializable
{
	/**
	 * @var int[]
	 */
	protected $facetIds = [];

	/**
	 * @var string
	 */
	protected $urlPattern;

	/**
	 * @var string|null
	 */
	protected $key;

	/**
	 * @var \Rbs\Seo\FacetUrl\Definition[]
	 */
	protected $urlDefinitions = [];

	/**
	 * @param \Rbs\Seo\FacetUrl\Batch|array $batch
	 */
	public function __construct($batch)
	{
		if (is_array($batch))
		{
			$this->fromArray($batch);
		}
		elseif ($batch instanceof \Rbs\Seo\FacetUrl\Batch)
		{
			$this->fromArray($batch->toArray());
		}
	}

	/**
	 * @return int[]
	 */
	public function getFacetIds() : array
	{
		return $this->facetIds;
	}

	/**
	 * @param int[] $facetIds
	 * @return $this
	 */
	public function setFacetIds(array $facetIds)
	{
		$this->facetIds = [];
		foreach ($facetIds as $facetId)
		{
			if (is_numeric($facetId))
			{
				$this->facetIds[] = (int)$facetId;
			}
		}
		$this->key = null;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		if ($this->key === null)
		{
			$key = [];
			foreach ($this->facetIds as $facetId)
			{
				$key[] = $facetId;
			}
			$this->key = implode('-', $key);
		}
		return $this->key;
	}

	/**
	 * @return string
	 */
	public function getUrlPattern()
	{
		return $this->urlPattern;
	}

	/**
	 * @param string $urlPattern
	 * @return $this
	 */
	public function setUrlPattern(string $urlPattern)
	{
		$this->urlPattern = $urlPattern;
		return $this;
	}

	/**
	 * @return \Rbs\Seo\FacetUrl\Definition[]
	 */
	public function getUrlDefinitions() : array
	{
		return $this->urlDefinitions;
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Definition[] $urlDefinitions
	 * @return $this
	 */
	public function setUrlDefinitions(array $urlDefinitions)
	{
		$this->urlDefinitions = [];
		foreach ($urlDefinitions as $urlDefinition)
		{
			$this->urlDefinitions[] = new \Rbs\Seo\FacetUrl\Definition($urlDefinition);
		}
		return $this;
	}

	/**
	 * @param array $array
	 * @return $this
	 */
	protected function fromArray(array $array)
	{
		foreach ($array as $name => $value)
		{
			switch ($name)
			{
				case 'urlPattern':
					$this->urlPattern = (string)$value;
					break;
				case 'facetIds':
					if (is_array($value))
					{
						$this->setFacetIds($value);
					}
					break;
				case 'urlDefinitions':
					if (is_array($value))
					{
						$this->setUrlDefinitions($value);
					}
					break;
			}
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray() : array
	{
		$array = [
			'urlPattern' => $this->urlPattern,
			'facetIds' => $this->facetIds,
			'urlDefinitions' => []
		];
		foreach($this->urlDefinitions as $urlDefinition)
		{
			$array['urlDefinitions'][] = $urlDefinition->toArray();
		}
		return $array;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>, which is a value of any type other than a resource.
	 */
	public function jsonSerialize()
	{
		return $this->toArray();
	}
}