<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\FacetUrl;

/**
 * @name \Rbs\Seo\FacetUrl\UrlDefinition
 */
class Definition implements \JsonSerializable
{
	/**
	 * @var \Rbs\Seo\FacetUrl\Value[]
	 */
	protected $values = [];

	/**
	 * @var int
	 */
	protected $decoratorId = 0;

	/**
	 * @var int
	 */
	protected $pathRuleId = 0;

	/**
	 * @var string|null
	 */
	protected $key;

	/**
	 * @var boolean
	 */
	protected $conflict = false;

	/**
	 * @param array|\Rbs\Seo\FacetUrl\Definition|null $data
	 */
	public function __construct($data = null)
	{
		if (is_array($data))
		{
			$this->fromArray($data);
		}
		else if ($data instanceof \Rbs\Seo\FacetUrl\Definition)
		{
			$this->fromArray($data->toArray());
		}
	}

	/**
	 * @return \Rbs\Seo\FacetUrl\Value[]
	 */
	public function getValues() : array
	{
		return $this->values;
	}

	/**
	 * @param array $values
	 * @return $this
	 */
	public function setValues(array $values)
	{
		$this->values = [];
		foreach ($values as $value)
		{
			if ($value instanceof \Rbs\Seo\FacetUrl\Value || (is_array($value) && isset($value['facetId'], $value['value'])))
			{
				$this->values[] = new \Rbs\Seo\FacetUrl\Value($value);
			}
		}
		$this->key = null;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		if ($this->key === null)
		{
			$key = [];
			foreach ($this->values as $value)
			{
				$key[] = $value->getValue();
			}
			$this->key = implode('-', $key);
		}
		return $this->key;
	}

	/**
	 * @return int
	 */
	public function getDecoratorId() : int
	{
		return $this->decoratorId;
	}

	/**
	 * @param int $decoratorId
	 * @return $this
	 */
	public function setDecoratorId(int $decoratorId)
	{
		$this->decoratorId = $decoratorId;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPathRuleId() : int
	{
		return $this->pathRuleId;
	}

	/**
	 * @param int $pathRuleId
	 * @return $this
	 */
	public function setPathRuleId(int $pathRuleId)
	{
		$this->pathRuleId = $pathRuleId;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getConflict() : bool
	{
		return $this->conflict;
	}

	/**
	 * @param boolean $conflict
	 * @return $this
	 */
	public function setConflict(bool $conflict)
	{
		$this->conflict = $conflict;
		return $this;
	}

	/**
	 * @param array $values
	 * @return bool
	 */
	public function matches($values)
	{
		return $this->getKey() === implode('-', $values);
	}

	/**
	 * @param array $array
	 * @return $this
	 */
	protected function fromArray(array $array)
	{
		foreach ($array as $name => $value)
		{
			switch ($name)
			{
				case 'values':
					if (is_array($value))
					{
						$this->setValues($value);
					}
					break;
				case 'decoratorId':
					$this->decoratorId = (int)$value;
					break;
				case 'pathRuleId':
					$this->pathRuleId = (int)$value;
					break;
				case 'conflict':
					$this->conflict = (bool)$value;
					break;
			}
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray() : array
	{
		$array = [
			'values' => [],
			'decoratorId' => $this->decoratorId,
			'pathRuleId' => $this->pathRuleId,
			'conflict' => $this->conflict
		];
		foreach ($this->values as $value)
		{
			$array['values'][] = $value->toArray();
		}
		return $array;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>, which is a value of any type other than a resource.
	 */
	public function jsonSerialize()
	{
		return $this->toArray();
	}
}