<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\FacetUrl;

/**
 * @name \Rbs\Seo\FacetUrl\UrlBuilder
 * @deprecated since 1.8.0 use \Rbs\Seo\FacetUrl\PageDataResolver
 */
class UrlBuilder
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $pathRuleManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @deprecated since 1.8.0 use \Rbs\Seo\FacetUrl\PageDataResolver
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, \Change\Http\Web\PathRuleManager $pathRuleManager)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_WARNING);
		$this->documentManager = $documentManager;
		$this->pathRuleManager = $pathRuleManager;
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website
	 * @param int|null $pageId
	 * @param int|null $sectionId
	 * @param string $LCID
	 * @param array $facetFilters
	 * @return string|null
	 */
	public function build(\Rbs\Website\Documents\Website $website, int $pageId, int $sectionId, string $LCID, array $facetFilters)
	{
		$uri = $this->buildUri($website, $pageId, $sectionId, $LCID, $facetFilters);
		return $uri ? $uri->normalize()->toString() : null;
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website
	 * @param int|null $pageId
	 * @param int|null $sectionId
	 * @param string $LCID
	 * @param array $facetFilters
	 * @return \Zend\Uri\Http|null
	 */
	public function buildUri(\Rbs\Website\Documents\Website $website, int $pageId, int $sectionId, string $LCID, array $facetFilters)
	{
		if (!($website instanceof \Rbs\Website\Documents\Website))
		{
			return null;
		}

		$targetIds = [];
		$detailDoc = null;
		$page = $this->documentManager->getDocumentInstance($pageId);
		if ($page instanceof \Rbs\Website\Documents\StaticPage)
		{
			$targetIds[] = $pageId;
			$section = $page->getSection();
			$detailDoc = $page;
			if ($section->getIndexPageId() == $pageId)
			{
				$sectionId = $section->getId();
				$targetIds[] = $sectionId;
				$detailDoc = $section;
			}
		}
		elseif ($page instanceof \Rbs\Website\Documents\FunctionalPage)
		{
			$section = $this->documentManager->getDocumentInstance($sectionId);
			if ($section instanceof \Rbs\Website\Documents\Section)
			{
				$targetIds[] = $sectionId;
				$detailDoc = $section;
			}
		}

		if ($targetIds)
		{
			$uri = $this->buildForFacet($website, $pageId, $sectionId, $LCID, $targetIds, $facetFilters);
			if ($uri)
			{
				return $uri;
			}
		}

		if ($detailDoc)
		{
			$urlManager = $website->getUrlManager($LCID);
			$urlManager->absoluteUrl(false);
			return $urlManager->getCanonicalByDocument($detailDoc, ['facetFilters' => $facetFilters]);
		}
		return null;
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website
	 * @param array $targetIds
	 * @param int|null $pageId
	 * @param int|null $sectionId
	 * @param string $LCID
	 * @param array $facetFilters
	 * @return \Zend\Uri\Http|null
	 */
	protected function buildForFacet($website, $pageId, $sectionId, string $LCID, array $targetIds, $facetFilters)
	{
		// Look for a facetUrlGroup.
		$query = $this->documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
		$query->andPredicates($query->in('targetId', $targetIds), $query->in('targetLCID', $LCID));
		$groups = [];
		foreach ($query->getDocuments()->preLoad()->toArray() as $group)
		{
			/** @var \Rbs\Seo\Documents\FacetUrlGroup $group */
			$groups[$group->getTargetId()] = $group;
		}

		if (isset($groups[$pageId]))
		{
			$group = $groups[$pageId];
		}
		elseif ($sectionId && isset($groups[$sectionId]))
		{
			$group = $groups[$sectionId];
		}
		else
		{
			return null;
		}

		// Look for a pathRule id.
		$values = [];
		$facetIdsByName = $this->getFacetIdsByName($group);
		foreach ($facetFilters as $facetIdentifier => $facetValues)
		{
			$facetId = $facetIdsByName[$facetIdentifier] ?? null;
			if (count($facetValues) === 1 && $facetId)
			{
				foreach ($facetValues as $value => $true)
				{
					$values[(int)$facetId] = $value;
				}
			}
		}
		ksort($values);
		$facetIds = array_keys($values);

		$pathRuleId = null;
		$combinations = $this->getAllCombinations($facetIds, []);
		foreach ($combinations as $combination)
		{
			foreach ($group->getFacetUrlBatches() as $batch)
			{
				if ($combination == $batch->getFacetIds())
				{
					$valuesToMatch = [];
					foreach ($values as $facetId => $value)
					{
						if (in_array($facetId, $combination))
						{
							$valuesToMatch[] = $value;
						}
					}
					foreach ($batch->getUrlDefinitions() as $definition)
					{
						if ($definition->matches($valuesToMatch) && $definition->getPathRuleId())
						{
							$pathRuleId = $definition->getPathRuleId();
							$facetIds = $combination;
							break 3;
						}
					}
					break;
				}
			}
		}
		if (!$pathRuleId)
		{
			return null;
		}

		// Generate the relative URL.
		$pathRule = $this->pathRuleManager->getPathRuleById($pathRuleId);
		if (!$pathRule)
		{
			return null;
		}

		$queryArray = $facetFilters;
		$facetNamesById = array_flip($facetIdsByName);
		foreach ($facetIds as $facetId)
		{
			$facetName = $facetNamesById[$facetId] ?? 'f_' . $facetId;
			unset($queryArray[$facetName]);
		}

		$urlManager = $website->getUrlManager($LCID);
		$urlManager->absoluteUrl(false);
		$uri = $urlManager->getByPathInfo($pathRule->getRelativePath(), $queryArray ? ['facetFilters' => $queryArray] : []);
		return $uri;
	}

	/**
	 * @param int[] $facetIds
	 * @param array $combinations
	 * @return array
	 */
	protected function getAllCombinations($facetIds, $combinations = [])
	{
		$combinations[] = $facetIds;
		$count = count($facetIds);
		if ($count > 1)
		{
			/** @noinspection ForeachInvariantsInspection */
			for ($i = 0; $i < $count; $i++)
			{
				$combination = array_values($facetIds);
				unset($combination[$i]);
				$combinations = $this->getAllCombinations(array_values($combination), $combinations);
			}
		}
		usort($combinations, function ($a, $b)
		{
			$ca = count($a);
			$cb = count($b);
			if ($ca == $cb)
			{
				return 0;
			}
			return ($ca > $cb) ? -1 : 1;
		});
		return $combinations;
	}

	/**
	 * @param \Rbs\Seo\Documents\FacetUrlGroup $facetUrlGroup
	 * @return int[]
	 */
	protected function getFacetIdsByName(\Rbs\Seo\Documents\FacetUrlGroup $facetUrlGroup)
	{
		$facetDefinitions = [];
		foreach ($facetUrlGroup->getFacetUrlBatches() as $batch)
		{
			foreach ($batch->getFacetIds() as $facetId)
			{
				if (isset($facetDefinitions[$facetId]))
				{
					continue;
				}

				$facet = $this->documentManager->getDocumentInstance($facetId);
				if ($facet instanceof \Rbs\Elasticsearch\Documents\Facet)
				{
					$facetDefinitions[$facet->getFacetDefinition()->getFieldName()] = $facetId;
				}
			}
		}
		return $facetDefinitions;
	}
}