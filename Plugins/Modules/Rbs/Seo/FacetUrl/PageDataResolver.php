<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\FacetUrl;

/**
 * @name \Rbs\Seo\FacetUrl\PageDataResolver
 */
class PageDataResolver
{
	const MODE_ALL = 'all';
	const MODE_URI = 'uri';
	const MODE_METAS = 'metas';

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $pathRuleManager;

	/**
	 * @var \Rbs\Seo\SeoManager
	 */
	protected $seoManager;

	/**
	 * @var \Zend\Uri\Http|null
	 */
	protected $uri;

	/**
	 * @var array
	 */
	protected $metas = [];

	/**
	 * @var \Change\Documents\AbstractDocument|null
	 */
	protected $detailDocument;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @param \Rbs\Seo\SeoManager $seoManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, \Change\Http\Web\PathRuleManager $pathRuleManager,
		\Rbs\Seo\SeoManager $seoManager)
	{
		$this->documentManager = $documentManager;
		$this->pathRuleManager = $pathRuleManager;
		$this->seoManager = $seoManager;
	}

	/**
	 * @return null|\Zend\Uri\Http
	 */
	public function getUri()
	{
		return $this->uri;
	}

	/**
	 * @return array
	 */
	public function getMetas()
	{
		return $this->metas;
	}

	/**
	 * @return \Rbs\Seo\Documents\FacetUrlDecorator|null
	 */
	public function getDecorator()
	{
		return $this->detailDocument instanceof \Rbs\Seo\Documents\FacetUrlDecorator ? $this->detailDocument : null;
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website
	 * @param int|null $pageId
	 * @param int|null $sectionId
	 * @param string $LCID
	 * @param array $facetFilters
	 * @param string $mode
	 * @return bool
	 */
	public function resolve(\Rbs\Website\Documents\Website $website, int $pageId, int $sectionId, string $LCID, array $facetFilters,
		$mode = self::MODE_ALL): bool
	{
		$this->uri = null;
		$this->metas = [];
		$this->detailDocument = null;

		if (!($website instanceof \Rbs\Website\Documents\Website))
		{
			return false;
		}

		$targetIds = [];
		$page = $this->documentManager->getDocumentInstance($pageId);
		if ($page instanceof \Rbs\Website\Documents\StaticPage)
		{
			$targetIds[] = $pageId;
			$section = $page->getSection();
			$this->detailDocument = $page;
			if ($section->getIndexPageId() == $pageId)
			{
				$sectionId = $section->getId();
				$targetIds[] = $sectionId;
				$this->detailDocument = $section;
			}
		}
		elseif ($page instanceof \Rbs\Website\Documents\FunctionalPage)
		{
			$section = $this->documentManager->getDocumentInstance($sectionId);
			if ($section instanceof \Rbs\Website\Documents\Topic && $section->getIndexPage() === $page)
			{
				$targetIds[] = $sectionId;
				$this->detailDocument = $section;
			}
		}

		if ($targetIds)
		{
			$this->buildForFacet($website, $pageId, $sectionId, $LCID, $targetIds, $facetFilters, $mode);
		}

		if (!$this->uri && $this->detailDocument && ($mode === self::MODE_ALL || $mode === self::MODE_URI))
		{
			$urlManager = $website->getUrlManager($LCID);
			$urlManager->absoluteUrl(false);
			$this->uri = $urlManager->getCanonicalByDocument($this->detailDocument, ['facetFilters' => $facetFilters]);
		}

		if ($this->detailDocument && ($mode === self::MODE_ALL || $mode === self::MODE_METAS))
		{
			$this->metas = $this->seoManager->getMetas($page, $this->detailDocument);
		}

		return $this->uri !== null;
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website
	 * @param int|null $pageId
	 * @param int|null $sectionId
	 * @param string $LCID
	 * @param array $targetIds
	 * @param array $facetFilters
	 * @param string $mode
	 */
	protected function buildForFacet($website, $pageId, $sectionId, string $LCID, array $targetIds, $facetFilters, string $mode)
	{
		// Look for a facetUrlGroup.
		$query = $this->documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
		$query->andPredicates($query->in('targetId', $targetIds), $query->in('targetLCID', $LCID));
		$groups = [];
		foreach ($query->getDocuments()->preLoad()->toArray() as $group)
		{
			/** @var \Rbs\Seo\Documents\FacetUrlGroup $group */
			$groups[$group->getTargetId()] = $group;
		}

		if (isset($groups[$pageId]))
		{
			$group = $groups[$pageId];
		}
		elseif ($sectionId && isset($groups[$sectionId]))
		{
			$group = $groups[$sectionId];
		}
		else
		{
			return;
		}

		// Look for a pathRule id.
		$facetIdsByName = $this->getFacetIdsByName($group);
		$values = [];
		$this->extractFacetValues($values, $facetFilters, $facetIdsByName);
		ksort($values);
		$facetIds = array_keys($values);

		$pathRuleId = null;
		$combinations = $this->getAllCombinations($facetIds, []);
		foreach ($combinations as $combination)
		{
			foreach ($group->getFacetUrlBatches() as $batch)
			{
				if ($combination == $batch->getFacetIds())
				{
					$valuesToMatch = [];
					foreach ($values as $facetId => $value)
					{
						if (in_array($facetId, $combination))
						{
							$valuesToMatch[] = $value;
						}
					}
					foreach ($batch->getUrlDefinitions() as $definition)
					{
						if ($definition->matches($valuesToMatch) && $definition->getPathRuleId())
						{
							$pathRuleId = $definition->getPathRuleId();
							$facetIds = $combination;
							break 3;
						}
					}
					break;
				}
			}
		}
		if (!$pathRuleId)
		{
			return;
		}

		// Generate the relative URL.
		$pathRule = $this->pathRuleManager->getPathRuleById($pathRuleId);
		if (!$pathRule)
		{
			return;
		}

		$decoratorId = $pathRule->getDecoratorId();
		if ($decoratorId && ($mode === self::MODE_ALL || $mode === self::MODE_METAS))
		{
			$this->detailDocument = $this->documentManager->getDocumentInstance($pathRule->getDecoratorId());
		}

		if ($mode === self::MODE_ALL || $mode === self::MODE_URI)
		{
			$queryArray = $facetFilters;
			$facetNamesById = array_flip($facetIdsByName);
			foreach ($facetIds as $facetId)
			{
				$facetName = $facetNamesById[$facetId] ?? 'f_' . $facetId;
				unset($queryArray[$facetName]);
			}

			$urlManager = $website->getUrlManager($LCID);
			$urlManager->absoluteUrl(false);
			$this->uri = $urlManager->getByPathInfo($pathRule->getRelativePath(), $queryArray ? ['facetFilters' => $queryArray] : []);
		}
	}

	/**
	 * @param int[] $facetIds
	 * @param array $combinations
	 * @return array
	 */
	protected function getAllCombinations($facetIds, $combinations = [])
	{
		$combinations[] = $facetIds;
		$count = count($facetIds);
		if ($count > 1)
		{
			/** @noinspection ForeachInvariantsInspection */
			for ($i = 0; $i < $count; $i++)
			{
				$combination = array_values($facetIds);
				unset($combination[$i]);
				$combinations = $this->getAllCombinations(array_values($combination), $combinations);
			}
		}
		usort($combinations, function ($a, $b)
		{
			$ca = count($a);
			$cb = count($b);
			if ($ca == $cb)
			{
				return 0;
			}
			return ($ca > $cb) ? -1 : 1;
		});
		return $combinations;
	}

	/**
	 * @param \Rbs\Seo\Documents\FacetUrlGroup $facetUrlGroup
	 * @return int[]
	 */
	protected function getFacetIdsByName(\Rbs\Seo\Documents\FacetUrlGroup $facetUrlGroup)
	{
		$facetDefinitions = [];
		foreach ($facetUrlGroup->getFacetUrlBatches() as $batch)
		{
			foreach ($batch->getFacetIds() as $facetId)
			{
				if (isset($facetDefinitions[$facetId]))
				{
					continue;
				}

				$facet = $this->documentManager->getDocumentInstance($facetId);
				if ($facet instanceof \Rbs\Elasticsearch\Documents\Facet)
				{
					$facetDefinitions[$facet->getFacetDefinition()->getFieldName()] = $facetId;
				}
			}
		}
		return $facetDefinitions;
	}

	/**
	 * @param array $values
	 * @param array $facetFilters
	 * @param array $facetIdsByName
	 */
	protected function extractFacetValues(&$values, $facetFilters, $facetIdsByName)
	{
		foreach ($facetFilters as $facetIdentifier => $facetValues)
		{
			$facetId = $facetIdsByName[$facetIdentifier] ?? null;
			if ($facetId && count($facetValues) === 1)
			{
				foreach ($facetValues as $value => $subFacetValues)
				{
					if (!is_array($subFacetValues))
					{
						$values[(int)$facetId] = $value;
					}
				}
			}
		}
	}
}