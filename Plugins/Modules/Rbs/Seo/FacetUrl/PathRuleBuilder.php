<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\FacetUrl;

/**
 * @name \Rbs\Seo\FacetUrl\PathRuleBuilder
 */
class PathRuleBuilder
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $pathRuleManager;

	/**
	 * @var \Rbs\Seo\SeoManager
	 */
	protected $seoManager;

	/**
	 * @var \Change\Workspace
	 */
	protected $workspace;

	/**
	 * @var \Rbs\Seo\Std\PathTemplateComposer
	 */
	protected $pathCleaner;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @param \Rbs\Seo\SeoManager $seoManager
	 * @param \Change\Workspace $workspace
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, \Change\Http\Web\PathRuleManager $pathRuleManager,
		\Rbs\Seo\SeoManager $seoManager, \Change\Workspace $workspace)
	{
		$this->documentManager = $documentManager;
		$this->pathRuleManager = $pathRuleManager;
		$this->seoManager = $seoManager;
		$this->workspace = $workspace;
		$this->pathCleaner = new \Rbs\Seo\Std\PathTemplateComposer();
	}

	/**
	 * @param \Rbs\Seo\Documents\FacetUrlGroup $group
	 * @param \Rbs\Seo\FacetUrl\Batch $batch
	 * @param \Rbs\Seo\FacetUrl\Definition $urlDefinition
	 * @return \Change\Http\Web\PathRule|null
	 */
	public function tryToBuild(\Rbs\Seo\Documents\FacetUrlGroup $group, \Rbs\Seo\FacetUrl\Batch $batch, \Rbs\Seo\FacetUrl\Definition $urlDefinition)
	{
		// Create new path rules.
		$target = $group->getTargetIdInstance();
		if ($target instanceof \Rbs\Website\Documents\Section)
		{
			$website = $target->getWebsite();
		}
		elseif ($target instanceof \Rbs\Website\Documents\StaticPage)
		{
			$website = $target->getSection()->getWebsite();
		}
		if (!isset($website))
		{
			return null;
		}

		$websiteId = $website->getId();
		$LCID = $group->getTargetLCID();
		$targetId = $target->getId();

		$urlPattern = $batch->getUrlPattern();
		$facetDefinitions = [];
		foreach ($batch->getFacetIds() as $facetId)
		{
			$facet = $this->documentManager->getDocumentInstance($facetId);
			if ($facet instanceof \Rbs\Elasticsearch\Documents\Facet)
			{
				$facetDefinitions[$facetId] = $facet->getFacetDefinition();
			}
		}

		return $this->build($urlDefinition, $websiteId, $LCID, $targetId, $urlPattern, $facetDefinitions);
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Definition $urlDefinition
	 * @param integer $websiteId
	 * @param string $LCID
	 * @param integer $targetId
	 * @param string $urlPattern
	 * @param \Rbs\Elasticsearch\Documents\Facet[] $facetDefinitions
	 * @return \Change\Http\Web\PathRule
	 */
	public function build(\Rbs\Seo\FacetUrl\Definition $urlDefinition, $websiteId, $LCID, $targetId, $urlPattern, $facetDefinitions)
	{
		$rootPath = $this->getTargetRelativePath($targetId, $websiteId, $LCID);
		$relativePath = $this->generateRelativePath($urlPattern, $rootPath, $urlDefinition);
		$relativePath = $this->pathCleaner->doCleanRelativePath($this->workspace, $relativePath);
		$query = $this->generateQuery($urlDefinition, $facetDefinitions);
		return $this->pathRuleManager->getNewRule($websiteId, $LCID, $relativePath, $targetId, 200, 0, $query, false, $urlDefinition->getDecoratorId());
	}

	/**
	 * @param string $urlPattern
	 * @param string $rootPath
	 * @param \Rbs\Seo\FacetUrl\Definition $urlDefinition
	 * @return string
	 */
	protected function generateRelativePath(string $urlPattern, string $rootPath, \Rbs\Seo\FacetUrl\Definition $urlDefinition)
	{
		$relativePath = str_replace('{root}', (substr($rootPath, -1) === '/') ? substr($rootPath, 0, -1) : $rootPath, $urlPattern);

		foreach ($urlDefinition->getValues() as $index => $facetValue)
		{
			$valueLabel = $this->seoManager->getFacetValueTitle($facetValue);
			$relativePath = str_replace('{' . ($index + 1) . '}', $valueLabel ?: $facetValue->getValue(), $relativePath);
		}
		return $relativePath;
	}

	/**
	 * @param \Rbs\Seo\FacetUrl\Definition $urlDefinition
	 * @param \Rbs\Elasticsearch\Facet\FacetDefinitionInterface[] $facetDefinitions
	 * @return string
	 */
	protected function generateQuery(\Rbs\Seo\FacetUrl\Definition $urlDefinition, array $facetDefinitions)
	{
		$query = ['facetFilters' => []];
		foreach ($urlDefinition->getValues() as $value)
		{
			$definition = $facetDefinitions[$value->getFacetId()] ?? null;
			$query['facetFilters'][$definition ? $definition->getFieldName() : 'f_' . $value->getFacetId()] = [$value->getValue() => 1];
		}
		ksort($query, SORT_STRING);
		return http_build_query($query);
	}

	/**
	 * @param int $targetId
	 * @param int $websiteId
	 * @param string $LCID
	 * @return string
	 */
	protected function getTargetRelativePath(int $targetId, int $websiteId, string $LCID)
	{
		$targetRelativePath = '';
		foreach ($this->pathRuleManager->getAllForDocumentId($targetId) as $pathRule)
		{
			if ($pathRule->getWebsiteId() === $websiteId && $pathRule->getLCID() === $LCID && $pathRule->getHttpStatus() === 200
				&& !$pathRule->getQuery()
			)
			{
				$targetRelativePath = $pathRule->getRelativePath();
				break;
			}
		}
		return $targetRelativePath;
	}
}