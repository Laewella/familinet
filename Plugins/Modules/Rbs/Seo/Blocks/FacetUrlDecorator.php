<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Blocks;

/**
 * @name \Rbs\Seo\Blocks\FacetUrlDecorator
 */
class FacetUrlDecorator extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);

		$parameters->addParameterMeta('imageFormats', 'attribute');
		$parameters->addParameterMeta('dataSetNames');

		$parameters->addParameterMeta('defaultImageFormat', 'attribute'); // Must be present in imageFormats.
		$parameters->addParameterMeta('visibility', 'list');
		$parameters->addParameterMeta('attributesMode', 'flat');

		$parameters->setLayoutParameters($event->getBlockLayout());

		/** @var \Change\Http\Web\PathRule $pathRule */
		$pathRule = $event->getParam('pathRule');

		$parameters->setParameterValue('decoratorId', $pathRule->getDecoratorId());

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$context = $this->populateContext($event->getApplication(), $documentManager, $parameters);
		$contextArray = $context->toArray();

		$decorator = $documentManager->getDocumentInstance($parameters->getParameter('decoratorId'), 'Rbs_Seo_FacetUrlDecorator');
		$decoratorData = $decorator instanceof \Rbs\Seo\Documents\FacetUrlDecorator ? $decorator->getAJAXData($contextArray) : null;

		unset($contextArray['page'], $contextArray['section'], $contextArray['website'], $contextArray['websiteUrlManager']);
		$attributes['blockData'] = ['context' => $contextArray, 'decoratorData' => $decoratorData];

		return 'facet-url-decorator.twig';
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return \Change\Http\Ajax\V1\Context
	 */
	protected function populateContext($application, $documentManager, $parameters)
	{
		$context = new \Change\Http\Ajax\V1\Context($application, $documentManager);
		$context->setVisualFormats($parameters->getParameter('imageFormats'));
		$context->setURLFormats(['canonical']);
		$context->setDataSetNames($parameters->getParameter('dataSetNames'));
		$context->setPage($parameters->getParameter('pageId'));
		$context->addData('facetFilters', $parameters->getParameter('facetFilters'));
		return $context;
	}
}