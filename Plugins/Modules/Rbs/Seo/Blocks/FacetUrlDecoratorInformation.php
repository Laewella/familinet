<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Blocks;

/**
 * @name \Rbs\Seo\Blocks\FacetUrlDecoratorInformation
 */
class FacetUrlDecoratorInformation extends \Change\Presentation\Blocks\Information
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.seo.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.seo.admin.facet_url_decorator_label', $ucf));
	}
}