<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API;

/**
 * @name \Rbs\Seo\Http\API\Models
 */
class Models extends \Rbs\Ua\Http\API\Models
{
	/**
	 * @var \Rbs\Seo\SeoManager
	 */
	protected $seoManager;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function __construct(\Change\Http\Event $event)
	{
		parent::__construct($event);

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$this->setSeoManager($genericServices->getSeoManager());
	}

	/**
	 * @return \Rbs\Seo\SeoManager
	 */
	public function getSeoManager()
	{
		return $this->seoManager;
	}

	/**
	 * @param \Rbs\Seo\SeoManager $seoManager
	 * @return $this
	 */
	public function setSeoManager($seoManager)
	{
		$this->seoManager = $seoManager;
		return $this;
	}

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @return mixed
	 */
	public function normalizeObject($object, $objectModel)
	{
		$type = $objectModel->getType();
		switch ($type)
		{
			case 'Rbs.Seo.PathRule':
			case 'Rbs.Seo.Url404':
				if (!isset($object['id']))
				{
					$object['id'] = $object['ruleId'] ?? null;
				}
				if (!isset($object['LCID']))
				{
					$object['LCID'] = $object['lcid'] ?? null;
				}

				$object['LCIDLabel'] = $this->getI18nManager()->transLCID($object['LCID'], ['ucf']);
				/** @var \Rbs\Website\Documents\Website $website */
				$website = $this->documentManager->getDocumentInstance($object['websiteId'], 'Rbs_Website_Website');
				if ($website)
				{
					$object['websiteLabel'] = $website ? $website->getLabel() : '';
					try
					{
						$object['publicUrl'] =
							$website->getUrlManager($object['LCID'])->getBaseUri()->normalize()->toString() . $object['relativePath'];
					}
					catch (\Exception $e)
					{
						$object['publicUrl'] = '';
					}
				}
				else
				{
					$object['websiteLabel'] = '';
					$object['publicUrl'] = '';
				}

				if ($type === 'Rbs.Seo.PathRule')
				{
					$object['model'] = 'Rbs_Seo_PathRule';
					$document = $this->documentManager->getDocumentInstance($object['documentId']);
					if ($document)
					{
						$object['documentLabel'] = $document ? $document->getDocumentModel()->getPropertyValue($document, 'label', '') : '';

						$documentModel = $document->getDocumentModel();
						$object['documentModelName'] = $documentModel->getName();
						$object['documentModelLabel'] = $this->i18nManager->trans($documentModel->getLabelKey(), ['ucf']);
					}

					$object['documentAliasLabel'] = null;
					if ($object['documentAliasId'])
					{
						$alias = $this->documentManager->getDocumentInstance($object['documentAliasId']);
						$object['documentAliasLabel'] = $alias ? $alias->getDocumentModel()->getPropertyValue($alias, 'label', '') : '';
					}

					$object['sectionLabel'] = null;
					if ($object['sectionId'])
					{
						/** @var \Rbs\Website\Documents\Section $section */
						$section = $this->documentManager->getDocumentInstance($object['sectionId'], 'Rbs_Website_Section');
						$object['sectionLabel'] = $section ? $section->getLabel() : '';
					}
				}
				elseif ($type === 'Rbs.Seo.Url404')
				{
					$object['model'] = 'Rbs_Seo_Url404';
					$object['httpStatus'] = 404;
					$object['insertionDate'] = new \DateTime($object['insertionDate']);

					$documentData = $object['documentData'] ? json_decode($object['documentData'], true) : null;
					if (is_array($documentData))
					{
						$object['documentLabel'] = $documentData['label'] ?? '';
						$documentModel = $this->documentManager->getModelManager()->getModelByName($documentData['model']);
						if ($documentModel)
						{
							$object['documentModelName'] = $documentModel->getName();
							$object['documentModelLabel'] = $this->i18nManager->trans($documentModel->getLabelKey(), ['ucf']);
						}
					}
					elseif ($object['documentId'])
					{
						$document = $this->documentManager->getDocumentInstance($object['documentId']);
						if ($document)
						{
							$object['documentLabel'] = $document ? $document->getDocumentModel()->getPropertyValue($document, 'label', '') : '';

							$documentModel = $document->getDocumentModel();
							$object['documentModelName'] = $documentModel->getName();
							$object['documentModelLabel'] = $this->i18nManager->trans($documentModel->getLabelKey(), ['ucf']);
						}
					}

					$ruleData = $object['ruleData'] ? json_decode($object['ruleData'], true) : null;
					if (is_array($ruleData))
					{
						$object['originalHttpStatus'] = isset($ruleData['http_status']) ? (int)$ruleData['http_status'] : null;
						$object['query'] = $ruleData['query'] ?? null;
						$object['userEdited'] = $ruleData['user_edited'] ?? null;
						$object['sectionId'] = isset($ruleData['section_id']) ? (int)$ruleData['section_id'] : '';

						$section = null;
						if ($object['sectionId'])
						{
							$section = $this->documentManager->getDocumentInstance($object['sectionId']);
						}
						$object['sectionLabel'] = ($section instanceof \Rbs\Website\Documents\Section) ? $section->getLabel() : '';
					}
				}
				return $object;
			case 'Rbs.Seo.PathRuleForSelector':
				$object['model'] = 'Rbs_Seo_PathRule';
				$object['label'] = $object['relativePath'];
				$object['LCIDLabel'] = $this->getI18nManager()->transLCID($object['lcid'], ['ucf']);

				/** @var \Rbs\Website\Documents\Website $website */
				$website = $this->documentManager->getDocumentInstance($object['websiteId'], 'Rbs_Website_Website');
				if ($website)
				{
					$object['websiteLabel'] = $website ? $website->getLabel() : '';
					try
					{
						$object['publishedUrl'] =
							$website->getUrlManager($object['lcid'])->getBaseUri()->normalize()->toString() . $object['relativePath'];
					}
					catch (\Exception $e)
					{
						$object['publishedUrl'] = '';
					}
				}
				else
				{
					$object['websiteLabel'] = '';
					$object['publishedUrl'] = '';
				}
				return $object;
		}
		return parent::normalizeObject($object, $objectModel);
	}

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @param string $propertyName
	 * @param bool $resolved
	 * @return int|mixed|null
	 */
	public function resolveProperty($object, $objectModel, $propertyName, &$resolved)
	{
		$resolved = true;
		$type = $objectModel->getType();
		$key = $type . '/' . $propertyName;
		switch ($key)
		{
			case 'Rbs.Seo.ModelMetasI18n/modeLabel':
			case 'Rbs.Seo.ModelMetasI18nMinimal/modeLabel':
			case 'Rbs.Seo.DocumentMetasI18n/modeLabel':
			case 'Rbs.Seo.DocumentMetasI18nMinimal/modeLabel':
				if ($object instanceof \Rbs\Seo\Std\PublicationMetas)
				{
					return $this->i18nManager->trans('m.rbs.seo.ua.rendering_mode_' . ($object ? $object->getMode() : 'none'), ['ucf']);
				}
				return null;

			case 'Rbs.Seo.ModelMetasI18n/metaGroups':
			case 'Rbs.Seo.DocumentMetasI18n/metaGroups':
				if ($object instanceof \Rbs\Seo\Std\PublicationMetas)
				{
					return $this->generateMetaValueGroupData($object);
				}
				return null;

			case 'Rbs.Seo.ModelMetas/i18n':
			case 'Rbs.Seo.ModelMetasSummary/i18n':
				if ($object instanceof \Rbs\Seo\Documents\ModelConfiguration)
				{
					$publicationMeta = $object->getDefaultPublicationMetas();
					if (!$publicationMeta)
					{
						$publicationMeta = new \Rbs\Seo\Std\PublicationMetas(['mode' => 'auto']);
					}
					return $publicationMeta;
				}
				return null;

			case 'Rbs.Seo.ModelPathRulesCommon/target':
			case 'Rbs.Seo.ModelMetasCommon/target':
			case 'Rbs.Seo.ModelSitemapCommon/target':
				if ($object instanceof \Rbs\Seo\Documents\ModelConfiguration)
				{
					return $object->getModelName();
				}
				return null;

			case 'Rbs.Seo.ModelMetas/languages':
			case 'Rbs.Seo.ModelMetasSummary/languages':
				if ($object instanceof \Rbs\Seo\Documents\ModelConfiguration)
				{
					$i18n = [];
					$refLCID = $this->i18nManager->getDefaultLCID();
					foreach ($this->i18nManager->getSupportedLCIDs() as $LCID)
					{
						$i18n[] = [
							'LCID' => $LCID,
							'label' => $this->i18nManager->transLCID($LCID, ['ucf']),
							'isRef' => $refLCID === $LCID
						];
					}
					return $i18n;
				}
				return null;

			case 'Rbs.Seo.DocumentMetas/i18n':
			case 'Rbs.Seo.DocumentMetasSummary/i18n':
				$forceAuto = false;
				if ($object instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
				{
					$publicationMeta = $object->getPublicationMetas();
					switch ($publicationMeta instanceof \Rbs\Seo\Std\PublicationMetas ? $publicationMeta->getMode() : 'auto')
					{
						case 'manual':
							return $publicationMeta;

						case 'none':
							return new \Rbs\Seo\Std\PublicationMetas(['mode' => 'none']);

						case 'auto':
						default:
							if ($object instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
							{
								$object = $object->getTargetIdInstance();
								$forceAuto = true;
							}
					}
				}

				if ($object instanceof \Change\Documents\Interfaces\Publishable)
				{
					$publicationMeta = $object->getPublicationMetas();
					switch ($publicationMeta instanceof \Rbs\Seo\Std\PublicationMetas ? $publicationMeta->getMode() : 'auto')
					{
						case 'manual':
							break;

						case 'none':
							$publicationMeta = new \Rbs\Seo\Std\PublicationMetas(['mode' => 'none']);
							break;

						case 'auto':
						default:
							$modelConfiguration = $this->documentManager->getDocumentInstance($object->getModelConfigurationId());
							$metaData = ['mode' => 'none'];
							if ($modelConfiguration instanceof \Rbs\Seo\Documents\ModelConfiguration)
							{
								$defaultPublicationMeta = $modelConfiguration->getDefaultPublicationMetas();
								if ($defaultPublicationMeta)
								{
									$metaData = $defaultPublicationMeta->toArray();
									$metaData['mode'] = 'auto';
								}
							}
							$publicationMeta = new \Rbs\Seo\Std\PublicationMetas($metaData);
							break;
					}

					if ($forceAuto)
					{
						$publicationMeta->setMode('auto');
					}
					return $publicationMeta;
				}
				return null;

			case 'Rbs.Seo.DocumentSitemap/data':
				if ($object instanceof \Change\Documents\Interfaces\Publishable || $object instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
				{
					$publicationSitemap = $object->getPublicationSitemap();
					if (!$publicationSitemap)
					{
						$publicationSitemap = new \Rbs\Seo\Std\PublicationSitemap();
					}
					return $publicationSitemap;
				}
				return null;

			case 'Rbs.Seo.DocumentSitemap/modelData':
				if ($object instanceof \Change\Documents\Interfaces\Publishable || $object instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
				{
					$publicationSitemap = new \Rbs\Seo\Std\PublicationSitemap();
					$modelConfiguration = $this->documentManager->getDocumentInstance($object->getModelConfigurationId());
					if ($modelConfiguration instanceof \Rbs\Seo\Documents\ModelConfiguration)
					{
						$publicationSitemap->setFrequency($modelConfiguration->getSitemapDefaultChangeFrequency());
						$publicationSitemap->setPriority($modelConfiguration->getSitemapDefaultPriority());
					}
					return $publicationSitemap;
				}
				return null;

			case 'Rbs.Seo.DocumentMetas/languages':
			case 'Rbs.Seo.DocumentMetasSummary/languages':
				if ($object instanceof \Rbs\Seo\Documents\FacetUrlDecorator)
				{
					$LCID = $object->getTargetLCID();
					$i18n = [[
						'LCID' => $LCID,
						'label' => $this->i18nManager->transLCID($LCID, ['ucf']),
						'isRef' => $LCID === $LCID
					]];
					return $i18n;
				}
				if ($object instanceof \Change\Documents\AbstractDocument)
				{
					if ($object instanceof \Change\Documents\Interfaces\Localizable)
					{
						$refLCID = $object->getRefLCID();
						$LCIDs = $object->getLCIDArray();
					}
					else
					{
						$refLCID = $this->i18nManager->getDefaultLCID();
						$LCIDs = $this->i18nManager->getSupportedLCIDs();
					}

					$i18n = [];
					foreach ($LCIDs as $LCID)
					{
						$i18n[] = [
							'LCID' => $LCID,
							'label' => $this->i18nManager->transLCID($LCID, ['ucf']),
							'isRef' => $refLCID === $LCID
						];
					}
					return $i18n;
				}
				return null;

			case 'Rbs.Seo.DocumentPathRulesCommon/publicationSectionCount':
				if ($object instanceof \Change\Documents\Interfaces\Publishable)
				{
					return count($object->getPublicationSections());
				}
				return null;

			case 'Rbs.Seo.FacetUrlDefinitionValue/label':
				if ($object instanceof \Rbs\Seo\FacetUrl\Value)
				{
					return $this->getSeoManager()->getFacetValueTitle($object);
				}
				return null;
		}
		return parent::resolveProperty($object, $objectModel, $propertyName, $resolved);
	}

	/**
	 * @param \Rbs\Seo\Std\PublicationMetas $publicationMeta
	 * @return array
	 */
	protected function generateMetaValueGroupData($publicationMeta)
	{
		$groupComposer = new \Rbs\Seo\Http\API\MetaValueGroupComposer($this->getI18nManager());
		$indexedMetaValues = $groupComposer->prepareMetaValues($publicationMeta);
		$groupsData = [
			$groupComposer->getDefaultMetaValueGroupData($indexedMetaValues)
		];

		if ($publicationMeta instanceof \Rbs\Seo\Std\PublicationMetas)
		{
			foreach ($publicationMeta->getGroupIds() as $groupId)
			{
				/** @noinspection PhpParamsInspection */
				$groupData = $groupComposer->getMetaValueGroupData($this->getDocumentManager()->getDocumentInstance($groupId), $indexedMetaValues);
				if ($groupData)
				{
					$groupsData[] = $groupData;
				}
			}
		}
		return $groupsData;
	}
}