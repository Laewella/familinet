<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API;

/**
 * @name \Rbs\Seo\Http\API\MetaValueResolver
 */
class MetaValueResolver
{
	/**
	 * @param string $type
	 * @param string $name
	 * @param array $newMetaValues
	 * @param array $oldMetaValues
	 * @return \Rbs\Seo\Std\PublicationMeta
	 */
	public function resolveMeta($type, $name, $newMetaValues, $oldMetaValues)
	{
		$key = $type . '=' . $name;
		$metaData = [
			'type' => $type,
			'name' => $name,
			'value' => $oldMetaValues[$key] ?? null
		];

		if (array_key_exists($key, $newMetaValues))
		{
			$metaData['value'] = $newMetaValues[$key];
		}
		return new \Rbs\Seo\Std\PublicationMeta($metaData);
	}

	/**
	 * @param \Rbs\Seo\Std\PublicationMetas|array $metaValues
	 * @return array
	 */
	public function indexMetaValues($metaValues)
	{
		$indexedMetaValues = [];
		if (is_array($metaValues))
		{
			foreach ($metaValues as $metaData)
			{
				$indexedMetaValues[$metaData['type'] . '=' . $metaData['name']] = $metaData['value'];
			}
		}
		elseif ($metaValues instanceof \Rbs\Seo\Std\PublicationMetas)
		{
			foreach ($metaValues->getMetas() as $meta)
			{
				$indexedMetaValues[$meta->getType() . '=' . $meta->getName()] = $meta->getValue();
			}
		}
		return $indexedMetaValues;
	}
}