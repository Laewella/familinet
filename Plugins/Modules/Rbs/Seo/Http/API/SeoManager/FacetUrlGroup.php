<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\FacetUrlGroup
 */
class FacetUrlGroup
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Seo_FacetUrlGroup', $inputParameters['LCID']);
		$query->andPredicates(
			$query->eq('targetLCID', $inputParameters['LCID']),
			$query->eq('websiteId', $inputParameters['websiteId'])
		);
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		foreach ($value['items'] as $item)
		{
			/** @var \Rbs\Seo\Documents\FacetUrlGroup $item */
			$target = $item->getTargetIdInstance();
			if ($target instanceof \Rbs\Website\Documents\Topic || $target instanceof \Rbs\Website\Documents\StaticPage)
			{
				\Rbs\Ua\Model\ObjectModel::addAPIProperties($item, [
					'targetId' => $target->getId(),
					'targetLabel' => $target->getLabel(),
					'targetModel' => $target->getDocumentModel()->getName(),
					'targetModelLabel' => $i18nManager->trans($target->getDocumentModel()->getLabelKey(), ['ucf'])
				]);
			}
			else
			{
				\Rbs\Ua\Model\ObjectModel::addAPIProperties($item, [
					'targetId' => $target->getId(),
					'targetLabel' => '[' . $target->getId() . ']',
					'targetModel' => 'UNDEFINED',
					'targetModelLabel' => $i18nManager->trans('m.rbs.seo.ua.undefined', ['ucf'])
				]);
			}

			\Rbs\Ua\Model\ObjectModel::addAPIProperties($item, [
				'urlConflictCount' => $item->getFacetUrlConflictCount()
			]);
		}

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if ($document instanceof \Rbs\Website\Documents\Section || $document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$query = $documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
			$query->andPredicates($query->eq('targetId', $document->getId()), $query->eq('targetLCID', $inputParameters->get('LCID')));
			$document = $query->getFirstDocument();
		}

		if (!($document instanceof \Rbs\Seo\Documents\FacetUrlGroup))
		{
			return;
		}

		/** @var \Rbs\Website\Documents\Section|\Rbs\Website\Documents\StaticPage $target */
		$target = $document->getTargetIdInstance();
		$website = $documentManager->getDocumentInstance($document->getWebsiteId());
		if ($website instanceof \Rbs\Website\Documents\Website)
		{
			$websiteId = $website->getId();
			$websiteLabel = $website->getLabel() . ' - ' . $i18nManager->transLCID($document->getTargetLCID());
			$baseUrl = $website->getBaseurl();
		}
		else
		{
			$websiteId = 0;
			$websiteLabel = $i18nManager->trans('m.rbs.seo.ua.undefined', ['ucf']) . ' - ' . $i18nManager->transLCID($document->getTargetLCID());
			$baseUrl = null;
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($document, [
			'websiteId' => $websiteId,
			'websiteLabel' => $websiteLabel,
			'baseUrl' => $baseUrl,
			'targetId' => $target->getId(),
			'targetLabel' => $target->getLabel(),
			'targetModel' => $target->getDocumentModel()->getName(),
			'targetModelLabel' => $i18nManager->trans($target->getDocumentModel()->getLabelKey()),
			'urlConflictCount' => $document->getFacetUrlConflictCount(),
			'generating' => $document->getMeta('Job_RefreshFacetUrls') ? true : false
		]);

		$data = ['item' => $document];

		$inputFields = (array)$event->getParam('inputFields');
		if (in_array('url', $inputFields))
		{
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$seoManager = $genericServices->getSeoManager();
			$workspace = $event->getApplication()->getWorkspace();
			$applicationServices = $event->getApplicationServices();
			$pathRuleManager = $applicationServices->getPathRuleManager();
			$data['url'] = $this->extractUrls($workspace, $documentManager, $pathRuleManager, $seoManager, $document);
		}

		$event->setResult($this->buildArrayResult($event, $data, new \Rbs\Seo\Http\API\Models($event), $inputFields));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function insert($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$target = $documentManager->getDocumentInstance($inputParameters->get('targetId'));
		if (!$LCID || !($target instanceof \Rbs\Website\Documents\Topic || $target instanceof \Rbs\Website\Documents\StaticPage))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			/** @var \Rbs\Seo\Documents\FacetUrlGroup $document */
			$document = $documentManager->getNewDocumentInstanceByModelName('Rbs_Seo_FacetUrlGroup');
			$document->setLabel($target->getLabel());
			$document->setTargetId($target->getId());
			$document->setTargetLCID($LCID);
			$document->setFacetUrlBatches($inputParameters->get('facetUrlBatches'));
			$document->save();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_saving_facet_url_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if ($document instanceof \Rbs\Website\Documents\Section || $document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$query = $documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
			$query->andPredicates($query->eq('targetId', $document->getId()), $query->eq('targetLCID', $inputParameters->get('LCID')));
			$document = $query->getFirstDocument();
		}

		if (!($document instanceof \Rbs\Seo\Documents\FacetUrlGroup))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$data = $inputParameters->get('data');
			$document->setFacetUrlBatches($data['facetUrlBatches']);
			$document->setForceRefreshFacetUrl(true);
			$document->save();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_saving_facet_url_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function delete($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if ($document instanceof \Rbs\Website\Documents\Section || $document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$query = $documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
			$query->andPredicates($query->eq('targetId', $document->getId()), $query->eq('targetLCID', $inputParameters->get('LCID')));
			$document = $query->getFirstDocument();
		}

		if (!($document instanceof \Rbs\Seo\Documents\FacetUrlGroup))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$document->delete();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_deleting_facet_url_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function urlList($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		$website = null;
		if ($document instanceof \Rbs\Website\Documents\Section || $document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$query = $documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
			$query->andPredicates($query->eq('targetId', $document->getId()), $query->eq('targetLCID', $inputParameters->get('LCID')));
			$document = $query->getFirstDocument();
		}

		if (!($document instanceof \Rbs\Seo\Documents\FacetUrlGroup))
		{
			return;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		$workspace = $event->getApplication()->getWorkspace();
		$applicationServices = $event->getApplicationServices();
		$pathRuleManager = $applicationServices->getPathRuleManager();
		$facetUrls = $this->extractUrls($workspace, $documentManager, $pathRuleManager, $seoManager, $document);

		$event->setResult($this->buildArrayResult($event, ['item' => $facetUrls], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function urlUpdate($event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$LCID = $inputParameters->get('LCID');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if ($document instanceof \Rbs\Website\Documents\Section || $document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$targetId = $document->getId();
			$query = $documentManager->getNewQuery('Rbs_Seo_FacetUrlGroup');
			$query->andPredicates($query->eq('targetId', $targetId), $query->eq('targetLCID', $LCID));
			$document = $query->getFirstDocument();
		}
		elseif ($document instanceof \Rbs\Seo\Documents\FacetUrlGroup)
		{
			$targetId = $document->getTargetId();
		}

		if (!isset($targetId) || !($document instanceof \Rbs\Seo\Documents\FacetUrlGroup))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$facetUrl = $inputParameters->get('facetUrl');
			$definition = $this->findDefinition($document, $facetUrl['values']);
			if (!$definition)
			{
				throw new \RuntimeException('Invalid values');
			}

			// Update path rules.
			$mainRule = [
				'id' => $facetUrl['pathRule']['_meta']['id'],
				'relativePath' => $facetUrl['pathRule']['common']['relativePath'],
				'userEdited' => $facetUrl['pathRule']['common']['userEdited'],
				'httpStatus' => $facetUrl['pathRule']['common']['httpStatus'],
				'query' => $facetUrl['pathRule']['common']['query']
			];

			$rules = ['urls' => [$mainRule], 'redirects' => $facetUrl['redirects']];
			$websiteId = $facetUrl['pathRule']['common']['websiteId'];
			$sectionId = $facetUrl['pathRule']['common']['sectionId'];

			$pathRuleId = $this->updatePathRules($rules, $targetId, $LCID, $websiteId, $sectionId, $pathRuleManager, $definition->getDecoratorId());
			if ($pathRuleId)
			{
				$definition->setPathRuleId($pathRuleId);
				$definition->setConflict(false);
			}

			$document->save();

			$message = $i18nManager->trans('m.rbs.seo.ua.success_saving_document_path_rules', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Rbs\Seo\Documents\FacetUrlGroup $document
	 * @param array $values
	 * @return \Rbs\Seo\FacetUrl\Definition
	 */
	protected function findDefinition($document, $values)
	{
		$facetIds = [];
		$facetValues = [];
		foreach ($values as $value)
		{
			$facetIds[] = $value['facetId'];
			$facetValues[$value['facetId']] = $value['value'];
		}

		foreach ($document->getFacetUrlBatches() as $batch)
		{
			if ($batch->getFacetIds() != $facetIds)
			{
				continue;
			}

			/** @noinspection LoopWhichDoesNotLoopInspection */
			foreach ($batch->getUrlDefinitions() as $definition)
			{
				foreach ($definition->getValues() as $value)
				{
					if ($value->getValue() !== $facetValues[$value->getFacetId()])
					{
						continue 2;
					}
				}
				return $definition;
			}
		}
		return null;
	}

	/**
	 * @param array $rules
	 * @param integer $documentId
	 * @param string $LCID
	 * @param integer $websiteId
	 * @param integer $sectionId
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @param integer $decoratorId
	 * @throws \Exception
	 * @return integer The main rule's id.
	 */
	protected function updatePathRules($rules, $documentId, $LCID, $websiteId, $sectionId, $pathRuleManager, $decoratorId)
	{
		$toAdd = [];
		$toUpdate = [];
		/** @var \Change\Http\Web\PathRule[] $existingRules */
		$existingRules = [];
		$pathRuleDocumentId = $documentId;
		$pathRuleDocumentAliasId = 0;

		foreach ($pathRuleManager->getAllForDocumentId($pathRuleDocumentId) as $pathRule)
		{
			$existingRules[$pathRule->getRuleId()] = $pathRule;

			if ($pathRule->getHttpStatus() === 200)
			{
				$pathRuleDocumentId = $pathRule->getDocumentId();
			}

			if ($pathRule->getDocumentAliasId() && (!$pathRuleDocumentAliasId || $pathRule->getHttpStatus() === 200))
			{
				$pathRuleDocumentAliasId = $pathRule->getDocumentAliasId();
			}
		}

		foreach (['urls', 'redirects'] as $type)
		{
			foreach ($rules[$type] as $rule)
			{
				$ruleId = (int)$rule['id'];
				if (isset($existingRules[$ruleId]) && $existingRules[$ruleId]->getHttpStatus() !== $rule['httpStatus'])
				{
					$toUpdate[] = $rule;
				}
				elseif ($ruleId < 0 && $rule['relativePath'])
				{
					$rule['websiteId'] = $websiteId;
					$rule['LCID'] = $LCID;
					$rule['sectionId'] = $sectionId;
					$toAdd[] = $rule;
				}
			}
		}

		$mainRuleId = 0;
		if (count($toUpdate) || count($toAdd))
		{
			foreach ($toUpdate as $rule)
			{
				$pathRuleManager->updateRuleStatus($rule['id'], $rule['httpStatus']);
				$pathRuleManager->updateRuleDecoratorId($rule['id'], $rule['httpStatus'] === 200 ? $decoratorId : 0);
			}

			foreach ($toAdd as $rule)
			{
				$pathRule = $pathRuleManager->getNewRule($rule['websiteId'], $rule['LCID'], $rule['relativePath'],
					$pathRuleDocumentId, $rule['httpStatus'], $rule['sectionId'], $rule['query'] ?? null, true,
					$rule['httpStatus'] === 200 ? $decoratorId : 0);
				$pathRule->setDocumentAliasId($pathRuleDocumentAliasId);
				$pathRuleManager->insertPathRule($pathRule);
				if ($pathRule->getHttpStatus() === 200)
				{
					$mainRuleId = $pathRule->getRuleId();
				}
			}
		}
		return $mainRuleId;
	}

	/**
	 * @param \Change\Workspace $workspace
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @param \Rbs\Seo\SeoManager $seoManager
	 * @param \Rbs\Seo\Documents\FacetUrlGroup $document
	 * @return array
	 */
	protected function extractUrls($workspace, $documentManager, $pathRuleManager, $seoManager, $document): array
	{
		$facetUrls = [];

		/** @var \Change\Http\Web\PathRule[] $redirects */
		$redirects = $pathRuleManager->findRedirectedRules($document->getWebsiteId(), $document->getTargetLCID(), $document->getTargetId(), 0);

		$ruleBuilder = new \Rbs\Seo\FacetUrl\PathRuleBuilder($documentManager, $pathRuleManager, $seoManager, $workspace);
		foreach ($document->getFacetUrlBatches() as $batch)
		{
			$facetDefinitions = [];
			foreach ($batch->getFacetIds() as $facetId)
			{
				$facet = $documentManager->getDocumentInstance($facetId);
				if ($facet instanceof \Rbs\Elasticsearch\Documents\Facet)
				{
					$facetDefinitions[$facetId] = $facet->getFacetDefinition();
				}
			}

			foreach ($batch->getUrlDefinitions() as $definition)
			{
				$url = [
					'values' => [],
					'decoratorId' => $definition->getDecoratorId(),
					'conflict' => $definition->getConflict(),
					'redirects' => []
				];

				foreach ($definition->getValues() as $facetValue)
				{
					$url['values'][] = [
						'facetId' => $facetValue->getFacetId(),
						'value' => $facetValue->getValue(),
						'label' => $seoManager->getFacetValueTitle($facetValue)
					];
				}

				$pathRule = $pathRuleManager->getPathRuleById($definition->getPathRuleId());
				if ($pathRule)
				{
					$url['pathRule'] = $pathRule->toArray();
					foreach ($redirects as $redirect)
					{
						if ($redirect->getQuery() === $pathRule->getQuery())
						{
							$url['redirects'][] = [
								'id' => $redirect->getRuleId(),
								'relativePath' => $redirect->getRelativePath(),
								'userEdited' => $redirect->getUserEdited(),
								'httpStatus' => $redirect->getHttpStatus(),
								'query' => $redirect->getQuery()
							];
						}
					}
				}
				elseif ($definition->getConflict())
				{
					$pathRule = $ruleBuilder->tryToBuild($document, $batch, $definition);
					if ($pathRule)
					{
						$pathRule->setRuleId(-1);
						$url['pathRule'] = $pathRule->toArray();
					}
				}

				$facetUrls[] = $url;
			}
		}
		return $facetUrls;
	}
}