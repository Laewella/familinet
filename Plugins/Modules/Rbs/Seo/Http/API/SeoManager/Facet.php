<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\Facet
 */
class Facet
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		$indexMappings = $event->getApplicationServices()->getCollectionManager()->getCollection('Rbs_Elasticsearch_IndexMapping');

		$data = [];
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$query = $documentManager->getNewQuery('Rbs_Elasticsearch_Facet');
		$query->addOrder('id', true);
		$facets = $query->getDocuments()->preLoad()->toArray();
		/** @var \Rbs\Elasticsearch\Documents\Facet $facet */
		foreach ($facets as $facet)
		{
			$data[] = $this->getFacetData($facet, $indexMappings);
		}

		$event->setResult($this->buildArrayResult($event, ['items' => $data]));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$facet = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($inputParameters->get('facetId'));
		if (!($facet instanceof \Rbs\Elasticsearch\Documents\Facet))
		{
			return;
		}

		$indexMappings = $event->getApplicationServices()->getCollectionManager()->getCollection('Rbs_Elasticsearch_IndexMapping');
		$data = ['item' => $this->getFacetData($facet, $indexMappings)];

		$inputFields = (array)$event->getParam('inputFields');
		if (in_array('value', $inputFields))
		{
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$elasticsearchManager = $genericServices->getElasticsearchManager();
			$data['value'] = $this->extractValues($elasticsearchManager, $facet);
		}

		$event->setResult($this->buildArrayResult($event, $data, null, $inputFields));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function values($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$facet = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($inputParameters->get('facetId'));
		if (!($facet instanceof \Rbs\Elasticsearch\Documents\Facet))
		{
			return;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$elasticsearchManager = $genericServices->getElasticsearchManager();
		$data = $this->extractValues($elasticsearchManager, $facet);
		$event->setResult($this->buildArrayResult($event, ['items' => $data]));
	}

	/**
	 * @param \Rbs\Elasticsearch\Documents\Facet $facet
	 * @param \Change\Collection\CollectionInterface $indexMappings
	 * @return array
	 */
	protected function getFacetData(\Rbs\Elasticsearch\Documents\Facet $facet, \Change\Collection\CollectionInterface $indexMappings)
	{
		$category = $facet->getIndexMapping();
		$item = $indexMappings->getItemByValue($category);
		$data = [
			'_meta' => [
				'id' => $facet->getId(),
				'model' => $facet->getDocumentModelName()
			],
			'label' => $facet->getLabel(),
			'indexCategory' => $category,
			'indexCategoryLabel' => $item ? $item->getLabel() : $category
		];
		return $data;
	}

	/**
	 * @param \Rbs\Elasticsearch\Manager $elasticsearchManager
	 * @param \Rbs\Elasticsearch\Documents\Facet $facet
	 * @return array
	 */
	protected function extractValues($elasticsearchManager, $facet): array
	{
		$index = $elasticsearchManager->getDocumentsSearchIndex(true);
		$values = [];
		$facet->setFacets([]);
		if ($index && ($definition = $facet->getFacetDefinition()))
		{
			$query = new \Elastica\Query(new \Elastica\Query\MatchAll());
			$aggregation = $definition->getAggregation([]);
			$query->addAggregation($aggregation);
			$query->setSize(0);
			$resultSet = $index->search($query);
			$aggregationValues = $definition->formatAggregation($resultSet->getAggregations(), []);
			foreach ($aggregationValues->getValues() as $idx => $aggregationValue)
			{
				$values[$aggregationValue->getKey()] = $aggregationValue->getTitle();
			}
		}
		return $values;
	}
}