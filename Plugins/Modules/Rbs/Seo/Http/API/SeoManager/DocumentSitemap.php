<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\DocumentSitemap
 */
class DocumentSitemap
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery($inputParameters->get('model'));
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$event->setResult($this->buildArrayResult($event, ['item' => $document], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$publicationData = $document->getPublicationData();
			$sitemapData = $publicationData['sitemap'] ?? [];

			$newSitemapData = $inputParameters->get('data');
			if (array_key_exists('frequency', $newSitemapData))
			{
				if ($newSitemapData['frequency'])
				{
					$sitemapData['frequency'] = (string)$newSitemapData['frequency'];
				}
				else
				{
					unset($sitemapData['frequency']);
				}
			}

			if (array_key_exists('priority', $newSitemapData))
			{
				if ($newSitemapData['priority'])
				{
					$sitemapData['priority'] = (float)$newSitemapData['priority'];
				}
				else
				{
					unset($sitemapData['priority']);
				}
			}

			if (array_key_exists('inactiveWebsiteIds', $newSitemapData))
			{
				if ($newSitemapData['inactiveWebsiteIds'])
				{
					$sitemapData['inactiveWebsiteIds'] = $newSitemapData['inactiveWebsiteIds'];
				}
				else
				{
					unset($sitemapData['inactiveWebsiteIds']);
				}
			}

			$publicationData['sitemap'] = $sitemapData;
			$document->setPublicationData($publicationData);
			$document->save();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_saving_document_sitemap', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}
}