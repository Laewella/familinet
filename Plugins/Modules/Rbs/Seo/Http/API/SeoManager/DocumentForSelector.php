<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\DocumentForSelector
 */
class DocumentForSelector
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function search($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$searchString = $inputParameters->get('search');
		if (!$searchString)
		{
			return;
		}

		$model = $event->getApplicationServices()->getModelManager()->getModelByName($inputParameters->get('model'));
		if (!$model)
		{
			return;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$query = $genericServices->getAdminManager()->getSearchDocumentQuery($model->getName(), $searchString);
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		$event->setResult($this->buildArrayResult($event, $value, $this->getModels($event, $inputParameters)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function load($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$ids = $inputParameters->get('documentIds');
		if (!$ids)
		{
			return;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$documents = [];
		foreach ($ids as $id)
		{
			$document = $documentManager->getDocumentInstance($id);
			if ($document)
			{
				$documents[] = $document;
			}
		}

		$event->setResult($this->buildArrayResult($event, ['items' => $documents], $this->getModels($event, $inputParameters)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Zend\Stdlib\Parameters $inputParameters
	 * @return \Rbs\Seo\Http\API\Models
	 */
	protected function getModels($event, $inputParameters)
	{
		$models = new \Rbs\Seo\Http\API\Models($event);
		$context = $inputParameters->get('urlContext');
		if (is_array($context) && isset($context['websiteId'], $context['LCID']))
		{
			$website = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($context['websiteId']);
			$LCID = $context['LCID'];
			if ($website instanceof \Rbs\Website\Documents\Website && $LCID)
			{
				$urlManager = $website->getUrlManager($LCID);
				if (isset($context['sectionId']))
				{
					$section = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($context['sectionId']);
					if ($section instanceof \Change\Presentation\Interfaces\Section)
					{
						$urlManager->setSection($section);
					}
				}
				$models->setWebsiteUrlManager($urlManager);
			}
		}
		return $models;
	}
}