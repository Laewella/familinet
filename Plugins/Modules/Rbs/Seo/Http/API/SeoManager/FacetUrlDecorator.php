<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\FacetUrlDecorator
 */
class FacetUrlDecorator
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('facetUrlDecoratorId'));
		if (!($document instanceof \Rbs\Seo\Documents\FacetUrlDecorator))
		{
			return;
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($document, [
			'typologyId' => $documentManager->getTypologyIdByDocument($document),
			'attributes' => $documentManager->getAttributeValues($document)->getValues()
		]);

		$event->setResult($this->buildArrayResult($event, ['item' => $document], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('facetUrlDecoratorId'));
		if (!($document instanceof \Rbs\Seo\Documents\FacetUrlDecorator))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$data = $inputParameters->get('document');
			$typologyId = $data['typologyId'] ?? 0;
			if (!$typologyId)
			{
				$documentManager->saveAttributeValues($document, null, null);
			}
			else
			{
				/** @var \Rbs\Generic\Documents\Typology $typology */
				$typology = $documentManager->getDocumentInstance($typologyId, 'Rbs_Generic_Typology');
				$compatibleModels = array_merge($document->getDocumentModel()->getAncestorsNames(), [$document->getDocumentModelName()]);
				if (!$typology || !in_array($typology->getModelName(), $compatibleModels))
				{
					throw new \RuntimeException('Invalid typology: ' . $typologyId);
				}
				$attributeValues = new \Change\Documents\Attributes\AttributeValues($document->getTargetLCID(), $data['attributes']);
				$documentManager->saveAttributeValues($document, new \Rbs\Generic\Attributes\Typology($typology), $attributeValues);
			}

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_saving_facet_url_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}
}