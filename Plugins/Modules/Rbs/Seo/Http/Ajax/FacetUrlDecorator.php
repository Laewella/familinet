<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\Ajax;

/**
 * @name \Rbs\Seo\Http\Ajax\FacetUrlDecorator
 */
class FacetUrlDecorator
{
	/**
	 * Default actionPath: Rbs/Seo/FacetUrlDecorator/{decoratorId}
	 * Event params:
	 *  - website, section, page
	 *  - dataSetNames
	 *  - visualFormats
	 *  - URLFormats
	 * @param \Change\Http\Event $event
	 */
	public function getData(\Change\Http\Event $event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();

		/** @var $product \Rbs\Seo\Documents\FacetUrlDecorator */
		$decorator = $documentManager->getDocumentInstance($event->getParam('decoratorId'), 'Rbs_Seo_FacetUrlDecorator');
		if ($decorator)
		{
			$event->setParam('detailed', true);
			$context = $event->paramsToArray();
			$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/Seo/FacetUrlDecorator', $decorator->getAJAXData($context));
			$event->setResult($result);
		}
	}
}