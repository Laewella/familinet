<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\UI;

/**
 * @name \Rbs\Seo\Http\UI\Resources
 */
class Resources extends \Rbs\Ua\Http\UI\Resources
{
	/**
	 * @param string $applicationName
	 */
	public function __construct($applicationName)
	{
		$this->applicationInfo = new \Rbs\Ua\ApplicationInfo($applicationName, 'Rbs_Seo_' . $applicationName);
	}

	/**
	 * @return string
	 */
	protected function getModule()
	{
		return 'Seo';
	}

	/**
	 * @return string[]
	 */
	protected function getRestApiFileNames()
	{
		return ['rest.' . $this->getApplicationInfo()->getName() . '.json'];
	}
}