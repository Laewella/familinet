<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\Rest\Result;

/**
 * @deprecated since 1.8 without replacement.
 * @name \Rbs\Seo\Http\Rest\Result\PathRuleResult
 */
class PathRuleResult extends \Change\Http\Result
{
	/**
	 * @deprecated since 1.8 without replacement.
	 * @var \Change\Http\Web\PathRule
	 */
	protected $pathRule;

	/**
	 * @deprecated since 1.8 without replacement.
	 * @param \Change\Http\Web\PathRule
	 */
	public function __construct(\Change\Http\Web\PathRule $pathRule = null)
	{
		trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
		parent::__construct();
		$this->pathRule = $pathRule;
	}

	/**
	 * @deprecated since 1.8 without replacement.
	 * @param \Change\Http\Web\PathRule $pathRule
	 * @return $this
	 */
	public function setPathRule($pathRule)
	{
		$this->pathRule = $pathRule;
		return $this;
	}

	/**
	 * @deprecated since 1.8 without replacement.
	 * @return \Change\Http\Web\PathRule
	 */
	public function getPathRule()
	{
		return $this->pathRule;
	}

	/**
	 * @deprecated since 1.8 without replacement.
	 * @return array
	 */
	public function toArray()
	{
		$array = [];
		if ($this->pathRule instanceof \Change\Http\Web\PathRule)
		{
			$rule['rule_id'] = $this->pathRule->getRuleId();
			$rule['website_id'] = $this->pathRule->getWebsiteId();
			$rule['lcid'] = $this->pathRule->getLCID();
			$rule['relative_path'] = $this->pathRule->getRelativePath();
			$rule['document_id'] = $this->pathRule->getDocumentId();
			$rule['section_id'] = $this->pathRule->getSectionId();
			$rule['http_status'] = $this->pathRule->getHttpStatus();
			$rule['query'] = $this->pathRule->getQuery();
			$array['rule'] = $rule;
		}
		return $array;
	}
}