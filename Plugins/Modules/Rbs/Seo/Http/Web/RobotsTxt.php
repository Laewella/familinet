<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\Web;

/**
 * @name \Rbs\Seo\Http\Web\RobotsTxt
 */
class RobotsTxt
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$uri = new \Zend\Uri\Http($event->getRequest()->getUri());
		$application = $event->getApplication();
		$workspace = $application->getWorkspace();
		$content = 'User-agent: * ' . PHP_EOL . 'Disallow: /admin.php/' . PHP_EOL . 'Disallow: /rest.php/' . PHP_EOL .
			'Disallow: /ua.php/' . PHP_EOL . 'Disallow: /uaRest.php/' . PHP_EOL;

		$defaultRobots = $workspace->appPath('Config', 'robots.txt');
		if (is_file($defaultRobots) && is_readable($defaultRobots))
		{
			$content .= file_get_contents($defaultRobots) . PHP_EOL;
		}

		//Check sitemap
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$sitemapGenerator = new \Rbs\Seo\Std\SitemapGenerator();
		$websiteIds = $sitemapGenerator->getWebsiteIds($uri->getHost(), $documentManager);
		if ($websiteIds)
		{
			$uri->setPath($sitemapGenerator->getIndexPath());
			$content .= 'Sitemap: ' . $uri->normalize()->toString() . PHP_EOL;
		}

		$result = new \Change\Http\Web\Result\RawResult(\Zend\Http\Response::STATUS_CODE_200);
		$result->setContent($content);
		$result->getHeaders()->addHeaderLine('Content-type', 'text/plain');
		$result->setHeaders($result->getHeaders());

		$event->setResult($result);
	}
}