<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Std;

/**
 * @name \Rbs\Seo\Std\PublicationMeta
 */
class PublicationMeta implements \Change\Presentation\Interfaces\PublicationMeta
{
	/**
	 * @var string|null
	 */
	protected $type;

	/**
	 * @var string|null
	 */
	protected $name;

	/**
	 * @var mixed
	 */
	protected $value;

	/**
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		if (isset($data['type']))
		{
			$this->setType($data['type']);
		}
		if (isset($data['name']))
		{
			$this->setName($data['name']);
		}
		if (isset($data['value']))
		{
			$this->setValue($data['value']);
		}
	}

	/**
	 * @return string|null
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string|null $type
	 * @return $this
	 */
	public function setType($type)
	{
		$this->type = $type === null ? null : (string)$type;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string|null $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name === null ? null : (string)$name;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return [
			'type' => $this->type,
			'name' => $this->name,
			'value' => $this->value
		];
	}
}