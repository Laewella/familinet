<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Std;

/**
 * @name \Rbs\Seo\Std\SitemapGenerator
 */
class SitemapGenerator
{
	/**
	 * @return string
	 */
	public function getIndexPath()
	{
		return '/Assets/Rbs/Seo/sitemap-index.xml';
	}

	/**
	 * @param string $host
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return array
	 */
	public function getWebsiteIds($host, \Change\Documents\DocumentManager $documentManager)
	{
		$websiteIds = [];
		$q = $documentManager->getNewQuery('Rbs_Website_Website');
		$q->andPredicates($q->eq('sitemapGeneration', true));

		/** @var \Rbs\Website\Documents\Website $website */
		foreach ($q->getDocuments() as $website)
		{
			foreach ($website->getLCIDArray() as $LCID)
			{
				$documentManager->pushLCID($LCID);
				/** @noinspection DisconnectedForeachInstructionInspection */
				$hostName = $website->getCurrentLocalization()->getHostName();
				if ($hostName && $hostName == $host)
				{
					$websiteIds[] = [$website->getId(), $LCID];
				}
				/** @noinspection DisconnectedForeachInstructionInspection */
				$documentManager->popLCID();
			}
		}
		return $websiteIds;
	}

	/**
	 * @param array $sitemapIndex
	 * @return \DOMDocument
	 */
	public function generateSitemapIndex(array $sitemapIndex)
	{
		$xml = new \DOMDocument('1.0', 'UTF-8');
		$xml->formatOutput = true;
		$sitemapIndexElement = $xml->createElement('sitemapindex');
		$sitemapIndexElement->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		$xml->appendChild($sitemapIndexElement);

		foreach ($sitemapIndex as $sitemapInfo)
		{
			/** @noinspection DisconnectedForeachInstructionInspection */
			$sitemap = $xml->createElement('sitemap');
			$loc = $xml->createElement('loc', $sitemapInfo['loc']);
			$sitemap->appendChild($loc);
			$lastMod = $xml->createElement('lastmod', $sitemapInfo['lastmod']);
			$sitemap->appendChild($lastMod);

			$sitemapIndexElement->appendChild($sitemap);
		}
		return $xml;
	}

	/**
	 * @param string $baseDir
	 * @param $websiteId
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return string[]
	 */
	public function generateSitemap($baseDir, $websiteId, \Change\Documents\DocumentManager $documentManager)
	{
		$fileNames = [];
		$baseDir = rtrim($baseDir, '/') . '/';
		$modelManager = $documentManager->getModelManager();
		$modelNames = $modelManager->getModelsNames();
		$LCID = $documentManager->getLCID();
		$total = 0;
		$chunkSize = 50;

		foreach ($modelNames as $modelName)
		{
			$model = $modelManager->getModelByName($modelName);
			if (!$model->isPublishable() || $model->isAbstract())
			{
				continue;
			}

			/** @noinspection DisconnectedForeachInstructionInspection */
			$dqb = $documentManager->getNewQuery('Rbs_Seo_ModelConfiguration');
			$dqb->andPredicates($dqb->eq('modelName', $modelName));
			/** @var \Rbs\Seo\Documents\ModelConfiguration $modelConfiguration */
			$modelConfiguration = $dqb->getFirstDocument();
			$defaultChangeFrequency = $modelConfiguration->getSitemapDefaultChangeFrequency();
			$defaultPriority = $modelConfiguration->getSitemapDefaultPriority();
			unset($modelConfiguration);

			$size = 0;
			$items = 0;
			$maxId = 0;
			$fileCount = 1;

			/** @var \DOMDocument $xml */
			/** @var \DOMElement $urlSetElement */
			list($xml, $urlSetElement) = $this->getNewUrlSetDocument();

			while (true)
			{
				/** @var \Rbs\Website\Documents\Website $website */
				$website = $documentManager->getDocumentInstance($websiteId);
				$urlManager = $website->getUrlManager($LCID);
				$pathRuleManager = $urlManager->getPathRuleManager();

				$query = $documentManager->getNewQuery($modelName);
				$query->andPredicates($query->gt('id', $maxId));
				$query->addOrder('id');
				$documents = $query->getDocuments(0, $chunkSize)->preLoad($modelName);

				/* @var \Change\Documents\Interfaces\Publishable|\Change\Documents\AbstractDocument $document */
				foreach ($documents as $document)
				{
					$maxId = $document->getId();
					if (!$document->getCanonicalSection($website) || !$document->published())
					{
						continue;
					}

					$publicationSitemap = $document->getPublicationSitemap();
					if ($publicationSitemap)
					{
						if (!$publicationSitemap->isActiveForWebsite($websiteId))
						{
							continue;
						}
						$frequency = $publicationSitemap->getFrequency() ?: $defaultChangeFrequency;
						$priority = $publicationSitemap->getPriority() ?: $defaultPriority;
					}
					else
					{
						$frequency = $defaultChangeFrequency;
						$priority = $defaultPriority;
					}

					$url = $xml->createElement('url');
					$documentUrl = $urlManager->getCanonicalByDocument($document)->normalize()->toString();
					$url->appendChild($xml->createElement('loc', $documentUrl));
					/* @var $targetModificationDate \DateTime */
					$targetModificationDate = $model->getProperty('modificationDate')->getValue($document);
					$url->appendChild($xml->createElement('lastmod', $targetModificationDate->format(\DateTime::W3C)));
					$url->appendChild($xml->createElement('changefreq', $frequency));
					$url->appendChild($xml->createElement('priority', $priority));

					$urlSetElement->appendChild($url);
					$items++;
					$total++;
					$size += 200 + strlen($documentUrl);

					// Create a sitemap for each 40000 urls or 10MB.
					if ($items > 40000 || $size > 10000000)
					{
						$fileName = $modelName . '.' . $fileCount . '.xml';
						$xml->save($baseDir . $fileName);
						$fileNames[] = $fileName;
						$fileCount++;

						$items = 0;
						$size = 0;
						unset($xml, $urlSetElement);
						list($xml, $urlSetElement) = $this->getNewUrlSetDocument();
					}

					// Handle URLs with queries.
					$rules = $pathRuleManager->findPathRules($websiteId, $LCID, $document->getId(), 0);
					foreach ($rules as $rule)
					{
						if ($rule->getHttpStatus() !== 200 || !$rule->getQuery())
						{
							continue;
						}

						$url =$xml->createElement('url');
						$thisUrl = $urlManager->getBaseUri()->normalize()->toString() . $rule->getRelativePath();
						$url->appendChild($xml->createElement('loc', $thisUrl));
						$url->appendChild($xml->createElement('lastmod', $targetModificationDate->format(\DateTime::W3C)));
						$url->appendChild($xml->createElement('changefreq', $frequency));
						$url->appendChild($xml->createElement('priority', $priority));
						$urlSetElement->appendChild($url);
						$items++;
						$total++;
						$size += 200 + strlen($thisUrl);

						// Create a sitemap for each 40000 urls or 10MB.
						if ($items > 40000 || $size > 10000000)
						{
							$fileName = $modelName . '.' . $fileCount . '.xml';
							$xml->save($baseDir . $fileName);
							$fileNames[] = $fileName;
							$fileCount++;

							$items = 0;
							$size = 0;
							unset($xml, $urlSetElement);
							list($xml, $urlSetElement) = $this->getNewUrlSetDocument();
						}
					}
				}

				if (count($documents) !== $chunkSize)
				{
					break;
				}
			}

			unset($urlManager, $website);
			/** @noinspection DisconnectedForeachInstructionInspection */
			$documentManager->reset();

			if ($items)
			{
				$fileName = $modelName . '.' . $fileCount . '.xml';
				$xml->save($baseDir . $fileName);
				$fileNames[] = $fileName;
			}
		}

		return $fileNames;
	}

	/**
	 * @return array
	 */
	protected function getNewUrlSetDocument()
	{
		$xml = new \DOMDocument('1.0', 'UTF-8');
		$xml->formatOutput = true;
		$urlSetElement = $xml->createElement('urlset');
		$urlSetElement->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		$xml->appendChild($urlSetElement);
		return [$xml, $urlSetElement];
	}
}