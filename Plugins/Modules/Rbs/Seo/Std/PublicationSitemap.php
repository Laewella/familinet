<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Std;

/**
 * @name \Rbs\Seo\Std\PublicationSitemap
 */
class PublicationSitemap implements \Change\Presentation\Interfaces\PublicationSitemap
{
	/**
	 * @var string|null
	 */
	protected $frequency;

	/**
	 * @var float|null
	 */
	protected $priority;

	/**
	 * @var integer[]
	 */
	protected $inactiveWebsiteIds = [];

	/**
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		if (isset($data['frequency']))
		{
			$this->setFrequency($data['frequency']);
		}
		if (isset($data['priority']))
		{
			$this->setPriority($data['priority']);
		}
		if (isset($data['inactiveWebsiteIds']) && is_array($data['inactiveWebsiteIds']))
		{
			$this->setInactiveWebsiteIds($data['inactiveWebsiteIds']);
		}
	}

	/**
	 * @return string|null
	 */
	public function getFrequency()
	{
		return $this->frequency;
	}

	/**
	 * @param string|null $frequency
	 * @return $this
	 */
	public function setFrequency($frequency)
	{
		$this->frequency = $frequency === null ? null : (string)$frequency;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * @param float|null $priority
	 * @return $this
	 */
	public function setPriority($priority)
	{
		$this->priority = $priority === null ? null : (float)$priority;
		return $this;
	}

	/**
	 * @param integer|\Change\Presentation\Interfaces\Website $website
	 * @return boolean
	 */
	public function isActiveForWebsite($website)
	{
		$websiteId = $website instanceof \Change\Presentation\Interfaces\Website ? $website->getId() : $website;
		return !$this->inactiveWebsiteIds || !in_array($websiteId, $this->inactiveWebsiteIds);
	}

	/**
	 * @return integer[]
	 */
	public function getInactiveWebsiteIds()
	{
		return $this->inactiveWebsiteIds;
	}

	/**
	 * @param integer[] $inactiveWebsiteIds
	 * @return $this
	 */
	public function setInactiveWebsiteIds(array $inactiveWebsiteIds)
	{
		$this->inactiveWebsiteIds = $inactiveWebsiteIds;
		return $this;
	}

	/**
	 * @param integer $websiteId
	 * @return $this
	 */
	public function addInactiveWebsiteIds($websiteId)
	{
		if (!in_array($websiteId, $this->inactiveWebsiteIds))
		{
			$this->inactiveWebsiteIds[] = $websiteId;
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return [
			'frequency' => $this->frequency,
			'priority' => $this->priority,
			'inactiveWebsiteIds' => $this->inactiveWebsiteIds
		];
	}
}