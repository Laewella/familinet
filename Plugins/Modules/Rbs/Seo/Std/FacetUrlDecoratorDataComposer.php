<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Std;

/**
 * @name \Rbs\Seo\Std\FacetUrlDecoratorDataComposer
 */
class FacetUrlDecoratorDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Rbs\Seo\Documents\FacetUrlDecorator
	 */
	protected $decorator;

	/**
	 * @var \Rbs\Seo\SeoManager
	 */
	protected $seoManager;

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function __construct(\Change\Documents\Events\Event $event)
	{
		$this->decorator = $event->getDocument();

		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$applicationServices = $event->getApplicationServices();
		$this->setServices($applicationServices);

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$this->seoManager = $genericServices->getSeoManager();
	}

	protected function generateDataSets()
	{
		if (!$this->decorator)
		{
			$this->dataSets = [];
			return;
		}

		$this->dataSets = [
			'common' => [
				'id' => $this->decorator->getId(),
				'facetValues' => []
			]
		];

		foreach ($this->decorator->getFacetValues() as $facetValue)
		{
			$facetId = $facetValue->getFacetId();
			/** @var \Rbs\Elasticsearch\Documents\Facet $facet */
			$facet = $this->documentManager->getDocumentInstance($facetId, 'Rbs_Elasticsearch_Facet');
			$facetTitle = $facet ? $facet->getCurrentLocalization()->getTitle() : null;

			$this->dataSets['common']['facetValues'] = [
				'facetId' => $facetId,
				'facetTitle' => $facetTitle,
				'value' => $facetValue->getValue(),
				'valueTitle' => $this->seoManager->getFacetValueTitle($facetValue)
			];
		}

		$this->generateTypologyDataSet($this->decorator);
	}
}