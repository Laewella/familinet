<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Tag\Admin;

/**
 * @name \Rbs\Tag\Admin\GetModelTwigAttributes
 */
class GetModelTwigAttributes
{
	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Events\Event $event)
	{
		$view = $event->getParam('view');

		$adminManager = $event->getTarget();
		if ($adminManager instanceof \Rbs\Admin\AdminManager)
		{
			if ($view == 'edit' || $view == 'translate')
			{
				$attributes = $event->getParam('attributes');

				$attributes['asideDirectives'][] = [
					'name' => 'rbs-aside-tag-selector',
					'attributes' => [['name' => 'document', 'value' => 'document']]
				];

				$event->setParam('attributes', $attributes);
			}
		}
	}
}