<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Tag\Db\Query;

class HasTag extends \Change\Db\Query\Expressions\AbstractExpression implements \Change\Db\Query\Predicates\InterfacePredicate
{
	/**
	 * @var \Change\Db\Query\Expressions\Column
	 */
	protected $documentIdColumn;

	/**
	 * @var \Change\Db\Query\Expressions\AbstractExpression
	 */
	protected $tagId;

	/**
	 * @param \Change\Db\Query\Expressions\AbstractExpression $tagId
	 */
	public function setTagId($tagId)
	{
		$this->tagId = $tagId;
	}

	/**
	 * @return \Change\Db\Query\Expressions\AbstractExpression
	 */
	public function getTagId()
	{
		return $this->tagId;
	}

	/**
	 * @param \Change\Db\Query\Expressions\Column $documentIdColumn
	 */
	public function setDocumentIdColumn($documentIdColumn)
	{
		$this->documentIdColumn = $documentIdColumn;
	}

	/**
	 * @return \Change\Db\Query\Expressions\Column
	 */
	public function getDocumentIdColumn()
	{
		return $this->documentIdColumn;
	}

	/**
	 * @api
	 * @throws \RuntimeException
	 */
	public function checkCompile()
	{
		if (!($this->getDocumentIdColumn() instanceof \Change\Db\Query\Expressions\AbstractExpression))
		{
			throw new \RuntimeException('Invalid documentId column Expression', 42030);
		}

		if (!($this->getTagId() instanceof \Change\Db\Query\Expressions\AbstractExpression))
		{
			throw new \RuntimeException('Invalid TagId Expression', 42030);
		}
	}

	/**
	 * @api
	 * @throws \RuntimeException
	 * @return string
	 */
	public function toSQL92String()
	{
		$this->checkCompile();
		return $this->buildPredicate()->toSQL92String();
	}

	/**
	 * @return \Change\Db\Query\Predicates\InterfacePredicate
	 */
	protected function buildPredicate()
	{
		$sq = new \Change\Db\Query\SelectQuery();
		$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
		$fromClause = new \Change\Db\Query\Clauses\FromClause();
		$fromClause->setTableExpression(new \Change\Db\Query\Expressions\Table('rbs_tag_document'));
		$p = new \Change\Db\Query\Expressions\Parentheses(new \Change\Db\Query\Expressions\Column(
			new \Change\Db\Query\Expressions\Identifier(['tag_id'])));
		$joinExpr = new \Change\Db\Query\Expressions\UnaryOperation($p, 'USING');
		$join = new  \Change\Db\Query\Expressions\Join(new \Change\Db\Query\Expressions\Table('rbs_tag_search'),
			\Change\Db\Query\Expressions\Join::INNER_JOIN, $joinExpr);

		$fromClause->addJoin($join);
		$sq->setFromClause($fromClause);

		$docEq = new \Change\Db\Query\Predicates\BinaryPredicate(new \Change\Db\Query\Expressions\Column(
			new \Change\Db\Query\Expressions\Identifier(['doc_id'])), $this->getDocumentIdColumn(),
			\Change\Db\Query\Predicates\BinaryPredicate::EQUAL);
		$tagEq = new \Change\Db\Query\Predicates\BinaryPredicate(new \Change\Db\Query\Expressions\Column(
			new \Change\Db\Query\Expressions\Identifier(['search_tag_id'])), $this->getTagId(), \Change\Db\Query\Predicates\BinaryPredicate::EQUAL);
		$and = new \Change\Db\Query\Predicates\Conjunction($docEq, $tagEq);
		$where = new \Change\Db\Query\Clauses\WhereClause($and);
		$sq->setWhereClause($where);
		return new \Change\Db\Query\Predicates\Exists(new \Change\Db\Query\Expressions\SubQuery($sq));
	}

	/**
	 * @param \Change\Db\DbProvider $provider
	 * @throws \RuntimeException
	 * @return string
	 */
	public function toSQLString($provider)
	{
		$this->checkCompile();
		return $provider->buildSQLFragment($this->buildPredicate());
	}

	/**
	 * @param array $predicateJSON
	 * @param \Change\Documents\Query\JSONDecoder $JSONDecoder
	 * @param \Change\Documents\Query\PredicateBuilder $predicateBuilder
	 * @return \Change\Db\Query\Predicates\Exists|null
	 */
	public function populate(array $predicateJSON, \Change\Documents\Query\JSONDecoder $JSONDecoder, \Change\Documents\Query\PredicateBuilder $predicateBuilder)
	{
		$this->setDocumentIdColumn($predicateBuilder->columnProperty('id'));
		$this->setTagId($JSONDecoder->getDocumentQuery()->getValueAsParameter($predicateJSON['tag'],
			\Change\Documents\Property::TYPE_INTEGER));
		return $this->buildPredicate();
	}
}