<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

/**
 * @name \Rbs\Website\Documents\StaticPage
 */
class StaticPage extends \Compilation\Rbs\Website\Documents\StaticPage
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, [$this, 'onValidateDisplayDocument'], 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, [$this, 'onValidateDisplayDocument'], 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onValidateSection($event); }, 5);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DISPLAY_PAGE, [$this, 'onDocumentDisplayPage'], 10);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			/* @var $page StaticPage */
			$page = $event->getDocument();
			if ($page->getSection())
			{
				$tm = $event->getApplicationServices()->getTreeManager();
				$parentNode = $tm->getNodeByDocument($page->getSection());
				if ($parentNode)
				{
					$tm->insertNode($parentNode, $page);
				}
			}
		};
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, $callback);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			/* @var $page StaticPage */
			if (in_array('section', $event->getParam('modifiedPropertyNames', [])))
			{
				$page = $event->getDocument();
				$tm = $event->getApplicationServices()->getTreeManager();
				$tm->deleteDocumentNode($page);
				if ($page->getSection())
				{
					$parentNode = $tm->getNodeByDocument($page->getSection());
					if ($parentNode)
					{
						$tm->insertNode($parentNode, $page);
					}
				}
			}
		};
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, $callback);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onValidateDisplayDocument(\Change\Documents\Events\Event $event)
	{
		/** @var $staticPage StaticPage */
		$staticPage = $event->getDocument();
		if ($staticPage->isPropertyModified('displayDocument') && ($displayDocument = $staticPage->getDisplayDocument()) !== null)
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$propertiesErrors = [];
			if ($displayDocument instanceof Page || $displayDocument instanceof Section
				|| !($displayDocument instanceof \Change\Documents\Interfaces\Publishable)
			)
			{
				$propertiesErrors['displayDocument'][] =
					$i18nManager->trans('m.rbs.website.admin.displaydocument_invalid_type', ['ucf']);
			}
			else
			{
				$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery($staticPage->getDocumentModel());
				$query->andPredicates($query->neq('id', $staticPage->getId()), $query->eq('displayDocument', $displayDocument));
				/** @var $duplicates StaticPage[]|\Change\Documents\DocumentCollection */

				$duplicates = $query->getDocuments();
				if ($duplicates->count())
				{
					$website = ($staticPage->getSection() ? $staticPage->getSection()->getWebsite() : null);
					foreach ($duplicates as $duplicateStaticPage)
					{
						$duplicateWebsite =
							($duplicateStaticPage->getSection() ? $duplicateStaticPage->getSection()->getWebsite() : null);
						if ($website === $duplicateWebsite)
						{
							$propertiesErrors['displayDocument'][] =
								$i18nManager->trans('m.rbs.website.admin.displaydocument_duplicate', ['ucf']);
							break;
						}
					}
				}
			}

			if (count($propertiesErrors))
			{
				$errors = $event->getParam('propertiesErrors');
				if (is_array($errors))
				{
					$event->setParam('propertiesErrors', array_merge($propertiesErrors, $errors));
				}
				else
				{
					$event->setParam('propertiesErrors', $propertiesErrors);
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onValidateSection(\Change\Documents\Events\Event $event)
	{
		if ($this->isPropertyModified('section'))
		{
			$newSection = $this->getSection();
			$oldSection = $this->getSectionOldValue();
			if ($newSection && $oldSection && $newSection->getWebsite() !== $oldSection->getWebsite())
			{
				$i18nManager = $event->getApplicationServices()->getI18nManager();
				$propertiesErrors['section'][] = $i18nManager->trans('m.rbs.website.admin.section_must_be_same_website', ['ucf']);
				$errors = $event->getParam('propertiesErrors');
				if (is_array($errors))
				{
					$event->setParam('propertiesErrors', array_merge($propertiesErrors, $errors));
				}
				else
				{
					$event->setParam('propertiesErrors', $propertiesErrors);
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDocumentDisplayPage(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if ($document instanceof \Change\Presentation\Interfaces\Page)
		{
			$event->setParam('page', $document);
			$event->stopPropagation();
		}
	}

	/**
	 * @return \Change\Presentation\Interfaces\Section[]
	 */
	public function getPublicationSections()
	{
		$section = $this->getSection();
		return $section ? [$section] : [];
	}

	/**
	 * @param \Change\Documents\AbstractDocument $publicationSections
	 * @return $this
	 */
	public function setPublicationSections($publicationSections)
	{
		return $this;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		/** @var $staticPage StaticPage */
		$staticPage = $event->getDocument();
		$website = null;
		$section = $staticPage->getSection();
		if ($section instanceof Topic)
		{
			$website = $section->getWebsite();
		}
		elseif ($section instanceof Website)
		{
			$website = $section;
		}

		$restResult = $event->getParam('restResult');

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$documentLink = $restResult;

			$um = $documentLink->getUrlManager();
			$vc = new \Change\Http\Rest\V1\ValueConverter($um, $event->getApplicationServices()->getDocumentManager());
			$documentLink->setProperty('website', $vc->toRestValue($website, \Change\Documents\Property::TYPE_DOCUMENT));

			$extraColumn = $event->getParam('extraColumn');
			if (in_array('functions', $extraColumn))
			{
				$functions = $this->getPageFunctionCodes($event, $staticPage, $documentLink->getDocument());
				$documentLink->setProperty('functions', $functions);
			}
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$documentResult = $restResult;
			$section = $staticPage->getSection();

			$website = null;
			if ($section instanceof Topic)
			{
				$website = $section->getWebsite();
			}
			elseif ($section instanceof Website)
			{
				$website = $section;
			}

			$functions = $this->getPageFunctionCodes($event, $staticPage);
			$documentResult->setProperty('functions', $functions);

			$um = $documentResult->getUrlManager();

			// Critical page if
			if (in_array('Rbs_Website_Section', $functions, true))
			{
				$documentResult->setProperty('_critical', true);
			}

			$vc = new \Change\Http\Rest\V1\ValueConverter($um, $event->getApplicationServices()->getDocumentManager());
			$documentResult->setProperty('website', $vc->toRestValue($website, \Change\Documents\Property::TYPE_DOCUMENT));
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @param \Rbs\Website\Documents\StaticPage $staticPage
	 * @param \Change\Http\Rest\V1\Resources\DocumentLink|null $pageFromLink
	 * @return array
	 */
	protected function getPageFunctionCodes(\Change\Documents\Events\Event $event, $staticPage, $pageFromLink = null)
	{
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Website_SectionPageFunction');
		$query->andPredicates(
			$query->eq('page', $pageFromLink ?? $staticPage),
			$query->eq('section',
				$event->getApplicationServices()->getTreeManager()->getNodeByDocument($staticPage)->getParentId())
		);
		$functions = [];
		$funcDocs = $query->getDocuments();
		foreach ($funcDocs as $func)
		{
			/* @var $func \Rbs\Website\Documents\SectionPageFunction */
			$functions[] = $func->getFunctionCode();
		}

		return $functions;
	}
}