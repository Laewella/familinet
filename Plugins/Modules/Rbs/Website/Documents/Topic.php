<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

use Change\Documents\Events\Event;
use Zend\Http\Response as HttpResponse;

/**
 * @name \Rbs\Website\Documents\Topic
 */
class Topic extends \Compilation\Rbs\Website\Documents\Topic
{
	/**
	 * @var \Rbs\Website\Documents\Section
	 */
	protected $section;

	/**
	 * @return \Change\Presentation\Interfaces\Section[]
	 */
	public function getPublicationSections()
	{
		return $this->isNew() ? [] : [$this];
	}

	/**
	 * @param \Rbs\Website\Documents\Section $section
	 * @return $this
	 */
	public function setSection($section)
	{
		$this->section = $section;
		return $this;
	}

	/**
	 * @return \Rbs\Website\Documents\Section
	 */
	public function getSection()
	{
		return $this->section;
	}

	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function($event) {$this->onInitializeSectionTree($event);}, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->onInitializeSectionTree($event); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onValidateSection($event); }, 5);
	}

	protected function onCreate()
	{
		$section = $this->getSection();
		if ($section instanceof Topic)
		{
			$this->setWebsite($section->getWebsite());
		}
		elseif ($section instanceof Website)
		{
			$this->setWebsite($section);
		}
	}

	protected function onUpdate()
	{
		$section = $this->getSection();
		if ($section instanceof Topic)
		{
			$this->setWebsite($section->getWebsite());
		}
		elseif ($section instanceof Website)
		{
			$this->setWebsite($section);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onValidateSection(\Change\Documents\Events\Event $event)
	{
		if ($this->isPropertyModified('website'))
		{
			$newWebsiteId = $this->getWebsiteId();
			$oldWebsiteId = $this->getWebsiteOldValueId();
			if ($newWebsiteId && $oldWebsiteId && $newWebsiteId !== $oldWebsiteId)
			{
				$i18nManager = $event->getApplicationServices()->getI18nManager();
				$propertiesErrors['website'][] = $i18nManager->trans('m.rbs.website.admin.website_cant_be_modified', ['ucf']);
				$errors = $event->getParam('propertiesErrors');
				if (is_array($errors))
				{
					$event->setParam('propertiesErrors', array_merge($propertiesErrors, $errors));
				}
				else
				{
					$event->setParam('propertiesErrors', $propertiesErrors);
				}
			}
		}
	}

	/**
	 * @param Event $event
	 */
	protected function onInitializeSectionTree(Event $event)
	{
		$treeManager = $event->getApplicationServices()->getTreeManager();
		$topicNode = $treeManager->getNodeByDocument($this);
		if (!$topicNode)
		{
			if ($this->getSection())
			{
				$parentNode = $treeManager->getNodeByDocument($this->getSection());
				if ($parentNode)
				{
					$treeManager->insertNode($parentNode, $this);
				}
			}
			elseif ($this->getWebsite())
			{
				$parentNode = $treeManager->getNodeByDocument($this->getWebsite());
				if ($parentNode)
				{
					$treeManager->insertNode($parentNode, $this);
				}
			}
		}
		elseif ($this->getSection())
		{
			$parentSection = $this->getSection();
			if ($topicNode->getParentId() != $parentSection->getId())
			{
				$parentNode = $treeManager->getNodeByDocument($parentSection);
				if ($parentNode)
				{
					$event->getApplicationServices()->getJobManager()->createNewJob('Rbs_Website_TopicMoved',
						['topicId' => $this->getId(), 'newParentSectionId' => $parentSection->getId()], null, false);

					$treeManager->moveNode($topicNode, $parentNode, null);

					$modifiedPropertyNames = $event->getParam('modifiedPropertyNames');
					if (is_array($modifiedPropertyNames))
					{
						if (!in_array('publicationSections', $modifiedPropertyNames))
						{
							$modifiedPropertyNames[] = 'publicationSections';
						}
					}
					else
					{
						$modifiedPropertyNames = ['publicationSections'];
					}
					$event->setParam('modifiedPropertyNames', $modifiedPropertyNames);
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		$restResult = $event->getParam('restResult');
		/** @var $document Topic */
		$document = $event->getDocument();
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$section = null;
			$tm = $event->getApplicationServices()->getTreeManager();
			$topicNode = $tm->getNodeByDocument($document);
			if ($topicNode)
			{
				$section = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($topicNode->getParentId());
				if (!($section instanceof Section))
				{
					$section = null;
				}
			}
			$vc = new \Change\Http\Rest\V1\ValueConverter($restResult->getUrlManager(), $event->getApplicationServices()->getDocumentManager());
			$restResult->setProperty('section', $vc->toRestValue($section, \Change\Documents\Property::TYPE_DOCUMENT));
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink) {
			$vc = new \Change\Http\Rest\V1\ValueConverter($restResult->getUrlManager(), $event->getApplicationServices()->getDocumentManager());
			$restResult->setProperty('website', $vc->toRestValue($document->getWebsite(), \Change\Documents\Property::TYPE_DOCUMENT));
		}
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return bool
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'section')
		{
			$vc = new \Change\Http\Rest\V1\ValueConverter($event->getUrlManager(), $event->getApplicationServices()->getDocumentManager());
			$section = $vc->toPropertyValue($value, \Change\Documents\Property::TYPE_DOCUMENT);
			if ($section instanceof Section)
			{
				$this->setSection($section);
			}
			return true;
		}
		else
		{

			return parent::processRestData($name, $value, $event);
		}
	}
}