<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

/**
 * @name \Rbs\Website\Documents\SectionPageFunction
 */
class SectionPageFunction extends \Compilation\Rbs\Website\Documents\SectionPageFunction
{

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->getFunctionCode();
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		return $this;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->validateUnique($event); }, 1);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->hideLinksOnIndexPage($event); }, 1);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->hideLinksOnIndexPage($event); }, 1);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->buildPathRule($event); }, 1);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->buildPathRule($event); }, 1);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function validateUnique($event)
	{
		if ($event->getParam('propertiesErrors') !== null)
		{
			return;
		}

		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery($this->getDocumentModel());
		$query->andPredicates($query->eq('section', $this->getSection()), $query->eq('functionCode', $this->getFunctionCode()));
		if ($query->getCountDocuments())
		{
			$event->setParam('propertiesErrors',
				['functionCode' => [new \Change\I18n\PreparedKey('m.rbs.website.admin.sectionpagefunction_error_not_unique', [],
					['code' => $this->getFunctionCode()])]]);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function hideLinksOnIndexPage(\Change\Documents\Events\Event $event)
	{
		if ($this->getFunctionCode() == 'Rbs_Website_Section')
		{
			$page = $this->getPage();
			if ($page instanceof \Rbs\Website\Documents\StaticPage && !$page->getHideLinks())
			{
				$page->setHideLinks(true);
				$page->update();
			}

			//Cleanup index page cache
			$section = $this->getSection();
			if ($section)
			{
				$cacheManager = $event->getApplicationServices()->getCacheManager();
				if ($cacheManager->isValidNamespace('WebsiteResolver'))
				{
					$key = $section->getId() . '-' . $this->getFunctionCode();
					$cacheManager->removeEntry('WebsiteResolver', $key);
				}
			}
		}
	}

	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		/* @var $document SectionPageFunction */
		$document = $event->getDocument();

		/* @var $restResult \Change\Http\Rest\V1\Resources\DocumentLink|\Change\Http\Rest\V1\Resources\DocumentResult */
		$restResult->setProperty('label', $document->getLabel());
	}

	protected function buildPathRule(\Change\Documents\Events\Event $event)
	{
		if ($this->getPage() instanceof FunctionalPage && $this->getSection())
		{
			$functionCode = $this->getFunctionCode();
			$functions = $event->getApplicationServices()->getPageManager()->getFunctions();
			foreach ($functions as $function)
			{
				if ($function['code'] == $functionCode && !$function['document'])
				{
					$applicationServices = $event->getApplicationServices();
					(new \Rbs\Website\Events\PathRuleBuilder())->updatePathRuleForSectionPageFunction($this,
						$applicationServices->getDocumentManager(),
						$applicationServices->getPathRuleManager(), $event->getApplication()->getLogging());
				}
			}
		}
	}
}