<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

/**
 * @name \Rbs\Website\Documents\FunctionalPage
 */
class FunctionalPage extends \Compilation\Rbs\Website\Documents\FunctionalPage
{
	/**
	 * @var \Rbs\Website\Documents\Section
	 */
	protected $section;

	/**
	 * @return \Change\Presentation\Interfaces\Section
	 */
	public function getSection()
	{
		return $this->section;
	}

	/**
	 * @param \Change\Presentation\Interfaces\Section $section
	 * @return $this
	 */
	public function setSection($section)
	{
		$this->section = $section;
		return $this;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DISPLAY_PAGE, [$this, 'onDocumentDisplayPage'], 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, [$this, 'onUpdatePathRule'], 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDocumentDisplayPage(\Change\Documents\Events\Event $event)
	{
		if ($this != $event->getDocument())
		{
			return;
		}

		$pathRule = $event->getParam('pathRule');

		if ($pathRule instanceof \Change\Http\Web\PathRule && $pathRule->getWebsiteId() == $this->getWebsiteId())
		{
			$this->setSection($this->getWebsite());
			if ($pathRule->getSectionId())
			{
				$section = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($pathRule->getSectionId());
				if ($section instanceof Section)
				{
					$this->setSection($section);
				}
			}
			$event->setParam('page', $this);
			$event->stopPropagation();
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onUpdatePathRule(\Change\Documents\Events\Event $event)
	{
		if ($this != $event->getDocument())
		{
			return;
		}

		$modifiedPropertyNames = $event->getParam('modifiedPropertyNames');
		if (is_array($modifiedPropertyNames) && in_array('title', $modifiedPropertyNames))
		{
			$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Website_SectionPageFunction');
			$query->andPredicates($query->eq('page', $this));

			/** @var $sectionPageFunction \Rbs\Website\Documents\SectionPageFunction */
			foreach ($query->getDocuments() as $sectionPageFunction)
			{
				$args = ['modifiedPropertyNames' => ['page']];
				$event = new \Change\Documents\Events\Event(
					\Change\Documents\Events\Event::EVENT_UPDATED, $sectionPageFunction, $args);
				$sectionPageFunction->getEventManager()->triggerEvent($event);
			}
			}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$documentLink = $restResult;

			/** @var $document FunctionalPage */
			$document = $documentLink->getDocument();
			$um = $restResult->getUrlManager();
			$vc = new \Change\Http\Rest\V1\ValueConverter($um, $event->getApplicationServices()->getDocumentManager());
			$documentLink->setProperty('website',
				$vc->toRestValue($document->getWebsite(), \Change\Documents\Property::TYPE_DOCUMENT));

			$extraColumn = $event->getParam('extraColumn');
			if (in_array('allSupportedFunctionsCode', $extraColumn))
			{
				$documentLink->setProperty('allSupportedFunctionsCode', $document->getAllSupportedFunctionsCode());
			}
		}
	}
}
