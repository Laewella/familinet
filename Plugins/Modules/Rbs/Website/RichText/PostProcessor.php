<?php
/**
 * Copyright (C) 2014 Gaël PORT
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\RichText;

/**
 * @name \Rbs\Website\RichText\PostProcessor
 */
class PostProcessor
{
	/**
	 * @var \Rbs\Website\Documents\Website
	 */
	protected $website;

	/**
	 * @param string $html
	 * @param array $context
	 * @return string
	 */
	public function process($html, $context)
	{
		return preg_replace_callback(
			'/(<a[^>]*) href="#([^ ?&">\']+)" ([^>]*>)/i',
			function ($matches) use ($context)
			{
				$href = '#' . $matches[2];
				if (isset($context['currentURI']) && $context['currentURI'] instanceof \Zend\Uri\Http)
				{
					/* @var $currentURI \Zend\Uri\Http */
					$currentURI = $context['currentURI'];
					$currentURI->setFragment($matches[2]);
					$href = $currentURI->normalize()->toString();
				}
				return $matches[1] . ' href="' . $href . '" data-rbs-anchor="' . $matches[2] . '" ' . $matches[3];
			},
			$html
		);
	}
}