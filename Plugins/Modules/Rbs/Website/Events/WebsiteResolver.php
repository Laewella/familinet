<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Events;

use Zend\Http\Response;

/**
 * @name \Rbs\Website\Events\WebsiteResolver
 */
class WebsiteResolver
{
	/**
	 * @var array|null
	 */
	protected $websiteData;

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	public function resolve($event)
	{
		$data = $this->getWebsiteData($event);
		if (count($data))
		{
			$urlManager = $event->getUrlManager();
			$script = $urlManager->getScript();
			$request = $event->getRequest();
			$path = $request->getPath();

			if ($script && strpos($path, $script) === 0)
			{
				$path = ($path === $script) ? null : substr($path, strlen($script));
			}

			$hostName = $request->getUri()->getHost();
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$documentManager = $event->getApplicationServices()->getDocumentManager();

			$currentWebsite = null;
			foreach ($data as $row)
			{
				if ($hostName === $row['hostName'] && $this->isBasePath($path, $row['pathPart']))
				{
					$documentManager->preLoad([[(int)$row['id'], $row['model']]]);
					$currentWebsite = $documentManager->getDocumentInstance($row['id']);
					if ($currentWebsite instanceof \Rbs\Website\Documents\Website)
					{
						$LCID = $row['LCID'];
						$i18nManager->setLCID($LCID);
						$request->setLCID($LCID);
						if ($row['pathPart'])
						{
							break;
						}
					}
					else
					{
						$currentWebsite = null;
					}
				}
			}

			if ($currentWebsite instanceof \Rbs\Website\Documents\Website)
			{
				$event->setParam('website', $currentWebsite);
				$urlManager->setWebsite($currentWebsite);
				$stdUrlManager = $currentWebsite->getUrlManager($i18nManager->getLCID());
				if ($stdUrlManager->getBasePath() || $urlManager->getBasePath() || $hostName != $currentWebsite->getHostName()
					|| $stdUrlManager->getScript() != $urlManager->getScript())
				{
					if ($path === '/')
					{
						$result = new \Change\Http\Result(Response::STATUS_CODE_301);
						$result->setHeaderLocation($stdUrlManager->getByPathInfo(''));
						$event->setResult($result);
					}
					elseif ($hostName != $currentWebsite->getHostName())
					{
						$result = new \Change\Http\Result(Response::STATUS_CODE_301);
						$location = $urlManager->getSelf()->setHost($currentWebsite->getHostName())->normalize()->toString();
						$result->setHeaderLocation($location);
						$event->setResult($result);
					}
					else
					{
						$urlManager->setScript($stdUrlManager->getScript());
						$urlManager->setBasePath($stdUrlManager->getBasePath());
						$event->setAction(function(\Change\Http\Web\Event $event) {
								$event->setResult($event->getController()->notFound($event));
							});
					}
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @param string $url
	 * @return array|null
	 */
	public function resolveURL($event, $url)
	{
		if (!$url)
		{
			return null;
		}

		$data = $this->getWebsiteData($event);
		if (!count($data))
		{
			return null;
		}

		$uri = new \Zend\Uri\Http($url);
		$websiteData = null;
		$path = $uri->getPath();
		$hostName = $uri->getHost();
		foreach ($data as $row)
		{
			$basePath = $row['pathPart'] . '/';
			if ($hostName === $row['hostName'] && strpos($path, $basePath) === 0)
			{
				return [
					'websiteId' => $row['id'],
					'LCID' => $row['LCID'],
					'relativePath' => substr($path, strlen($basePath))
				];
			}
		}

		return null;
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	public function prefetchDocument($event)
	{
		$website = $event->getWebsite();
		if (!$website)
		{
			return;
		}

		$url = $event->getRequest()->getUri()->toString();
		$prefetchKey = 'Http-Web-' . $website->getId() . '-' . $website->getLCID() . '-' . md5($url);
		$applicationServices = $event->getApplicationServices();
		$cacheManager = $applicationServices->getCacheManager();
		$documentManager = $applicationServices->getDocumentManager();

		$c = 0;
		$prefetchIds = $cacheManager->getEntry('WebsiteResolver', $prefetchKey, ['ttl' => 120]);
		if (!$prefetchIds)
		{
			$prefetchIds = [];
		}
		else
		{
			$event->getApplication()->getLogging()->debug('GET', $prefetchKey, count($prefetchIds));
		}

		$listener = function (\Change\Documents\Events\Event $event) use (&$prefetchIds, $cacheManager, $documentManager)
		{
			$doc = $event->getDocument();
			$id = $doc->getId();
			if (!$doc instanceof \Rbs\User\Documents\User && !$doc instanceof \Rbs\User\Documents\Group && !isset($prefetchIds[$id]))
			{
				$prefetchIds[$id] = [$id, $doc->getDocumentModelName()];
			}
		};

		foreach ([\Change\Documents\Events\Event::EVENT_LOADED, \Change\Documents\Events\Event::EVENT_LOCALIZED_LOADED,
			'documents.preLoaded'] as $eventName)
		{
			$event->getApplication()->getSharedEventManager()->attach('Documents', $eventName, $listener);
		}

		if ($prefetchIds)
		{
			$c = count($prefetchIds);
			$documentManager->preLoad($prefetchIds);
		}

		$event->getApplication()->getSharedEventManager()->attach('Http.Web', \Change\Http\Web\Event::EVENT_RESPONSE, function (\Change\Http\Web\Event $event)
		use (&$prefetchIds, $prefetchKey, $listener, $c)
		{
			$application = $event->getApplication();
			$nc = count($prefetchIds);

			if ($c != $nc && $nc < 1000)
			{
				$event->getApplication()->getLogging()->debug('SET prefetch', $prefetchKey, $nc);

				$cacheManager = $event->getApplicationServices()->getCacheManager();
				$cacheManager->setEntry('WebsiteResolver', $prefetchKey, $prefetchIds, ['ttl' => 120]);
			}

			$application->getSharedEventManager()->detach($listener);
		}, 1);
	}

	/**
	 * @param string $path
	 * @param string $websitePathPart
	 * @return boolean
	 */
	protected function isBasePath($path, $websitePathPart)
	{
		if ($websitePathPart)
		{
			if ($path)
			{
				if ($path[0] == '/') {$path = substr($path, 1);}
				if ($websitePathPart === $path || $websitePathPart .'/' === $path || strpos($path, $websitePathPart . '/') === 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * @param \Change\Cache\CacheManager $cacheManager
	 */
	public function changed(\Change\Cache\CacheManager $cacheManager)
	{
		$cacheManager->removeEntry('WebsiteResolver', 'WebsiteData');
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return array
	 */
	protected function getWebsiteData($event)
	{
		if (is_array($this->websiteData))
		{
			return $this->websiteData;
		}

		$cacheManager = $event->getApplicationServices()->getCacheManager();
		$item = $cacheManager->getEntry('WebsiteResolver', 'WebsiteData', ['ttl' => 3600]);
		if ($item !== null)
		{
			$this->websiteData = $item;
			return $item;
		}

		$websiteModel =  $event->getApplicationServices()->getModelManager()->getModelByName('Rbs_Website_Website');
		if ($websiteModel)
		{
			$qb = $event->getApplicationServices()->getDbProvider()->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->getDocumentColumn('id'), 'id'),
				$fb->alias($fb->getDocumentColumn('model'), 'model'),
				$fb->alias($fb->getDocumentColumn('LCID'), 'LCID'),
				$fb->alias($fb->getDocumentColumn('hostName'), 'hostName'),
				$fb->alias($fb->getDocumentColumn('pathPart'), 'pathPart'));
			$qb->from($fb->getDocumentI18nTable($websiteModel->getRootName()));
			$qb->innerJoin($fb->getDocumentTable($websiteModel->getRootName()), $fb->getDocumentColumn('id'));
			$qb->andWhere($fb->isNotNull($fb->getDocumentColumn('hostName')));
			$item = $qb->query()->getResults();
		}
		else
		{
			$item = [];
		}
		$cacheManager->setEntry('WebsiteResolver', 'WebsiteData', $item, ['ttl' => 3600]);
		$this->websiteData = $item;
		return $item;
	}
}