<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Events;

/**
 * @name \Rbs\Website\Events\PageManager
 */
class PageManager
{
	/**
	 * @param \Change\Presentation\Pages\PageEvent $event
	 */
	public function addTrackersManager($event)
	{
		if ($event->getParam('TTL') !== 0)
		{
			return;
		}

		$result = $event->getPageResult();
		if ($result->getJsonObject('rbsWebsiteTrackersManager'))
		{
			return;
		}

		$application = $event->getApplication();
		$trackersManager = [
			'configuration' => [
				'consultedMaxCount' => $application->getConfiguration('Rbs/Website/Trackers/Consulted/maxCount'),
				'consultedPersistDays' => $application->getConfiguration('Rbs/Website/Trackers/Consulted/persistDays')
			]
		];
		$result->setJsonObject('rbsWebsiteTrackersManager', $trackersManager);
	}
}