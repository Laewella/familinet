<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Events;

/**
 * @name \Rbs\Website\Events\ElasticsearchManager
 */
class ElasticsearchManager
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetIndexData(\Change\Events\Event $event)
	{
		$data = $event->getParam('data');
		if (!$data || $event->getParam('indexName') !== \Rbs\Elasticsearch\Manager::DOCUMENTS_INDEX)
		{
			return;
		}

		/** @var \Rbs\Catalog\Documents\Product $sourceDocument */
		$sourceDocument = $event->getParam('sourceDocument');
		if ($sourceDocument instanceof \Rbs\Website\Documents\StaticPage)
		{
			if ($sourceDocument->getHideLinks())
			{
				return;
			}
			$applicationServices = $event->getApplicationServices();
			$documentManager = $applicationServices->getDocumentManager();
			$richTextManager = $applicationServices->getRichTextManager();
			$data = $this->addPageContent($sourceDocument, $data, $documentManager, $richTextManager);
			$event->setParam('data', $data);
		}
		elseif ($sourceDocument instanceof \Rbs\Website\Documents\Topic)
		{
			$indexPage = $sourceDocument->getIndexPage();
			if ($indexPage instanceof \Rbs\Website\Documents\StaticPage && $indexPage->getHideLinks())
			{
				$data['dependencies'][] = $indexPage->getId();
				$applicationServices = $event->getApplicationServices();
				$documentManager = $applicationServices->getDocumentManager();
				$richTextManager = $applicationServices->getRichTextManager();
				$data = $this->addPageContent($indexPage, $data, $documentManager, $richTextManager);
				$event->setParam('data', $data);
			}
		}
	}

	/**
	 * @param \Rbs\Website\Documents\Page $page
	 * @param array $data
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\RichText\RichTextManager $richTextManager
	 * @return array
	 */
	protected function addPageContent(\Rbs\Website\Documents\Page $page, array $data, $documentManager, $richTextManager)
	{
		/** @var array $LCIDArray */
		$LCIDArray = $data['LCID'] ?? [];
		foreach ($LCIDArray as $LCID)
		{
			try
			{
				$documentManager->pushLCID($LCID);
				$layout = $page->getContentLayout();
				foreach ($layout->getBlocks() as $block)
				{
					$params = $block->getParameters();
					if (isset($params['content']))
					{
						$richText = new \Change\Documents\RichtextProperty($params['content']);
						$text = $richTextManager->render($richText, 'Mail', null);
						$text = trim(strip_tags($text, '<p><br>'));
						if ($text)
						{
							$data[$LCID]['content'][] = $text;
						}
					}
				}
				$documentManager->popLCID();
			}
			catch (\Exception $e)
			{
				$documentManager->popLCID($e);
			}
		}
		return $data;
	}
}