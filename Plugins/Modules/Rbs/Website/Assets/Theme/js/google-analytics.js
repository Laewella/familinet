(function(window) {
	'use strict';

	if (window.__change.rbsWebsiteTrackersManager && angular.isFunction(window.__change.rbsWebsiteTrackersManager.isAllowed)
			&& window.__change.rbsWebsiteTrackersManager.isAllowed()) {

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		var app = angular.module('RbsChangeApp');

		app.provider('RbsChange.GoogleAnalytics', function() {
			this.$get = function() {
				var trackingID;

				function send(data) {
					if (angular.isObject(data) && data.hitType) {
						window.ga('send', data);
					}
				}

				function sendEvent(category, action, label, value) {
					var data = {
						'hitType': 'event',
						'eventCategory': category,
						'eventAction': action
					};
					if (label) {
						data.eventLabel = label;
					}
					if (value) {
						data.eventValue = value;
					}
					send(data);
				}

				function create(identifier, options) {
					trackingID = identifier;
					window.ga('create', trackingID, options || {
							cookieExpires: 31536000 // 365 days (<13 months) in seconds
						}
					);
				}

				function getContext() {
					var context = { href: window.location.href, trackingID: trackingID };
					if (window.ga.getByName) {
						var tracker = window.ga.getByName('t0');
						context.clientID = tracker.get('clientId');
					}
					return context;
				}

				return {
					create: create,
					send: send,
					sendEvent: sendEvent,
					getContext: getContext
				};
			}
		});

		app.directive('rbsWebsiteGoogleAnalytics',
			['RbsChange.GoogleAnalytics', '$rootScope', '$timeout', function(GoogleAnalytics, $rootScope, $timeout) {
				return {
					restrict: 'A',
					link: function(scope, elm, attr) {
						GoogleAnalytics.create(attr.identifier);

						$rootScope.$on('analytics', function(event, args) {
							if (args && args.category && args.action) {
								GoogleAnalytics.sendEvent(args.category, args.action, args.label, args.value);
							}
							else if (angular.isObject(args)) {
								angular.copy(GoogleAnalytics.getContext(), args);
							}
						});
						$timeout(function() {
							GoogleAnalytics.send({ 'hitType': 'pageview' });
							GoogleAnalytics.getContext();
						})
					}
				}
			}]
		);

		app.directive('rbsWebsiteEventAnalytics', ['RbsChange.GoogleAnalytics', function(GoogleAnalytics) {
			return {
				restrict: 'A',
				link: function(scope, elm, attr) {
					if (attr.analyticsTrigger == 'link') {
						var category = attr.analyticsCategory || 'element';
						var action = '' + attr.rbsWebsiteEventAnalytics;
						if (action === '') {
							action = 'view';
						}
						var label = '' + attr.analyticsLabel;
						var value = parseInt(attr.analyticsValue);
						GoogleAnalytics.sendEvent(category, action, label !== '' ? label : null, isNaN(value) ? null : value);
					}
					else {
						elm.on('click', function() {
							var category = attr.analyticsCategory || 'element';
							var action = '' + attr.rbsWebsiteEventAnalytics;
							if (action === '') {
								action = 'click';
							}
							var label = '' + attr.analyticsLabel;
							var value = parseInt(attr.analyticsValue);
							GoogleAnalytics.sendEvent(category, action, label !== '' ? label : null, isNaN(value) ? null : value);
						});
					}
				}
			}
		}]);
	}
})(window);