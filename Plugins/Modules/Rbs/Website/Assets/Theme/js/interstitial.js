(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.directive('rbsWebsiteInterstitial',
		['RbsChange.AjaxAPI', '$http', '$compile', '$timeout', '$cookies', rbsWebsiteInterstitial]);
	function rbsWebsiteInterstitial(AjaxAPI, $http, $compile, $timeout, $cookies) {
		return {
			restrict: 'A',
			link: function(scope, elem, attrs) {
				scope.modalId = attrs.modalId;
				scope.contentUrl = attrs.contentUrl;
				scope.parameters = scope.blockParameters || {};

				// If there is no modalId or no contentUrl, do nothing.
				if (!scope.modalId || !scope.contentUrl) {
					return;
				}

				var allowClosing = scope.parameters['allowClosing'];
				var frequency = scope.parameters['displayFrequency'];
				var modalNode = jQuery('#' + scope.modalId);
				var mainContentElement = jQuery('#' + scope.modalId + ' .modal-main-content');

				var closeDialog = function() {
					modalNode.modal('hide');
				};

				var loadContentSuccess = function(resultData) {
					mainContentElement.html(resultData);
					$compile(mainContentElement.contents())(scope);
					scope.modalContentMode = 'success';

					var autoCloseDelay = parseInt(scope.parameters['autoCloseDelay']);
					if (autoCloseDelay > 1) {
						$timeout(closeDialog, autoCloseDelay * 1000);
					}
				};

				var loadContentError = function(data, status, headers) {
					scope.modalContentMode = 'error';
					console.error('Interstitial', data, status, headers);
				};

				var showDialog = function() {
					scope.modalContentMode = 'loading';
					$http.get(scope.contentUrl).then(loadContentSuccess,loadContentError);
					modalNode.modal({ keyboard: allowClosing, backdrop: allowClosing ? true : 'static' });
				};

				if (frequency === 'always') {
					showDialog();
				}
				else {
					var cookieName = 'rbsWebsiteInterstitial-' + scope.parameters['displayedPage'];
					if ($cookies.get(cookieName)) {
						return;
					}

					var getCookieExpireDate = function(cookieTimeout) {
						var date = new Date();
						date.setTime(date.getTime() + cookieTimeout);
						return date.toGMTString();
					};

					switch (frequency) {
						case 'session':
							$cookies.put(cookieName, true);
							showDialog();
							break;

						case 'reprieve':
							var expire = getCookieExpireDate(scope.parameters['displayReprieve'] * 24 * 3600 * 1000);
							$cookies.put(cookieName, true, { expires: expire });
							showDialog();
							break;

						case 'once':
							expire = getCookieExpireDate(10 * 365 * 24 * 3600 * 1000); // 10 years...
							$cookies.put(cookieName, true, { expires: expire });
							showDialog();
							break;

						default:
							break;
					}
				}
			}
		}
	}
})();