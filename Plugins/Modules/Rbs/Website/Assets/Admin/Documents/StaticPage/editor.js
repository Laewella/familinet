(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange'),
		INDEX_FUNCTION_CODE = 'Rbs_Website_Section';

	Editor.$inject = ['$routeParams', 'RbsChange.REST', 'RbsChange.Breadcrumb', 'RbsChange.i18n'];
	app.directive('rbsDocumentEditorRbsWebsiteStaticPageNew', Editor);
	app.directive('rbsDocumentEditorRbsWebsiteStaticPageEdit', Editor);
	app.directive('rbsDocumentEditorRbsWebsiteStaticPageTranslate', Editor);

	function Editor($routeParams, REST, Breadcrumb, i18n) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.indexPageLabel = scope.warningDeleteIndexPage = '';

				scope.onLoad = function() {
					if (!scope.document.section) {
						var nodeId = Breadcrumb.getCurrentNodeId();
						if (nodeId) {
							REST.resource(nodeId).then(function(doc) { scope.document.section = doc; });
						}
					}
					if (scope.document.isNew() && $routeParams.website && !scope.document.website) {
						scope.document.website = $routeParams.website;
						REST.resource($routeParams.website).then(function(doc) { scope.document.website = doc; });
					}

					// Displays index page label if current page in modification is an index page
					if(scope.document.functions && scope.document.functions.indexOf(INDEX_FUNCTION_CODE) > -1) {
						scope.labels = [{ text: i18n.trans('m.rbs.website.admin.index_page_label | ucf'), type:'info'}];
						scope.warningDeleteIndexPage = i18n.trans('m.rbs.website.admin.warning_delete_index_page | ucf');
					}
				};

				// Content edition.
				var contentSectionInitialized = false;

				scope.initSection = function(sectionName) {
					if (sectionName === 'content') {
						scope.loadTemplate();
						contentSectionInitialized = true;
					}
				};

				scope.$on('Navigation.saveContext', function(event, args) {
					args.context.savedData('pageTemplate', scope.pageTemplate);
				});

				scope.onRestoreContext = function(currentContext) {
					scope.pageTemplate = currentContext.savedData('pageTemplate');
				};

				scope.loadTemplate = function() {
					var pt = scope.document.pageTemplate;
					if (pt) {
						if (!scope.pageTemplate || scope.pageTemplate.id != pt.id) {
							REST.resource(pt).then(function(template) {
								scope.pageTemplate = {
									id: template.id,
									html: template.htmlForBackoffice,
									data: template.editableContent,
									defaultDisplayColumnsFrom: pt.defaultDisplayColumnsFrom
								};
							});
						}
					}
				};

				scope.leaveSection = function(section) {
					if (section === 'content') {
						jQuery('[data-rbs-aside-column]').children().show();
						jQuery('#rbsWebsitePageBlockPropertiesAside').hide();
					}
				};

				scope.enterSection = function(section) {
					if (section === 'content') {
						jQuery('[data-rbs-aside-column]').children().hide();
						jQuery('#rbsWebsitePageBlockPropertiesAside').show();
						jQuery('rbs-aside-editor-menu').show();
					}
				};

				// This is for the "undo" dropdown menu:
				// Each item automatically activates its previous siblings.
				jQuery('[data-role=undo-menu]').on('mouseenter', 'li', function() {
					jQuery(this).siblings().removeClass('active');
					jQuery(this).prevAll().addClass('active');
				});

				scope.$watch('document.pageTemplate', function() {
					scope.loadTemplate();
				}, true);
			}
		};
	}
})(window.jQuery);