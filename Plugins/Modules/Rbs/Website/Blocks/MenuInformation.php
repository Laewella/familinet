<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Blocks;

/**
 * @name \Rbs\Website\Blocks\MenuInformation
 */
class MenuInformation extends \Change\Presentation\Blocks\Information
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.website.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.website.admin.menu', $ucf));
		$this->addParameterInformation('contextual', \Change\Documents\Property::TYPE_BOOLEAN, false, false)
			->setLabel($i18nManager->trans('m.rbs.website.admin.menu_contextual', $ucf));
		$this->addParameterInformationForDetailBlock(['Rbs_Website_Topic', 'Rbs_Website_Website', 'Rbs_Website_Menu'], $i18nManager)
			->setNormalizeCallback(function ($parametersValues) {
				if ($parametersValues['contextual'] ?? false)
				{
					return null;
				}
				$propertyName = \Change\Presentation\Blocks\Standard\Block::DOCUMENT_TO_DISPLAY_PROPERTY_NAME;
				return isset($parametersValues[$propertyName]) ? (int)$parametersValues[$propertyName] : 0;
			});
		$this->addParameterInformation('offset', \Change\Documents\Property::TYPE_INTEGER, false, 0)
			->setLabel($i18nManager->trans('m.rbs.website.admin.menu_offset', $ucf))
			->setNormalizeCallback(function ($parametersValues) {
				if (!($parametersValues['contextual'] ?? false))
				{
					return 0;
				}
				return isset($parametersValues['offset']) ? (int)$parametersValues['offset'] : 0;
			});
		$this->addParameterInformation('maxLevel', \Change\Documents\Property::TYPE_INTEGER, true, 1)
			->setLabel($i18nManager->trans('m.rbs.website.admin.menu_maxlevel', $ucf));
		$this->addParameterInformation('showTitle', \Change\Documents\Property::TYPE_BOOLEAN, true, false)
			->setLabel($i18nManager->trans('m.rbs.website.admin.menu_showtitle', $ucf));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-contextual.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_contextual_label', ['ucf']));
		$templateInformation->addParameterInformation('deployAll', \Change\Documents\Property::TYPE_BOOLEAN, false)
			->setLabel($i18nManager->trans('m.rbs.website.admin.block_menu_deploy_all', $ucf));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-vertical.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_vertical_label', ['ucf']));
		$templateInformation->addParameterInformation('deployAll', \Change\Documents\Property::TYPE_BOOLEAN, false)
			->setLabel($i18nManager->trans('m.rbs.website.admin.block_menu_deploy_all', $ucf));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-scroll.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_scroll_label', ['ucf']));
		$templateInformation->addParameterInformation('inverseColors', \Change\Documents\Property::TYPE_BOOLEAN, true)
			->setLabel($i18nManager->trans('m.rbs.website.admin.block_menu_inverse_colors', $ucf));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-pills.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_pills_label', ['ucf']));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-nav.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_nav_label', ['ucf']));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-inline.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_inline_label', ['ucf']));

		$templateInformation = $this->addTemplateInformation('Rbs_Website', 'menu-enriched.twig');
		$templateInformation->setLabel($i18nManager->trans('m.rbs.website.admin.template_menu_enriched_label', ['ucf']));
		$templateInformation->addParameterInformation('inverseColors', \Change\Documents\Property::TYPE_BOOLEAN, true)
			->setLabel($i18nManager->trans('m.rbs.website.admin.block_menu_inverse_colors', $ucf));
		$templateInformation->addParameterInformation('imageFormats', \Change\Documents\Property::TYPE_STRING, false, 'menuItemThumbnail')
			->setHidden(true);
		$templateInformation->addParameterInformation('attributesMaxLevel', \Change\Documents\Property::TYPE_STRING, false, '1')
			->setHidden(true);
	}
}
