<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Blocks;

/**
 * @name \Rbs\Website\Blocks\AnalyticsInformation
 */
class AnalyticsInformation extends \Change\Presentation\Blocks\Information
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.website.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.website.admin.analytics_label', $ucf));

		// Block parameters.
		$this->addParameterInformation('identifier', \Change\Documents\Property::TYPE_STRING)
			->setLabel($i18nManager->trans('m.rbs.website.admin.analytics_identifier', $ucf));

		// Default template parameters.
		$defaultInformation = $this->addDefaultTemplateInformation();
		$defaultInformation->setLabel($i18nManager->trans('m.rbs.website.admin.analytics_google_label', $ucf));
	}
}