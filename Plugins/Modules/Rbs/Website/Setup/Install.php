<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Setup;

/**
 * @name \Rbs\Website\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		// Consulted docs config.
		$configuration->addPersistentEntry('Rbs/Website/Trackers/Consulted/maxCount', 10);
		$configuration->addPersistentEntry('Rbs/Website/Trackers/Consulted/persistDays', 30);

		// Cache namespaces
		$configuration->addPersistentEntry('Change/Cache/Namespaces/WebsiteResolver', true);
		$configuration->addPersistentEntry('Change/Cache/Namespaces/adminPreview', true);

		$workspace = $application->getWorkspace();
		$webBaseDirectory = $configuration->getEntry('Change/Install/webBaseDirectory');
		if ($webBaseDirectory && !$workspace->isAbsolutePath($webBaseDirectory))
		{
			$requirePath = implode(DIRECTORY_SEPARATOR, array_fill(0, count(explode(DIRECTORY_SEPARATOR, $webBaseDirectory)), '..'));
		}
		else
		{
			$requirePath = $workspace->projectPath();
		}

		$webBasePath = $workspace->composeAbsolutePath($webBaseDirectory);
		if (is_dir($webBasePath))
		{
			$srcPath = __DIR__ . '/Assets/index.php';
			$content = \Change\Stdlib\FileUtils::read($srcPath);
			$content = str_replace('__DIR__', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($webBasePath . DIRECTORY_SEPARATOR . basename($srcPath), $content);
		}
		else
		{
			throw new \RuntimeException('Invalid document root path: ' . $webBasePath .
				'. Check "Change/Install/webBaseDirectory" configuration entry.', 999999);
		}

		// Patch.
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Website', \Rbs\Website\Setup\Patch\Listeners::class);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$this->installGenericAttributes($applicationServices);

		$rootNode = $applicationServices->getTreeManager()->getRootNode('Rbs_Website');
		if (!$rootNode)
		{
			$transactionManager = $applicationServices->getTransactionManager();

			try
			{
				$transactionManager->begin();

				/* @var $folder \Rbs\Generic\Documents\Folder */
				$folder = $applicationServices->getDocumentManager()->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
				$folder->setLabel('Rbs_Website');
				$folder->create();
				$applicationServices->getTreeManager()->insertRootNode($folder, 'Rbs_Website');

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}
		}

		$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
		$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());
		$import->addOnly(true);

		$json = json_decode(file_get_contents(__DIR__ . '/Assets/generic-texts.json'), true);
		try
		{
			$applicationServices->getTransactionManager()->begin();
			$import->fromArray($json);
			$applicationServices->getTransactionManager()->commit();
		}
		catch (\Exception $e)
		{
			$applicationServices->getTransactionManager()->rollBack($e);
		}
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function installGenericAttributes($applicationServices)
	{
		$json = json_decode(file_get_contents(__DIR__ . '/Assets/attributes.json'), true);

		$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
		$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());
		$import->getOptions()->set('resolveInlineDocument', [$import, 'defaultResolveCollectionItem']);
		$import->addOnly(true);

		$dcm = $applicationServices->getDocumentCodeManager();
		$callback = function ($document, $jsonDocument) use ($import, $dcm)
		{
			(new \Rbs\Generic\Attributes\ListenerCallbacks)->preSaveImport($document, $jsonDocument, $import, $dcm);
		};
		$import->getOptions()->set('preSave', $callback);

		try
		{
			$applicationServices->getTransactionManager()->begin();
			$import->fromArray($json);
			$applicationServices->getTransactionManager()->commit();
		}
		catch (\Exception $e)
		{
			throw $applicationServices->getTransactionManager()->rollBack($e);
		}
	}
}