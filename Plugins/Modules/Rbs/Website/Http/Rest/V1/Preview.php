<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Http\Rest\V1;

/**
 * @name \Rbs\Website\Http\Rest\V1\Preview
 */
class Preview
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();
		if (!$request->isPost())
		{
			$result = $event->getController()->notAllowedError($request->getMethod(), [\Change\Http\Request::METHOD_POST]);
			$event->setResult($result);
			return;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$this->handleLCID($event);

		$helper = new \Rbs\Website\Preview\PreviewHelper();
		$previewId = $event->getParam('previewId');
		$preview = $documentManager->getDocumentInstance($previewId);
		if (!($preview instanceof \Change\Documents\AbstractDocument))
		{
			$event->getController()->notFound($event);
			return;
		}

		$params = $request->getPost();

		$page = $this->preResolvePage($event, $preview, $params->get('pageId'));
		$this->resolveTemplate($event, $preview, $params->get('templateId'));
		$this->resolveDocument($event, $preview, $params->get('detailId'));
		$this->resolveSection($event, $preview, $page, $params->get('sectionId'));
		$helper->preparePreview($event, $preview, $params->get('properties'));
		$helper->setContext($event, $params->toArray());
		$this->resolvePage($event, $preview, $page);

		$event->setResult($this->generateResult($event));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult(\Change\Http\Event $event)
	{
		$urlManager = $event->getParam('websiteUrlManager');
		$applicationServices = $event->getApplicationServices();

		$contextData = [
			'LCID' => $event->getApplicationServices()->getDocumentManager()->getLCID(),
			'previewId' => $event->getParam('previewId'),
			'sectionId' => $event->getParam('section')->getId(),
			'websiteId' => $event->getParam('website')->getId(),
			'detailId' => $event->getParam('document') ? $event->getParam('document')->getId() : null,
			'pageId' => $event->getParam('page')->getId(),
			'userId' => $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->getId(),
			'properties' => $event->getRequest()->getPost()->get('properties')
		];
		$previewKey = md5(serialize($contextData));

		$webEvent = new \Change\Http\Web\Event();
		$webEvent->setParams($event->getParams());
		$webEvent->setUrlManager($urlManager);
		$request = new \Change\Http\Request();
		$request->setLCID($applicationServices->getDocumentManager()->getLCID());
		$request->setMethod(\Zend\Http\Request::METHOD_GET);
		$webEvent->setRequest($request);

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$extension = new \Rbs\Generic\Presentation\Twig\Extension($event->getApplication(), $event->getApplicationServices(),
				$genericServices, $urlManager);

		$event->getApplication()->getSharedEventManager()->attach('TemplateManager', 'registerExtensions',
				function(\Change\Events\Event $event) use ($extension) {
					/** @var \ArrayObject $extensions */
					$extensions = $event->getParam('extensions');
					$extensions->append($extension);
				}, 5);

		$page = $event->getParam('page');
		$pageManager = $applicationServices->getPageManager();
		$attached1 = $pageManager->getEventManager()->attach(
			\Change\Presentation\Pages\PageManager::EVENT_GET_PAGE_RESULT,
			function (\Change\Presentation\Pages\PageEvent $event) use ($previewKey)
			{
				$result = $event->getPageResult();
				if ($result instanceof \Change\Http\Web\Result\Page)
				{
					$result->setJsonObject('previewKey', $previewKey);
				}
			},
			10);
		$attached2 = $pageManager->getEventManager()->attach(
			'buildTemplateReplacements',
			function (\Change\Events\Event $event) use ($extension)
			{
				$replacements = $event->getParam('templateReplacements');
				if (isset($replacements['<!-- jsFooter -->']))
				{
					$replacements['<!-- jsFooter -->'] .= PHP_EOL
						. '<script type="text/javascript" src="' . $extension->resourceURL('Theme/Rbs/Base/js/preview.js')
						. '"></script>';
					$event->setParam('templateReplacements', $replacements);
				}
			},
			1);
		$pageResult = $pageManager->setHttpWebEvent($webEvent)->getPageResult($page);
		$pageManager->getEventManager()->detach($attached1);
		$pageManager->getEventManager()->detach($attached2);

		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray([
			'contextData' => $contextData,
			'previewKey' => $previewKey,
			'pageContent' => $pageResult->toHtml()
		]);

		$cacheManager = $applicationServices->getCacheManager();
		$options = ['ttl' => 600];
		$cacheManager->setEntry('adminPreview', $previewKey, $contextData, $options);
		return $result;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return string
	 */
	protected function handleLCID(\Change\Http\Event $event)
	{
		$LCID = $event->getParam('LCID');
		if ($LCID)
		{
			if (!$event->getApplicationServices()->getI18nManager()->isSupportedLCID($LCID))
			{
				throw new \RuntimeException('Invalid Parameter: LCID', 71000);
			}
			$event->getApplicationServices()->getDocumentManager()->pushLCID($LCID);
		}
		return $event->getApplicationServices()->getDocumentManager()->getLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param integer|null $templateId
	 * @return \Rbs\Theme\Documents\Template|null
	 */
	protected function resolveTemplate(\Change\Http\Event $event, \Change\Documents\AbstractDocument $document, $templateId)
	{
		if ($templateId)
		{
			$template = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($templateId);
			if (!($template instanceof \Rbs\Theme\Documents\Template))
			{
				throw new \RuntimeException('Invalid template!', 99999);
			}
			$event->setParam('template', $template);
			return $template;
		}
		elseif ($document instanceof \Rbs\Theme\Documents\Template)
		{
			$event->setParam('template', $document);
			return $document;
		}
		return null;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param int|null $documentId
	 * @return \Change\Documents\AbstractDocument
	 */
	protected function resolveDocument(\Change\Http\Event $event, \Change\Documents\AbstractDocument $document, $documentId)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			$event->setParam('document', $document);
			return $document;
		}
		if ($documentId)
		{
			$doc = $documentManager->getDocumentInstance($documentId);
			if ($doc instanceof \Change\Documents\AbstractDocument)
			{
				$event->setParam('document', $doc);
				return $doc;
			}
		}
		return null;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param \Change\Presentation\Interfaces\Page|null $page
	 * @param int|null $sectionId
	 * @return \Rbs\Website\Documents\Section
	 */
	protected function resolveSection(\Change\Http\Event $event, \Change\Documents\AbstractDocument $document, $page, $sectionId)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$section = null;
		if ($sectionId)
		{
			$section = $documentManager->getDocumentInstance($sectionId);
		}
		elseif ($page instanceof \Rbs\Website\Documents\StaticPage)
		{
			$section = $page->getSection();
		}
		elseif ($document instanceof \Rbs\Website\Documents\StaticPage)
		{
			$section = $document->getSection();
		}

		if ($section instanceof \Rbs\Website\Documents\Section)
		{
			$event->setParam('section', $section);
			$website = $section->getWebsite();
			$event->setParam('website', $website);
			$websiteUrlManager = $website->getUrlManager($documentManager->getLCID());
			$websiteUrlManager->setSection($section);
			$event->setParam('websiteUrlManager', $websiteUrlManager);
			return $section;
		}
		throw new \RuntimeException('No valid section!', 99999);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param int|null $pageId
	 * @return \Change\Presentation\Interfaces\Page|null
	 */
	protected function preResolvePage(\Change\Http\Event $event, \Change\Documents\AbstractDocument $document, $pageId)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$page = null;
		if ($pageId)
		{
			$page = $documentManager->getDocumentInstance($pageId);
			if (!($page instanceof \Change\Presentation\Interfaces\Page))
			{
				throw new \RuntimeException('Invalid page!', 99999);
			}
		}
		elseif ($document instanceof \Change\Presentation\Interfaces\Page)
		{
			$page = $document;
		}
		return $page;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument|null $document
	 * @param \Change\Presentation\Interfaces\Page|null $preResolvedPage
	 * @return mixed|\Rbs\Website\Documents\StaticPage
	 */
	protected function resolvePage(\Change\Http\Event $event, $document, $preResolvedPage)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		if ($preResolvedPage instanceof \Change\Presentation\Interfaces\Page)
		{
			if ($preResolvedPage instanceof \Rbs\Website\Documents\FunctionalPage)
			{
				/** @var \Rbs\Website\Documents\Section $section */
				$section = $event->getParam('section');
				$preResolvedPage->setSection($section);
			}
			$page = $preResolvedPage;
		}
		elseif ($document instanceof \Change\Documents\AbstractDocument)
		{
			$LCID = $documentManager->getLCID();
			/** @var \Change\Http\Web\UrlManager $websiteUrlManager */
			$websiteUrlManager = $event->getParam('websiteUrlManager');
			/** @var \Change\Presentation\Interfaces\Website $website */
			$website = $event->getParam('website');
			/** @var \Rbs\Website\Documents\Section $section */
			$section = $event->getParam('section');
			$pathRule = $websiteUrlManager->getPathRuleManager()
				->getNewRule($website->getId(), $LCID, 'NULL', $document->getId(), 200, $section->getId());
			$params['pathRule'] = $pathRule;
			$params['website'] = $website;

			$eventName = \Change\Documents\Events\Event::EVENT_DISPLAY_PAGE;
			$documentEvent = new \Change\Documents\Events\Event($eventName, $document, $params);
			$document->getEventManager()->triggerEvent($documentEvent);
			$page = $documentEvent->getParam('page');
			if (!($page instanceof \Change\Presentation\Interfaces\Page))
			{
				throw new \RuntimeException('No page found!', 99999);
			}
			elseif ($page instanceof \Rbs\Website\Documents\FunctionalPage)
			{
				$page->setSection($section);
			}
		}
		else
		{
			throw new \RuntimeException('No page found!', 99999);
		}
		$event->setParam('page', $page);

		$template = $event->getParam('template');
		if ($template instanceof \Rbs\Theme\Documents\Template)
		{
			$page->setPageTemplate($template);
		}
		return $page;
	}
}