<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Menu;

/**
 * @name \Rbs\Website\Menu\MenuEntry
 */
class MenuEntry
{
	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string|\Zend\Uri\Http|null
	 */
	protected $url;

	/**
	 * @var integer|null
	 */
	protected $documentId;

	/**
	 * @var array
	 */
	protected $typology;

	/**
	 * @var boolean
	 */
	protected $inPath = false;

	/**
	 * @var boolean
	 */
	protected $current = false;

	/**
	 * @var MenuEntry[]
	 */
	protected $children;

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setTitle($label)
	{
		$this->title = $label;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string|\Zend\Uri\Http $url
	 * @return $this
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @return string|\Zend\Uri\Http|null
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @return bool
	 */
	public function hasUrl()
	{
		return $this->url !== null;
	}

	/**
	 * @return int|null
	 */
	public function getDocumentId()
	{
		return $this->documentId;
	}

	/**
	 * @param int|null $documentId
	 * @return $this
	 */
	public function setDocumentId($documentId)
	{
		$this->documentId = $documentId;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getTypology()
	{
		return $this->typology;
	}

	/**
	 * @param array $typology
	 * @return $this
	 */
	public function setTypology($typology)
	{
		$this->typology = $typology;
		return $this;
	}

	/**
	 * @param boolean $inPath
	 * @return $this
	 */
	public function setInPath($inPath)
	{
		$this->inPath = $inPath;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isInPath()
	{
		return $this->inPath;
	}

	/**
	 * @param boolean $current
	 * @return $this
	 */
	public function setCurrent($current)
	{
		$this->current = $current;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isCurrent()
	{
		return $this->current;
	}

	/**
	 * @param MenuEntry[] $children
	 * @return $this
	 */
	public function setChildren($children)
	{
		$this->children = $children;
		return $this;
	}

	/**
	 * @param MenuEntry $child
	 * @return $this
	 */
	public function addChild($child)
	{
		if ($child instanceof MenuEntry)
		{
			$this->children[] = $child;
		}
		return $this;
	}

	/**
	 * @return MenuEntry[]
	 */
	public function getChildren()
	{
		return $this->children;
	}

	/**
	 * @return bool
	 */
	public function hasChild()
	{
		return is_array($this->children) && count($this->children) > 0;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$array = [
			'title' => $this->title,
			'url' => $this->url instanceof \Zend\Uri\Http ? $this->url->normalize()->toString() : $this->url,
			'documentId' => $this->documentId,
			'typology' => $this->typology,
			'isInPath' => $this->inPath,
			'isCurrent' => $this->current,
			'children' => null
		];

		if ($this->hasChild())
		{
			$children = [];
			foreach ($this->getChildren() as $child)
			{
				$children[] = $child->toArray();
			}
			$array['children'] = $children;
		}
		return $array;
	}
}