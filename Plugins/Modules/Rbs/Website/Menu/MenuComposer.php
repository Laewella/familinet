<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Menu;

/**
 * @name \Rbs\Website\Menu\MenuComposer
 */
class MenuComposer
{
	/**
	 * @var \Change\Http\Web\UrlManager
	 */
	protected $urlManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Documents\TreeManager
	 */
	protected $treeManager;

	/**
	 * @param \Change\Http\Web\UrlManager $urlManager
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Documents\TreeManager $treeManager
	 */
	public function __construct($urlManager, $i18nManager, $documentManager, $treeManager)
	{
		$this->urlManager = $urlManager;
		$this->i18nManager = $i18nManager;
		$this->documentManager = $documentManager;
		$this->treeManager = $treeManager;
	}

	/**
	 * @param \Change\Presentation\Interfaces\Website $website
	 * @param \Change\Documents\AbstractDocument $doc
	 * @param integer $maxLevel
	 * @param null|\Rbs\Website\Documents\Page $currentPage
	 * @param \Rbs\Website\Documents\Section[] $path
	 * @return \Rbs\Website\Menu\MenuEntry|null
	 */
	public function getMenuEntry($website, $doc, $maxLevel, $currentPage, $path)
	{
		$pageId = $currentPage ? $currentPage->getId() : 0;
		$pathIds = [];
		if ($path) {
			foreach ($path as $s)
			{
				if ($s instanceof \Rbs\Website\Documents\Section)
				{
					$pathIds[] = $s->getId();
				}
			}
		}

		$entry = $this->buildMenuEntry($doc, $maxLevel, $pageId, $pathIds);

		if ($entry)
		{
			$urlManager = $this->urlManager;
			$setUrl = function (\Rbs\Website\Menu\MenuEntry $entry, $setUrl) use ($urlManager) {
				if ($entry->getUrl() === true) {
					$entry->setUrl($urlManager->getCanonicalByDocument($entry->getDocumentId()));
				}
				if ($entry->hasChild()) {
					foreach ($entry->getChildren() as $c)
					{
						$setUrl($c, $setUrl);
					}
				}
			};
			$setUrl($entry, $setUrl);
		}
		return $entry;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $doc
	 * @param integer $maxLevel
	 * @param null|integer $pageId
	 * @param integer[] $pathIds
	 * @param \Change\Documents\TreeNode $tn
	 * @return null|MenuEntry
	 */
	public function buildMenuEntry($doc, $maxLevel, $pageId, $pathIds, $tn = null)
	{
		$entry = new \Rbs\Website\Menu\MenuEntry();
		$entry->setTitle($doc->getDocumentModel()->getPropertyValue($doc, 'title'));
		$docId = $doc->getId();
		$entry->setDocumentId($docId);

		if ($doc instanceof \Rbs\Website\Documents\Section)
		{
			if ($this->getIndexPage($doc))
			{
				$entry->setUrl(true);
			}
			elseif ($maxLevel < 1)
			{
				return null; // Hide empty topics.
			}
			if (count($pathIds) && in_array($docId, $pathIds))
			{
				$entry->setInPath(true);
			}
		}
		elseif ($doc instanceof \Rbs\Website\Documents\Menu)
		{
			$doc->preLoadEntries();
		}
		else
		{
			$entry->setUrl(true);
			if ($pageId === $docId)
			{
				$entry->setCurrent(true);
				$entry->setInPath(true);
			}
		}

		if ($maxLevel >= 1)
		{
			if ($doc instanceof \Rbs\Website\Documents\Section)
			{
				if ($tn === null)
				{
					$tn = $this->treeManager->getNodeByDocument($doc);
					if (!$tn) {
						return null;
					}
					$children = $this->treeManager->getDescendantNodes($tn, $maxLevel);
				}
				else
				{
					$children = $tn->getChildren();
				}

				if ($children)
				{
					$preLoad = [];
					foreach ($children as $childNode)
					{
						$preLoad[] = [$childNode->getDocumentId(), null];
					}
					$this->documentManager->preLoad($preLoad);

					foreach ($children as $childNode)
					{
						$childDoc = $this->documentManager->getDocumentInstance($childNode->getDocumentId());
						if ($this->shouldBeDisplayed($childDoc, $doc))
						{
							$entry->addChild($this->buildMenuEntry($childDoc, $maxLevel - 1, $pageId, $pathIds, $childNode));
						}
					}
				}

				if (!$entry->getUrl() && !count($entry->getChildren()))
				{
					return null; // Hide empty topics.
				}

			}
			elseif ($doc instanceof \Rbs\Website\Documents\Menu)
			{
				/** @var $entryDoc \Rbs\Website\Documents\MenuEntry */
				foreach ($doc->getEntries()->toArray() as $entryDoc)
				{
					$title = $entryDoc->getCurrentLocalization()->getTitle();

					switch ($entryDoc->getEntryTypeCode())
					{
						case 'document':
							$childDoc = $entryDoc->getTargetDocument();
							if ($this->shouldBeDisplayed($childDoc, $doc))
							{
								$subEntry = $this->buildMenuEntry($childDoc, $maxLevel - 1, $pageId, $pathIds);
								if ($subEntry)
								{
									if ($title)
									{
										$subEntry->setTitle($title);
									}
									$entry->addChild($subEntry);
								}
							}
							break;

						case 'sectionIndex':
							$childDoc = $entryDoc->getTargetDocument();
							if ($this->shouldBeDisplayed($childDoc, $doc))
							{
								$subEntry = $this->buildMenuEntry($childDoc, 0, $pageId, $pathIds);
								if ($subEntry)
								{
									if ($title)
									{
										$subEntry->setTitle($title);
									}
									$entry->addChild($subEntry);
								}
							}
							break;

						case 'menu':
							$subMenu = $entryDoc->getSubMenu();
							if ($subMenu)
							{
								$subEntry = $this->buildMenuEntry($subMenu, $maxLevel - 1, $pageId, $pathIds);
								if ($subEntry)
								{
									if ($title)
									{
										$subEntry->setTitle($title);
									}

									$url = $entryDoc->getCurrentLocalization()->getUrl();
									if ($url)
									{
										$subEntry->setUrl($url);
									}
									$entry->addChild($subEntry);
								}
							}
							break;

						case 'url':
							if ($title && ($url = $entryDoc->getCurrentLocalization()->getUrl()))
							{
								$subEntry = new \Rbs\Website\Menu\MenuEntry();
								$subEntry->setTitle($title);
								$subEntry->setUrl($url);
								$entry->addChild($subEntry);
							}
							break;
					}
				}
			}
		}
		return $entry;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $doc
	 * @param \Change\Documents\AbstractDocument $parent
	 * @return boolean
	 */
	protected function shouldBeDisplayed($doc, $parent)
	{
		if (!($doc instanceof \Change\Documents\Interfaces\Publishable) || !$doc->published())
		{
			return false;
		}
		return $parent instanceof \Rbs\Website\Documents\Menu || !($doc instanceof \Rbs\Website\Documents\StaticPage) || !$doc->getHideLinks();
	}

	/**
	 * @var array
	 */
	protected $indexPages;

	/**
	 * @param \Rbs\Website\Documents\Section $section
	 * @return bool
	 */
	protected function getIndexPage(\Rbs\Website\Documents\Section $section)
	{
		if ($this->indexPages === null)
		{
			$this->indexPages = [];
			$query = $this->documentManager->getNewQuery('Rbs_Website_SectionPageFunction');
			$query->andPredicates($query->gt('section', 0), $query->eq('functionCode', 'Rbs_Website_Section'));
			$spf = $query->getTableAliasName();

			$dbq = $query->dbQueryBuilder();
			$fb = $dbq->getFragmentBuilder();
			$dbq->innerJoin($fb->alias($fb->getDocumentTable('Rbs_Website_Page'), 'p'),
				$fb->eq($fb->column('page', $spf), $fb->getDocumentColumn('id', 'p'))
			);
			$dbq->leftJoin($fb->alias($fb->getDocumentI18nTable('Rbs_Website_Page'), 'i'),
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('id', 'p'), $fb->getDocumentColumn('id', 'i')),
					$fb->eq($fb->getDocumentColumn('publicationStatus', 'i'), $fb->string('PUBLISHABLE')),
					$fb->eq($fb->getDocumentColumn('LCID', 'i'), $fb->string($this->documentManager->getLCID()))
				)
			);
			$dbq->addColumn($fb->getDocumentColumn('section', $spf));
			$dbq->addColumn($fb->alias($fb->getDocumentColumn('startPublication', 'i'), 'startPublication'));
			$dbq->addColumn($fb->alias($fb->getDocumentColumn('endPublication', 'i'), 'endPublication'));
			$select = $dbq->query();
			$rows = $select->getResults($select->getRowsConverter()
				->addIntCol('section')->addDtCol('startPublication', 'endPublication'));

			$test = new \DateTime();
			foreach ($rows as $row)
			{
				$st = $row['startPublication'];
				$ep = $row['endPublication'];
				if ((null === $st || $st <= $test) && (null === $ep || $test < $ep))
				{
					$this->indexPages[$row['section']] = true;
				}
			}
		}
		return isset($this->indexPages[$section->getId()]);
	}
} 