<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Review\Documents;

/**
 * @name \Rbs\Review\Documents\Review
 */
class Review extends \Compilation\Rbs\Review\Documents\Review
{
	/**
	 * @return string
	 */
	public function getTitle()
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs(['review' => $this]);
		$documentEvent = new \Change\Documents\Events\Event('getTitle', $this, $eventArgs);
		$em->triggerEvent($documentEvent);
		return $eventArgs['title'] ?? '';
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultGetTitle(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('title'))
		{
			return;
		}

		$review = $event->getDocument();
		if ($review instanceof Review)
		{
			$target = $review->getTarget();
			$targetTitle = ($target instanceof \Change\Documents\AbstractDocument) ?
				$target->getDocumentModel()->getPropertyValue($target, 'title') : '';
			$key = 'm.rbs.review.front.review_title_content';
			$replacements = ['TARGET' => $targetTitle, 'PSEUDONYM' => $review->getPseudonym()];
			$event->setParam('title', $event->getApplicationServices()->getI18nManager()->trans($key, ['ucf'], $replacements));
		}
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		// Do nothing.
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs([]);
		$documentEvent = new \Change\Documents\Events\Event('getLabel', $this, $eventArgs);
		$em->triggerEvent($documentEvent);
		return $eventArgs['label'] ?? '';
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetLabel(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('label'))
		{
			return;
		}

		$review = $event->getDocument();
		if ($review instanceof Review)
		{
			$target = $review->getTarget();
			$targetLabel = ($target instanceof \Change\Documents\AbstractDocument) ?
				$target->getDocumentModel()->getPropertyValue($target, 'label') : '';
			$key = 'm.rbs.review.admin.review_label_content';
			$replacements = ['targetLabel' => $targetLabel, 'pseudonym' => $review->getPseudonym()];
			$event->setParam('label', $event->getApplicationServices()->getI18nManager()->trans($key, ['ucf'], $replacements));
		}
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Do nothing.
		return $this;
	}

	/**
	 * @@param array|null $options
	 * @return string
	 */
	public function getPseudonym($options = null)
	{
		$em = $this->getEventManager();
		$eventArgs = (is_array($options) && count($options)) ? $em->prepareArgs($options) : $em->prepareArgs([]);
		$documentEvent = new \Change\Documents\Events\Event('getPseudonym', $this, $eventArgs);
		$em->triggerEvent($documentEvent);
		return $eventArgs['pseudonym'] ?? null;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onGetPseudonym(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('pseudonym'))
		{
			return;
		}

		$review = $event->getDocument();
		if ($review instanceof Review)
		{
			$pseudonym = null;
			$author = $review->getAuthorIdInstance();
			if ($author instanceof \Rbs\User\Documents\User)
			{
				$author = new \Rbs\User\Events\AuthenticatedUser($author);
				$webProfile = $event->getApplicationServices()->getProfileManager()->loadProfile($author, 'Rbs_Website');
				$pseudonym = $webProfile ? $webProfile->getPropertyValue('pseudonym') : null;
			}

			if (!$pseudonym && !$event->getParam('ignoreGuestPseudonym'))
			{
				$pseudonym = $review->getGuestPseudonym();
			}
			$event->setParam('pseudonym', $pseudonym);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultSave(\Change\Documents\Events\Event $event)
	{
		$review = $event->getDocument();
		if ($review instanceof Review && !$review->getReviewDate())
		{
			$review->setReviewDate(new \DateTime());
		}
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onDefaultSave($event); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onDefaultSave($event); }, 10);
		$eventManager->attach('getLabel', function ($event) { $this->onDefaultGetLabel($event); }, 5);
		$eventManager->attach('getTitle', function ($event) { $this->onDefaultGetTitle($event); }, 5);
		$eventManager->attach('getPseudonym', function ($event) { $this->onGetPseudonym($event); }, 5);
		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); }, 5);
	}

	/**
	 * @return array|\Change\Documents\AbstractDocument
	 */
	public function getPublicationSections()
	{
		return $this->getSection() ? [$this->getSection()] : [];
	}

	/**
	 * @param \Change\Presentation\Interfaces\Section[] $publicationSections
	 * @return $this
	 */
	public function setPublicationSections($publicationSections)
	{
		if (is_array($publicationSections) && count($publicationSections))
		{
			$this->setSection($publicationSections[0]);
		}
		return $this;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$document = $event->getDocument();
		if ($document instanceof Review)
		{
			if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
			{
				$pseudonym = $document->getPseudonym();
				$restResult->setProperty('pseudonym', $pseudonym);
				$canEditPseudonym = $pseudonym ? !$document->getPseudonym(['ignoreGuestPseudonym' => true]) : true;
				$restResult->setProperty('canEditPseudonym', $canEditPseudonym);
				$richTextManager = $event->getApplicationServices()->getRichTextManager();
				$restResult->setProperty('renderedContent', $richTextManager->render($document->getContent(), 'Admin'));
			}
			elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
			{
				$restResult->setProperty('pseudonym', $document->getPseudonym());
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Review\ReviewDataComposer($event))->toArray());
	}
}
