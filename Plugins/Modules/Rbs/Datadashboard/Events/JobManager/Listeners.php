<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Events\JobManager;

/**
 * @name \Rbs\Datadashboard\Events\JobManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $events$
	 * @param int $priority
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('process_Rbs_Datadashboard_Import', function (\Change\Job\Event $event)
		{
			(new \Rbs\Datadashboard\Job\Import())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('process_Rbs_Datadashboard_Validate', function (\Change\Job\Event $event)
		{
			(new \Rbs\Datadashboard\Job\Validate())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('process_Rbs_Datadashboard_CheckFTPFiles', function (\Change\Job\Event $event)
		{
			(new \Rbs\Datadashboard\Job\CheckFTPFiles())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('process_Rbs_Datadashboard_GetFTPFiles', function (\Change\Job\Event $event)
		{
			(new \Rbs\Datadashboard\Job\GetFTPFiles())->execute($event);
		}, 5);
	}
}