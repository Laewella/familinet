<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Http\UI;

/**
 * @name \Rbs\Datadashboard\Http\UI\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the EventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->onAction($event); });
		$this->listeners[] = $events->attach('ApplicationsMenu', function ($event) { $this->onApplicationsMenu($event); });
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onApplicationsMenu(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$applicationsMenu = $event->getParam('applicationsMenu');

		$applicationsMenu['administration']['items'][] = [
			'position' => 10,
			'name' => 'dataDashboard',
			'realm' => 'Rbs_Datadashboard_dataDashboard',
			'title' => $i18nManager->trans('m.rbs.datadashboard.ua.dataDashboard_title', ['ucf'])
		];

		$event->setParam('applicationsMenu', $applicationsMenu);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onAction(\Change\Http\Event $event)
	{
		$applicationName = $event->getParam('applicationName');
		if ($applicationName === 'dataDashboard')
		{
			$event->setParam('serverRealm', 'Rbs_Datadashboard_' . $applicationName);
			if ($event->getAction())
			{
				return;
			}

			$event->setAction([new \Rbs\Datadashboard\Http\UI\Home(), 'execute']);
		}
	}
}