<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Http\API\Datadashboard;

/**
 * @name \Rbs\Datadashboard\Http\Rest\Actions\GetFilePatterns
 */
class GetFilePatterns
{

	public function execute(\Change\Http\Event $event)
	{
		$csvEngine = new \Change\Synchronization\CSV\CSVEngine($event->getApplication());
		$sortedImport = $csvEngine->getSortedImport();
		$filePatterns = [];
		foreach($sortedImport as $import)
		{
			$data = $csvEngine->resolveImportType($import);
			$filePatterns[] = $data['filePattern'];
		}
		$formattedPattern = '/' . implode('|', $filePatterns) . '/';
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray(['patterns' => implode('|', $filePatterns)]);
		$event->setResult($result);
	}
}