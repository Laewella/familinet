<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Http\API\Datadashboard;

/**
 * @name \Rbs\Datadashboard\Http\API\Datadashboard\SynchronizationCodes
 */
class SynchronizationCodes
{

	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getSynchronizationCodes(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$context = $inputParameters->toArray();
		if (empty($context['filter']['filters']))
		{
			$result = new \Change\Http\Result();
			$event->setResult($result);
			return;
		}
		if (isset($context['sort']) && $context['sort'] === 'id')
		{
			$context['sort'] = 'document_id';
		}
		$hasCode = $hasLabel = false;
		$codePredicate = $code = $model = null;

		$applicationServices = $event->getApplicationServices();
		$dbProvider = $applicationServices->getDbProvider();
		$qbCount = $dbProvider->getNewQueryBuilder();
		$fb = $qbCount->getFragmentBuilder();
		$qb = $dbProvider->getNewQueryBuilder();

		foreach ($context['filter']['filters'] as $filter)
		{
			if ($filter['name'] === 'model')
			{
				$model = $event->getApplicationServices()->getModelManager()->getModelByName($filter['parameters']['value']);
				if ($model)
				{
					if ($model->hasProperty('code'))
					{
						$hasCode = true;
					}
					if ($model->hasProperty('label') && !$model->getProperty('label')->getStateless())
					{
						$hasLabel = true;
					}
				}
			}
			if ($filter['name'] === 'code')
			{
				$code = $filter['parameters']['value'];
				$codePredicate = $fb->like($fb->column('code', 'c'), $code);
			}
		}
		if ($model)
		{
			if ($hasLabel)
			{
				if ($hasCode && $code)
				{
					$qb->select($fb->column('document_id', 'd'), $fb->alias($fb->func('GROUP_CONCAT', 'c.code SEPARATOR ", "'), 'codes'),
							$fb->column('document_model', 'd'), $fb->alias($fb->column('code', 'doc'), 'documentCode'),
							$fb->alias($fb->column('label', 'doc'), 'label'));
					$qb->group($fb->column('label', 'doc'));
				}
				else
				{
					$qb->select($fb->column('document_id', 'd'), $fb->alias($fb->func('GROUP_CONCAT', 'c.code SEPARATOR ", "'), 'codes'),
							$fb->column('document_model', 'd'), $fb->alias($fb->column('label', 'doc'), 'label'));
					$qb->group($fb->column('label', 'doc'));
				}
			}
			else
			{
				if ($hasCode && $code)
				{
					$qb->select($fb->column('document_id', 'd'), $fb->alias($fb->func('GROUP_CONCAT', 'c.code SEPARATOR ", "'), 'codes'),
							$fb->column('document_model', 'd'), $fb->alias($fb->column('code', 'doc'), 'documentCode'));
				}
				else
				{
					$qb->select($fb->column('document_id', 'd'), $fb->alias($fb->func('GROUP_CONCAT', 'c.code SEPARATOR ", "'), 'codes'),
							$fb->column('document_model', 'd'));
				}
			}

			$qb->from($fb->alias($fb->getDocumentTable($model->getRootName()), 'doc'));
			$qb->innerJoin($fb->alias($fb->table('change_document'), 'd'), $fb->eq($fb->column('document_id', 'doc'), $fb->column('document_id', 'd')));
			$qb->leftJoin($fb->alias($fb->table('change_document_code'), 'c'), $fb->eq($fb->column('document_id', 'c'), $fb->column('document_id', 'd')));

			$qb->group($fb->column('document_id', 'c'))->group($fb->column('document_id', 'doc'));

			$qbCount->select($fb->alias($fb->count($fb->column('document_id', 'd')), 'count'));
			$qbCount->from($fb->alias($fb->getDocumentTable($model->getRootName()), 'doc'));
			$qbCount->innerJoin($fb->alias($fb->table('change_document'), 'd'), $fb->eq($fb->column('document_id', 'doc'), $fb->column('document_id', 'd')));
			$qbCount->leftJoin($fb->alias($fb->table('change_document_code'), 'c'), $fb->eq($fb->column('document_id', 'c'), $fb->column('document_id', 'd')));

			if ($hasCode && $code)
			{
				$qb->where($fb->logicOr($fb->logicAnd($codePredicate, $fb->eq($fb->column('document_model', 'd'), $fb->string($model->getShortName()))),
						$fb->like($fb->column('code', 'doc'), $code)));
				$qbCount->where($fb->logicOr($fb->logicAnd($codePredicate, $fb->eq($fb->column('document_model', 'd'), $fb->string($model->getShortName()))),
						$fb->like($fb->column('code', 'doc'), $code)));
				unset($context['filter']);
			}

			$array = $this->getListResult($context, $qbCount, $qb);
		}
		else
		{
			$qb->select($fb->column('document_id', 'd'), $fb->alias($fb->func('GROUP_CONCAT', 'c.code SEPARATOR ", "'), 'codes'),
					$fb->column('document_model', 'd'));
			$qb->from($fb->alias($fb->table('change_document'), 'd'));
			$qb->leftJoin($fb->alias($fb->table('change_document_code'), 'c'), $fb->eq($fb->column('document_id', 'c'), $fb->column('document_id', 'd')));
			$qb->group($fb->column('document_id', 'c'));

			$qbCount->select($fb->alias($fb->count('id'), 'count'));
			$qbCount->from($fb->alias($fb->table('change_document'), 'd'));
			$qbCount->leftJoin($fb->alias($fb->table('change_document_code'), 'c'), $fb->eq($fb->column('document_id', 'c'), $fb->column('document_id', 'd')));

			$array = $this->getListResult($context, $qbCount, $qb);
		}

		foreach ($array['items'] as &$result)
		{
			$result['_meta'] = ['id' => $result['document_id'], 'model' => $result['document_model']];
			$result['id'] = $result['document_id'];
			if (!empty($result['documentCode']))
			{
				$result['code'] = !empty($result['codes']) ? implode(',', [$result['codes'], $result['documentCode']]) : $result['documentCode'];
			}
			else
			{
				$result['code'] = $result['codes'];
			}
			if (empty($result['label']))
			{
				if (empty($result['label']) && !empty($result['documentCode']))
				{
					$result['label'] = $result['documentCode'];
				}
				else
				{
					$result['label'] = $result['codes'];
				}
			}
			$result['model'] = $result['document_model'];
		}
		unset($result);

		$result = $this->buildArrayResult($event, $array);
		$event->setResult($result);
	}

	public function detail(\Change\Http\Event $event)
	{
		$inputParameters = $event->getParam('inputParameters');
		$params = $inputParameters->toArray();
		$id = $params['id'];
		$context= $params['context'];

		$applicationServices = $event->getApplicationServices();
		$documentManger = $applicationServices->getDocumentManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();
		$document = $documentManger->getDocumentInstance($id);
		$codes = $documentCodeManager->getCodesByDocument($document, $context);
		$documentModel = $document->getDocumentModel();
		if ($documentModel->hasProperty('label'))
		{
			$label = $document->getLabel();
		}

	}

}