<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Http\API\Datadashboard;

/**
 * @name \Http\API\Datadashboard\Report
 */
class Report
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getToProcess(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$qbCount = $dbProvider->getNewQueryBuilder();
		$fb = $qbCount->getFragmentBuilder();
		$qbCount->select($fb->alias($fb->count('id'), 'count'));
		$qbCount->from('rbs_datadashboard_dat_report');

		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->allColumns());
		$qb->from('rbs_datadashboard_dat_report');
		$array = $this->getListResult($inputParameters->toArray(), $qbCount, $qb);

		foreach ($array['items'] as &$result)
		{
			$result['_meta'] = ['id' => $result['id'], 'model' => 'Rbs_Datadashboard_Report'];
			$result['askedDate'] = (new \DateTime($result['asked_date'], new \DateTimeZone('UTC')))->format(\DateTime::ATOM);
			$data = json_decode($result['data'], true);
			$result['data'] = json_decode($result['data'], true);
			if (isset($data['fileStats'], $data['fileStats']['import']))
			{
				$result['filesCount'] = count($data['fileStats']['import']);
			}
			elseif (isset($data['fileStats'], $data['fileStats']['validation']))
			{
				$result['filesCount'] = count($data['fileStats']['validation']);
			}
			else
			{
				$result['filesCount'] = 0;
			}
			$result['totalErrorsCount'] = isset($data['totalErrorsCount']) ? $data['totalErrorsCount'] : 0;
			$result['data'] = json_encode($result['data']);
			unset($result['asked_date']);
			unset($result['files']);
		}
		unset($result);

		$result = $this->buildArrayResult($event, $array);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function createReport(\Change\Http\Event $event)
	{
		$report = new \Rbs\Datadashboard\Report\Report();
		$report->setDbProvider($event->getApplicationServices()->getDbProvider());
		$applicationServices = $event->getApplicationServices();
		$report->setTransactionManager($applicationServices->getTransactionManager());
		$currentUser = $applicationServices->getAuthenticationManager()->getCurrentUser();
		$report->addData(['author' => $currentUser->getName(), 'authorId' => $currentUser->getId()]);
		$report->setAskedDate(new \DateTime('now', new \DateTimeZone('UTC')));
		$report->save();
		$report = $report->toArray();
		$report['_meta'] = ['id' => $report['id'], 'model' => 'Rbs_Datadashboard_Report'];
		$result = $this->buildArrayResult($event, ['item' => $report]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getDetail(\Change\Http\Event $event)
	{
		$report = new \Rbs\Datadashboard\Report\Report();
		$report->setDbProvider($event->getApplicationServices()->getDbProvider());
		$report->setTransactionManager($event->getApplicationServices()->getTransactionManager());
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$configuration = $event->getApplication()->getConfiguration();
		$emails = $configuration->getEntry('Rbs/Datadashboard/Contacts', []);
		$emails = implode(', ', $emails);

		$report->getReport($inputParameters->get('reportId'));
		if ($report->getId())
		{
			$report = $report->toArray();
			$report['emails'] = $emails;
			$report['_meta'] = ['id' => $report['id'], 'model' => 'Rbs_Datadashboard_Report'];
			$result = $this->buildArrayResult($event, ['item' => $report]);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function launchReport(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$report = new \Rbs\Datadashboard\Report\Report($inputParameters->get('report'));
		$report->setDbProvider($applicationServices->getDbProvider());
		$report->setTransactionManager($applicationServices->getTransactionManager());
		$report->setJobManager($applicationServices->getJobManager());
		$currentUser = $applicationServices->getAuthenticationManager()->getCurrentUser();

		if ($report->getStatus() === \Rbs\Datadashboard\Report\Report::INITIATED)
		{
			if (is_int($report->getOrigin()))
			{
				if ($report->getOrigin() === 0)
				{
					$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_GetFTPFiles', ['reportId' => $report->getId()]);
					$report->setStatus(\Rbs\Datadashboard\Report\Report::SCHEDULED);
					if ($report->getValidate())
					{
						$report->addData(['validationLauncher' => $currentUser->getName(), 'validationLauncherId' => $currentUser->getId()]);
					}
					if ($report->getImport())
					{
						$report->addData(['importLauncher' => $currentUser->getName(), 'importLauncherId' => $currentUser->getId()]);
					}
					$report->save();
				}
				else
				{
					if ($report->getValidate())
					{
						$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_Validate', ['reportId' => $report->getId()]);
						$report->setStatus(\Rbs\Datadashboard\Report\Report::SCHEDULED);
						$report->addData(['validationLauncher' => $currentUser->getName(), 'validationLauncherId' => $currentUser->getId()]);
						if ($report->getImport())
						{
							$report->addData(['importLauncher' => $currentUser->getName(), 'importLauncherId' => $currentUser->getId()]);
						}
						$report->save();
					}
					elseif ($report->getImport())
					{
						$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_Import', ['reportId' => $report->getId()]);
						$report->setStatus(\Rbs\Datadashboard\Report\Report::SCHEDULED);
						$report->addData(['importLauncher' => $currentUser->getName(), 'importLauncherId' => $currentUser->getId()]);
						$report->save();
					}
				}
			}
		}
		elseif ($report->getStatus() === \Rbs\Datadashboard\Report\Report::FINISHED)
		{
			$reportData = $report->getData();
			if ($report->getImport() && !isset($reportData['importJobId']))
			{
				$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_Import', ['reportId' => $report->getId()]);
				$report->setStatus(\Rbs\Datadashboard\Report\Report::SCHEDULED);
				$report->addData(['importLauncher' => $currentUser->getName(), 'importLauncherId' => $currentUser->getId()]);
				$report->save();
			}
		}
		$report = $report->toArray();
		$report['_meta'] = ['id' => $report['id'], 'model' => 'Rbs_Datadashboard_Report'];
		$result = $this->buildArrayResult($event, ['item' => $report]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function updateReport(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$reportArray = $inputParameters->get('report');
		$reportArray['id'] = $inputParameters->get('reportId');

		$report = new \Rbs\Datadashboard\Report\Report($reportArray);
		$report->setDbProvider($applicationServices->getDbProvider());
		$report->setTransactionManager($applicationServices->getTransactionManager());
		$report->setJobManager($applicationServices->getJobManager());
		$report->save();
		$report = $report->toArray();
		$report['_meta'] = ['id' => $report['id'], 'model' => 'Rbs_Datadashboard_Report'];
		$result = $this->buildArrayResult($event, ['item' => $report]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function listFiles(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$relativePath = $inputParameters->get('relativePath');
		$applicationServices = $event->getApplicationServices();
		$storageManager = $applicationServices->getStorageManager();
		$path = $storageManager->buildChangeURI('synchro', '/' . $relativePath)->toString();
		$storage = $storageManager->getStorageByStorageURI($path);
		$result = [];
		if ($storage->dir_opendir(0))
		{
			while (($file = $storage->dir_readdir()) !== false)
			{
				if (!in_array($file, ['.', '..'], false))
				{
					$result[] = $file;
				}
			}
		}
		$result = $this->buildArrayResult($event, ['items' => $result]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function removeFile(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$relativePath = $inputParameters->get('relativePath');
		$applicationServices = $event->getApplicationServices();
		$storageManager = $applicationServices->getStorageManager();
		$path = $storageManager->buildChangeURI('synchro', '/' . $relativePath)->toString();
		$items = [];
		if (file_exists($path))
		{
			@unlink($path);
			$storage = $storageManager->getStorageByStorageURI(dirname($path));
			if ($storage->dir_opendir(0))
			{
				while (($file = $storage->dir_readdir()) !== false)
				{
					if (!in_array($file, ['.', '..'], false))
					{
						$items[] = $file;
					}
				}
			}
		}
		$result = $this->buildArrayResult($event, ['items' => $items]);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function uploadFile(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$inputParameters->set('storageName', 'synchro');
		$storageHelper = new \Rbs\Ua\Http\API\Common\Storage();
		$storageHelper->setStorage($event);
	}
}