<?php

/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Http\API;

/**
 * @name \Rbs\Datadashboard\Http\API\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the EventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->onAction($event); });
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onAction(\Change\Http\Event $event)
	{
		if ($event->getAction())
		{
			return;
		}
		$applicationName = $event->getParam('applicationName');
		if ($applicationName === 'dataDashboard')
		{
			/** @var \Rbs\Ua\API\Definitions $definitions */
			$definitions = $event->getParam('definitions');
			if ($definitions)
			{
				$filePath = __DIR__ . '/../../Assets/Ua/rest.' . $applicationName . '.json';
				$definitions->initFromFile($filePath);
				$definitions->resolveApi($event);
			}
		}
	}
}