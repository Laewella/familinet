<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Job;

/**
 * @name \Rbs\Datadashboard\Job\DispatchFiles
 */
class GetFTPFiles
{

	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$report = new \Rbs\Datadashboard\Report\Report();
		$applicationServices = $event->getApplicationServices();
		$logging = $event->getApplication()->getLogging();
		$storageManager = $applicationServices->getStorageManager();
		$csvEngine = new \Change\Synchronization\CSV\CSVEngine($event->getApplication(), []);

		$report->setDbProvider($applicationServices->getDbProvider());
		$report->setTransactionManager($applicationServices->getTransactionManager());

		$job = $event->getJob();
		$reportId = $job->getArgument('reportId');
		if ($storageManager->getStorageByName('synchro'))
		{
			if ($reportId)
			{
				$report->getReport($reportId);
				$reportData = $csvEngine->preTreatment($report->getData(), 'FTP');
				$reportData = $csvEngine->aggregateFiles($reportData, 'FTP');
				$report->setData($reportData);
				$imports = $csvEngine->getSortedImport();
				$runProcess = false;
				$toCopy = [];
				foreach ($imports as $index => &$import)
				{
					$data = $csvEngine->resolveImportType($import);
					unset($imports[$index]);
					$allFiles = $csvEngine->getFiles($data['filePattern'], $data['modelName'], 'FTP');
					if (!$allFiles || !array_key_exists('files', $allFiles))
					{
						continue;
					}
					$files = $allFiles['files'];
					foreach ($files as $fileIndex => $file)
					{
						$isOk = $this->testFile($file);
						if (!$isOk)
						{
							$event->reported((new \DateTime())->add(new \DateInterval('PT3M')));
							return;
						}
						else
						{
							$toCopy[] = $isOk;
						}
					}
				}
				$allFiles = $csvEngine->getFiles('[a-zA-Z0-9-\-_]{1,}.(jpg|jpeg|png)', 'visuals', 'FTP');
				if (array_key_exists('files', $allFiles))
				{

					$files = $allFiles['files'];
					foreach ($files as $fileIndex => $file)
					{
						$isOk = $this->testFile($file);
						if (!$isOk)
						{
							$event->reported((new \DateTime())->add(new \DateInterval('PT3M')));
							return;
						}
						else
						{
							$toCopy[] = $isOk;
						}
					}
				}
				if ($toCopy)
				{
					$runProcess = true;
					foreach ($toCopy as $file)
					{
						$this->copyFile($storageManager, $file, $report);
					}
				}
				$report->addData([$toCopy]);
				if ($runProcess)
				{
					if ($report->getValidate())
					{
						$report->setStatus(\Rbs\Datadashboard\Report\Report::SCHEDULED);
						$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_Validate', ['reportId' => $report->getId()]);
					}
					elseif ($report->getImport())
					{
						$report->setStatus(\Rbs\Datadashboard\Report\Report::SCHEDULED);
						$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_Import', ['reportId' => $report->getId()]);
					}
				}
				$report->save();
			}
			else
			{
				$report->setStatus(\Rbs\Datadashboard\Report\Report::FINISHED);
				$report->save();
				$logging->warn('No report id for job ' . $event->getJob()->getId());
			}
		}
		else
		{
			$logging->error('Storage synchro must be defined.');
		}
	}

	/**
	 * @param string $sourceFile
	 * @return string
	 */
	protected function testFile($sourceFile)
	{
		if (time() - filemtime($sourceFile) < 180)
		{
			return false;
		}
		return $sourceFile;
	}

	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @param string $sourceFile
	 * @param \Rbs\Datadashboard\Report\Report $report
	 * @return bool
	 */
	protected function copyFile($storageManager, $sourceFile, $report)
	{
		$localPath = $storageManager->buildChangeURI('synchro', '/in/' . $report->getId() . '/')->toString();
		$fileName = basename($sourceFile);
		return rename($sourceFile, $localPath . $fileName);
	}
}