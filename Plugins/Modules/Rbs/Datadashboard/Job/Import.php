<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Job;

/**
 * @name \Rbs\Datadashboard\Job\Import
 */
class Import extends \Change\Job\Job
{

	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$time = -microtime(true);
		$job = $event->getJob();
		$reportId = $job->getArgument('reportId');
		$report = new \Rbs\Datadashboard\Report\Report();
		$applicationServices = $event->getApplicationServices();
		$application = $event->getApplication();
		$report->setDbProvider($applicationServices->getDbProvider());
		$report->setTransactionManager($applicationServices->getTransactionManager());
		if ($reportId)
		{
			$report->getReport($reportId);
		}
		else
		{
			$report->setAskedDate((new \DateTime('now', new \DateTimeZone('UTC'))));
			$report->save();
		}
		$report->setStatus(\Rbs\Datadashboard\Report\Report::RUNNING);
		$report->addData(['importJobId' => $job->getId(), 'importRunningDate' => (new \DateTime())->format(\DateTime::ATOM)]);
		$report->save();
		$commonConfigArray = [
			'additionalFilePart' => '/' . $report->getId() . '/'
		];
		try
		{
			$csvEngine = new \Change\Synchronization\CSV\CSVEngine($event->getApplication(), $commonConfigArray);
			$reportData = $csvEngine->import();
			$report->addData(isset($reportData) ? $reportData : null);
			$time += microtime(true);
			$totalTime = $time;
			$dataErrors = 0;
			$structureErrors = 0;
			if ($reportData['fileStats']['import'])
			{
				foreach ($reportData['fileStats']['import'] as $data)
				{
					$structureErrors += isset($data['structureErrorsCount']) ? $data['structureErrorsCount'] : 0;
					$dataErrors += isset($data['dataErrorsCount']) ? $data['dataErrorsCount'] : 0;
				}
			}

			$data = $report->getData();
			$data['totalErrorsCount'] =
				$dataErrors + $structureErrors + (isset($data['totalErrorsCount']) ? $data['totalErrorsCount'] : 0);
			$report->setData($data);

			if (isset($data['executionTime']))
			{
				$totalTime += (float)$data['executionTime'];
			}
			$report->addData(
				[
					'import' => [
						'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
						'executionTime' => number_format($time, 2) . ' seconds'
					]
				]
			);
			$reportData = $report->getData();
			$reportData['executionTime'] = number_format($totalTime, 2);
			$report->setData($reportData);
			if ($structureErrors > 0 || $dataErrors > 0)
			{
				$this->handleNotification($application, $applicationServices, $report);
				$report->setStatus(\Rbs\Datadashboard\Report\Report::ERROR);
			}
			else
			{
				$report->setStatus(\Rbs\Datadashboard\Report\Report::FINISHED);
			}
			$report->save();
		}
		catch (\Exception $e)
		{
			$report->setStatus(\Rbs\Datadashboard\Report\Report::JOB_FAILED);
			$this->handleNotification($application, $applicationServices, $report, $e);
			$data['trace']['message'] = $e->getMessage();
			$data['trace']['trace'] = $e->getTraceAsString();
			$report->addData($data);
			$report->save();
			throw $e;
		}
	}

	/**
	 * @param \Change\Application $application
	 * @param \change\Services\ApplicationServices $applicationServices
	 * @param \Rbs\Datadashboard\Report\Report $report
	 * @param \Exception $e
	 */
	protected function handleNotification($application, $applicationServices, $report, $e = null)
	{
		$data = $report->getData();
		$emails = [];
		if ($data['notifyDefaultList'])
		{
			$emails = $application->getConfiguration('Rbs/Datadashboard/Contacts');
		}
		if (isset($data['notifyExtraPeople']))
		{
			if ($extraEmails = explode(';', $data['notifyExtraPeople']))
			{
				$emails = array_merge($emails, $extraEmails);
			}
		}
		if (!$emails)
		{
			$application->getLogging()->error('Empty emails list. Can\'t send notification.');
			return;
		}
		$uuid = $application->getConfiguration('Change/Application/uuid');
		if (empty($uuid))
		{
			$application->getLogging()->error('Application uuid (Change/Application/uuid) must be defined in configuration.');
			return;
		}
		$files = 0;
		$global = 0;
		$content = ['Notification for platform : <b>' . $uuid . '</b>'];
		$content[] = '';
		$reportURL = $application->getConfiguration('Rbs/Datadashboard/reportURL');
		if (!empty($reportURL))
		{
			$content[] = 'Rapport complet disponible <a href="' . $reportURL . $report->getId() .'">ici</a>';
			$content[] = '';
		}


		$structureErrors = [];
		$dataErrors = [];
		foreach ($data['fileStats']['import'] as $file => $importData)
		{
			if (isset($importData['structureErrorsCount']) && $importData['structureErrorsCount'] > 0)
			{
				$structureErrors[] = '<li>' . $file . ' with ' . $importData['structureErrorsCount'] . ' errors</li>';
				$files++;
			}
			if (isset($importData['dataErrorsCount']) && $importData['dataErrorsCount'] > 0)
			{
				$dataErrors[] = '<li>' . $file . ' with ' . $importData['dataErrorsCount'] . ' errors</li>';
				$files++;
			}
		}
		if ($structureErrors)
		{
			$content[] = 'File(s) with structure errors :';
			$content[] = '';
			$content[] = '<ul>';
			$content[] = implode('', $structureErrors);
			$content[] = '</ul>';
			$content[] = '';
		}

		if ($dataErrors)
		{
			$content[] = 'File(s) with data errors :';
			$content[] = '';
			$content[] = '<ul>';
			$content[] = implode('', $dataErrors);
			$content[] = '</ul>';
			$content[] = '';
		}

		if ($e)
		{
			$content[] = 'An exception has been raised :';
			$content[] = '';
			$content[] = $e->getMessage() . '<br/>' . $e->getTraceAsString();
			$content[] = '';
			$global = 1;
		}
		$all = (count($structureErrors) + count($dataErrors) + $global);
		$subject = $files . ' file' . ($all > 1 ? 's' : '') . ' in error for platform ' . $uuid;
		// Send email
		$mailManager = $applicationServices->getMailManager();
		$message = $mailManager->prepareMessage(['monitoring-omn@proximis.com'], $emails, $subject, implode('<br/>', $content));
		$mailManager->send($message);
	}

}