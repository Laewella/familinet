<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Job;

/**
 * @name \Rbs\Datadashboard\Job\ScheduledImport
 */
class CheckFTPFiles
{

	/**
	 * @param \Change\Job\Event $event
	 */
	public function execute(\Change\Job\Event $event)
	{
		$configuration = $event->getApplication()->getConfiguration();
		$workerEnabled = $configuration->getEntry('Rbs/Datadashboard/FTPWorkerEnabled', true);
		if ($workerEnabled)
		{
			$report = null;
			$applicationServices = $event->getApplicationServices();
			$jobManager = $applicationServices->getJobManager();
			$csvEngine = new \Change\Synchronization\CSV\CSVEngine($event->getApplication(), []);
			$imports = $csvEngine->getSortedImport();
			foreach ($imports as $index => $import)
			{
				$data = $csvEngine->resolveImportType($import);
				$allFiles = $csvEngine->getFiles($data['filePattern'], $data['modelName'], 'FTP');
				if ($allFiles && isset($allFiles['files']))
				{
					$report = new \Rbs\Datadashboard\Report\Report();
					$applicationServices = $event->getApplicationServices();
					$report->setDbProvider($applicationServices->getDbProvider());
					$report->setTransactionManager($applicationServices->getTransactionManager());
					$report->setOrigin(0);
					$report->setImport(true);
					$report->setAskedDate((new \DateTime('now', new \DateTimeZone('UTC'))));
					$report->addData(['importLauncher' => 'Automatic', 'importLauncherId' => 0]);
					$report->save();
					break;
				}
			}
			if ($report)
			{
				$jobManager->createNewJob('Rbs_Datadashboard_GetFTPFiles', ['reportId' => $report->getId()]);
			}
			$rescheduleTime = $configuration->getEntry('Rbs/Datadashboard/rescheduleTime', 60);
			if ($rescheduleTime < 10)
			{
				$rescheduleTime = 10;
			}
			$dateTime = (new \DateTime())->add(new \DateInterval('PT' . $rescheduleTime . 'M'));
			$event->reported($dateTime);
		}
	}
}