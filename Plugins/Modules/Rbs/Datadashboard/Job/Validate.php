<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Job;

/**
 * @name \Rbs\Datadashboard\Job\Validate
 */
class Validate extends \Change\Job\Job
{

	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{

		$execTime = -microtime(true);
		$job = $event->getJob();
		$reportId = $job->getArgument('reportId');
		$report = new \Rbs\Datadashboard\Report\Report();
		$applicationServices = $event->getApplicationServices();
		$report->setDbProvider($applicationServices->getDbProvider());
		$report->setTransactionManager($applicationServices->getTransactionManager());
		$commonConfigArray = [];
		if ($reportId)
		{
			$report->getReport($reportId);
			$report->setStatus(\Rbs\Datadashboard\Report\Report::RUNNING);
			$commonConfigArray = [
				'additionalFilePart' => '/' . $report->getId() . '/',
				'loggerPath' => '/' . $report->getId() . '/'
			];
		}
		else
		{
			$report->setAskedDate(new \DateTime('now', new \DateTimeZone('UTC')));
			$report->setStatus(\Rbs\Datadashboard\Report\Report::RUNNING);
		}
		$report->addData(['validationJobId' => $job->getId(), 'validationRunningDate' => (new \DateTime())->format(\DateTime::ATOM)]);
		$report->save();
		try
		{
			$reportData = (new \Change\Synchronization\CSV\CSVEngine($event->getApplication(), $commonConfigArray))->validate();
			$report->addData($reportData);
			$structureErrors = 0;
			$dataErrors = 0;
			if ($reportData['fileStats']['validation'])
			{
				foreach ($reportData['fileStats']['validation'] as $data)
				{
					$structureErrors += isset($data['structureErrorsCount']) ? $data['structureErrorsCount'] : 0;
					$dataErrors += isset($data['dataErrorsCount']) ? $data['dataErrorsCount'] : 0;
				}
			}
			$execTime += microtime(true);
			$time = false;
			$report->addData(['totalErrorsCount' => $dataErrors + $structureErrors]);
			if ($structureErrors > 0 || $dataErrors > 0)
			{
				$report->setStatus(\Rbs\Datadashboard\Report\Report::ERROR);
			}
			else
			{
				if ($report->getImport())
				{
					$report->setStatus(\Rbs\Datadashboard\Report\Report::RUNNING);
					$applicationServices->getJobManager()->createNewJob('Rbs_Datadashboard_Import', ['reportId' => $report->getId()]);
				}
				else
				{
					$report->setStatus(\Rbs\Datadashboard\Report\Report::FINISHED);
					$time = number_format($execTime, 2);
				}
			}
			$report->addData(
				[
					'executionTime' => $execTime,
					'validationErrors' => $structureErrors + $dataErrors,
					'validate' => [
						'memory_get_peak_usage' => memory_get_peak_usage(true) / 1048576 . ' Mo',
						'executionTime' => $time
					]
				]);
			$report->save();
		}
		catch (\Exception $e)
		{
			$report->setStatus(\Rbs\Datadashboard\Report\Report::JOB_FAILED);
			$data['trace']['message'] = $e->getMessage();
			$data['trace']['trace'] = $e->getTraceAsString();
			$report->addData($data);
			$report->save();
			throw $e;
		}
	}

}