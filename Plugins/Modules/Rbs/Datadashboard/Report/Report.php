<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Report;

/**
 * @name \Rbs\Datadashboard\Report\Report
 */
class Report
{
	const INITIATED = 'INITIATED';

	const SCHEDULED = 'SCHEDULED';

	const RUNNING = 'RUNNING';

	const ERROR = 'ERROR';

	const JOB_FAILED = 'JOB_FAILED';

	const FINISHED = 'FINISHED';

	/** @var integer */
	private $id;

	/** @var \DateTime */
	private $askedDate;

	/** @var  integer */
	private $origin;

	/** @var  float */
	private $runningTime;

	/** @var  integer */
	private $errorCount;

	/** @var  integer */
	private $linesCount;

	/** @var string */
	private $status;

	/** @var  boolean */
	private $import = false;

	/** @var  boolean */
	private $validate = false;

	/** @var  array */
	private $data = [];

	/** @var \Change\Db\DbProvider */
	protected $dbProvider;

	/** @var \Change\Transaction\TransactionManager */
	protected $transactionManager;

	/** @var [] $meta */
	protected $_meta;

	/** @var \Change\Job\JobManager */
	protected $jobManager;

	/**
	 * Report constructor.
	 * @param null|array $data
	 */
	public function __construct($data = null)
	{
		if ($data && is_array($data))
		{
			$this->fromArray($data);
		}
	}

	/**
	 * @return \DateTime|null
	 */
	public function getAskedDate()
	{
		return $this->askedDate;
	}

	/**
	 * @return int
	 */
	public function getErrorCount()
	{
		return $this->errorCount;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function getLinesCount()
	{
		return $this->linesCount;
	}

	/**
	 * @return float
	 */
	public function getRunningTime()
	{
		return $this->runningTime;
	}

	/**
	 * @return \Change\Db\DbProvider
	 */
	public function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @return \Change\Transaction\TransactionManager
	 */
	public function getTransactionManager()
	{
		return $this->transactionManager;
	}

	/**
	 * @return \Change\Job\JobManager
	 */
	public function getJobManager()
	{
		return $this->jobManager;
	}

	/**
	 * @return array
	 */
	public function getMeta()
	{
		if (!$this->_meta)
		{
			$this->_meta = ['_id' => $this->getId(), 'model' => 'Rbs_Datadashboard_Report'];
		}
		return $this->_meta;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @return bool
	 */
	public function getImport()
	{
		return $this->import;
	}

	/**
	 * @return bool
	 */
	public function getValidate()
	{
		return $this->validate;
	}

	/**
	 * @return int
	 */
	public function getOrigin()
	{
		return $this->origin;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param \DateTime|string $askedDate
	 * @return $this
	 */
	public function setAskedDate($askedDate)
	{
		if (is_string($askedDate))
		{
			$this->askedDate = new \DateTime($askedDate);
		}
		elseif ($askedDate instanceof \DateTime)
		{
			$this->askedDate = $askedDate;
		}
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider($dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @return $this
	 */
	public function setTransactionManager($transactionManager)
	{
		$this->transactionManager = $transactionManager;
		return $this;
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 * @return $this
	 */
	public function setJobManager($jobManager)
	{
		$this->jobManager = $jobManager;
		return $this;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @param boolean $import
	 * @return $this
	 */
	public function setImport($import)
	{
		$this->import = $import;
		return $this;
	}

	/**
	 * @param boolean $validate
	 * @return $this
	 */
	public function setValidate($validate)
	{
		$this->validate = $validate;
		return $this;
	}

	/**
	 * @param integer $origin
	 * @return $this
	 */
	public function setOrigin($origin)
	{
		$this->origin = $origin;
		return $this;
	}

	/**
	 * @param string|array $data
	 * @return $this
	 */
	public function setData($data = [])
	{
		if ($this->data === null)
		{
			$this->data = [];
		}
		if ($data && is_string($data))
		{
			$json = json_decode($data, true);
			if (!$json)
			{
				return $this;
			}
			if (is_array($json))
			{
				$this->data = $json;
			}
		}
		else if ($data && is_array($data))
		{
			$this->data = $data;
		}
		return $this;
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	public function addData($data = [])
	{
		if ($this->data === null)
		{
			$this->data = [];
		}
		if ($data && is_string($data))
		{
			$json = json_decode($data, true);
			if (!$json)
			{
				return $this;
			}
			if (is_array($json))
			{
				$this->data = array_replace($this->data, $json);
			}
		}
		else if ($data && is_array($data))
		{
			$this->data = array_merge_recursive($this->data, $data);
		}
		return $this;
	}

	/**
	 * @throws \Change\Transaction\RollbackException
	 */
	public function save()
	{
		$tm = $this->getTransactionManager();
		try
		{
			$tm->begin();
			if ($this->getId() !== null)
			{
				$this->update();
			}
			else
			{
				$this->create();
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
			(new \Change\Application())->getLogging()->exception($e);
		}
	}

	/**
	 * @param integer $identifier
	 */
	public function getReport($identifier)
	{
		$provider = $this->getDbProvider();
		$qb = $provider->getNewQueryBuilder();
		$fragment = $qb->getFragmentBuilder();
		$query = $qb->select($fragment->allColumns());
		$query->from('rbs_datadashboard_dat_report');
		$query->where($fragment->eq($fragment->column('id'), $identifier));
		$selectQuery = $query->query();
		$result = $selectQuery->getFirstResult();
		if ($result)
		{
			$result['askedDate'] = (new \DateTime($result['asked_date'], new \DateTimeZone('UTC')))->format(\DateTime::ATOM);
			unset($result['asked_date']);
			$this->fromArray($result);
		}
	}

	/**
	 * @throws \Exception
	 */
	protected function update()
	{
		$provider = $this->getDbProvider();
		$sb = $provider->getNewStatementBuilder();
		$fragment = $sb->getFragmentBuilder();

		$query = $sb->update('rbs_datadashboard_dat_report');
		$query->assign($fragment->column('asked_date'), $fragment->dateTimeParameter('askedDate'));
		$query->assign($fragment->column('origin'), $fragment->integerParameter('origin'));
		$query->assign($fragment->column('data'), $fragment->lobParameter('data'));
		$query->assign($fragment->column('status'), $fragment->parameter('status'));
		$query->assign($fragment->column('validate'), $fragment->booleanParameter('validate'));
		$query->assign($fragment->column('import'), $fragment->booleanParameter('import'));
		$query->where($fragment->eq($fragment->column('id'), $this->getId()));

		$updateQuery = $query->updateQuery();
		$updateQuery->bindParameter('askedDate', $this->getAskedDate() ?? new \DateTime());
		$updateQuery->bindParameter('origin', $this->getOrigin());
		$updateQuery->bindParameter('data', json_encode($this->getData()));
		$updateQuery->bindParameter('status', $this->getStatus());
		$updateQuery->bindParameter('validate', $this->getValidate());
		$updateQuery->bindParameter('import', $this->getImport());
		$updateQuery->execute();
	}

	protected function create()
	{
		$provider = $this->getDbProvider();
		$sb = $provider->getNewStatementBuilder();
		$fragment = $sb->getFragmentBuilder();
		$sb->insert($fragment->table('rbs_datadashboard_dat_report'), $fragment->column('asked_date'), $fragment->column('data'),
			$fragment->column('origin'), $fragment->column('import'), $fragment->column('validate'));
		$sb->addValue($fragment->dateTimeParameter('askedDate'));
		$sb->addValue($fragment->parameter('data'));
		$sb->addValue($fragment->integerParameter('origin'));
		$sb->addValue($fragment->booleanParameter('import'));
		$sb->addValue($fragment->booleanParameter('validate'));

		$this->setAskedDate($askedDate = $this->getAskedDate() ?? new \DateTime());
		$this->addData(['files' => [], 'notifyDefaultList' => true]);
		$insertQuery = $sb->insertQuery();
		$insertQuery->bindParameter('askedDate', $askedDate);
		$insertQuery->bindParameter('data', json_encode($this->getData()));
		$insertQuery->bindParameter('origin', 1);
		$insertQuery->bindParameter('import', $this->getImport());
		$insertQuery->bindParameter('validate', $this->getValidate());
		$insertQuery->execute();
		$this->setId($provider->getLastInsertId('rbs_datadashboard_dat_report'));
		$this->setData(['files' => []]);
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	protected function fromArray($data)
	{
		foreach ($data as $key => $value)
		{
			$callback = [$this, 'set' . ucfirst($key)];
			if (is_callable($callback))
			{
				$callback($value);
			}
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$data = [
			'id' => $this->getId(),
			'askedDate' => $this->getAskedDate(),
			'origin' => $this->getOrigin(),
			'data' => $this->getData(),
			'status' => $this->getStatus(),
			'validate' => $this->getValidate(),
			'import' => $this->getImport(),
			'_meta' => $this->getMeta(),
			'model' => 'Rbs_Datadashboard_Report'
		];
		return $data;
	}
}