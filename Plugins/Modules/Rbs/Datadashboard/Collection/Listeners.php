<?php

/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Collection;

/**
 * @name \Rbs\Datadashboard\Collection\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			if (!$event->getParam('collection'))
			{
				switch ($event->getParam('code'))
				{
					case 'Rbs_Datadashboard_DocumentTypes':
						(new \Rbs\Datadashboard\Collection\Collections())->addDocumentTypes($event);
						break;
				}
			}
		};
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_COLLECTION, $callback, 10);
	}
}