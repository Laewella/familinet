<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Collection;

/**
 * @name \Rbs\Datadashboard\Collection\Collections
 */
class Collections
{

	public function addDocumentTypes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18n = $applicationServices->getI18nManager();
		$modelManager = $applicationServices->getModelManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();
		$configFiles = (array)$event->getApplication()->getConfiguration('Rbs/Admin/Asides/codeConfig');
		foreach ($configFiles as &$configFile)
		{
			$configFile = $event->getApplication()->getWorkspace()->appPath('Config', $configFile);
			if (!file_exists($configFile))
			{
				$configFile = null;
			}
		}
		unset($configFile);
		$items = [];
		$asideModels = [];
		$configFiles = array_filter($configFiles);
		if ($configFiles)
		{
			$configuration = new \Change\Configuration\Configuration($configFiles);
			$contexts = $configuration->getEntry('Contexts');
			if (is_array($contexts))
			{
				/** @var array $contexts */
				foreach ($contexts as $context => $contextData)
				{
					$contextId = $documentCodeManager->resolveContextId($context);
					foreach ($contextData['models'] as $modelName)
					{
						$model = $modelManager->getModelByName($modelName);
						if ($model)
						{
							$pluginKey = strtolower('m.' . $model->getVendorName() . '.' . $model->getShortModuleName() . '.admin.module_name');
							$pluginLabel = $i18n->trans($pluginKey, ['ucf']);
							$items[$contextId . '|' . $modelName] = $pluginLabel . ' > ' . new \Change\I18n\I18nString($i18n,
											$model->getLabelKey(), ['ucf']);
							$asideModels[] = $model->getName();
						}
					}
				}
				foreach ($modelManager->getModelsNames() as $modelName)
				{
					if (!in_array($modelName, $asideModels))
					{
						$model = $modelManager->getModelByName($modelName);
						if ($model->hasProperty('code') && !$model->isAbstract())
						{
							$pluginKey = strtolower('m.' . $model->getVendorName() . '.' . $model->getShortModuleName() . '.admin.module_name');
							$pluginLabel = $i18n->trans($pluginKey, ['ucf']);
							$items['none' . '|' . $modelName] = $pluginLabel . ' > ' . new \Change\I18n\I18nString($i18n,
											$model->getLabelKey(), ['ucf']);
						}
					}
				}
				asort($items);
			}
		}
		$collection = new \Change\Collection\CollectionArray('Rbs_Datadashboard_DocumentTypes', $items);
		$event->setParam('collection', $collection);
	}
}