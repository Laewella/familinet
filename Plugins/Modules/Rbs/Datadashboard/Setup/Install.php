<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Setup;

/**
 * @name \Rbs\Datadashboard\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/Http/UA/UI/Rbs_Datadashboard', \Rbs\Datadashboard\Http\UI\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/UA/API/Rbs_Datadashboard', \Rbs\Datadashboard\Http\API\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/CollectionManager/Rbs_Datadashboard', \Rbs\Datadashboard\Collection\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/JobManager/Rbs_Datadashboard', \Rbs\Datadashboard\Events\JobManager\Listeners::class);

		$configuration->addPersistentEntry('Rbs/Datadashboard/rescheduleTime', 60);
		$configuration->addPersistentEntry('Rbs/Datadashboard/FTPWorkerEnabled', true);
		$configuration->addPersistentEntry('Rbs/Datadashboard/reportURL', '');
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		parent::executeThemeAssets($plugin, $pluginManager, $application);
		$manager = new \Rbs\Datadashboard\Http\UI\Resources('dataDashboard');
		$manager->setApplication($application)->setPluginManager($pluginManager);
		$assetManager = $manager->getNewAssetManager();
		$manager->registerAssets($assetManager);
		$manager->write($assetManager);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \RuntimeException
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$registration = new \Rbs\Ua\Setup\Registration();
		$applicationNames = ['dataDashboard'];
		$registration->installApplication($plugin, $applicationNames, $applicationServices);

		$jobManager = $applicationServices->getJobManager();
		$name = 'Rbs_Datadashboard_CheckFTPFiles';
		$ids = $jobManager->getJobIdsByName($name);

		if (count($ids) === 0)
		{
			$jobManager->createNewJob($name, null, null, true);
		}
		else
		{
			$first = true;
			foreach ($ids as $id)
			{
				$job = $jobManager->getJob($id);
				if (!$job)
				{
					continue;
				}
				if ($first && $job->getStatus() !== \Change\Job\JobInterface::STATUS_WAITING)
				{
					$jobManager->updateJobStatus($job, \Change\Job\JobInterface::STATUS_WAITING,
							['reportedAt' => new \DateTime()]);
				}
				else if (!$first && $job->getStatus() !== \Change\Job\JobInterface::STATUS_FAILED)
				{
					$jobManager->updateJobStatus($job, \Change\Job\JobInterface::STATUS_FAILED);
				}
				$first = false;
			}
		}
	}
}
