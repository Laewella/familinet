<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Datadashboard\Setup;

/**
 * @name \Rbs\Datadashboard\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	const REPORT_TABLE = 'rbs_datadashboard_dat_report';

	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			$this->tables[self::REPORT_TABLE] = $td = $schemaManager->newTableDefinition(self::REPORT_TABLE);
			$td->addField($schemaManager->newIntegerFieldDefinition('id')->setNullable(false)->setAutoNumber(true))
				->addField($schemaManager->newDateFieldDefinition('asked_date')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('origin')->setLength(1)->setNullable(false))
				->addField($schemaManager->newBooleanFieldDefinition('import')->setNullable(true))
				->addField($schemaManager->newBooleanFieldDefinition('validate')->setNullable(true))
				->addField($schemaManager->newLobFieldDefinition('data')->setNullable(true))
				->addField($schemaManager->newEnumFieldDefinition('status',
						['VALUES' => ['INITIATED', 'SCHEDULED', 'RUNNING', 'ERROR', 'JOB_FAILED', 'FINISHED']])->setNullable(false)->setDefaultValue('INITIATED'))
				->addKey($this->newPrimaryKey()->addField($td->getField('id')))
				->setOption('AUTONUMBER', 1);
		}
		return $this->tables;
	}
}