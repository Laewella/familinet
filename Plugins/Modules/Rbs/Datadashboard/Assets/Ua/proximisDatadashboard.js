/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	/**
	 * @ngdoc module
	 * @name proximisDatadashboard
	 *
	 * @description
	 * The proximisDatadashboard module contains all components for Datadashboard applications.
	 */
	var app = angular.module('proximisDatadashboard', ['proximis', 'ngResource', 'ngSanitize', 'ngTouch', 'ngCookies', 'ngAnimate',
		'ngMessages', 'ui.bootstrap', 'angularFileUpload']);

	// Configuration.

	app.config(['$locationProvider', '$interpolateProvider', 'localStorageServiceProvider', 'proximisRestOAuthProvider',
		'proximisRestApiProvider', 'proximisRouteProvider', 'proximisRestApiDefinitionProvider', 'proximisUserProvider', 'ProximisGlobal',
		'ProximisRestGlobal',
		function($locationProvider, $interpolateProvider, localStorageServiceProvider, proximisRestOAuthProvider, proximisRestApiProvider, proximisRouteProvider, proximisRestApiDefinitionProvider, proximisUserProvider, ProximisGlobal, ProximisRestGlobal) {
			$locationProvider.html5Mode(true);
			$interpolateProvider.startSymbol('(=').endSymbol('=)');

			/** @var {string} ProximisGlobal.role */
			localStorageServiceProvider.setPrefix('_Datadashboard_' + ProximisGlobal.role + '_');

			var restUrl = ProximisRestGlobal.restURL;
			var oauthUrl = restUrl + 'OAuth/';
			proximisRestOAuthProvider.setBaseUrl(oauthUrl);
			proximisRestOAuthProvider.setRealm(ProximisRestGlobal.OAuth.realm);
			// Sign all the requests on our REST services...
			proximisRestOAuthProvider.setSignedUrlPatternInclude(restUrl);
			// ... but do NOT sign OAuth requests.
			proximisRestOAuthProvider.setSignedUrlPatternExclude(oauthUrl);

			proximisRestApiProvider.setBaseUrl(restUrl);
			proximisRestApiDefinitionProvider.applyDefinition(ProximisRestGlobal.restApi);

			proximisRouteProvider.setRoutesDefaultModule('Rbs_Datadashboard');
			proximisRouteProvider.applyRoutes(ProximisGlobal.routes);

			proximisUserProvider.setProfileNames(['Change_User', 'Rbs_Datadashboard', 'Rbs_Datadashboard_' + ProximisGlobal.role]);

		}
	]);

	// Controllers.

	/**
	 * This Controller is bound to the <body/> tag and is, thus, the "root Controller".
	 * Mostly, it deals with user authentication and settings.
	 */
	app.controller('ProximisDatadashboardRootController',
		['$scope', '$rootScope', '$location', 'proximisUser', 'ProximisRestEvents',
			function(scope, $rootScope, $location, proximisUser, ProximisRestEvents) {
				var initialized = false;
				scope.loading = true;
				scope.showMainMenu = true;

				$rootScope.$on('$locationChangeSuccess', function() {
					if (!initialized) {
						initialized = true;
						proximisUser.init();
					}
					else if ($location.path() === '/OAuth/Authorize') {
						scope.loading = false;
					}
				});

				proximisUser.ready().then(function() {
					scope.loading = false;
				});

				$rootScope.$on(ProximisRestEvents.AuthenticationSuccess, function(event, args) {
					proximisUser.load().then(function() {
						if (args['callback']) {
							$location.url(args['callback']);
						}
					});
				});

				scope.logout = function() {
					$rootScope.$emit(ProximisRestEvents.Logout);
				};

			}
		]
	);
})();