/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc controller
	 * @id proximis.controller:ProximisDatadashboardHomeController
	 * @name ProximisDatadashboardHomeController
	 *
	 * @description
	 * A controller building the documentation url version.
	 */
	app.controller('ProximisDatadashboardHomeController', ['$scope', 'proximisRestApi', 'ProximisGlobal',
		function(scope, proximisRestApi, ProximisGlobal) {
			scope.version = null;
			if (ProximisGlobal.application.version != null) {
				if (ProximisGlobal.application.version.indexOf('[') === 0) {
					ProximisGlobal.application.version = ProximisGlobal.application.version.substring(1, (ProximisGlobal.application.version.length - 1));
				}

				var versionPart = ProximisGlobal.application.version.split('.');
				if (versionPart[2] === 'dev') {
					scope.documentationURL = 'https://gitlab.change-commerce.com/change/Doc/wikis/import-summary';
				}
				else {
					scope.version = versionPart[0] + '.' + versionPart[1] + '.0';
					scope.documentationURL = 'http://doc.change-commerce.com/omn/' + scope.version + '/developer/import-summary.html';

				}
			}

			scope.regex = null;
			proximisRestApi.get('getFilePatterns').then(
				function(result) {
					if (result.data.patterns != null) {
						scope.filePatterns = result.data.patterns;
						scope.regex = new RegExp(scope.filePatterns, 'i');
					}
				}
			);
		}
	]);
})();