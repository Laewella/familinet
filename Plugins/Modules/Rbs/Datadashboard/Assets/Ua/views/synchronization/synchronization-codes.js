/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	var app = angular.module('proximisDatadashboard');
	app.controller('ProximisDatadashboardSynchonizationCodesListController', ['$scope', '$timeout', 'proximisCoreI18n', 'proximisRestApi', 'proximisRestApiDefinition',
		'proximisModalStack', 'proximisMessages', ProximisDatadashboardSynchonizationCodesListController]);

	function ProximisDatadashboardSynchonizationCodesListController(scope, $timeout, proximisCoreI18n, proximisRestApi, proximisRestApiDefinition, proximisModalStack,
			proximisMessages) {
		//filters
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return true;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.none.isValid(filter)) {
						return null;
					}
					return prepareFilter(filter, null);
				}
			},
			code: {
				isValid: function(filter) {
					return filter.parameters.code && filter.parameters.model;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.code.isValid(filter)) {
						return null;
					}
					scope.submitCode = false;
					return prepareFilter(filter, [{
						name: 'code',
						parameters: {
							propertyName: 'c`.`code', operator: 'contains', value: filter.parameters.code
						}
					}]);
				}
			}
		};

		function prepareFilter(filter, conditions) {
			if (!conditions && !filter.parameters['model'] || conditions && !filter.parameters['model']) {
				return null;
			}

			var staticFilter = {
				name: 'group',
				parameters: { operator: 'AND' },
				filters: []
			};

			if (conditions) {
				staticFilter.filters = conditions;
			}

			if (filter.parameters['model']) {
				var values = filter.parameters.model.split("|");
				staticFilter.filters.push({
					name: 'model',
					parameters: {
						propertyName: 'd`.`document_model',
						operator: 'eq',
						value: values[1]
					}
				});
			}
			return staticFilter;
		}
		var filterTextTimeout;
		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter', function(filterInfo) {
				if (filterInfo) {
					if (filterTextTimeout) {
						$timeout.cancel(filterTextTimeout);
					}
					var hasCode = false;
					if (filterInfo.parameters.code) {
						hasCode = true;
					}
					if (hasCode) {
						filterTextTimeout = $timeout(function() {
							scope.$broadcast('filterUpdated', filterInfo.compiled);
						}, 500);
					}
					else {
						scope.$broadcast('filterUpdated', filterInfo.compiled);
					}
				}
			}, true);
		});

		scope.openAddCodeModal = function() {
			proximisModalStack.open({
				templateUrl: 'Rbs/Datadashboard/Ua/views/synchronization/detail-modal-content.twig',
				size: 'lg',
				controller: 'ProximisDatadashboardAddCodeController',
				resolve: {
					refreshParent: function() { return scope.actionMethods.refresh; }
				}
			});
		};

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Datadashboard/Ua/views/synchronization/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisDatadashboardAddCodeController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};
	}

	app.controller('ProximisDatadashboardAddCodeController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisDatadashboardAddCodeController]);

	function ProximisDatadashboardAddCodeController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
			proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: false,
			savingData: false,
			errorMessage: null,
			detail: {
				common: {
					label: null,
					content: []
				}
			}
		};

		var params = { documentId: parentData.documentId };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_SynchronizationCodes.detail', 'GET', params)).then(
				function(result) {
					scope.modalData.detail = result.data.item;
					scope.modalData.loadingData = false;
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to load data related to ' + params.id + '".', result);
				}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { data: scope.modalData.detail.common };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_SynchronizationCodes.new', 'POST', params)).then(
					function(result) {
						var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
						proximisMessages.success(title, result.data.status.message, true, 10000);
						refreshParent();
						proximisModalStack.close();
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
						console.error('Unable to add a synchronization code.', result);
					}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - documentId
	 */
	app.controller('ProximisDocumentCodeDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisDocumentCodeDetailController]);

	function ProximisDocumentCodeDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack,
			proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			detail: null
		};

		var params = { metaGroupId: parentData.documentId };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_DocumentCode.detail', 'GET', params)).then(
				function(result) {
					scope.modalData.detail = result.data.item;
					scope.modalData.loadingData = false;
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to load data related to ' + params.id + '".', result);
				}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { metaGroupId: scope.modalData.detail._meta.id, data: scope.modalData.detail.common };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_DocumentCode.detail', 'PUT', params)).then(
					function(result) {
						var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
						proximisMessages.success(title, result.data.status.message, true, 10000);
						refreshParent();
						proximisModalStack.close();
					},
					function(result) {
						scope.modalData.errorMessage = result.data.status.message;
						console.error('Unable to save data related to ' + params.id + '".', result);
					}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

})();