/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	var app = angular.module('proximisDatadashboard');
	app.controller('ProximisDatadashboardReportListController', ['$scope', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisRoute',
		'$location', ProximisDatadashboardReportListController]);

	function ProximisDatadashboardReportListController(scope, proximisRestApi, proximisRestApiDefinition, proximisRoute, $location) {
		scope.createReport = function() {
			var data = proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.new', 'POST', {});
			proximisRestApi.sendData(data).then(function(result) {
				var reportId = result.data.item && result.data.item._meta.id;
				if (!reportId) {
					return;
				}
				var params = { id: reportId };
				var redirectTo = proximisRoute.get('Rbs_Datadashboard_Report', 'edit', params);
				if (!redirectTo) {
					return;
				}
				$location.url(redirectTo);
			});
		};
	}
})();