/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';
	var app = angular.module('proximisDatadashboard');

	app.directive('datadashboardConvertToNumber', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModel) {
				ngModel.$parsers.push(function(val) {
					return parseInt(val, 10);
				});
				ngModel.$formatters.push(function(val) {
					return '' + val;
				});
			}
		};
	});

	app.directive('pxDatadashboardPanelToggle', function() {
		return {
			restrict: 'A',
			link: function(scope, element) {
				var arrow = angular.element('<em class="glyphicon glyphicon-chevron-down pull-right"></em>');
				var paneltitle = angular.element(element[0].querySelector('.panel-heading .panel-title'));
				paneltitle.append(arrow);
				var body = angular.element(element[0].querySelector('.panel-body'));
				body.hide();
				arrow.data('body', body);
				angular.element(element[0].querySelector('.panel-heading')).bind('click', function() {
					angular.element(this).find('.glyphicon').toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-up');
					angular.element(this).find('.glyphicon').data('body').slideToggle();
				});
			}
		};
	});

	app.controller('ProximisDatadashboardReportEditController',
		['$scope', '$routeParams', 'proximisRestApi', 'proximisRestApiDefinition', '$attrs', 'FileUploader', 'proximisCoreI18n', 'proximisMessages',
			ProximisDatadashboardReportEditController]);

	function ProximisDatadashboardReportEditController(scope, $routeParams, proximisRestApi, proximisRestApiDefinition, attrs, FileUploader,
		proximisCoreI18n, proximisMessages) {
		scope.copiedDocument = {};

		scope.import = function(filesList) {
			for (var i = 0; i < filesList.length; i++) {
				(function() {
					var filename = filesList[i].name;
					var params = { reportId: scope.document._meta.id, relativePath: 'in/' + scope.document._meta.id + '/' + filename };
					proximisRestApi.upload(filesList[i], proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.uploadFile', 'POST', params))
						.then(function(result) {
							scope.document.data.files.push(filename);
						});
				})();
			}
			angular.element("input[type='file']").val(null);
		};

		scope.saveImport = function() {
			var params = { reportId: scope.document._meta.id, report: scope.document };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.detail', 'PUT', params))
				.then(function(response) {
					var data = response.data.item;
					var files = scope.document.data.files;
					scope.document = data;
					if (!scope.document.data) {
						scope.document.data = {};
					}
					scope.document.data.files = files;
					angular.copy(scope.document, scope.copiedDocument);
				});
		};

		scope.undo = function() {
			angular.copy(scope.copiedDocument, scope.document);
		};

		scope.removeFile = function(filename) {
			if (confirm(proximisCoreI18n.trans('m.rbs.datadashboard.ua.confirm_delete'))) {
				var params = {
					reportId: scope.document._meta.id,
					relativePath: 'in/' + scope.document._meta.id + '/' + filename
				};
				proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.listFiles', 'DELETE', params))
					.then(function(result) {
						scope.document.data.files.splice(scope.document.data.files.indexOf(filename), 1);
						angular.copy(scope.document, scope.copiedDocument);
						if (scope.document.data.files.length === 0) {
							proximisMessages.success(proximisCoreI18n.trans('m.rbs.datadashboard.ua.action_success_remove | ucf'), null, true, 10000);
						}
					});
			}
		};

		scope.launchImport = function() {

			if (scope.document.status === 'FINISHED') {
				scope.document.import = true;
			}

			var params = {
				reportId: scope.document._meta.id,
				report: scope.document
			};
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.launch', 'PUT', params))
				.then(function(result) {
					var data = result.data.item;
					var files = scope.document.data.files;
					scope.document = data;
					if (!scope.document.data) {
						scope.document.data = {};
					}
					scope.document.data.files = files;
					angular.copy(scope.document, scope.copiedDocument);
				});
		};

		scope.onReady = function(attrs) {
			var uploader = scope.uploader = new FileUploader({
				url: '',
				autoUpload: true,
				removeAfterUpload: true
			});

			uploader.onAfterAddingFile = function(item) {
				if (scope.document.data.files.indexOf(item.file.name) !== -1) {
					if (!confirm(proximisCoreI18n.trans('m.rbs.datadashboard.ua.erase_file_confirm', { filename: item.file.name }))) {
						this.cancelItem(item);
						this.removeFromQueue(item);
					}
				}
			};

			uploader.onCancelItem = function(item, response, status, headers) {
				if (this.queue.length === 1) {
					proximisMessages.success(proximisCoreI18n.trans('m.rbs.datadashboard.ua.action_success_cancel | ucf'), null, true, 10000);
				}
			};

			uploader.uploadItem = function uploadItem(value) {
				var index = this.getIndexOfItem(value);
				var filename = value.file.name;
				var item = this.queue[index];
				var transport = this.isHTML5 ? '_xhrTransport' : '_iframeTransport';

				item._prepareToUploading();
				if (this.isUploading) {
					return;
				}

				this._onBeforeUploadItem(item);
				if (item.isCancel) {
					return;
				}

				item.isUploading = true;
				this.isUploading = true;
				var params = { reportId: scope.document._meta.id, relativePath: 'in/' + scope.document._meta.id + '/' + filename };
				var requestData = proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.uploadFile', 'POST', params);
				proximisRestApi.upload(item._file, requestData)
					.then(function(result) {
						if (scope.document.data.files.indexOf(filename) === -1) {
							scope.document.data.files.push(filename);
							scope.copiedDocument.data.files = scope.document.data.files;
						}
					});
				this[transport](item);
				this._render();
			};

			scope.loading = true;
			scope.documentId = $routeParams.id;

			scope.fileAccept = attrs.fileAccept;
			scope.storageName = attrs.storageName;
			var params = (angular.isObject(scope.detailParams)) ? angular.copy(scope.detailParams) : {};
			params.reportId = $routeParams.id;
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.detail', 'GET', params))
				.then(function(result) {
					scope.document = result.data.item;
					if (!scope.document.data) {
						scope.document.data = {};
					}
					scope.loading = false;
					var params = { reportId: scope.document._meta.id, relativePath: 'in/' + scope.document._meta.id };
					proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Datadashboard_Report.listFiles', 'GET', params))
						.then(function(response) {
							scope.document.data.files = response.data.items;
							angular.copy(scope.document, scope.copiedDocument);
							if (scope.document.data.files.length > 0 && scope.document.origin == 0) {
								scope.document.origin = 1;
							}

							scope.$watch('document.import', function(newValue, oldValue) {
								angular.element(document.getElementById('import')).toggleClass('active', scope.document.import);
							});

							scope.$watch('document.validate', function(newValue, oldValue) {
								angular.element(document.getElementById('validate')).toggleClass('active', scope.document.validate);
							});

							angular.element(document.getElementById('import')).bind('click', function(e) {
								scope.$apply(function(scope) {
									scope.document.import = !scope.document.import;
								});
								e.stopPropagation();
							});
							angular.element(document.getElementById('validate')).bind('click', function(e) {
								scope.$apply(function(scope) {
									scope.document.validate = !scope.document.validate;
								});
								e.stopPropagation();
							});
						});
				});
		};
		scope.onReady(attrs);

		scope.compare = function() {
			return angular.equals(scope.document, scope.copiedDocument)
		}
	}

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsSwitch
	 * @name rbsSwitch
	 * @restrict AE
	 *
	 * @description
	 * Displays a Yes/No switch control.
	 *
	 * @param {boolean} ngModel The bound value.
	 * @param {string=} labelOn Label for the "on" position
	 * @param {string=} labelOff Label for the "off" position
	 * @param {string=} confirmOn Confirmation message when switching from "off" to "on"
	 * @param {string=} confirmOff Confirmation message when switching from "on" to "off"
	 * @param {string=} confirmTitle Title in the confirmation dialog box (requires "confirm-on" and/or "confirm-off")
	 */
	app.directive('rbsSwitch', ['proximisCoreI18n', function(i18n) {
		return {
			template: '<div class="switch-on-off switch">' +
			'	<div class="switch-button"></div>' +
			'	<label class="on" data-ng-bind-html="labelOn"></label>' +
			'	<label class="off" data-ng-bind-html="labelOff"></label>' +
			'</div>',

			restrict: 'AE',
			require: 'ngModel',
			replace: true,
			priority: -1, // Let `required=""` directive execute before this one.
			scope: true,

			link: function(scope, elm, attrs, ngModel) {
				var sw = $(elm), valueOff, valueOn, acceptedValuesOn;

				scope.labelOn = attrs.labelOn || i18n.trans('m.rbs.ua.common.yes');
				scope.labelOff = attrs.labelOff || i18n.trans('m.rbs.ua.common.no');
				valueOff = attrs.valueOff || false;
				valueOn = attrs.valueOn || true;
				acceptedValuesOn = attrs.acceptedValuesOn || [];

				// Remove all parsers that could invalidate this widget (required=true for example).
				ngModel.$parsers.length = 0;

				ngModel.$render = function() {
					if (isON()) {
						sw.addClass('on');
					}
					else {
						sw.removeClass('on');
					}
				};

				function isON() {
					return ngModel.$viewValue === valueOn || acceptedValuesOn.indexOf(ngModel.$viewValue) !== -1;
				}

				function toggleState() {
					ngModel.$setViewValue(isON() ? valueOff : valueOn);
					ngModel.$render();
				}

				sw.click(function() {
					if (attrs.disabled) {
						return;
					}

					scope.$apply(toggleState);
				});
			}
		};
	}]);

})();