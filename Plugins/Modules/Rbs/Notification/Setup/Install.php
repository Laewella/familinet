<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Setup;

/**
 * @name \Rbs\Notification\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		parent::executeApplication($plugin, $application, $configuration);

		$configuration->addPersistentEntry('Rbs/Notification/fromEmail', 'noreply@proximis.com');
		$configuration->addPersistentEntry('Rbs/Notification/adminURL', '');
		$configuration->addPersistentEntry('Rbs/Notification/Sms/ReplaceSpecialChar', false);

		// Add patches.
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Notification', \Rbs\Notification\Setup\Patch\Listeners::class);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$jm = $applicationServices->getJobManager();
		if (count($jm->getJobIdsByName('Rbs_Notification_SendMails')) === 0)
		{
			$jm->createNewJob('Rbs_Notification_SendMails');
		}
	}
}
