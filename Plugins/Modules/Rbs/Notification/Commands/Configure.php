<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Commands;

/**
 * @name \Rbs\Mail\Notification\Configure
 */
class Configure
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$force = $event->getParam('force');
		if ($force)
		{
			$response->addWarningMessage('Reset to default configuration');
		}
		$verbose = $event->getParam('verbose');
		if ($verbose)
		{
			$response->addCommentMessage('Verbose activated');
		}
		$configurations = json_decode(file_get_contents(__DIR__ . '/../Assets/configurations.json'), true);
		if (!$configurations)
		{
			$response->addErrorMessage('Invalid configuration file: ' . __DIR__ . '/../Assets/configurations.json');
			return;
		}

		$customConfigurationsFile = $event->getApplication()->getWorkspace()->appPath('Config', 'notifications.json');
		if (file_exists($customConfigurationsFile))
		{
			$customConfigurations = json_decode(file_get_contents($customConfigurationsFile), true);
			if ($customConfigurations && is_array($customConfigurations))
			{
				if ($invalidEntries = array_diff_key($customConfigurations, $configurations))
				{
					$response->addErrorMessage('Invalid notifications: ' . implode(', ', array_keys($invalidEntries)));
					return;
				}
				$configurations = $this->merge($configurations, $customConfigurations);
			}
			else
			{
				$response->addErrorMessage('Invalid configuration file: ' . $customConfigurationsFile);
				return;
			}
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$q = $documentManager->getNewQuery('Rbs_Mail_Mail');
		$qb = $q->dbQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->innerJoin($fb->table('change_document_code'), $fb->eq($q->getColumn('id'), $fb->column('document_id', 'change_document_code')));
		$qb->innerJoin($fb->table('change_document_code_context'), $fb->column('context_id'));
		$qb->addColumn($fb->alias($q->getColumn('id'), 'id'))->addColumn($fb->column('code', 'change_document_code'));
		$qb->where($fb->eq($fb->column('name', 'change_document_code_context'), $fb->string('Rbs Notifications Install')));
		$select = $qb->query();
		$mailList = $select->getResults($select->getRowsConverter()->addIntCol('id')->addStrCol('code')->indexBy('code')->singleColumn('id'));

		$q = $documentManager->getNewQuery('Rbs_Notification_Sms');
		$qb = $q->dbQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->innerJoin($fb->table('change_document_code'), $fb->eq($q->getColumn('id'), $fb->column('document_id', 'change_document_code')));
		$qb->innerJoin($fb->table('change_document_code_context'), $fb->column('context_id'));
		$qb->addColumn($fb->alias($q->getColumn('id'), 'id'))->addColumn($fb->column('code', 'change_document_code'));
		$qb->where($fb->eq($fb->column('name', 'change_document_code_context'), $fb->string('Rbs Notifications Install')));
		$select = $qb->query();
		$smsList = $select->getResults($select->getRowsConverter()->addIntCol('id')->addStrCol('code')->indexBy('code')->singleColumn('id'));

		$q = $documentManager->getNewQuery('Rbs_Notification_Svi');
		$qb = $q->dbQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->innerJoin($fb->table('change_document_code'), $fb->eq($q->getColumn('id'), $fb->column('document_id', 'change_document_code')));
		$qb->innerJoin($fb->table('change_document_code_context'), $fb->column('context_id'));
		$qb->addColumn($fb->alias($q->getColumn('id'), 'id'))->addColumn($fb->column('code', 'change_document_code'));
		$qb->where($fb->eq($fb->column('name', 'change_document_code_context'), $fb->string('Rbs Notifications Install')));
		$select = $qb->query();
		$sviList = $select->getResults($select->getRowsConverter()->addIntCol('id')->addStrCol('code')->indexBy('code')->singleColumn('id'));

		foreach ($configurations as $code => &$confData)
		{
			if ($err = $this->replaceCode($confData, $response, $mailList, $smsList, $sviList))
			{
				$response->addWarningMessage($err . ' errors on: ' . $code);
			}
		}
		unset($confData);

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$q = $documentManager->getNewQuery('Rbs_Notification_Configuration');
			/** @var \Rbs\Notification\Documents\Configuration[] $docs */
			$docs = [];
			/** @var \Rbs\Notification\Documents\Configuration $doc */
			foreach ($q->getDocuments()->preLoad()->toArray() as $doc)
			{
				$docs[$doc->getCode()] = $doc;
			}

			foreach ($configurations as $code => $confData)
			{
				if (!isset($docs[$code]))
				{
					$doc = $documentManager->getNewDocumentInstanceByModelName('Rbs_Notification_Configuration');
					$doc->setCode($code);
				}
				else
				{
					$doc = $docs[$code];
				}

				$doc->setType($confData['type']);
				if ($force || $doc->isNew())
				{
					$doc->setLabel($confData['label']);
					$currentConfigData = $confData['configData'];
				}
				else
				{
					$currentConfigData = $doc->getConfigData() ?? [];
					$currentConfigData['static'] = $confData['configData']['static'] ?? null;
					if (isset($confData['configData']['recall']))
					{
						foreach ($confData['configData']['recall'] as $i => $recall)
						{
							$currentConfigData['recall'][$i]['static'] = $recall['static'] ?? null;
						}
					}
				}
				if ($verbose)
				{
					$response->addCommentMessage($code . ($doc->isNew() ? ' Added.' : ' Updated.'));
				}
				$doc->setConfigData($currentConfigData);

				$doc->save();
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			throw $transactionManager->rollBack($e);
		}

		$applicationServices->getCacheManager()->removeEntry('prefetch', 'notificationCodes');

		$response->addInfoMessage(count($configurations) . ' notifications updated.');
	}

	/**
	 * @param array $config
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @param array $mailList
	 * @param array $smsList
	 * @param array $sviList
	 * @return int
	 */
	protected function replaceCode(array &$config, $response, $mailList, $smsList, $sviList)
	{
		$err = 0;
		foreach ($config as $name => &$value)
		{
			if (is_array($value))
			{
				$err += $this->replaceCode($value, $response, $mailList, $smsList, $sviList);
			}
			elseif (is_string($value))
			{
				if ($name === 'mail')
				{
					if (isset($mailList[$value]))
					{
						$value = $mailList[$value];
					}
					else
					{
						$err++;
						$response->addWarningMessage('Mail code: ' . $value . ' not found.');
					}
				}
				elseif ($name === 'sms')
				{
					if (isset($smsList[$value]))
					{
						$value = $smsList[$value];
					}
					else
					{
						$err++;
						$response->addWarningMessage('SMS code: ' . $value . ' not found.');
					}
				}
				elseif ($name === 'svi')
				{
					if (isset($sviList[$value]))
					{
						$value = $sviList[$value];
					}
					else
					{
						$err++;
						$response->addWarningMessage('SVI code: ' . $value . ' not found.');
					}
				}
			}
		}
		return $err;
	}

	/**
	 * @param array $base
	 * @param array $custom
	 * @return array
	 */
	protected function merge(array $base, array $custom)
	{
		foreach ($custom as $key => $value)
		{
			if ($key === 'template' || $key === 'type')
			{
				continue;
			}
			if (array_key_exists($key, $base) || isset($base[$key]))
			{
				if (is_array($value) && is_array($base[$key]))
				{
					$base[$key] = $this->merge($base[$key], $value);
				}
				elseif ($value === null)
				{
					unset($base[$key]);
				}
				else
				{
					$base[$key] = $value;
				}
			}
			else
			{
				$base[$key] = $value;
			}
		}
		return $base;
	}
}