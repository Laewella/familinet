<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Rbs\Mail\Notification\InstallNotifications
 */
class InstallNotifications
{
	/**
	 * @param Event $event
	 * @throws \Exception
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();

		$force = $event->getParam('force');

		$filePaths = [];

		$pluginManager = $applicationServices->getPluginManager();
		$plugins = $pluginManager->getInstalledPlugins();
		foreach ($plugins as $plugin)
		{
			if($plugin->getActivated())
			{
				$filePath = $plugin->getAssetsPath() . DIRECTORY_SEPARATOR . 'notifications.json';
				if (file_exists($filePath))
				{
					$filePaths[] = $filePath;
				}
			}
		}

		if ($filePaths)
		{
			$templateCode = $event->getParam('template');
			if ($templateCode)
			{
				$dqb = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Theme_Template');
				$dqb->andPredicates($dqb->eq('code', $templateCode), $dqb->eq('mailSuitable', true));
				$template = $dqb->getFirstDocument();
				if ($template instanceof \Rbs\Theme\Documents\Template)
				{
					$this->moveDocumentCodeContext($applicationServices, $response);

					$jsonArray = ['documents' => []];
					foreach ($filePaths as $filePath)
					{
						$json = json_decode(file_get_contents($filePath), true);
						if ($json)
						{
							$jsonArray['documents'] = array_merge($jsonArray['documents'], $json['documents']);
						}
					}
					$jsonArray['contextId'] = 'Rbs Notifications Install';

					$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
					$import->addOnly(!$force);
					$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());

					$resolveDocument = function ($id, $contextId) use ($template)
					{
						switch ($id)
						{
							case 'mail_template':
								return $template;
								break;
						}
						return null;
					};
					$import->getOptions()->set('resolveDocument', $resolveDocument);

					$transactionManager = $applicationServices->getTransactionManager();
					try
					{
						$transactionManager->begin();
						$import->fromArray($jsonArray);
						$transactionManager->commit();
					}
					catch (\Exception $e)
					{
						throw $transactionManager->rollBack($e);
					}
					$response->addInfoMessage('Notifications template installed');
				}
				else
				{
					$response->addErrorMessage('Template suitable for mail with code: ' . $templateCode . ' not found');
				}
			}
			else
			{
				$response->addErrorMessage('No template found for mail');
			}
		}
		else
		{
			$response->addInfoMessage('No notifications template files found');
		}
	}

	/**
	 * @deprecated since 1.9.0 with no replacement
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @throws \Exception
	 */
	protected function moveDocumentCodeContext(\Change\Services\ApplicationServices $applicationServices,
		\Change\Commands\Events\CommandResponseInterface $response)
	{
		$documentManager = $applicationServices->getDocumentManager();
		$q = $documentManager->getNewQuery('Rbs_Mail_Mail');
		$qb = $q->dbQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->innerJoin($fb->table('change_document_code'), $fb->eq($q->getColumn('id'), $fb->column('document_id', 'change_document_code')));
		$qb->innerJoin($fb->table('change_document_code_context'), $fb->column('context_id'));
		$qb->addColumn($fb->alias($q->getColumn('id'), 'id'))->addColumn($fb->column('code', 'change_document_code'));
		$qb->where($fb->eq($fb->column('name', 'change_document_code_context'), $fb->string('Rbs Mail Install')));
		$select = $qb->query();
		$mailList = $select->getResults($select->getRowsConverter()->addIntCol('id')->addStrCol('code')->indexBy('code')->singleColumn('id'));

		if (!$mailList)
		{
			return;
		}
		$response->addWarningMessage('Move ' . count($mailList) . ' emails code context from "Rbs Mail Install" to "Rbs Notifications Install"');

		$transactionManager = $applicationServices->getTransactionManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();
		try
		{
			$transactionManager->begin();
			foreach ($mailList as $code => $id)
			{
				$doc = $documentManager->getDocumentInstance($id);
				$documentCodeManager->removeDocumentCode($doc, $code, 'Rbs Mail Install');
				$documentCodeManager->addDocumentCode($doc, $code, 'Rbs Notifications Install');
				$response->addWarningMessage('Move context for mail: ' . $code);
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}