<?php
namespace Rbs\Notification\Documents;

/**
 * @name \Rbs\Notification\Documents\Configuration
 */
class Configuration extends \Compilation\Rbs\Notification\Documents\Configuration
{
	/**
	 * @return array
	 */
	public function getAdminConfig()
	{
		return $this->getConfigData();
	}

	/**
	 * @param array $adminConfig
	 * @return $this
	 */
	public function setAdminConfig($adminConfig)
	{
		$configData = $this->getConfigData();
		if ($template = $adminConfig['template'] ?? [])
		{
			$configData['template'] = $template;
		}

		if ($recalls = $adminConfig['recall'] ?? [])
		{
			foreach ($recalls as $i => $data)
			{
				if ($template = $data['template'] ?? [])
				{
					$configData['recall'][$i]['template'] = $template;
				}
			}
		}
		return $this->setConfigData($configData);
	}

	public function test()
	{
		$this->setConfigData(['allow' => ['svi' => false]]);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->removeRelAction('delete');
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->removeRelAction('delete');
		}
	}

	/**
	 * @return int[]
	 */
	public function getTemplateIds()
	{
		$ids = [];
		if ($configData = $this->getConfigData())
		{
			$this->extractIds($configData, $ids);
			$ids = array_keys($ids);
		}
		return $ids;
	}

	protected function extractIds(array $array, &$ids)
	{
		foreach ($array as $n => $v)
		{
			if (is_int($v) && $v >= 100000)
			{
				$ids[$v] = true;
			}
			elseif(is_array($v))
			{
				$this->extractIds($v, $ids);
			}
		}
	}
}
