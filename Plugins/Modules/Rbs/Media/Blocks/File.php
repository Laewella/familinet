<?php
/**
 * Copyright (C) 2014 Gaël PORT
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Blocks;

/**
 * @name \Rbs\Media\Blocks\File
 */
class File extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('toDisplayDocumentIds');
		$parameters->addParameterMeta('blockTitle');
		$parameters->addParameterMeta('imageFormats');
		$parameters->addParameterMeta('dataSetNames');
		$parameters->setLayoutParameters($event->getBlockLayout());

		$page = $event->getParam('page');
		if ($page instanceof \Rbs\Website\Documents\Page)
		{
			$parameters->setParameterValue('pageId', $page->getId());
		}
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();

		$fileIds = $parameters->getParameter('toDisplayDocumentIds');
		if (is_array($fileIds) && count($fileIds))
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$context = $this->populateContext($event->getApplication(), $documentManager, $parameters)->toArray();

			$attributes['files'] = [];
			foreach ($fileIds as $fileId)
			{
				$file = $documentManager->getDocumentInstance($fileId);
				if ($file instanceof \Rbs\Media\Documents\File)
				{
					$ajaxData = $file->getAJAXData($context);
					if ($ajaxData)
					{
						$attributes['files'][] = $ajaxData;
					}
				}
			}
			return count($attributes['files']) ? 'file.twig' : null;
		}

		return null;
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return \Change\Http\Ajax\V1\Context
	 */
	protected function populateContext($application, $documentManager, $parameters)
	{
		$context = new \Change\Http\Ajax\V1\Context($application, $documentManager);
		$context->setDetailed(true);
		$context->setPage($parameters->getParameter('pageId'));
		$context->setVisualFormats($parameters->getParameter('imageFormats'));
		$context->setURLFormats(['canonical', 'contextual']);
		$context->setDataSetNames($parameters->getParameter('dataSetNames'));
		return $context;
	}
}