<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Setup;

/**
 * @name \Rbs\Media\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$webBaseDirectory = $configuration->getEntry('Change/Install/webBaseDirectory', '');
		if ($webBaseDirectory)
		{
			$formattedPath = $application->getWorkspace()->composePath($webBaseDirectory, 'Imagestorage', 'images');
		}
		else
		{
			$formattedPath = $application->getWorkspace()->composePath('Imagestorage', 'images');
		}
		$images = $configuration->getEntry('Change/Storage/images', []);
		$images = array_merge([
			'class' => \Change\Storage\Engines\LocalImageStorage::class,
			'basePath' => 'App/Storage/images',
			'formattedPath' => $formattedPath,
			'baseURL' => '/index.php'
		], $images);
		$configuration->addPersistentEntry('Change/Storage/images', $images);

		$videos = $configuration->getEntry('Change/Storage/videos', []);
		$videos = array_merge([
			'class' => \Change\Storage\Engines\LocalStorage::class,
			'basePath' => 'App/Storage/videos',
			'baseURL' => '/index.php'
		], $videos);
		$configuration->addPersistentEntry('Change/Storage/videos', $videos);

		$files = $configuration->getEntry('Change/Storage/files', []);
		$files = array_merge([
			'class' => \Change\Storage\Engines\LocalStorage::class,
			'basePath' => 'App/Storage/files',
			'baseURL' => '/index.php'
		], $files);
		$configuration->addPersistentEntry('Change/Storage/files', $files);

		$configuration->addPersistentEntry('Rbs/Media/FullPage/height','1600');
		$configuration->addPersistentEntry('Rbs/Media/FullPage/width', '2560');
		// Patch.
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Media', \Rbs\Media\Setup\Patch\Listeners::class);
	}

	public function executeServices($plugin, $applicationServices)
	{
		parent::executeServices($plugin, $applicationServices);

		// Add Extensions collection.
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Rbs_Media_FileExtensions') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('File extensions');
				$collection->setCode('Rbs_Media_FileExtensions');

				// TODO: add some default values.

				$collection->setLocked(true);
				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}

		// Add Mime types collection.
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Rbs_Media_MimeTypes') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Mime types');
				$collection->setCode('Rbs_Media_MimeTypes');

				// TODO: add some default values.

				$collection->setLocked(true);
				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}
}