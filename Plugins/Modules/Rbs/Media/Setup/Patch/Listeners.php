<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Setup\Patch;

/**
 * @name \Rbs\Media\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		//Rbs_Media_0001 v1.1 Fill "fileSize" and "mimeType" columns in image, file and video documents.
	}
}