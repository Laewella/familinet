<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Documents;

/**
 * @name \Rbs\Media\Documents\Video
 */
class Video extends \Compilation\Rbs\Media\Documents\Video
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onDefaultSave($event); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onDefaultSave($event); }, 10);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, function ($event) { $this->onDefaultDeleted($event); }, 10);
		$eventManager->attach('getDownloadUri', function ($event) { $this->onDefaultGetDownloadUri($event); }, 5);
		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); }, 5);
		$eventManager->attach('getPublicURL', function ($event) { $this->onDefaultGetPublicURL($event); }, 5);
	}

	/**
	 * @param int $maxWidth
	 * @param int $maxHeight
	 * @return null|string
	 */
	public function getPublicURL($maxWidth = 0, $maxHeight = 0)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['maxWidth' => $maxWidth, 'maxHeight' => $maxHeight]);
		$em->trigger('getPublicURL', $this, $args);
		if (isset($args['publicUrl']))
		{
			return $args['publicUrl'];
		}
		return null;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetPublicURL(\Change\Documents\Events\Event $event)
	{
		if ($this !== $event->getDocument())
		{
			return;
		}
		$maxWidth = $event->getParam('maxWidth');
		$maxHeight = $event->getParam('maxHeight');
		$sm = $event->getApplicationServices()->getStorageManager();
		$query = [];
		if ($maxWidth !== null)
		{
			$query['max-width'] = (int)$maxWidth;
		}
		if ($maxHeight !== null)
		{
			$query['max-height'] = (int)$maxHeight;
		}
		$changeUri = $this->getPath();
		if (count($query))
		{
			$changeUri .= '?' . http_build_query($query);
		}
		$event->setParam('publicUrl', $sm->getPublicURL($changeUri));
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultDeleted(\Change\Documents\Events\Event $event)
	{
		if ($this !== $event->getDocument())
		{
			return;
		}
		if ($this->getPath())
		{
			$arguments = ['storageURI' => $this->getPath()];
			$event->getApplicationServices()->getJobManager()->createNewJob('Change_Storage_URICleanUp', $arguments);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultSave(\Change\Documents\Events\Event $event)
	{
		if ($this !== $event->getDocument())
		{
			return;
		}
		if ($this->isPropertyModified('path'))
		{
			if ($this->getPathOldValue() && $this->getPath() !== $this->getPathOldValue())
			{
				$arguments = ['storageURI' => $this->getPathOldValue()];
				$event->getApplicationServices()->getJobManager()->createNewJob('Change_Storage_URICleanUp', $arguments);
			}

			if ($this->getPath())
			{
				$itemInfo = $event->getApplicationServices()->getStorageManager()->getItemInfo($this->getPath());
				if (!($itemInfo instanceof \Change\Storage\ItemInfo) || !$itemInfo->isFile())
				{
					$errors = $event->getParam('propertiesErrors', []);
					$errors['path'][] = new \Change\I18n\PreparedKey(
						'm.rbs.media.admin.invalid_path', ['ucf'], ['path' => $this->getPath()]
					);
					$event->setParam('propertiesErrors', $errors);
					return;
				}

				$this->setMimeType($itemInfo->getMimeType());
				$this->setContentLength($itemInfo->getSize());
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetDownloadUri(\Change\Documents\Events\Event $event)
	{
		if ($this->activated())
		{
			$event->setParam('downloadUri', $this->getPath());
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Media\Http\Ajax\V1\VideoDataComposer($event))->toArray());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$result = $event->getParam('restResult');
		if ($this !== $event->getDocument())
		{
			return;
		}
		if ($result instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$result->setProperty('publicurl', $this->getPublicURL());
		}
	}

	/**
	 * @api
	 * @return string
	 */
	public function getFileName()
	{
		if ($this->getPath())
		{
			return basename($this->getPath());
		}
		return null;
	}

	/**
	 * @api
	 * @return string
	 */
	public function getExtension()
	{
		if ($this->getPath())
		{
			$arrayPath = explode('.', $this->getPath());
			return (count($arrayPath) > 1) ? end($arrayPath) : null;
		}
		return null;
	}
}
