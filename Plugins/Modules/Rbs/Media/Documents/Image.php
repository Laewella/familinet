<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Documents;

/**
 * @name \Rbs\Media\Documents\Image
 */
class Image extends \Compilation\Rbs\Media\Documents\Image
{
	/**
	 * @return array
	 */
	public function getImageSize()
	{
		return ['width' => $this->getWidth(), 'height' => $this->getHeight()];
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$onSave = function ($event) { $this->onDefaultSave($event); };
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, $onSave, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, $onSave, 10);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, function ($event) { $this->onDefaultDeleted($event); }, 10);
		$eventManager->attach('getDownloadUri', function ($event) { $this->onDefaultGetDownloadUri($event); }, 5);
		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); }, 5);
		$eventManager->attach('getPublicURL', function ($event) { $this->onDefaultGetPublicURL($event); }, 5);
	}

	/**
	 * @param int $maxWidth
	 * @param int $maxHeight
	 * @return null|string
	 */
	public function getPublicURL($maxWidth = 0, $maxHeight = 0)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['maxWidth' => $maxWidth, 'maxHeight' => $maxHeight]);
		$em->trigger('getPublicURL', $this, $args);
		if (isset($args['publicUrl']))
		{
			return $args['publicUrl'];
		}
		return null;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetPublicURL(\Change\Documents\Events\Event $event)
	{
		$maxWidth = $event->getParam('maxWidth');
		$maxHeight = $event->getParam('maxHeight');
		$sm = $event->getApplicationServices()->getStorageManager();
		$query = [];
		if ($maxWidth !== null)
		{
			$query['max-width'] = (int)$maxWidth;
		}
		if ($maxHeight !== null)
		{
			$query['max-height'] = (int)$maxHeight;
		}
		$changeUri = $this->getPath();
		if (count($query))
		{
			$changeUri .= '?' . http_build_query($query);
		}
		$event->setParam('publicUrl', $sm->getPublicURL($changeUri));
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultSave(\Change\Documents\Events\Event $event)
	{
		if ($this->isPropertyModified('path'))
		{
			$path = $this->getPath();
			$applicationServices = $event->getApplicationServices();
			if (($oldPath = $this->getPathOldValue()) && $path !== $oldPath)
			{
				$arguments = ['storageURI' => $oldPath];
				$applicationServices->getJobManager()->createNewJob('Change_Storage_URICleanUp', $arguments);
			}

			if ($path && !$event->getParam('ignoreImageInfo'))
			{
				if ($itemInfo = $applicationServices->getStorageManager()->getItemInfo($this->getPath()))
				{
					if ($itemInfo->isReadable())
					{
						$this->setMimeType($itemInfo->getMimeType());
						$this->setContentLength($itemInfo->getSize());

						$size = (new \Change\Presentation\Images\Resizer())->getImageSize($this->getPath());
						$this->setHeight($size['height']);
						$this->setWidth($size['width']);
					}
				}
				else
				{
					$errors = $event->getParam('propertiesErrors', []);
					$errors['path'][] = new \Change\I18n\PreparedKey(
						'm.rbs.media.admin.invalid_path', ['ucf'], ['path' => $this->getPath()]
					);
					$event->setParam('propertiesErrors', $errors);
					return;
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultDeleted(\Change\Documents\Events\Event $event)
	{
		if ($this->getPath())
		{
			$arguments = ['storageURI' => $this->getPath()];
			$event->getApplicationServices()->getJobManager()->createNewJob('Change_Storage_URICleanUp', $arguments);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetDownloadUri(\Change\Documents\Events\Event $event)
	{
		if ($this->activated())
		{
			$event->setParam('downloadUri', $this->getPath());
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}
		$event->setParam('data', (new \Rbs\Media\Http\Ajax\V1\ImageDataComposer($event))->toArray());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$result = $event->getParam('restResult');
		if ($this !== $event->getDocument())
		{
			return;
		}
		$urlManager = $result->getUrlManager();
		if ($result instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$link = ['rel' => 'publicurl', 'href' => $this->getPublicURL()];
			$result->addLink($link);
			$selfLinks = $result->getRelLink('self');
			$selfLink = array_shift($selfLinks);
			if ($selfLink instanceof \Change\Http\Rest\V1\Link)
			{
				$pathParts = explode('/', $selfLink->getPathInfo());
				array_pop($pathParts);
				$result->addAction(new \Change\Http\Rest\V1\Link($urlManager, implode('/', $pathParts) . '/resize', 'resizeurl'));
			}
		}
		elseif ($result instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$pathParts = explode('/', $result->getPathInfo());
			array_pop($pathParts);
			$result->addAction(new \Change\Http\Rest\V1\Link($urlManager, implode('/', $pathParts) . '/resize', 'resizeurl'));
			$result->setProperty('width', $this->getWidth());
			$result->setProperty('height', $this->getHeight());
		}
	}

	/**
	 * @api
	 * @return string
	 */
	public function getFileName()
	{
		if ($this->getPath())
		{
			return basename($this->getPath());
		}
		return null;
	}

	/**
	 * @api
	 * @return string
	 */
	public function getExtension()
	{
		if ($this->getPath())
		{
			$arrayPath = explode('.', $this->getPath());
			return (count($arrayPath) > 1) ? end($arrayPath) : null;
		}
		return null;
	}
}