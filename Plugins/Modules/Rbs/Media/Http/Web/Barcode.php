<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Http\Web;

/**
 * @name \Rbs\Media\Http\Web\Barcode
 */
class Barcode
{
	public function __invoke()
	{
		if (func_num_args() === 1)
		{
			$event = func_get_arg(0);
			if ($event instanceof \Change\Http\Web\Event)
			{
				$this->execute($event);
			}
		}
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Web\Event $event)
	{
		if ($event->getRequest()->getMethod() === 'GET')
		{
			$text = $event->getRequest()->getQuery('text');
			if (!$text)
			{
				throw new \RuntimeException('No text to convert to barcode!');
			}

			$result = new \Change\Http\Result();
			$callback = function ($event)
			{
				$this->onResultContent($event);
			};
			$event->getController()->getEventManager()->attach(\Change\Http\Event::EVENT_RESPONSE, $callback, 10);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	protected function onResultContent($event)
	{
		$format = $event->getRequest()->getQuery('format', 'code128');

		$options = [
			'text' => $event->getRequest()->getQuery('text')
		];

		$factor = $event->getRequest()->getQuery('factor');
		if ($factor > 1)
		{
			$options['factor'] = $factor;
		}

		$barHeight = (int)$event->getRequest()->getQuery('barHeight');
		if ($barHeight >= 1)
		{
			$options['barHeight'] = $barHeight;
		}

		$drawText = (bool)$event->getRequest()->getQuery('drawText', 1);
		if (!$drawText)
		{
			$options['drawText'] = $drawText;
		}
		else
		{
			$stretchText = (bool)$event->getRequest()->getQuery('stretchText');
			if ($stretchText)
			{
				$options['stretchText'] = $stretchText;
			}

			$font = $event->getRequest()->getQuery('font');
			if (is_readable($font))
			{
				$options['font'] = $font;
			}

			$fontSize = (int)$event->getRequest()->getQuery('fontSize');
			if ($fontSize)
			{
				$options['fontSize'] = $fontSize;
			}
		}

		\Zend\Barcode\Barcode::factory($format, 'image', $options, [])->render();
	}
}