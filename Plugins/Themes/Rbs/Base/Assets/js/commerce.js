(function(__change) {
	'use strict';

	var app = angular.module('RbsChangeApp');

	/**
	 * @ngdoc filter
	 * @id RbsChangeApp.filter:rbsFormatRate
	 * @name rbsFormatRate
	 * @function
	 *
	 * @description
	 * Format a rate.
	 *
	 * @param {number} rate A rate to format.
	 * @param {number=} fractionSize The fraction size (2 if omitted).
	 *
	 * @example
	 * ```html
	 *   <span>(= 10.55 | rbsFormatRate:'1' =)</span>
	 * ```
	 */
	app.filter('rbsFormatRate', ['proximisIntlFormatter', function(proximisIntlFormatter) {
		var formatters = {};
		return function(input, fractionSize) {
			if (!angular.isNumber(input)) {
				return input;
			}
			if (!angular.isNumber(fractionSize)) {
				fractionSize = 2;
			}

			var key = fractionSize;
			if (!formatters.hasOwnProperty(key)) {
				var options = { style: 'percent', minimumFractionDigits: fractionSize, maximumFractionDigits: fractionSize };
				formatters[key] = proximisIntlFormatter.getNumberFormatter(options);
			}
			return formatters[key].format(input);
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChangeApp.filter:rbsFormatPrice
	 * @name rbsFormatPrice
	 * @function
	 *
	 * @description
	 * Format price by currency.
	 *
	 * @param {number} input A value to format.
	 * @param {string} currencyCode The string for the code (ex: `'EUR'`).
	 * @param {number=} fractionSize The fraction size (deducted from the locale if omitted).
	 *
	 * @example
	 * ```html
	 *   <span>(= 10.55 | rbsFormatPrice:'EUR' =)</span>
	 * ```
	 */
	app.filter('rbsFormatPrice', ['proximisIntlFormatter', function(proximisIntlFormatter) {
		var formatters = {};
		return function(input, currencyCode, fractionSize) {
			if (!angular.isNumber(input)) {
				return input;
			}
			fractionSize = angular.isNumber(fractionSize) ? fractionSize : undefined;

			var key = currencyCode + '-' + fractionSize;
			if (!formatters.hasOwnProperty(key)) {
				var params = { style: 'currency', currency: currencyCode };
				if (fractionSize !== undefined) {
					params.minimumFractionDigits = fractionSize;
					params.maximumFractionDigits = fractionSize;
				}
				formatters[key] = proximisIntlFormatter.getNumberFormatter(params);
			}
			return formatters[key].format(input);
		};
	}]);

	/**
	 * @ngdoc provider
	 * @id RbsChangeApp.provider:ProductServiceProvider
	 * @name ProductServiceProvider
	 *
	 * @description
	 * The provider for the {@link RbsChange.service:ProductService commerce service}, allowing to configure it.
	 */
	app.provider('RbsChange.ProductService', function ProductServiceProvider() {
		var config = {};

		/**
		 * @ngdoc method
		 * @methodOf ProductServiceProvider
		 * @name setConfig
		 *
		 * @description
		 * Set configuration info.
		 *
		 * @param {Object} newConfig The new configuration.
		 */
		this.setConfig = function(newConfig) {
			angular.extend(config, newConfig);
		};

		/**
		 * @ngdoc service
		 * @id RbsChange.service:ProductService
		 * @name ProductService
		 *
		 * @description
		 * This service provides methods to manipulate products.
		 */
		this.$get = ['$rootScope', '$q', 'RbsChange.ModalStack', 'RbsChange.AjaxAPI',
			function($rootScope, $q, ModalStack, AjaxAPI) {

				/**
				 * @ngdoc method
				 * @methodOf ProductService
				 * @name extractPictogram
				 *
				 * @description
				 * Extract the pictograms.
				 *
				 * @param {Object} productData The product data.
				 * @param {Object=} rootProductData The root product data.
				 * @returns {Array|null}
				 */
				function extractPictogram(productData, rootProductData) {
					if (productData && productData['typology'] && productData['typology'].attributes
						&& productData['typology'].attributes.pictograms) {
						var attr = productData['typology'].attributes.pictograms;
						if (attr && attr.value && attr.value.length) {
							return attr.value;
						}
					}
					if (rootProductData && rootProductData['typology'] && rootProductData['typology'].attributes
						&& rootProductData['typology'].attributes.pictograms) {
						attr = rootProductData['typology'].attributes.pictograms;
						if (attr && attr.value && attr.value.length) {
							return attr.value;
						}
					}
					return null;
				}

				/**
				 * @ngdoc method
				 * @methodOf ProductService
				 * @name extractVisuals
				 *
				 * @description
				 * Extract visual.
				 *
				 * @param {Object} productData The product data.
				 * @param {Object=} rootProductData The root product data.
				 * @returns {Array|null}
				 */
				function extractVisuals(productData, rootProductData) {
					if (productData && productData.common && productData.common.visuals) {
						var visuals = productData.common.visuals;
						if (visuals && visuals.length) {
							return visuals;
						}
					}
					if (rootProductData && rootProductData.common && rootProductData.common.visuals) {
						visuals = rootProductData.common.visuals;
						if (visuals && visuals.length) {
							return visuals;
						}
					}
					return null;
				}

				/**
				 * @ngdoc method
				 * @methodOf ProductService
				 * @name extractBrand
				 *
				 * @description
				 * Extract brand data.
				 *
				 * @param {Object} productData The product data.
				 * @param {Object=} rootProductData The root product data.
				 * @returns {Object|null}
				 */
				function extractBrand(productData, rootProductData) {
					if (productData && productData.common.brand && productData.common.brand.common) {
						return productData.common.brand;
					}
					if (rootProductData && rootProductData.common.brand && rootProductData.common.brand.common) {
						return rootProductData.common.brand;
					}
					return null;
				}

				/**
				 * @ngdoc method
				 * @methodOf ProductService
				 * @name extractURL
				 *
				 * @description
				 * Extract the URL.
				 *
				 * @param {Object} productData The product data.
				 * @param {Object=} rootProductData The root product data.
				 * @returns {string|null}
				 */
				function extractURL(productData, rootProductData) {
					if (productData && angular.isObject(productData.common.URL)) {
						return productData.common.URL['contextual'] || productData.common.URL['canonical'];
					}
					if (rootProductData && angular.isObject(rootProductData.common.URL)) {
						return rootProductData.common.URL['contextual'] || rootProductData.common.URL['canonical'];
					}
					return null;
				}

				/**
				 * @ngdoc method
				 * @methodOf ProductService
				 * @name extractSpecificationsTitle
				 *
				 * @description
				 * Extract the specifications title.
				 *
				 * @param {Object} productData The product data.
				 * @param {Object=} rootProductData The root product data.
				 * @returns {string|null}
				 */
				function extractSpecificationsTitle(productData, rootProductData) {
					if (productData && angular.isObject(productData['typology'])
						&& angular.isObject(productData['typology'].contexts.specifications)) {
						return productData['typology'].contexts.specifications.title;
					}
					if (rootProductData && angular.isObject(rootProductData['typology'])
						&& angular.isObject(rootProductData['typology'].contexts.specifications)) {
						return rootProductData['typology'].contexts.specifications.title;
					}
					return null;
				}

				/**
				 * @ngdoc method
				 * @methodOf ProductService
				 * @name addToCart
				 *
				 * @description
				 * Add a product to cart.
				 *
				 * @param {Object} data `data` The the content of the `data` entry.
				 * @param {Object=} params Additional values for the standard params. Main params like the website have default
				 *    value and don't need to be specified here.
				 * @returns {HttpPromise}
				 */
				function addToCart(data, params) {
					if (!params) {
						params = {
							detailed: false,
							URLFormats: 'canonical',
							visualFormats: 'shortCartItem'
						}
					}
					if (data) {
						data.clientDate = (new Date()).toString();
					}
					return AjaxAPI.postData('Rbs/Commerce/Cart/Lines', data, params).then(function(result) {
						var cart = result.data.dataSets;
						var refreshCart = !!cart;
						if (refreshCart) {
							$rootScope.$broadcast('rbsRefreshCart', { cart: cart, linesAdded: cart.linesAdded });
						}
						return result;
					});
				}

				// Public API
				return {
					extractPictogram: extractPictogram,
					extractVisuals: extractVisuals,
					extractBrand: extractBrand,
					extractURL: extractURL,
					extractSpecificationsTitle: extractSpecificationsTitle,
					addToCart: addToCart
				};
			}];
	});

	app.run(['$rootScope', 'RbsChange.AjaxAPI', function($rootScope, AjaxAPI) {
		$rootScope.$on('rbsStorelocatorChooseStore', function(event, storeId) {
			if (angular.isNumber(storeId)) {
				var data = { storeId: storeId };
				var storeData = null;
				AjaxAPI.putData('Rbs/Storeshipping/Store/Default', data, { URLFormats: 'canonical' })
					.then(function(result) {
							if (!angular.isArray(result.data.dataSets) && angular.isObject(result.data.dataSets)) {
								storeData = result.data.dataSets;
							}
							$rootScope.$emit('rbsStorelocatorDefaultStore', storeData);
						},
						function(result) {
							console.error('on rbsStorelocatorChooseStore', result, storeId);
						}
					);
			}
			else {
				AjaxAPI.getData('Rbs/Storeshipping/Store/Default', {}, { URLFormats: 'canonical' })
					.then(function(result) {
							if (!angular.isArray(result.data.dataSets) && angular.isObject(result.data.dataSets)) {
								storeData = result.data.dataSets;
							}
							$rootScope.$emit('rbsStorelocatorDefaultStore', storeData);
						},
						function(result) {
							console.error('on rbsStorelocatorChooseStore', result, storeId);
						}
					);
			}
		});

		$rootScope.$on('rbsUserConnected', function(event, params) {
			if (params && params.accessorId) {
				$rootScope.$emit('rbsStorelocatorChooseStore', null);
			}
		});
	}])
})(window.__change);