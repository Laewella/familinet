(function(jQuery, __change) {
	'use strict';

	// Initialize Intl.
	__change.RBS_Ua_Intl.initialize(__change.navigationContext.LCID);

	/**
	 * @ngdoc module
	 * @name RbsChangeApp
	 *
	 * @description
	 * The RbsChange module contains all components for the front office.
	 */
	var app = angular.module('RbsChangeApp', ['ngCookies', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'infinite-scroll', 'proximisIntl']);

	app.config(['$compileProvider', '$interpolateProvider', '$locationProvider', '$anchorScrollProvider',
		function($compileProvider, $interpolateProvider, $locationProvider, $anchorScrollProvider) {
			$interpolateProvider.startSymbol('(=').endSymbol('=)');
			$locationProvider.html5Mode(true);
			$anchorScrollProvider.disableAutoScrolling();

			if (!__change.debug || !__change.debug.angularDevMode) {
				$compileProvider.debugInfoEnabled(false);
			}
		}
	]);

	app.directive('a', function() {
		return {
			restrict: 'E',
			scope: false,
			compile: function(element, attributes) {
				if (!attributes['target']) {
					element.attr('target', '_self');
				}
			}
		}
	});

	app.run(['$anchorScroll', function($anchorScroll) {
		$anchorScroll.yOffset = 20;
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsAnchor
	 * @name rbsAnchor
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * A directive to handle anchors that deals with `<base href="..." />`.
	 *
	 * @param {string} rbsAnchor The anchor name (without the #).
	 */
	app.directive('rbsAnchor', ['$location', '$anchorScroll', function($location, $anchorScroll) {

		function animateScroll(id) {
			var offset = jQuery('#' + id).offset();
			if (offset) {
				jQuery('html, body').animate({ scrollTop: offset.top - 20 }, 1000);
			}
			else {
				$anchorScroll(id);
			}
		}

		return {
			restrict: 'A',
			compile: function(element, attributes) {
				var anchor = attributes['rbsAnchor'];
				if (anchor) {
					element.attr('href', window.location.pathname + window.location.search + '#' + anchor);
				}
				return function(scope, elem) {
					if (anchor) {
						elem.click(function(event) {
							event.preventDefault();
							event.stopPropagation();
							animateScroll(anchor);
						});
					}
				}
			}
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsMaxHeight
	 * @name rbsMaxHeight
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * If the content is higher than the limit, it is reduced to it and a button allows to show the full content.
	 *
	 * @param {number} rbsMaxHeight The max height.
	 */
	app.directive('rbsMaxHeight', ['$timeout', function($timeout) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-max-height.twig',
			transclude: true,
			scope: {},
			link: function(scope, elm, attrs) {
				scope.containerNode = elm.find('.max-height-container');
				scope.contentNode = elm.find('.max-height-content').get(0);
				scope.deployed = false;
				scope.showButtons = false;
				scope.maxHeight = parseInt(attrs['rbsMaxHeight'], 10);
				if (isNaN(scope.maxHeight) || scope.maxHeight < 0) {
					scope.maxHeight = 0;
				}

				scope.toggle = function() {
					scope.deployed = !scope.deployed;
					refreshStyles();
				};

				var height;

				$timeout(checkHeight, 100);
				function checkHeight() {
					var newHeight = scope.contentNode.offsetHeight;
					if (height !== newHeight) {
						height = newHeight;

						var newShowButtons;
						if (!scope.maxHeight) {
							newShowButtons = false;
						}
						else {
							newShowButtons = (height > scope.maxHeight + 20);
						}

						if (scope.showButtons !== newShowButtons) {
							scope.showButtons = newShowButtons;
							refreshStyles();
						}
					}
					$timeout(checkHeight, 100);
				}

				function refreshStyles() {
					if (!scope.showButtons || scope.deployed) {
						scope.containerNode.css({ overflow: 'visible', 'max-height': '' });
					}
					else {
						scope.containerNode.css({ overflow: 'hidden', 'max-height': scope.maxHeight + 'px' });
					}
				}
			}
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsDatePicker
	 * @name rbsDatePicker
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * A date picker.
	 *
	 * @param {string} ngModel Assignable angular expression to data-bind to.
	 * @param {string} fieldId The `id` attribute of the input field.
	 * @param {string} fieldName The `name` attribute of the input field.
	 * @param {string} placeholder The `placeholder` attribute of the input field.
	 * @param {string=} [mode=day] Current mode of the picker (`day`|`month`|`year`). Can be used to initialize the picker to specific mode.
	 */
	app.directive('rbsDatePicker', [function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-date-picker.twig',
			require: 'ngModel',
			scope: {},
			link: function(scope, elm, attrs, ngModel) {
				scope.fieldId = attrs['fieldId'];
				scope.fieldName = attrs['fieldName'];
				scope.placeholder = attrs['placeholder'];
				scope.options = {
					'min-mode': attrs['mode'] || 'day',
					'max-mode': attrs['mode'] || 'day'
				};
				scope.picker = {
					opened: false,
					value: null
				};

				scope.open = function(event) {
					event.preventDefault();
					event.stopPropagation();
					scope.picker.opened = true;
				};

				// viewValue => modelValue
				ngModel.$parsers.unshift(function(viewValue) {
					function addZero(i) {
						return ((i < 10) ? '0' : '') + i;
					}

					if (viewValue) {
						return viewValue.getFullYear()
							+ '-' + addZero(viewValue.getMonth() + 1)
							+ '-' + addZero(viewValue.getDate())
							+ 'T00:00:00+00:00';
					}
					return null;
				});

				// modelValue => viewValue
				ngModel.$formatters.unshift(function(modelValue) {
					if (modelValue) {
						return new Date(modelValue);
					}
					return null;
				});

				ngModel.$render = function() {
					if (ngModel.$viewValue) {
						scope.picker.value = ngModel.$viewValue;
					}
				};

				scope.$watch('picker.value', function(fieldValue) {
					if (angular.isObject(fieldValue)) {
						if (fieldValue.getTimezoneOffset()) {
							fieldValue.setHours(0 - (fieldValue.getTimezoneOffset() / 60));
						}
						ngModel.$setViewValue(fieldValue);
					}
				});
			}
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsPagination
	 * @name rbsPagination
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * A pagination control.
	 *
	 * @param {Object} rbsPagination An object describing the pagination:
	 *
	 *    - `offset`: the offset value, updated when the user chooses a page
	 *    - `limit`: the number of items per page
	 *    - `count`: the total items count
	 *
	 * @param {Function} updateOffset A function called when the offset is updated.
	 *    The function should have one integer parameter `offset`.
	 */
	app.directive('rbsPagination', [function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-pagination.twig',
			scope: {
				pagination: '=rbsPagination',
				updateOffset: '='
			},
			link: function(scope) {
				function refreshData() {
					if (angular.isObject(scope.pagination)) {
						scope.pageNumber = Math.floor(scope.pagination.offset / scope.pagination.limit) + 1;
						scope.pageCount = Math.ceil((scope.pagination.count) / scope.pagination.limit);
					}
					else {
						scope.pageNumber = 0;
						scope.pageCount = 0;
					}

					scope.pagesToShow = [];
					var start = scope.pageNumber > 3 ? scope.pageNumber - 3 : 1;
					var end = ((scope.pageCount - scope.pageNumber) > 3) ? scope.pageNumber + 3 : scope.pageCount;
					for (var i = start; i <= end; i++) {
						scope.pagesToShow.push(i);
					}
				}

				scope.$watch('pagination', function() {
					refreshData();
				}, true);

				scope.setPageNumber = function(pageNumber) {
					scope.updateOffset((pageNumber - 1) * scope.pagination.limit);
				}
			}
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsLoading
	 * @name rbsLoading
	 *
	 * @description
	 * Show an animated loading icon.
	 * Prefer to use the Twig block rbsGenericIconLoading defined in Rbs/Generic/Assets/Twig/icon-templates.twig than directly
	 * this directive, to allow overriding.
	 */
	app.directive('rbsLoading', function() {
		return {
			restrict: 'A',
			template: '<img alt="" src="data:image/gif;base64,R0lGODlhGAAYAIQAACQmJJyenNTS1Ozq7GRiZLy+vNze3PT29MzKzDw+PIyKjNza3PTy9GxubMTGxOTm5Pz+/CwqLNTW1Ozu7GRmZMTCxOTi5Pz6/MzOzExOTP///wAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQJCQAaACwAAAAAGAAYAAAF6qAmjho0GcKBUIpzkfAIWU5VFUwB7EnwxiLVbZjbRQCRzAKoYQwLt+Ju2ogdJBeGA1pAHASZ446QZcgQFQxEuziQBooIgeFEQEQWrgDyiy3oNwUWJVtETCIQNVAOJjZQS4ciC1wVE5NcbpEaFwVcCwJDCJojGEMYDBOpZqNNE6h0rhOZo6iuDAJcoqylnQIGlLOHnEMLE08GowtPExeKUZEQT4waeTcCF3dADGtDgyUIBddaBsEXyntadiO3WU8YBwzgneFlMVqUFQwDUE8STCqUcOxztwrIDEUFDuxbZCEbtBMpbhmY4JBECAAh+QQJCQAaACwAAAAAGAAYAIQkJiScnpzU0tTs6uxkZmQ8Pjy8vrzc3tz09vTMysw0NjTc2tz08vRMTkzExsTk5uT8/vwsKizU1tTs7uyMiozEwsTk4uT8+vzMzsxUUlT///8AAAAAAAAAAAAAAAAAAAAF76Amjho0HQLCCMcEkfAIWU5VGcxg3In1xiJE4kacTHaGXQIB1DCIyBzyZpDEEJILw4FcMhJTAUSwkA0xkO3iQkIcKmiBosHWWJDieowxVkQAASVcRAxNQQUAiQUXEzY7ZYYiFImJFQtJN0yRGg9/iRQCRAmbIxmUBAxGE4WkGgsOCQkCqamapAw5qwJdrRpgNyxTtoYXSAYLjUgHpAtEFRMXNVGREFxJDi93wBc/e2k2FRYiEGACWg4HwxfN5k8J3StaUBgqYEkGYhPDIltTFVKOblgBImQKDh3zWAGZIc0AAh07HPggZQKFChYugIQAACH5BAkJABoALAAAAAAYABgAhCQmJJyenNTS1Ozq7GRmZDw+PLy+vNze3PT29MzKzDQ2NNza3PTy9MTGxOTm5Pz+/CwqLNTW1Ozu7IyKjExOTMTCxOTi5Pz6/MzOzDw6PP///wAAAAAAAAAAAAAAAAAAAAXroCaO2iMdAsIIh/SQ8PhYTVUZzGDcifXGIkTiRpRIdoZdAgHUMIjIHPJmiMQQkQujgVwyElPBg8EUPYaYcWNxISEOlfQz8bMgxW0gY0y0lLhEDE1mNUkNJjY7C4MjCzs3Eo5IZYwXSTcLAkQJjCRDOwIMRhKCnSKiRgyiopSdCw0JCQICXaYiFAC5BAdTrU0DELkAExJQB6YTucEVF4U3pU0XGcIZbXY3Ahc/MXsCCrkBZmDZWwetFwtxD94UeU7kUBgqYJdpAoswW1MVUok2Ak2ETMGhA8qSQTMKGUCgY0cDH6ZMoFDBwgWQEAAh+QQJCQAcACwAAAAAGAAYAIQkJiScnpzU0tTs6uxkYmS8urzc3tz09vTExsQ8PjyMiozc2tz08vR0cnTEwsTk5uT8/vzMzsxMTkwsKizU1tTs7uxkZmS8vrzk4uT8+vzMysxUUlT///8AAAAAAAAAAAAF6iAnjhxUGcLBCEYFkfAIYYjjXMxw3Rr2xqKD5kasVHaXneYA5DCIyBzydqHEDpQMA4FcMjRTAYTBFEGGkTFikSEdDI70U/PDIMVtIGNMxJS4RAxNZjVJCCY2OwuDIws7NxWOSGWMGUk3CwJEGowkQzsCDEYVgp0iokYMoqKUnSqkK12mImA3LFOtTZZUCxVQBqYLUBUZhTelTRBcO4ccdrYZPzELKol+JWACWggGrQMKEwTVdCMrWlARBwISEwDu4mQxW1MODAXu+BMNTUJTOPf4AEhYlIwGFXv4EgTIw8gEigMILChwwJBECAAh+QQJCQAZACwAAAAAGAAYAIQkJiScnpzU0tTs6uxkZmS8vrzc3tz09vQ8PjzMysw0NjTc2tz08vTExsTk5uT8/vwsKizU1tTs7uyMiozEwsTk4uT8+vxMTkzMzsz///8AAAAAAAAAAAAAAAAAAAAAAAAF7mAmjtkjGcLBCIb0kPD4VA1FFcxQ3En1xqJD4kaUSHaFXeIAzDCIyBzyVojEDhELo4FcMhJTwYPBFD2GmHFjYSEdDJT0M/GrIMVtIGNMrJS4RAxNZjVJDSY2OwuDIws7NxKOSGWMFkk3CwJECYwkQzsCDEYSgp0iokYMoqKUnSqkK12mImA3LFOtTZZUCxJQBqYLUBIWhTelTQ9cO4cZdrYWeTF7Tzd+JWACFgIIEw4kFo5icz9O2hEKAAAQFxVflwXaErkZ6OrqEBE6UFVNCxf31C3Y92jJIAsBENwTQLCBD1MWKEwgUEECCxdAQgAAIfkECQkAGgAsAAAAABgAGAAABeqgJo4aNBnCwQjGBJHwCFlOVRXMUNyI9caiA+JGnEx2hR3iANQwiMgc8laQxA6SC8OBXDIQUwGEwRRBhpixY3EhHQyV9BPxsyDFbSBjTLSUuEQMTWY1SQ4mNjsLgyMLOzcTjkhljBdJNwsCRAiMJEM7AgxGE4KdIqJGDBIICGumQaSkFAC0Ga8an3EKtBERD6aWVHC0tAqmjjYVAxcJxBGLgxdchi8BvAQHPzF7TzZ+GhcZAAQMWwaU4AtxfHSNDVpEFV5glwIXE+inUDtSiUlWesBA6fdoyaAZhQoc0LHDgQ9TJlCoYOECSAgAIfkECQkAGgAsAAAAABgAGACEJCYknJ6c1NLU7OrsZGJk3N7c9Pb0PD48vL68jIqMxMbE3Nrc9PL0dHJ05Obk/P78TE5MLCos1NbU7O7sZGZk5OLk/Pr8xMLEzMrMVFJU////AAAAAAAAAAAAAAAAAAAABemgJo7aMxWCwQjF9JDw+FTKdSHMgNxY9cYiA+ZGnEx2iB3GANQwiMgc8oaQxBYNlQK5ZGCmggeDKbJAABTtwkIyFC4YMfwXANgJll+MId9VNBYHABGDVk0lNUkKDxd2dgmHIws7NxMJjhEDkUFQCwSOGZsjXzYCEhioC6IiDEYTDK0DE2SisK8TAlyrGl87LFO0hxZICAsTUAWiC0QXExaJNwyRD1s3ixoVSAJ5TXxPfiIPX9sMCgXBFsvkcyMrFt88Kr1JYbB71ZRSNkiGMUJTCAzogLLk0IxEOI7sUOBDlAkUKgQY00MiBAAh+QQJCQAaACwAAAAAGAAYAIQkJiScnpzU0tTs6uxkZmQ8Pjy8vrzc3tz09vTMysw0NjTc2tz08vTExsTk5uT8/vwsKizU1tTs7uyMioxMTkzEwsTk4uT8+vzMzsw8Ojz///8AAAAAAAAAAAAAAAAAAAAF76AmjtrVTMTBCIf0kPB4BQVgR4NRVY31xqIFBQAhAgS5ikGXQAA1AoVtKpAor4ZIDBG5RG0QioWR0C0FD4ZT9CgLvJmJhXRZVN6MSuJnMb/XMQxpSgZzDw2EFQxPbA1mDQ9WZgeMIwc6ShILZhWAjBdLSgcCZgmVJBhXAgwSEgyLpyKsDAOvrhKelaytK6GmsRoJVxgHiblACFgtmAaUp3ZmEiahBrBPh6UXGhaqFz+BgzrObQZ4DQeedRUYg3sjDF15ZhgIZEs6eMcMjleKSYlakJXBQouanmMjHlhAtARBEgMJDnxjFGlUPRYugIQAADs=" />',
			link: function(scope) {
			}
		}
	});

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsBlockContainer
	 * @name rbsBlockContainer
	 *
	 * @description
	 * Initialize scope with blockId, blockParameters, and blockData.
	 *
	 * @param {int} rbsBlockContainer The block id.
	 */
	app.directive('rbsBlockContainer', ['RbsChange.AjaxAPI', function(AjaxAPI) {
		return {
			restrict: 'A',
			scope: true,
			link: {
				pre: function(scope, element, attrs) {
					var blockId = attrs['rbsBlockContainer'];
					scope.blockId = blockId;
					scope.blockParameters = AjaxAPI.getBlockParameters(blockId);
					scope.blockData = AjaxAPI.globalVar(blockId);
				}
			}
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChangeApp.directive:rbsTrackConsultedDocument
	 * @name rbsTrackConsultedDocument
	 *
	 * @description
	 * Tracks a document consultation.
	 *
	 * @param {int} rbsTrackConsultedDocument The document id.
	 * @param {string} modelName The document model name.
	 * @param {string=} context A context that may be used to discriminate consultations.
	 */
	app.directive('rbsTrackConsultedDocument', ['RbsChange.AjaxAPI', '$cookies', function(AjaxAPI, $cookies) {
		function getCookieExpireDate(days) {
			var cookieTimeout = days * 86400000; // days in milliseconds.
			var date = new Date();
			date.setTime(date.getTime() + cookieTimeout);
			return date.toGMTString();
		}

		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var data = {
					documentId: attrs.rbsTrackConsultedDocument,
					modelName: attrs.modelName
				};
				if (!data.documentId) {
					console.warn('rbsTrackConsultedDocument: no document id');
				}
				else if (!data.modelName) {
					console.warn('rbsTrackConsultedDocument: no document model name');
				}
				else {
					// Check trackers consent, handle cookies locally.
					var trackersManager = AjaxAPI.globalVar('rbsWebsiteTrackersManager');
					var cookieName = 'rbsConsultedDocuments_' + data.modelName;
					if (angular.isObject(trackersManager) && trackersManager.isAllowed()) {
						var oldValue = $cookies.getObject(cookieName);
						if (!angular.isArray(oldValue)) {
							oldValue = [];
						}
						oldValue.push(data.documentId);

						var newValue = [], obj = {};
						angular.forEach(oldValue, function(docId) {
							if (!obj[docId]) {
								newValue.splice(0, 0, docId);
								obj[docId] = true;
							}
						});

						if (newValue.length - trackersManager.configuration.consultedMaxCount > 0) {
							newValue.splice(trackersManager.configuration.consultedMaxCount, newValue.length);
						}

						var persistDays = trackersManager.configuration.consultedPersistDays;
						$cookies.putObject(cookieName, newValue, { expires: getCookieExpireDate(persistDays) });
					}
					else {
						$cookies.remove(cookieName);
					}
				}
			}
		}
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsRbsChangeAppChange.filter:rbsTrustHtml
	 * @name rbsTrustHtml
	 * @kind function
	 *
	 * @description
	 * A simple wrapper for `$sce.trustAsHtml`.
	 *
	 * @param {string} key The i18n key.
	 */
	app.filter('rbsTrustHtml', ['$sce', function($sce) {
		return function(html) {
			return $sce.trustAsHtml(html);
		};
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsRbsChangeAppChange.filter:rbsI18n
	 * @name rbsI18n
	 * @kind function
	 *
	 * @description
	 * Translate an i18n key. The key are declared un `__change.i18n`.
	 *
	 * @param {string} key The i18n key.
	 */
	app.filter('rbsI18n', ['RbsChange.AjaxAPI', function(AjaxAPI) {
		var i18n = AjaxAPI.globalVar('i18n');

		function filter(key) {
			return i18n[key];
		}

		return filter;
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChangeApp.filter:rbsDate
	 * @name rbsDate
	 * @kind function
	 *
	 * @description
	 * Formats a date without time. This filter is similar to the standard {@link date date} one, but just adds a default value to the format.
	 *
	 * @param {Object} input The date.
	 * @param {string=} format The date format.
	 */
	app.filter('rbsDate', ['RbsChange.AjaxAPI', '$filter', function(AjaxAPI, $filter) {
		var i18n = AjaxAPI.globalVar('i18n');

		function filter(input, format) {
			if (angular.isUndefined(format)) {
				format = i18n.dateFormat;
			}
			return $filter('date')(input, format);
		}

		return filter;
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChangeApp.filter:rbsDateTime
	 * @name rbsDateTime
	 * @kind function
	 *
	 * @description
	 * Formats a date with time. This filter is similar to the standard {@link date date} one, but just adds a default value to the format.
	 *
	 * @param {Object} input The date.
	 * @param {string=} format The date format.
	 */
	app.filter('rbsDateTime', ['RbsChange.AjaxAPI', '$filter', function(AjaxAPI, $filter) {
		var i18n = AjaxAPI.globalVar('i18n');

		function filter(input, format) {
			if (angular.isUndefined(format)) {
				format = i18n.dateTimeFormat;
			}
			return $filter('date')(input, format);
		}

		return filter;
	}]);

	/**
	 * @ngdoc filter
	 * @id RbsChangeApp.filter:rbsAttribute
	 * @name rbsAttribute
	 * @kind function
	 *
	 * @description
	 * Returns the attribute definition.
	 * If more than one document is given, each one will be evaluated iteratively until a value is found.
	 *
	 * @param {Object} documentData The data of the document.
	 * @param {string} key The key of the attribute.
	 * @param {...Object=} ancestorDocumentData The data of the ancestor documents.
	 */
	app.filter('rbsAttribute', function() {
		function filter(documentData, key) {
			var args = Array.prototype.slice.call(arguments);
			if (args.length < 2 || !angular.isString(key)) {
				return null;
			}

			var definition = null;
			for (var i = 0; i < args.length; i++) {
				if (i == 1) {
					continue;
				}
				var document = args[i];
				if (angular.isObject(document) && angular.isObject(document['typology']) &&
					angular.isObject(document['typology']['attributes']) &&
					angular.isObject(document['typology']['attributes'][key])) {
					definition = document['typology']['attributes'][key];
					if (definition.value !== null) {
						return definition;
					}
				}
			}
			return definition;
		}

		return filter;
	});

	/**
	 * @ngdoc filter
	 * @id RbsChangeApp.filter:rbsVisibleAttributes
	 * @name rbsVisibleAttributes
	 * @kind function
	 *
	 * @description
	 * Returns the attribute visible attributes for a visibility context.
	 * These attributes may be returned as an array of groups (mode 'group') or a flat array of attributes (mode 'flat').
	 *
	 * @param {Object} documentData The data of the documents.
	 * @param {string} visibility The visibility context name.
	 * @param {string} mode The attribute list mode 'flat' or 'group'.
	 * @param {...Object=} ancestorDocumentData The data of the ancestor documents.
	 */
	app.filter('rbsVisibleAttributes', ['$filter', function($filter) {
		function filter(documentData, visibility, mode) {
			var args = Array.prototype.slice.call(arguments);
			if (args.length < 3 || !angular.isString(visibility) || mode != 'flat' && mode != 'group' || !angular.isObject(documentData)) {
				return [];
			}

			function getGroupData(group, document) {
				for (var i = 0; i < groups.length; i++) {
					if (groups[i].key === group.key) {
						return groups[i];
					}
				}
				var groupData = angular.copy(document['typology']['groups'][group.key]);
				groupData.attributes = [];
				groups.push(groupData);
				return groupData;
			}

			var groups = [];
			var attributes = [];
			for (var i = 0; i < args.length; i++) {
				var document = args[i];
				if (i == 1 || i == 2 || !angular.isObject(document) || !angular.isObject(document['typology']) ||
					!angular.isArray(document['typology']['visibilities'][visibility])) {
					continue;
				}

				angular.forEach(document['typology']['visibilities'][visibility], function(group) {
					var groupData = getGroupData(group, document);

					angular.forEach(group.items, function(item) {
						for (var i = 0; i < attributes.length; i++) {
							if (attributes[i].key === item.key) {
								return;
							}
						}

						var attribute = $filter('rbsAttribute')(document, item.key);
						if (angular.isObject(attribute)) {
							attribute.key = item.key;
							attributes.push(attribute);
							groupData.attributes.push(attribute);
						}
					});
				});
			}

			if (mode == 'flat') {
				return attributes;
			}
			else {
				return groups;
			}
		}

		return filter;
	}]);

	/**
	 * @ngdoc directive
	 * @name RbsChangeApp.directive:rbsAttributeValue
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * Renders an attribute value.
	 *
	 * @param {Object} rbsAttributeValue The attribute data.
	 */
	app.directive('rbsAttributeValue', ['RbsChange.AjaxAPI', '$sce', '$compile', function(AjaxAPI, $sce, $compile) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value.twig',
			replace: false,
			scope: { attribute: '=rbsAttributeValue' },
			link: function(scope, element, attributes) {
				scope.imageFormat = attributes['imageFormat'] || 'attribute';

				scope.isDocumentArray = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.type == 'DocumentIdArray');
				};

				scope.isJSON = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.type == 'JSON');
				};

				scope.isDocument = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.type == 'DocumentId');
				};

				scope.isLinkValue = function(value) {
					return (value && value.common.URL && value.common.URL['canonical']);
				};

				scope.isLink = function(attribute) {
					attribute = attribute || scope.attribute;
					return (scope.isDocument(attribute) && scope.isLinkValue(attribute.value));
				};

				scope.isHtml = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.type == 'RichText' && angular.isString(attribute.value));
				};

				scope.isDate = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.type == 'Date');
				};

				scope.isDateTime = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.type == 'DateTime');
				};

				scope.isString = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && !scope.isDocumentArray(attribute) && !scope.isDocument(attribute) && !scope.isHtml(attribute) &&
					!scope.isDate(attribute) && !scope.isDateTime(attribute) && !scope.isJSON(attribute));
				};

				scope.getValueTitle = function(attribute) {
					if (attribute && attribute.formattedValue) {
						var formattedValue = attribute.formattedValue;
						if (angular.isString(formattedValue)) {
							return formattedValue;
						}
						else if (angular.isObject(formattedValue) && angular.isObject(formattedValue.common)
							&& formattedValue.common.title) {
							return formattedValue.common.title;
						}
					}
					return attribute.value;
				};

				scope.$watch('attribute', function(attribute) {
					var directiveName = attribute.renderingMode;
					if (!directiveName) {
						directiveName = 'default';
					}
					else {
						directiveName = directiveName.replace(/([A-Z])/g, function(_, letter, offset) {
							return (offset ? '-' : '') + letter.toLowerCase();
						});
					}
					directiveName = 'rbs-attribute-value-' + directiveName;

					var container = element.find('.attribute-value-container');
					container.html('<div data-' + directiveName + '=""></div>');
					$compile(container.contents())(scope);
				}, true);

				// @deprecated since 1.8.0, use the rbsTrustHtml filter instead.
				scope.trustHtml = function(html) {
					console.warn('trustHtml() is deprecated since 1.8.0, use the rbsTrustHtml filter instead.');
					return $sce.trustAsHtml(html);
				};
				// END @deprecated
			}
		}
	}]);

	app.directive('rbsAttributeValueDefault', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value-default.twig'
		}
	});

	app.directive('rbsAttributeValueImageFormatted', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value-image-formatted.twig'
		}
	});

	app.directive('rbsAttributeValueImageFull', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value-image-full.twig'
		}
	});

	app.directive('rbsAttributeValueVideo', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value-video.twig'
		}
	});

	app.directive('rbsAttributeValueFile', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value-file.twig'
		}
	});

	app.directive('rbsAttributeValueHtmlFragment', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-attribute-value-html-fragment.twig'
		}
	});

	app.directive('rbsDirectiveGenerator', ['$compile', function($compile) {
		return {
			link: function(scope, element, attrs) {
				if (attrs.rbsDirectiveGenerator) {
					element.append($compile('<div data-' + attrs.rbsDirectiveGenerator + '=""></div>')(scope));
				}
			}
		};
	}]);

	/**
	 * @ngdoc provider
	 * @id RbsChangeApp.provider:AjaxAPIProvider
	 * @name AjaxAPIProvider
	 *
	 * @description
	 * The provider for the {@link RbsChange.service:AjaxAPI AJAX API service}, allowing to configure it.
	 */
	app.provider('RbsChange.AjaxAPI', function AjaxAPIProvider() {
		var apiURL = '/ajax.V1.php/', LCID = 'fr_FR',
			defaultParams = { websiteId: null, sectionId: null, pageId: null, data: {} };
		var headers = { "Content-Type": "application/json" };

		/**
		 * @ngdoc method
		 * @methodOf AjaxAPIProvider
		 * @name setHeader
		 *
		 * @description
		 * Adds a header to *all* AJAX request done by AjaxAPI.
		 *
		 * @param {string} name The name of the header.
		 * @param {string} value The new value of the header.
		 */
		this.setHeader = function(name, value) {
			headers[name] = value;
		};

		/**
		 * @ngdoc service
		 * @id RbsChange.service:AjaxAPI
		 * @name AjaxAPI
		 *
		 * @description
		 * This service provides methods to access to the AJAX API.
		 */
		this.$get = ['$http', '$location', '$rootScope', 'RbsChange.ModalStack', '$uibModal',
			function($http, $location, $rootScope, ModalStack, $uibModal) {
				if (angular.isObject(__change)) {
					if (angular.isObject(__change.navigationContext)) {
						var data = __change.navigationContext;
						if (data.websiteId) {
							defaultParams.websiteId = data.websiteId;
						}
						if (data.sectionId) {
							defaultParams.sectionId = data.sectionId;
						}
						if (data['pageIdentifier']) {
							var p = data['pageIdentifier'].split(',');
							if (p.length == 2) {
								defaultParams.pageId = parseInt(p[0], 10);
								LCID = p[1];
							}
						}
					}
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name globalVar
				 *
				 * @description
				 * Get or set the content of a specific key of the `__change` global var.
				 *
				 * @param {string} name The name of the global var.
				 * @param {*=} value The new value of the global var.
				 * @returns {*} The current value of the gloval var.
				 */
				function globalVar(name, value) {
					if (angular.isObject(__change)) {
						if (angular.isUndefined(value)) {
							return __change.hasOwnProperty(name) ? __change[name] : value;
						}
						else {
							return __change[name] = value;
						}
					}
					return __change;
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name getVersion
				 *
				 * @description
				 * Get the current API version.
				 *
				 * @returns {string} The current API version.
				 */
				function getVersion() {
					return 'V1';
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name getLCID
				 *
				 * @description
				 * Get the current LCID.
				 *
				 * @returns {string} The current LCID.
				 */
				function getLCID() {
					return LCID;
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name getBlockParameters
				 *
				 * @description
				 * Get the parameters of a block.
				 *
				 * @param {string} blockId The block id corresponding to the `blockId` var available in block templates.
				 * @returns {Object} The block's parameters.
				 */
				function getBlockParameters(blockId) {
					if (angular.isObject(__change.blockParameters[blockId])) {
						return __change.blockParameters[blockId];
					}
					console.error('Parameters not found for block', blockId);
					return {};
				}

				function getDefaultParams() {
					return angular.copy(defaultParams);
				}

				function getHttpConfig(method, actionPath) {
					if (angular.isArray(actionPath)) {
						actionPath = actionPath.join('/');
					}

					var config = {
						method: 'POST', url: apiURL + LCID + '/' + actionPath,
						headers: angular.copy(headers)
					};
					config.headers['X-HTTP-Method-Override'] = method;
					return config;
				}

				function buildConfigData(data, params) {
					var configData = getDefaultParams();
					if (angular.isObject(params)) {
						angular.extend(configData, params);
					}
					if (angular.isObject(data)) {
						angular.extend(configData.data, data);
					}
					return configData;
				}

				function sendData(method, actionPath, data, params) {
					var config = getHttpConfig(method, actionPath);
					config.data = buildConfigData(data, params);
					return $http(config);
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name getData
				 *
				 * @description
				 * Send a `GET` request to AJAX API.
				 *
				 * @param {string} actionPath The action path.
				 * @param {Object=} data The the content of the `data` entry.
				 * @param {Object=} params Additional values for the standard params. Main params like the website have default value and don't need
				 *    to be specified here.
				 * @returns {HttpPromise} Future object.
				 */
				function getData(actionPath, data, params) {
					return sendData('GET', actionPath, data, params);
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name postData
				 *
				 * @description
				 * Send a `POST` request to AJAX API.
				 *
				 * @param {string} actionPath The action path.
				 * @param {Object=} data The the content of the `data` entry.
				 * @param {Object=} params Additional values for the standard params. Main params like the website have default value and don't need
				 *    to be specified here.
				 * @returns {HttpPromise} Future object.
				 */
				function postData(actionPath, data, params) {
					return sendData('POST', actionPath, data, params);
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name putData
				 *
				 * @description
				 * Send a `PUT` request to AJAX API.
				 *
				 * @param {string} actionPath The action path.
				 * @param {Object=} data The the content of the `data` entry.
				 * @param {Object=} params Additional values for the standard params. Main params like the website have default value and don't need
				 *    to be specified here.
				 * @returns {HttpPromise} Future object.
				 */
				function putData(actionPath, data, params) {
					return sendData('PUT', actionPath, data, params);
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name deleteData
				 *
				 * @description
				 * Send a `DELETE` request to AJAX API.
				 *
				 * @param {string} actionPath The action path.
				 * @param {Object=} data The the content of the `data` entry.
				 * @param {Object=} params Additional values for the standard params. Main params like the website have default value and don't need
				 *    to be specified here.
				 * @returns {HttpPromise} Future object.
				 */
				function deleteData(actionPath, data, params) {
					return sendData('DELETE', actionPath, data, params);
				}

				var navigationContext = globalVar('navigationContext');
				var themeName = (angular.isObject(navigationContext) ? navigationContext.themeName : null) || 'Rbs_Base';
				var themePath = 'Theme/' + themeName.split('_').join('/');

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name getThemeName
				 *
				 * @description
				 * Get the name to the current theme.
				 *
				 * @returns {string} The current theme name.
				 */
				function getThemeName() {
					return themeName;
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name getThemePath
				 *
				 * @description
				 * Get the path to the current theme. Mainly used to refer to dynamically loaded directive templates provided by the theme.
				 *
				 * @returns {string} The current theme path.
				 */
				function getThemePath() {
					return themePath;
				}

				var waitingModal;

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name openWaitingModal
				 *
				 * @description
				 * Open a waiting modal. Mainly used before sending an AJAX request and followed by a call to closeWaitingModal()
				 * when the request ends.
				 *
				 * @param {string=} message An optional message to display. If omitted, a default message is used.
				 */
				function openWaitingModal(message) {
					var options = {
						templateUrl: '/rbs-ajax-waiting-modal.twig',
						keyboard: false,
						backdrop: 'static',
						backdropClass: 'modal-backdrop-ajax-waiting-modal',
						windowClass: 'modal-ajax-waiting-modal',
						size: 'sm',
						controller: 'RbsAjaxWaitingModal',
						resolve: {
							message: function() {
								return message;
							}
						}
					};
					waitingModal = $uibModal.open(options);
				}

				/**
				 * @ngdoc method
				 * @methodOf AjaxAPI
				 * @name closeWaitingModal
				 *
				 * @description
				 * Open a waiting modal. Mainly used before sending an AJAX request and followed by a call to closeWaitingModal()
				 * when the request ends.
				 *
				 * @param {number=} [parentModalsToClose=0] The number of parent modals that should be closed.
				 */
				function closeWaitingModal(parentModalsToClose) {
					if (parentModalsToClose) {
						ModalStack.close(parentModalsToClose - 1);
					}

					var modal = waitingModal;
					waitingModal = null;

					var deferredDismiss = function() {
						modal.dismiss('cancel');
					};
					modal.opened.then(deferredDismiss, deferredDismiss);
				}

				// Public API
				return {
					getVersion: getVersion,
					getLCID: getLCID,
					globalVar: globalVar,
					getBlockParameters: getBlockParameters,
					getDefaultParams: getDefaultParams,
					getData: getData,
					postData: postData,
					putData: putData,
					deleteData: deleteData,
					getThemeName: getThemeName,
					getThemePath: getThemePath,
					openWaitingModal: openWaitingModal,
					closeWaitingModal: closeWaitingModal
				};
			}];
	});

	app.controller('RbsAjaxWaitingModal', ['$uibModalInstance', '$scope', 'message', function(modalInstance, scope, message) {
		scope.message = message;
		var closeFunction = function() {
			// Nothing to do. but empty function necessary for "unhandled rejection cancel error"
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}]);

	/**
	 * @ngdoc service
	 * @id RbsChangeApp.service:ModalStack
	 * @name ModalStack
	 *
	 * @description
	 * This service is used to manage a modal stack where opening a new modal hides the others.
	 * It provides methods to close the last modal and show back the previous or close the full stack.
	 * This is useful for cascading modals, where the child extends the scope of the parent.
	 */
	app.service('RbsChange.ModalStack', ['$uibModal', '$timeout', function($uibModal, $timeout) {
		var className = 'modal-hidden-stack';
		var opened = [];

		/**
		 * @ngdoc method
		 * @methodOf ModalStack
		 * @name open
		 *
		 * @description
		 * Open a new modal and hide any other modal in the stack.
		 *
		 * @param {Object} options A list of options for the modal.
		 *    See {@link http://angular-ui.github.io/bootstrap/#/modal UI-bootstrap modal} for the list of available options.
		 */
		function open(options) {
			hideStack();
			var classNames = className + ' modal-stack-idx-' + (opened.length + 1);
			if (options.windowClass) {
				options.windowClass += ' ' + classNames;
			}
			else {
				options.windowClass = classNames;
			}

			var modal = $uibModal.open(options);

			if (!options.hasOwnProperty('controller')) {
				var closeAllFunction = function() {
					closeAll();
				};
				modal.result.then(closeAllFunction, closeAllFunction);
			}
			opened.push(modal);
			return modal;
		}

		/**
		 * @ngdoc method
		 * @methodOf ModalStack
		 * @name close
		 *
		 * @description
		 * Close the last modal (and `parentModalsToClose` parent ones) and show the previous one.
		 *
		 * @param {number=} [parentModalsToClose=0] The number of parent modals that should be closed.
		 */
		function close(parentModalsToClose) {
			if (opened.length) {
				var modal = opened[opened.length - 1];
				var deferredDismiss = function() {
					$timeout(function() {
						modal.dismiss('cancel');
					});
				};
				modal.opened.then(deferredDismiss, deferredDismiss);
			}
			if (parentModalsToClose > 0) {
				opened.pop();
				close(parentModalsToClose - 1);
			}
			else {
				showPrevious();
			}
		}

		/**
		 * @ngdoc method
		 * @methodOf ModalStack
		 * @name closeAll
		 *
		 * @description
		 * Close each modals in the stack.
		 */
		function closeAll() {
			for (var i = 0; i < opened.length; i++) {
				opened[i].dismiss('close all');
			}
			opened = [];
		}

		/**
		 * @ngdoc method
		 * @methodOf ModalStack
		 * @name showPrevious
		 *
		 * @description
		 * Show the previous opened modal. Call it only when the current one is closed.
		 */
		function showPrevious() {
			opened.pop();
			if (opened.length) {
				jQuery('.modal-stack-idx-' + opened.length).show();
			}
		}

		function hideStack() {
			jQuery('.' + className).hide();
		}

		this.open = open;
		this.close = close;
		this.closeAll = closeAll;
		this.showPrevious = showPrevious;
	}]);

	/**
	 * @ngdoc service
	 * @id RbsChangeApp.service:ResponsiveSummaries
	 * @name ResponsiveSummaries
	 *
	 * @description
	 * This service handle content registration for the ResponsiveSummary block.
	 */
	app.service('RbsChange.ResponsiveSummaries', ['$rootScope', function($rootScope) {
		var items = {};

		/**
		 * @ngdoc method
		 * @methodOf ResponsiveSummaries
		 * @name registerItem
		 *
		 * @description
		 * Register an item.
		 *
		 * @param {string} name The item name.
		 * @param {Object} scope The scope to compile.
		 * @param {Object} toCompile The HTML code to compile.
		 * @param {Object=} options Some options.
		 */
		this.registerItem = function(name, scope, toCompile, options) {
			items[name] = {
				name: name,
				scope: scope,
				toCompile: toCompile,
				options: angular.isObject(options) ? options : {}
			};

			$rootScope.$emit('ResponsiveSummaries.updated');
		};

		/**
		 * @ngdoc method
		 * @methodOf ResponsiveSummaries
		 * @name getItems
		 *
		 * @description
		 * Get the named items. If an item from the list is not registered, it is ignored.
		 *
		 * @param {Array} names The sorted item names.
		 * @returns {Array}
		 */
		this.getItems = function(names) {
			var result = [];
			for (var i = 0; i < names.length; i++) {
				if (items[names[i]]) {
					result.push(items[names[i]]);
				}
			}
			return result;
		}
	}]);

	/**
	 * @ngdoc service
	 * @id RbsChangeApp.service:RecursionHelper
	 * @name RecursionHelper
	 *
	 * @description
	 * Compiles the element, fixing the recursion loop.
	 */
	app.factory('RbsChange.RecursionHelper', ['$compile', function($compile) {
		return {
			/**
			 * @ngdoc method
			 * @methodOf RecursionHelper
			 * @name compile
			 *
			 * @description
			 * Manually compiles the element, fixing the recursion loop.
			 *
			 * @param {DOMElement} element The element to compile.
			 * @param {Object|Function} link A post-link function, or an object with function(s) registered via pre and post properties.
			 * @returns {Object} An object containing the linking functions.
			 */
			compile: function(element, link) {
				// Normalize the link parameter
				if (angular.isFunction(link)) {
					link = { post: link };
				}

				// Break the recursion loop by removing the contents
				var contents = element.contents().remove();
				var compiledContents;
				return {
					pre: (link && link.pre) ? link.pre : null,
					/**
					 * Compiles and re-adds the contents
					 */
					post: function(scope, element) {
						// Compile the contents
						if (!compiledContents) {
							compiledContents = $compile(contents);
						}
						// Re-add the compiled contents to the element
						compiledContents(scope, function(clone) {
							element.append(clone);
						});

						// Call the post-linking function, if any
						if (link && link.post) {
							link.post.apply(null, arguments);
						}
					}
				};
			},

			/**
			 * @ngdoc method
			 * @methodOf RecursionHelper
			 * @name baseCompile
			 *
			 * @description
			 * Wrapper on $compile(element).
			 *
			 * @param {string|DOMElement} element The element to compile.
			 * @returns {Function}
			 */
			baseCompile: function(element) {
				return $compile(element);
			}
		};
	}]);

})(window.jQuery, window.__change);